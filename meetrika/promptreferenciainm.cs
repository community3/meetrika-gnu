/*
               File: PromptReferenciaINM
        Description: Selecione Referencia INM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:45:55.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptreferenciainm : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptreferenciainm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptreferenciainm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutReferenciaINM_Codigo ,
                           ref String aP1_InOutReferenciaINM_Descricao )
      {
         this.AV7InOutReferenciaINM_Codigo = aP0_InOutReferenciaINM_Codigo;
         this.AV8InOutReferenciaINM_Descricao = aP1_InOutReferenciaINM_Descricao;
         executePrivate();
         aP0_InOutReferenciaINM_Codigo=this.AV7InOutReferenciaINM_Codigo;
         aP1_InOutReferenciaINM_Descricao=this.AV8InOutReferenciaINM_Descricao;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkReferenciaINM_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_69 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_69_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_69_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17ReferenciaINM_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaINM_Descricao1", AV17ReferenciaINM_Descricao1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV21ReferenciaINM_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ReferenciaINM_Descricao2", AV21ReferenciaINM_Descricao2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV25ReferenciaINM_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ReferenciaINM_Descricao3", AV25ReferenciaINM_Descricao3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV35TFReferenciaINM_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFReferenciaINM_Codigo), 6, 0)));
               AV36TFReferenciaINM_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFReferenciaINM_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFReferenciaINM_Codigo_To), 6, 0)));
               AV39TFReferenciaINM_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFReferenciaINM_Descricao", AV39TFReferenciaINM_Descricao);
               AV40TFReferenciaINM_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFReferenciaINM_Descricao_Sel", AV40TFReferenciaINM_Descricao_Sel);
               AV43TFReferenciaINM_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFReferenciaINM_AreaTrabalhoCod), 6, 0)));
               AV44TFReferenciaINM_AreaTrabalhoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFReferenciaINM_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFReferenciaINM_AreaTrabalhoCod_To), 6, 0)));
               AV47TFReferenciaINM_AreaTrabalhoDes = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFReferenciaINM_AreaTrabalhoDes", AV47TFReferenciaINM_AreaTrabalhoDes);
               AV48TFReferenciaINM_AreaTrabalhoDes_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFReferenciaINM_AreaTrabalhoDes_Sel", AV48TFReferenciaINM_AreaTrabalhoDes_Sel);
               AV51TFReferenciaINM_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFReferenciaINM_Ativo_Sel", StringUtil.Str( (decimal)(AV51TFReferenciaINM_Ativo_Sel), 1, 0));
               AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace", AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace);
               AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace", AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace);
               AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace", AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace);
               AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace", AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace);
               AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace", AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace);
               AV33ReferenciaINM_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33ReferenciaINM_AreaTrabalhoCod), 6, 0)));
               AV60Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ReferenciaINM_Descricao1, AV19DynamicFiltersSelector2, AV21ReferenciaINM_Descricao2, AV23DynamicFiltersSelector3, AV25ReferenciaINM_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFReferenciaINM_Codigo, AV36TFReferenciaINM_Codigo_To, AV39TFReferenciaINM_Descricao, AV40TFReferenciaINM_Descricao_Sel, AV43TFReferenciaINM_AreaTrabalhoCod, AV44TFReferenciaINM_AreaTrabalhoCod_To, AV47TFReferenciaINM_AreaTrabalhoDes, AV48TFReferenciaINM_AreaTrabalhoDes_Sel, AV51TFReferenciaINM_Ativo_Sel, AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace, AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace, AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace, AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace, AV33ReferenciaINM_AreaTrabalhoCod, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutReferenciaINM_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutReferenciaINM_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutReferenciaINM_Descricao = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutReferenciaINM_Descricao", AV8InOutReferenciaINM_Descricao);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAE32( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV60Pgmname = "PromptReferenciaINM";
               context.Gx_err = 0;
               WSE32( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEE32( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117455594");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptreferenciainm.aspx") + "?" + UrlEncode("" +AV7InOutReferenciaINM_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutReferenciaINM_Descricao))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vREFERENCIAINM_DESCRICAO1", AV17ReferenciaINM_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vREFERENCIAINM_DESCRICAO2", AV21ReferenciaINM_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vREFERENCIAINM_DESCRICAO3", AV25ReferenciaINM_Descricao3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIAINM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFReferenciaINM_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIAINM_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFReferenciaINM_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIAINM_DESCRICAO", AV39TFReferenciaINM_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIAINM_DESCRICAO_SEL", AV40TFReferenciaINM_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIAINM_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFReferenciaINM_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIAINM_AREATRABALHOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFReferenciaINM_AreaTrabalhoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIAINM_AREATRABALHODES", AV47TFReferenciaINM_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIAINM_AREATRABALHODES_SEL", AV48TFReferenciaINM_AreaTrabalhoDes_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREFERENCIAINM_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFReferenciaINM_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_69", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_69), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV53DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV53DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREFERENCIAINM_CODIGOTITLEFILTERDATA", AV34ReferenciaINM_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREFERENCIAINM_CODIGOTITLEFILTERDATA", AV34ReferenciaINM_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREFERENCIAINM_DESCRICAOTITLEFILTERDATA", AV38ReferenciaINM_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREFERENCIAINM_DESCRICAOTITLEFILTERDATA", AV38ReferenciaINM_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREFERENCIAINM_AREATRABALHOCODTITLEFILTERDATA", AV42ReferenciaINM_AreaTrabalhoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREFERENCIAINM_AREATRABALHOCODTITLEFILTERDATA", AV42ReferenciaINM_AreaTrabalhoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREFERENCIAINM_AREATRABALHODESTITLEFILTERDATA", AV46ReferenciaINM_AreaTrabalhoDesTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREFERENCIAINM_AREATRABALHODESTITLEFILTERDATA", AV46ReferenciaINM_AreaTrabalhoDesTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREFERENCIAINM_ATIVOTITLEFILTERDATA", AV50ReferenciaINM_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREFERENCIAINM_ATIVOTITLEFILTERDATA", AV50ReferenciaINM_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV60Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTREFERENCIAINM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutReferenciaINM_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTREFERENCIAINM_DESCRICAO", AV8InOutReferenciaINM_Descricao);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Caption", StringUtil.RTrim( Ddo_referenciainm_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Tooltip", StringUtil.RTrim( Ddo_referenciainm_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Cls", StringUtil.RTrim( Ddo_referenciainm_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_referenciainm_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_referenciainm_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_referenciainm_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_referenciainm_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_referenciainm_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_referenciainm_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_referenciainm_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_referenciainm_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Filtertype", StringUtil.RTrim( Ddo_referenciainm_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_referenciainm_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_referenciainm_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Sortasc", StringUtil.RTrim( Ddo_referenciainm_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_referenciainm_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_referenciainm_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_referenciainm_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_referenciainm_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_referenciainm_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Caption", StringUtil.RTrim( Ddo_referenciainm_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_referenciainm_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Cls", StringUtil.RTrim( Ddo_referenciainm_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_referenciainm_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_referenciainm_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_referenciainm_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_referenciainm_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_referenciainm_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_referenciainm_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_referenciainm_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_referenciainm_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_referenciainm_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_referenciainm_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_referenciainm_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_referenciainm_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_referenciainm_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_referenciainm_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_referenciainm_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_referenciainm_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_referenciainm_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_referenciainm_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_referenciainm_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_referenciainm_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Caption", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Tooltip", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Cls", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_referenciainm_areatrabalhocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_referenciainm_areatrabalhocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Sortedstatus", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Includefilter", StringUtil.BoolToStr( Ddo_referenciainm_areatrabalhocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Filtertype", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_referenciainm_areatrabalhocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_referenciainm_areatrabalhocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Sortasc", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Sortdsc", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Cleanfilter", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Rangefilterto", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Caption", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Tooltip", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Cls", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Filteredtext_set", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Selectedvalue_set", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Dropdownoptionstype", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Includesortasc", StringUtil.BoolToStr( Ddo_referenciainm_areatrabalhodes_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Includesortdsc", StringUtil.BoolToStr( Ddo_referenciainm_areatrabalhodes_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Sortedstatus", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Includefilter", StringUtil.BoolToStr( Ddo_referenciainm_areatrabalhodes_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Filtertype", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Filterisrange", StringUtil.BoolToStr( Ddo_referenciainm_areatrabalhodes_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Includedatalist", StringUtil.BoolToStr( Ddo_referenciainm_areatrabalhodes_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Datalisttype", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Datalistproc", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_referenciainm_areatrabalhodes_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Sortasc", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Sortdsc", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Loadingdata", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Cleanfilter", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Noresultsfound", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Searchbuttontext", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Caption", StringUtil.RTrim( Ddo_referenciainm_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Tooltip", StringUtil.RTrim( Ddo_referenciainm_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Cls", StringUtil.RTrim( Ddo_referenciainm_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_referenciainm_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_referenciainm_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_referenciainm_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_referenciainm_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_referenciainm_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_referenciainm_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_referenciainm_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_referenciainm_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_referenciainm_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_referenciainm_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Sortasc", StringUtil.RTrim( Ddo_referenciainm_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_referenciainm_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_referenciainm_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_referenciainm_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_referenciainm_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_referenciainm_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_referenciainm_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_referenciainm_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_referenciainm_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_referenciainm_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Activeeventkey", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_referenciainm_areatrabalhocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Activeeventkey", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Filteredtext_get", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_AREATRABALHODES_Selectedvalue_get", StringUtil.RTrim( Ddo_referenciainm_areatrabalhodes_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_referenciainm_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REFERENCIAINM_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_referenciainm_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormE32( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptReferenciaINM" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Referencia INM" ;
      }

      protected void WBE30( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_E32( true) ;
         }
         else
         {
            wb_table1_2_E32( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_E32e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(80, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(81, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciainm_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFReferenciaINM_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFReferenciaINM_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciainm_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciainm_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaINM.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciainm_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFReferenciaINM_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFReferenciaINM_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciainm_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciainm_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaINM.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciainm_descricao_Internalname, AV39TFReferenciaINM_Descricao, StringUtil.RTrim( context.localUtil.Format( AV39TFReferenciaINM_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciainm_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciainm_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaINM.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciainm_descricao_sel_Internalname, AV40TFReferenciaINM_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV40TFReferenciaINM_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciainm_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciainm_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaINM.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciainm_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFReferenciaINM_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43TFReferenciaINM_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciainm_areatrabalhocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciainm_areatrabalhocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaINM.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciainm_areatrabalhocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFReferenciaINM_AreaTrabalhoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFReferenciaINM_AreaTrabalhoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciainm_areatrabalhocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciainm_areatrabalhocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaINM.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciainm_areatrabalhodes_Internalname, AV47TFReferenciaINM_AreaTrabalhoDes, StringUtil.RTrim( context.localUtil.Format( AV47TFReferenciaINM_AreaTrabalhoDes, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciainm_areatrabalhodes_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciainm_areatrabalhodes_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaINM.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciainm_areatrabalhodes_sel_Internalname, AV48TFReferenciaINM_AreaTrabalhoDes_Sel, StringUtil.RTrim( context.localUtil.Format( AV48TFReferenciaINM_AreaTrabalhoDes_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciainm_areatrabalhodes_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciainm_areatrabalhodes_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaINM.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfreferenciainm_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFReferenciaINM_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51TFReferenciaINM_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfreferenciainm_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfreferenciainm_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaINM.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REFERENCIAINM_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_referenciainm_codigotitlecontrolidtoreplace_Internalname, AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,92);\"", 0, edtavDdo_referenciainm_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaINM.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REFERENCIAINM_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Internalname, AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", 0, edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaINM.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REFERENCIAINM_AREATRABALHOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_referenciainm_areatrabalhocodtitlecontrolidtoreplace_Internalname, AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", 0, edtavDdo_referenciainm_areatrabalhocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaINM.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REFERENCIAINM_AREATRABALHODESContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_referenciainm_areatrabalhodestitlecontrolidtoreplace_Internalname, AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"", 0, edtavDdo_referenciainm_areatrabalhodestitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaINM.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REFERENCIAINM_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_69_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_referenciainm_ativotitlecontrolidtoreplace_Internalname, AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"", 0, edtavDdo_referenciainm_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptReferenciaINM.htm");
         }
         wbLoad = true;
      }

      protected void STARTE32( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Referencia INM", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPE30( ) ;
      }

      protected void WSE32( )
      {
         STARTE32( ) ;
         EVTE32( ) ;
      }

      protected void EVTE32( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11E32 */
                           E11E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_REFERENCIAINM_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12E32 */
                           E12E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_REFERENCIAINM_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13E32 */
                           E13E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_REFERENCIAINM_AREATRABALHOCOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14E32 */
                           E14E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_REFERENCIAINM_AREATRABALHODES.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15E32 */
                           E15E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_REFERENCIAINM_ATIVO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16E32 */
                           E16E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17E32 */
                           E17E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18E32 */
                           E18E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19E32 */
                           E19E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20E32 */
                           E20E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21E32 */
                           E21E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22E32 */
                           E22E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23E32 */
                           E23E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24E32 */
                           E24E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25E32 */
                           E25E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26E32 */
                           E26E32 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_69_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
                           SubsflControlProps_692( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV59Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A709ReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( edtReferenciaINM_Codigo_Internalname), ",", "."));
                           A710ReferenciaINM_Descricao = StringUtil.Upper( cgiGet( edtReferenciaINM_Descricao_Internalname));
                           A712ReferenciaINM_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtReferenciaINM_AreaTrabalhoCod_Internalname), ",", "."));
                           A713ReferenciaINM_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtReferenciaINM_AreaTrabalhoDes_Internalname));
                           n713ReferenciaINM_AreaTrabalhoDes = false;
                           A711ReferenciaINM_Ativo = StringUtil.StrToBool( cgiGet( chkReferenciaINM_Ativo_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E27E32 */
                                 E27E32 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E28E32 */
                                 E28E32 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E29E32 */
                                 E29E32 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Referenciainm_descricao1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO1"), AV17ReferenciaINM_Descricao1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Referenciainm_descricao2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO2"), AV21ReferenciaINM_Descricao2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Referenciainm_descricao3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO3"), AV25ReferenciaINM_Descricao3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciainm_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIAINM_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFReferenciaINM_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciainm_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIAINM_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFReferenciaINM_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciainm_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_DESCRICAO"), AV39TFReferenciaINM_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciainm_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_DESCRICAO_SEL"), AV40TFReferenciaINM_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciainm_areatrabalhocod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIAINM_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV43TFReferenciaINM_AreaTrabalhoCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciainm_areatrabalhocod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIAINM_AREATRABALHOCOD_TO"), ",", ".") != Convert.ToDecimal( AV44TFReferenciaINM_AreaTrabalhoCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciainm_areatrabalhodes Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_AREATRABALHODES"), AV47TFReferenciaINM_AreaTrabalhoDes) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciainm_areatrabalhodes_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_AREATRABALHODES_SEL"), AV48TFReferenciaINM_AreaTrabalhoDes_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfreferenciainm_ativo_sel Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIAINM_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV51TFReferenciaINM_Ativo_Sel )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E30E32 */
                                       E30E32 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEE32( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormE32( ) ;
            }
         }
      }

      protected void PAE32( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("REFERENCIAINM_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("REFERENCIAINM_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("REFERENCIAINM_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            GXCCtl = "REFERENCIAINM_ATIVO_" + sGXsfl_69_idx;
            chkReferenciaINM_Ativo.Name = GXCCtl;
            chkReferenciaINM_Ativo.WebTags = "";
            chkReferenciaINM_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkReferenciaINM_Ativo_Internalname, "TitleCaption", chkReferenciaINM_Ativo.Caption);
            chkReferenciaINM_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_692( ) ;
         while ( nGXsfl_69_idx <= nRC_GXsfl_69 )
         {
            sendrow_692( ) ;
            nGXsfl_69_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_69_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_69_idx+1));
            sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
            SubsflControlProps_692( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17ReferenciaINM_Descricao1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV21ReferenciaINM_Descricao2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV25ReferenciaINM_Descricao3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV35TFReferenciaINM_Codigo ,
                                       int AV36TFReferenciaINM_Codigo_To ,
                                       String AV39TFReferenciaINM_Descricao ,
                                       String AV40TFReferenciaINM_Descricao_Sel ,
                                       int AV43TFReferenciaINM_AreaTrabalhoCod ,
                                       int AV44TFReferenciaINM_AreaTrabalhoCod_To ,
                                       String AV47TFReferenciaINM_AreaTrabalhoDes ,
                                       String AV48TFReferenciaINM_AreaTrabalhoDes_Sel ,
                                       short AV51TFReferenciaINM_Ativo_Sel ,
                                       String AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace ,
                                       String AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace ,
                                       String AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace ,
                                       String AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace ,
                                       String AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace ,
                                       int AV33ReferenciaINM_AreaTrabalhoCod ,
                                       String AV60Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFE32( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIAINM_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "REFERENCIAINM_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A709ReferenciaINM_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIAINM_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A710ReferenciaINM_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "REFERENCIAINM_DESCRICAO", A710ReferenciaINM_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIAINM_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "REFERENCIAINM_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIAINM_ATIVO", GetSecureSignedToken( "", A711ReferenciaINM_Ativo));
         GxWebStd.gx_hidden_field( context, "REFERENCIAINM_ATIVO", StringUtil.BoolToStr( A711ReferenciaINM_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFE32( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV60Pgmname = "PromptReferenciaINM";
         context.Gx_err = 0;
      }

      protected void RFE32( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 69;
         /* Execute user event: E28E32 */
         E28E32 ();
         nGXsfl_69_idx = 1;
         sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
         SubsflControlProps_692( ) ;
         nGXsfl_69_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_692( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV17ReferenciaINM_Descricao1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV21ReferenciaINM_Descricao2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV25ReferenciaINM_Descricao3 ,
                                                 AV35TFReferenciaINM_Codigo ,
                                                 AV36TFReferenciaINM_Codigo_To ,
                                                 AV40TFReferenciaINM_Descricao_Sel ,
                                                 AV39TFReferenciaINM_Descricao ,
                                                 AV43TFReferenciaINM_AreaTrabalhoCod ,
                                                 AV44TFReferenciaINM_AreaTrabalhoCod_To ,
                                                 AV48TFReferenciaINM_AreaTrabalhoDes_Sel ,
                                                 AV47TFReferenciaINM_AreaTrabalhoDes ,
                                                 AV51TFReferenciaINM_Ativo_Sel ,
                                                 A710ReferenciaINM_Descricao ,
                                                 A709ReferenciaINM_Codigo ,
                                                 A712ReferenciaINM_AreaTrabalhoCod ,
                                                 A713ReferenciaINM_AreaTrabalhoDes ,
                                                 A711ReferenciaINM_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            lV17ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17ReferenciaINM_Descricao1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaINM_Descricao1", AV17ReferenciaINM_Descricao1);
            lV21ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV21ReferenciaINM_Descricao2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ReferenciaINM_Descricao2", AV21ReferenciaINM_Descricao2);
            lV25ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV25ReferenciaINM_Descricao3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ReferenciaINM_Descricao3", AV25ReferenciaINM_Descricao3);
            lV39TFReferenciaINM_Descricao = StringUtil.Concat( StringUtil.RTrim( AV39TFReferenciaINM_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFReferenciaINM_Descricao", AV39TFReferenciaINM_Descricao);
            lV47TFReferenciaINM_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV47TFReferenciaINM_AreaTrabalhoDes), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFReferenciaINM_AreaTrabalhoDes", AV47TFReferenciaINM_AreaTrabalhoDes);
            /* Using cursor H00E32 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV17ReferenciaINM_Descricao1, lV21ReferenciaINM_Descricao2, lV25ReferenciaINM_Descricao3, AV35TFReferenciaINM_Codigo, AV36TFReferenciaINM_Codigo_To, lV39TFReferenciaINM_Descricao, AV40TFReferenciaINM_Descricao_Sel, AV43TFReferenciaINM_AreaTrabalhoCod, AV44TFReferenciaINM_AreaTrabalhoCod_To, lV47TFReferenciaINM_AreaTrabalhoDes, AV48TFReferenciaINM_AreaTrabalhoDes_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_69_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A711ReferenciaINM_Ativo = H00E32_A711ReferenciaINM_Ativo[0];
               A713ReferenciaINM_AreaTrabalhoDes = H00E32_A713ReferenciaINM_AreaTrabalhoDes[0];
               n713ReferenciaINM_AreaTrabalhoDes = H00E32_n713ReferenciaINM_AreaTrabalhoDes[0];
               A712ReferenciaINM_AreaTrabalhoCod = H00E32_A712ReferenciaINM_AreaTrabalhoCod[0];
               A710ReferenciaINM_Descricao = H00E32_A710ReferenciaINM_Descricao[0];
               A709ReferenciaINM_Codigo = H00E32_A709ReferenciaINM_Codigo[0];
               A713ReferenciaINM_AreaTrabalhoDes = H00E32_A713ReferenciaINM_AreaTrabalhoDes[0];
               n713ReferenciaINM_AreaTrabalhoDes = H00E32_n713ReferenciaINM_AreaTrabalhoDes[0];
               /* Execute user event: E29E32 */
               E29E32 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 69;
            WBE30( ) ;
         }
         nGXsfl_69_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV17ReferenciaINM_Descricao1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV21ReferenciaINM_Descricao2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV25ReferenciaINM_Descricao3 ,
                                              AV35TFReferenciaINM_Codigo ,
                                              AV36TFReferenciaINM_Codigo_To ,
                                              AV40TFReferenciaINM_Descricao_Sel ,
                                              AV39TFReferenciaINM_Descricao ,
                                              AV43TFReferenciaINM_AreaTrabalhoCod ,
                                              AV44TFReferenciaINM_AreaTrabalhoCod_To ,
                                              AV48TFReferenciaINM_AreaTrabalhoDes_Sel ,
                                              AV47TFReferenciaINM_AreaTrabalhoDes ,
                                              AV51TFReferenciaINM_Ativo_Sel ,
                                              A710ReferenciaINM_Descricao ,
                                              A709ReferenciaINM_Codigo ,
                                              A712ReferenciaINM_AreaTrabalhoCod ,
                                              A713ReferenciaINM_AreaTrabalhoDes ,
                                              A711ReferenciaINM_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT
                                              }
         });
         lV17ReferenciaINM_Descricao1 = StringUtil.Concat( StringUtil.RTrim( AV17ReferenciaINM_Descricao1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaINM_Descricao1", AV17ReferenciaINM_Descricao1);
         lV21ReferenciaINM_Descricao2 = StringUtil.Concat( StringUtil.RTrim( AV21ReferenciaINM_Descricao2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ReferenciaINM_Descricao2", AV21ReferenciaINM_Descricao2);
         lV25ReferenciaINM_Descricao3 = StringUtil.Concat( StringUtil.RTrim( AV25ReferenciaINM_Descricao3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ReferenciaINM_Descricao3", AV25ReferenciaINM_Descricao3);
         lV39TFReferenciaINM_Descricao = StringUtil.Concat( StringUtil.RTrim( AV39TFReferenciaINM_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFReferenciaINM_Descricao", AV39TFReferenciaINM_Descricao);
         lV47TFReferenciaINM_AreaTrabalhoDes = StringUtil.Concat( StringUtil.RTrim( AV47TFReferenciaINM_AreaTrabalhoDes), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFReferenciaINM_AreaTrabalhoDes", AV47TFReferenciaINM_AreaTrabalhoDes);
         /* Using cursor H00E33 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV17ReferenciaINM_Descricao1, lV21ReferenciaINM_Descricao2, lV25ReferenciaINM_Descricao3, AV35TFReferenciaINM_Codigo, AV36TFReferenciaINM_Codigo_To, lV39TFReferenciaINM_Descricao, AV40TFReferenciaINM_Descricao_Sel, AV43TFReferenciaINM_AreaTrabalhoCod, AV44TFReferenciaINM_AreaTrabalhoCod_To, lV47TFReferenciaINM_AreaTrabalhoDes, AV48TFReferenciaINM_AreaTrabalhoDes_Sel});
         GRID_nRecordCount = H00E33_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ReferenciaINM_Descricao1, AV19DynamicFiltersSelector2, AV21ReferenciaINM_Descricao2, AV23DynamicFiltersSelector3, AV25ReferenciaINM_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFReferenciaINM_Codigo, AV36TFReferenciaINM_Codigo_To, AV39TFReferenciaINM_Descricao, AV40TFReferenciaINM_Descricao_Sel, AV43TFReferenciaINM_AreaTrabalhoCod, AV44TFReferenciaINM_AreaTrabalhoCod_To, AV47TFReferenciaINM_AreaTrabalhoDes, AV48TFReferenciaINM_AreaTrabalhoDes_Sel, AV51TFReferenciaINM_Ativo_Sel, AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace, AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace, AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace, AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace, AV33ReferenciaINM_AreaTrabalhoCod, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ReferenciaINM_Descricao1, AV19DynamicFiltersSelector2, AV21ReferenciaINM_Descricao2, AV23DynamicFiltersSelector3, AV25ReferenciaINM_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFReferenciaINM_Codigo, AV36TFReferenciaINM_Codigo_To, AV39TFReferenciaINM_Descricao, AV40TFReferenciaINM_Descricao_Sel, AV43TFReferenciaINM_AreaTrabalhoCod, AV44TFReferenciaINM_AreaTrabalhoCod_To, AV47TFReferenciaINM_AreaTrabalhoDes, AV48TFReferenciaINM_AreaTrabalhoDes_Sel, AV51TFReferenciaINM_Ativo_Sel, AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace, AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace, AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace, AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace, AV33ReferenciaINM_AreaTrabalhoCod, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ReferenciaINM_Descricao1, AV19DynamicFiltersSelector2, AV21ReferenciaINM_Descricao2, AV23DynamicFiltersSelector3, AV25ReferenciaINM_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFReferenciaINM_Codigo, AV36TFReferenciaINM_Codigo_To, AV39TFReferenciaINM_Descricao, AV40TFReferenciaINM_Descricao_Sel, AV43TFReferenciaINM_AreaTrabalhoCod, AV44TFReferenciaINM_AreaTrabalhoCod_To, AV47TFReferenciaINM_AreaTrabalhoDes, AV48TFReferenciaINM_AreaTrabalhoDes_Sel, AV51TFReferenciaINM_Ativo_Sel, AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace, AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace, AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace, AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace, AV33ReferenciaINM_AreaTrabalhoCod, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ReferenciaINM_Descricao1, AV19DynamicFiltersSelector2, AV21ReferenciaINM_Descricao2, AV23DynamicFiltersSelector3, AV25ReferenciaINM_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFReferenciaINM_Codigo, AV36TFReferenciaINM_Codigo_To, AV39TFReferenciaINM_Descricao, AV40TFReferenciaINM_Descricao_Sel, AV43TFReferenciaINM_AreaTrabalhoCod, AV44TFReferenciaINM_AreaTrabalhoCod_To, AV47TFReferenciaINM_AreaTrabalhoDes, AV48TFReferenciaINM_AreaTrabalhoDes_Sel, AV51TFReferenciaINM_Ativo_Sel, AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace, AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace, AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace, AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace, AV33ReferenciaINM_AreaTrabalhoCod, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ReferenciaINM_Descricao1, AV19DynamicFiltersSelector2, AV21ReferenciaINM_Descricao2, AV23DynamicFiltersSelector3, AV25ReferenciaINM_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFReferenciaINM_Codigo, AV36TFReferenciaINM_Codigo_To, AV39TFReferenciaINM_Descricao, AV40TFReferenciaINM_Descricao_Sel, AV43TFReferenciaINM_AreaTrabalhoCod, AV44TFReferenciaINM_AreaTrabalhoCod_To, AV47TFReferenciaINM_AreaTrabalhoDes, AV48TFReferenciaINM_AreaTrabalhoDes_Sel, AV51TFReferenciaINM_Ativo_Sel, AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace, AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace, AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace, AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace, AV33ReferenciaINM_AreaTrabalhoCod, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPE30( )
      {
         /* Before Start, stand alone formulas. */
         AV60Pgmname = "PromptReferenciaINM";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E27E32 */
         E27E32 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV53DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vREFERENCIAINM_CODIGOTITLEFILTERDATA"), AV34ReferenciaINM_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREFERENCIAINM_DESCRICAOTITLEFILTERDATA"), AV38ReferenciaINM_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREFERENCIAINM_AREATRABALHOCODTITLEFILTERDATA"), AV42ReferenciaINM_AreaTrabalhoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREFERENCIAINM_AREATRABALHODESTITLEFILTERDATA"), AV46ReferenciaINM_AreaTrabalhoDesTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREFERENCIAINM_ATIVOTITLEFILTERDATA"), AV50ReferenciaINM_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavReferenciainm_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavReferenciainm_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vREFERENCIAINM_AREATRABALHOCOD");
               GX_FocusControl = edtavReferenciainm_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33ReferenciaINM_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33ReferenciaINM_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV33ReferenciaINM_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavReferenciainm_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33ReferenciaINM_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17ReferenciaINM_Descricao1 = StringUtil.Upper( cgiGet( edtavReferenciainm_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaINM_Descricao1", AV17ReferenciaINM_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV21ReferenciaINM_Descricao2 = StringUtil.Upper( cgiGet( edtavReferenciainm_descricao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ReferenciaINM_Descricao2", AV21ReferenciaINM_Descricao2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV25ReferenciaINM_Descricao3 = StringUtil.Upper( cgiGet( edtavReferenciainm_descricao3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ReferenciaINM_Descricao3", AV25ReferenciaINM_Descricao3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciainm_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciainm_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREFERENCIAINM_CODIGO");
               GX_FocusControl = edtavTfreferenciainm_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFReferenciaINM_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFReferenciaINM_Codigo), 6, 0)));
            }
            else
            {
               AV35TFReferenciaINM_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfreferenciainm_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFReferenciaINM_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciainm_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciainm_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREFERENCIAINM_CODIGO_TO");
               GX_FocusControl = edtavTfreferenciainm_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFReferenciaINM_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFReferenciaINM_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFReferenciaINM_Codigo_To), 6, 0)));
            }
            else
            {
               AV36TFReferenciaINM_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfreferenciainm_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFReferenciaINM_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFReferenciaINM_Codigo_To), 6, 0)));
            }
            AV39TFReferenciaINM_Descricao = StringUtil.Upper( cgiGet( edtavTfreferenciainm_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFReferenciaINM_Descricao", AV39TFReferenciaINM_Descricao);
            AV40TFReferenciaINM_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfreferenciainm_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFReferenciaINM_Descricao_Sel", AV40TFReferenciaINM_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciainm_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciainm_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREFERENCIAINM_AREATRABALHOCOD");
               GX_FocusControl = edtavTfreferenciainm_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFReferenciaINM_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFReferenciaINM_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV43TFReferenciaINM_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfreferenciainm_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFReferenciaINM_AreaTrabalhoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciainm_areatrabalhocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciainm_areatrabalhocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREFERENCIAINM_AREATRABALHOCOD_TO");
               GX_FocusControl = edtavTfreferenciainm_areatrabalhocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFReferenciaINM_AreaTrabalhoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFReferenciaINM_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFReferenciaINM_AreaTrabalhoCod_To), 6, 0)));
            }
            else
            {
               AV44TFReferenciaINM_AreaTrabalhoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfreferenciainm_areatrabalhocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFReferenciaINM_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFReferenciaINM_AreaTrabalhoCod_To), 6, 0)));
            }
            AV47TFReferenciaINM_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtavTfreferenciainm_areatrabalhodes_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFReferenciaINM_AreaTrabalhoDes", AV47TFReferenciaINM_AreaTrabalhoDes);
            AV48TFReferenciaINM_AreaTrabalhoDes_Sel = StringUtil.Upper( cgiGet( edtavTfreferenciainm_areatrabalhodes_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFReferenciaINM_AreaTrabalhoDes_Sel", AV48TFReferenciaINM_AreaTrabalhoDes_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciainm_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfreferenciainm_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFREFERENCIAINM_ATIVO_SEL");
               GX_FocusControl = edtavTfreferenciainm_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFReferenciaINM_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFReferenciaINM_Ativo_Sel", StringUtil.Str( (decimal)(AV51TFReferenciaINM_Ativo_Sel), 1, 0));
            }
            else
            {
               AV51TFReferenciaINM_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfreferenciainm_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFReferenciaINM_Ativo_Sel", StringUtil.Str( (decimal)(AV51TFReferenciaINM_Ativo_Sel), 1, 0));
            }
            AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_referenciainm_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace", AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace);
            AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace", AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace);
            AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace = cgiGet( edtavDdo_referenciainm_areatrabalhocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace", AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace);
            AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace = cgiGet( edtavDdo_referenciainm_areatrabalhodestitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace", AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace);
            AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_referenciainm_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace", AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_69 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_69"), ",", "."));
            AV55GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV56GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_referenciainm_codigo_Caption = cgiGet( "DDO_REFERENCIAINM_CODIGO_Caption");
            Ddo_referenciainm_codigo_Tooltip = cgiGet( "DDO_REFERENCIAINM_CODIGO_Tooltip");
            Ddo_referenciainm_codigo_Cls = cgiGet( "DDO_REFERENCIAINM_CODIGO_Cls");
            Ddo_referenciainm_codigo_Filteredtext_set = cgiGet( "DDO_REFERENCIAINM_CODIGO_Filteredtext_set");
            Ddo_referenciainm_codigo_Filteredtextto_set = cgiGet( "DDO_REFERENCIAINM_CODIGO_Filteredtextto_set");
            Ddo_referenciainm_codigo_Dropdownoptionstype = cgiGet( "DDO_REFERENCIAINM_CODIGO_Dropdownoptionstype");
            Ddo_referenciainm_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_REFERENCIAINM_CODIGO_Titlecontrolidtoreplace");
            Ddo_referenciainm_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_CODIGO_Includesortasc"));
            Ddo_referenciainm_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_CODIGO_Includesortdsc"));
            Ddo_referenciainm_codigo_Sortedstatus = cgiGet( "DDO_REFERENCIAINM_CODIGO_Sortedstatus");
            Ddo_referenciainm_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_CODIGO_Includefilter"));
            Ddo_referenciainm_codigo_Filtertype = cgiGet( "DDO_REFERENCIAINM_CODIGO_Filtertype");
            Ddo_referenciainm_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_CODIGO_Filterisrange"));
            Ddo_referenciainm_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_CODIGO_Includedatalist"));
            Ddo_referenciainm_codigo_Sortasc = cgiGet( "DDO_REFERENCIAINM_CODIGO_Sortasc");
            Ddo_referenciainm_codigo_Sortdsc = cgiGet( "DDO_REFERENCIAINM_CODIGO_Sortdsc");
            Ddo_referenciainm_codigo_Cleanfilter = cgiGet( "DDO_REFERENCIAINM_CODIGO_Cleanfilter");
            Ddo_referenciainm_codigo_Rangefilterfrom = cgiGet( "DDO_REFERENCIAINM_CODIGO_Rangefilterfrom");
            Ddo_referenciainm_codigo_Rangefilterto = cgiGet( "DDO_REFERENCIAINM_CODIGO_Rangefilterto");
            Ddo_referenciainm_codigo_Searchbuttontext = cgiGet( "DDO_REFERENCIAINM_CODIGO_Searchbuttontext");
            Ddo_referenciainm_descricao_Caption = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Caption");
            Ddo_referenciainm_descricao_Tooltip = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Tooltip");
            Ddo_referenciainm_descricao_Cls = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Cls");
            Ddo_referenciainm_descricao_Filteredtext_set = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Filteredtext_set");
            Ddo_referenciainm_descricao_Selectedvalue_set = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Selectedvalue_set");
            Ddo_referenciainm_descricao_Dropdownoptionstype = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Dropdownoptionstype");
            Ddo_referenciainm_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_referenciainm_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Includesortasc"));
            Ddo_referenciainm_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Includesortdsc"));
            Ddo_referenciainm_descricao_Sortedstatus = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Sortedstatus");
            Ddo_referenciainm_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Includefilter"));
            Ddo_referenciainm_descricao_Filtertype = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Filtertype");
            Ddo_referenciainm_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Filterisrange"));
            Ddo_referenciainm_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Includedatalist"));
            Ddo_referenciainm_descricao_Datalisttype = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Datalisttype");
            Ddo_referenciainm_descricao_Datalistproc = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Datalistproc");
            Ddo_referenciainm_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_referenciainm_descricao_Sortasc = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Sortasc");
            Ddo_referenciainm_descricao_Sortdsc = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Sortdsc");
            Ddo_referenciainm_descricao_Loadingdata = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Loadingdata");
            Ddo_referenciainm_descricao_Cleanfilter = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Cleanfilter");
            Ddo_referenciainm_descricao_Noresultsfound = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Noresultsfound");
            Ddo_referenciainm_descricao_Searchbuttontext = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Searchbuttontext");
            Ddo_referenciainm_areatrabalhocod_Caption = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Caption");
            Ddo_referenciainm_areatrabalhocod_Tooltip = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Tooltip");
            Ddo_referenciainm_areatrabalhocod_Cls = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Cls");
            Ddo_referenciainm_areatrabalhocod_Filteredtext_set = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Filteredtext_set");
            Ddo_referenciainm_areatrabalhocod_Filteredtextto_set = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Filteredtextto_set");
            Ddo_referenciainm_areatrabalhocod_Dropdownoptionstype = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Dropdownoptionstype");
            Ddo_referenciainm_areatrabalhocod_Titlecontrolidtoreplace = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Titlecontrolidtoreplace");
            Ddo_referenciainm_areatrabalhocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Includesortasc"));
            Ddo_referenciainm_areatrabalhocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Includesortdsc"));
            Ddo_referenciainm_areatrabalhocod_Sortedstatus = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Sortedstatus");
            Ddo_referenciainm_areatrabalhocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Includefilter"));
            Ddo_referenciainm_areatrabalhocod_Filtertype = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Filtertype");
            Ddo_referenciainm_areatrabalhocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Filterisrange"));
            Ddo_referenciainm_areatrabalhocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Includedatalist"));
            Ddo_referenciainm_areatrabalhocod_Sortasc = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Sortasc");
            Ddo_referenciainm_areatrabalhocod_Sortdsc = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Sortdsc");
            Ddo_referenciainm_areatrabalhocod_Cleanfilter = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Cleanfilter");
            Ddo_referenciainm_areatrabalhocod_Rangefilterfrom = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Rangefilterfrom");
            Ddo_referenciainm_areatrabalhocod_Rangefilterto = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Rangefilterto");
            Ddo_referenciainm_areatrabalhocod_Searchbuttontext = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Searchbuttontext");
            Ddo_referenciainm_areatrabalhodes_Caption = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Caption");
            Ddo_referenciainm_areatrabalhodes_Tooltip = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Tooltip");
            Ddo_referenciainm_areatrabalhodes_Cls = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Cls");
            Ddo_referenciainm_areatrabalhodes_Filteredtext_set = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Filteredtext_set");
            Ddo_referenciainm_areatrabalhodes_Selectedvalue_set = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Selectedvalue_set");
            Ddo_referenciainm_areatrabalhodes_Dropdownoptionstype = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Dropdownoptionstype");
            Ddo_referenciainm_areatrabalhodes_Titlecontrolidtoreplace = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Titlecontrolidtoreplace");
            Ddo_referenciainm_areatrabalhodes_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Includesortasc"));
            Ddo_referenciainm_areatrabalhodes_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Includesortdsc"));
            Ddo_referenciainm_areatrabalhodes_Sortedstatus = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Sortedstatus");
            Ddo_referenciainm_areatrabalhodes_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Includefilter"));
            Ddo_referenciainm_areatrabalhodes_Filtertype = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Filtertype");
            Ddo_referenciainm_areatrabalhodes_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Filterisrange"));
            Ddo_referenciainm_areatrabalhodes_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Includedatalist"));
            Ddo_referenciainm_areatrabalhodes_Datalisttype = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Datalisttype");
            Ddo_referenciainm_areatrabalhodes_Datalistproc = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Datalistproc");
            Ddo_referenciainm_areatrabalhodes_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_referenciainm_areatrabalhodes_Sortasc = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Sortasc");
            Ddo_referenciainm_areatrabalhodes_Sortdsc = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Sortdsc");
            Ddo_referenciainm_areatrabalhodes_Loadingdata = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Loadingdata");
            Ddo_referenciainm_areatrabalhodes_Cleanfilter = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Cleanfilter");
            Ddo_referenciainm_areatrabalhodes_Noresultsfound = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Noresultsfound");
            Ddo_referenciainm_areatrabalhodes_Searchbuttontext = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Searchbuttontext");
            Ddo_referenciainm_ativo_Caption = cgiGet( "DDO_REFERENCIAINM_ATIVO_Caption");
            Ddo_referenciainm_ativo_Tooltip = cgiGet( "DDO_REFERENCIAINM_ATIVO_Tooltip");
            Ddo_referenciainm_ativo_Cls = cgiGet( "DDO_REFERENCIAINM_ATIVO_Cls");
            Ddo_referenciainm_ativo_Selectedvalue_set = cgiGet( "DDO_REFERENCIAINM_ATIVO_Selectedvalue_set");
            Ddo_referenciainm_ativo_Dropdownoptionstype = cgiGet( "DDO_REFERENCIAINM_ATIVO_Dropdownoptionstype");
            Ddo_referenciainm_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_REFERENCIAINM_ATIVO_Titlecontrolidtoreplace");
            Ddo_referenciainm_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_ATIVO_Includesortasc"));
            Ddo_referenciainm_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_ATIVO_Includesortdsc"));
            Ddo_referenciainm_ativo_Sortedstatus = cgiGet( "DDO_REFERENCIAINM_ATIVO_Sortedstatus");
            Ddo_referenciainm_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_ATIVO_Includefilter"));
            Ddo_referenciainm_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REFERENCIAINM_ATIVO_Includedatalist"));
            Ddo_referenciainm_ativo_Datalisttype = cgiGet( "DDO_REFERENCIAINM_ATIVO_Datalisttype");
            Ddo_referenciainm_ativo_Datalistfixedvalues = cgiGet( "DDO_REFERENCIAINM_ATIVO_Datalistfixedvalues");
            Ddo_referenciainm_ativo_Sortasc = cgiGet( "DDO_REFERENCIAINM_ATIVO_Sortasc");
            Ddo_referenciainm_ativo_Sortdsc = cgiGet( "DDO_REFERENCIAINM_ATIVO_Sortdsc");
            Ddo_referenciainm_ativo_Cleanfilter = cgiGet( "DDO_REFERENCIAINM_ATIVO_Cleanfilter");
            Ddo_referenciainm_ativo_Searchbuttontext = cgiGet( "DDO_REFERENCIAINM_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_referenciainm_codigo_Activeeventkey = cgiGet( "DDO_REFERENCIAINM_CODIGO_Activeeventkey");
            Ddo_referenciainm_codigo_Filteredtext_get = cgiGet( "DDO_REFERENCIAINM_CODIGO_Filteredtext_get");
            Ddo_referenciainm_codigo_Filteredtextto_get = cgiGet( "DDO_REFERENCIAINM_CODIGO_Filteredtextto_get");
            Ddo_referenciainm_descricao_Activeeventkey = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Activeeventkey");
            Ddo_referenciainm_descricao_Filteredtext_get = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Filteredtext_get");
            Ddo_referenciainm_descricao_Selectedvalue_get = cgiGet( "DDO_REFERENCIAINM_DESCRICAO_Selectedvalue_get");
            Ddo_referenciainm_areatrabalhocod_Activeeventkey = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Activeeventkey");
            Ddo_referenciainm_areatrabalhocod_Filteredtext_get = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Filteredtext_get");
            Ddo_referenciainm_areatrabalhocod_Filteredtextto_get = cgiGet( "DDO_REFERENCIAINM_AREATRABALHOCOD_Filteredtextto_get");
            Ddo_referenciainm_areatrabalhodes_Activeeventkey = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Activeeventkey");
            Ddo_referenciainm_areatrabalhodes_Filteredtext_get = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Filteredtext_get");
            Ddo_referenciainm_areatrabalhodes_Selectedvalue_get = cgiGet( "DDO_REFERENCIAINM_AREATRABALHODES_Selectedvalue_get");
            Ddo_referenciainm_ativo_Activeeventkey = cgiGet( "DDO_REFERENCIAINM_ATIVO_Activeeventkey");
            Ddo_referenciainm_ativo_Selectedvalue_get = cgiGet( "DDO_REFERENCIAINM_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO1"), AV17ReferenciaINM_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO2"), AV21ReferenciaINM_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREFERENCIAINM_DESCRICAO3"), AV25ReferenciaINM_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIAINM_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFReferenciaINM_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIAINM_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFReferenciaINM_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_DESCRICAO"), AV39TFReferenciaINM_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_DESCRICAO_SEL"), AV40TFReferenciaINM_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIAINM_AREATRABALHOCOD"), ",", ".") != Convert.ToDecimal( AV43TFReferenciaINM_AreaTrabalhoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIAINM_AREATRABALHOCOD_TO"), ",", ".") != Convert.ToDecimal( AV44TFReferenciaINM_AreaTrabalhoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_AREATRABALHODES"), AV47TFReferenciaINM_AreaTrabalhoDes) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREFERENCIAINM_AREATRABALHODES_SEL"), AV48TFReferenciaINM_AreaTrabalhoDes_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFREFERENCIAINM_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV51TFReferenciaINM_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E27E32 */
         E27E32 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27E32( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "REFERENCIAINM_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "REFERENCIAINM_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "REFERENCIAINM_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfreferenciainm_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciainm_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciainm_codigo_Visible), 5, 0)));
         edtavTfreferenciainm_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciainm_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciainm_codigo_to_Visible), 5, 0)));
         edtavTfreferenciainm_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciainm_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciainm_descricao_Visible), 5, 0)));
         edtavTfreferenciainm_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciainm_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciainm_descricao_sel_Visible), 5, 0)));
         edtavTfreferenciainm_areatrabalhocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciainm_areatrabalhocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciainm_areatrabalhocod_Visible), 5, 0)));
         edtavTfreferenciainm_areatrabalhocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciainm_areatrabalhocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciainm_areatrabalhocod_to_Visible), 5, 0)));
         edtavTfreferenciainm_areatrabalhodes_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciainm_areatrabalhodes_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciainm_areatrabalhodes_Visible), 5, 0)));
         edtavTfreferenciainm_areatrabalhodes_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciainm_areatrabalhodes_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciainm_areatrabalhodes_sel_Visible), 5, 0)));
         edtavTfreferenciainm_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfreferenciainm_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfreferenciainm_ativo_sel_Visible), 5, 0)));
         Ddo_referenciainm_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_ReferenciaINM_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_codigo_Internalname, "TitleControlIdToReplace", Ddo_referenciainm_codigo_Titlecontrolidtoreplace);
         AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace = Ddo_referenciainm_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace", AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace);
         edtavDdo_referenciainm_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_referenciainm_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_referenciainm_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_referenciainm_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_ReferenciaINM_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "TitleControlIdToReplace", Ddo_referenciainm_descricao_Titlecontrolidtoreplace);
         AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace = Ddo_referenciainm_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace", AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace);
         edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_referenciainm_areatrabalhocod_Titlecontrolidtoreplace = subGrid_Internalname+"_ReferenciaINM_AreaTrabalhoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhocod_Internalname, "TitleControlIdToReplace", Ddo_referenciainm_areatrabalhocod_Titlecontrolidtoreplace);
         AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace = Ddo_referenciainm_areatrabalhocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace", AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace);
         edtavDdo_referenciainm_areatrabalhocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_referenciainm_areatrabalhocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_referenciainm_areatrabalhocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_referenciainm_areatrabalhodes_Titlecontrolidtoreplace = subGrid_Internalname+"_ReferenciaINM_AreaTrabalhoDes";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhodes_Internalname, "TitleControlIdToReplace", Ddo_referenciainm_areatrabalhodes_Titlecontrolidtoreplace);
         AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace = Ddo_referenciainm_areatrabalhodes_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace", AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace);
         edtavDdo_referenciainm_areatrabalhodestitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_referenciainm_areatrabalhodestitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_referenciainm_areatrabalhodestitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_referenciainm_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_ReferenciaINM_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_ativo_Internalname, "TitleControlIdToReplace", Ddo_referenciainm_ativo_Titlecontrolidtoreplace);
         AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace = Ddo_referenciainm_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace", AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace);
         edtavDdo_referenciainm_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_referenciainm_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_referenciainm_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Referencia INM";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Descri��o", 0);
         cmbavOrderedby.addItem("2", "Guia", 0);
         cmbavOrderedby.addItem("3", "C�digo", 0);
         cmbavOrderedby.addItem("4", "de Trabalho", 0);
         cmbavOrderedby.addItem("5", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV53DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV53DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E28E32( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV34ReferenciaINM_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ReferenciaINM_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42ReferenciaINM_AreaTrabalhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46ReferenciaINM_AreaTrabalhoDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50ReferenciaINM_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtReferenciaINM_Codigo_Titleformat = 2;
         edtReferenciaINM_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Guia", AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINM_Codigo_Internalname, "Title", edtReferenciaINM_Codigo_Title);
         edtReferenciaINM_Descricao_Titleformat = 2;
         edtReferenciaINM_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINM_Descricao_Internalname, "Title", edtReferenciaINM_Descricao_Title);
         edtReferenciaINM_AreaTrabalhoCod_Titleformat = 2;
         edtReferenciaINM_AreaTrabalhoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINM_AreaTrabalhoCod_Internalname, "Title", edtReferenciaINM_AreaTrabalhoCod_Title);
         edtReferenciaINM_AreaTrabalhoDes_Titleformat = 2;
         edtReferenciaINM_AreaTrabalhoDes_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Trabalho", AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtReferenciaINM_AreaTrabalhoDes_Internalname, "Title", edtReferenciaINM_AreaTrabalhoDes_Title);
         chkReferenciaINM_Ativo_Titleformat = 2;
         chkReferenciaINM_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkReferenciaINM_Ativo_Internalname, "Title", chkReferenciaINM_Ativo.Title.Text);
         AV55GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridCurrentPage), 10, 0)));
         AV56GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34ReferenciaINM_CodigoTitleFilterData", AV34ReferenciaINM_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38ReferenciaINM_DescricaoTitleFilterData", AV38ReferenciaINM_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42ReferenciaINM_AreaTrabalhoCodTitleFilterData", AV42ReferenciaINM_AreaTrabalhoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46ReferenciaINM_AreaTrabalhoDesTitleFilterData", AV46ReferenciaINM_AreaTrabalhoDesTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50ReferenciaINM_AtivoTitleFilterData", AV50ReferenciaINM_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11E32( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV54PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV54PageToGo) ;
         }
      }

      protected void E12E32( )
      {
         /* Ddo_referenciainm_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_referenciainm_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_codigo_Internalname, "SortedStatus", Ddo_referenciainm_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_codigo_Internalname, "SortedStatus", Ddo_referenciainm_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFReferenciaINM_Codigo = (int)(NumberUtil.Val( Ddo_referenciainm_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFReferenciaINM_Codigo), 6, 0)));
            AV36TFReferenciaINM_Codigo_To = (int)(NumberUtil.Val( Ddo_referenciainm_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFReferenciaINM_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFReferenciaINM_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13E32( )
      {
         /* Ddo_referenciainm_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_referenciainm_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "SortedStatus", Ddo_referenciainm_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "SortedStatus", Ddo_referenciainm_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFReferenciaINM_Descricao = Ddo_referenciainm_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFReferenciaINM_Descricao", AV39TFReferenciaINM_Descricao);
            AV40TFReferenciaINM_Descricao_Sel = Ddo_referenciainm_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFReferenciaINM_Descricao_Sel", AV40TFReferenciaINM_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14E32( )
      {
         /* Ddo_referenciainm_areatrabalhocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_referenciainm_areatrabalhocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_areatrabalhocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhocod_Internalname, "SortedStatus", Ddo_referenciainm_areatrabalhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_areatrabalhocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_areatrabalhocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhocod_Internalname, "SortedStatus", Ddo_referenciainm_areatrabalhocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_areatrabalhocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFReferenciaINM_AreaTrabalhoCod = (int)(NumberUtil.Val( Ddo_referenciainm_areatrabalhocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFReferenciaINM_AreaTrabalhoCod), 6, 0)));
            AV44TFReferenciaINM_AreaTrabalhoCod_To = (int)(NumberUtil.Val( Ddo_referenciainm_areatrabalhocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFReferenciaINM_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFReferenciaINM_AreaTrabalhoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15E32( )
      {
         /* Ddo_referenciainm_areatrabalhodes_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_referenciainm_areatrabalhodes_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_areatrabalhodes_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhodes_Internalname, "SortedStatus", Ddo_referenciainm_areatrabalhodes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_areatrabalhodes_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_areatrabalhodes_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhodes_Internalname, "SortedStatus", Ddo_referenciainm_areatrabalhodes_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_areatrabalhodes_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFReferenciaINM_AreaTrabalhoDes = Ddo_referenciainm_areatrabalhodes_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFReferenciaINM_AreaTrabalhoDes", AV47TFReferenciaINM_AreaTrabalhoDes);
            AV48TFReferenciaINM_AreaTrabalhoDes_Sel = Ddo_referenciainm_areatrabalhodes_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFReferenciaINM_AreaTrabalhoDes_Sel", AV48TFReferenciaINM_AreaTrabalhoDes_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16E32( )
      {
         /* Ddo_referenciainm_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_referenciainm_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_ativo_Internalname, "SortedStatus", Ddo_referenciainm_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_referenciainm_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_ativo_Internalname, "SortedStatus", Ddo_referenciainm_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_referenciainm_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFReferenciaINM_Ativo_Sel = (short)(NumberUtil.Val( Ddo_referenciainm_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFReferenciaINM_Ativo_Sel", StringUtil.Str( (decimal)(AV51TFReferenciaINM_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29E32( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV59Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 69;
         }
         sendrow_692( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_69_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(69, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E30E32 */
         E30E32 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E30E32( )
      {
         /* Enter Routine */
         AV7InOutReferenciaINM_Codigo = A709ReferenciaINM_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutReferenciaINM_Codigo), 6, 0)));
         AV8InOutReferenciaINM_Descricao = A710ReferenciaINM_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutReferenciaINM_Descricao", AV8InOutReferenciaINM_Descricao);
         context.setWebReturnParms(new Object[] {(int)AV7InOutReferenciaINM_Codigo,(String)AV8InOutReferenciaINM_Descricao});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E17E32( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22E32( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18E32( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ReferenciaINM_Descricao1, AV19DynamicFiltersSelector2, AV21ReferenciaINM_Descricao2, AV23DynamicFiltersSelector3, AV25ReferenciaINM_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFReferenciaINM_Codigo, AV36TFReferenciaINM_Codigo_To, AV39TFReferenciaINM_Descricao, AV40TFReferenciaINM_Descricao_Sel, AV43TFReferenciaINM_AreaTrabalhoCod, AV44TFReferenciaINM_AreaTrabalhoCod_To, AV47TFReferenciaINM_AreaTrabalhoDes, AV48TFReferenciaINM_AreaTrabalhoDes_Sel, AV51TFReferenciaINM_Ativo_Sel, AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace, AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace, AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace, AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace, AV33ReferenciaINM_AreaTrabalhoCod, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23E32( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24E32( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19E32( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ReferenciaINM_Descricao1, AV19DynamicFiltersSelector2, AV21ReferenciaINM_Descricao2, AV23DynamicFiltersSelector3, AV25ReferenciaINM_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFReferenciaINM_Codigo, AV36TFReferenciaINM_Codigo_To, AV39TFReferenciaINM_Descricao, AV40TFReferenciaINM_Descricao_Sel, AV43TFReferenciaINM_AreaTrabalhoCod, AV44TFReferenciaINM_AreaTrabalhoCod_To, AV47TFReferenciaINM_AreaTrabalhoDes, AV48TFReferenciaINM_AreaTrabalhoDes_Sel, AV51TFReferenciaINM_Ativo_Sel, AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace, AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace, AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace, AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace, AV33ReferenciaINM_AreaTrabalhoCod, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E25E32( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20E32( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17ReferenciaINM_Descricao1, AV19DynamicFiltersSelector2, AV21ReferenciaINM_Descricao2, AV23DynamicFiltersSelector3, AV25ReferenciaINM_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFReferenciaINM_Codigo, AV36TFReferenciaINM_Codigo_To, AV39TFReferenciaINM_Descricao, AV40TFReferenciaINM_Descricao_Sel, AV43TFReferenciaINM_AreaTrabalhoCod, AV44TFReferenciaINM_AreaTrabalhoCod_To, AV47TFReferenciaINM_AreaTrabalhoDes, AV48TFReferenciaINM_AreaTrabalhoDes_Sel, AV51TFReferenciaINM_Ativo_Sel, AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace, AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace, AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace, AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace, AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace, AV33ReferenciaINM_AreaTrabalhoCod, AV60Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E26E32( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21E32( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_referenciainm_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_codigo_Internalname, "SortedStatus", Ddo_referenciainm_codigo_Sortedstatus);
         Ddo_referenciainm_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "SortedStatus", Ddo_referenciainm_descricao_Sortedstatus);
         Ddo_referenciainm_areatrabalhocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhocod_Internalname, "SortedStatus", Ddo_referenciainm_areatrabalhocod_Sortedstatus);
         Ddo_referenciainm_areatrabalhodes_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhodes_Internalname, "SortedStatus", Ddo_referenciainm_areatrabalhodes_Sortedstatus);
         Ddo_referenciainm_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_ativo_Internalname, "SortedStatus", Ddo_referenciainm_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_referenciainm_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_codigo_Internalname, "SortedStatus", Ddo_referenciainm_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_referenciainm_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "SortedStatus", Ddo_referenciainm_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_referenciainm_areatrabalhocod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhocod_Internalname, "SortedStatus", Ddo_referenciainm_areatrabalhocod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_referenciainm_areatrabalhodes_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhodes_Internalname, "SortedStatus", Ddo_referenciainm_areatrabalhodes_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_referenciainm_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_ativo_Internalname, "SortedStatus", Ddo_referenciainm_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavReferenciainm_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 )
         {
            edtavReferenciainm_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavReferenciainm_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 )
         {
            edtavReferenciainm_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavReferenciainm_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 )
         {
            edtavReferenciainm_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavReferenciainm_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavReferenciainm_descricao3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "REFERENCIAINM_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV21ReferenciaINM_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ReferenciaINM_Descricao2", AV21ReferenciaINM_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "REFERENCIAINM_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV25ReferenciaINM_Descricao3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ReferenciaINM_Descricao3", AV25ReferenciaINM_Descricao3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV33ReferenciaINM_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33ReferenciaINM_AreaTrabalhoCod), 6, 0)));
         AV35TFReferenciaINM_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFReferenciaINM_Codigo), 6, 0)));
         Ddo_referenciainm_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_codigo_Internalname, "FilteredText_set", Ddo_referenciainm_codigo_Filteredtext_set);
         AV36TFReferenciaINM_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFReferenciaINM_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFReferenciaINM_Codigo_To), 6, 0)));
         Ddo_referenciainm_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_codigo_Internalname, "FilteredTextTo_set", Ddo_referenciainm_codigo_Filteredtextto_set);
         AV39TFReferenciaINM_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFReferenciaINM_Descricao", AV39TFReferenciaINM_Descricao);
         Ddo_referenciainm_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "FilteredText_set", Ddo_referenciainm_descricao_Filteredtext_set);
         AV40TFReferenciaINM_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFReferenciaINM_Descricao_Sel", AV40TFReferenciaINM_Descricao_Sel);
         Ddo_referenciainm_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_descricao_Internalname, "SelectedValue_set", Ddo_referenciainm_descricao_Selectedvalue_set);
         AV43TFReferenciaINM_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFReferenciaINM_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFReferenciaINM_AreaTrabalhoCod), 6, 0)));
         Ddo_referenciainm_areatrabalhocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhocod_Internalname, "FilteredText_set", Ddo_referenciainm_areatrabalhocod_Filteredtext_set);
         AV44TFReferenciaINM_AreaTrabalhoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFReferenciaINM_AreaTrabalhoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFReferenciaINM_AreaTrabalhoCod_To), 6, 0)));
         Ddo_referenciainm_areatrabalhocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhocod_Internalname, "FilteredTextTo_set", Ddo_referenciainm_areatrabalhocod_Filteredtextto_set);
         AV47TFReferenciaINM_AreaTrabalhoDes = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFReferenciaINM_AreaTrabalhoDes", AV47TFReferenciaINM_AreaTrabalhoDes);
         Ddo_referenciainm_areatrabalhodes_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhodes_Internalname, "FilteredText_set", Ddo_referenciainm_areatrabalhodes_Filteredtext_set);
         AV48TFReferenciaINM_AreaTrabalhoDes_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48TFReferenciaINM_AreaTrabalhoDes_Sel", AV48TFReferenciaINM_AreaTrabalhoDes_Sel);
         Ddo_referenciainm_areatrabalhodes_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_areatrabalhodes_Internalname, "SelectedValue_set", Ddo_referenciainm_areatrabalhodes_Selectedvalue_set);
         AV51TFReferenciaINM_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFReferenciaINM_Ativo_Sel", StringUtil.Str( (decimal)(AV51TFReferenciaINM_Ativo_Sel), 1, 0));
         Ddo_referenciainm_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_referenciainm_ativo_Internalname, "SelectedValue_set", Ddo_referenciainm_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "REFERENCIAINM_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17ReferenciaINM_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaINM_Descricao1", AV17ReferenciaINM_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 )
            {
               AV17ReferenciaINM_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ReferenciaINM_Descricao1", AV17ReferenciaINM_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 )
               {
                  AV21ReferenciaINM_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ReferenciaINM_Descricao2", AV21ReferenciaINM_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 )
                  {
                     AV25ReferenciaINM_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ReferenciaINM_Descricao3", AV25ReferenciaINM_Descricao3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV33ReferenciaINM_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "REFERENCIAINM_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV33ReferenciaINM_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV35TFReferenciaINM_Codigo) && (0==AV36TFReferenciaINM_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIAINM_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35TFReferenciaINM_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV36TFReferenciaINM_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFReferenciaINM_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIAINM_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFReferenciaINM_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFReferenciaINM_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIAINM_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFReferenciaINM_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV43TFReferenciaINM_AreaTrabalhoCod) && (0==AV44TFReferenciaINM_AreaTrabalhoCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIAINM_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV43TFReferenciaINM_AreaTrabalhoCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV44TFReferenciaINM_AreaTrabalhoCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFReferenciaINM_AreaTrabalhoDes)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIAINM_AREATRABALHODES";
            AV11GridStateFilterValue.gxTpr_Value = AV47TFReferenciaINM_AreaTrabalhoDes;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFReferenciaINM_AreaTrabalhoDes_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIAINM_AREATRABALHODES_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV48TFReferenciaINM_AreaTrabalhoDes_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV51TFReferenciaINM_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREFERENCIAINM_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV51TFReferenciaINM_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV60Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ReferenciaINM_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ReferenciaINM_Descricao1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ReferenciaINM_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21ReferenciaINM_Descricao2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ReferenciaINM_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25ReferenciaINM_Descricao3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_E32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_E32( true) ;
         }
         else
         {
            wb_table2_5_E32( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_E32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_63_E32( true) ;
         }
         else
         {
            wb_table3_63_E32( false) ;
         }
         return  ;
      }

      protected void wb_table3_63_E32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_E32e( true) ;
         }
         else
         {
            wb_table1_2_E32e( false) ;
         }
      }

      protected void wb_table3_63_E32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_66_E32( true) ;
         }
         else
         {
            wb_table4_66_E32( false) ;
         }
         return  ;
      }

      protected void wb_table4_66_E32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_63_E32e( true) ;
         }
         else
         {
            wb_table3_63_E32e( false) ;
         }
      }

      protected void wb_table4_66_E32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"69\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtReferenciaINM_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtReferenciaINM_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtReferenciaINM_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtReferenciaINM_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtReferenciaINM_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtReferenciaINM_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtReferenciaINM_AreaTrabalhoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtReferenciaINM_AreaTrabalhoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtReferenciaINM_AreaTrabalhoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtReferenciaINM_AreaTrabalhoDes_Titleformat == 0 )
               {
                  context.SendWebValue( edtReferenciaINM_AreaTrabalhoDes_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtReferenciaINM_AreaTrabalhoDes_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkReferenciaINM_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkReferenciaINM_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkReferenciaINM_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A709ReferenciaINM_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtReferenciaINM_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaINM_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A710ReferenciaINM_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtReferenciaINM_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaINM_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtReferenciaINM_AreaTrabalhoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaINM_AreaTrabalhoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A713ReferenciaINM_AreaTrabalhoDes);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtReferenciaINM_AreaTrabalhoDes_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtReferenciaINM_AreaTrabalhoDes_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A711ReferenciaINM_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkReferenciaINM_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkReferenciaINM_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 69 )
         {
            wbEnd = 0;
            nRC_GXsfl_69 = (short)(nGXsfl_69_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_66_E32e( true) ;
         }
         else
         {
            wb_table4_66_E32e( false) ;
         }
      }

      protected void wb_table2_5_E32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptReferenciaINM.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_E32( true) ;
         }
         else
         {
            wb_table5_14_E32( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_E32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_E32e( true) ;
         }
         else
         {
            wb_table2_5_E32e( false) ;
         }
      }

      protected void wb_table5_14_E32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextreferenciainm_areatrabalhocod_Internalname, "C�digo", "", "", lblFiltertextreferenciainm_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavReferenciainm_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33ReferenciaINM_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33ReferenciaINM_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavReferenciainm_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_23_E32( true) ;
         }
         else
         {
            wb_table6_23_E32( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_E32e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_E32e( true) ;
         }
         else
         {
            wb_table5_14_E32e( false) ;
         }
      }

      protected void wb_table6_23_E32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_PromptReferenciaINM.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavReferenciainm_descricao1_Internalname, AV17ReferenciaINM_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV17ReferenciaINM_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavReferenciainm_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavReferenciainm_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaINM.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_PromptReferenciaINM.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavReferenciainm_descricao2_Internalname, AV21ReferenciaINM_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV21ReferenciaINM_Descricao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavReferenciainm_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavReferenciainm_descricao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaINM.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_69_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_PromptReferenciaINM.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_69_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavReferenciainm_descricao3_Internalname, AV25ReferenciaINM_Descricao3, StringUtil.RTrim( context.localUtil.Format( AV25ReferenciaINM_Descricao3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavReferenciainm_descricao3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavReferenciainm_descricao3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptReferenciaINM.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_E32e( true) ;
         }
         else
         {
            wb_table6_23_E32e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutReferenciaINM_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutReferenciaINM_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutReferenciaINM_Codigo), 6, 0)));
         AV8InOutReferenciaINM_Descricao = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutReferenciaINM_Descricao", AV8InOutReferenciaINM_Descricao);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAE32( ) ;
         WSE32( ) ;
         WEE32( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311746262");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptreferenciainm.js", "?2020311746262");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_692( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_69_idx;
         edtReferenciaINM_Codigo_Internalname = "REFERENCIAINM_CODIGO_"+sGXsfl_69_idx;
         edtReferenciaINM_Descricao_Internalname = "REFERENCIAINM_DESCRICAO_"+sGXsfl_69_idx;
         edtReferenciaINM_AreaTrabalhoCod_Internalname = "REFERENCIAINM_AREATRABALHOCOD_"+sGXsfl_69_idx;
         edtReferenciaINM_AreaTrabalhoDes_Internalname = "REFERENCIAINM_AREATRABALHODES_"+sGXsfl_69_idx;
         chkReferenciaINM_Ativo_Internalname = "REFERENCIAINM_ATIVO_"+sGXsfl_69_idx;
      }

      protected void SubsflControlProps_fel_692( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_69_fel_idx;
         edtReferenciaINM_Codigo_Internalname = "REFERENCIAINM_CODIGO_"+sGXsfl_69_fel_idx;
         edtReferenciaINM_Descricao_Internalname = "REFERENCIAINM_DESCRICAO_"+sGXsfl_69_fel_idx;
         edtReferenciaINM_AreaTrabalhoCod_Internalname = "REFERENCIAINM_AREATRABALHOCOD_"+sGXsfl_69_fel_idx;
         edtReferenciaINM_AreaTrabalhoDes_Internalname = "REFERENCIAINM_AREATRABALHODES_"+sGXsfl_69_fel_idx;
         chkReferenciaINM_Ativo_Internalname = "REFERENCIAINM_ATIVO_"+sGXsfl_69_fel_idx;
      }

      protected void sendrow_692( )
      {
         SubsflControlProps_692( ) ;
         WBE30( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_69_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_69_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_69_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 70,'',false,'',69)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV59Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV59Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_69_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINM_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A709ReferenciaINM_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaINM_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINM_Descricao_Internalname,(String)A710ReferenciaINM_Descricao,StringUtil.RTrim( context.localUtil.Format( A710ReferenciaINM_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaINM_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINM_AreaTrabalhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaINM_AreaTrabalhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtReferenciaINM_AreaTrabalhoDes_Internalname,(String)A713ReferenciaINM_AreaTrabalhoDes,StringUtil.RTrim( context.localUtil.Format( A713ReferenciaINM_AreaTrabalhoDes, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtReferenciaINM_AreaTrabalhoDes_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)69,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkReferenciaINM_Ativo_Internalname,StringUtil.BoolToStr( A711ReferenciaINM_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIAINM_CODIGO"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, context.localUtil.Format( (decimal)(A709ReferenciaINM_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIAINM_DESCRICAO"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, StringUtil.RTrim( context.localUtil.Format( A710ReferenciaINM_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIAINM_AREATRABALHOCOD"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, context.localUtil.Format( (decimal)(A712ReferenciaINM_AreaTrabalhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_REFERENCIAINM_ATIVO"+"_"+sGXsfl_69_idx, GetSecureSignedToken( sGXsfl_69_idx, A711ReferenciaINM_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_69_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_69_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_69_idx+1));
            sGXsfl_69_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_69_idx), 4, 0)), 4, "0");
            SubsflControlProps_692( ) ;
         }
         /* End function sendrow_692 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextreferenciainm_areatrabalhocod_Internalname = "FILTERTEXTREFERENCIAINM_AREATRABALHOCOD";
         edtavReferenciainm_areatrabalhocod_Internalname = "vREFERENCIAINM_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavReferenciainm_descricao1_Internalname = "vREFERENCIAINM_DESCRICAO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavReferenciainm_descricao2_Internalname = "vREFERENCIAINM_DESCRICAO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavReferenciainm_descricao3_Internalname = "vREFERENCIAINM_DESCRICAO3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtReferenciaINM_Codigo_Internalname = "REFERENCIAINM_CODIGO";
         edtReferenciaINM_Descricao_Internalname = "REFERENCIAINM_DESCRICAO";
         edtReferenciaINM_AreaTrabalhoCod_Internalname = "REFERENCIAINM_AREATRABALHOCOD";
         edtReferenciaINM_AreaTrabalhoDes_Internalname = "REFERENCIAINM_AREATRABALHODES";
         chkReferenciaINM_Ativo_Internalname = "REFERENCIAINM_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfreferenciainm_codigo_Internalname = "vTFREFERENCIAINM_CODIGO";
         edtavTfreferenciainm_codigo_to_Internalname = "vTFREFERENCIAINM_CODIGO_TO";
         edtavTfreferenciainm_descricao_Internalname = "vTFREFERENCIAINM_DESCRICAO";
         edtavTfreferenciainm_descricao_sel_Internalname = "vTFREFERENCIAINM_DESCRICAO_SEL";
         edtavTfreferenciainm_areatrabalhocod_Internalname = "vTFREFERENCIAINM_AREATRABALHOCOD";
         edtavTfreferenciainm_areatrabalhocod_to_Internalname = "vTFREFERENCIAINM_AREATRABALHOCOD_TO";
         edtavTfreferenciainm_areatrabalhodes_Internalname = "vTFREFERENCIAINM_AREATRABALHODES";
         edtavTfreferenciainm_areatrabalhodes_sel_Internalname = "vTFREFERENCIAINM_AREATRABALHODES_SEL";
         edtavTfreferenciainm_ativo_sel_Internalname = "vTFREFERENCIAINM_ATIVO_SEL";
         Ddo_referenciainm_codigo_Internalname = "DDO_REFERENCIAINM_CODIGO";
         edtavDdo_referenciainm_codigotitlecontrolidtoreplace_Internalname = "vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_referenciainm_descricao_Internalname = "DDO_REFERENCIAINM_DESCRICAO";
         edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Internalname = "vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_referenciainm_areatrabalhocod_Internalname = "DDO_REFERENCIAINM_AREATRABALHOCOD";
         edtavDdo_referenciainm_areatrabalhocodtitlecontrolidtoreplace_Internalname = "vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE";
         Ddo_referenciainm_areatrabalhodes_Internalname = "DDO_REFERENCIAINM_AREATRABALHODES";
         edtavDdo_referenciainm_areatrabalhodestitlecontrolidtoreplace_Internalname = "vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE";
         Ddo_referenciainm_ativo_Internalname = "DDO_REFERENCIAINM_ATIVO";
         edtavDdo_referenciainm_ativotitlecontrolidtoreplace_Internalname = "vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtReferenciaINM_AreaTrabalhoDes_Jsonclick = "";
         edtReferenciaINM_AreaTrabalhoCod_Jsonclick = "";
         edtReferenciaINM_Descricao_Jsonclick = "";
         edtReferenciaINM_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavReferenciainm_descricao3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavReferenciainm_descricao2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavReferenciainm_descricao1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavReferenciainm_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         chkReferenciaINM_Ativo_Titleformat = 0;
         edtReferenciaINM_AreaTrabalhoDes_Titleformat = 0;
         edtReferenciaINM_AreaTrabalhoCod_Titleformat = 0;
         edtReferenciaINM_Descricao_Titleformat = 0;
         edtReferenciaINM_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavReferenciainm_descricao3_Visible = 1;
         edtavReferenciainm_descricao2_Visible = 1;
         edtavReferenciainm_descricao1_Visible = 1;
         chkReferenciaINM_Ativo.Title.Text = "Ativo";
         edtReferenciaINM_AreaTrabalhoDes_Title = "de Trabalho";
         edtReferenciaINM_AreaTrabalhoCod_Title = "C�digo";
         edtReferenciaINM_Descricao_Title = "Descri��o";
         edtReferenciaINM_Codigo_Title = "Guia";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkReferenciaINM_Ativo.Caption = "";
         edtavDdo_referenciainm_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_referenciainm_areatrabalhodestitlecontrolidtoreplace_Visible = 1;
         edtavDdo_referenciainm_areatrabalhocodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_referenciainm_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfreferenciainm_ativo_sel_Jsonclick = "";
         edtavTfreferenciainm_ativo_sel_Visible = 1;
         edtavTfreferenciainm_areatrabalhodes_sel_Jsonclick = "";
         edtavTfreferenciainm_areatrabalhodes_sel_Visible = 1;
         edtavTfreferenciainm_areatrabalhodes_Jsonclick = "";
         edtavTfreferenciainm_areatrabalhodes_Visible = 1;
         edtavTfreferenciainm_areatrabalhocod_to_Jsonclick = "";
         edtavTfreferenciainm_areatrabalhocod_to_Visible = 1;
         edtavTfreferenciainm_areatrabalhocod_Jsonclick = "";
         edtavTfreferenciainm_areatrabalhocod_Visible = 1;
         edtavTfreferenciainm_descricao_sel_Jsonclick = "";
         edtavTfreferenciainm_descricao_sel_Visible = 1;
         edtavTfreferenciainm_descricao_Jsonclick = "";
         edtavTfreferenciainm_descricao_Visible = 1;
         edtavTfreferenciainm_codigo_to_Jsonclick = "";
         edtavTfreferenciainm_codigo_to_Visible = 1;
         edtavTfreferenciainm_codigo_Jsonclick = "";
         edtavTfreferenciainm_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_referenciainm_ativo_Searchbuttontext = "Pesquisar";
         Ddo_referenciainm_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_referenciainm_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_referenciainm_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_referenciainm_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_referenciainm_ativo_Datalisttype = "FixedValues";
         Ddo_referenciainm_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_referenciainm_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_referenciainm_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_referenciainm_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_referenciainm_ativo_Titlecontrolidtoreplace = "";
         Ddo_referenciainm_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_referenciainm_ativo_Cls = "ColumnSettings";
         Ddo_referenciainm_ativo_Tooltip = "Op��es";
         Ddo_referenciainm_ativo_Caption = "";
         Ddo_referenciainm_areatrabalhodes_Searchbuttontext = "Pesquisar";
         Ddo_referenciainm_areatrabalhodes_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_referenciainm_areatrabalhodes_Cleanfilter = "Limpar pesquisa";
         Ddo_referenciainm_areatrabalhodes_Loadingdata = "Carregando dados...";
         Ddo_referenciainm_areatrabalhodes_Sortdsc = "Ordenar de Z � A";
         Ddo_referenciainm_areatrabalhodes_Sortasc = "Ordenar de A � Z";
         Ddo_referenciainm_areatrabalhodes_Datalistupdateminimumcharacters = 0;
         Ddo_referenciainm_areatrabalhodes_Datalistproc = "GetPromptReferenciaINMFilterData";
         Ddo_referenciainm_areatrabalhodes_Datalisttype = "Dynamic";
         Ddo_referenciainm_areatrabalhodes_Includedatalist = Convert.ToBoolean( -1);
         Ddo_referenciainm_areatrabalhodes_Filterisrange = Convert.ToBoolean( 0);
         Ddo_referenciainm_areatrabalhodes_Filtertype = "Character";
         Ddo_referenciainm_areatrabalhodes_Includefilter = Convert.ToBoolean( -1);
         Ddo_referenciainm_areatrabalhodes_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_referenciainm_areatrabalhodes_Includesortasc = Convert.ToBoolean( -1);
         Ddo_referenciainm_areatrabalhodes_Titlecontrolidtoreplace = "";
         Ddo_referenciainm_areatrabalhodes_Dropdownoptionstype = "GridTitleSettings";
         Ddo_referenciainm_areatrabalhodes_Cls = "ColumnSettings";
         Ddo_referenciainm_areatrabalhodes_Tooltip = "Op��es";
         Ddo_referenciainm_areatrabalhodes_Caption = "";
         Ddo_referenciainm_areatrabalhocod_Searchbuttontext = "Pesquisar";
         Ddo_referenciainm_areatrabalhocod_Rangefilterto = "At�";
         Ddo_referenciainm_areatrabalhocod_Rangefilterfrom = "Desde";
         Ddo_referenciainm_areatrabalhocod_Cleanfilter = "Limpar pesquisa";
         Ddo_referenciainm_areatrabalhocod_Sortdsc = "Ordenar de Z � A";
         Ddo_referenciainm_areatrabalhocod_Sortasc = "Ordenar de A � Z";
         Ddo_referenciainm_areatrabalhocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_referenciainm_areatrabalhocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_referenciainm_areatrabalhocod_Filtertype = "Numeric";
         Ddo_referenciainm_areatrabalhocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_referenciainm_areatrabalhocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_referenciainm_areatrabalhocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_referenciainm_areatrabalhocod_Titlecontrolidtoreplace = "";
         Ddo_referenciainm_areatrabalhocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_referenciainm_areatrabalhocod_Cls = "ColumnSettings";
         Ddo_referenciainm_areatrabalhocod_Tooltip = "Op��es";
         Ddo_referenciainm_areatrabalhocod_Caption = "";
         Ddo_referenciainm_descricao_Searchbuttontext = "Pesquisar";
         Ddo_referenciainm_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_referenciainm_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_referenciainm_descricao_Loadingdata = "Carregando dados...";
         Ddo_referenciainm_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_referenciainm_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_referenciainm_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_referenciainm_descricao_Datalistproc = "GetPromptReferenciaINMFilterData";
         Ddo_referenciainm_descricao_Datalisttype = "Dynamic";
         Ddo_referenciainm_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_referenciainm_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_referenciainm_descricao_Filtertype = "Character";
         Ddo_referenciainm_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_referenciainm_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_referenciainm_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_referenciainm_descricao_Titlecontrolidtoreplace = "";
         Ddo_referenciainm_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_referenciainm_descricao_Cls = "ColumnSettings";
         Ddo_referenciainm_descricao_Tooltip = "Op��es";
         Ddo_referenciainm_descricao_Caption = "";
         Ddo_referenciainm_codigo_Searchbuttontext = "Pesquisar";
         Ddo_referenciainm_codigo_Rangefilterto = "At�";
         Ddo_referenciainm_codigo_Rangefilterfrom = "Desde";
         Ddo_referenciainm_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_referenciainm_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_referenciainm_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_referenciainm_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_referenciainm_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_referenciainm_codigo_Filtertype = "Numeric";
         Ddo_referenciainm_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_referenciainm_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_referenciainm_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_referenciainm_codigo_Titlecontrolidtoreplace = "";
         Ddo_referenciainm_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_referenciainm_codigo_Cls = "ColumnSettings";
         Ddo_referenciainm_codigo_Tooltip = "Op��es";
         Ddo_referenciainm_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Referencia INM";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''}],oparms:[{av:'AV34ReferenciaINM_CodigoTitleFilterData',fld:'vREFERENCIAINM_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38ReferenciaINM_DescricaoTitleFilterData',fld:'vREFERENCIAINM_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV42ReferenciaINM_AreaTrabalhoCodTitleFilterData',fld:'vREFERENCIAINM_AREATRABALHOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV46ReferenciaINM_AreaTrabalhoDesTitleFilterData',fld:'vREFERENCIAINM_AREATRABALHODESTITLEFILTERDATA',pic:'',nv:null},{av:'AV50ReferenciaINM_AtivoTitleFilterData',fld:'vREFERENCIAINM_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtReferenciaINM_Codigo_Titleformat',ctrl:'REFERENCIAINM_CODIGO',prop:'Titleformat'},{av:'edtReferenciaINM_Codigo_Title',ctrl:'REFERENCIAINM_CODIGO',prop:'Title'},{av:'edtReferenciaINM_Descricao_Titleformat',ctrl:'REFERENCIAINM_DESCRICAO',prop:'Titleformat'},{av:'edtReferenciaINM_Descricao_Title',ctrl:'REFERENCIAINM_DESCRICAO',prop:'Title'},{av:'edtReferenciaINM_AreaTrabalhoCod_Titleformat',ctrl:'REFERENCIAINM_AREATRABALHOCOD',prop:'Titleformat'},{av:'edtReferenciaINM_AreaTrabalhoCod_Title',ctrl:'REFERENCIAINM_AREATRABALHOCOD',prop:'Title'},{av:'edtReferenciaINM_AreaTrabalhoDes_Titleformat',ctrl:'REFERENCIAINM_AREATRABALHODES',prop:'Titleformat'},{av:'edtReferenciaINM_AreaTrabalhoDes_Title',ctrl:'REFERENCIAINM_AREATRABALHODES',prop:'Title'},{av:'chkReferenciaINM_Ativo_Titleformat',ctrl:'REFERENCIAINM_ATIVO',prop:'Titleformat'},{av:'chkReferenciaINM_Ativo.Title.Text',ctrl:'REFERENCIAINM_ATIVO',prop:'Title'},{av:'AV55GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV56GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11E32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_REFERENCIAINM_CODIGO.ONOPTIONCLICKED","{handler:'E12E32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_referenciainm_codigo_Activeeventkey',ctrl:'DDO_REFERENCIAINM_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_referenciainm_codigo_Filteredtext_get',ctrl:'DDO_REFERENCIAINM_CODIGO',prop:'FilteredText_get'},{av:'Ddo_referenciainm_codigo_Filteredtextto_get',ctrl:'DDO_REFERENCIAINM_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_referenciainm_codigo_Sortedstatus',ctrl:'DDO_REFERENCIAINM_CODIGO',prop:'SortedStatus'},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_referenciainm_descricao_Sortedstatus',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_referenciainm_areatrabalhocod_Sortedstatus',ctrl:'DDO_REFERENCIAINM_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_referenciainm_areatrabalhodes_Sortedstatus',ctrl:'DDO_REFERENCIAINM_AREATRABALHODES',prop:'SortedStatus'},{av:'Ddo_referenciainm_ativo_Sortedstatus',ctrl:'DDO_REFERENCIAINM_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REFERENCIAINM_DESCRICAO.ONOPTIONCLICKED","{handler:'E13E32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_referenciainm_descricao_Activeeventkey',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_referenciainm_descricao_Filteredtext_get',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_referenciainm_descricao_Selectedvalue_get',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_referenciainm_descricao_Sortedstatus',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SortedStatus'},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_referenciainm_codigo_Sortedstatus',ctrl:'DDO_REFERENCIAINM_CODIGO',prop:'SortedStatus'},{av:'Ddo_referenciainm_areatrabalhocod_Sortedstatus',ctrl:'DDO_REFERENCIAINM_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_referenciainm_areatrabalhodes_Sortedstatus',ctrl:'DDO_REFERENCIAINM_AREATRABALHODES',prop:'SortedStatus'},{av:'Ddo_referenciainm_ativo_Sortedstatus',ctrl:'DDO_REFERENCIAINM_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REFERENCIAINM_AREATRABALHOCOD.ONOPTIONCLICKED","{handler:'E14E32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_referenciainm_areatrabalhocod_Activeeventkey',ctrl:'DDO_REFERENCIAINM_AREATRABALHOCOD',prop:'ActiveEventKey'},{av:'Ddo_referenciainm_areatrabalhocod_Filteredtext_get',ctrl:'DDO_REFERENCIAINM_AREATRABALHOCOD',prop:'FilteredText_get'},{av:'Ddo_referenciainm_areatrabalhocod_Filteredtextto_get',ctrl:'DDO_REFERENCIAINM_AREATRABALHOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_referenciainm_areatrabalhocod_Sortedstatus',ctrl:'DDO_REFERENCIAINM_AREATRABALHOCOD',prop:'SortedStatus'},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_referenciainm_codigo_Sortedstatus',ctrl:'DDO_REFERENCIAINM_CODIGO',prop:'SortedStatus'},{av:'Ddo_referenciainm_descricao_Sortedstatus',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_referenciainm_areatrabalhodes_Sortedstatus',ctrl:'DDO_REFERENCIAINM_AREATRABALHODES',prop:'SortedStatus'},{av:'Ddo_referenciainm_ativo_Sortedstatus',ctrl:'DDO_REFERENCIAINM_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REFERENCIAINM_AREATRABALHODES.ONOPTIONCLICKED","{handler:'E15E32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_referenciainm_areatrabalhodes_Activeeventkey',ctrl:'DDO_REFERENCIAINM_AREATRABALHODES',prop:'ActiveEventKey'},{av:'Ddo_referenciainm_areatrabalhodes_Filteredtext_get',ctrl:'DDO_REFERENCIAINM_AREATRABALHODES',prop:'FilteredText_get'},{av:'Ddo_referenciainm_areatrabalhodes_Selectedvalue_get',ctrl:'DDO_REFERENCIAINM_AREATRABALHODES',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_referenciainm_areatrabalhodes_Sortedstatus',ctrl:'DDO_REFERENCIAINM_AREATRABALHODES',prop:'SortedStatus'},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'Ddo_referenciainm_codigo_Sortedstatus',ctrl:'DDO_REFERENCIAINM_CODIGO',prop:'SortedStatus'},{av:'Ddo_referenciainm_descricao_Sortedstatus',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_referenciainm_areatrabalhocod_Sortedstatus',ctrl:'DDO_REFERENCIAINM_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_referenciainm_ativo_Sortedstatus',ctrl:'DDO_REFERENCIAINM_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REFERENCIAINM_ATIVO.ONOPTIONCLICKED","{handler:'E16E32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_referenciainm_ativo_Activeeventkey',ctrl:'DDO_REFERENCIAINM_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_referenciainm_ativo_Selectedvalue_get',ctrl:'DDO_REFERENCIAINM_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_referenciainm_ativo_Sortedstatus',ctrl:'DDO_REFERENCIAINM_ATIVO',prop:'SortedStatus'},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_referenciainm_codigo_Sortedstatus',ctrl:'DDO_REFERENCIAINM_CODIGO',prop:'SortedStatus'},{av:'Ddo_referenciainm_descricao_Sortedstatus',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_referenciainm_areatrabalhocod_Sortedstatus',ctrl:'DDO_REFERENCIAINM_AREATRABALHOCOD',prop:'SortedStatus'},{av:'Ddo_referenciainm_areatrabalhodes_Sortedstatus',ctrl:'DDO_REFERENCIAINM_AREATRABALHODES',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29E32',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E30E32',iparms:[{av:'A709ReferenciaINM_Codigo',fld:'REFERENCIAINM_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A710ReferenciaINM_Descricao',fld:'REFERENCIAINM_DESCRICAO',pic:'@!',hsh:true,nv:''}],oparms:[{av:'AV7InOutReferenciaINM_Codigo',fld:'vINOUTREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutReferenciaINM_Descricao',fld:'vINOUTREFERENCIAINM_DESCRICAO',pic:'@!',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17E32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22E32',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18E32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavReferenciainm_descricao2_Visible',ctrl:'vREFERENCIAINM_DESCRICAO2',prop:'Visible'},{av:'edtavReferenciainm_descricao3_Visible',ctrl:'vREFERENCIAINM_DESCRICAO3',prop:'Visible'},{av:'edtavReferenciainm_descricao1_Visible',ctrl:'vREFERENCIAINM_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23E32',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavReferenciainm_descricao1_Visible',ctrl:'vREFERENCIAINM_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24E32',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19E32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavReferenciainm_descricao2_Visible',ctrl:'vREFERENCIAINM_DESCRICAO2',prop:'Visible'},{av:'edtavReferenciainm_descricao3_Visible',ctrl:'vREFERENCIAINM_DESCRICAO3',prop:'Visible'},{av:'edtavReferenciainm_descricao1_Visible',ctrl:'vREFERENCIAINM_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25E32',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavReferenciainm_descricao2_Visible',ctrl:'vREFERENCIAINM_DESCRICAO2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20E32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavReferenciainm_descricao2_Visible',ctrl:'vREFERENCIAINM_DESCRICAO2',prop:'Visible'},{av:'edtavReferenciainm_descricao3_Visible',ctrl:'vREFERENCIAINM_DESCRICAO3',prop:'Visible'},{av:'edtavReferenciainm_descricao1_Visible',ctrl:'vREFERENCIAINM_DESCRICAO1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26E32',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavReferenciainm_descricao3_Visible',ctrl:'vREFERENCIAINM_DESCRICAO3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21E32',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_AREATRABALHODESTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace',fld:'vDDO_REFERENCIAINM_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV60Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV33ReferenciaINM_AreaTrabalhoCod',fld:'vREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV35TFReferenciaINM_Codigo',fld:'vTFREFERENCIAINM_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_referenciainm_codigo_Filteredtext_set',ctrl:'DDO_REFERENCIAINM_CODIGO',prop:'FilteredText_set'},{av:'AV36TFReferenciaINM_Codigo_To',fld:'vTFREFERENCIAINM_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_referenciainm_codigo_Filteredtextto_set',ctrl:'DDO_REFERENCIAINM_CODIGO',prop:'FilteredTextTo_set'},{av:'AV39TFReferenciaINM_Descricao',fld:'vTFREFERENCIAINM_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_referenciainm_descricao_Filteredtext_set',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'FilteredText_set'},{av:'AV40TFReferenciaINM_Descricao_Sel',fld:'vTFREFERENCIAINM_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_referenciainm_descricao_Selectedvalue_set',ctrl:'DDO_REFERENCIAINM_DESCRICAO',prop:'SelectedValue_set'},{av:'AV43TFReferenciaINM_AreaTrabalhoCod',fld:'vTFREFERENCIAINM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_referenciainm_areatrabalhocod_Filteredtext_set',ctrl:'DDO_REFERENCIAINM_AREATRABALHOCOD',prop:'FilteredText_set'},{av:'AV44TFReferenciaINM_AreaTrabalhoCod_To',fld:'vTFREFERENCIAINM_AREATRABALHOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_referenciainm_areatrabalhocod_Filteredtextto_set',ctrl:'DDO_REFERENCIAINM_AREATRABALHOCOD',prop:'FilteredTextTo_set'},{av:'AV47TFReferenciaINM_AreaTrabalhoDes',fld:'vTFREFERENCIAINM_AREATRABALHODES',pic:'@!',nv:''},{av:'Ddo_referenciainm_areatrabalhodes_Filteredtext_set',ctrl:'DDO_REFERENCIAINM_AREATRABALHODES',prop:'FilteredText_set'},{av:'AV48TFReferenciaINM_AreaTrabalhoDes_Sel',fld:'vTFREFERENCIAINM_AREATRABALHODES_SEL',pic:'@!',nv:''},{av:'Ddo_referenciainm_areatrabalhodes_Selectedvalue_set',ctrl:'DDO_REFERENCIAINM_AREATRABALHODES',prop:'SelectedValue_set'},{av:'AV51TFReferenciaINM_Ativo_Sel',fld:'vTFREFERENCIAINM_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_referenciainm_ativo_Selectedvalue_set',ctrl:'DDO_REFERENCIAINM_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17ReferenciaINM_Descricao1',fld:'vREFERENCIAINM_DESCRICAO1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavReferenciainm_descricao1_Visible',ctrl:'vREFERENCIAINM_DESCRICAO1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21ReferenciaINM_Descricao2',fld:'vREFERENCIAINM_DESCRICAO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25ReferenciaINM_Descricao3',fld:'vREFERENCIAINM_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavReferenciainm_descricao2_Visible',ctrl:'vREFERENCIAINM_DESCRICAO2',prop:'Visible'},{av:'edtavReferenciainm_descricao3_Visible',ctrl:'vREFERENCIAINM_DESCRICAO3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutReferenciaINM_Descricao = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_referenciainm_codigo_Activeeventkey = "";
         Ddo_referenciainm_codigo_Filteredtext_get = "";
         Ddo_referenciainm_codigo_Filteredtextto_get = "";
         Ddo_referenciainm_descricao_Activeeventkey = "";
         Ddo_referenciainm_descricao_Filteredtext_get = "";
         Ddo_referenciainm_descricao_Selectedvalue_get = "";
         Ddo_referenciainm_areatrabalhocod_Activeeventkey = "";
         Ddo_referenciainm_areatrabalhocod_Filteredtext_get = "";
         Ddo_referenciainm_areatrabalhocod_Filteredtextto_get = "";
         Ddo_referenciainm_areatrabalhodes_Activeeventkey = "";
         Ddo_referenciainm_areatrabalhodes_Filteredtext_get = "";
         Ddo_referenciainm_areatrabalhodes_Selectedvalue_get = "";
         Ddo_referenciainm_ativo_Activeeventkey = "";
         Ddo_referenciainm_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ReferenciaINM_Descricao1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21ReferenciaINM_Descricao2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25ReferenciaINM_Descricao3 = "";
         AV39TFReferenciaINM_Descricao = "";
         AV40TFReferenciaINM_Descricao_Sel = "";
         AV47TFReferenciaINM_AreaTrabalhoDes = "";
         AV48TFReferenciaINM_AreaTrabalhoDes_Sel = "";
         AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace = "";
         AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace = "";
         AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace = "";
         AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace = "";
         AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace = "";
         AV60Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV53DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV34ReferenciaINM_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38ReferenciaINM_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42ReferenciaINM_AreaTrabalhoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46ReferenciaINM_AreaTrabalhoDesTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50ReferenciaINM_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_referenciainm_codigo_Filteredtext_set = "";
         Ddo_referenciainm_codigo_Filteredtextto_set = "";
         Ddo_referenciainm_codigo_Sortedstatus = "";
         Ddo_referenciainm_descricao_Filteredtext_set = "";
         Ddo_referenciainm_descricao_Selectedvalue_set = "";
         Ddo_referenciainm_descricao_Sortedstatus = "";
         Ddo_referenciainm_areatrabalhocod_Filteredtext_set = "";
         Ddo_referenciainm_areatrabalhocod_Filteredtextto_set = "";
         Ddo_referenciainm_areatrabalhocod_Sortedstatus = "";
         Ddo_referenciainm_areatrabalhodes_Filteredtext_set = "";
         Ddo_referenciainm_areatrabalhodes_Selectedvalue_set = "";
         Ddo_referenciainm_areatrabalhodes_Sortedstatus = "";
         Ddo_referenciainm_ativo_Selectedvalue_set = "";
         Ddo_referenciainm_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV59Select_GXI = "";
         A710ReferenciaINM_Descricao = "";
         A713ReferenciaINM_AreaTrabalhoDes = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         lV17ReferenciaINM_Descricao1 = "";
         lV21ReferenciaINM_Descricao2 = "";
         lV25ReferenciaINM_Descricao3 = "";
         lV39TFReferenciaINM_Descricao = "";
         lV47TFReferenciaINM_AreaTrabalhoDes = "";
         H00E32_A711ReferenciaINM_Ativo = new bool[] {false} ;
         H00E32_A713ReferenciaINM_AreaTrabalhoDes = new String[] {""} ;
         H00E32_n713ReferenciaINM_AreaTrabalhoDes = new bool[] {false} ;
         H00E32_A712ReferenciaINM_AreaTrabalhoCod = new int[1] ;
         H00E32_A710ReferenciaINM_Descricao = new String[] {""} ;
         H00E32_A709ReferenciaINM_Codigo = new int[1] ;
         H00E33_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextreferenciainm_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptreferenciainm__default(),
            new Object[][] {
                new Object[] {
               H00E32_A711ReferenciaINM_Ativo, H00E32_A713ReferenciaINM_AreaTrabalhoDes, H00E32_n713ReferenciaINM_AreaTrabalhoDes, H00E32_A712ReferenciaINM_AreaTrabalhoCod, H00E32_A710ReferenciaINM_Descricao, H00E32_A709ReferenciaINM_Codigo
               }
               , new Object[] {
               H00E33_AGRID_nRecordCount
               }
            }
         );
         AV60Pgmname = "PromptReferenciaINM";
         /* GeneXus formulas. */
         AV60Pgmname = "PromptReferenciaINM";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_69 ;
      private short nGXsfl_69_idx=1 ;
      private short AV13OrderedBy ;
      private short AV51TFReferenciaINM_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_69_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtReferenciaINM_Codigo_Titleformat ;
      private short edtReferenciaINM_Descricao_Titleformat ;
      private short edtReferenciaINM_AreaTrabalhoCod_Titleformat ;
      private short edtReferenciaINM_AreaTrabalhoDes_Titleformat ;
      private short chkReferenciaINM_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutReferenciaINM_Codigo ;
      private int wcpOAV7InOutReferenciaINM_Codigo ;
      private int subGrid_Rows ;
      private int AV35TFReferenciaINM_Codigo ;
      private int AV36TFReferenciaINM_Codigo_To ;
      private int AV43TFReferenciaINM_AreaTrabalhoCod ;
      private int AV44TFReferenciaINM_AreaTrabalhoCod_To ;
      private int AV33ReferenciaINM_AreaTrabalhoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_referenciainm_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_referenciainm_areatrabalhodes_Datalistupdateminimumcharacters ;
      private int edtavTfreferenciainm_codigo_Visible ;
      private int edtavTfreferenciainm_codigo_to_Visible ;
      private int edtavTfreferenciainm_descricao_Visible ;
      private int edtavTfreferenciainm_descricao_sel_Visible ;
      private int edtavTfreferenciainm_areatrabalhocod_Visible ;
      private int edtavTfreferenciainm_areatrabalhocod_to_Visible ;
      private int edtavTfreferenciainm_areatrabalhodes_Visible ;
      private int edtavTfreferenciainm_areatrabalhodes_sel_Visible ;
      private int edtavTfreferenciainm_ativo_sel_Visible ;
      private int edtavDdo_referenciainm_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_referenciainm_areatrabalhocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_referenciainm_areatrabalhodestitlecontrolidtoreplace_Visible ;
      private int edtavDdo_referenciainm_ativotitlecontrolidtoreplace_Visible ;
      private int A709ReferenciaINM_Codigo ;
      private int A712ReferenciaINM_AreaTrabalhoCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int edtavOrdereddsc_Visible ;
      private int AV54PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavReferenciainm_descricao1_Visible ;
      private int edtavReferenciainm_descricao2_Visible ;
      private int edtavReferenciainm_descricao3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV55GridCurrentPage ;
      private long AV56GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_referenciainm_codigo_Activeeventkey ;
      private String Ddo_referenciainm_codigo_Filteredtext_get ;
      private String Ddo_referenciainm_codigo_Filteredtextto_get ;
      private String Ddo_referenciainm_descricao_Activeeventkey ;
      private String Ddo_referenciainm_descricao_Filteredtext_get ;
      private String Ddo_referenciainm_descricao_Selectedvalue_get ;
      private String Ddo_referenciainm_areatrabalhocod_Activeeventkey ;
      private String Ddo_referenciainm_areatrabalhocod_Filteredtext_get ;
      private String Ddo_referenciainm_areatrabalhocod_Filteredtextto_get ;
      private String Ddo_referenciainm_areatrabalhodes_Activeeventkey ;
      private String Ddo_referenciainm_areatrabalhodes_Filteredtext_get ;
      private String Ddo_referenciainm_areatrabalhodes_Selectedvalue_get ;
      private String Ddo_referenciainm_ativo_Activeeventkey ;
      private String Ddo_referenciainm_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_69_idx="0001" ;
      private String AV60Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_referenciainm_codigo_Caption ;
      private String Ddo_referenciainm_codigo_Tooltip ;
      private String Ddo_referenciainm_codigo_Cls ;
      private String Ddo_referenciainm_codigo_Filteredtext_set ;
      private String Ddo_referenciainm_codigo_Filteredtextto_set ;
      private String Ddo_referenciainm_codigo_Dropdownoptionstype ;
      private String Ddo_referenciainm_codigo_Titlecontrolidtoreplace ;
      private String Ddo_referenciainm_codigo_Sortedstatus ;
      private String Ddo_referenciainm_codigo_Filtertype ;
      private String Ddo_referenciainm_codigo_Sortasc ;
      private String Ddo_referenciainm_codigo_Sortdsc ;
      private String Ddo_referenciainm_codigo_Cleanfilter ;
      private String Ddo_referenciainm_codigo_Rangefilterfrom ;
      private String Ddo_referenciainm_codigo_Rangefilterto ;
      private String Ddo_referenciainm_codigo_Searchbuttontext ;
      private String Ddo_referenciainm_descricao_Caption ;
      private String Ddo_referenciainm_descricao_Tooltip ;
      private String Ddo_referenciainm_descricao_Cls ;
      private String Ddo_referenciainm_descricao_Filteredtext_set ;
      private String Ddo_referenciainm_descricao_Selectedvalue_set ;
      private String Ddo_referenciainm_descricao_Dropdownoptionstype ;
      private String Ddo_referenciainm_descricao_Titlecontrolidtoreplace ;
      private String Ddo_referenciainm_descricao_Sortedstatus ;
      private String Ddo_referenciainm_descricao_Filtertype ;
      private String Ddo_referenciainm_descricao_Datalisttype ;
      private String Ddo_referenciainm_descricao_Datalistproc ;
      private String Ddo_referenciainm_descricao_Sortasc ;
      private String Ddo_referenciainm_descricao_Sortdsc ;
      private String Ddo_referenciainm_descricao_Loadingdata ;
      private String Ddo_referenciainm_descricao_Cleanfilter ;
      private String Ddo_referenciainm_descricao_Noresultsfound ;
      private String Ddo_referenciainm_descricao_Searchbuttontext ;
      private String Ddo_referenciainm_areatrabalhocod_Caption ;
      private String Ddo_referenciainm_areatrabalhocod_Tooltip ;
      private String Ddo_referenciainm_areatrabalhocod_Cls ;
      private String Ddo_referenciainm_areatrabalhocod_Filteredtext_set ;
      private String Ddo_referenciainm_areatrabalhocod_Filteredtextto_set ;
      private String Ddo_referenciainm_areatrabalhocod_Dropdownoptionstype ;
      private String Ddo_referenciainm_areatrabalhocod_Titlecontrolidtoreplace ;
      private String Ddo_referenciainm_areatrabalhocod_Sortedstatus ;
      private String Ddo_referenciainm_areatrabalhocod_Filtertype ;
      private String Ddo_referenciainm_areatrabalhocod_Sortasc ;
      private String Ddo_referenciainm_areatrabalhocod_Sortdsc ;
      private String Ddo_referenciainm_areatrabalhocod_Cleanfilter ;
      private String Ddo_referenciainm_areatrabalhocod_Rangefilterfrom ;
      private String Ddo_referenciainm_areatrabalhocod_Rangefilterto ;
      private String Ddo_referenciainm_areatrabalhocod_Searchbuttontext ;
      private String Ddo_referenciainm_areatrabalhodes_Caption ;
      private String Ddo_referenciainm_areatrabalhodes_Tooltip ;
      private String Ddo_referenciainm_areatrabalhodes_Cls ;
      private String Ddo_referenciainm_areatrabalhodes_Filteredtext_set ;
      private String Ddo_referenciainm_areatrabalhodes_Selectedvalue_set ;
      private String Ddo_referenciainm_areatrabalhodes_Dropdownoptionstype ;
      private String Ddo_referenciainm_areatrabalhodes_Titlecontrolidtoreplace ;
      private String Ddo_referenciainm_areatrabalhodes_Sortedstatus ;
      private String Ddo_referenciainm_areatrabalhodes_Filtertype ;
      private String Ddo_referenciainm_areatrabalhodes_Datalisttype ;
      private String Ddo_referenciainm_areatrabalhodes_Datalistproc ;
      private String Ddo_referenciainm_areatrabalhodes_Sortasc ;
      private String Ddo_referenciainm_areatrabalhodes_Sortdsc ;
      private String Ddo_referenciainm_areatrabalhodes_Loadingdata ;
      private String Ddo_referenciainm_areatrabalhodes_Cleanfilter ;
      private String Ddo_referenciainm_areatrabalhodes_Noresultsfound ;
      private String Ddo_referenciainm_areatrabalhodes_Searchbuttontext ;
      private String Ddo_referenciainm_ativo_Caption ;
      private String Ddo_referenciainm_ativo_Tooltip ;
      private String Ddo_referenciainm_ativo_Cls ;
      private String Ddo_referenciainm_ativo_Selectedvalue_set ;
      private String Ddo_referenciainm_ativo_Dropdownoptionstype ;
      private String Ddo_referenciainm_ativo_Titlecontrolidtoreplace ;
      private String Ddo_referenciainm_ativo_Sortedstatus ;
      private String Ddo_referenciainm_ativo_Datalisttype ;
      private String Ddo_referenciainm_ativo_Datalistfixedvalues ;
      private String Ddo_referenciainm_ativo_Sortasc ;
      private String Ddo_referenciainm_ativo_Sortdsc ;
      private String Ddo_referenciainm_ativo_Cleanfilter ;
      private String Ddo_referenciainm_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfreferenciainm_codigo_Internalname ;
      private String edtavTfreferenciainm_codigo_Jsonclick ;
      private String edtavTfreferenciainm_codigo_to_Internalname ;
      private String edtavTfreferenciainm_codigo_to_Jsonclick ;
      private String edtavTfreferenciainm_descricao_Internalname ;
      private String edtavTfreferenciainm_descricao_Jsonclick ;
      private String edtavTfreferenciainm_descricao_sel_Internalname ;
      private String edtavTfreferenciainm_descricao_sel_Jsonclick ;
      private String edtavTfreferenciainm_areatrabalhocod_Internalname ;
      private String edtavTfreferenciainm_areatrabalhocod_Jsonclick ;
      private String edtavTfreferenciainm_areatrabalhocod_to_Internalname ;
      private String edtavTfreferenciainm_areatrabalhocod_to_Jsonclick ;
      private String edtavTfreferenciainm_areatrabalhodes_Internalname ;
      private String edtavTfreferenciainm_areatrabalhodes_Jsonclick ;
      private String edtavTfreferenciainm_areatrabalhodes_sel_Internalname ;
      private String edtavTfreferenciainm_areatrabalhodes_sel_Jsonclick ;
      private String edtavTfreferenciainm_ativo_sel_Internalname ;
      private String edtavTfreferenciainm_ativo_sel_Jsonclick ;
      private String edtavDdo_referenciainm_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_referenciainm_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_referenciainm_areatrabalhocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_referenciainm_areatrabalhodestitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_referenciainm_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtReferenciaINM_Codigo_Internalname ;
      private String edtReferenciaINM_Descricao_Internalname ;
      private String edtReferenciaINM_AreaTrabalhoCod_Internalname ;
      private String edtReferenciaINM_AreaTrabalhoDes_Internalname ;
      private String chkReferenciaINM_Ativo_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavReferenciainm_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavReferenciainm_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavReferenciainm_descricao2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavReferenciainm_descricao3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_referenciainm_codigo_Internalname ;
      private String Ddo_referenciainm_descricao_Internalname ;
      private String Ddo_referenciainm_areatrabalhocod_Internalname ;
      private String Ddo_referenciainm_areatrabalhodes_Internalname ;
      private String Ddo_referenciainm_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtReferenciaINM_Codigo_Title ;
      private String edtReferenciaINM_Descricao_Title ;
      private String edtReferenciaINM_AreaTrabalhoCod_Title ;
      private String edtReferenciaINM_AreaTrabalhoDes_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextreferenciainm_areatrabalhocod_Internalname ;
      private String lblFiltertextreferenciainm_areatrabalhocod_Jsonclick ;
      private String edtavReferenciainm_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavReferenciainm_descricao1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavReferenciainm_descricao2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavReferenciainm_descricao3_Jsonclick ;
      private String sGXsfl_69_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtReferenciaINM_Codigo_Jsonclick ;
      private String edtReferenciaINM_Descricao_Jsonclick ;
      private String edtReferenciaINM_AreaTrabalhoCod_Jsonclick ;
      private String edtReferenciaINM_AreaTrabalhoDes_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_referenciainm_codigo_Includesortasc ;
      private bool Ddo_referenciainm_codigo_Includesortdsc ;
      private bool Ddo_referenciainm_codigo_Includefilter ;
      private bool Ddo_referenciainm_codigo_Filterisrange ;
      private bool Ddo_referenciainm_codigo_Includedatalist ;
      private bool Ddo_referenciainm_descricao_Includesortasc ;
      private bool Ddo_referenciainm_descricao_Includesortdsc ;
      private bool Ddo_referenciainm_descricao_Includefilter ;
      private bool Ddo_referenciainm_descricao_Filterisrange ;
      private bool Ddo_referenciainm_descricao_Includedatalist ;
      private bool Ddo_referenciainm_areatrabalhocod_Includesortasc ;
      private bool Ddo_referenciainm_areatrabalhocod_Includesortdsc ;
      private bool Ddo_referenciainm_areatrabalhocod_Includefilter ;
      private bool Ddo_referenciainm_areatrabalhocod_Filterisrange ;
      private bool Ddo_referenciainm_areatrabalhocod_Includedatalist ;
      private bool Ddo_referenciainm_areatrabalhodes_Includesortasc ;
      private bool Ddo_referenciainm_areatrabalhodes_Includesortdsc ;
      private bool Ddo_referenciainm_areatrabalhodes_Includefilter ;
      private bool Ddo_referenciainm_areatrabalhodes_Filterisrange ;
      private bool Ddo_referenciainm_areatrabalhodes_Includedatalist ;
      private bool Ddo_referenciainm_ativo_Includesortasc ;
      private bool Ddo_referenciainm_ativo_Includesortdsc ;
      private bool Ddo_referenciainm_ativo_Includefilter ;
      private bool Ddo_referenciainm_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n713ReferenciaINM_AreaTrabalhoDes ;
      private bool A711ReferenciaINM_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV8InOutReferenciaINM_Descricao ;
      private String wcpOAV8InOutReferenciaINM_Descricao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17ReferenciaINM_Descricao1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21ReferenciaINM_Descricao2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25ReferenciaINM_Descricao3 ;
      private String AV39TFReferenciaINM_Descricao ;
      private String AV40TFReferenciaINM_Descricao_Sel ;
      private String AV47TFReferenciaINM_AreaTrabalhoDes ;
      private String AV48TFReferenciaINM_AreaTrabalhoDes_Sel ;
      private String AV37ddo_ReferenciaINM_CodigoTitleControlIdToReplace ;
      private String AV41ddo_ReferenciaINM_DescricaoTitleControlIdToReplace ;
      private String AV45ddo_ReferenciaINM_AreaTrabalhoCodTitleControlIdToReplace ;
      private String AV49ddo_ReferenciaINM_AreaTrabalhoDesTitleControlIdToReplace ;
      private String AV52ddo_ReferenciaINM_AtivoTitleControlIdToReplace ;
      private String AV59Select_GXI ;
      private String A710ReferenciaINM_Descricao ;
      private String A713ReferenciaINM_AreaTrabalhoDes ;
      private String lV17ReferenciaINM_Descricao1 ;
      private String lV21ReferenciaINM_Descricao2 ;
      private String lV25ReferenciaINM_Descricao3 ;
      private String lV39TFReferenciaINM_Descricao ;
      private String lV47TFReferenciaINM_AreaTrabalhoDes ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutReferenciaINM_Codigo ;
      private String aP1_InOutReferenciaINM_Descricao ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkReferenciaINM_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private bool[] H00E32_A711ReferenciaINM_Ativo ;
      private String[] H00E32_A713ReferenciaINM_AreaTrabalhoDes ;
      private bool[] H00E32_n713ReferenciaINM_AreaTrabalhoDes ;
      private int[] H00E32_A712ReferenciaINM_AreaTrabalhoCod ;
      private String[] H00E32_A710ReferenciaINM_Descricao ;
      private int[] H00E32_A709ReferenciaINM_Codigo ;
      private long[] H00E33_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34ReferenciaINM_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38ReferenciaINM_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42ReferenciaINM_AreaTrabalhoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46ReferenciaINM_AreaTrabalhoDesTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50ReferenciaINM_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV53DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptreferenciainm__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00E32( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV17ReferenciaINM_Descricao1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             String AV21ReferenciaINM_Descricao2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             String AV25ReferenciaINM_Descricao3 ,
                                             int AV35TFReferenciaINM_Codigo ,
                                             int AV36TFReferenciaINM_Codigo_To ,
                                             String AV40TFReferenciaINM_Descricao_Sel ,
                                             String AV39TFReferenciaINM_Descricao ,
                                             int AV43TFReferenciaINM_AreaTrabalhoCod ,
                                             int AV44TFReferenciaINM_AreaTrabalhoCod_To ,
                                             String AV48TFReferenciaINM_AreaTrabalhoDes_Sel ,
                                             String AV47TFReferenciaINM_AreaTrabalhoDes ,
                                             short AV51TFReferenciaINM_Ativo_Sel ,
                                             String A710ReferenciaINM_Descricao ,
                                             int A709ReferenciaINM_Codigo ,
                                             int A712ReferenciaINM_AreaTrabalhoCod ,
                                             String A713ReferenciaINM_AreaTrabalhoDes ,
                                             bool A711ReferenciaINM_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [17] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ReferenciaINM_Ativo], T2.[AreaTrabalho_Descricao] AS ReferenciaINM_AreaTrabalhoDes, T1.[ReferenciaINM_AreaTrabalhoCod] AS ReferenciaINM_AreaTrabalhoCod, T1.[ReferenciaINM_Descricao], T1.[ReferenciaINM_Codigo]";
         sFromString = " FROM ([ReferenciaINM] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ReferenciaINM_AreaTrabalhoCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ReferenciaINM_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ReferenciaINM_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV17ReferenciaINM_Descricao1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ReferenciaINM_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV21ReferenciaINM_Descricao2 + '%')";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ReferenciaINM_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV25ReferenciaINM_Descricao3 + '%')";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (0==AV35TFReferenciaINM_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] >= @AV35TFReferenciaINM_Codigo)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (0==AV36TFReferenciaINM_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] <= @AV36TFReferenciaINM_Codigo_To)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFReferenciaINM_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFReferenciaINM_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like @lV39TFReferenciaINM_Descricao)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFReferenciaINM_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] = @AV40TFReferenciaINM_Descricao_Sel)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV43TFReferenciaINM_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_AreaTrabalhoCod] >= @AV43TFReferenciaINM_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV44TFReferenciaINM_AreaTrabalhoCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_AreaTrabalhoCod] <= @AV44TFReferenciaINM_AreaTrabalhoCod_To)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48TFReferenciaINM_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFReferenciaINM_AreaTrabalhoDes)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV47TFReferenciaINM_AreaTrabalhoDes)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFReferenciaINM_AreaTrabalhoDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] = @AV48TFReferenciaINM_AreaTrabalhoDes_Sel)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV51TFReferenciaINM_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Ativo] = 1)";
         }
         if ( AV51TFReferenciaINM_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Ativo] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaINM_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaINM_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaINM_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaINM_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaINM_AreaTrabalhoCod]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaINM_AreaTrabalhoCod] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[AreaTrabalho_Descricao]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[AreaTrabalho_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaINM_Ativo]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaINM_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ReferenciaINM_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00E33( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             String AV17ReferenciaINM_Descricao1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             String AV21ReferenciaINM_Descricao2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             String AV25ReferenciaINM_Descricao3 ,
                                             int AV35TFReferenciaINM_Codigo ,
                                             int AV36TFReferenciaINM_Codigo_To ,
                                             String AV40TFReferenciaINM_Descricao_Sel ,
                                             String AV39TFReferenciaINM_Descricao ,
                                             int AV43TFReferenciaINM_AreaTrabalhoCod ,
                                             int AV44TFReferenciaINM_AreaTrabalhoCod_To ,
                                             String AV48TFReferenciaINM_AreaTrabalhoDes_Sel ,
                                             String AV47TFReferenciaINM_AreaTrabalhoDes ,
                                             short AV51TFReferenciaINM_Ativo_Sel ,
                                             String A710ReferenciaINM_Descricao ,
                                             int A709ReferenciaINM_Codigo ,
                                             int A712ReferenciaINM_AreaTrabalhoCod ,
                                             String A713ReferenciaINM_AreaTrabalhoDes ,
                                             bool A711ReferenciaINM_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [12] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ReferenciaINM] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ReferenciaINM_AreaTrabalhoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ReferenciaINM_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ReferenciaINM_Descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV17ReferenciaINM_Descricao1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21ReferenciaINM_Descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV21ReferenciaINM_Descricao2 + '%')";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REFERENCIAINM_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25ReferenciaINM_Descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like '%' + @lV25ReferenciaINM_Descricao3 + '%')";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (0==AV35TFReferenciaINM_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] >= @AV35TFReferenciaINM_Codigo)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (0==AV36TFReferenciaINM_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Codigo] <= @AV36TFReferenciaINM_Codigo_To)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFReferenciaINM_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFReferenciaINM_Descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] like @lV39TFReferenciaINM_Descricao)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFReferenciaINM_Descricao_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Descricao] = @AV40TFReferenciaINM_Descricao_Sel)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV43TFReferenciaINM_AreaTrabalhoCod) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_AreaTrabalhoCod] >= @AV43TFReferenciaINM_AreaTrabalhoCod)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV44TFReferenciaINM_AreaTrabalhoCod_To) )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_AreaTrabalhoCod] <= @AV44TFReferenciaINM_AreaTrabalhoCod_To)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48TFReferenciaINM_AreaTrabalhoDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFReferenciaINM_AreaTrabalhoDes)) ) )
         {
            sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV47TFReferenciaINM_AreaTrabalhoDes)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFReferenciaINM_AreaTrabalhoDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] = @AV48TFReferenciaINM_AreaTrabalhoDes_Sel)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV51TFReferenciaINM_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Ativo] = 1)";
         }
         if ( AV51TFReferenciaINM_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[ReferenciaINM_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00E32(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] , (int)dynConstraints[24] );
               case 1 :
                     return conditional_H00E33(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (int)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] , (int)dynConstraints[24] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00E32 ;
          prmH00E32 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV21ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV25ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV35TFReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFReferenciaINM_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV40TFReferenciaINM_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV43TFReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFReferenciaINM_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV47TFReferenciaINM_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV48TFReferenciaINM_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00E33 ;
          prmH00E33 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV17ReferenciaINM_Descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV21ReferenciaINM_Descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV25ReferenciaINM_Descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV35TFReferenciaINM_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFReferenciaINM_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39TFReferenciaINM_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV40TFReferenciaINM_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV43TFReferenciaINM_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFReferenciaINM_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV47TFReferenciaINM_AreaTrabalhoDes",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV48TFReferenciaINM_AreaTrabalhoDes_Sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00E32", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00E32,11,0,true,false )
             ,new CursorDef("H00E33", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00E33,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

}
