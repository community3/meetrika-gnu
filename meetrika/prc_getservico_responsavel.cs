/*
               File: PRC_GetServico_Responsavel
        Description: Get Servico_Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:40.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getservico_responsavel : GXProcedure
   {
      public prc_getservico_responsavel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getservico_responsavel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ServicoResponsavel_SrvCod ,
                           out int aP1_Servico_Responsavel )
      {
         this.A1549ServicoResponsavel_SrvCod = aP0_ServicoResponsavel_SrvCod;
         this.AV8Servico_Responsavel = 0 ;
         initialize();
         executePrivate();
         aP0_ServicoResponsavel_SrvCod=this.A1549ServicoResponsavel_SrvCod;
         aP1_Servico_Responsavel=this.AV8Servico_Responsavel;
      }

      public int executeUdp( ref int aP0_ServicoResponsavel_SrvCod )
      {
         this.A1549ServicoResponsavel_SrvCod = aP0_ServicoResponsavel_SrvCod;
         this.AV8Servico_Responsavel = 0 ;
         initialize();
         executePrivate();
         aP0_ServicoResponsavel_SrvCod=this.A1549ServicoResponsavel_SrvCod;
         aP1_Servico_Responsavel=this.AV8Servico_Responsavel;
         return AV8Servico_Responsavel ;
      }

      public void executeSubmit( ref int aP0_ServicoResponsavel_SrvCod ,
                                 out int aP1_Servico_Responsavel )
      {
         prc_getservico_responsavel objprc_getservico_responsavel;
         objprc_getservico_responsavel = new prc_getservico_responsavel();
         objprc_getservico_responsavel.A1549ServicoResponsavel_SrvCod = aP0_ServicoResponsavel_SrvCod;
         objprc_getservico_responsavel.AV8Servico_Responsavel = 0 ;
         objprc_getservico_responsavel.context.SetSubmitInitialConfig(context);
         objprc_getservico_responsavel.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getservico_responsavel);
         aP0_ServicoResponsavel_SrvCod=this.A1549ServicoResponsavel_SrvCod;
         aP1_Servico_Responsavel=this.AV8Servico_Responsavel;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getservico_responsavel)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Using cursor P00BK3 */
         pr_default.execute(0, new Object[] {n1549ServicoResponsavel_SrvCod, A1549ServicoResponsavel_SrvCod, AV9WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1548ServicoResponsavel_Codigo = P00BK3_A1548ServicoResponsavel_Codigo[0];
            A1550ServicoResponsavel_CteUsrCod = P00BK3_A1550ServicoResponsavel_CteUsrCod[0];
            n1550ServicoResponsavel_CteUsrCod = P00BK3_n1550ServicoResponsavel_CteUsrCod[0];
            A1643ServicoResponsavel_CteAreaCod = P00BK3_A1643ServicoResponsavel_CteAreaCod[0];
            n1643ServicoResponsavel_CteAreaCod = P00BK3_n1643ServicoResponsavel_CteAreaCod[0];
            A1643ServicoResponsavel_CteAreaCod = P00BK3_A1643ServicoResponsavel_CteAreaCod[0];
            n1643ServicoResponsavel_CteAreaCod = P00BK3_n1643ServicoResponsavel_CteAreaCod[0];
            AV8Servico_Responsavel = A1550ServicoResponsavel_CteUsrCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         P00BK3_A1548ServicoResponsavel_Codigo = new int[1] ;
         P00BK3_A1549ServicoResponsavel_SrvCod = new int[1] ;
         P00BK3_n1549ServicoResponsavel_SrvCod = new bool[] {false} ;
         P00BK3_A1550ServicoResponsavel_CteUsrCod = new int[1] ;
         P00BK3_n1550ServicoResponsavel_CteUsrCod = new bool[] {false} ;
         P00BK3_A1643ServicoResponsavel_CteAreaCod = new int[1] ;
         P00BK3_n1643ServicoResponsavel_CteAreaCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getservico_responsavel__default(),
            new Object[][] {
                new Object[] {
               P00BK3_A1548ServicoResponsavel_Codigo, P00BK3_A1549ServicoResponsavel_SrvCod, P00BK3_n1549ServicoResponsavel_SrvCod, P00BK3_A1550ServicoResponsavel_CteUsrCod, P00BK3_n1550ServicoResponsavel_CteUsrCod, P00BK3_A1643ServicoResponsavel_CteAreaCod, P00BK3_n1643ServicoResponsavel_CteAreaCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1549ServicoResponsavel_SrvCod ;
      private int AV8Servico_Responsavel ;
      private int A1548ServicoResponsavel_Codigo ;
      private int A1550ServicoResponsavel_CteUsrCod ;
      private int A1643ServicoResponsavel_CteAreaCod ;
      private String scmdbuf ;
      private bool n1549ServicoResponsavel_SrvCod ;
      private bool n1550ServicoResponsavel_CteUsrCod ;
      private bool n1643ServicoResponsavel_CteAreaCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ServicoResponsavel_SrvCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00BK3_A1548ServicoResponsavel_Codigo ;
      private int[] P00BK3_A1549ServicoResponsavel_SrvCod ;
      private bool[] P00BK3_n1549ServicoResponsavel_SrvCod ;
      private int[] P00BK3_A1550ServicoResponsavel_CteUsrCod ;
      private bool[] P00BK3_n1550ServicoResponsavel_CteUsrCod ;
      private int[] P00BK3_A1643ServicoResponsavel_CteAreaCod ;
      private bool[] P00BK3_n1643ServicoResponsavel_CteAreaCod ;
      private int aP1_Servico_Responsavel ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class prc_getservico_responsavel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BK3 ;
          prmP00BK3 = new Object[] {
          new Object[] {"@ServicoResponsavel_SrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BK3", "SELECT TOP 1 T1.[ServicoResponsavel_Codigo], T1.[ServicoResponsavel_SrvCod], T1.[ServicoResponsavel_CteUsrCod], COALESCE( T2.[ServicoResponsavel_CteAreaCod], 0) AS ServicoResponsavel_CteAreaCod FROM ([ServicoResponsavel] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T3.[AreaTrabalho_Codigo]) AS ServicoResponsavel_CteAreaCod, T4.[ServicoResponsavel_Codigo] FROM [AreaTrabalho] T3 WITH (NOLOCK),  [ServicoResponsavel] T4 WITH (NOLOCK) WHERE T3.[Contratante_Codigo] = T4.[ServicoResponsavel_CteCteCod] GROUP BY T4.[ServicoResponsavel_Codigo] ) T2 ON T2.[ServicoResponsavel_Codigo] = T1.[ServicoResponsavel_Codigo]) WHERE (T1.[ServicoResponsavel_SrvCod] = @ServicoResponsavel_SrvCod) AND (COALESCE( T2.[ServicoResponsavel_CteAreaCod], 0) = @AV9WWPCo_1Areatrabalho_codigo) ORDER BY T1.[ServicoResponsavel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00BK3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
