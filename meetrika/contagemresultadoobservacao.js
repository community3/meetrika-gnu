/**@preserve  GeneXus C# 10_3_14-114418 on 3/24/2020 22:57:13.46
*/
gx.evt.autoSkip = false;
gx.define('contagemresultadoobservacao', true, function (CmpContext) {
   this.ServerClass =  "contagemresultadoobservacao" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.A456ContagemResultado_Codigo=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CODIGO",'.') ;
   };
   this.e13ja2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e14ja2_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,8,13];
   this.GXLastCtrlId =13;
   this.CONTAGEMRESULTADO_OBSERVACAOContainer = gx.uc.getNew(this, 10, 0, "CKEditorControl", this.CmpContext + "CONTAGEMRESULTADO_OBSERVACAOContainer", "Contagemresultado_observacao");
   var CONTAGEMRESULTADO_OBSERVACAOContainer = this.CONTAGEMRESULTADO_OBSERVACAOContainer;
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("Width", "Width", "800", "str");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("Height", "Height", "4row", "str");
   CONTAGEMRESULTADO_OBSERVACAOContainer.addV2CFunction('A514ContagemResultado_Observacao', "CONTAGEMRESULTADO_OBSERVACAO", 'SetAttribute');
   CONTAGEMRESULTADO_OBSERVACAOContainer.AttributeReadonly = false;
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("Skin", "Skin", "silver", "str");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("Toolbar", "Toolbar", "Default", "str");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("CustomToolbar", "Customtoolbar", "", "str");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("CustomConfiguration", "Customconfiguration", "", "str");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("ToolbarCanCollapse", "Toolbarcancollapse", true, "bool");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("ToolbarExpanded", "Toolbarexpanded", true, "bool");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("Color", "Color", gx.color.fromRGB(211,211,211), "color");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("ButtonPressedId", "Buttonpressedid", "", "char");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("SdtItemObject", "Prop_sdt_item_object", "", "char");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("Dimensions", "Attnumdim", "", "char");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("BaseAttType", "Baseatttype", '', "int");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("PROP_EXT_BASE_ATT_COLLECTION", "Prop_ext_base_att_collection", false, "boolean");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("FieldSpecifier", "Fieldspecifier", "", "char");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("CaptionValue", "Captionvalue", "", "str");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("CaptionClass", "Captionclass", "", "str");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("CaptionPosition", "Captionposition", "", "str");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("InternalTitle", "Coltitle", "", "char");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("TitleFont", "Coltitlefont", "", "char");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("TitleForeColor", "Coltitlecolor", '', "int");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("UserControlIsColumn", "Usercontroliscolumn", false, "boolean");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("Visible", "Visible", true, "bool");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setDynProp("Enabled", "Enabled", true, "boolean");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setProp("Class", "Class", "", "char");
   CONTAGEMRESULTADO_OBSERVACAOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(CONTAGEMRESULTADO_OBSERVACAOContainer);
   GXValidFnc[2]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[5]={fld:"UNNAMEDTABLE2",grid:0};
   GXValidFnc[8]={fld:"TEXTBLOCKCONTAGEMRESULTADO_OBSERVACAO", format:0,grid:0};
   GXValidFnc[13]={fld:"UNNAMEDTABLE3",grid:0};
   this.A514ContagemResultado_Observacao = "" ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.Events = {"e13ja2_client": ["ENTER", true] ,"e14ja2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["LOAD"] = [[],[]];
   this.setVCMap("A456ContagemResultado_Codigo", "CONTAGEMRESULTADO_CODIGO", 0, "int");
   this.InitStandaloneVars( );
   this.setComponent({id: "WCCONTAGEMRESULTADONOTASWC" ,GXClass: null , Prefix: "W0016" , lvl: 1 });
});
