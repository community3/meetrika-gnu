/*
               File: PromptFuncaoAPF
        Description: Selecione Fun��es APF - An�lise de Ponto de Fun��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:33:38.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptfuncaoapf : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptfuncaoapf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptfuncaoapf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutFuncaoAPF_Codigo ,
                           ref String aP1_InOutFuncaoAPF_Nome )
      {
         this.AV7InOutFuncaoAPF_Codigo = aP0_InOutFuncaoAPF_Codigo;
         this.AV8InOutFuncaoAPF_Nome = aP1_InOutFuncaoAPF_Nome;
         executePrivate();
         aP0_InOutFuncaoAPF_Codigo=this.AV7InOutFuncaoAPF_Codigo;
         aP1_InOutFuncaoAPF_Nome=this.AV8InOutFuncaoAPF_Nome;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavFuncaoapf_tipo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavFuncaoapf_tipo2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbavFuncaoapf_tipo3 = new GXCombobox();
         cmbFuncaoAPF_Tipo = new GXCombobox();
         cmbFuncaoAPF_Complexidade = new GXCombobox();
         cmbFuncaoAPF_Ativo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_92 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_92_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_92_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17FuncaoAPF_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPF_Nome1", AV17FuncaoAPF_Nome1);
               AV60FuncaoAPF_SistemaDes1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60FuncaoAPF_SistemaDes1", AV60FuncaoAPF_SistemaDes1);
               AV61FuncaoAPF_ModuloNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61FuncaoAPF_ModuloNom1", AV61FuncaoAPF_ModuloNom1);
               AV62FuncaoAPF_FunAPFPaiNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62FuncaoAPF_FunAPFPaiNom1", AV62FuncaoAPF_FunAPFPaiNom1);
               AV31FuncaoAPF_Tipo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31FuncaoAPF_Tipo1", AV31FuncaoAPF_Tipo1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21FuncaoAPF_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPF_Nome2", AV21FuncaoAPF_Nome2);
               AV63FuncaoAPF_SistemaDes2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63FuncaoAPF_SistemaDes2", AV63FuncaoAPF_SistemaDes2);
               AV64FuncaoAPF_ModuloNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64FuncaoAPF_ModuloNom2", AV64FuncaoAPF_ModuloNom2);
               AV65FuncaoAPF_FunAPFPaiNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65FuncaoAPF_FunAPFPaiNom2", AV65FuncaoAPF_FunAPFPaiNom2);
               AV34FuncaoAPF_Tipo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34FuncaoAPF_Tipo2", AV34FuncaoAPF_Tipo2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25FuncaoAPF_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPF_Nome3", AV25FuncaoAPF_Nome3);
               AV66FuncaoAPF_SistemaDes3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66FuncaoAPF_SistemaDes3", AV66FuncaoAPF_SistemaDes3);
               AV67FuncaoAPF_ModuloNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67FuncaoAPF_ModuloNom3", AV67FuncaoAPF_ModuloNom3);
               AV68FuncaoAPF_FunAPFPaiNom3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68FuncaoAPF_FunAPFPaiNom3", AV68FuncaoAPF_FunAPFPaiNom3);
               AV37FuncaoAPF_Tipo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FuncaoAPF_Tipo3", AV37FuncaoAPF_Tipo3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV70TFFuncaoAPF_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFFuncaoAPF_Nome", AV70TFFuncaoAPF_Nome);
               AV71TFFuncaoAPF_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFFuncaoAPF_Nome_Sel", AV71TFFuncaoAPF_Nome_Sel);
               AV82TFFuncaoAPF_TD = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFFuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82TFFuncaoAPF_TD), 4, 0)));
               AV83TFFuncaoAPF_TD_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFFuncaoAPF_TD_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFFuncaoAPF_TD_To), 4, 0)));
               AV86TFFuncaoAPF_AR = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFFuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86TFFuncaoAPF_AR), 4, 0)));
               AV87TFFuncaoAPF_AR_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFFuncaoAPF_AR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87TFFuncaoAPF_AR_To), 4, 0)));
               AV90TFFuncaoAPF_PF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV90TFFuncaoAPF_PF, 14, 5)));
               AV91TFFuncaoAPF_PF_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV91TFFuncaoAPF_PF_To, 14, 5)));
               AV94TFFuncaoAPF_FunAPFPaiNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94TFFuncaoAPF_FunAPFPaiNom", AV94TFFuncaoAPF_FunAPFPaiNom);
               AV95TFFuncaoAPF_FunAPFPaiNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95TFFuncaoAPF_FunAPFPaiNom_Sel", AV95TFFuncaoAPF_FunAPFPaiNom_Sel);
               AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace);
               AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace", AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace);
               AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace", AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace);
               AV84ddo_FuncaoAPF_TDTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_FuncaoAPF_TDTitleControlIdToReplace", AV84ddo_FuncaoAPF_TDTitleControlIdToReplace);
               AV88ddo_FuncaoAPF_ARTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_FuncaoAPF_ARTitleControlIdToReplace", AV88ddo_FuncaoAPF_ARTitleControlIdToReplace);
               AV92ddo_FuncaoAPF_PFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_FuncaoAPF_PFTitleControlIdToReplace", AV92ddo_FuncaoAPF_PFTitleControlIdToReplace);
               AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace", AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace);
               AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace", AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV75TFFuncaoAPF_Tipo_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV79TFFuncaoAPF_Complexidade_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV99TFFuncaoAPF_Ativo_Sels);
               AV108Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPF_Nome1, AV60FuncaoAPF_SistemaDes1, AV61FuncaoAPF_ModuloNom1, AV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPF_Nome2, AV63FuncaoAPF_SistemaDes2, AV64FuncaoAPF_ModuloNom2, AV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPF_Nome3, AV66FuncaoAPF_SistemaDes3, AV67FuncaoAPF_ModuloNom3, AV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, AV82TFFuncaoAPF_TD, AV83TFFuncaoAPF_TD_To, AV86TFFuncaoAPF_AR, AV87TFFuncaoAPF_AR_To, AV90TFFuncaoAPF_PF, AV91TFFuncaoAPF_PF_To, AV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV75TFFuncaoAPF_Tipo_Sels, AV79TFFuncaoAPF_Complexidade_Sels, AV99TFFuncaoAPF_Ativo_Sels, AV108Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutFuncaoAPF_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoAPF_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutFuncaoAPF_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoAPF_Nome", AV8InOutFuncaoAPF_Nome);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA402( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV108Pgmname = "PromptFuncaoAPF";
               context.Gx_err = 0;
               WS402( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE402( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117333863");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptfuncaoapf.aspx") + "?" + UrlEncode("" +AV7InOutFuncaoAPF_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutFuncaoAPF_Nome))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME1", AV17FuncaoAPF_Nome1);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_SISTEMADES1", AV60FuncaoAPF_SistemaDes1);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_MODULONOM1", StringUtil.RTrim( AV61FuncaoAPF_ModuloNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_FUNAPFPAINOM1", AV62FuncaoAPF_FunAPFPaiNom1);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_TIPO1", StringUtil.RTrim( AV31FuncaoAPF_Tipo1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME2", AV21FuncaoAPF_Nome2);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_SISTEMADES2", AV63FuncaoAPF_SistemaDes2);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_MODULONOM2", StringUtil.RTrim( AV64FuncaoAPF_ModuloNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_FUNAPFPAINOM2", AV65FuncaoAPF_FunAPFPaiNom2);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_TIPO2", StringUtil.RTrim( AV34FuncaoAPF_Tipo2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_NOME3", AV25FuncaoAPF_Nome3);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_SISTEMADES3", AV66FuncaoAPF_SistemaDes3);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_MODULONOM3", StringUtil.RTrim( AV67FuncaoAPF_ModuloNom3));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_FUNAPFPAINOM3", AV68FuncaoAPF_FunAPFPaiNom3);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAOAPF_TIPO3", StringUtil.RTrim( AV37FuncaoAPF_Tipo3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_NOME", AV70TFFuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_NOME_SEL", AV71TFFuncaoAPF_Nome_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_TD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV82TFFuncaoAPF_TD), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_TD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83TFFuncaoAPF_TD_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_AR", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86TFFuncaoAPF_AR), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_AR_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV87TFFuncaoAPF_AR_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_PF", StringUtil.LTrim( StringUtil.NToC( AV90TFFuncaoAPF_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_PF_TO", StringUtil.LTrim( StringUtil.NToC( AV91TFFuncaoAPF_PF_To, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_FUNAPFPAINOM", AV94TFFuncaoAPF_FunAPFPaiNom);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAOAPF_FUNAPFPAINOM_SEL", AV95TFFuncaoAPF_FunAPFPaiNom_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_92", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_92), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV103GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV104GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV101DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV101DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_NOMETITLEFILTERDATA", AV69FuncaoAPF_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_NOMETITLEFILTERDATA", AV69FuncaoAPF_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_TIPOTITLEFILTERDATA", AV73FuncaoAPF_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_TIPOTITLEFILTERDATA", AV73FuncaoAPF_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA", AV77FuncaoAPF_ComplexidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA", AV77FuncaoAPF_ComplexidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_TDTITLEFILTERDATA", AV81FuncaoAPF_TDTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_TDTITLEFILTERDATA", AV81FuncaoAPF_TDTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_ARTITLEFILTERDATA", AV85FuncaoAPF_ARTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_ARTITLEFILTERDATA", AV85FuncaoAPF_ARTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_PFTITLEFILTERDATA", AV89FuncaoAPF_PFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_PFTITLEFILTERDATA", AV89FuncaoAPF_PFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA", AV93FuncaoAPF_FunAPFPaiNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA", AV93FuncaoAPF_FunAPFPaiNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAOAPF_ATIVOTITLEFILTERDATA", AV97FuncaoAPF_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAOAPF_ATIVOTITLEFILTERDATA", AV97FuncaoAPF_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAOAPF_TIPO_SELS", AV75TFFuncaoAPF_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAOAPF_TIPO_SELS", AV75TFFuncaoAPF_Tipo_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAOAPF_COMPLEXIDADE_SELS", AV79TFFuncaoAPF_Complexidade_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAOAPF_COMPLEXIDADE_SELS", AV79TFFuncaoAPF_Complexidade_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAOAPF_ATIVO_SELS", AV99TFFuncaoAPF_Ativo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAOAPF_ATIVO_SELS", AV99TFFuncaoAPF_Ativo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV108Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTFUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutFuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTFUNCAOAPF_NOME", AV8InOutFuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Caption", StringUtil.RTrim( Ddo_funcaoapf_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Cls", StringUtil.RTrim( Ddo_funcaoapf_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcaoapf_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapf_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Sortasc", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcaoapf_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcaoapf_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapf_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Caption", StringUtil.RTrim( Ddo_funcaoapf_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Cls", StringUtil.RTrim( Ddo_funcaoapf_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapf_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaoapf_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaoapf_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Sortasc", StringUtil.RTrim( Ddo_funcaoapf_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Sortdsc", StringUtil.RTrim( Ddo_funcaoapf_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Caption", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Cls", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaoapf_complexidade_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Caption", StringUtil.RTrim( Ddo_funcaoapf_td_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_td_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Cls", StringUtil.RTrim( Ddo_funcaoapf_td_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_td_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapf_td_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_td_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_td_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_td_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_td_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_td_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_td_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_td_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_td_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_td_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapf_td_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapf_td_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_td_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Caption", StringUtil.RTrim( Ddo_funcaoapf_ar_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_ar_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Cls", StringUtil.RTrim( Ddo_funcaoapf_ar_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_ar_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapf_ar_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_ar_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_ar_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_ar_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_ar_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_ar_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_ar_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_ar_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_ar_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_ar_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapf_ar_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapf_ar_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_ar_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Caption", StringUtil.RTrim( Ddo_funcaoapf_pf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_pf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Cls", StringUtil.RTrim( Ddo_funcaoapf_pf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_pf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_pf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_pf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_pf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_pf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapf_pf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapf_pf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_pf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Caption", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Cls", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_funapfpainom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_funapfpainom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_funapfpainom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Filtertype", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapf_funapfpainom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_funapfpainom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalistproc", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapf_funapfpainom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortasc", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortdsc", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Loadingdata", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Caption", StringUtil.RTrim( Ddo_funcaoapf_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Tooltip", StringUtil.RTrim( Ddo_funcaoapf_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Cls", StringUtil.RTrim( Ddo_funcaoapf_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapf_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapf_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapf_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapf_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapf_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapf_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapf_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapf_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_funcaoapf_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaoapf_ativo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaoapf_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Sortasc", StringUtil.RTrim( Ddo_funcaoapf_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_funcaoapf_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapf_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapf_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_complexidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_td_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_td_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_TD_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapf_td_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_ar_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_ar_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_AR_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapf_ar_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_pf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_PF_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaoapf_pf_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_FUNAPFPAINOM_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_funapfpainom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapf_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAOAPF_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapf_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm402( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptFuncaoAPF" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Fun��es APF - An�lise de Ponto de Fun��o" ;
      }

      protected void WB400( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_402( true) ;
         }
         else
         {
            wb_table1_2_402( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_402e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(110, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(111, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_nome_Internalname, AV70TFFuncaoAPF_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavTffuncaoapf_nome_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_nome_sel_Internalname, AV71TFFuncaoAPF_Nome_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", 0, edtavTffuncaoapf_nome_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_td_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV82TFFuncaoAPF_TD), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV82TFFuncaoAPF_TD), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_td_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_td_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_td_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV83TFFuncaoAPF_TD_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV83TFFuncaoAPF_TD_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_td_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_td_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_ar_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV86TFFuncaoAPF_AR), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV86TFFuncaoAPF_AR), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_ar_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_ar_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_ar_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV87TFFuncaoAPF_AR_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV87TFFuncaoAPF_AR_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_ar_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_ar_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_pf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV90TFFuncaoAPF_PF, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV90TFFuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_pf_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_pf_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapf_pf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV91TFFuncaoAPF_PF_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV91TFFuncaoAPF_PF_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapf_pf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapf_pf_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptFuncaoAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_funapfpainom_Internalname, AV94TFFuncaoAPF_FunAPFPaiNom, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", 0, edtavTffuncaoapf_funapfpainom_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapf_funapfpainom_sel_Internalname, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavTffuncaoapf_funapfpainom_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Internalname, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_COMPLEXIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_TDContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Internalname, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", 0, edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_ARContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_artitlecontrolidtoreplace_Internalname, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", 0, edtavDdo_funcaoapf_artitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_PFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", 0, edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_FUNAPFPAINOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Internalname, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", 0, edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAOAPF_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Internalname, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", 0, edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
         }
         wbLoad = true;
      }

      protected void START402( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Fun��es APF - An�lise de Ponto de Fun��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP400( ) ;
      }

      protected void WS402( )
      {
         START402( ) ;
         EVT402( ) ;
      }

      protected void EVT402( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11402 */
                           E11402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12402 */
                           E12402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_TIPO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13402 */
                           E13402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_COMPLEXIDADE.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14402 */
                           E14402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_TD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15402 */
                           E15402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_AR.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16402 */
                           E16402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_PF.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17402 */
                           E17402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_FUNAPFPAINOM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18402 */
                           E18402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPF_ATIVO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19402 */
                           E19402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20402 */
                           E20402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21402 */
                           E21402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22402 */
                           E22402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23402 */
                           E23402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24402 */
                           E24402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25402 */
                           E25402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26402 */
                           E26402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E27402 */
                           E27402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E28402 */
                           E28402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E29402 */
                           E29402 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_92_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
                           SubsflControlProps_922( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV107Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                           A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
                           cmbFuncaoAPF_Tipo.Name = cmbFuncaoAPF_Tipo_Internalname;
                           cmbFuncaoAPF_Tipo.CurrentValue = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
                           A184FuncaoAPF_Tipo = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
                           cmbFuncaoAPF_Complexidade.Name = cmbFuncaoAPF_Complexidade_Internalname;
                           cmbFuncaoAPF_Complexidade.CurrentValue = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
                           A185FuncaoAPF_Complexidade = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
                           A388FuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_TD_Internalname), ",", "."));
                           A387FuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_AR_Internalname), ",", "."));
                           A386FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtFuncaoAPF_PF_Internalname), ",", ".");
                           A360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_SistemaCod_Internalname), ",", "."));
                           n360FuncaoAPF_SistemaCod = false;
                           A359FuncaoAPF_ModuloCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_ModuloCod_Internalname), ",", "."));
                           n359FuncaoAPF_ModuloCod = false;
                           A358FuncaoAPF_FunAPFPaiCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_FunAPFPaiCod_Internalname), ",", "."));
                           n358FuncaoAPF_FunAPFPaiCod = false;
                           A363FuncaoAPF_FunAPFPaiNom = cgiGet( edtFuncaoAPF_FunAPFPaiNom_Internalname);
                           n363FuncaoAPF_FunAPFPaiNom = false;
                           cmbFuncaoAPF_Ativo.Name = cmbFuncaoAPF_Ativo_Internalname;
                           cmbFuncaoAPF_Ativo.CurrentValue = cgiGet( cmbFuncaoAPF_Ativo_Internalname);
                           A183FuncaoAPF_Ativo = cgiGet( cmbFuncaoAPF_Ativo_Internalname);
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E30402 */
                                 E30402 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E31402 */
                                 E31402 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E32402 */
                                 E32402 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME1"), AV17FuncaoAPF_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_sistemades1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_SISTEMADES1"), AV60FuncaoAPF_SistemaDes1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_modulonom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_MODULONOM1"), AV61FuncaoAPF_ModuloNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_funapfpainom1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_FUNAPFPAINOM1"), AV62FuncaoAPF_FunAPFPaiNom1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_tipo1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO1"), AV31FuncaoAPF_Tipo1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME2"), AV21FuncaoAPF_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_sistemades2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_SISTEMADES2"), AV63FuncaoAPF_SistemaDes2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_modulonom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_MODULONOM2"), AV64FuncaoAPF_ModuloNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_funapfpainom2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_FUNAPFPAINOM2"), AV65FuncaoAPF_FunAPFPaiNom2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_tipo2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO2"), AV34FuncaoAPF_Tipo2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME3"), AV25FuncaoAPF_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_sistemades3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_SISTEMADES3"), AV66FuncaoAPF_SistemaDes3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_modulonom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_MODULONOM3"), AV67FuncaoAPF_ModuloNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_funapfpainom3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_FUNAPFPAINOM3"), AV68FuncaoAPF_FunAPFPaiNom3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Funcaoapf_tipo3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO3"), AV37FuncaoAPF_Tipo3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME"), AV70TFFuncaoAPF_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME_SEL"), AV71TFFuncaoAPF_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_td Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_TD"), ",", ".") != Convert.ToDecimal( AV82TFFuncaoAPF_TD )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_td_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_TD_TO"), ",", ".") != Convert.ToDecimal( AV83TFFuncaoAPF_TD_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_ar Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_AR"), ",", ".") != Convert.ToDecimal( AV86TFFuncaoAPF_AR )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_ar_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_AR_TO"), ",", ".") != Convert.ToDecimal( AV87TFFuncaoAPF_AR_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_pf Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_PF"), ",", ".") != AV90TFFuncaoAPF_PF )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_pf_to Changed */
                                    if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_PF_TO"), ",", ".") != AV91TFFuncaoAPF_PF_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_funapfpainom Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_FUNAPFPAINOM"), AV94TFFuncaoAPF_FunAPFPaiNom) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tffuncaoapf_funapfpainom_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_FUNAPFPAINOM_SEL"), AV95TFFuncaoAPF_FunAPFPaiNom_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E33402 */
                                       E33402 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE402( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm402( ) ;
            }
         }
      }

      protected void PA402( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_NOME", "de Transa��o", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_SISTEMADES", "Sistema", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_MODULONOM", "M�dulo", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_FUNAPFPAINOM", "Vinculada com", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavFuncaoapf_tipo1.Name = "vFUNCAOAPF_TIPO1";
            cmbavFuncaoapf_tipo1.WebTags = "";
            cmbavFuncaoapf_tipo1.addItem("", "Todos", 0);
            cmbavFuncaoapf_tipo1.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo1.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo1.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo1.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo1.ItemCount > 0 )
            {
               AV31FuncaoAPF_Tipo1 = cmbavFuncaoapf_tipo1.getValidValue(AV31FuncaoAPF_Tipo1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31FuncaoAPF_Tipo1", AV31FuncaoAPF_Tipo1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_NOME", "de Transa��o", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_SISTEMADES", "Sistema", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_MODULONOM", "M�dulo", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_FUNAPFPAINOM", "Vinculada com", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavFuncaoapf_tipo2.Name = "vFUNCAOAPF_TIPO2";
            cmbavFuncaoapf_tipo2.WebTags = "";
            cmbavFuncaoapf_tipo2.addItem("", "Todos", 0);
            cmbavFuncaoapf_tipo2.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo2.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo2.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo2.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo2.ItemCount > 0 )
            {
               AV34FuncaoAPF_Tipo2 = cmbavFuncaoapf_tipo2.getValidValue(AV34FuncaoAPF_Tipo2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34FuncaoAPF_Tipo2", AV34FuncaoAPF_Tipo2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_NOME", "de Transa��o", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_SISTEMADES", "Sistema", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_MODULONOM", "M�dulo", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_FUNAPFPAINOM", "Vinculada com", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAOAPF_TIPO", "Tipo", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            cmbavFuncaoapf_tipo3.Name = "vFUNCAOAPF_TIPO3";
            cmbavFuncaoapf_tipo3.WebTags = "";
            cmbavFuncaoapf_tipo3.addItem("", "Todos", 0);
            cmbavFuncaoapf_tipo3.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo3.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo3.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo3.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo3.ItemCount > 0 )
            {
               AV37FuncaoAPF_Tipo3 = cmbavFuncaoapf_tipo3.getValidValue(AV37FuncaoAPF_Tipo3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FuncaoAPF_Tipo3", AV37FuncaoAPF_Tipo3);
            }
            GXCCtl = "FUNCAOAPF_TIPO_" + sGXsfl_92_idx;
            cmbFuncaoAPF_Tipo.Name = GXCCtl;
            cmbFuncaoAPF_Tipo.WebTags = "";
            cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
            cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
            cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
            cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
            if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
            {
               A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            }
            GXCCtl = "FUNCAOAPF_COMPLEXIDADE_" + sGXsfl_92_idx;
            cmbFuncaoAPF_Complexidade.Name = GXCCtl;
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            }
            GXCCtl = "FUNCAOAPF_ATIVO_" + sGXsfl_92_idx;
            cmbFuncaoAPF_Ativo.Name = GXCCtl;
            cmbFuncaoAPF_Ativo.WebTags = "";
            cmbFuncaoAPF_Ativo.addItem("A", "Ativa", 0);
            cmbFuncaoAPF_Ativo.addItem("E", "Exclu�da", 0);
            cmbFuncaoAPF_Ativo.addItem("R", "Rejeitada", 0);
            if ( cmbFuncaoAPF_Ativo.ItemCount > 0 )
            {
               A183FuncaoAPF_Ativo = cmbFuncaoAPF_Ativo.getValidValue(A183FuncaoAPF_Ativo);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_922( ) ;
         while ( nGXsfl_92_idx <= nRC_GXsfl_92 )
         {
            sendrow_922( ) ;
            nGXsfl_92_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_92_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_92_idx+1));
            sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
            SubsflControlProps_922( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17FuncaoAPF_Nome1 ,
                                       String AV60FuncaoAPF_SistemaDes1 ,
                                       String AV61FuncaoAPF_ModuloNom1 ,
                                       String AV62FuncaoAPF_FunAPFPaiNom1 ,
                                       String AV31FuncaoAPF_Tipo1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21FuncaoAPF_Nome2 ,
                                       String AV63FuncaoAPF_SistemaDes2 ,
                                       String AV64FuncaoAPF_ModuloNom2 ,
                                       String AV65FuncaoAPF_FunAPFPaiNom2 ,
                                       String AV34FuncaoAPF_Tipo2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25FuncaoAPF_Nome3 ,
                                       String AV66FuncaoAPF_SistemaDes3 ,
                                       String AV67FuncaoAPF_ModuloNom3 ,
                                       String AV68FuncaoAPF_FunAPFPaiNom3 ,
                                       String AV37FuncaoAPF_Tipo3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV70TFFuncaoAPF_Nome ,
                                       String AV71TFFuncaoAPF_Nome_Sel ,
                                       short AV82TFFuncaoAPF_TD ,
                                       short AV83TFFuncaoAPF_TD_To ,
                                       short AV86TFFuncaoAPF_AR ,
                                       short AV87TFFuncaoAPF_AR_To ,
                                       decimal AV90TFFuncaoAPF_PF ,
                                       decimal AV91TFFuncaoAPF_PF_To ,
                                       String AV94TFFuncaoAPF_FunAPFPaiNom ,
                                       String AV95TFFuncaoAPF_FunAPFPaiNom_Sel ,
                                       String AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace ,
                                       String AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace ,
                                       String AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace ,
                                       String AV84ddo_FuncaoAPF_TDTitleControlIdToReplace ,
                                       String AV88ddo_FuncaoAPF_ARTitleControlIdToReplace ,
                                       String AV92ddo_FuncaoAPF_PFTitleControlIdToReplace ,
                                       String AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace ,
                                       String AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace ,
                                       IGxCollection AV75TFFuncaoAPF_Tipo_Sels ,
                                       IGxCollection AV79TFFuncaoAPF_Complexidade_Sels ,
                                       IGxCollection AV99TFFuncaoAPF_Ativo_Sels ,
                                       String AV108Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF402( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_NOME", A166FuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A184FuncaoAPF_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_TIPO", StringUtil.RTrim( A184FuncaoAPF_Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_COMPLEXIDADE", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_TD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_TD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_AR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_AR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_PF", GetSecureSignedToken( "", context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_PF", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_MODULOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A359FuncaoAPF_ModuloCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_MODULOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_FUNAPFPAICOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A358FuncaoAPF_FunAPFPaiCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_FUNAPFPAICOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_ATIVO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A183FuncaoAPF_Ativo, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_ATIVO", StringUtil.RTrim( A183FuncaoAPF_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavFuncaoapf_tipo1.ItemCount > 0 )
         {
            AV31FuncaoAPF_Tipo1 = cmbavFuncaoapf_tipo1.getValidValue(AV31FuncaoAPF_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31FuncaoAPF_Tipo1", AV31FuncaoAPF_Tipo1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavFuncaoapf_tipo2.ItemCount > 0 )
         {
            AV34FuncaoAPF_Tipo2 = cmbavFuncaoapf_tipo2.getValidValue(AV34FuncaoAPF_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34FuncaoAPF_Tipo2", AV34FuncaoAPF_Tipo2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
         if ( cmbavFuncaoapf_tipo3.ItemCount > 0 )
         {
            AV37FuncaoAPF_Tipo3 = cmbavFuncaoapf_tipo3.getValidValue(AV37FuncaoAPF_Tipo3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FuncaoAPF_Tipo3", AV37FuncaoAPF_Tipo3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF402( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV108Pgmname = "PromptFuncaoAPF";
         context.Gx_err = 0;
      }

      protected void RF402( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 92;
         /* Execute user event: E31402 */
         E31402 ();
         nGXsfl_92_idx = 1;
         sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
         SubsflControlProps_922( ) ;
         nGXsfl_92_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_922( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A185FuncaoAPF_Complexidade ,
                                                 AV79TFFuncaoAPF_Complexidade_Sels ,
                                                 A184FuncaoAPF_Tipo ,
                                                 AV75TFFuncaoAPF_Tipo_Sels ,
                                                 A183FuncaoAPF_Ativo ,
                                                 AV99TFFuncaoAPF_Ativo_Sels ,
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17FuncaoAPF_Nome1 ,
                                                 AV60FuncaoAPF_SistemaDes1 ,
                                                 AV61FuncaoAPF_ModuloNom1 ,
                                                 AV62FuncaoAPF_FunAPFPaiNom1 ,
                                                 AV31FuncaoAPF_Tipo1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV21FuncaoAPF_Nome2 ,
                                                 AV63FuncaoAPF_SistemaDes2 ,
                                                 AV64FuncaoAPF_ModuloNom2 ,
                                                 AV65FuncaoAPF_FunAPFPaiNom2 ,
                                                 AV34FuncaoAPF_Tipo2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV25FuncaoAPF_Nome3 ,
                                                 AV66FuncaoAPF_SistemaDes3 ,
                                                 AV67FuncaoAPF_ModuloNom3 ,
                                                 AV68FuncaoAPF_FunAPFPaiNom3 ,
                                                 AV37FuncaoAPF_Tipo3 ,
                                                 AV71TFFuncaoAPF_Nome_Sel ,
                                                 AV70TFFuncaoAPF_Nome ,
                                                 AV75TFFuncaoAPF_Tipo_Sels.Count ,
                                                 AV95TFFuncaoAPF_FunAPFPaiNom_Sel ,
                                                 AV94TFFuncaoAPF_FunAPFPaiNom ,
                                                 AV99TFFuncaoAPF_Ativo_Sels.Count ,
                                                 A166FuncaoAPF_Nome ,
                                                 A362FuncaoAPF_SistemaDes ,
                                                 A361FuncaoAPF_ModuloNom ,
                                                 A363FuncaoAPF_FunAPFPaiNom ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 AV79TFFuncaoAPF_Complexidade_Sels.Count ,
                                                 AV82TFFuncaoAPF_TD ,
                                                 A388FuncaoAPF_TD ,
                                                 AV83TFFuncaoAPF_TD_To ,
                                                 AV86TFFuncaoAPF_AR ,
                                                 A387FuncaoAPF_AR ,
                                                 AV87TFFuncaoAPF_AR_To ,
                                                 AV90TFFuncaoAPF_PF ,
                                                 A386FuncaoAPF_PF ,
                                                 AV91TFFuncaoAPF_PF_To },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.DECIMAL
                                                 }
            });
            lV17FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV17FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPF_Nome1", AV17FuncaoAPF_Nome1);
            lV17FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV17FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPF_Nome1", AV17FuncaoAPF_Nome1);
            lV60FuncaoAPF_SistemaDes1 = StringUtil.Concat( StringUtil.RTrim( AV60FuncaoAPF_SistemaDes1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60FuncaoAPF_SistemaDes1", AV60FuncaoAPF_SistemaDes1);
            lV60FuncaoAPF_SistemaDes1 = StringUtil.Concat( StringUtil.RTrim( AV60FuncaoAPF_SistemaDes1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60FuncaoAPF_SistemaDes1", AV60FuncaoAPF_SistemaDes1);
            lV61FuncaoAPF_ModuloNom1 = StringUtil.PadR( StringUtil.RTrim( AV61FuncaoAPF_ModuloNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61FuncaoAPF_ModuloNom1", AV61FuncaoAPF_ModuloNom1);
            lV61FuncaoAPF_ModuloNom1 = StringUtil.PadR( StringUtil.RTrim( AV61FuncaoAPF_ModuloNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61FuncaoAPF_ModuloNom1", AV61FuncaoAPF_ModuloNom1);
            lV62FuncaoAPF_FunAPFPaiNom1 = StringUtil.Concat( StringUtil.RTrim( AV62FuncaoAPF_FunAPFPaiNom1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62FuncaoAPF_FunAPFPaiNom1", AV62FuncaoAPF_FunAPFPaiNom1);
            lV62FuncaoAPF_FunAPFPaiNom1 = StringUtil.Concat( StringUtil.RTrim( AV62FuncaoAPF_FunAPFPaiNom1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62FuncaoAPF_FunAPFPaiNom1", AV62FuncaoAPF_FunAPFPaiNom1);
            lV21FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV21FuncaoAPF_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPF_Nome2", AV21FuncaoAPF_Nome2);
            lV21FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV21FuncaoAPF_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPF_Nome2", AV21FuncaoAPF_Nome2);
            lV63FuncaoAPF_SistemaDes2 = StringUtil.Concat( StringUtil.RTrim( AV63FuncaoAPF_SistemaDes2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63FuncaoAPF_SistemaDes2", AV63FuncaoAPF_SistemaDes2);
            lV63FuncaoAPF_SistemaDes2 = StringUtil.Concat( StringUtil.RTrim( AV63FuncaoAPF_SistemaDes2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63FuncaoAPF_SistemaDes2", AV63FuncaoAPF_SistemaDes2);
            lV64FuncaoAPF_ModuloNom2 = StringUtil.PadR( StringUtil.RTrim( AV64FuncaoAPF_ModuloNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64FuncaoAPF_ModuloNom2", AV64FuncaoAPF_ModuloNom2);
            lV64FuncaoAPF_ModuloNom2 = StringUtil.PadR( StringUtil.RTrim( AV64FuncaoAPF_ModuloNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64FuncaoAPF_ModuloNom2", AV64FuncaoAPF_ModuloNom2);
            lV65FuncaoAPF_FunAPFPaiNom2 = StringUtil.Concat( StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65FuncaoAPF_FunAPFPaiNom2", AV65FuncaoAPF_FunAPFPaiNom2);
            lV65FuncaoAPF_FunAPFPaiNom2 = StringUtil.Concat( StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65FuncaoAPF_FunAPFPaiNom2", AV65FuncaoAPF_FunAPFPaiNom2);
            lV25FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV25FuncaoAPF_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPF_Nome3", AV25FuncaoAPF_Nome3);
            lV25FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV25FuncaoAPF_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPF_Nome3", AV25FuncaoAPF_Nome3);
            lV66FuncaoAPF_SistemaDes3 = StringUtil.Concat( StringUtil.RTrim( AV66FuncaoAPF_SistemaDes3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66FuncaoAPF_SistemaDes3", AV66FuncaoAPF_SistemaDes3);
            lV66FuncaoAPF_SistemaDes3 = StringUtil.Concat( StringUtil.RTrim( AV66FuncaoAPF_SistemaDes3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66FuncaoAPF_SistemaDes3", AV66FuncaoAPF_SistemaDes3);
            lV67FuncaoAPF_ModuloNom3 = StringUtil.PadR( StringUtil.RTrim( AV67FuncaoAPF_ModuloNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67FuncaoAPF_ModuloNom3", AV67FuncaoAPF_ModuloNom3);
            lV67FuncaoAPF_ModuloNom3 = StringUtil.PadR( StringUtil.RTrim( AV67FuncaoAPF_ModuloNom3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67FuncaoAPF_ModuloNom3", AV67FuncaoAPF_ModuloNom3);
            lV68FuncaoAPF_FunAPFPaiNom3 = StringUtil.Concat( StringUtil.RTrim( AV68FuncaoAPF_FunAPFPaiNom3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68FuncaoAPF_FunAPFPaiNom3", AV68FuncaoAPF_FunAPFPaiNom3);
            lV68FuncaoAPF_FunAPFPaiNom3 = StringUtil.Concat( StringUtil.RTrim( AV68FuncaoAPF_FunAPFPaiNom3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68FuncaoAPF_FunAPFPaiNom3", AV68FuncaoAPF_FunAPFPaiNom3);
            lV70TFFuncaoAPF_Nome = StringUtil.Concat( StringUtil.RTrim( AV70TFFuncaoAPF_Nome), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFFuncaoAPF_Nome", AV70TFFuncaoAPF_Nome);
            lV94TFFuncaoAPF_FunAPFPaiNom = StringUtil.Concat( StringUtil.RTrim( AV94TFFuncaoAPF_FunAPFPaiNom), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94TFFuncaoAPF_FunAPFPaiNom", AV94TFFuncaoAPF_FunAPFPaiNom);
            /* Using cursor H00402 */
            pr_default.execute(0, new Object[] {AV79TFFuncaoAPF_Complexidade_Sels.Count, lV17FuncaoAPF_Nome1, lV17FuncaoAPF_Nome1, lV60FuncaoAPF_SistemaDes1, lV60FuncaoAPF_SistemaDes1, lV61FuncaoAPF_ModuloNom1, lV61FuncaoAPF_ModuloNom1, lV62FuncaoAPF_FunAPFPaiNom1, lV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, lV21FuncaoAPF_Nome2, lV21FuncaoAPF_Nome2, lV63FuncaoAPF_SistemaDes2, lV63FuncaoAPF_SistemaDes2, lV64FuncaoAPF_ModuloNom2, lV64FuncaoAPF_ModuloNom2, lV65FuncaoAPF_FunAPFPaiNom2, lV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, lV25FuncaoAPF_Nome3, lV25FuncaoAPF_Nome3, lV66FuncaoAPF_SistemaDes3, lV66FuncaoAPF_SistemaDes3, lV67FuncaoAPF_ModuloNom3, lV67FuncaoAPF_ModuloNom3, lV68FuncaoAPF_FunAPFPaiNom3, lV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, lV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, lV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel});
            nGXsfl_92_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A361FuncaoAPF_ModuloNom = H00402_A361FuncaoAPF_ModuloNom[0];
               n361FuncaoAPF_ModuloNom = H00402_n361FuncaoAPF_ModuloNom[0];
               A362FuncaoAPF_SistemaDes = H00402_A362FuncaoAPF_SistemaDes[0];
               n362FuncaoAPF_SistemaDes = H00402_n362FuncaoAPF_SistemaDes[0];
               A183FuncaoAPF_Ativo = H00402_A183FuncaoAPF_Ativo[0];
               A363FuncaoAPF_FunAPFPaiNom = H00402_A363FuncaoAPF_FunAPFPaiNom[0];
               n363FuncaoAPF_FunAPFPaiNom = H00402_n363FuncaoAPF_FunAPFPaiNom[0];
               A358FuncaoAPF_FunAPFPaiCod = H00402_A358FuncaoAPF_FunAPFPaiCod[0];
               n358FuncaoAPF_FunAPFPaiCod = H00402_n358FuncaoAPF_FunAPFPaiCod[0];
               A359FuncaoAPF_ModuloCod = H00402_A359FuncaoAPF_ModuloCod[0];
               n359FuncaoAPF_ModuloCod = H00402_n359FuncaoAPF_ModuloCod[0];
               A360FuncaoAPF_SistemaCod = H00402_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H00402_n360FuncaoAPF_SistemaCod[0];
               A184FuncaoAPF_Tipo = H00402_A184FuncaoAPF_Tipo[0];
               A166FuncaoAPF_Nome = H00402_A166FuncaoAPF_Nome[0];
               A165FuncaoAPF_Codigo = H00402_A165FuncaoAPF_Codigo[0];
               A363FuncaoAPF_FunAPFPaiNom = H00402_A363FuncaoAPF_FunAPFPaiNom[0];
               n363FuncaoAPF_FunAPFPaiNom = H00402_n363FuncaoAPF_FunAPFPaiNom[0];
               A361FuncaoAPF_ModuloNom = H00402_A361FuncaoAPF_ModuloNom[0];
               n361FuncaoAPF_ModuloNom = H00402_n361FuncaoAPF_ModuloNom[0];
               A362FuncaoAPF_SistemaDes = H00402_A362FuncaoAPF_SistemaDes[0];
               n362FuncaoAPF_SistemaDes = H00402_n362FuncaoAPF_SistemaDes[0];
               GXt_char1 = A185FuncaoAPF_Complexidade;
               new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
               A185FuncaoAPF_Complexidade = GXt_char1;
               if ( ( AV79TFFuncaoAPF_Complexidade_Sels.Count <= 0 ) || ( (AV79TFFuncaoAPF_Complexidade_Sels.IndexOf(StringUtil.RTrim( A185FuncaoAPF_Complexidade))>0) ) )
               {
                  GXt_int2 = A388FuncaoAPF_TD;
                  new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
                  A388FuncaoAPF_TD = GXt_int2;
                  if ( (0==AV82TFFuncaoAPF_TD) || ( ( A388FuncaoAPF_TD >= AV82TFFuncaoAPF_TD ) ) )
                  {
                     if ( (0==AV83TFFuncaoAPF_TD_To) || ( ( A388FuncaoAPF_TD <= AV83TFFuncaoAPF_TD_To ) ) )
                     {
                        GXt_int2 = A387FuncaoAPF_AR;
                        new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
                        A387FuncaoAPF_AR = GXt_int2;
                        if ( (0==AV86TFFuncaoAPF_AR) || ( ( A387FuncaoAPF_AR >= AV86TFFuncaoAPF_AR ) ) )
                        {
                           if ( (0==AV87TFFuncaoAPF_AR_To) || ( ( A387FuncaoAPF_AR <= AV87TFFuncaoAPF_AR_To ) ) )
                           {
                              GXt_decimal3 = A386FuncaoAPF_PF;
                              new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
                              A386FuncaoAPF_PF = GXt_decimal3;
                              if ( (Convert.ToDecimal(0)==AV90TFFuncaoAPF_PF) || ( ( A386FuncaoAPF_PF >= AV90TFFuncaoAPF_PF ) ) )
                              {
                                 if ( (Convert.ToDecimal(0)==AV91TFFuncaoAPF_PF_To) || ( ( A386FuncaoAPF_PF <= AV91TFFuncaoAPF_PF_To ) ) )
                                 {
                                    /* Execute user event: E32402 */
                                    E32402 ();
                                 }
                              }
                           }
                        }
                     }
                  }
               }
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 92;
            WB400( ) ;
         }
         nGXsfl_92_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPF_Nome1, AV60FuncaoAPF_SistemaDes1, AV61FuncaoAPF_ModuloNom1, AV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPF_Nome2, AV63FuncaoAPF_SistemaDes2, AV64FuncaoAPF_ModuloNom2, AV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPF_Nome3, AV66FuncaoAPF_SistemaDes3, AV67FuncaoAPF_ModuloNom3, AV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, AV82TFFuncaoAPF_TD, AV83TFFuncaoAPF_TD_To, AV86TFFuncaoAPF_AR, AV87TFFuncaoAPF_AR_To, AV90TFFuncaoAPF_PF, AV91TFFuncaoAPF_PF_To, AV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV75TFFuncaoAPF_Tipo_Sels, AV79TFFuncaoAPF_Complexidade_Sels, AV99TFFuncaoAPF_Ativo_Sels, AV108Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPF_Nome1, AV60FuncaoAPF_SistemaDes1, AV61FuncaoAPF_ModuloNom1, AV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPF_Nome2, AV63FuncaoAPF_SistemaDes2, AV64FuncaoAPF_ModuloNom2, AV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPF_Nome3, AV66FuncaoAPF_SistemaDes3, AV67FuncaoAPF_ModuloNom3, AV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, AV82TFFuncaoAPF_TD, AV83TFFuncaoAPF_TD_To, AV86TFFuncaoAPF_AR, AV87TFFuncaoAPF_AR_To, AV90TFFuncaoAPF_PF, AV91TFFuncaoAPF_PF_To, AV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV75TFFuncaoAPF_Tipo_Sels, AV79TFFuncaoAPF_Complexidade_Sels, AV99TFFuncaoAPF_Ativo_Sels, AV108Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPF_Nome1, AV60FuncaoAPF_SistemaDes1, AV61FuncaoAPF_ModuloNom1, AV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPF_Nome2, AV63FuncaoAPF_SistemaDes2, AV64FuncaoAPF_ModuloNom2, AV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPF_Nome3, AV66FuncaoAPF_SistemaDes3, AV67FuncaoAPF_ModuloNom3, AV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, AV82TFFuncaoAPF_TD, AV83TFFuncaoAPF_TD_To, AV86TFFuncaoAPF_AR, AV87TFFuncaoAPF_AR_To, AV90TFFuncaoAPF_PF, AV91TFFuncaoAPF_PF_To, AV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV75TFFuncaoAPF_Tipo_Sels, AV79TFFuncaoAPF_Complexidade_Sels, AV99TFFuncaoAPF_Ativo_Sels, AV108Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPF_Nome1, AV60FuncaoAPF_SistemaDes1, AV61FuncaoAPF_ModuloNom1, AV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPF_Nome2, AV63FuncaoAPF_SistemaDes2, AV64FuncaoAPF_ModuloNom2, AV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPF_Nome3, AV66FuncaoAPF_SistemaDes3, AV67FuncaoAPF_ModuloNom3, AV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, AV82TFFuncaoAPF_TD, AV83TFFuncaoAPF_TD_To, AV86TFFuncaoAPF_AR, AV87TFFuncaoAPF_AR_To, AV90TFFuncaoAPF_PF, AV91TFFuncaoAPF_PF_To, AV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV75TFFuncaoAPF_Tipo_Sels, AV79TFFuncaoAPF_Complexidade_Sels, AV99TFFuncaoAPF_Ativo_Sels, AV108Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPF_Nome1, AV60FuncaoAPF_SistemaDes1, AV61FuncaoAPF_ModuloNom1, AV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPF_Nome2, AV63FuncaoAPF_SistemaDes2, AV64FuncaoAPF_ModuloNom2, AV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPF_Nome3, AV66FuncaoAPF_SistemaDes3, AV67FuncaoAPF_ModuloNom3, AV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, AV82TFFuncaoAPF_TD, AV83TFFuncaoAPF_TD_To, AV86TFFuncaoAPF_AR, AV87TFFuncaoAPF_AR_To, AV90TFFuncaoAPF_PF, AV91TFFuncaoAPF_PF_To, AV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV75TFFuncaoAPF_Tipo_Sels, AV79TFFuncaoAPF_Complexidade_Sels, AV99TFFuncaoAPF_Ativo_Sels, AV108Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUP400( )
      {
         /* Before Start, stand alone formulas. */
         AV108Pgmname = "PromptFuncaoAPF";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E30402 */
         E30402 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV101DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_NOMETITLEFILTERDATA"), AV69FuncaoAPF_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_TIPOTITLEFILTERDATA"), AV73FuncaoAPF_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA"), AV77FuncaoAPF_ComplexidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_TDTITLEFILTERDATA"), AV81FuncaoAPF_TDTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_ARTITLEFILTERDATA"), AV85FuncaoAPF_ARTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_PFTITLEFILTERDATA"), AV89FuncaoAPF_PFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA"), AV93FuncaoAPF_FunAPFPaiNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAOAPF_ATIVOTITLEFILTERDATA"), AV97FuncaoAPF_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17FuncaoAPF_Nome1 = cgiGet( edtavFuncaoapf_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPF_Nome1", AV17FuncaoAPF_Nome1);
            AV60FuncaoAPF_SistemaDes1 = StringUtil.Upper( cgiGet( edtavFuncaoapf_sistemades1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60FuncaoAPF_SistemaDes1", AV60FuncaoAPF_SistemaDes1);
            AV61FuncaoAPF_ModuloNom1 = StringUtil.Upper( cgiGet( edtavFuncaoapf_modulonom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61FuncaoAPF_ModuloNom1", AV61FuncaoAPF_ModuloNom1);
            AV62FuncaoAPF_FunAPFPaiNom1 = cgiGet( edtavFuncaoapf_funapfpainom1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62FuncaoAPF_FunAPFPaiNom1", AV62FuncaoAPF_FunAPFPaiNom1);
            cmbavFuncaoapf_tipo1.Name = cmbavFuncaoapf_tipo1_Internalname;
            cmbavFuncaoapf_tipo1.CurrentValue = cgiGet( cmbavFuncaoapf_tipo1_Internalname);
            AV31FuncaoAPF_Tipo1 = cgiGet( cmbavFuncaoapf_tipo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31FuncaoAPF_Tipo1", AV31FuncaoAPF_Tipo1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21FuncaoAPF_Nome2 = cgiGet( edtavFuncaoapf_nome2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPF_Nome2", AV21FuncaoAPF_Nome2);
            AV63FuncaoAPF_SistemaDes2 = StringUtil.Upper( cgiGet( edtavFuncaoapf_sistemades2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63FuncaoAPF_SistemaDes2", AV63FuncaoAPF_SistemaDes2);
            AV64FuncaoAPF_ModuloNom2 = StringUtil.Upper( cgiGet( edtavFuncaoapf_modulonom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64FuncaoAPF_ModuloNom2", AV64FuncaoAPF_ModuloNom2);
            AV65FuncaoAPF_FunAPFPaiNom2 = cgiGet( edtavFuncaoapf_funapfpainom2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65FuncaoAPF_FunAPFPaiNom2", AV65FuncaoAPF_FunAPFPaiNom2);
            cmbavFuncaoapf_tipo2.Name = cmbavFuncaoapf_tipo2_Internalname;
            cmbavFuncaoapf_tipo2.CurrentValue = cgiGet( cmbavFuncaoapf_tipo2_Internalname);
            AV34FuncaoAPF_Tipo2 = cgiGet( cmbavFuncaoapf_tipo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34FuncaoAPF_Tipo2", AV34FuncaoAPF_Tipo2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25FuncaoAPF_Nome3 = cgiGet( edtavFuncaoapf_nome3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPF_Nome3", AV25FuncaoAPF_Nome3);
            AV66FuncaoAPF_SistemaDes3 = StringUtil.Upper( cgiGet( edtavFuncaoapf_sistemades3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66FuncaoAPF_SistemaDes3", AV66FuncaoAPF_SistemaDes3);
            AV67FuncaoAPF_ModuloNom3 = StringUtil.Upper( cgiGet( edtavFuncaoapf_modulonom3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67FuncaoAPF_ModuloNom3", AV67FuncaoAPF_ModuloNom3);
            AV68FuncaoAPF_FunAPFPaiNom3 = cgiGet( edtavFuncaoapf_funapfpainom3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68FuncaoAPF_FunAPFPaiNom3", AV68FuncaoAPF_FunAPFPaiNom3);
            cmbavFuncaoapf_tipo3.Name = cmbavFuncaoapf_tipo3_Internalname;
            cmbavFuncaoapf_tipo3.CurrentValue = cgiGet( cmbavFuncaoapf_tipo3_Internalname);
            AV37FuncaoAPF_Tipo3 = cgiGet( cmbavFuncaoapf_tipo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FuncaoAPF_Tipo3", AV37FuncaoAPF_Tipo3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV70TFFuncaoAPF_Nome = cgiGet( edtavTffuncaoapf_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFFuncaoAPF_Nome", AV70TFFuncaoAPF_Nome);
            AV71TFFuncaoAPF_Nome_Sel = cgiGet( edtavTffuncaoapf_nome_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFFuncaoAPF_Nome_Sel", AV71TFFuncaoAPF_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_TD");
               GX_FocusControl = edtavTffuncaoapf_td_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV82TFFuncaoAPF_TD = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFFuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82TFFuncaoAPF_TD), 4, 0)));
            }
            else
            {
               AV82TFFuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFFuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82TFFuncaoAPF_TD), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_TD_TO");
               GX_FocusControl = edtavTffuncaoapf_td_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV83TFFuncaoAPF_TD_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFFuncaoAPF_TD_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFFuncaoAPF_TD_To), 4, 0)));
            }
            else
            {
               AV83TFFuncaoAPF_TD_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaoapf_td_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFFuncaoAPF_TD_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFFuncaoAPF_TD_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_AR");
               GX_FocusControl = edtavTffuncaoapf_ar_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV86TFFuncaoAPF_AR = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFFuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86TFFuncaoAPF_AR), 4, 0)));
            }
            else
            {
               AV86TFFuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFFuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86TFFuncaoAPF_AR), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_AR_TO");
               GX_FocusControl = edtavTffuncaoapf_ar_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV87TFFuncaoAPF_AR_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFFuncaoAPF_AR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87TFFuncaoAPF_AR_To), 4, 0)));
            }
            else
            {
               AV87TFFuncaoAPF_AR_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaoapf_ar_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFFuncaoAPF_AR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87TFFuncaoAPF_AR_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_PF");
               GX_FocusControl = edtavTffuncaoapf_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV90TFFuncaoAPF_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV90TFFuncaoAPF_PF, 14, 5)));
            }
            else
            {
               AV90TFFuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV90TFFuncaoAPF_PF, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAOAPF_PF_TO");
               GX_FocusControl = edtavTffuncaoapf_pf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV91TFFuncaoAPF_PF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV91TFFuncaoAPF_PF_To, 14, 5)));
            }
            else
            {
               AV91TFFuncaoAPF_PF_To = context.localUtil.CToN( cgiGet( edtavTffuncaoapf_pf_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV91TFFuncaoAPF_PF_To, 14, 5)));
            }
            AV94TFFuncaoAPF_FunAPFPaiNom = cgiGet( edtavTffuncaoapf_funapfpainom_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94TFFuncaoAPF_FunAPFPaiNom", AV94TFFuncaoAPF_FunAPFPaiNom);
            AV95TFFuncaoAPF_FunAPFPaiNom_Sel = cgiGet( edtavTffuncaoapf_funapfpainom_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95TFFuncaoAPF_FunAPFPaiNom_Sel", AV95TFFuncaoAPF_FunAPFPaiNom_Sel);
            AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace);
            AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace", AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace);
            AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace", AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace);
            AV84ddo_FuncaoAPF_TDTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_FuncaoAPF_TDTitleControlIdToReplace", AV84ddo_FuncaoAPF_TDTitleControlIdToReplace);
            AV88ddo_FuncaoAPF_ARTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_artitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_FuncaoAPF_ARTitleControlIdToReplace", AV88ddo_FuncaoAPF_ARTitleControlIdToReplace);
            AV92ddo_FuncaoAPF_PFTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_FuncaoAPF_PFTitleControlIdToReplace", AV92ddo_FuncaoAPF_PFTitleControlIdToReplace);
            AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace", AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace);
            AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace", AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_92 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_92"), ",", "."));
            AV103GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV104GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcaoapf_nome_Caption = cgiGet( "DDO_FUNCAOAPF_NOME_Caption");
            Ddo_funcaoapf_nome_Tooltip = cgiGet( "DDO_FUNCAOAPF_NOME_Tooltip");
            Ddo_funcaoapf_nome_Cls = cgiGet( "DDO_FUNCAOAPF_NOME_Cls");
            Ddo_funcaoapf_nome_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_NOME_Filteredtext_set");
            Ddo_funcaoapf_nome_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPF_NOME_Selectedvalue_set");
            Ddo_funcaoapf_nome_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_NOME_Dropdownoptionstype");
            Ddo_funcaoapf_nome_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_NOME_Titlecontrolidtoreplace");
            Ddo_funcaoapf_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includesortasc"));
            Ddo_funcaoapf_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includesortdsc"));
            Ddo_funcaoapf_nome_Sortedstatus = cgiGet( "DDO_FUNCAOAPF_NOME_Sortedstatus");
            Ddo_funcaoapf_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includefilter"));
            Ddo_funcaoapf_nome_Filtertype = cgiGet( "DDO_FUNCAOAPF_NOME_Filtertype");
            Ddo_funcaoapf_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Filterisrange"));
            Ddo_funcaoapf_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_NOME_Includedatalist"));
            Ddo_funcaoapf_nome_Datalisttype = cgiGet( "DDO_FUNCAOAPF_NOME_Datalisttype");
            Ddo_funcaoapf_nome_Datalistproc = cgiGet( "DDO_FUNCAOAPF_NOME_Datalistproc");
            Ddo_funcaoapf_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOAPF_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapf_nome_Sortasc = cgiGet( "DDO_FUNCAOAPF_NOME_Sortasc");
            Ddo_funcaoapf_nome_Sortdsc = cgiGet( "DDO_FUNCAOAPF_NOME_Sortdsc");
            Ddo_funcaoapf_nome_Loadingdata = cgiGet( "DDO_FUNCAOAPF_NOME_Loadingdata");
            Ddo_funcaoapf_nome_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_NOME_Cleanfilter");
            Ddo_funcaoapf_nome_Noresultsfound = cgiGet( "DDO_FUNCAOAPF_NOME_Noresultsfound");
            Ddo_funcaoapf_nome_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_NOME_Searchbuttontext");
            Ddo_funcaoapf_tipo_Caption = cgiGet( "DDO_FUNCAOAPF_TIPO_Caption");
            Ddo_funcaoapf_tipo_Tooltip = cgiGet( "DDO_FUNCAOAPF_TIPO_Tooltip");
            Ddo_funcaoapf_tipo_Cls = cgiGet( "DDO_FUNCAOAPF_TIPO_Cls");
            Ddo_funcaoapf_tipo_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPF_TIPO_Selectedvalue_set");
            Ddo_funcaoapf_tipo_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_TIPO_Dropdownoptionstype");
            Ddo_funcaoapf_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_TIPO_Titlecontrolidtoreplace");
            Ddo_funcaoapf_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TIPO_Includesortasc"));
            Ddo_funcaoapf_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TIPO_Includesortdsc"));
            Ddo_funcaoapf_tipo_Sortedstatus = cgiGet( "DDO_FUNCAOAPF_TIPO_Sortedstatus");
            Ddo_funcaoapf_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TIPO_Includefilter"));
            Ddo_funcaoapf_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TIPO_Includedatalist"));
            Ddo_funcaoapf_tipo_Datalisttype = cgiGet( "DDO_FUNCAOAPF_TIPO_Datalisttype");
            Ddo_funcaoapf_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TIPO_Allowmultipleselection"));
            Ddo_funcaoapf_tipo_Datalistfixedvalues = cgiGet( "DDO_FUNCAOAPF_TIPO_Datalistfixedvalues");
            Ddo_funcaoapf_tipo_Sortasc = cgiGet( "DDO_FUNCAOAPF_TIPO_Sortasc");
            Ddo_funcaoapf_tipo_Sortdsc = cgiGet( "DDO_FUNCAOAPF_TIPO_Sortdsc");
            Ddo_funcaoapf_tipo_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_TIPO_Cleanfilter");
            Ddo_funcaoapf_tipo_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_TIPO_Searchbuttontext");
            Ddo_funcaoapf_complexidade_Caption = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Caption");
            Ddo_funcaoapf_complexidade_Tooltip = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Tooltip");
            Ddo_funcaoapf_complexidade_Cls = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Cls");
            Ddo_funcaoapf_complexidade_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_set");
            Ddo_funcaoapf_complexidade_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Dropdownoptionstype");
            Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Titlecontrolidtoreplace");
            Ddo_funcaoapf_complexidade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Includesortasc"));
            Ddo_funcaoapf_complexidade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Includesortdsc"));
            Ddo_funcaoapf_complexidade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Includefilter"));
            Ddo_funcaoapf_complexidade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Includedatalist"));
            Ddo_funcaoapf_complexidade_Datalisttype = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Datalisttype");
            Ddo_funcaoapf_complexidade_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Allowmultipleselection"));
            Ddo_funcaoapf_complexidade_Datalistfixedvalues = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Datalistfixedvalues");
            Ddo_funcaoapf_complexidade_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Cleanfilter");
            Ddo_funcaoapf_complexidade_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Searchbuttontext");
            Ddo_funcaoapf_td_Caption = cgiGet( "DDO_FUNCAOAPF_TD_Caption");
            Ddo_funcaoapf_td_Tooltip = cgiGet( "DDO_FUNCAOAPF_TD_Tooltip");
            Ddo_funcaoapf_td_Cls = cgiGet( "DDO_FUNCAOAPF_TD_Cls");
            Ddo_funcaoapf_td_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_TD_Filteredtext_set");
            Ddo_funcaoapf_td_Filteredtextto_set = cgiGet( "DDO_FUNCAOAPF_TD_Filteredtextto_set");
            Ddo_funcaoapf_td_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_TD_Dropdownoptionstype");
            Ddo_funcaoapf_td_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_TD_Titlecontrolidtoreplace");
            Ddo_funcaoapf_td_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TD_Includesortasc"));
            Ddo_funcaoapf_td_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TD_Includesortdsc"));
            Ddo_funcaoapf_td_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TD_Includefilter"));
            Ddo_funcaoapf_td_Filtertype = cgiGet( "DDO_FUNCAOAPF_TD_Filtertype");
            Ddo_funcaoapf_td_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TD_Filterisrange"));
            Ddo_funcaoapf_td_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_TD_Includedatalist"));
            Ddo_funcaoapf_td_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_TD_Cleanfilter");
            Ddo_funcaoapf_td_Rangefilterfrom = cgiGet( "DDO_FUNCAOAPF_TD_Rangefilterfrom");
            Ddo_funcaoapf_td_Rangefilterto = cgiGet( "DDO_FUNCAOAPF_TD_Rangefilterto");
            Ddo_funcaoapf_td_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_TD_Searchbuttontext");
            Ddo_funcaoapf_ar_Caption = cgiGet( "DDO_FUNCAOAPF_AR_Caption");
            Ddo_funcaoapf_ar_Tooltip = cgiGet( "DDO_FUNCAOAPF_AR_Tooltip");
            Ddo_funcaoapf_ar_Cls = cgiGet( "DDO_FUNCAOAPF_AR_Cls");
            Ddo_funcaoapf_ar_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_AR_Filteredtext_set");
            Ddo_funcaoapf_ar_Filteredtextto_set = cgiGet( "DDO_FUNCAOAPF_AR_Filteredtextto_set");
            Ddo_funcaoapf_ar_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_AR_Dropdownoptionstype");
            Ddo_funcaoapf_ar_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_AR_Titlecontrolidtoreplace");
            Ddo_funcaoapf_ar_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_AR_Includesortasc"));
            Ddo_funcaoapf_ar_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_AR_Includesortdsc"));
            Ddo_funcaoapf_ar_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_AR_Includefilter"));
            Ddo_funcaoapf_ar_Filtertype = cgiGet( "DDO_FUNCAOAPF_AR_Filtertype");
            Ddo_funcaoapf_ar_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_AR_Filterisrange"));
            Ddo_funcaoapf_ar_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_AR_Includedatalist"));
            Ddo_funcaoapf_ar_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_AR_Cleanfilter");
            Ddo_funcaoapf_ar_Rangefilterfrom = cgiGet( "DDO_FUNCAOAPF_AR_Rangefilterfrom");
            Ddo_funcaoapf_ar_Rangefilterto = cgiGet( "DDO_FUNCAOAPF_AR_Rangefilterto");
            Ddo_funcaoapf_ar_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_AR_Searchbuttontext");
            Ddo_funcaoapf_pf_Caption = cgiGet( "DDO_FUNCAOAPF_PF_Caption");
            Ddo_funcaoapf_pf_Tooltip = cgiGet( "DDO_FUNCAOAPF_PF_Tooltip");
            Ddo_funcaoapf_pf_Cls = cgiGet( "DDO_FUNCAOAPF_PF_Cls");
            Ddo_funcaoapf_pf_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_PF_Filteredtext_set");
            Ddo_funcaoapf_pf_Filteredtextto_set = cgiGet( "DDO_FUNCAOAPF_PF_Filteredtextto_set");
            Ddo_funcaoapf_pf_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_PF_Dropdownoptionstype");
            Ddo_funcaoapf_pf_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_PF_Titlecontrolidtoreplace");
            Ddo_funcaoapf_pf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_PF_Includesortasc"));
            Ddo_funcaoapf_pf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_PF_Includesortdsc"));
            Ddo_funcaoapf_pf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_PF_Includefilter"));
            Ddo_funcaoapf_pf_Filtertype = cgiGet( "DDO_FUNCAOAPF_PF_Filtertype");
            Ddo_funcaoapf_pf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_PF_Filterisrange"));
            Ddo_funcaoapf_pf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_PF_Includedatalist"));
            Ddo_funcaoapf_pf_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_PF_Cleanfilter");
            Ddo_funcaoapf_pf_Rangefilterfrom = cgiGet( "DDO_FUNCAOAPF_PF_Rangefilterfrom");
            Ddo_funcaoapf_pf_Rangefilterto = cgiGet( "DDO_FUNCAOAPF_PF_Rangefilterto");
            Ddo_funcaoapf_pf_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_PF_Searchbuttontext");
            Ddo_funcaoapf_funapfpainom_Caption = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Caption");
            Ddo_funcaoapf_funapfpainom_Tooltip = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Tooltip");
            Ddo_funcaoapf_funapfpainom_Cls = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Cls");
            Ddo_funcaoapf_funapfpainom_Filteredtext_set = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Filteredtext_set");
            Ddo_funcaoapf_funapfpainom_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Selectedvalue_set");
            Ddo_funcaoapf_funapfpainom_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Dropdownoptionstype");
            Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Titlecontrolidtoreplace");
            Ddo_funcaoapf_funapfpainom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Includesortasc"));
            Ddo_funcaoapf_funapfpainom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Includesortdsc"));
            Ddo_funcaoapf_funapfpainom_Sortedstatus = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortedstatus");
            Ddo_funcaoapf_funapfpainom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Includefilter"));
            Ddo_funcaoapf_funapfpainom_Filtertype = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Filtertype");
            Ddo_funcaoapf_funapfpainom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Filterisrange"));
            Ddo_funcaoapf_funapfpainom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Includedatalist"));
            Ddo_funcaoapf_funapfpainom_Datalisttype = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalisttype");
            Ddo_funcaoapf_funapfpainom_Datalistproc = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalistproc");
            Ddo_funcaoapf_funapfpainom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapf_funapfpainom_Sortasc = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortasc");
            Ddo_funcaoapf_funapfpainom_Sortdsc = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Sortdsc");
            Ddo_funcaoapf_funapfpainom_Loadingdata = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Loadingdata");
            Ddo_funcaoapf_funapfpainom_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Cleanfilter");
            Ddo_funcaoapf_funapfpainom_Noresultsfound = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Noresultsfound");
            Ddo_funcaoapf_funapfpainom_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Searchbuttontext");
            Ddo_funcaoapf_ativo_Caption = cgiGet( "DDO_FUNCAOAPF_ATIVO_Caption");
            Ddo_funcaoapf_ativo_Tooltip = cgiGet( "DDO_FUNCAOAPF_ATIVO_Tooltip");
            Ddo_funcaoapf_ativo_Cls = cgiGet( "DDO_FUNCAOAPF_ATIVO_Cls");
            Ddo_funcaoapf_ativo_Selectedvalue_set = cgiGet( "DDO_FUNCAOAPF_ATIVO_Selectedvalue_set");
            Ddo_funcaoapf_ativo_Dropdownoptionstype = cgiGet( "DDO_FUNCAOAPF_ATIVO_Dropdownoptionstype");
            Ddo_funcaoapf_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAOAPF_ATIVO_Titlecontrolidtoreplace");
            Ddo_funcaoapf_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_ATIVO_Includesortasc"));
            Ddo_funcaoapf_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_ATIVO_Includesortdsc"));
            Ddo_funcaoapf_ativo_Sortedstatus = cgiGet( "DDO_FUNCAOAPF_ATIVO_Sortedstatus");
            Ddo_funcaoapf_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_ATIVO_Includefilter"));
            Ddo_funcaoapf_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_ATIVO_Includedatalist"));
            Ddo_funcaoapf_ativo_Datalisttype = cgiGet( "DDO_FUNCAOAPF_ATIVO_Datalisttype");
            Ddo_funcaoapf_ativo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAOAPF_ATIVO_Allowmultipleselection"));
            Ddo_funcaoapf_ativo_Datalistfixedvalues = cgiGet( "DDO_FUNCAOAPF_ATIVO_Datalistfixedvalues");
            Ddo_funcaoapf_ativo_Sortasc = cgiGet( "DDO_FUNCAOAPF_ATIVO_Sortasc");
            Ddo_funcaoapf_ativo_Sortdsc = cgiGet( "DDO_FUNCAOAPF_ATIVO_Sortdsc");
            Ddo_funcaoapf_ativo_Cleanfilter = cgiGet( "DDO_FUNCAOAPF_ATIVO_Cleanfilter");
            Ddo_funcaoapf_ativo_Searchbuttontext = cgiGet( "DDO_FUNCAOAPF_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcaoapf_nome_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_NOME_Activeeventkey");
            Ddo_funcaoapf_nome_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_NOME_Filteredtext_get");
            Ddo_funcaoapf_nome_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPF_NOME_Selectedvalue_get");
            Ddo_funcaoapf_tipo_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_TIPO_Activeeventkey");
            Ddo_funcaoapf_tipo_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPF_TIPO_Selectedvalue_get");
            Ddo_funcaoapf_complexidade_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Activeeventkey");
            Ddo_funcaoapf_complexidade_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPF_COMPLEXIDADE_Selectedvalue_get");
            Ddo_funcaoapf_td_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_TD_Activeeventkey");
            Ddo_funcaoapf_td_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_TD_Filteredtext_get");
            Ddo_funcaoapf_td_Filteredtextto_get = cgiGet( "DDO_FUNCAOAPF_TD_Filteredtextto_get");
            Ddo_funcaoapf_ar_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_AR_Activeeventkey");
            Ddo_funcaoapf_ar_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_AR_Filteredtext_get");
            Ddo_funcaoapf_ar_Filteredtextto_get = cgiGet( "DDO_FUNCAOAPF_AR_Filteredtextto_get");
            Ddo_funcaoapf_pf_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_PF_Activeeventkey");
            Ddo_funcaoapf_pf_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_PF_Filteredtext_get");
            Ddo_funcaoapf_pf_Filteredtextto_get = cgiGet( "DDO_FUNCAOAPF_PF_Filteredtextto_get");
            Ddo_funcaoapf_funapfpainom_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Activeeventkey");
            Ddo_funcaoapf_funapfpainom_Filteredtext_get = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Filteredtext_get");
            Ddo_funcaoapf_funapfpainom_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPF_FUNAPFPAINOM_Selectedvalue_get");
            Ddo_funcaoapf_ativo_Activeeventkey = cgiGet( "DDO_FUNCAOAPF_ATIVO_Activeeventkey");
            Ddo_funcaoapf_ativo_Selectedvalue_get = cgiGet( "DDO_FUNCAOAPF_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME1"), AV17FuncaoAPF_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_SISTEMADES1"), AV60FuncaoAPF_SistemaDes1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_MODULONOM1"), AV61FuncaoAPF_ModuloNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_FUNAPFPAINOM1"), AV62FuncaoAPF_FunAPFPaiNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO1"), AV31FuncaoAPF_Tipo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME2"), AV21FuncaoAPF_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_SISTEMADES2"), AV63FuncaoAPF_SistemaDes2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_MODULONOM2"), AV64FuncaoAPF_ModuloNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_FUNAPFPAINOM2"), AV65FuncaoAPF_FunAPFPaiNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO2"), AV34FuncaoAPF_Tipo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_NOME3"), AV25FuncaoAPF_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_SISTEMADES3"), AV66FuncaoAPF_SistemaDes3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_MODULONOM3"), AV67FuncaoAPF_ModuloNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_FUNAPFPAINOM3"), AV68FuncaoAPF_FunAPFPaiNom3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAOAPF_TIPO3"), AV37FuncaoAPF_Tipo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME"), AV70TFFuncaoAPF_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_NOME_SEL"), AV71TFFuncaoAPF_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_TD"), ",", ".") != Convert.ToDecimal( AV82TFFuncaoAPF_TD )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_TD_TO"), ",", ".") != Convert.ToDecimal( AV83TFFuncaoAPF_TD_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_AR"), ",", ".") != Convert.ToDecimal( AV86TFFuncaoAPF_AR )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_AR_TO"), ",", ".") != Convert.ToDecimal( AV87TFFuncaoAPF_AR_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_PF"), ",", ".") != AV90TFFuncaoAPF_PF )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( "GXH_vTFFUNCAOAPF_PF_TO"), ",", ".") != AV91TFFuncaoAPF_PF_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_FUNAPFPAINOM"), AV94TFFuncaoAPF_FunAPFPaiNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAOAPF_FUNAPFPAINOM_SEL"), AV95TFFuncaoAPF_FunAPFPaiNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E30402 */
         E30402 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E30402( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV31FuncaoAPF_Tipo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31FuncaoAPF_Tipo1", AV31FuncaoAPF_Tipo1);
         AV15DynamicFiltersSelector1 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV34FuncaoAPF_Tipo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34FuncaoAPF_Tipo2", AV34FuncaoAPF_Tipo2);
         AV19DynamicFiltersSelector2 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV37FuncaoAPF_Tipo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FuncaoAPF_Tipo3", AV37FuncaoAPF_Tipo3);
         AV23DynamicFiltersSelector3 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTffuncaoapf_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_nome_Visible), 5, 0)));
         edtavTffuncaoapf_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_nome_sel_Visible), 5, 0)));
         edtavTffuncaoapf_td_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_td_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_td_Visible), 5, 0)));
         edtavTffuncaoapf_td_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_td_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_td_to_Visible), 5, 0)));
         edtavTffuncaoapf_ar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_ar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_ar_Visible), 5, 0)));
         edtavTffuncaoapf_ar_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_ar_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_ar_to_Visible), 5, 0)));
         edtavTffuncaoapf_pf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_pf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_pf_Visible), 5, 0)));
         edtavTffuncaoapf_pf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_pf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_pf_to_Visible), 5, 0)));
         edtavTffuncaoapf_funapfpainom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_funapfpainom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_funapfpainom_Visible), 5, 0)));
         edtavTffuncaoapf_funapfpainom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaoapf_funapfpainom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapf_funapfpainom_sel_Visible), 5, 0)));
         Ddo_funcaoapf_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_nome_Titlecontrolidtoreplace);
         AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace = Ddo_funcaoapf_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace", AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace);
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_tipo_Titlecontrolidtoreplace);
         AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace = Ddo_funcaoapf_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace", AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace);
         edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Complexidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_complexidade_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace);
         AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace", AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace);
         edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_td_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_TD";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_td_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_td_Titlecontrolidtoreplace);
         AV84ddo_FuncaoAPF_TDTitleControlIdToReplace = Ddo_funcaoapf_td_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84ddo_FuncaoAPF_TDTitleControlIdToReplace", AV84ddo_FuncaoAPF_TDTitleControlIdToReplace);
         edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_ar_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_AR";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ar_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_ar_Titlecontrolidtoreplace);
         AV88ddo_FuncaoAPF_ARTitleControlIdToReplace = Ddo_funcaoapf_ar_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88ddo_FuncaoAPF_ARTitleControlIdToReplace", AV88ddo_FuncaoAPF_ARTitleControlIdToReplace);
         edtavDdo_funcaoapf_artitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_artitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_artitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_pf_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_PF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_pf_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_pf_Titlecontrolidtoreplace);
         AV92ddo_FuncaoAPF_PFTitleControlIdToReplace = Ddo_funcaoapf_pf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92ddo_FuncaoAPF_PFTitleControlIdToReplace", AV92ddo_FuncaoAPF_PFTitleControlIdToReplace);
         edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_FunAPFPaiNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace);
         AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace = Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace", AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace);
         edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapf_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPF_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "TitleControlIdToReplace", Ddo_funcaoapf_ativo_Titlecontrolidtoreplace);
         AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace = Ddo_funcaoapf_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace", AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace);
         edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Select Fun��o de Transa��o";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "de Transa��o", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         cmbavOrderedby.addItem("3", "Vinculada com", 0);
         cmbavOrderedby.addItem("4", "da Fun��o", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons4 = AV101DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons4) ;
         AV101DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons4;
      }

      protected void E31402( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV69FuncaoAPF_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73FuncaoAPF_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV77FuncaoAPF_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV81FuncaoAPF_TDTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV85FuncaoAPF_ARTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV89FuncaoAPF_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV93FuncaoAPF_FunAPFPaiNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV97FuncaoAPF_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_SISTEMADES") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_MODULONOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_SISTEMADES") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_MODULONOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV22DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_SISTEMADES") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_MODULONOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoAPF_Nome_Titleformat = 2;
         edtFuncaoAPF_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Transa��o", AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_Nome_Internalname, "Title", edtFuncaoAPF_Nome_Title);
         cmbFuncaoAPF_Tipo_Titleformat = 2;
         cmbFuncaoAPF_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Tipo_Internalname, "Title", cmbFuncaoAPF_Tipo.Title.Text);
         cmbFuncaoAPF_Complexidade_Titleformat = 2;
         cmbFuncaoAPF_Complexidade.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Complexidade", AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Complexidade_Internalname, "Title", cmbFuncaoAPF_Complexidade.Title.Text);
         edtFuncaoAPF_TD_Titleformat = 2;
         edtFuncaoAPF_TD_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "DER", AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_TD_Internalname, "Title", edtFuncaoAPF_TD_Title);
         edtFuncaoAPF_AR_Titleformat = 2;
         edtFuncaoAPF_AR_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "ALR", AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_AR_Internalname, "Title", edtFuncaoAPF_AR_Title);
         edtFuncaoAPF_PF_Titleformat = 2;
         edtFuncaoAPF_PF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PF", AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_PF_Internalname, "Title", edtFuncaoAPF_PF_Title);
         edtFuncaoAPF_FunAPFPaiNom_Titleformat = 2;
         edtFuncaoAPF_FunAPFPaiNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Vinculada com", AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPF_FunAPFPaiNom_Internalname, "Title", edtFuncaoAPF_FunAPFPaiNom_Title);
         cmbFuncaoAPF_Ativo_Titleformat = 2;
         cmbFuncaoAPF_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "da Fun��o", AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Ativo_Internalname, "Title", cmbFuncaoAPF_Ativo.Title.Text);
         AV103GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV103GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV103GridCurrentPage), 10, 0)));
         AV104GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV104GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV104GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV69FuncaoAPF_NomeTitleFilterData", AV69FuncaoAPF_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73FuncaoAPF_TipoTitleFilterData", AV73FuncaoAPF_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV77FuncaoAPF_ComplexidadeTitleFilterData", AV77FuncaoAPF_ComplexidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV81FuncaoAPF_TDTitleFilterData", AV81FuncaoAPF_TDTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV85FuncaoAPF_ARTitleFilterData", AV85FuncaoAPF_ARTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV89FuncaoAPF_PFTitleFilterData", AV89FuncaoAPF_PFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV93FuncaoAPF_FunAPFPaiNomTitleFilterData", AV93FuncaoAPF_FunAPFPaiNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV97FuncaoAPF_AtivoTitleFilterData", AV97FuncaoAPF_AtivoTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11402( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV102PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV102PageToGo) ;
         }
      }

      protected void E12402( )
      {
         /* Ddo_funcaoapf_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV70TFFuncaoAPF_Nome = Ddo_funcaoapf_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFFuncaoAPF_Nome", AV70TFFuncaoAPF_Nome);
            AV71TFFuncaoAPF_Nome_Sel = Ddo_funcaoapf_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFFuncaoAPF_Nome_Sel", AV71TFFuncaoAPF_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13402( )
      {
         /* Ddo_funcaoapf_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "SortedStatus", Ddo_funcaoapf_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "SortedStatus", Ddo_funcaoapf_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFFuncaoAPF_Tipo_SelsJson = Ddo_funcaoapf_tipo_Selectedvalue_get;
            AV75TFFuncaoAPF_Tipo_Sels.FromJSonString(AV74TFFuncaoAPF_Tipo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV75TFFuncaoAPF_Tipo_Sels", AV75TFFuncaoAPF_Tipo_Sels);
      }

      protected void E14402( )
      {
         /* Ddo_funcaoapf_complexidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_complexidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV78TFFuncaoAPF_Complexidade_SelsJson = Ddo_funcaoapf_complexidade_Selectedvalue_get;
            AV79TFFuncaoAPF_Complexidade_Sels.FromJSonString(AV78TFFuncaoAPF_Complexidade_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79TFFuncaoAPF_Complexidade_Sels", AV79TFFuncaoAPF_Complexidade_Sels);
      }

      protected void E15402( )
      {
         /* Ddo_funcaoapf_td_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_td_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV82TFFuncaoAPF_TD = (short)(NumberUtil.Val( Ddo_funcaoapf_td_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFFuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82TFFuncaoAPF_TD), 4, 0)));
            AV83TFFuncaoAPF_TD_To = (short)(NumberUtil.Val( Ddo_funcaoapf_td_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFFuncaoAPF_TD_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFFuncaoAPF_TD_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E16402( )
      {
         /* Ddo_funcaoapf_ar_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_ar_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV86TFFuncaoAPF_AR = (short)(NumberUtil.Val( Ddo_funcaoapf_ar_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFFuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86TFFuncaoAPF_AR), 4, 0)));
            AV87TFFuncaoAPF_AR_To = (short)(NumberUtil.Val( Ddo_funcaoapf_ar_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFFuncaoAPF_AR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87TFFuncaoAPF_AR_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E17402( )
      {
         /* Ddo_funcaoapf_pf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_pf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV90TFFuncaoAPF_PF = NumberUtil.Val( Ddo_funcaoapf_pf_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV90TFFuncaoAPF_PF, 14, 5)));
            AV91TFFuncaoAPF_PF_To = NumberUtil.Val( Ddo_funcaoapf_pf_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV91TFFuncaoAPF_PF_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E18402( )
      {
         /* Ddo_funcaoapf_funapfpainom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_funapfpainom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_funapfpainom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "SortedStatus", Ddo_funcaoapf_funapfpainom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_funapfpainom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_funapfpainom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "SortedStatus", Ddo_funcaoapf_funapfpainom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_funapfpainom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV94TFFuncaoAPF_FunAPFPaiNom = Ddo_funcaoapf_funapfpainom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94TFFuncaoAPF_FunAPFPaiNom", AV94TFFuncaoAPF_FunAPFPaiNom);
            AV95TFFuncaoAPF_FunAPFPaiNom_Sel = Ddo_funcaoapf_funapfpainom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95TFFuncaoAPF_FunAPFPaiNom_Sel", AV95TFFuncaoAPF_FunAPFPaiNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19402( )
      {
         /* Ddo_funcaoapf_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapf_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "SortedStatus", Ddo_funcaoapf_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_funcaoapf_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "SortedStatus", Ddo_funcaoapf_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapf_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV98TFFuncaoAPF_Ativo_SelsJson = Ddo_funcaoapf_ativo_Selectedvalue_get;
            AV99TFFuncaoAPF_Ativo_Sels.FromJSonString(AV98TFFuncaoAPF_Ativo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV99TFFuncaoAPF_Ativo_Sels", AV99TFFuncaoAPF_Ativo_Sels);
      }

      private void E32402( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV107Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 92;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_922( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_92_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(92, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E33402 */
         E33402 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E33402( )
      {
         /* Enter Routine */
         AV7InOutFuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoAPF_Codigo), 6, 0)));
         AV8InOutFuncaoAPF_Nome = A166FuncaoAPF_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoAPF_Nome", AV8InOutFuncaoAPF_Nome);
         context.setWebReturnParms(new Object[] {(int)AV7InOutFuncaoAPF_Codigo,(String)AV8InOutFuncaoAPF_Nome});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E20402( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E25402( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPF_Nome1, AV60FuncaoAPF_SistemaDes1, AV61FuncaoAPF_ModuloNom1, AV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPF_Nome2, AV63FuncaoAPF_SistemaDes2, AV64FuncaoAPF_ModuloNom2, AV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPF_Nome3, AV66FuncaoAPF_SistemaDes3, AV67FuncaoAPF_ModuloNom3, AV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, AV82TFFuncaoAPF_TD, AV83TFFuncaoAPF_TD_To, AV86TFFuncaoAPF_AR, AV87TFFuncaoAPF_AR_To, AV90TFFuncaoAPF_PF, AV91TFFuncaoAPF_PF_To, AV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV75TFFuncaoAPF_Tipo_Sels, AV79TFFuncaoAPF_Complexidade_Sels, AV99TFFuncaoAPF_Ativo_Sels, AV108Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
      }

      protected void E21402( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPF_Nome1, AV60FuncaoAPF_SistemaDes1, AV61FuncaoAPF_ModuloNom1, AV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPF_Nome2, AV63FuncaoAPF_SistemaDes2, AV64FuncaoAPF_ModuloNom2, AV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPF_Nome3, AV66FuncaoAPF_SistemaDes3, AV67FuncaoAPF_ModuloNom3, AV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, AV82TFFuncaoAPF_TD, AV83TFFuncaoAPF_TD_To, AV86TFFuncaoAPF_AR, AV87TFFuncaoAPF_AR_To, AV90TFFuncaoAPF_PF, AV91TFFuncaoAPF_PF_To, AV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV75TFFuncaoAPF_Tipo_Sels, AV79TFFuncaoAPF_Complexidade_Sels, AV99TFFuncaoAPF_Ativo_Sels, AV108Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV31FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV34FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV37FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
      }

      protected void E26402( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E27402( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPF_Nome1, AV60FuncaoAPF_SistemaDes1, AV61FuncaoAPF_ModuloNom1, AV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPF_Nome2, AV63FuncaoAPF_SistemaDes2, AV64FuncaoAPF_ModuloNom2, AV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPF_Nome3, AV66FuncaoAPF_SistemaDes3, AV67FuncaoAPF_ModuloNom3, AV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, AV82TFFuncaoAPF_TD, AV83TFFuncaoAPF_TD_To, AV86TFFuncaoAPF_AR, AV87TFFuncaoAPF_AR_To, AV90TFFuncaoAPF_PF, AV91TFFuncaoAPF_PF_To, AV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV75TFFuncaoAPF_Tipo_Sels, AV79TFFuncaoAPF_Complexidade_Sels, AV99TFFuncaoAPF_Ativo_Sels, AV108Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
      }

      protected void E22402( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPF_Nome1, AV60FuncaoAPF_SistemaDes1, AV61FuncaoAPF_ModuloNom1, AV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPF_Nome2, AV63FuncaoAPF_SistemaDes2, AV64FuncaoAPF_ModuloNom2, AV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPF_Nome3, AV66FuncaoAPF_SistemaDes3, AV67FuncaoAPF_ModuloNom3, AV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, AV82TFFuncaoAPF_TD, AV83TFFuncaoAPF_TD_To, AV86TFFuncaoAPF_AR, AV87TFFuncaoAPF_AR_To, AV90TFFuncaoAPF_PF, AV91TFFuncaoAPF_PF_To, AV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV75TFFuncaoAPF_Tipo_Sels, AV79TFFuncaoAPF_Complexidade_Sels, AV99TFFuncaoAPF_Ativo_Sels, AV108Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV31FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV34FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV37FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
      }

      protected void E28402( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E23402( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoAPF_Nome1, AV60FuncaoAPF_SistemaDes1, AV61FuncaoAPF_ModuloNom1, AV62FuncaoAPF_FunAPFPaiNom1, AV31FuncaoAPF_Tipo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21FuncaoAPF_Nome2, AV63FuncaoAPF_SistemaDes2, AV64FuncaoAPF_ModuloNom2, AV65FuncaoAPF_FunAPFPaiNom2, AV34FuncaoAPF_Tipo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25FuncaoAPF_Nome3, AV66FuncaoAPF_SistemaDes3, AV67FuncaoAPF_ModuloNom3, AV68FuncaoAPF_FunAPFPaiNom3, AV37FuncaoAPF_Tipo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV70TFFuncaoAPF_Nome, AV71TFFuncaoAPF_Nome_Sel, AV82TFFuncaoAPF_TD, AV83TFFuncaoAPF_TD_To, AV86TFFuncaoAPF_AR, AV87TFFuncaoAPF_AR_To, AV90TFFuncaoAPF_PF, AV91TFFuncaoAPF_PF_To, AV94TFFuncaoAPF_FunAPFPaiNom, AV95TFFuncaoAPF_FunAPFPaiNom_Sel, AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace, AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace, AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace, AV84ddo_FuncaoAPF_TDTitleControlIdToReplace, AV88ddo_FuncaoAPF_ARTitleControlIdToReplace, AV92ddo_FuncaoAPF_PFTitleControlIdToReplace, AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace, AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace, AV75TFFuncaoAPF_Tipo_Sels, AV79TFFuncaoAPF_Complexidade_Sels, AV99TFFuncaoAPF_Ativo_Sels, AV108Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV31FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV34FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV37FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
      }

      protected void E29402( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E24402( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV75TFFuncaoAPF_Tipo_Sels", AV75TFFuncaoAPF_Tipo_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79TFFuncaoAPF_Complexidade_Sels", AV79TFFuncaoAPF_Complexidade_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV99TFFuncaoAPF_Ativo_Sels", AV99TFFuncaoAPF_Ativo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV31FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV34FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV37FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_funcaoapf_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
         Ddo_funcaoapf_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "SortedStatus", Ddo_funcaoapf_tipo_Sortedstatus);
         Ddo_funcaoapf_funapfpainom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "SortedStatus", Ddo_funcaoapf_funapfpainom_Sortedstatus);
         Ddo_funcaoapf_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "SortedStatus", Ddo_funcaoapf_ativo_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_funcaoapf_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SortedStatus", Ddo_funcaoapf_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_funcaoapf_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "SortedStatus", Ddo_funcaoapf_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_funcaoapf_funapfpainom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "SortedStatus", Ddo_funcaoapf_funapfpainom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_funcaoapf_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "SortedStatus", Ddo_funcaoapf_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaoapf_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
         edtavFuncaoapf_sistemades1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_sistemades1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_sistemades1_Visible), 5, 0)));
         edtavFuncaoapf_modulonom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_modulonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_modulonom1_Visible), 5, 0)));
         edtavFuncaoapf_funapfpainom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_funapfpainom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_funapfpainom1_Visible), 5, 0)));
         cmbavFuncaoapf_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_SISTEMADES") == 0 )
         {
            edtavFuncaoapf_sistemades1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_sistemades1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_sistemades1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_MODULONOM") == 0 )
         {
            edtavFuncaoapf_modulonom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_modulonom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_modulonom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
         {
            edtavFuncaoapf_funapfpainom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_funapfpainom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_funapfpainom1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 )
         {
            cmbavFuncaoapf_tipo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavFuncaoapf_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
         edtavFuncaoapf_sistemades2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_sistemades2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_sistemades2_Visible), 5, 0)));
         edtavFuncaoapf_modulonom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_modulonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_modulonom2_Visible), 5, 0)));
         edtavFuncaoapf_funapfpainom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_funapfpainom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_funapfpainom2_Visible), 5, 0)));
         cmbavFuncaoapf_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_SISTEMADES") == 0 )
         {
            edtavFuncaoapf_sistemades2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_sistemades2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_sistemades2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_MODULONOM") == 0 )
         {
            edtavFuncaoapf_modulonom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_modulonom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_modulonom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
         {
            edtavFuncaoapf_funapfpainom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_funapfpainom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_funapfpainom2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 )
         {
            cmbavFuncaoapf_tipo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavFuncaoapf_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
         edtavFuncaoapf_sistemades3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_sistemades3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_sistemades3_Visible), 5, 0)));
         edtavFuncaoapf_modulonom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_modulonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_modulonom3_Visible), 5, 0)));
         edtavFuncaoapf_funapfpainom3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_funapfpainom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_funapfpainom3_Visible), 5, 0)));
         cmbavFuncaoapf_tipo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo3.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_SISTEMADES") == 0 )
         {
            edtavFuncaoapf_sistemades3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_sistemades3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_sistemades3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_MODULONOM") == 0 )
         {
            edtavFuncaoapf_modulonom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_modulonom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_modulonom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
         {
            edtavFuncaoapf_funapfpainom3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_funapfpainom3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_funapfpainom3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_TIPO") == 0 )
         {
            cmbavFuncaoapf_tipo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21FuncaoAPF_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPF_Nome2", AV21FuncaoAPF_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25FuncaoAPF_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPF_Nome3", AV25FuncaoAPF_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV70TFFuncaoAPF_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFFuncaoAPF_Nome", AV70TFFuncaoAPF_Nome);
         Ddo_funcaoapf_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "FilteredText_set", Ddo_funcaoapf_nome_Filteredtext_set);
         AV71TFFuncaoAPF_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFFuncaoAPF_Nome_Sel", AV71TFFuncaoAPF_Nome_Sel);
         Ddo_funcaoapf_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_nome_Internalname, "SelectedValue_set", Ddo_funcaoapf_nome_Selectedvalue_set);
         AV75TFFuncaoAPF_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaoapf_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_tipo_Internalname, "SelectedValue_set", Ddo_funcaoapf_tipo_Selectedvalue_set);
         AV79TFFuncaoAPF_Complexidade_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaoapf_complexidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_complexidade_Internalname, "SelectedValue_set", Ddo_funcaoapf_complexidade_Selectedvalue_set);
         AV82TFFuncaoAPF_TD = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82TFFuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(AV82TFFuncaoAPF_TD), 4, 0)));
         Ddo_funcaoapf_td_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_td_Internalname, "FilteredText_set", Ddo_funcaoapf_td_Filteredtext_set);
         AV83TFFuncaoAPF_TD_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV83TFFuncaoAPF_TD_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV83TFFuncaoAPF_TD_To), 4, 0)));
         Ddo_funcaoapf_td_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_td_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_td_Filteredtextto_set);
         AV86TFFuncaoAPF_AR = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV86TFFuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV86TFFuncaoAPF_AR), 4, 0)));
         Ddo_funcaoapf_ar_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ar_Internalname, "FilteredText_set", Ddo_funcaoapf_ar_Filteredtext_set);
         AV87TFFuncaoAPF_AR_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87TFFuncaoAPF_AR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV87TFFuncaoAPF_AR_To), 4, 0)));
         Ddo_funcaoapf_ar_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ar_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_ar_Filteredtextto_set);
         AV90TFFuncaoAPF_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90TFFuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( AV90TFFuncaoAPF_PF, 14, 5)));
         Ddo_funcaoapf_pf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_pf_Internalname, "FilteredText_set", Ddo_funcaoapf_pf_Filteredtext_set);
         AV91TFFuncaoAPF_PF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91TFFuncaoAPF_PF_To", StringUtil.LTrim( StringUtil.Str( AV91TFFuncaoAPF_PF_To, 14, 5)));
         Ddo_funcaoapf_pf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_pf_Internalname, "FilteredTextTo_set", Ddo_funcaoapf_pf_Filteredtextto_set);
         AV94TFFuncaoAPF_FunAPFPaiNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV94TFFuncaoAPF_FunAPFPaiNom", AV94TFFuncaoAPF_FunAPFPaiNom);
         Ddo_funcaoapf_funapfpainom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "FilteredText_set", Ddo_funcaoapf_funapfpainom_Filteredtext_set);
         AV95TFFuncaoAPF_FunAPFPaiNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV95TFFuncaoAPF_FunAPFPaiNom_Sel", AV95TFFuncaoAPF_FunAPFPaiNom_Sel);
         Ddo_funcaoapf_funapfpainom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_funapfpainom_Internalname, "SelectedValue_set", Ddo_funcaoapf_funapfpainom_Selectedvalue_set);
         AV99TFFuncaoAPF_Ativo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaoapf_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaoapf_ativo_Internalname, "SelectedValue_set", Ddo_funcaoapf_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17FuncaoAPF_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPF_Nome1", AV17FuncaoAPF_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17FuncaoAPF_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoAPF_Nome1", AV17FuncaoAPF_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_SISTEMADES") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV60FuncaoAPF_SistemaDes1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60FuncaoAPF_SistemaDes1", AV60FuncaoAPF_SistemaDes1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_MODULONOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV61FuncaoAPF_ModuloNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61FuncaoAPF_ModuloNom1", AV61FuncaoAPF_ModuloNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV62FuncaoAPF_FunAPFPaiNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62FuncaoAPF_FunAPFPaiNom1", AV62FuncaoAPF_FunAPFPaiNom1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 )
            {
               AV31FuncaoAPF_Tipo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31FuncaoAPF_Tipo1", AV31FuncaoAPF_Tipo1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21FuncaoAPF_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21FuncaoAPF_Nome2", AV21FuncaoAPF_Nome2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_SISTEMADES") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV63FuncaoAPF_SistemaDes2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63FuncaoAPF_SistemaDes2", AV63FuncaoAPF_SistemaDes2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_MODULONOM") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV64FuncaoAPF_ModuloNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64FuncaoAPF_ModuloNom2", AV64FuncaoAPF_ModuloNom2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV65FuncaoAPF_FunAPFPaiNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65FuncaoAPF_FunAPFPaiNom2", AV65FuncaoAPF_FunAPFPaiNom2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 )
               {
                  AV34FuncaoAPF_Tipo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34FuncaoAPF_Tipo2", AV34FuncaoAPF_Tipo2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25FuncaoAPF_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25FuncaoAPF_Nome3", AV25FuncaoAPF_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_SISTEMADES") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV66FuncaoAPF_SistemaDes3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66FuncaoAPF_SistemaDes3", AV66FuncaoAPF_SistemaDes3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_MODULONOM") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV67FuncaoAPF_ModuloNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67FuncaoAPF_ModuloNom3", AV67FuncaoAPF_ModuloNom3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_FUNAPFPAINOM") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV68FuncaoAPF_FunAPFPaiNom3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68FuncaoAPF_FunAPFPaiNom3", AV68FuncaoAPF_FunAPFPaiNom3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_TIPO") == 0 )
                  {
                     AV37FuncaoAPF_Tipo3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37FuncaoAPF_Tipo3", AV37FuncaoAPF_Tipo3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFFuncaoAPF_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV70TFFuncaoAPF_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFFuncaoAPF_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV71TFFuncaoAPF_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV75TFFuncaoAPF_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV75TFFuncaoAPF_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV79TFFuncaoAPF_Complexidade_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_COMPLEXIDADE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV79TFFuncaoAPF_Complexidade_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV82TFFuncaoAPF_TD) && (0==AV83TFFuncaoAPF_TD_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_TD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV82TFFuncaoAPF_TD), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV83TFFuncaoAPF_TD_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV86TFFuncaoAPF_AR) && (0==AV87TFFuncaoAPF_AR_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_AR";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV86TFFuncaoAPF_AR), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV87TFFuncaoAPF_AR_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV90TFFuncaoAPF_PF) && (Convert.ToDecimal(0)==AV91TFFuncaoAPF_PF_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_PF";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV90TFFuncaoAPF_PF, 14, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV91TFFuncaoAPF_PF_To, 14, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94TFFuncaoAPF_FunAPFPaiNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_FUNAPFPAINOM";
            AV11GridStateFilterValue.gxTpr_Value = AV94TFFuncaoAPF_FunAPFPaiNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95TFFuncaoAPF_FunAPFPaiNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_FUNAPFPAINOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV95TFFuncaoAPF_FunAPFPaiNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV99TFFuncaoAPF_Ativo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPF_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV99TFFuncaoAPF_Ativo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV108Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17FuncaoAPF_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17FuncaoAPF_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_SISTEMADES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV60FuncaoAPF_SistemaDes1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV60FuncaoAPF_SistemaDes1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_MODULONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV61FuncaoAPF_ModuloNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV61FuncaoAPF_ModuloNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV62FuncaoAPF_FunAPFPaiNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV62FuncaoAPF_FunAPFPaiNom1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31FuncaoAPF_Tipo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31FuncaoAPF_Tipo1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21FuncaoAPF_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21FuncaoAPF_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_SISTEMADES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV63FuncaoAPF_SistemaDes2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV63FuncaoAPF_SistemaDes2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_MODULONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV64FuncaoAPF_ModuloNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV64FuncaoAPF_ModuloNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV65FuncaoAPF_FunAPFPaiNom2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34FuncaoAPF_Tipo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV34FuncaoAPF_Tipo2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25FuncaoAPF_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25FuncaoAPF_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_SISTEMADES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV66FuncaoAPF_SistemaDes3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV66FuncaoAPF_SistemaDes3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_MODULONOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV67FuncaoAPF_ModuloNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV67FuncaoAPF_ModuloNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV68FuncaoAPF_FunAPFPaiNom3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV68FuncaoAPF_FunAPFPaiNom3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV37FuncaoAPF_Tipo3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV37FuncaoAPF_Tipo3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_402( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_402( true) ;
         }
         else
         {
            wb_table2_5_402( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_402e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_86_402( true) ;
         }
         else
         {
            wb_table3_86_402( false) ;
         }
         return  ;
      }

      protected void wb_table3_86_402e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_402e( true) ;
         }
         else
         {
            wb_table1_2_402e( false) ;
         }
      }

      protected void wb_table3_86_402( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_89_402( true) ;
         }
         else
         {
            wb_table4_89_402( false) ;
         }
         return  ;
      }

      protected void wb_table4_89_402e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_86_402e( true) ;
         }
         else
         {
            wb_table3_86_402e( false) ;
         }
      }

      protected void wb_table4_89_402( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"92\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de Transa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoAPF_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoAPF_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoAPF_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoAPF_Complexidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoAPF_Complexidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoAPF_Complexidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_TD_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_TD_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_TD_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_AR_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_AR_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_AR_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_PF_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_PF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_PF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_FunAPFPaiNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_FunAPFPaiNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_FunAPFPaiNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoAPF_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoAPF_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoAPF_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A166FuncaoAPF_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A184FuncaoAPF_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoAPF_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoAPF_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoAPF_Complexidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoAPF_Complexidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_TD_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_TD_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_AR_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_AR_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_PF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_PF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A363FuncaoAPF_FunAPFPaiNom);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_FunAPFPaiNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_FunAPFPaiNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A183FuncaoAPF_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoAPF_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoAPF_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 92 )
         {
            wbEnd = 0;
            nRC_GXsfl_92 = (short)(nGXsfl_92_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_89_402e( true) ;
         }
         else
         {
            wb_table4_89_402e( false) ;
         }
      }

      protected void wb_table2_5_402( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptFuncaoAPF.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_402( true) ;
         }
         else
         {
            wb_table5_14_402( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_402e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_402e( true) ;
         }
         else
         {
            wb_table2_5_402e( false) ;
         }
      }

      protected void wb_table5_14_402( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_402( true) ;
         }
         else
         {
            wb_table6_19_402( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_402e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_402e( true) ;
         }
         else
         {
            wb_table5_14_402e( false) ;
         }
      }

      protected void wb_table6_19_402( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptFuncaoAPF.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_402( true) ;
         }
         else
         {
            wb_table7_28_402( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_402e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncaoAPF.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_PromptFuncaoAPF.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_49_402( true) ;
         }
         else
         {
            wb_table8_49_402( false) ;
         }
         return  ;
      }

      protected void wb_table8_49_402e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncaoAPF.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "", true, "HLP_PromptFuncaoAPF.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_70_402( true) ;
         }
         else
         {
            wb_table9_70_402( false) ;
         }
         return  ;
      }

      protected void wb_table9_70_402e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptFuncaoAPF.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_402e( true) ;
         }
         else
         {
            wb_table6_19_402e( false) ;
         }
      }

      protected void wb_table9_70_402( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "", true, "HLP_PromptFuncaoAPF.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome3_Internalname, AV25FuncaoAPF_Nome3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", 0, edtavFuncaoapf_nome3_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_sistemades3_Internalname, AV66FuncaoAPF_SistemaDes3, StringUtil.RTrim( context.localUtil.Format( AV66FuncaoAPF_SistemaDes3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_sistemades3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_sistemades3_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_modulonom3_Internalname, StringUtil.RTrim( AV67FuncaoAPF_ModuloNom3), StringUtil.RTrim( context.localUtil.Format( AV67FuncaoAPF_ModuloNom3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_modulonom3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_modulonom3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncaoAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_funapfpainom3_Internalname, AV68FuncaoAPF_FunAPFPaiNom3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", 0, edtavFuncaoapf_funapfpainom3_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo3, cmbavFuncaoapf_tipo3_Internalname, StringUtil.RTrim( AV37FuncaoAPF_Tipo3), 1, cmbavFuncaoapf_tipo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "", true, "HLP_PromptFuncaoAPF.htm");
            cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV37FuncaoAPF_Tipo3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", (String)(cmbavFuncaoapf_tipo3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_70_402e( true) ;
         }
         else
         {
            wb_table9_70_402e( false) ;
         }
      }

      protected void wb_table8_49_402( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_PromptFuncaoAPF.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome2_Internalname, AV21FuncaoAPF_Nome2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, edtavFuncaoapf_nome2_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_sistemades2_Internalname, AV63FuncaoAPF_SistemaDes2, StringUtil.RTrim( context.localUtil.Format( AV63FuncaoAPF_SistemaDes2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,55);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_sistemades2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_sistemades2_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_modulonom2_Internalname, StringUtil.RTrim( AV64FuncaoAPF_ModuloNom2), StringUtil.RTrim( context.localUtil.Format( AV64FuncaoAPF_ModuloNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_modulonom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_modulonom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncaoAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_funapfpainom2_Internalname, AV65FuncaoAPF_FunAPFPaiNom2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", 0, edtavFuncaoapf_funapfpainom2_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo2, cmbavFuncaoapf_tipo2_Internalname, StringUtil.RTrim( AV34FuncaoAPF_Tipo2), 1, cmbavFuncaoapf_tipo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_PromptFuncaoAPF.htm");
            cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV34FuncaoAPF_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", (String)(cmbavFuncaoapf_tipo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_49_402e( true) ;
         }
         else
         {
            wb_table8_49_402e( false) ;
         }
      }

      protected void wb_table7_28_402( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptFuncaoAPF.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome1_Internalname, AV17FuncaoAPF_Nome1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", 0, edtavFuncaoapf_nome1_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_sistemades1_Internalname, AV60FuncaoAPF_SistemaDes1, StringUtil.RTrim( context.localUtil.Format( AV60FuncaoAPF_SistemaDes1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_sistemades1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_sistemades1_Visible, 1, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncaoAPF.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_92_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_modulonom1_Internalname, StringUtil.RTrim( AV61FuncaoAPF_ModuloNom1), StringUtil.RTrim( context.localUtil.Format( AV61FuncaoAPF_ModuloNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_modulonom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_modulonom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptFuncaoAPF.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_92_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_funapfpainom1_Internalname, AV62FuncaoAPF_FunAPFPaiNom1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", 0, edtavFuncaoapf_funapfpainom1_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptFuncaoAPF.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_92_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo1, cmbavFuncaoapf_tipo1_Internalname, StringUtil.RTrim( AV31FuncaoAPF_Tipo1), 1, cmbavFuncaoapf_tipo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_PromptFuncaoAPF.htm");
            cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV31FuncaoAPF_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", (String)(cmbavFuncaoapf_tipo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_402e( true) ;
         }
         else
         {
            wb_table7_28_402e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutFuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutFuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutFuncaoAPF_Codigo), 6, 0)));
         AV8InOutFuncaoAPF_Nome = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutFuncaoAPF_Nome", AV8InOutFuncaoAPF_Nome);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA402( ) ;
         WS402( ) ;
         WE402( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117335190");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptfuncaoapf.js", "?20203117335190");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_922( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_92_idx;
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO_"+sGXsfl_92_idx;
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME_"+sGXsfl_92_idx;
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO_"+sGXsfl_92_idx;
         cmbFuncaoAPF_Complexidade_Internalname = "FUNCAOAPF_COMPLEXIDADE_"+sGXsfl_92_idx;
         edtFuncaoAPF_TD_Internalname = "FUNCAOAPF_TD_"+sGXsfl_92_idx;
         edtFuncaoAPF_AR_Internalname = "FUNCAOAPF_AR_"+sGXsfl_92_idx;
         edtFuncaoAPF_PF_Internalname = "FUNCAOAPF_PF_"+sGXsfl_92_idx;
         edtFuncaoAPF_SistemaCod_Internalname = "FUNCAOAPF_SISTEMACOD_"+sGXsfl_92_idx;
         edtFuncaoAPF_ModuloCod_Internalname = "FUNCAOAPF_MODULOCOD_"+sGXsfl_92_idx;
         edtFuncaoAPF_FunAPFPaiCod_Internalname = "FUNCAOAPF_FUNAPFPAICOD_"+sGXsfl_92_idx;
         edtFuncaoAPF_FunAPFPaiNom_Internalname = "FUNCAOAPF_FUNAPFPAINOM_"+sGXsfl_92_idx;
         cmbFuncaoAPF_Ativo_Internalname = "FUNCAOAPF_ATIVO_"+sGXsfl_92_idx;
      }

      protected void SubsflControlProps_fel_922( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_92_fel_idx;
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO_"+sGXsfl_92_fel_idx;
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME_"+sGXsfl_92_fel_idx;
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO_"+sGXsfl_92_fel_idx;
         cmbFuncaoAPF_Complexidade_Internalname = "FUNCAOAPF_COMPLEXIDADE_"+sGXsfl_92_fel_idx;
         edtFuncaoAPF_TD_Internalname = "FUNCAOAPF_TD_"+sGXsfl_92_fel_idx;
         edtFuncaoAPF_AR_Internalname = "FUNCAOAPF_AR_"+sGXsfl_92_fel_idx;
         edtFuncaoAPF_PF_Internalname = "FUNCAOAPF_PF_"+sGXsfl_92_fel_idx;
         edtFuncaoAPF_SistemaCod_Internalname = "FUNCAOAPF_SISTEMACOD_"+sGXsfl_92_fel_idx;
         edtFuncaoAPF_ModuloCod_Internalname = "FUNCAOAPF_MODULOCOD_"+sGXsfl_92_fel_idx;
         edtFuncaoAPF_FunAPFPaiCod_Internalname = "FUNCAOAPF_FUNAPFPAICOD_"+sGXsfl_92_fel_idx;
         edtFuncaoAPF_FunAPFPaiNom_Internalname = "FUNCAOAPF_FUNAPFPAINOM_"+sGXsfl_92_fel_idx;
         cmbFuncaoAPF_Ativo_Internalname = "FUNCAOAPF_ATIVO_"+sGXsfl_92_fel_idx;
      }

      protected void sendrow_922( )
      {
         SubsflControlProps_922( ) ;
         WB400( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_92_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_92_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_92_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 93,'',false,'',92)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV107Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV107Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_92_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Nome_Internalname,(String)A166FuncaoAPF_Nome,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_92_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAOAPF_TIPO_" + sGXsfl_92_idx;
               cmbFuncaoAPF_Tipo.Name = GXCCtl;
               cmbFuncaoAPF_Tipo.WebTags = "";
               cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
               cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
               cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
               cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
               cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
               if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
               {
                  A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Tipo,(String)cmbFuncaoAPF_Tipo_Internalname,StringUtil.RTrim( A184FuncaoAPF_Tipo),(short)1,(String)cmbFuncaoAPF_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Tipo.CurrentValue = StringUtil.RTrim( A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Tipo_Internalname, "Values", (String)(cmbFuncaoAPF_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "FUNCAOAPF_COMPLEXIDADE_" + sGXsfl_92_idx;
            cmbFuncaoAPF_Complexidade.Name = GXCCtl;
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Complexidade,(String)cmbFuncaoAPF_Complexidade_Internalname,StringUtil.RTrim( A185FuncaoAPF_Complexidade),(short)1,(String)cmbFuncaoAPF_Complexidade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Complexidade.CurrentValue = StringUtil.RTrim( A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Complexidade_Internalname, "Values", (String)(cmbFuncaoAPF_Complexidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_TD_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_TD_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_AR_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_AR_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_PF_Internalname,StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")),context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_PF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_ModuloCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A359FuncaoAPF_ModuloCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A359FuncaoAPF_ModuloCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_ModuloCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_FunAPFPaiCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A358FuncaoAPF_FunAPFPaiCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_FunAPFPaiCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_FunAPFPaiNom_Internalname,(String)A363FuncaoAPF_FunAPFPaiNom,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_FunAPFPaiNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)92,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_92_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAOAPF_ATIVO_" + sGXsfl_92_idx;
               cmbFuncaoAPF_Ativo.Name = GXCCtl;
               cmbFuncaoAPF_Ativo.WebTags = "";
               cmbFuncaoAPF_Ativo.addItem("A", "Ativa", 0);
               cmbFuncaoAPF_Ativo.addItem("E", "Exclu�da", 0);
               cmbFuncaoAPF_Ativo.addItem("R", "Rejeitada", 0);
               if ( cmbFuncaoAPF_Ativo.ItemCount > 0 )
               {
                  A183FuncaoAPF_Ativo = cmbFuncaoAPF_Ativo.getValidValue(A183FuncaoAPF_Ativo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Ativo,(String)cmbFuncaoAPF_Ativo_Internalname,StringUtil.RTrim( A183FuncaoAPF_Ativo),(short)1,(String)cmbFuncaoAPF_Ativo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Ativo.CurrentValue = StringUtil.RTrim( A183FuncaoAPF_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoAPF_Ativo_Internalname, "Values", (String)(cmbFuncaoAPF_Ativo.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_CODIGO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_NOME"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_TIPO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A184FuncaoAPF_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_COMPLEXIDADE"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_TD"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_AR"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_PF"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_SISTEMACOD"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_MODULOCOD"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A359FuncaoAPF_ModuloCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_FUNAPFPAICOD"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, context.localUtil.Format( (decimal)(A358FuncaoAPF_FunAPFPaiCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAOAPF_ATIVO"+"_"+sGXsfl_92_idx, GetSecureSignedToken( sGXsfl_92_idx, StringUtil.RTrim( context.localUtil.Format( A183FuncaoAPF_Ativo, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_92_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_92_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_92_idx+1));
            sGXsfl_92_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_92_idx), 4, 0)), 4, "0");
            SubsflControlProps_922( ) ;
         }
         /* End function sendrow_922 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavFuncaoapf_nome1_Internalname = "vFUNCAOAPF_NOME1";
         edtavFuncaoapf_sistemades1_Internalname = "vFUNCAOAPF_SISTEMADES1";
         edtavFuncaoapf_modulonom1_Internalname = "vFUNCAOAPF_MODULONOM1";
         edtavFuncaoapf_funapfpainom1_Internalname = "vFUNCAOAPF_FUNAPFPAINOM1";
         cmbavFuncaoapf_tipo1_Internalname = "vFUNCAOAPF_TIPO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavFuncaoapf_nome2_Internalname = "vFUNCAOAPF_NOME2";
         edtavFuncaoapf_sistemades2_Internalname = "vFUNCAOAPF_SISTEMADES2";
         edtavFuncaoapf_modulonom2_Internalname = "vFUNCAOAPF_MODULONOM2";
         edtavFuncaoapf_funapfpainom2_Internalname = "vFUNCAOAPF_FUNAPFPAINOM2";
         cmbavFuncaoapf_tipo2_Internalname = "vFUNCAOAPF_TIPO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavFuncaoapf_nome3_Internalname = "vFUNCAOAPF_NOME3";
         edtavFuncaoapf_sistemades3_Internalname = "vFUNCAOAPF_SISTEMADES3";
         edtavFuncaoapf_modulonom3_Internalname = "vFUNCAOAPF_MODULONOM3";
         edtavFuncaoapf_funapfpainom3_Internalname = "vFUNCAOAPF_FUNAPFPAINOM3";
         cmbavFuncaoapf_tipo3_Internalname = "vFUNCAOAPF_TIPO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtFuncaoAPF_Codigo_Internalname = "FUNCAOAPF_CODIGO";
         edtFuncaoAPF_Nome_Internalname = "FUNCAOAPF_NOME";
         cmbFuncaoAPF_Tipo_Internalname = "FUNCAOAPF_TIPO";
         cmbFuncaoAPF_Complexidade_Internalname = "FUNCAOAPF_COMPLEXIDADE";
         edtFuncaoAPF_TD_Internalname = "FUNCAOAPF_TD";
         edtFuncaoAPF_AR_Internalname = "FUNCAOAPF_AR";
         edtFuncaoAPF_PF_Internalname = "FUNCAOAPF_PF";
         edtFuncaoAPF_SistemaCod_Internalname = "FUNCAOAPF_SISTEMACOD";
         edtFuncaoAPF_ModuloCod_Internalname = "FUNCAOAPF_MODULOCOD";
         edtFuncaoAPF_FunAPFPaiCod_Internalname = "FUNCAOAPF_FUNAPFPAICOD";
         edtFuncaoAPF_FunAPFPaiNom_Internalname = "FUNCAOAPF_FUNAPFPAINOM";
         cmbFuncaoAPF_Ativo_Internalname = "FUNCAOAPF_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTffuncaoapf_nome_Internalname = "vTFFUNCAOAPF_NOME";
         edtavTffuncaoapf_nome_sel_Internalname = "vTFFUNCAOAPF_NOME_SEL";
         edtavTffuncaoapf_td_Internalname = "vTFFUNCAOAPF_TD";
         edtavTffuncaoapf_td_to_Internalname = "vTFFUNCAOAPF_TD_TO";
         edtavTffuncaoapf_ar_Internalname = "vTFFUNCAOAPF_AR";
         edtavTffuncaoapf_ar_to_Internalname = "vTFFUNCAOAPF_AR_TO";
         edtavTffuncaoapf_pf_Internalname = "vTFFUNCAOAPF_PF";
         edtavTffuncaoapf_pf_to_Internalname = "vTFFUNCAOAPF_PF_TO";
         edtavTffuncaoapf_funapfpainom_Internalname = "vTFFUNCAOAPF_FUNAPFPAINOM";
         edtavTffuncaoapf_funapfpainom_sel_Internalname = "vTFFUNCAOAPF_FUNAPFPAINOM_SEL";
         Ddo_funcaoapf_nome_Internalname = "DDO_FUNCAOAPF_NOME";
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_tipo_Internalname = "DDO_FUNCAOAPF_TIPO";
         edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_complexidade_Internalname = "DDO_FUNCAOAPF_COMPLEXIDADE";
         edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_td_Internalname = "DDO_FUNCAOAPF_TD";
         edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_ar_Internalname = "DDO_FUNCAOAPF_AR";
         edtavDdo_funcaoapf_artitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_pf_Internalname = "DDO_FUNCAOAPF_PF";
         edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_funapfpainom_Internalname = "DDO_FUNCAOAPF_FUNAPFPAINOM";
         edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapf_ativo_Internalname = "DDO_FUNCAOAPF_ATIVO";
         edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Internalname = "vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbFuncaoAPF_Ativo_Jsonclick = "";
         edtFuncaoAPF_FunAPFPaiNom_Jsonclick = "";
         edtFuncaoAPF_FunAPFPaiCod_Jsonclick = "";
         edtFuncaoAPF_ModuloCod_Jsonclick = "";
         edtFuncaoAPF_SistemaCod_Jsonclick = "";
         edtFuncaoAPF_PF_Jsonclick = "";
         edtFuncaoAPF_AR_Jsonclick = "";
         edtFuncaoAPF_TD_Jsonclick = "";
         cmbFuncaoAPF_Complexidade_Jsonclick = "";
         cmbFuncaoAPF_Tipo_Jsonclick = "";
         edtFuncaoAPF_Nome_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         cmbavFuncaoapf_tipo1_Jsonclick = "";
         edtavFuncaoapf_modulonom1_Jsonclick = "";
         edtavFuncaoapf_sistemades1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavFuncaoapf_tipo2_Jsonclick = "";
         edtavFuncaoapf_modulonom2_Jsonclick = "";
         edtavFuncaoapf_sistemades2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavFuncaoapf_tipo3_Jsonclick = "";
         edtavFuncaoapf_modulonom3_Jsonclick = "";
         edtavFuncaoapf_sistemades3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         cmbFuncaoAPF_Ativo_Titleformat = 0;
         edtFuncaoAPF_FunAPFPaiNom_Titleformat = 0;
         edtFuncaoAPF_PF_Titleformat = 0;
         edtFuncaoAPF_AR_Titleformat = 0;
         edtFuncaoAPF_TD_Titleformat = 0;
         cmbFuncaoAPF_Complexidade_Titleformat = 0;
         cmbFuncaoAPF_Tipo_Titleformat = 0;
         edtFuncaoAPF_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         cmbavFuncaoapf_tipo3.Visible = 1;
         edtavFuncaoapf_funapfpainom3_Visible = 1;
         edtavFuncaoapf_modulonom3_Visible = 1;
         edtavFuncaoapf_sistemades3_Visible = 1;
         edtavFuncaoapf_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         cmbavFuncaoapf_tipo2.Visible = 1;
         edtavFuncaoapf_funapfpainom2_Visible = 1;
         edtavFuncaoapf_modulonom2_Visible = 1;
         edtavFuncaoapf_sistemades2_Visible = 1;
         edtavFuncaoapf_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         cmbavFuncaoapf_tipo1.Visible = 1;
         edtavFuncaoapf_funapfpainom1_Visible = 1;
         edtavFuncaoapf_modulonom1_Visible = 1;
         edtavFuncaoapf_sistemades1_Visible = 1;
         edtavFuncaoapf_nome1_Visible = 1;
         cmbFuncaoAPF_Ativo.Title.Text = "da Fun��o";
         edtFuncaoAPF_FunAPFPaiNom_Title = "Vinculada com";
         edtFuncaoAPF_PF_Title = "PF";
         edtFuncaoAPF_AR_Title = "ALR";
         edtFuncaoAPF_TD_Title = "DER";
         cmbFuncaoAPF_Complexidade.Title.Text = "Complexidade";
         cmbFuncaoAPF_Tipo.Title.Text = "Tipo";
         edtFuncaoAPF_Nome_Title = "de Transa��o";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_artitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaoapf_funapfpainom_sel_Visible = 1;
         edtavTffuncaoapf_funapfpainom_Visible = 1;
         edtavTffuncaoapf_pf_to_Jsonclick = "";
         edtavTffuncaoapf_pf_to_Visible = 1;
         edtavTffuncaoapf_pf_Jsonclick = "";
         edtavTffuncaoapf_pf_Visible = 1;
         edtavTffuncaoapf_ar_to_Jsonclick = "";
         edtavTffuncaoapf_ar_to_Visible = 1;
         edtavTffuncaoapf_ar_Jsonclick = "";
         edtavTffuncaoapf_ar_Visible = 1;
         edtavTffuncaoapf_td_to_Jsonclick = "";
         edtavTffuncaoapf_td_to_Visible = 1;
         edtavTffuncaoapf_td_Jsonclick = "";
         edtavTffuncaoapf_td_Visible = 1;
         edtavTffuncaoapf_nome_sel_Visible = 1;
         edtavTffuncaoapf_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_funcaoapf_ativo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaoapf_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapf_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapf_ativo_Datalistfixedvalues = "A:Ativa,E:Exclu�da,R:Rejeitada";
         Ddo_funcaoapf_ativo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ativo_Datalisttype = "FixedValues";
         Ddo_funcaoapf_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaoapf_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ativo_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_ativo_Cls = "ColumnSettings";
         Ddo_funcaoapf_ativo_Tooltip = "Op��es";
         Ddo_funcaoapf_ativo_Caption = "";
         Ddo_funcaoapf_funapfpainom_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_funapfpainom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapf_funapfpainom_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_funapfpainom_Loadingdata = "Carregando dados...";
         Ddo_funcaoapf_funapfpainom_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapf_funapfpainom_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapf_funapfpainom_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapf_funapfpainom_Datalistproc = "GetPromptFuncaoAPFFilterData";
         Ddo_funcaoapf_funapfpainom_Datalisttype = "Dynamic";
         Ddo_funcaoapf_funapfpainom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_funapfpainom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapf_funapfpainom_Filtertype = "Character";
         Ddo_funcaoapf_funapfpainom_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_funapfpainom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_funapfpainom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_funapfpainom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_funapfpainom_Cls = "ColumnSettings";
         Ddo_funcaoapf_funapfpainom_Tooltip = "Op��es";
         Ddo_funcaoapf_funapfpainom_Caption = "";
         Ddo_funcaoapf_pf_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_pf_Rangefilterto = "At�";
         Ddo_funcaoapf_pf_Rangefilterfrom = "Desde";
         Ddo_funcaoapf_pf_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_pf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapf_pf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapf_pf_Filtertype = "Numeric";
         Ddo_funcaoapf_pf_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_pf_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_pf_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_pf_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_pf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_pf_Cls = "ColumnSettings";
         Ddo_funcaoapf_pf_Tooltip = "Op��es";
         Ddo_funcaoapf_pf_Caption = "";
         Ddo_funcaoapf_ar_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_ar_Rangefilterto = "At�";
         Ddo_funcaoapf_ar_Rangefilterfrom = "Desde";
         Ddo_funcaoapf_ar_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_ar_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapf_ar_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ar_Filtertype = "Numeric";
         Ddo_funcaoapf_ar_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_ar_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_ar_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_ar_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_ar_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_ar_Cls = "ColumnSettings";
         Ddo_funcaoapf_ar_Tooltip = "Op��es";
         Ddo_funcaoapf_ar_Caption = "";
         Ddo_funcaoapf_td_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_td_Rangefilterto = "At�";
         Ddo_funcaoapf_td_Rangefilterfrom = "Desde";
         Ddo_funcaoapf_td_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_td_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaoapf_td_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaoapf_td_Filtertype = "Numeric";
         Ddo_funcaoapf_td_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_td_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_td_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_td_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_td_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_td_Cls = "ColumnSettings";
         Ddo_funcaoapf_td_Tooltip = "Op��es";
         Ddo_funcaoapf_td_Caption = "";
         Ddo_funcaoapf_complexidade_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaoapf_complexidade_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_complexidade_Datalistfixedvalues = "E:,B:Baixa,M:M�dia,A:Alta";
         Ddo_funcaoapf_complexidade_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaoapf_complexidade_Datalisttype = "FixedValues";
         Ddo_funcaoapf_complexidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_complexidade_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaoapf_complexidade_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_complexidade_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_complexidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_complexidade_Cls = "ColumnSettings";
         Ddo_funcaoapf_complexidade_Tooltip = "Op��es";
         Ddo_funcaoapf_complexidade_Caption = "";
         Ddo_funcaoapf_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaoapf_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapf_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapf_tipo_Datalistfixedvalues = "EE:EE,SE:SE,CE:CE,NM:NM";
         Ddo_funcaoapf_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaoapf_tipo_Datalisttype = "FixedValues";
         Ddo_funcaoapf_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaoapf_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_tipo_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_tipo_Cls = "ColumnSettings";
         Ddo_funcaoapf_tipo_Tooltip = "Op��es";
         Ddo_funcaoapf_tipo_Caption = "";
         Ddo_funcaoapf_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapf_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapf_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapf_nome_Loadingdata = "Carregando dados...";
         Ddo_funcaoapf_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapf_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapf_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapf_nome_Datalistproc = "GetPromptFuncaoAPFFilterData";
         Ddo_funcaoapf_nome_Datalisttype = "Dynamic";
         Ddo_funcaoapf_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapf_nome_Filtertype = "Character";
         Ddo_funcaoapf_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapf_nome_Titlecontrolidtoreplace = "";
         Ddo_funcaoapf_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapf_nome_Cls = "ColumnSettings";
         Ddo_funcaoapf_nome_Tooltip = "Op��es";
         Ddo_funcaoapf_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Fun��es APF - An�lise de Ponto de Fun��o";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''}],oparms:[{av:'AV69FuncaoAPF_NomeTitleFilterData',fld:'vFUNCAOAPF_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV73FuncaoAPF_TipoTitleFilterData',fld:'vFUNCAOAPF_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV77FuncaoAPF_ComplexidadeTitleFilterData',fld:'vFUNCAOAPF_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV81FuncaoAPF_TDTitleFilterData',fld:'vFUNCAOAPF_TDTITLEFILTERDATA',pic:'',nv:null},{av:'AV85FuncaoAPF_ARTitleFilterData',fld:'vFUNCAOAPF_ARTITLEFILTERDATA',pic:'',nv:null},{av:'AV89FuncaoAPF_PFTitleFilterData',fld:'vFUNCAOAPF_PFTITLEFILTERDATA',pic:'',nv:null},{av:'AV93FuncaoAPF_FunAPFPaiNomTitleFilterData',fld:'vFUNCAOAPF_FUNAPFPAINOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV97FuncaoAPF_AtivoTitleFilterData',fld:'vFUNCAOAPF_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtFuncaoAPF_Nome_Titleformat',ctrl:'FUNCAOAPF_NOME',prop:'Titleformat'},{av:'edtFuncaoAPF_Nome_Title',ctrl:'FUNCAOAPF_NOME',prop:'Title'},{av:'cmbFuncaoAPF_Tipo'},{av:'cmbFuncaoAPF_Complexidade'},{av:'edtFuncaoAPF_TD_Titleformat',ctrl:'FUNCAOAPF_TD',prop:'Titleformat'},{av:'edtFuncaoAPF_TD_Title',ctrl:'FUNCAOAPF_TD',prop:'Title'},{av:'edtFuncaoAPF_AR_Titleformat',ctrl:'FUNCAOAPF_AR',prop:'Titleformat'},{av:'edtFuncaoAPF_AR_Title',ctrl:'FUNCAOAPF_AR',prop:'Title'},{av:'edtFuncaoAPF_PF_Titleformat',ctrl:'FUNCAOAPF_PF',prop:'Titleformat'},{av:'edtFuncaoAPF_PF_Title',ctrl:'FUNCAOAPF_PF',prop:'Title'},{av:'edtFuncaoAPF_FunAPFPaiNom_Titleformat',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Titleformat'},{av:'edtFuncaoAPF_FunAPFPaiNom_Title',ctrl:'FUNCAOAPF_FUNAPFPAINOM',prop:'Title'},{av:'cmbFuncaoAPF_Ativo'},{av:'AV103GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV104GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAOAPF_NOME.ONOPTIONCLICKED","{handler:'E12402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapf_nome_Activeeventkey',ctrl:'DDO_FUNCAOAPF_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_nome_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_NOME',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_nome_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_tipo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_funapfpainom_Sortedstatus',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SortedStatus'},{av:'Ddo_funcaoapf_ativo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPF_TIPO.ONOPTIONCLICKED","{handler:'E13402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapf_tipo_Activeeventkey',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_tipo_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapf_tipo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SortedStatus'},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapf_funapfpainom_Sortedstatus',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SortedStatus'},{av:'Ddo_funcaoapf_ativo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPF_COMPLEXIDADE.ONOPTIONCLICKED","{handler:'E14402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapf_complexidade_Activeeventkey',ctrl:'DDO_FUNCAOAPF_COMPLEXIDADE',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_complexidade_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_COMPLEXIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_FUNCAOAPF_TD.ONOPTIONCLICKED","{handler:'E15402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapf_td_Activeeventkey',ctrl:'DDO_FUNCAOAPF_TD',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_td_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_TD',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_td_Filteredtextto_get',ctrl:'DDO_FUNCAOAPF_TD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAOAPF_AR.ONOPTIONCLICKED","{handler:'E16402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapf_ar_Activeeventkey',ctrl:'DDO_FUNCAOAPF_AR',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_ar_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_AR',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_ar_Filteredtextto_get',ctrl:'DDO_FUNCAOAPF_AR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAOAPF_PF.ONOPTIONCLICKED","{handler:'E17402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapf_pf_Activeeventkey',ctrl:'DDO_FUNCAOAPF_PF',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_pf_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_pf_Filteredtextto_get',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_FUNCAOAPF_FUNAPFPAINOM.ONOPTIONCLICKED","{handler:'E18402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapf_funapfpainom_Activeeventkey',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_funapfpainom_Filteredtext_get',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'FilteredText_get'},{av:'Ddo_funcaoapf_funapfpainom_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapf_funapfpainom_Sortedstatus',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SortedStatus'},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapf_tipo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_ativo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPF_ATIVO.ONOPTIONCLICKED","{handler:'E19402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_funcaoapf_ativo_Activeeventkey',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_funcaoapf_ativo_Selectedvalue_get',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapf_ativo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SortedStatus'},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'Ddo_funcaoapf_nome_Sortedstatus',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SortedStatus'},{av:'Ddo_funcaoapf_tipo_Sortedstatus',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SortedStatus'},{av:'Ddo_funcaoapf_funapfpainom_Sortedstatus',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E32402',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E33402',iparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutFuncaoAPF_Codigo',fld:'vINOUTFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutFuncaoAPF_Nome',fld:'vINOUTFUNCAOAPF_NOME',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E20402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E25402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E21402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavFuncaoapf_sistemades2_Visible',ctrl:'vFUNCAOAPF_SISTEMADES2',prop:'Visible'},{av:'edtavFuncaoapf_modulonom2_Visible',ctrl:'vFUNCAOAPF_MODULONOM2',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom2_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavFuncaoapf_sistemades3_Visible',ctrl:'vFUNCAOAPF_SISTEMADES3',prop:'Visible'},{av:'edtavFuncaoapf_modulonom3_Visible',ctrl:'vFUNCAOAPF_MODULONOM3',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom3_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavFuncaoapf_sistemades1_Visible',ctrl:'vFUNCAOAPF_SISTEMADES1',prop:'Visible'},{av:'edtavFuncaoapf_modulonom1_Visible',ctrl:'vFUNCAOAPF_MODULONOM1',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom1_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E26402',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavFuncaoapf_sistemades1_Visible',ctrl:'vFUNCAOAPF_SISTEMADES1',prop:'Visible'},{av:'edtavFuncaoapf_modulonom1_Visible',ctrl:'vFUNCAOAPF_MODULONOM1',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom1_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E27402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E22402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavFuncaoapf_sistemades2_Visible',ctrl:'vFUNCAOAPF_SISTEMADES2',prop:'Visible'},{av:'edtavFuncaoapf_modulonom2_Visible',ctrl:'vFUNCAOAPF_MODULONOM2',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom2_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavFuncaoapf_sistemades3_Visible',ctrl:'vFUNCAOAPF_SISTEMADES3',prop:'Visible'},{av:'edtavFuncaoapf_modulonom3_Visible',ctrl:'vFUNCAOAPF_MODULONOM3',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom3_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavFuncaoapf_sistemades1_Visible',ctrl:'vFUNCAOAPF_SISTEMADES1',prop:'Visible'},{av:'edtavFuncaoapf_modulonom1_Visible',ctrl:'vFUNCAOAPF_MODULONOM1',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom1_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E28402',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavFuncaoapf_sistemades2_Visible',ctrl:'vFUNCAOAPF_SISTEMADES2',prop:'Visible'},{av:'edtavFuncaoapf_modulonom2_Visible',ctrl:'vFUNCAOAPF_MODULONOM2',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom2_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E23402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavFuncaoapf_sistemades2_Visible',ctrl:'vFUNCAOAPF_SISTEMADES2',prop:'Visible'},{av:'edtavFuncaoapf_modulonom2_Visible',ctrl:'vFUNCAOAPF_MODULONOM2',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom2_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavFuncaoapf_sistemades3_Visible',ctrl:'vFUNCAOAPF_SISTEMADES3',prop:'Visible'},{av:'edtavFuncaoapf_modulonom3_Visible',ctrl:'vFUNCAOAPF_MODULONOM3',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom3_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavFuncaoapf_sistemades1_Visible',ctrl:'vFUNCAOAPF_SISTEMADES1',prop:'Visible'},{av:'edtavFuncaoapf_modulonom1_Visible',ctrl:'vFUNCAOAPF_MODULONOM1',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom1_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E29402',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavFuncaoapf_sistemades3_Visible',ctrl:'vFUNCAOAPF_SISTEMADES3',prop:'Visible'},{av:'edtavFuncaoapf_modulonom3_Visible',ctrl:'vFUNCAOAPF_MODULONOM3',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom3_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E24402',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV84ddo_FuncaoAPF_TDTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_TDTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV88ddo_FuncaoAPF_ARTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV92ddo_FuncaoAPF_PFTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_FUNAPFPAINOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAOAPF_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV70TFFuncaoAPF_Nome',fld:'vTFFUNCAOAPF_NOME',pic:'',nv:''},{av:'Ddo_funcaoapf_nome_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_NOME',prop:'FilteredText_set'},{av:'AV71TFFuncaoAPF_Nome_Sel',fld:'vTFFUNCAOAPF_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_nome_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_NOME',prop:'SelectedValue_set'},{av:'AV75TFFuncaoAPF_Tipo_Sels',fld:'vTFFUNCAOAPF_TIPO_SELS',pic:'',nv:null},{av:'Ddo_funcaoapf_tipo_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_TIPO',prop:'SelectedValue_set'},{av:'AV79TFFuncaoAPF_Complexidade_Sels',fld:'vTFFUNCAOAPF_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'Ddo_funcaoapf_complexidade_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_COMPLEXIDADE',prop:'SelectedValue_set'},{av:'AV82TFFuncaoAPF_TD',fld:'vTFFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'Ddo_funcaoapf_td_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_TD',prop:'FilteredText_set'},{av:'AV83TFFuncaoAPF_TD_To',fld:'vTFFUNCAOAPF_TD_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaoapf_td_Filteredtextto_set',ctrl:'DDO_FUNCAOAPF_TD',prop:'FilteredTextTo_set'},{av:'AV86TFFuncaoAPF_AR',fld:'vTFFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'Ddo_funcaoapf_ar_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_AR',prop:'FilteredText_set'},{av:'AV87TFFuncaoAPF_AR_To',fld:'vTFFUNCAOAPF_AR_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaoapf_ar_Filteredtextto_set',ctrl:'DDO_FUNCAOAPF_AR',prop:'FilteredTextTo_set'},{av:'AV90TFFuncaoAPF_PF',fld:'vTFFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaoapf_pf_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredText_set'},{av:'AV91TFFuncaoAPF_PF_To',fld:'vTFFUNCAOAPF_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaoapf_pf_Filteredtextto_set',ctrl:'DDO_FUNCAOAPF_PF',prop:'FilteredTextTo_set'},{av:'AV94TFFuncaoAPF_FunAPFPaiNom',fld:'vTFFUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'Ddo_funcaoapf_funapfpainom_Filteredtext_set',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'FilteredText_set'},{av:'AV95TFFuncaoAPF_FunAPFPaiNom_Sel',fld:'vTFFUNCAOAPF_FUNAPFPAINOM_SEL',pic:'',nv:''},{av:'Ddo_funcaoapf_funapfpainom_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_FUNAPFPAINOM',prop:'SelectedValue_set'},{av:'AV99TFFuncaoAPF_Ativo_Sels',fld:'vTFFUNCAOAPF_ATIVO_SELS',pic:'',nv:null},{av:'Ddo_funcaoapf_ativo_Selectedvalue_set',ctrl:'DDO_FUNCAOAPF_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'edtavFuncaoapf_sistemades1_Visible',ctrl:'vFUNCAOAPF_SISTEMADES1',prop:'Visible'},{av:'edtavFuncaoapf_modulonom1_Visible',ctrl:'vFUNCAOAPF_MODULONOM1',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom1_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV60FuncaoAPF_SistemaDes1',fld:'vFUNCAOAPF_SISTEMADES1',pic:'@!',nv:''},{av:'AV61FuncaoAPF_ModuloNom1',fld:'vFUNCAOAPF_MODULONOM1',pic:'@!',nv:''},{av:'AV62FuncaoAPF_FunAPFPaiNom1',fld:'vFUNCAOAPF_FUNAPFPAINOM1',pic:'',nv:''},{av:'AV31FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV63FuncaoAPF_SistemaDes2',fld:'vFUNCAOAPF_SISTEMADES2',pic:'@!',nv:''},{av:'AV64FuncaoAPF_ModuloNom2',fld:'vFUNCAOAPF_MODULONOM2',pic:'@!',nv:''},{av:'AV65FuncaoAPF_FunAPFPaiNom2',fld:'vFUNCAOAPF_FUNAPFPAINOM2',pic:'',nv:''},{av:'AV34FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV66FuncaoAPF_SistemaDes3',fld:'vFUNCAOAPF_SISTEMADES3',pic:'@!',nv:''},{av:'AV67FuncaoAPF_ModuloNom3',fld:'vFUNCAOAPF_MODULONOM3',pic:'@!',nv:''},{av:'AV68FuncaoAPF_FunAPFPaiNom3',fld:'vFUNCAOAPF_FUNAPFPAINOM3',pic:'',nv:''},{av:'AV37FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'edtavFuncaoapf_sistemades2_Visible',ctrl:'vFUNCAOAPF_SISTEMADES2',prop:'Visible'},{av:'edtavFuncaoapf_modulonom2_Visible',ctrl:'vFUNCAOAPF_MODULONOM2',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom2_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'edtavFuncaoapf_sistemades3_Visible',ctrl:'vFUNCAOAPF_SISTEMADES3',prop:'Visible'},{av:'edtavFuncaoapf_modulonom3_Visible',ctrl:'vFUNCAOAPF_MODULONOM3',prop:'Visible'},{av:'edtavFuncaoapf_funapfpainom3_Visible',ctrl:'vFUNCAOAPF_FUNAPFPAINOM3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutFuncaoAPF_Nome = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcaoapf_nome_Activeeventkey = "";
         Ddo_funcaoapf_nome_Filteredtext_get = "";
         Ddo_funcaoapf_nome_Selectedvalue_get = "";
         Ddo_funcaoapf_tipo_Activeeventkey = "";
         Ddo_funcaoapf_tipo_Selectedvalue_get = "";
         Ddo_funcaoapf_complexidade_Activeeventkey = "";
         Ddo_funcaoapf_complexidade_Selectedvalue_get = "";
         Ddo_funcaoapf_td_Activeeventkey = "";
         Ddo_funcaoapf_td_Filteredtext_get = "";
         Ddo_funcaoapf_td_Filteredtextto_get = "";
         Ddo_funcaoapf_ar_Activeeventkey = "";
         Ddo_funcaoapf_ar_Filteredtext_get = "";
         Ddo_funcaoapf_ar_Filteredtextto_get = "";
         Ddo_funcaoapf_pf_Activeeventkey = "";
         Ddo_funcaoapf_pf_Filteredtext_get = "";
         Ddo_funcaoapf_pf_Filteredtextto_get = "";
         Ddo_funcaoapf_funapfpainom_Activeeventkey = "";
         Ddo_funcaoapf_funapfpainom_Filteredtext_get = "";
         Ddo_funcaoapf_funapfpainom_Selectedvalue_get = "";
         Ddo_funcaoapf_ativo_Activeeventkey = "";
         Ddo_funcaoapf_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17FuncaoAPF_Nome1 = "";
         AV60FuncaoAPF_SistemaDes1 = "";
         AV61FuncaoAPF_ModuloNom1 = "";
         AV62FuncaoAPF_FunAPFPaiNom1 = "";
         AV31FuncaoAPF_Tipo1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21FuncaoAPF_Nome2 = "";
         AV63FuncaoAPF_SistemaDes2 = "";
         AV64FuncaoAPF_ModuloNom2 = "";
         AV65FuncaoAPF_FunAPFPaiNom2 = "";
         AV34FuncaoAPF_Tipo2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25FuncaoAPF_Nome3 = "";
         AV66FuncaoAPF_SistemaDes3 = "";
         AV67FuncaoAPF_ModuloNom3 = "";
         AV68FuncaoAPF_FunAPFPaiNom3 = "";
         AV37FuncaoAPF_Tipo3 = "";
         AV70TFFuncaoAPF_Nome = "";
         AV71TFFuncaoAPF_Nome_Sel = "";
         AV94TFFuncaoAPF_FunAPFPaiNom = "";
         AV95TFFuncaoAPF_FunAPFPaiNom_Sel = "";
         AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace = "";
         AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace = "";
         AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace = "";
         AV84ddo_FuncaoAPF_TDTitleControlIdToReplace = "";
         AV88ddo_FuncaoAPF_ARTitleControlIdToReplace = "";
         AV92ddo_FuncaoAPF_PFTitleControlIdToReplace = "";
         AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace = "";
         AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace = "";
         AV75TFFuncaoAPF_Tipo_Sels = new GxSimpleCollection();
         AV79TFFuncaoAPF_Complexidade_Sels = new GxSimpleCollection();
         AV99TFFuncaoAPF_Ativo_Sels = new GxSimpleCollection();
         AV108Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV101DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV69FuncaoAPF_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73FuncaoAPF_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV77FuncaoAPF_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV81FuncaoAPF_TDTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV85FuncaoAPF_ARTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV89FuncaoAPF_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV93FuncaoAPF_FunAPFPaiNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV97FuncaoAPF_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaoapf_nome_Filteredtext_set = "";
         Ddo_funcaoapf_nome_Selectedvalue_set = "";
         Ddo_funcaoapf_nome_Sortedstatus = "";
         Ddo_funcaoapf_tipo_Selectedvalue_set = "";
         Ddo_funcaoapf_tipo_Sortedstatus = "";
         Ddo_funcaoapf_complexidade_Selectedvalue_set = "";
         Ddo_funcaoapf_td_Filteredtext_set = "";
         Ddo_funcaoapf_td_Filteredtextto_set = "";
         Ddo_funcaoapf_ar_Filteredtext_set = "";
         Ddo_funcaoapf_ar_Filteredtextto_set = "";
         Ddo_funcaoapf_pf_Filteredtext_set = "";
         Ddo_funcaoapf_pf_Filteredtextto_set = "";
         Ddo_funcaoapf_funapfpainom_Filteredtext_set = "";
         Ddo_funcaoapf_funapfpainom_Selectedvalue_set = "";
         Ddo_funcaoapf_funapfpainom_Sortedstatus = "";
         Ddo_funcaoapf_ativo_Selectedvalue_set = "";
         Ddo_funcaoapf_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV107Select_GXI = "";
         A166FuncaoAPF_Nome = "";
         A184FuncaoAPF_Tipo = "";
         A185FuncaoAPF_Complexidade = "";
         A363FuncaoAPF_FunAPFPaiNom = "";
         A183FuncaoAPF_Ativo = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17FuncaoAPF_Nome1 = "";
         lV60FuncaoAPF_SistemaDes1 = "";
         lV61FuncaoAPF_ModuloNom1 = "";
         lV62FuncaoAPF_FunAPFPaiNom1 = "";
         lV21FuncaoAPF_Nome2 = "";
         lV63FuncaoAPF_SistemaDes2 = "";
         lV64FuncaoAPF_ModuloNom2 = "";
         lV65FuncaoAPF_FunAPFPaiNom2 = "";
         lV25FuncaoAPF_Nome3 = "";
         lV66FuncaoAPF_SistemaDes3 = "";
         lV67FuncaoAPF_ModuloNom3 = "";
         lV68FuncaoAPF_FunAPFPaiNom3 = "";
         lV70TFFuncaoAPF_Nome = "";
         lV94TFFuncaoAPF_FunAPFPaiNom = "";
         A362FuncaoAPF_SistemaDes = "";
         A361FuncaoAPF_ModuloNom = "";
         H00402_A361FuncaoAPF_ModuloNom = new String[] {""} ;
         H00402_n361FuncaoAPF_ModuloNom = new bool[] {false} ;
         H00402_A362FuncaoAPF_SistemaDes = new String[] {""} ;
         H00402_n362FuncaoAPF_SistemaDes = new bool[] {false} ;
         H00402_A183FuncaoAPF_Ativo = new String[] {""} ;
         H00402_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         H00402_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         H00402_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         H00402_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         H00402_A359FuncaoAPF_ModuloCod = new int[1] ;
         H00402_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         H00402_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00402_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00402_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00402_A166FuncaoAPF_Nome = new String[] {""} ;
         H00402_A165FuncaoAPF_Codigo = new int[1] ;
         GXt_char1 = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons4 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV74TFFuncaoAPF_Tipo_SelsJson = "";
         AV78TFFuncaoAPF_Complexidade_SelsJson = "";
         AV98TFFuncaoAPF_Ativo_SelsJson = "";
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptfuncaoapf__default(),
            new Object[][] {
                new Object[] {
               H00402_A361FuncaoAPF_ModuloNom, H00402_n361FuncaoAPF_ModuloNom, H00402_A362FuncaoAPF_SistemaDes, H00402_n362FuncaoAPF_SistemaDes, H00402_A183FuncaoAPF_Ativo, H00402_A363FuncaoAPF_FunAPFPaiNom, H00402_n363FuncaoAPF_FunAPFPaiNom, H00402_A358FuncaoAPF_FunAPFPaiCod, H00402_n358FuncaoAPF_FunAPFPaiCod, H00402_A359FuncaoAPF_ModuloCod,
               H00402_n359FuncaoAPF_ModuloCod, H00402_A360FuncaoAPF_SistemaCod, H00402_n360FuncaoAPF_SistemaCod, H00402_A184FuncaoAPF_Tipo, H00402_A166FuncaoAPF_Nome, H00402_A165FuncaoAPF_Codigo
               }
            }
         );
         AV108Pgmname = "PromptFuncaoAPF";
         /* GeneXus formulas. */
         AV108Pgmname = "PromptFuncaoAPF";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_92 ;
      private short nGXsfl_92_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV82TFFuncaoAPF_TD ;
      private short AV83TFFuncaoAPF_TD_To ;
      private short AV86TFFuncaoAPF_AR ;
      private short AV87TFFuncaoAPF_AR_To ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A388FuncaoAPF_TD ;
      private short A387FuncaoAPF_AR ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_92_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short GXt_int2 ;
      private short edtFuncaoAPF_Nome_Titleformat ;
      private short cmbFuncaoAPF_Tipo_Titleformat ;
      private short cmbFuncaoAPF_Complexidade_Titleformat ;
      private short edtFuncaoAPF_TD_Titleformat ;
      private short edtFuncaoAPF_AR_Titleformat ;
      private short edtFuncaoAPF_PF_Titleformat ;
      private short edtFuncaoAPF_FunAPFPaiNom_Titleformat ;
      private short cmbFuncaoAPF_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutFuncaoAPF_Codigo ;
      private int wcpOAV7InOutFuncaoAPF_Codigo ;
      private int subGrid_Rows ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcaoapf_nome_Datalistupdateminimumcharacters ;
      private int Ddo_funcaoapf_funapfpainom_Datalistupdateminimumcharacters ;
      private int edtavTffuncaoapf_nome_Visible ;
      private int edtavTffuncaoapf_nome_sel_Visible ;
      private int edtavTffuncaoapf_td_Visible ;
      private int edtavTffuncaoapf_td_to_Visible ;
      private int edtavTffuncaoapf_ar_Visible ;
      private int edtavTffuncaoapf_ar_to_Visible ;
      private int edtavTffuncaoapf_pf_Visible ;
      private int edtavTffuncaoapf_pf_to_Visible ;
      private int edtavTffuncaoapf_funapfpainom_Visible ;
      private int edtavTffuncaoapf_funapfpainom_sel_Visible ;
      private int edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_artitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Visible ;
      private int A165FuncaoAPF_Codigo ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A359FuncaoAPF_ModuloCod ;
      private int A358FuncaoAPF_FunAPFPaiCod ;
      private int subGrid_Islastpage ;
      private int AV75TFFuncaoAPF_Tipo_Sels_Count ;
      private int AV99TFFuncaoAPF_Ativo_Sels_Count ;
      private int AV79TFFuncaoAPF_Complexidade_Sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int AV102PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavFuncaoapf_nome1_Visible ;
      private int edtavFuncaoapf_sistemades1_Visible ;
      private int edtavFuncaoapf_modulonom1_Visible ;
      private int edtavFuncaoapf_funapfpainom1_Visible ;
      private int edtavFuncaoapf_nome2_Visible ;
      private int edtavFuncaoapf_sistemades2_Visible ;
      private int edtavFuncaoapf_modulonom2_Visible ;
      private int edtavFuncaoapf_funapfpainom2_Visible ;
      private int edtavFuncaoapf_nome3_Visible ;
      private int edtavFuncaoapf_sistemades3_Visible ;
      private int edtavFuncaoapf_modulonom3_Visible ;
      private int edtavFuncaoapf_funapfpainom3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV103GridCurrentPage ;
      private long AV104GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV90TFFuncaoAPF_PF ;
      private decimal AV91TFFuncaoAPF_PF_To ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal3 ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcaoapf_nome_Activeeventkey ;
      private String Ddo_funcaoapf_nome_Filteredtext_get ;
      private String Ddo_funcaoapf_nome_Selectedvalue_get ;
      private String Ddo_funcaoapf_tipo_Activeeventkey ;
      private String Ddo_funcaoapf_tipo_Selectedvalue_get ;
      private String Ddo_funcaoapf_complexidade_Activeeventkey ;
      private String Ddo_funcaoapf_complexidade_Selectedvalue_get ;
      private String Ddo_funcaoapf_td_Activeeventkey ;
      private String Ddo_funcaoapf_td_Filteredtext_get ;
      private String Ddo_funcaoapf_td_Filteredtextto_get ;
      private String Ddo_funcaoapf_ar_Activeeventkey ;
      private String Ddo_funcaoapf_ar_Filteredtext_get ;
      private String Ddo_funcaoapf_ar_Filteredtextto_get ;
      private String Ddo_funcaoapf_pf_Activeeventkey ;
      private String Ddo_funcaoapf_pf_Filteredtext_get ;
      private String Ddo_funcaoapf_pf_Filteredtextto_get ;
      private String Ddo_funcaoapf_funapfpainom_Activeeventkey ;
      private String Ddo_funcaoapf_funapfpainom_Filteredtext_get ;
      private String Ddo_funcaoapf_funapfpainom_Selectedvalue_get ;
      private String Ddo_funcaoapf_ativo_Activeeventkey ;
      private String Ddo_funcaoapf_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_92_idx="0001" ;
      private String AV61FuncaoAPF_ModuloNom1 ;
      private String AV31FuncaoAPF_Tipo1 ;
      private String AV64FuncaoAPF_ModuloNom2 ;
      private String AV34FuncaoAPF_Tipo2 ;
      private String AV67FuncaoAPF_ModuloNom3 ;
      private String AV37FuncaoAPF_Tipo3 ;
      private String AV108Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcaoapf_nome_Caption ;
      private String Ddo_funcaoapf_nome_Tooltip ;
      private String Ddo_funcaoapf_nome_Cls ;
      private String Ddo_funcaoapf_nome_Filteredtext_set ;
      private String Ddo_funcaoapf_nome_Selectedvalue_set ;
      private String Ddo_funcaoapf_nome_Dropdownoptionstype ;
      private String Ddo_funcaoapf_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_nome_Sortedstatus ;
      private String Ddo_funcaoapf_nome_Filtertype ;
      private String Ddo_funcaoapf_nome_Datalisttype ;
      private String Ddo_funcaoapf_nome_Datalistproc ;
      private String Ddo_funcaoapf_nome_Sortasc ;
      private String Ddo_funcaoapf_nome_Sortdsc ;
      private String Ddo_funcaoapf_nome_Loadingdata ;
      private String Ddo_funcaoapf_nome_Cleanfilter ;
      private String Ddo_funcaoapf_nome_Noresultsfound ;
      private String Ddo_funcaoapf_nome_Searchbuttontext ;
      private String Ddo_funcaoapf_tipo_Caption ;
      private String Ddo_funcaoapf_tipo_Tooltip ;
      private String Ddo_funcaoapf_tipo_Cls ;
      private String Ddo_funcaoapf_tipo_Selectedvalue_set ;
      private String Ddo_funcaoapf_tipo_Dropdownoptionstype ;
      private String Ddo_funcaoapf_tipo_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_tipo_Sortedstatus ;
      private String Ddo_funcaoapf_tipo_Datalisttype ;
      private String Ddo_funcaoapf_tipo_Datalistfixedvalues ;
      private String Ddo_funcaoapf_tipo_Sortasc ;
      private String Ddo_funcaoapf_tipo_Sortdsc ;
      private String Ddo_funcaoapf_tipo_Cleanfilter ;
      private String Ddo_funcaoapf_tipo_Searchbuttontext ;
      private String Ddo_funcaoapf_complexidade_Caption ;
      private String Ddo_funcaoapf_complexidade_Tooltip ;
      private String Ddo_funcaoapf_complexidade_Cls ;
      private String Ddo_funcaoapf_complexidade_Selectedvalue_set ;
      private String Ddo_funcaoapf_complexidade_Dropdownoptionstype ;
      private String Ddo_funcaoapf_complexidade_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_complexidade_Datalisttype ;
      private String Ddo_funcaoapf_complexidade_Datalistfixedvalues ;
      private String Ddo_funcaoapf_complexidade_Cleanfilter ;
      private String Ddo_funcaoapf_complexidade_Searchbuttontext ;
      private String Ddo_funcaoapf_td_Caption ;
      private String Ddo_funcaoapf_td_Tooltip ;
      private String Ddo_funcaoapf_td_Cls ;
      private String Ddo_funcaoapf_td_Filteredtext_set ;
      private String Ddo_funcaoapf_td_Filteredtextto_set ;
      private String Ddo_funcaoapf_td_Dropdownoptionstype ;
      private String Ddo_funcaoapf_td_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_td_Filtertype ;
      private String Ddo_funcaoapf_td_Cleanfilter ;
      private String Ddo_funcaoapf_td_Rangefilterfrom ;
      private String Ddo_funcaoapf_td_Rangefilterto ;
      private String Ddo_funcaoapf_td_Searchbuttontext ;
      private String Ddo_funcaoapf_ar_Caption ;
      private String Ddo_funcaoapf_ar_Tooltip ;
      private String Ddo_funcaoapf_ar_Cls ;
      private String Ddo_funcaoapf_ar_Filteredtext_set ;
      private String Ddo_funcaoapf_ar_Filteredtextto_set ;
      private String Ddo_funcaoapf_ar_Dropdownoptionstype ;
      private String Ddo_funcaoapf_ar_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_ar_Filtertype ;
      private String Ddo_funcaoapf_ar_Cleanfilter ;
      private String Ddo_funcaoapf_ar_Rangefilterfrom ;
      private String Ddo_funcaoapf_ar_Rangefilterto ;
      private String Ddo_funcaoapf_ar_Searchbuttontext ;
      private String Ddo_funcaoapf_pf_Caption ;
      private String Ddo_funcaoapf_pf_Tooltip ;
      private String Ddo_funcaoapf_pf_Cls ;
      private String Ddo_funcaoapf_pf_Filteredtext_set ;
      private String Ddo_funcaoapf_pf_Filteredtextto_set ;
      private String Ddo_funcaoapf_pf_Dropdownoptionstype ;
      private String Ddo_funcaoapf_pf_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_pf_Filtertype ;
      private String Ddo_funcaoapf_pf_Cleanfilter ;
      private String Ddo_funcaoapf_pf_Rangefilterfrom ;
      private String Ddo_funcaoapf_pf_Rangefilterto ;
      private String Ddo_funcaoapf_pf_Searchbuttontext ;
      private String Ddo_funcaoapf_funapfpainom_Caption ;
      private String Ddo_funcaoapf_funapfpainom_Tooltip ;
      private String Ddo_funcaoapf_funapfpainom_Cls ;
      private String Ddo_funcaoapf_funapfpainom_Filteredtext_set ;
      private String Ddo_funcaoapf_funapfpainom_Selectedvalue_set ;
      private String Ddo_funcaoapf_funapfpainom_Dropdownoptionstype ;
      private String Ddo_funcaoapf_funapfpainom_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_funapfpainom_Sortedstatus ;
      private String Ddo_funcaoapf_funapfpainom_Filtertype ;
      private String Ddo_funcaoapf_funapfpainom_Datalisttype ;
      private String Ddo_funcaoapf_funapfpainom_Datalistproc ;
      private String Ddo_funcaoapf_funapfpainom_Sortasc ;
      private String Ddo_funcaoapf_funapfpainom_Sortdsc ;
      private String Ddo_funcaoapf_funapfpainom_Loadingdata ;
      private String Ddo_funcaoapf_funapfpainom_Cleanfilter ;
      private String Ddo_funcaoapf_funapfpainom_Noresultsfound ;
      private String Ddo_funcaoapf_funapfpainom_Searchbuttontext ;
      private String Ddo_funcaoapf_ativo_Caption ;
      private String Ddo_funcaoapf_ativo_Tooltip ;
      private String Ddo_funcaoapf_ativo_Cls ;
      private String Ddo_funcaoapf_ativo_Selectedvalue_set ;
      private String Ddo_funcaoapf_ativo_Dropdownoptionstype ;
      private String Ddo_funcaoapf_ativo_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapf_ativo_Sortedstatus ;
      private String Ddo_funcaoapf_ativo_Datalisttype ;
      private String Ddo_funcaoapf_ativo_Datalistfixedvalues ;
      private String Ddo_funcaoapf_ativo_Sortasc ;
      private String Ddo_funcaoapf_ativo_Sortdsc ;
      private String Ddo_funcaoapf_ativo_Cleanfilter ;
      private String Ddo_funcaoapf_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTffuncaoapf_nome_Internalname ;
      private String edtavTffuncaoapf_nome_sel_Internalname ;
      private String edtavTffuncaoapf_td_Internalname ;
      private String edtavTffuncaoapf_td_Jsonclick ;
      private String edtavTffuncaoapf_td_to_Internalname ;
      private String edtavTffuncaoapf_td_to_Jsonclick ;
      private String edtavTffuncaoapf_ar_Internalname ;
      private String edtavTffuncaoapf_ar_Jsonclick ;
      private String edtavTffuncaoapf_ar_to_Internalname ;
      private String edtavTffuncaoapf_ar_to_Jsonclick ;
      private String edtavTffuncaoapf_pf_Internalname ;
      private String edtavTffuncaoapf_pf_Jsonclick ;
      private String edtavTffuncaoapf_pf_to_Internalname ;
      private String edtavTffuncaoapf_pf_to_Jsonclick ;
      private String edtavTffuncaoapf_funapfpainom_Internalname ;
      private String edtavTffuncaoapf_funapfpainom_sel_Internalname ;
      private String edtavDdo_funcaoapf_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_complexidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_tdtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_artitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_pftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_funapfpainomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapf_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String cmbFuncaoAPF_Tipo_Internalname ;
      private String A184FuncaoAPF_Tipo ;
      private String cmbFuncaoAPF_Complexidade_Internalname ;
      private String A185FuncaoAPF_Complexidade ;
      private String edtFuncaoAPF_TD_Internalname ;
      private String edtFuncaoAPF_AR_Internalname ;
      private String edtFuncaoAPF_PF_Internalname ;
      private String edtFuncaoAPF_SistemaCod_Internalname ;
      private String edtFuncaoAPF_ModuloCod_Internalname ;
      private String edtFuncaoAPF_FunAPFPaiCod_Internalname ;
      private String edtFuncaoAPF_FunAPFPaiNom_Internalname ;
      private String cmbFuncaoAPF_Ativo_Internalname ;
      private String A183FuncaoAPF_Ativo ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV61FuncaoAPF_ModuloNom1 ;
      private String lV64FuncaoAPF_ModuloNom2 ;
      private String lV67FuncaoAPF_ModuloNom3 ;
      private String A361FuncaoAPF_ModuloNom ;
      private String GXt_char1 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavFuncaoapf_nome1_Internalname ;
      private String edtavFuncaoapf_sistemades1_Internalname ;
      private String edtavFuncaoapf_modulonom1_Internalname ;
      private String edtavFuncaoapf_funapfpainom1_Internalname ;
      private String cmbavFuncaoapf_tipo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavFuncaoapf_nome2_Internalname ;
      private String edtavFuncaoapf_sistemades2_Internalname ;
      private String edtavFuncaoapf_modulonom2_Internalname ;
      private String edtavFuncaoapf_funapfpainom2_Internalname ;
      private String cmbavFuncaoapf_tipo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavFuncaoapf_nome3_Internalname ;
      private String edtavFuncaoapf_sistemades3_Internalname ;
      private String edtavFuncaoapf_modulonom3_Internalname ;
      private String edtavFuncaoapf_funapfpainom3_Internalname ;
      private String cmbavFuncaoapf_tipo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_funcaoapf_nome_Internalname ;
      private String Ddo_funcaoapf_tipo_Internalname ;
      private String Ddo_funcaoapf_complexidade_Internalname ;
      private String Ddo_funcaoapf_td_Internalname ;
      private String Ddo_funcaoapf_ar_Internalname ;
      private String Ddo_funcaoapf_pf_Internalname ;
      private String Ddo_funcaoapf_funapfpainom_Internalname ;
      private String Ddo_funcaoapf_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtFuncaoAPF_Nome_Title ;
      private String edtFuncaoAPF_TD_Title ;
      private String edtFuncaoAPF_AR_Title ;
      private String edtFuncaoAPF_PF_Title ;
      private String edtFuncaoAPF_FunAPFPaiNom_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavFuncaoapf_sistemades3_Jsonclick ;
      private String edtavFuncaoapf_modulonom3_Jsonclick ;
      private String cmbavFuncaoapf_tipo3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavFuncaoapf_sistemades2_Jsonclick ;
      private String edtavFuncaoapf_modulonom2_Jsonclick ;
      private String cmbavFuncaoapf_tipo2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavFuncaoapf_sistemades1_Jsonclick ;
      private String edtavFuncaoapf_modulonom1_Jsonclick ;
      private String cmbavFuncaoapf_tipo1_Jsonclick ;
      private String sGXsfl_92_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtFuncaoAPF_Nome_Jsonclick ;
      private String cmbFuncaoAPF_Tipo_Jsonclick ;
      private String cmbFuncaoAPF_Complexidade_Jsonclick ;
      private String edtFuncaoAPF_TD_Jsonclick ;
      private String edtFuncaoAPF_AR_Jsonclick ;
      private String edtFuncaoAPF_PF_Jsonclick ;
      private String edtFuncaoAPF_SistemaCod_Jsonclick ;
      private String edtFuncaoAPF_ModuloCod_Jsonclick ;
      private String edtFuncaoAPF_FunAPFPaiCod_Jsonclick ;
      private String edtFuncaoAPF_FunAPFPaiNom_Jsonclick ;
      private String cmbFuncaoAPF_Ativo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcaoapf_nome_Includesortasc ;
      private bool Ddo_funcaoapf_nome_Includesortdsc ;
      private bool Ddo_funcaoapf_nome_Includefilter ;
      private bool Ddo_funcaoapf_nome_Filterisrange ;
      private bool Ddo_funcaoapf_nome_Includedatalist ;
      private bool Ddo_funcaoapf_tipo_Includesortasc ;
      private bool Ddo_funcaoapf_tipo_Includesortdsc ;
      private bool Ddo_funcaoapf_tipo_Includefilter ;
      private bool Ddo_funcaoapf_tipo_Includedatalist ;
      private bool Ddo_funcaoapf_tipo_Allowmultipleselection ;
      private bool Ddo_funcaoapf_complexidade_Includesortasc ;
      private bool Ddo_funcaoapf_complexidade_Includesortdsc ;
      private bool Ddo_funcaoapf_complexidade_Includefilter ;
      private bool Ddo_funcaoapf_complexidade_Includedatalist ;
      private bool Ddo_funcaoapf_complexidade_Allowmultipleselection ;
      private bool Ddo_funcaoapf_td_Includesortasc ;
      private bool Ddo_funcaoapf_td_Includesortdsc ;
      private bool Ddo_funcaoapf_td_Includefilter ;
      private bool Ddo_funcaoapf_td_Filterisrange ;
      private bool Ddo_funcaoapf_td_Includedatalist ;
      private bool Ddo_funcaoapf_ar_Includesortasc ;
      private bool Ddo_funcaoapf_ar_Includesortdsc ;
      private bool Ddo_funcaoapf_ar_Includefilter ;
      private bool Ddo_funcaoapf_ar_Filterisrange ;
      private bool Ddo_funcaoapf_ar_Includedatalist ;
      private bool Ddo_funcaoapf_pf_Includesortasc ;
      private bool Ddo_funcaoapf_pf_Includesortdsc ;
      private bool Ddo_funcaoapf_pf_Includefilter ;
      private bool Ddo_funcaoapf_pf_Filterisrange ;
      private bool Ddo_funcaoapf_pf_Includedatalist ;
      private bool Ddo_funcaoapf_funapfpainom_Includesortasc ;
      private bool Ddo_funcaoapf_funapfpainom_Includesortdsc ;
      private bool Ddo_funcaoapf_funapfpainom_Includefilter ;
      private bool Ddo_funcaoapf_funapfpainom_Filterisrange ;
      private bool Ddo_funcaoapf_funapfpainom_Includedatalist ;
      private bool Ddo_funcaoapf_ativo_Includesortasc ;
      private bool Ddo_funcaoapf_ativo_Includesortdsc ;
      private bool Ddo_funcaoapf_ativo_Includefilter ;
      private bool Ddo_funcaoapf_ativo_Includedatalist ;
      private bool Ddo_funcaoapf_ativo_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool n359FuncaoAPF_ModuloCod ;
      private bool n358FuncaoAPF_FunAPFPaiCod ;
      private bool n363FuncaoAPF_FunAPFPaiNom ;
      private bool n361FuncaoAPF_ModuloNom ;
      private bool n362FuncaoAPF_SistemaDes ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String AV74TFFuncaoAPF_Tipo_SelsJson ;
      private String AV78TFFuncaoAPF_Complexidade_SelsJson ;
      private String AV98TFFuncaoAPF_Ativo_SelsJson ;
      private String AV8InOutFuncaoAPF_Nome ;
      private String wcpOAV8InOutFuncaoAPF_Nome ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17FuncaoAPF_Nome1 ;
      private String AV60FuncaoAPF_SistemaDes1 ;
      private String AV62FuncaoAPF_FunAPFPaiNom1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21FuncaoAPF_Nome2 ;
      private String AV63FuncaoAPF_SistemaDes2 ;
      private String AV65FuncaoAPF_FunAPFPaiNom2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25FuncaoAPF_Nome3 ;
      private String AV66FuncaoAPF_SistemaDes3 ;
      private String AV68FuncaoAPF_FunAPFPaiNom3 ;
      private String AV70TFFuncaoAPF_Nome ;
      private String AV71TFFuncaoAPF_Nome_Sel ;
      private String AV94TFFuncaoAPF_FunAPFPaiNom ;
      private String AV95TFFuncaoAPF_FunAPFPaiNom_Sel ;
      private String AV72ddo_FuncaoAPF_NomeTitleControlIdToReplace ;
      private String AV76ddo_FuncaoAPF_TipoTitleControlIdToReplace ;
      private String AV80ddo_FuncaoAPF_ComplexidadeTitleControlIdToReplace ;
      private String AV84ddo_FuncaoAPF_TDTitleControlIdToReplace ;
      private String AV88ddo_FuncaoAPF_ARTitleControlIdToReplace ;
      private String AV92ddo_FuncaoAPF_PFTitleControlIdToReplace ;
      private String AV96ddo_FuncaoAPF_FunAPFPaiNomTitleControlIdToReplace ;
      private String AV100ddo_FuncaoAPF_AtivoTitleControlIdToReplace ;
      private String AV107Select_GXI ;
      private String A166FuncaoAPF_Nome ;
      private String A363FuncaoAPF_FunAPFPaiNom ;
      private String lV17FuncaoAPF_Nome1 ;
      private String lV60FuncaoAPF_SistemaDes1 ;
      private String lV62FuncaoAPF_FunAPFPaiNom1 ;
      private String lV21FuncaoAPF_Nome2 ;
      private String lV63FuncaoAPF_SistemaDes2 ;
      private String lV65FuncaoAPF_FunAPFPaiNom2 ;
      private String lV25FuncaoAPF_Nome3 ;
      private String lV66FuncaoAPF_SistemaDes3 ;
      private String lV68FuncaoAPF_FunAPFPaiNom3 ;
      private String lV70TFFuncaoAPF_Nome ;
      private String lV94TFFuncaoAPF_FunAPFPaiNom ;
      private String A362FuncaoAPF_SistemaDes ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutFuncaoAPF_Codigo ;
      private String aP1_InOutFuncaoAPF_Nome ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavFuncaoapf_tipo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavFuncaoapf_tipo2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbavFuncaoapf_tipo3 ;
      private GXCombobox cmbFuncaoAPF_Tipo ;
      private GXCombobox cmbFuncaoAPF_Complexidade ;
      private GXCombobox cmbFuncaoAPF_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00402_A361FuncaoAPF_ModuloNom ;
      private bool[] H00402_n361FuncaoAPF_ModuloNom ;
      private String[] H00402_A362FuncaoAPF_SistemaDes ;
      private bool[] H00402_n362FuncaoAPF_SistemaDes ;
      private String[] H00402_A183FuncaoAPF_Ativo ;
      private String[] H00402_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] H00402_n363FuncaoAPF_FunAPFPaiNom ;
      private int[] H00402_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] H00402_n358FuncaoAPF_FunAPFPaiCod ;
      private int[] H00402_A359FuncaoAPF_ModuloCod ;
      private bool[] H00402_n359FuncaoAPF_ModuloCod ;
      private int[] H00402_A360FuncaoAPF_SistemaCod ;
      private bool[] H00402_n360FuncaoAPF_SistemaCod ;
      private String[] H00402_A184FuncaoAPF_Tipo ;
      private String[] H00402_A166FuncaoAPF_Nome ;
      private int[] H00402_A165FuncaoAPF_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV75TFFuncaoAPF_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV79TFFuncaoAPF_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV99TFFuncaoAPF_Ativo_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV69FuncaoAPF_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73FuncaoAPF_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV77FuncaoAPF_ComplexidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV81FuncaoAPF_TDTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV85FuncaoAPF_ARTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV89FuncaoAPF_PFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV93FuncaoAPF_FunAPFPaiNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV97FuncaoAPF_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV101DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons4 ;
   }

   public class promptfuncaoapf__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00402( IGxContext context ,
                                             String A185FuncaoAPF_Complexidade ,
                                             IGxCollection AV79TFFuncaoAPF_Complexidade_Sels ,
                                             String A184FuncaoAPF_Tipo ,
                                             IGxCollection AV75TFFuncaoAPF_Tipo_Sels ,
                                             String A183FuncaoAPF_Ativo ,
                                             IGxCollection AV99TFFuncaoAPF_Ativo_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17FuncaoAPF_Nome1 ,
                                             String AV60FuncaoAPF_SistemaDes1 ,
                                             String AV61FuncaoAPF_ModuloNom1 ,
                                             String AV62FuncaoAPF_FunAPFPaiNom1 ,
                                             String AV31FuncaoAPF_Tipo1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21FuncaoAPF_Nome2 ,
                                             String AV63FuncaoAPF_SistemaDes2 ,
                                             String AV64FuncaoAPF_ModuloNom2 ,
                                             String AV65FuncaoAPF_FunAPFPaiNom2 ,
                                             String AV34FuncaoAPF_Tipo2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25FuncaoAPF_Nome3 ,
                                             String AV66FuncaoAPF_SistemaDes3 ,
                                             String AV67FuncaoAPF_ModuloNom3 ,
                                             String AV68FuncaoAPF_FunAPFPaiNom3 ,
                                             String AV37FuncaoAPF_Tipo3 ,
                                             String AV71TFFuncaoAPF_Nome_Sel ,
                                             String AV70TFFuncaoAPF_Nome ,
                                             int AV75TFFuncaoAPF_Tipo_Sels_Count ,
                                             String AV95TFFuncaoAPF_FunAPFPaiNom_Sel ,
                                             String AV94TFFuncaoAPF_FunAPFPaiNom ,
                                             int AV99TFFuncaoAPF_Ativo_Sels_Count ,
                                             String A166FuncaoAPF_Nome ,
                                             String A362FuncaoAPF_SistemaDes ,
                                             String A361FuncaoAPF_ModuloNom ,
                                             String A363FuncaoAPF_FunAPFPaiNom ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int AV79TFFuncaoAPF_Complexidade_Sels_Count ,
                                             short AV82TFFuncaoAPF_TD ,
                                             short A388FuncaoAPF_TD ,
                                             short AV83TFFuncaoAPF_TD_To ,
                                             short AV86TFFuncaoAPF_AR ,
                                             short A387FuncaoAPF_AR ,
                                             short AV87TFFuncaoAPF_AR_To ,
                                             decimal AV90TFFuncaoAPF_PF ,
                                             decimal A386FuncaoAPF_PF ,
                                             decimal AV91TFFuncaoAPF_PF_To )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [32] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T3.[Modulo_Nome] AS FuncaoAPF_ModuloNom, T4.[Sistema_Nome] AS FuncaoAPF_SistemaDes, T1.[FuncaoAPF_Ativo], T2.[FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom, T1.[FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, T1.[FuncaoAPF_ModuloCod] AS FuncaoAPF_ModuloCod, T1.[FuncaoAPF_SistemaCod] AS FuncaoAPF_SistemaCod, T1.[FuncaoAPF_Tipo], T1.[FuncaoAPF_Nome], T1.[FuncaoAPF_Codigo] FROM ((([FuncoesAPF] T1 WITH (NOLOCK) LEFT JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_FunAPFPaiCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = T1.[FuncaoAPF_ModuloCod]) LEFT JOIN [Sistema] T4 WITH (NOLOCK) ON T4.[Sistema_Codigo] = T1.[FuncaoAPF_SistemaCod])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV17FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV17FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17FuncaoAPF_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like '%' + @lV17FuncaoAPF_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like '%' + @lV17FuncaoAPF_Nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60FuncaoAPF_SistemaDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Sistema_Nome] like @lV60FuncaoAPF_SistemaDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Sistema_Nome] like @lV60FuncaoAPF_SistemaDes1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60FuncaoAPF_SistemaDes1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Sistema_Nome] like '%' + @lV60FuncaoAPF_SistemaDes1)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Sistema_Nome] like '%' + @lV60FuncaoAPF_SistemaDes1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61FuncaoAPF_ModuloNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV61FuncaoAPF_ModuloNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like @lV61FuncaoAPF_ModuloNom1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61FuncaoAPF_ModuloNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV61FuncaoAPF_ModuloNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like '%' + @lV61FuncaoAPF_ModuloNom1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62FuncaoAPF_FunAPFPaiNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV62FuncaoAPF_FunAPFPaiNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV62FuncaoAPF_FunAPFPaiNom1)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62FuncaoAPF_FunAPFPaiNom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV62FuncaoAPF_FunAPFPaiNom1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like '%' + @lV62FuncaoAPF_FunAPFPaiNom1)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31FuncaoAPF_Tipo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV31FuncaoAPF_Tipo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV31FuncaoAPF_Tipo1)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV21FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV21FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21FuncaoAPF_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like '%' + @lV21FuncaoAPF_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like '%' + @lV21FuncaoAPF_Nome2)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63FuncaoAPF_SistemaDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Sistema_Nome] like @lV63FuncaoAPF_SistemaDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Sistema_Nome] like @lV63FuncaoAPF_SistemaDes2)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63FuncaoAPF_SistemaDes2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Sistema_Nome] like '%' + @lV63FuncaoAPF_SistemaDes2)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Sistema_Nome] like '%' + @lV63FuncaoAPF_SistemaDes2)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64FuncaoAPF_ModuloNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV64FuncaoAPF_ModuloNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like @lV64FuncaoAPF_ModuloNom2)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64FuncaoAPF_ModuloNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV64FuncaoAPF_ModuloNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like '%' + @lV64FuncaoAPF_ModuloNom2)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV65FuncaoAPF_FunAPFPaiNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV65FuncaoAPF_FunAPFPaiNom2)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65FuncaoAPF_FunAPFPaiNom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV65FuncaoAPF_FunAPFPaiNom2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like '%' + @lV65FuncaoAPF_FunAPFPaiNom2)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34FuncaoAPF_Tipo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV34FuncaoAPF_Tipo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV34FuncaoAPF_Tipo2)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV25FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV25FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25FuncaoAPF_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like '%' + @lV25FuncaoAPF_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like '%' + @lV25FuncaoAPF_Nome3)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66FuncaoAPF_SistemaDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Sistema_Nome] like @lV66FuncaoAPF_SistemaDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Sistema_Nome] like @lV66FuncaoAPF_SistemaDes3)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_SISTEMADES") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66FuncaoAPF_SistemaDes3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Sistema_Nome] like '%' + @lV66FuncaoAPF_SistemaDes3)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Sistema_Nome] like '%' + @lV66FuncaoAPF_SistemaDes3)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67FuncaoAPF_ModuloNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like @lV67FuncaoAPF_ModuloNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like @lV67FuncaoAPF_ModuloNom3)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_MODULONOM") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67FuncaoAPF_ModuloNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Modulo_Nome] like '%' + @lV67FuncaoAPF_ModuloNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Modulo_Nome] like '%' + @lV67FuncaoAPF_ModuloNom3)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68FuncaoAPF_FunAPFPaiNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV68FuncaoAPF_FunAPFPaiNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV68FuncaoAPF_FunAPFPaiNom3)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_FUNAPFPAINOM") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68FuncaoAPF_FunAPFPaiNom3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like '%' + @lV68FuncaoAPF_FunAPFPaiNom3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like '%' + @lV68FuncaoAPF_FunAPFPaiNom3)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37FuncaoAPF_Tipo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV37FuncaoAPF_Tipo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV37FuncaoAPF_Tipo3)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71TFFuncaoAPF_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFFuncaoAPF_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV70TFFuncaoAPF_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV70TFFuncaoAPF_Nome)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71TFFuncaoAPF_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] = @AV71TFFuncaoAPF_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] = @AV71TFFuncaoAPF_Nome_Sel)";
            }
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( AV75TFFuncaoAPF_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV75TFFuncaoAPF_Tipo_Sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV75TFFuncaoAPF_Tipo_Sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV95TFFuncaoAPF_FunAPFPaiNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94TFFuncaoAPF_FunAPFPaiNom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV94TFFuncaoAPF_FunAPFPaiNom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV94TFFuncaoAPF_FunAPFPaiNom)";
            }
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95TFFuncaoAPF_FunAPFPaiNom_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] = @AV95TFFuncaoAPF_FunAPFPaiNom_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] = @AV95TFFuncaoAPF_FunAPFPaiNom_Sel)";
            }
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( AV99TFFuncaoAPF_Ativo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV99TFFuncaoAPF_Ativo_Sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV99TFFuncaoAPF_Ativo_Sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Tipo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[FuncaoAPF_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[FuncaoAPF_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Ativo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Ativo] DESC";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00402(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (String)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (String)dynConstraints[22] , (short)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (int)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (short)dynConstraints[39] , (bool)dynConstraints[40] , (int)dynConstraints[41] , (short)dynConstraints[42] , (short)dynConstraints[43] , (short)dynConstraints[44] , (short)dynConstraints[45] , (short)dynConstraints[46] , (short)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (decimal)dynConstraints[50] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00402 ;
          prmH00402 = new Object[] {
          new Object[] {"@AV79TFFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV17FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV17FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV60FuncaoAPF_SistemaDes1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV60FuncaoAPF_SistemaDes1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV61FuncaoAPF_ModuloNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV61FuncaoAPF_ModuloNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62FuncaoAPF_FunAPFPaiNom1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV62FuncaoAPF_FunAPFPaiNom1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV31FuncaoAPF_Tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV21FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV21FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV63FuncaoAPF_SistemaDes2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV63FuncaoAPF_SistemaDes2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV64FuncaoAPF_ModuloNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV64FuncaoAPF_ModuloNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65FuncaoAPF_FunAPFPaiNom2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV65FuncaoAPF_FunAPFPaiNom2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV34FuncaoAPF_Tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@lV25FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV25FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV66FuncaoAPF_SistemaDes3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV66FuncaoAPF_SistemaDes3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV67FuncaoAPF_ModuloNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67FuncaoAPF_ModuloNom3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68FuncaoAPF_FunAPFPaiNom3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV68FuncaoAPF_FunAPFPaiNom3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV37FuncaoAPF_Tipo3",SqlDbType.Char,3,0} ,
          new Object[] {"@lV70TFFuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV71TFFuncaoAPF_Nome_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV94TFFuncaoAPF_FunAPFPaiNom",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV95TFFuncaoAPF_FunAPFPaiNom_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00402", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00402,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getString(8, 3) ;
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[63]);
                }
                return;
       }
    }

 }

}
