/*
               File: PRC_GetContratoServicosTelas_Sequencia
        Description: Get Contrato Servicos Telas_Sequencia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:30.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getcontratoservicostelas_sequencia : GXProcedure
   {
      public prc_getcontratoservicostelas_sequencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getcontratoservicostelas_sequencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicosTelas_ContratoCod ,
                           out short aP1_Sequencial )
      {
         this.AV9ContratoServicosTelas_ContratoCod = aP0_ContratoServicosTelas_ContratoCod;
         this.AV8Sequencial = 0 ;
         initialize();
         executePrivate();
         aP1_Sequencial=this.AV8Sequencial;
      }

      public short executeUdp( int aP0_ContratoServicosTelas_ContratoCod )
      {
         this.AV9ContratoServicosTelas_ContratoCod = aP0_ContratoServicosTelas_ContratoCod;
         this.AV8Sequencial = 0 ;
         initialize();
         executePrivate();
         aP1_Sequencial=this.AV8Sequencial;
         return AV8Sequencial ;
      }

      public void executeSubmit( int aP0_ContratoServicosTelas_ContratoCod ,
                                 out short aP1_Sequencial )
      {
         prc_getcontratoservicostelas_sequencia objprc_getcontratoservicostelas_sequencia;
         objprc_getcontratoservicostelas_sequencia = new prc_getcontratoservicostelas_sequencia();
         objprc_getcontratoservicostelas_sequencia.AV9ContratoServicosTelas_ContratoCod = aP0_ContratoServicosTelas_ContratoCod;
         objprc_getcontratoservicostelas_sequencia.AV8Sequencial = 0 ;
         objprc_getcontratoservicostelas_sequencia.context.SetSubmitInitialConfig(context);
         objprc_getcontratoservicostelas_sequencia.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getcontratoservicostelas_sequencia);
         aP1_Sequencial=this.AV8Sequencial;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getcontratoservicostelas_sequencia)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P006G3 */
         pr_default.execute(0, new Object[] {AV9ContratoServicosTelas_ContratoCod});
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000GXC1 = P006G3_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(0);
         AV8Sequencial = (short)(A40000GXC1+1);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P006G3_A40000GXC1 = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getcontratoservicostelas_sequencia__default(),
            new Object[][] {
                new Object[] {
               P006G3_A40000GXC1
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8Sequencial ;
      private short A40000GXC1 ;
      private int AV9ContratoServicosTelas_ContratoCod ;
      private String scmdbuf ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P006G3_A40000GXC1 ;
      private short aP1_Sequencial ;
   }

   public class prc_getcontratoservicostelas_sequencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006G3 ;
          prmP006G3 = new Object[] {
          new Object[] {"@AV9ContratoServicosTelas_ContratoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006G3", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT MAX([ContratoServicosTelas_Sequencial]) AS GXC1 FROM [ContratoServicosTelas] WITH (NOLOCK) WHERE [ContratoServicosTelas_ContratoCod] = @AV9ContratoServicosTelas_ContratoCod ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006G3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
