/*
               File: ParametrosPlanilhasGeneral
        Description: Parametros Planilhas General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:23:28.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class parametrosplanilhasgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public parametrosplanilhasgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public parametrosplanilhasgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ParametrosPln_Codigo )
      {
         this.A848ParametrosPln_Codigo = aP0_ParametrosPln_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         dynParametrosPln_AreaTrabalhoCod = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A848ParametrosPln_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A848ParametrosPln_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"PARAMETROSPLN_AREATRABALHOCOD") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLAPARAMETROSPLN_AREATRABALHOCODF22( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAF22( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ParametrosPlanilhasGeneral";
               context.Gx_err = 0;
               GXAPARAMETROSPLN_AREATRABALHOCOD_htmlF22( ) ;
               WSF22( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Parametros Planilhas General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117232859");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("parametrosplanilhasgeneral.aspx") + "?" + UrlEncode("" +A848ParametrosPln_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA848ParametrosPln_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSPLN_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A847ParametrosPln_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSPLN_ABA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2015ParametrosPln_Aba, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSPLN_CAMPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A849ParametrosPln_Campo, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSPLN_COLUNA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A850ParametrosPln_Coluna, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PARAMETROSPLN_LINHA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A851ParametrosPln_Linha), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormF22( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("parametrosplanilhasgeneral.js", "?20203117232861");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ParametrosPlanilhasGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Parametros Planilhas General" ;
      }

      protected void WBF20( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "parametrosplanilhasgeneral.aspx");
            }
            wb_table1_2_F22( true) ;
         }
         else
         {
            wb_table1_2_F22( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_F22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosPln_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A848ParametrosPln_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A848ParametrosPln_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosPln_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtParametrosPln_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ParametrosPlanilhasGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTF22( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Parametros Planilhas General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPF20( ) ;
            }
         }
      }

      protected void WSF22( )
      {
         STARTF22( ) ;
         EVTF22( ) ;
      }

      protected void EVTF22( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPF20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPF20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11F22 */
                                    E11F22 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPF20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12F22 */
                                    E12F22 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPF20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13F22 */
                                    E13F22 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPF20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14F22 */
                                    E14F22 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPF20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPF20( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEF22( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormF22( ) ;
            }
         }
      }

      protected void PAF22( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            dynParametrosPln_AreaTrabalhoCod.Name = "PARAMETROSPLN_AREATRABALHOCOD";
            dynParametrosPln_AreaTrabalhoCod.WebTags = "";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLAPARAMETROSPLN_AREATRABALHOCODF22( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAPARAMETROSPLN_AREATRABALHOCOD_dataF22( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAPARAMETROSPLN_AREATRABALHOCOD_htmlF22( )
      {
         int gxdynajaxvalue ;
         GXDLAPARAMETROSPLN_AREATRABALHOCOD_dataF22( ) ;
         gxdynajaxindex = 1;
         dynParametrosPln_AreaTrabalhoCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynParametrosPln_AreaTrabalhoCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynParametrosPln_AreaTrabalhoCod.ItemCount > 0 )
         {
            A847ParametrosPln_AreaTrabalhoCod = (int)(NumberUtil.Val( dynParametrosPln_AreaTrabalhoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0))), "."));
            n847ParametrosPln_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A847ParametrosPln_AreaTrabalhoCod), "ZZZZZ9")));
         }
      }

      protected void GXDLAPARAMETROSPLN_AREATRABALHOCOD_dataF22( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Todas");
         /* Using cursor H00F22 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00F22_A847ParametrosPln_AreaTrabalhoCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00F22_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynParametrosPln_AreaTrabalhoCod.ItemCount > 0 )
         {
            A847ParametrosPln_AreaTrabalhoCod = (int)(NumberUtil.Val( dynParametrosPln_AreaTrabalhoCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0))), "."));
            n847ParametrosPln_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A847ParametrosPln_AreaTrabalhoCod), "ZZZZZ9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFF22( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ParametrosPlanilhasGeneral";
         context.Gx_err = 0;
      }

      protected void RFF22( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00F23 */
            pr_default.execute(1, new Object[] {A848ParametrosPln_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A851ParametrosPln_Linha = H00F23_A851ParametrosPln_Linha[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A851ParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(A851ParametrosPln_Linha), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_LINHA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A851ParametrosPln_Linha), "ZZZZZ9")));
               A850ParametrosPln_Coluna = H00F23_A850ParametrosPln_Coluna[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A850ParametrosPln_Coluna", A850ParametrosPln_Coluna);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_COLUNA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A850ParametrosPln_Coluna, "@!"))));
               A849ParametrosPln_Campo = H00F23_A849ParametrosPln_Campo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A849ParametrosPln_Campo", A849ParametrosPln_Campo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_CAMPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A849ParametrosPln_Campo, "@!"))));
               A2015ParametrosPln_Aba = H00F23_A2015ParametrosPln_Aba[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2015ParametrosPln_Aba", A2015ParametrosPln_Aba);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_ABA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2015ParametrosPln_Aba, ""))));
               A847ParametrosPln_AreaTrabalhoCod = H00F23_A847ParametrosPln_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A847ParametrosPln_AreaTrabalhoCod), "ZZZZZ9")));
               n847ParametrosPln_AreaTrabalhoCod = H00F23_n847ParametrosPln_AreaTrabalhoCod[0];
               GXAPARAMETROSPLN_AREATRABALHOCOD_htmlF22( ) ;
               /* Execute user event: E12F22 */
               E12F22 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
            WBF20( ) ;
         }
      }

      protected void STRUPF20( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ParametrosPlanilhasGeneral";
         context.Gx_err = 0;
         GXAPARAMETROSPLN_AREATRABALHOCOD_htmlF22( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11F22 */
         E11F22 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynParametrosPln_AreaTrabalhoCod.CurrentValue = cgiGet( dynParametrosPln_AreaTrabalhoCod_Internalname);
            A847ParametrosPln_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynParametrosPln_AreaTrabalhoCod_Internalname), "."));
            n847ParametrosPln_AreaTrabalhoCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A847ParametrosPln_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_AREATRABALHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A847ParametrosPln_AreaTrabalhoCod), "ZZZZZ9")));
            A2015ParametrosPln_Aba = cgiGet( edtParametrosPln_Aba_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2015ParametrosPln_Aba", A2015ParametrosPln_Aba);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_ABA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A2015ParametrosPln_Aba, ""))));
            A849ParametrosPln_Campo = StringUtil.Upper( cgiGet( edtParametrosPln_Campo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A849ParametrosPln_Campo", A849ParametrosPln_Campo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_CAMPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A849ParametrosPln_Campo, "@!"))));
            A850ParametrosPln_Coluna = StringUtil.Upper( cgiGet( edtParametrosPln_Coluna_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A850ParametrosPln_Coluna", A850ParametrosPln_Coluna);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_COLUNA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A850ParametrosPln_Coluna, "@!"))));
            A851ParametrosPln_Linha = (int)(context.localUtil.CToN( cgiGet( edtParametrosPln_Linha_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A851ParametrosPln_Linha", StringUtil.LTrim( StringUtil.Str( (decimal)(A851ParametrosPln_Linha), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PARAMETROSPLN_LINHA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A851ParametrosPln_Linha), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA848ParametrosPln_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA848ParametrosPln_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXAPARAMETROSPLN_AREATRABALHOCOD_htmlF22( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11F22 */
         E11F22 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11F22( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12F22( )
      {
         /* Load Routine */
         edtParametrosPln_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtParametrosPln_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtParametrosPln_Codigo_Visible), 5, 0)));
      }

      protected void E13F22( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("parametrosplanilhas.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A848ParametrosPln_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14F22( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("parametrosplanilhas.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A848ParametrosPln_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ParametrosPlanilhas";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ParametrosPln_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ParametrosPln_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_F22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_F22( true) ;
         }
         else
         {
            wb_table2_8_F22( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_F22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_36_F22( true) ;
         }
         else
         {
            wb_table3_36_F22( false) ;
         }
         return  ;
      }

      protected void wb_table3_36_F22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_F22e( true) ;
         }
         else
         {
            wb_table1_2_F22e( false) ;
         }
      }

      protected void wb_table3_36_F22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosPlanilhasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ParametrosPlanilhasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_F22e( true) ;
         }
         else
         {
            wb_table3_36_F22e( false) ;
         }
      }

      protected void wb_table2_8_F22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrospln_areatrabalhocod_Internalname, "�rea", "", "", lblTextblockparametrospln_areatrabalhocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosPlanilhasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynParametrosPln_AreaTrabalhoCod, dynParametrosPln_AreaTrabalhoCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0)), 1, dynParametrosPln_AreaTrabalhoCod_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ParametrosPlanilhasGeneral.htm");
            dynParametrosPln_AreaTrabalhoCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynParametrosPln_AreaTrabalhoCod_Internalname, "Values", (String)(dynParametrosPln_AreaTrabalhoCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrospln_aba_Internalname, "Aba", "", "", lblTextblockparametrospln_aba_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosPlanilhasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosPln_Aba_Internalname, StringUtil.RTrim( A2015ParametrosPln_Aba), StringUtil.RTrim( context.localUtil.Format( A2015ParametrosPln_Aba, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosPln_Aba_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ParametrosPlanilhasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrospln_campo_Internalname, "Campo", "", "", lblTextblockparametrospln_campo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosPlanilhasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosPln_Campo_Internalname, A849ParametrosPln_Campo, StringUtil.RTrim( context.localUtil.Format( A849ParametrosPln_Campo, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosPln_Campo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ParametrosPlanilhasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrospln_coluna_Internalname, "Coluna", "", "", lblTextblockparametrospln_coluna_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosPlanilhasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosPln_Coluna_Internalname, StringUtil.RTrim( A850ParametrosPln_Coluna), StringUtil.RTrim( context.localUtil.Format( A850ParametrosPln_Coluna, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosPln_Coluna_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 30, "px", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ParametrosPlanilhasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockparametrospln_linha_Internalname, "Linha", "", "", lblTextblockparametrospln_linha_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ParametrosPlanilhasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtParametrosPln_Linha_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A851ParametrosPln_Linha), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A851ParametrosPln_Linha), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtParametrosPln_Linha_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 70, "px", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ParametrosPlanilhasGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_F22e( true) ;
         }
         else
         {
            wb_table2_8_F22e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A848ParametrosPln_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAF22( ) ;
         WSF22( ) ;
         WEF22( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA848ParametrosPln_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAF22( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "parametrosplanilhasgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAF22( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A848ParametrosPln_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
         }
         wcpOA848ParametrosPln_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA848ParametrosPln_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A848ParametrosPln_Codigo != wcpOA848ParametrosPln_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA848ParametrosPln_Codigo = A848ParametrosPln_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA848ParametrosPln_Codigo = cgiGet( sPrefix+"A848ParametrosPln_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA848ParametrosPln_Codigo) > 0 )
         {
            A848ParametrosPln_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA848ParametrosPln_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A848ParametrosPln_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A848ParametrosPln_Codigo), 6, 0)));
         }
         else
         {
            A848ParametrosPln_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A848ParametrosPln_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAF22( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSF22( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSF22( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A848ParametrosPln_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A848ParametrosPln_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA848ParametrosPln_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A848ParametrosPln_Codigo_CTRL", StringUtil.RTrim( sCtrlA848ParametrosPln_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEF22( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311723290");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("parametrosplanilhasgeneral.js", "?2020311723290");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockparametrospln_areatrabalhocod_Internalname = sPrefix+"TEXTBLOCKPARAMETROSPLN_AREATRABALHOCOD";
         dynParametrosPln_AreaTrabalhoCod_Internalname = sPrefix+"PARAMETROSPLN_AREATRABALHOCOD";
         lblTextblockparametrospln_aba_Internalname = sPrefix+"TEXTBLOCKPARAMETROSPLN_ABA";
         edtParametrosPln_Aba_Internalname = sPrefix+"PARAMETROSPLN_ABA";
         lblTextblockparametrospln_campo_Internalname = sPrefix+"TEXTBLOCKPARAMETROSPLN_CAMPO";
         edtParametrosPln_Campo_Internalname = sPrefix+"PARAMETROSPLN_CAMPO";
         lblTextblockparametrospln_coluna_Internalname = sPrefix+"TEXTBLOCKPARAMETROSPLN_COLUNA";
         edtParametrosPln_Coluna_Internalname = sPrefix+"PARAMETROSPLN_COLUNA";
         lblTextblockparametrospln_linha_Internalname = sPrefix+"TEXTBLOCKPARAMETROSPLN_LINHA";
         edtParametrosPln_Linha_Internalname = sPrefix+"PARAMETROSPLN_LINHA";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtParametrosPln_Codigo_Internalname = sPrefix+"PARAMETROSPLN_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtParametrosPln_Linha_Jsonclick = "";
         edtParametrosPln_Coluna_Jsonclick = "";
         edtParametrosPln_Campo_Jsonclick = "";
         edtParametrosPln_Aba_Jsonclick = "";
         dynParametrosPln_AreaTrabalhoCod_Jsonclick = "";
         edtParametrosPln_Codigo_Jsonclick = "";
         edtParametrosPln_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13F22',iparms:[{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14F22',iparms:[{av:'A848ParametrosPln_Codigo',fld:'PARAMETROSPLN_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A2015ParametrosPln_Aba = "";
         A849ParametrosPln_Campo = "";
         A850ParametrosPln_Coluna = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00F22_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         H00F22_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         H00F22_A6AreaTrabalho_Descricao = new String[] {""} ;
         H00F22_n6AreaTrabalho_Descricao = new bool[] {false} ;
         H00F23_A848ParametrosPln_Codigo = new int[1] ;
         H00F23_A851ParametrosPln_Linha = new int[1] ;
         H00F23_A850ParametrosPln_Coluna = new String[] {""} ;
         H00F23_A849ParametrosPln_Campo = new String[] {""} ;
         H00F23_A2015ParametrosPln_Aba = new String[] {""} ;
         H00F23_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         H00F23_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockparametrospln_areatrabalhocod_Jsonclick = "";
         lblTextblockparametrospln_aba_Jsonclick = "";
         lblTextblockparametrospln_campo_Jsonclick = "";
         lblTextblockparametrospln_coluna_Jsonclick = "";
         lblTextblockparametrospln_linha_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA848ParametrosPln_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.parametrosplanilhasgeneral__default(),
            new Object[][] {
                new Object[] {
               H00F22_A847ParametrosPln_AreaTrabalhoCod, H00F22_A6AreaTrabalho_Descricao, H00F22_n6AreaTrabalho_Descricao
               }
               , new Object[] {
               H00F23_A848ParametrosPln_Codigo, H00F23_A851ParametrosPln_Linha, H00F23_A850ParametrosPln_Coluna, H00F23_A849ParametrosPln_Campo, H00F23_A2015ParametrosPln_Aba, H00F23_A847ParametrosPln_AreaTrabalhoCod, H00F23_n847ParametrosPln_AreaTrabalhoCod
               }
            }
         );
         AV14Pgmname = "ParametrosPlanilhasGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ParametrosPlanilhasGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A848ParametrosPln_Codigo ;
      private int wcpOA848ParametrosPln_Codigo ;
      private int A847ParametrosPln_AreaTrabalhoCod ;
      private int A851ParametrosPln_Linha ;
      private int edtParametrosPln_Codigo_Visible ;
      private int gxdynajaxindex ;
      private int AV7ParametrosPln_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A2015ParametrosPln_Aba ;
      private String A850ParametrosPln_Coluna ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtParametrosPln_Codigo_Internalname ;
      private String edtParametrosPln_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynParametrosPln_AreaTrabalhoCod_Internalname ;
      private String edtParametrosPln_Aba_Internalname ;
      private String edtParametrosPln_Campo_Internalname ;
      private String edtParametrosPln_Coluna_Internalname ;
      private String edtParametrosPln_Linha_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockparametrospln_areatrabalhocod_Internalname ;
      private String lblTextblockparametrospln_areatrabalhocod_Jsonclick ;
      private String dynParametrosPln_AreaTrabalhoCod_Jsonclick ;
      private String lblTextblockparametrospln_aba_Internalname ;
      private String lblTextblockparametrospln_aba_Jsonclick ;
      private String edtParametrosPln_Aba_Jsonclick ;
      private String lblTextblockparametrospln_campo_Internalname ;
      private String lblTextblockparametrospln_campo_Jsonclick ;
      private String edtParametrosPln_Campo_Jsonclick ;
      private String lblTextblockparametrospln_coluna_Internalname ;
      private String lblTextblockparametrospln_coluna_Jsonclick ;
      private String edtParametrosPln_Coluna_Jsonclick ;
      private String lblTextblockparametrospln_linha_Internalname ;
      private String lblTextblockparametrospln_linha_Jsonclick ;
      private String edtParametrosPln_Linha_Jsonclick ;
      private String sCtrlA848ParametrosPln_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n847ParametrosPln_AreaTrabalhoCod ;
      private bool returnInSub ;
      private String A849ParametrosPln_Campo ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynParametrosPln_AreaTrabalhoCod ;
      private IDataStoreProvider pr_default ;
      private int[] H00F22_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] H00F22_n847ParametrosPln_AreaTrabalhoCod ;
      private String[] H00F22_A6AreaTrabalho_Descricao ;
      private bool[] H00F22_n6AreaTrabalho_Descricao ;
      private int[] H00F23_A848ParametrosPln_Codigo ;
      private int[] H00F23_A851ParametrosPln_Linha ;
      private String[] H00F23_A850ParametrosPln_Coluna ;
      private String[] H00F23_A849ParametrosPln_Campo ;
      private String[] H00F23_A2015ParametrosPln_Aba ;
      private int[] H00F23_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] H00F23_n847ParametrosPln_AreaTrabalhoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class parametrosplanilhasgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00F22 ;
          prmH00F22 = new Object[] {
          } ;
          Object[] prmH00F23 ;
          prmH00F23 = new Object[] {
          new Object[] {"@ParametrosPln_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00F22", "SELECT [AreaTrabalho_Codigo] AS ParametrosPln_AreaTrabalhoCod, [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F22,0,0,true,false )
             ,new CursorDef("H00F23", "SELECT [ParametrosPln_Codigo], [ParametrosPln_Linha], [ParametrosPln_Coluna], [ParametrosPln_Campo], [ParametrosPln_Aba], [ParametrosPln_AreaTrabalhoCod] AS ParametrosPln_AreaTrabalhoCod FROM [ParametrosPlanilhas] WITH (NOLOCK) WHERE [ParametrosPln_Codigo] = @ParametrosPln_Codigo ORDER BY [ParametrosPln_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00F23,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 30) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
