/*
               File: ContagemResultadoChckLstLog_BC
        Description: Contagem Resultado Chck Lst Log
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:53:56.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadochcklstlog_bc : GXHttpHandler, IGxSilentTrn
   {
      public contagemresultadochcklstlog_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadochcklstlog_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow2L207( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey2L207( ) ;
         standaloneModal( ) ;
         AddRow2L207( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z820ContagemResultadoChckLstLog_Codigo = A820ContagemResultadoChckLstLog_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_2L0( )
      {
         BeforeValidate2L207( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls2L207( ) ;
            }
            else
            {
               CheckExtendedTable2L207( ) ;
               if ( AnyError == 0 )
               {
                  ZM2L207( 3) ;
                  ZM2L207( 4) ;
                  ZM2L207( 5) ;
                  ZM2L207( 6) ;
                  ZM2L207( 7) ;
               }
               CloseExtendedTableCursors2L207( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM2L207( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z814ContagemResultadoChckLstLog_DataHora = A814ContagemResultadoChckLstLog_DataHora;
            Z824ContagemResultadoChckLstLog_Etapa = A824ContagemResultadoChckLstLog_Etapa;
            Z426NaoConformidade_Codigo = A426NaoConformidade_Codigo;
            Z1853ContagemResultadoChckLstLog_OSCodigo = A1853ContagemResultadoChckLstLog_OSCodigo;
            Z811ContagemResultadoChckLstLog_ChckLstCod = A811ContagemResultadoChckLstLog_ChckLstCod;
            Z822ContagemResultadoChckLstLog_UsuarioCod = A822ContagemResultadoChckLstLog_UsuarioCod;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z823ContagemResultadoChckLstLog_PessoaCod = A823ContagemResultadoChckLstLog_PessoaCod;
         }
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z817ContagemResultadoChckLstLog_UsuarioNome = A817ContagemResultadoChckLstLog_UsuarioNome;
         }
         if ( GX_JID == -2 )
         {
            Z820ContagemResultadoChckLstLog_Codigo = A820ContagemResultadoChckLstLog_Codigo;
            Z814ContagemResultadoChckLstLog_DataHora = A814ContagemResultadoChckLstLog_DataHora;
            Z824ContagemResultadoChckLstLog_Etapa = A824ContagemResultadoChckLstLog_Etapa;
            Z426NaoConformidade_Codigo = A426NaoConformidade_Codigo;
            Z1853ContagemResultadoChckLstLog_OSCodigo = A1853ContagemResultadoChckLstLog_OSCodigo;
            Z811ContagemResultadoChckLstLog_ChckLstCod = A811ContagemResultadoChckLstLog_ChckLstCod;
            Z822ContagemResultadoChckLstLog_UsuarioCod = A822ContagemResultadoChckLstLog_UsuarioCod;
            Z812ContagemResultadoChckLstLog_ChckLstDes = A812ContagemResultadoChckLstLog_ChckLstDes;
            Z823ContagemResultadoChckLstLog_PessoaCod = A823ContagemResultadoChckLstLog_PessoaCod;
            Z817ContagemResultadoChckLstLog_UsuarioNome = A817ContagemResultadoChckLstLog_UsuarioNome;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load2L207( )
      {
         /* Using cursor BC002L9 */
         pr_default.execute(7, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound207 = 1;
            A814ContagemResultadoChckLstLog_DataHora = BC002L9_A814ContagemResultadoChckLstLog_DataHora[0];
            A812ContagemResultadoChckLstLog_ChckLstDes = BC002L9_A812ContagemResultadoChckLstLog_ChckLstDes[0];
            n812ContagemResultadoChckLstLog_ChckLstDes = BC002L9_n812ContagemResultadoChckLstLog_ChckLstDes[0];
            A817ContagemResultadoChckLstLog_UsuarioNome = BC002L9_A817ContagemResultadoChckLstLog_UsuarioNome[0];
            n817ContagemResultadoChckLstLog_UsuarioNome = BC002L9_n817ContagemResultadoChckLstLog_UsuarioNome[0];
            A824ContagemResultadoChckLstLog_Etapa = BC002L9_A824ContagemResultadoChckLstLog_Etapa[0];
            n824ContagemResultadoChckLstLog_Etapa = BC002L9_n824ContagemResultadoChckLstLog_Etapa[0];
            A426NaoConformidade_Codigo = BC002L9_A426NaoConformidade_Codigo[0];
            n426NaoConformidade_Codigo = BC002L9_n426NaoConformidade_Codigo[0];
            A1853ContagemResultadoChckLstLog_OSCodigo = BC002L9_A1853ContagemResultadoChckLstLog_OSCodigo[0];
            A811ContagemResultadoChckLstLog_ChckLstCod = BC002L9_A811ContagemResultadoChckLstLog_ChckLstCod[0];
            n811ContagemResultadoChckLstLog_ChckLstCod = BC002L9_n811ContagemResultadoChckLstLog_ChckLstCod[0];
            A822ContagemResultadoChckLstLog_UsuarioCod = BC002L9_A822ContagemResultadoChckLstLog_UsuarioCod[0];
            A823ContagemResultadoChckLstLog_PessoaCod = BC002L9_A823ContagemResultadoChckLstLog_PessoaCod[0];
            n823ContagemResultadoChckLstLog_PessoaCod = BC002L9_n823ContagemResultadoChckLstLog_PessoaCod[0];
            ZM2L207( -2) ;
         }
         pr_default.close(7);
         OnLoadActions2L207( ) ;
      }

      protected void OnLoadActions2L207( )
      {
      }

      protected void CheckExtendedTable2L207( )
      {
         standaloneModal( ) ;
         /* Using cursor BC002L5 */
         pr_default.execute(3, new Object[] {A1853ContagemResultadoChckLstLog_OSCodigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Chck Lst Log_Contagem Resutlado'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO");
            AnyError = 1;
         }
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A814ContagemResultadoChckLstLog_DataHora) || ( A814ContagemResultadoChckLstLog_DataHora >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC002L6 */
         pr_default.execute(4, new Object[] {n811ContagemResultadoChckLstLog_ChckLstCod, A811ContagemResultadoChckLstLog_ChckLstCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A811ContagemResultadoChckLstLog_ChckLstCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Chck LstLog_Check List'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD");
               AnyError = 1;
            }
         }
         A812ContagemResultadoChckLstLog_ChckLstDes = BC002L6_A812ContagemResultadoChckLstLog_ChckLstDes[0];
         n812ContagemResultadoChckLstLog_ChckLstDes = BC002L6_n812ContagemResultadoChckLstLog_ChckLstDes[0];
         pr_default.close(4);
         /* Using cursor BC002L4 */
         pr_default.execute(2, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A426NaoConformidade_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'N�o Conformidade'.", "ForeignKeyNotFound", 1, "NAOCONFORMIDADE_CODIGO");
               AnyError = 1;
            }
         }
         pr_default.close(2);
         /* Using cursor BC002L7 */
         pr_default.execute(5, new Object[] {A822ContagemResultadoChckLstLog_UsuarioCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Chck Lst Log_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD");
            AnyError = 1;
         }
         A823ContagemResultadoChckLstLog_PessoaCod = BC002L7_A823ContagemResultadoChckLstLog_PessoaCod[0];
         n823ContagemResultadoChckLstLog_PessoaCod = BC002L7_n823ContagemResultadoChckLstLog_PessoaCod[0];
         pr_default.close(5);
         /* Using cursor BC002L8 */
         pr_default.execute(6, new Object[] {n823ContagemResultadoChckLstLog_PessoaCod, A823ContagemResultadoChckLstLog_PessoaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A817ContagemResultadoChckLstLog_UsuarioNome = BC002L8_A817ContagemResultadoChckLstLog_UsuarioNome[0];
         n817ContagemResultadoChckLstLog_UsuarioNome = BC002L8_n817ContagemResultadoChckLstLog_UsuarioNome[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors2L207( )
      {
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(2);
         pr_default.close(5);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey2L207( )
      {
         /* Using cursor BC002L10 */
         pr_default.execute(8, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound207 = 1;
         }
         else
         {
            RcdFound207 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC002L3 */
         pr_default.execute(1, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2L207( 2) ;
            RcdFound207 = 1;
            A820ContagemResultadoChckLstLog_Codigo = BC002L3_A820ContagemResultadoChckLstLog_Codigo[0];
            A814ContagemResultadoChckLstLog_DataHora = BC002L3_A814ContagemResultadoChckLstLog_DataHora[0];
            A824ContagemResultadoChckLstLog_Etapa = BC002L3_A824ContagemResultadoChckLstLog_Etapa[0];
            n824ContagemResultadoChckLstLog_Etapa = BC002L3_n824ContagemResultadoChckLstLog_Etapa[0];
            A426NaoConformidade_Codigo = BC002L3_A426NaoConformidade_Codigo[0];
            n426NaoConformidade_Codigo = BC002L3_n426NaoConformidade_Codigo[0];
            A1853ContagemResultadoChckLstLog_OSCodigo = BC002L3_A1853ContagemResultadoChckLstLog_OSCodigo[0];
            A811ContagemResultadoChckLstLog_ChckLstCod = BC002L3_A811ContagemResultadoChckLstLog_ChckLstCod[0];
            n811ContagemResultadoChckLstLog_ChckLstCod = BC002L3_n811ContagemResultadoChckLstLog_ChckLstCod[0];
            A822ContagemResultadoChckLstLog_UsuarioCod = BC002L3_A822ContagemResultadoChckLstLog_UsuarioCod[0];
            Z820ContagemResultadoChckLstLog_Codigo = A820ContagemResultadoChckLstLog_Codigo;
            sMode207 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load2L207( ) ;
            if ( AnyError == 1 )
            {
               RcdFound207 = 0;
               InitializeNonKey2L207( ) ;
            }
            Gx_mode = sMode207;
         }
         else
         {
            RcdFound207 = 0;
            InitializeNonKey2L207( ) ;
            sMode207 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode207;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2L207( ) ;
         if ( RcdFound207 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_2L0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency2L207( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC002L2 */
            pr_default.execute(0, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoChckLstLog"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z814ContagemResultadoChckLstLog_DataHora != BC002L2_A814ContagemResultadoChckLstLog_DataHora[0] ) || ( Z824ContagemResultadoChckLstLog_Etapa != BC002L2_A824ContagemResultadoChckLstLog_Etapa[0] ) || ( Z426NaoConformidade_Codigo != BC002L2_A426NaoConformidade_Codigo[0] ) || ( Z1853ContagemResultadoChckLstLog_OSCodigo != BC002L2_A1853ContagemResultadoChckLstLog_OSCodigo[0] ) || ( Z811ContagemResultadoChckLstLog_ChckLstCod != BC002L2_A811ContagemResultadoChckLstLog_ChckLstCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z822ContagemResultadoChckLstLog_UsuarioCod != BC002L2_A822ContagemResultadoChckLstLog_UsuarioCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoChckLstLog"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2L207( )
      {
         BeforeValidate2L207( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2L207( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2L207( 0) ;
            CheckOptimisticConcurrency2L207( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2L207( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2L207( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002L11 */
                     pr_default.execute(9, new Object[] {A814ContagemResultadoChckLstLog_DataHora, n824ContagemResultadoChckLstLog_Etapa, A824ContagemResultadoChckLstLog_Etapa, n426NaoConformidade_Codigo, A426NaoConformidade_Codigo, A1853ContagemResultadoChckLstLog_OSCodigo, n811ContagemResultadoChckLstLog_ChckLstCod, A811ContagemResultadoChckLstLog_ChckLstCod, A822ContagemResultadoChckLstLog_UsuarioCod});
                     A820ContagemResultadoChckLstLog_Codigo = BC002L11_A820ContagemResultadoChckLstLog_Codigo[0];
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLstLog") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2L207( ) ;
            }
            EndLevel2L207( ) ;
         }
         CloseExtendedTableCursors2L207( ) ;
      }

      protected void Update2L207( )
      {
         BeforeValidate2L207( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2L207( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2L207( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2L207( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2L207( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC002L12 */
                     pr_default.execute(10, new Object[] {A814ContagemResultadoChckLstLog_DataHora, n824ContagemResultadoChckLstLog_Etapa, A824ContagemResultadoChckLstLog_Etapa, n426NaoConformidade_Codigo, A426NaoConformidade_Codigo, A1853ContagemResultadoChckLstLog_OSCodigo, n811ContagemResultadoChckLstLog_ChckLstCod, A811ContagemResultadoChckLstLog_ChckLstCod, A822ContagemResultadoChckLstLog_UsuarioCod, A820ContagemResultadoChckLstLog_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLstLog") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoChckLstLog"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2L207( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2L207( ) ;
         }
         CloseExtendedTableCursors2L207( ) ;
      }

      protected void DeferredUpdate2L207( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate2L207( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2L207( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2L207( ) ;
            AfterConfirm2L207( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2L207( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC002L13 */
                  pr_default.execute(11, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLstLog") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode207 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel2L207( ) ;
         Gx_mode = sMode207;
      }

      protected void OnDeleteControls2L207( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC002L14 */
            pr_default.execute(12, new Object[] {n811ContagemResultadoChckLstLog_ChckLstCod, A811ContagemResultadoChckLstLog_ChckLstCod});
            A812ContagemResultadoChckLstLog_ChckLstDes = BC002L14_A812ContagemResultadoChckLstLog_ChckLstDes[0];
            n812ContagemResultadoChckLstLog_ChckLstDes = BC002L14_n812ContagemResultadoChckLstLog_ChckLstDes[0];
            pr_default.close(12);
            /* Using cursor BC002L15 */
            pr_default.execute(13, new Object[] {A822ContagemResultadoChckLstLog_UsuarioCod});
            A823ContagemResultadoChckLstLog_PessoaCod = BC002L15_A823ContagemResultadoChckLstLog_PessoaCod[0];
            n823ContagemResultadoChckLstLog_PessoaCod = BC002L15_n823ContagemResultadoChckLstLog_PessoaCod[0];
            pr_default.close(13);
            /* Using cursor BC002L16 */
            pr_default.execute(14, new Object[] {n823ContagemResultadoChckLstLog_PessoaCod, A823ContagemResultadoChckLstLog_PessoaCod});
            A817ContagemResultadoChckLstLog_UsuarioNome = BC002L16_A817ContagemResultadoChckLstLog_UsuarioNome[0];
            n817ContagemResultadoChckLstLog_UsuarioNome = BC002L16_n817ContagemResultadoChckLstLog_UsuarioNome[0];
            pr_default.close(14);
         }
      }

      protected void EndLevel2L207( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2L207( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart2L207( )
      {
         /* Using cursor BC002L17 */
         pr_default.execute(15, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
         RcdFound207 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound207 = 1;
            A820ContagemResultadoChckLstLog_Codigo = BC002L17_A820ContagemResultadoChckLstLog_Codigo[0];
            A814ContagemResultadoChckLstLog_DataHora = BC002L17_A814ContagemResultadoChckLstLog_DataHora[0];
            A812ContagemResultadoChckLstLog_ChckLstDes = BC002L17_A812ContagemResultadoChckLstLog_ChckLstDes[0];
            n812ContagemResultadoChckLstLog_ChckLstDes = BC002L17_n812ContagemResultadoChckLstLog_ChckLstDes[0];
            A817ContagemResultadoChckLstLog_UsuarioNome = BC002L17_A817ContagemResultadoChckLstLog_UsuarioNome[0];
            n817ContagemResultadoChckLstLog_UsuarioNome = BC002L17_n817ContagemResultadoChckLstLog_UsuarioNome[0];
            A824ContagemResultadoChckLstLog_Etapa = BC002L17_A824ContagemResultadoChckLstLog_Etapa[0];
            n824ContagemResultadoChckLstLog_Etapa = BC002L17_n824ContagemResultadoChckLstLog_Etapa[0];
            A426NaoConformidade_Codigo = BC002L17_A426NaoConformidade_Codigo[0];
            n426NaoConformidade_Codigo = BC002L17_n426NaoConformidade_Codigo[0];
            A1853ContagemResultadoChckLstLog_OSCodigo = BC002L17_A1853ContagemResultadoChckLstLog_OSCodigo[0];
            A811ContagemResultadoChckLstLog_ChckLstCod = BC002L17_A811ContagemResultadoChckLstLog_ChckLstCod[0];
            n811ContagemResultadoChckLstLog_ChckLstCod = BC002L17_n811ContagemResultadoChckLstLog_ChckLstCod[0];
            A822ContagemResultadoChckLstLog_UsuarioCod = BC002L17_A822ContagemResultadoChckLstLog_UsuarioCod[0];
            A823ContagemResultadoChckLstLog_PessoaCod = BC002L17_A823ContagemResultadoChckLstLog_PessoaCod[0];
            n823ContagemResultadoChckLstLog_PessoaCod = BC002L17_n823ContagemResultadoChckLstLog_PessoaCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext2L207( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound207 = 0;
         ScanKeyLoad2L207( ) ;
      }

      protected void ScanKeyLoad2L207( )
      {
         sMode207 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound207 = 1;
            A820ContagemResultadoChckLstLog_Codigo = BC002L17_A820ContagemResultadoChckLstLog_Codigo[0];
            A814ContagemResultadoChckLstLog_DataHora = BC002L17_A814ContagemResultadoChckLstLog_DataHora[0];
            A812ContagemResultadoChckLstLog_ChckLstDes = BC002L17_A812ContagemResultadoChckLstLog_ChckLstDes[0];
            n812ContagemResultadoChckLstLog_ChckLstDes = BC002L17_n812ContagemResultadoChckLstLog_ChckLstDes[0];
            A817ContagemResultadoChckLstLog_UsuarioNome = BC002L17_A817ContagemResultadoChckLstLog_UsuarioNome[0];
            n817ContagemResultadoChckLstLog_UsuarioNome = BC002L17_n817ContagemResultadoChckLstLog_UsuarioNome[0];
            A824ContagemResultadoChckLstLog_Etapa = BC002L17_A824ContagemResultadoChckLstLog_Etapa[0];
            n824ContagemResultadoChckLstLog_Etapa = BC002L17_n824ContagemResultadoChckLstLog_Etapa[0];
            A426NaoConformidade_Codigo = BC002L17_A426NaoConformidade_Codigo[0];
            n426NaoConformidade_Codigo = BC002L17_n426NaoConformidade_Codigo[0];
            A1853ContagemResultadoChckLstLog_OSCodigo = BC002L17_A1853ContagemResultadoChckLstLog_OSCodigo[0];
            A811ContagemResultadoChckLstLog_ChckLstCod = BC002L17_A811ContagemResultadoChckLstLog_ChckLstCod[0];
            n811ContagemResultadoChckLstLog_ChckLstCod = BC002L17_n811ContagemResultadoChckLstLog_ChckLstCod[0];
            A822ContagemResultadoChckLstLog_UsuarioCod = BC002L17_A822ContagemResultadoChckLstLog_UsuarioCod[0];
            A823ContagemResultadoChckLstLog_PessoaCod = BC002L17_A823ContagemResultadoChckLstLog_PessoaCod[0];
            n823ContagemResultadoChckLstLog_PessoaCod = BC002L17_n823ContagemResultadoChckLstLog_PessoaCod[0];
         }
         Gx_mode = sMode207;
      }

      protected void ScanKeyEnd2L207( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm2L207( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2L207( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2L207( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2L207( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2L207( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2L207( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2L207( )
      {
      }

      protected void AddRow2L207( )
      {
         VarsToRow207( bcContagemResultadoChckLstLog) ;
      }

      protected void ReadRow2L207( )
      {
         RowToVars207( bcContagemResultadoChckLstLog, 1) ;
      }

      protected void InitializeNonKey2L207( )
      {
         A1853ContagemResultadoChckLstLog_OSCodigo = 0;
         A814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         A811ContagemResultadoChckLstLog_ChckLstCod = 0;
         n811ContagemResultadoChckLstLog_ChckLstCod = false;
         A812ContagemResultadoChckLstLog_ChckLstDes = "";
         n812ContagemResultadoChckLstLog_ChckLstDes = false;
         A426NaoConformidade_Codigo = 0;
         n426NaoConformidade_Codigo = false;
         A822ContagemResultadoChckLstLog_UsuarioCod = 0;
         A823ContagemResultadoChckLstLog_PessoaCod = 0;
         n823ContagemResultadoChckLstLog_PessoaCod = false;
         A817ContagemResultadoChckLstLog_UsuarioNome = "";
         n817ContagemResultadoChckLstLog_UsuarioNome = false;
         A824ContagemResultadoChckLstLog_Etapa = 0;
         n824ContagemResultadoChckLstLog_Etapa = false;
         Z814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         Z824ContagemResultadoChckLstLog_Etapa = 0;
         Z426NaoConformidade_Codigo = 0;
         Z1853ContagemResultadoChckLstLog_OSCodigo = 0;
         Z811ContagemResultadoChckLstLog_ChckLstCod = 0;
         Z822ContagemResultadoChckLstLog_UsuarioCod = 0;
      }

      protected void InitAll2L207( )
      {
         A820ContagemResultadoChckLstLog_Codigo = 0;
         InitializeNonKey2L207( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow207( SdtContagemResultadoChckLstLog obj207 )
      {
         obj207.gxTpr_Mode = Gx_mode;
         obj207.gxTpr_Contagemresultadochcklstlog_oscodigo = A1853ContagemResultadoChckLstLog_OSCodigo;
         obj207.gxTpr_Contagemresultadochcklstlog_datahora = A814ContagemResultadoChckLstLog_DataHora;
         obj207.gxTpr_Contagemresultadochcklstlog_chcklstcod = A811ContagemResultadoChckLstLog_ChckLstCod;
         obj207.gxTpr_Contagemresultadochcklstlog_chcklstdes = A812ContagemResultadoChckLstLog_ChckLstDes;
         obj207.gxTpr_Naoconformidade_codigo = A426NaoConformidade_Codigo;
         obj207.gxTpr_Contagemresultadochcklstlog_usuariocod = A822ContagemResultadoChckLstLog_UsuarioCod;
         obj207.gxTpr_Contagemresultadochcklstlog_pessoacod = A823ContagemResultadoChckLstLog_PessoaCod;
         obj207.gxTpr_Contagemresultadochcklstlog_usuarionome = A817ContagemResultadoChckLstLog_UsuarioNome;
         obj207.gxTpr_Contagemresultadochcklstlog_etapa = A824ContagemResultadoChckLstLog_Etapa;
         obj207.gxTpr_Contagemresultadochcklstlog_codigo = A820ContagemResultadoChckLstLog_Codigo;
         obj207.gxTpr_Contagemresultadochcklstlog_codigo_Z = Z820ContagemResultadoChckLstLog_Codigo;
         obj207.gxTpr_Contagemresultadochcklstlog_oscodigo_Z = Z1853ContagemResultadoChckLstLog_OSCodigo;
         obj207.gxTpr_Contagemresultadochcklstlog_datahora_Z = Z814ContagemResultadoChckLstLog_DataHora;
         obj207.gxTpr_Contagemresultadochcklstlog_chcklstcod_Z = Z811ContagemResultadoChckLstLog_ChckLstCod;
         obj207.gxTpr_Naoconformidade_codigo_Z = Z426NaoConformidade_Codigo;
         obj207.gxTpr_Contagemresultadochcklstlog_usuariocod_Z = Z822ContagemResultadoChckLstLog_UsuarioCod;
         obj207.gxTpr_Contagemresultadochcklstlog_pessoacod_Z = Z823ContagemResultadoChckLstLog_PessoaCod;
         obj207.gxTpr_Contagemresultadochcklstlog_usuarionome_Z = Z817ContagemResultadoChckLstLog_UsuarioNome;
         obj207.gxTpr_Contagemresultadochcklstlog_etapa_Z = Z824ContagemResultadoChckLstLog_Etapa;
         obj207.gxTpr_Contagemresultadochcklstlog_chcklstcod_N = (short)(Convert.ToInt16(n811ContagemResultadoChckLstLog_ChckLstCod));
         obj207.gxTpr_Contagemresultadochcklstlog_chcklstdes_N = (short)(Convert.ToInt16(n812ContagemResultadoChckLstLog_ChckLstDes));
         obj207.gxTpr_Naoconformidade_codigo_N = (short)(Convert.ToInt16(n426NaoConformidade_Codigo));
         obj207.gxTpr_Contagemresultadochcklstlog_pessoacod_N = (short)(Convert.ToInt16(n823ContagemResultadoChckLstLog_PessoaCod));
         obj207.gxTpr_Contagemresultadochcklstlog_usuarionome_N = (short)(Convert.ToInt16(n817ContagemResultadoChckLstLog_UsuarioNome));
         obj207.gxTpr_Contagemresultadochcklstlog_etapa_N = (short)(Convert.ToInt16(n824ContagemResultadoChckLstLog_Etapa));
         obj207.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow207( SdtContagemResultadoChckLstLog obj207 )
      {
         obj207.gxTpr_Contagemresultadochcklstlog_codigo = A820ContagemResultadoChckLstLog_Codigo;
         return  ;
      }

      public void RowToVars207( SdtContagemResultadoChckLstLog obj207 ,
                                int forceLoad )
      {
         Gx_mode = obj207.gxTpr_Mode;
         A1853ContagemResultadoChckLstLog_OSCodigo = obj207.gxTpr_Contagemresultadochcklstlog_oscodigo;
         A814ContagemResultadoChckLstLog_DataHora = obj207.gxTpr_Contagemresultadochcklstlog_datahora;
         A811ContagemResultadoChckLstLog_ChckLstCod = obj207.gxTpr_Contagemresultadochcklstlog_chcklstcod;
         n811ContagemResultadoChckLstLog_ChckLstCod = false;
         A812ContagemResultadoChckLstLog_ChckLstDes = obj207.gxTpr_Contagemresultadochcklstlog_chcklstdes;
         n812ContagemResultadoChckLstLog_ChckLstDes = false;
         A426NaoConformidade_Codigo = obj207.gxTpr_Naoconformidade_codigo;
         n426NaoConformidade_Codigo = false;
         A822ContagemResultadoChckLstLog_UsuarioCod = obj207.gxTpr_Contagemresultadochcklstlog_usuariocod;
         A823ContagemResultadoChckLstLog_PessoaCod = obj207.gxTpr_Contagemresultadochcklstlog_pessoacod;
         n823ContagemResultadoChckLstLog_PessoaCod = false;
         A817ContagemResultadoChckLstLog_UsuarioNome = obj207.gxTpr_Contagemresultadochcklstlog_usuarionome;
         n817ContagemResultadoChckLstLog_UsuarioNome = false;
         A824ContagemResultadoChckLstLog_Etapa = obj207.gxTpr_Contagemresultadochcklstlog_etapa;
         n824ContagemResultadoChckLstLog_Etapa = false;
         A820ContagemResultadoChckLstLog_Codigo = obj207.gxTpr_Contagemresultadochcklstlog_codigo;
         Z820ContagemResultadoChckLstLog_Codigo = obj207.gxTpr_Contagemresultadochcklstlog_codigo_Z;
         Z1853ContagemResultadoChckLstLog_OSCodigo = obj207.gxTpr_Contagemresultadochcklstlog_oscodigo_Z;
         Z814ContagemResultadoChckLstLog_DataHora = obj207.gxTpr_Contagemresultadochcklstlog_datahora_Z;
         Z811ContagemResultadoChckLstLog_ChckLstCod = obj207.gxTpr_Contagemresultadochcklstlog_chcklstcod_Z;
         Z426NaoConformidade_Codigo = obj207.gxTpr_Naoconformidade_codigo_Z;
         Z822ContagemResultadoChckLstLog_UsuarioCod = obj207.gxTpr_Contagemresultadochcklstlog_usuariocod_Z;
         Z823ContagemResultadoChckLstLog_PessoaCod = obj207.gxTpr_Contagemresultadochcklstlog_pessoacod_Z;
         Z817ContagemResultadoChckLstLog_UsuarioNome = obj207.gxTpr_Contagemresultadochcklstlog_usuarionome_Z;
         Z824ContagemResultadoChckLstLog_Etapa = obj207.gxTpr_Contagemresultadochcklstlog_etapa_Z;
         n811ContagemResultadoChckLstLog_ChckLstCod = (bool)(Convert.ToBoolean(obj207.gxTpr_Contagemresultadochcklstlog_chcklstcod_N));
         n812ContagemResultadoChckLstLog_ChckLstDes = (bool)(Convert.ToBoolean(obj207.gxTpr_Contagemresultadochcklstlog_chcklstdes_N));
         n426NaoConformidade_Codigo = (bool)(Convert.ToBoolean(obj207.gxTpr_Naoconformidade_codigo_N));
         n823ContagemResultadoChckLstLog_PessoaCod = (bool)(Convert.ToBoolean(obj207.gxTpr_Contagemresultadochcklstlog_pessoacod_N));
         n817ContagemResultadoChckLstLog_UsuarioNome = (bool)(Convert.ToBoolean(obj207.gxTpr_Contagemresultadochcklstlog_usuarionome_N));
         n824ContagemResultadoChckLstLog_Etapa = (bool)(Convert.ToBoolean(obj207.gxTpr_Contagemresultadochcklstlog_etapa_N));
         Gx_mode = obj207.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A820ContagemResultadoChckLstLog_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey2L207( ) ;
         ScanKeyStart2L207( ) ;
         if ( RcdFound207 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z820ContagemResultadoChckLstLog_Codigo = A820ContagemResultadoChckLstLog_Codigo;
         }
         ZM2L207( -2) ;
         OnLoadActions2L207( ) ;
         AddRow2L207( ) ;
         ScanKeyEnd2L207( ) ;
         if ( RcdFound207 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars207( bcContagemResultadoChckLstLog, 0) ;
         ScanKeyStart2L207( ) ;
         if ( RcdFound207 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z820ContagemResultadoChckLstLog_Codigo = A820ContagemResultadoChckLstLog_Codigo;
         }
         ZM2L207( -2) ;
         OnLoadActions2L207( ) ;
         AddRow2L207( ) ;
         ScanKeyEnd2L207( ) ;
         if ( RcdFound207 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars207( bcContagemResultadoChckLstLog, 0) ;
         nKeyPressed = 1;
         GetKey2L207( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert2L207( ) ;
         }
         else
         {
            if ( RcdFound207 == 1 )
            {
               if ( A820ContagemResultadoChckLstLog_Codigo != Z820ContagemResultadoChckLstLog_Codigo )
               {
                  A820ContagemResultadoChckLstLog_Codigo = Z820ContagemResultadoChckLstLog_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update2L207( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A820ContagemResultadoChckLstLog_Codigo != Z820ContagemResultadoChckLstLog_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2L207( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert2L207( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow207( bcContagemResultadoChckLstLog) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars207( bcContagemResultadoChckLstLog, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey2L207( ) ;
         if ( RcdFound207 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A820ContagemResultadoChckLstLog_Codigo != Z820ContagemResultadoChckLstLog_Codigo )
            {
               A820ContagemResultadoChckLstLog_Codigo = Z820ContagemResultadoChckLstLog_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A820ContagemResultadoChckLstLog_Codigo != Z820ContagemResultadoChckLstLog_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(13);
         pr_default.close(14);
         context.RollbackDataStores( "ContagemResultadoChckLstLog_BC");
         VarsToRow207( bcContagemResultadoChckLstLog) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContagemResultadoChckLstLog.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContagemResultadoChckLstLog.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContagemResultadoChckLstLog )
         {
            bcContagemResultadoChckLstLog = (SdtContagemResultadoChckLstLog)(sdt);
            if ( StringUtil.StrCmp(bcContagemResultadoChckLstLog.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoChckLstLog.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow207( bcContagemResultadoChckLstLog) ;
            }
            else
            {
               RowToVars207( bcContagemResultadoChckLstLog, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContagemResultadoChckLstLog.gxTpr_Mode, "") == 0 )
            {
               bcContagemResultadoChckLstLog.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars207( bcContagemResultadoChckLstLog, 1) ;
         return  ;
      }

      public SdtContagemResultadoChckLstLog ContagemResultadoChckLstLog_BC
      {
         get {
            return bcContagemResultadoChckLstLog ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(13);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         A814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         Z817ContagemResultadoChckLstLog_UsuarioNome = "";
         A817ContagemResultadoChckLstLog_UsuarioNome = "";
         Z812ContagemResultadoChckLstLog_ChckLstDes = "";
         A812ContagemResultadoChckLstLog_ChckLstDes = "";
         BC002L9_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         BC002L9_A814ContagemResultadoChckLstLog_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC002L9_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         BC002L9_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         BC002L9_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         BC002L9_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         BC002L9_A824ContagemResultadoChckLstLog_Etapa = new short[1] ;
         BC002L9_n824ContagemResultadoChckLstLog_Etapa = new bool[] {false} ;
         BC002L9_A426NaoConformidade_Codigo = new int[1] ;
         BC002L9_n426NaoConformidade_Codigo = new bool[] {false} ;
         BC002L9_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         BC002L9_A811ContagemResultadoChckLstLog_ChckLstCod = new int[1] ;
         BC002L9_n811ContagemResultadoChckLstLog_ChckLstCod = new bool[] {false} ;
         BC002L9_A822ContagemResultadoChckLstLog_UsuarioCod = new int[1] ;
         BC002L9_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         BC002L9_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         BC002L5_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         BC002L6_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         BC002L6_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         BC002L4_A426NaoConformidade_Codigo = new int[1] ;
         BC002L4_n426NaoConformidade_Codigo = new bool[] {false} ;
         BC002L7_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         BC002L7_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         BC002L8_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         BC002L8_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         BC002L10_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         BC002L3_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         BC002L3_A814ContagemResultadoChckLstLog_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC002L3_A824ContagemResultadoChckLstLog_Etapa = new short[1] ;
         BC002L3_n824ContagemResultadoChckLstLog_Etapa = new bool[] {false} ;
         BC002L3_A426NaoConformidade_Codigo = new int[1] ;
         BC002L3_n426NaoConformidade_Codigo = new bool[] {false} ;
         BC002L3_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         BC002L3_A811ContagemResultadoChckLstLog_ChckLstCod = new int[1] ;
         BC002L3_n811ContagemResultadoChckLstLog_ChckLstCod = new bool[] {false} ;
         BC002L3_A822ContagemResultadoChckLstLog_UsuarioCod = new int[1] ;
         sMode207 = "";
         BC002L2_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         BC002L2_A814ContagemResultadoChckLstLog_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC002L2_A824ContagemResultadoChckLstLog_Etapa = new short[1] ;
         BC002L2_n824ContagemResultadoChckLstLog_Etapa = new bool[] {false} ;
         BC002L2_A426NaoConformidade_Codigo = new int[1] ;
         BC002L2_n426NaoConformidade_Codigo = new bool[] {false} ;
         BC002L2_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         BC002L2_A811ContagemResultadoChckLstLog_ChckLstCod = new int[1] ;
         BC002L2_n811ContagemResultadoChckLstLog_ChckLstCod = new bool[] {false} ;
         BC002L2_A822ContagemResultadoChckLstLog_UsuarioCod = new int[1] ;
         BC002L11_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         BC002L14_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         BC002L14_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         BC002L15_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         BC002L15_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         BC002L16_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         BC002L16_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         BC002L17_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         BC002L17_A814ContagemResultadoChckLstLog_DataHora = new DateTime[] {DateTime.MinValue} ;
         BC002L17_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         BC002L17_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         BC002L17_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         BC002L17_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         BC002L17_A824ContagemResultadoChckLstLog_Etapa = new short[1] ;
         BC002L17_n824ContagemResultadoChckLstLog_Etapa = new bool[] {false} ;
         BC002L17_A426NaoConformidade_Codigo = new int[1] ;
         BC002L17_n426NaoConformidade_Codigo = new bool[] {false} ;
         BC002L17_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         BC002L17_A811ContagemResultadoChckLstLog_ChckLstCod = new int[1] ;
         BC002L17_n811ContagemResultadoChckLstLog_ChckLstCod = new bool[] {false} ;
         BC002L17_A822ContagemResultadoChckLstLog_UsuarioCod = new int[1] ;
         BC002L17_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         BC002L17_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadochcklstlog_bc__default(),
            new Object[][] {
                new Object[] {
               BC002L2_A820ContagemResultadoChckLstLog_Codigo, BC002L2_A814ContagemResultadoChckLstLog_DataHora, BC002L2_A824ContagemResultadoChckLstLog_Etapa, BC002L2_n824ContagemResultadoChckLstLog_Etapa, BC002L2_A426NaoConformidade_Codigo, BC002L2_n426NaoConformidade_Codigo, BC002L2_A1853ContagemResultadoChckLstLog_OSCodigo, BC002L2_A811ContagemResultadoChckLstLog_ChckLstCod, BC002L2_n811ContagemResultadoChckLstLog_ChckLstCod, BC002L2_A822ContagemResultadoChckLstLog_UsuarioCod
               }
               , new Object[] {
               BC002L3_A820ContagemResultadoChckLstLog_Codigo, BC002L3_A814ContagemResultadoChckLstLog_DataHora, BC002L3_A824ContagemResultadoChckLstLog_Etapa, BC002L3_n824ContagemResultadoChckLstLog_Etapa, BC002L3_A426NaoConformidade_Codigo, BC002L3_n426NaoConformidade_Codigo, BC002L3_A1853ContagemResultadoChckLstLog_OSCodigo, BC002L3_A811ContagemResultadoChckLstLog_ChckLstCod, BC002L3_n811ContagemResultadoChckLstLog_ChckLstCod, BC002L3_A822ContagemResultadoChckLstLog_UsuarioCod
               }
               , new Object[] {
               BC002L4_A426NaoConformidade_Codigo
               }
               , new Object[] {
               BC002L5_A1853ContagemResultadoChckLstLog_OSCodigo
               }
               , new Object[] {
               BC002L6_A812ContagemResultadoChckLstLog_ChckLstDes, BC002L6_n812ContagemResultadoChckLstLog_ChckLstDes
               }
               , new Object[] {
               BC002L7_A823ContagemResultadoChckLstLog_PessoaCod, BC002L7_n823ContagemResultadoChckLstLog_PessoaCod
               }
               , new Object[] {
               BC002L8_A817ContagemResultadoChckLstLog_UsuarioNome, BC002L8_n817ContagemResultadoChckLstLog_UsuarioNome
               }
               , new Object[] {
               BC002L9_A820ContagemResultadoChckLstLog_Codigo, BC002L9_A814ContagemResultadoChckLstLog_DataHora, BC002L9_A812ContagemResultadoChckLstLog_ChckLstDes, BC002L9_n812ContagemResultadoChckLstLog_ChckLstDes, BC002L9_A817ContagemResultadoChckLstLog_UsuarioNome, BC002L9_n817ContagemResultadoChckLstLog_UsuarioNome, BC002L9_A824ContagemResultadoChckLstLog_Etapa, BC002L9_n824ContagemResultadoChckLstLog_Etapa, BC002L9_A426NaoConformidade_Codigo, BC002L9_n426NaoConformidade_Codigo,
               BC002L9_A1853ContagemResultadoChckLstLog_OSCodigo, BC002L9_A811ContagemResultadoChckLstLog_ChckLstCod, BC002L9_n811ContagemResultadoChckLstLog_ChckLstCod, BC002L9_A822ContagemResultadoChckLstLog_UsuarioCod, BC002L9_A823ContagemResultadoChckLstLog_PessoaCod, BC002L9_n823ContagemResultadoChckLstLog_PessoaCod
               }
               , new Object[] {
               BC002L10_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               BC002L11_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC002L14_A812ContagemResultadoChckLstLog_ChckLstDes, BC002L14_n812ContagemResultadoChckLstLog_ChckLstDes
               }
               , new Object[] {
               BC002L15_A823ContagemResultadoChckLstLog_PessoaCod, BC002L15_n823ContagemResultadoChckLstLog_PessoaCod
               }
               , new Object[] {
               BC002L16_A817ContagemResultadoChckLstLog_UsuarioNome, BC002L16_n817ContagemResultadoChckLstLog_UsuarioNome
               }
               , new Object[] {
               BC002L17_A820ContagemResultadoChckLstLog_Codigo, BC002L17_A814ContagemResultadoChckLstLog_DataHora, BC002L17_A812ContagemResultadoChckLstLog_ChckLstDes, BC002L17_n812ContagemResultadoChckLstLog_ChckLstDes, BC002L17_A817ContagemResultadoChckLstLog_UsuarioNome, BC002L17_n817ContagemResultadoChckLstLog_UsuarioNome, BC002L17_A824ContagemResultadoChckLstLog_Etapa, BC002L17_n824ContagemResultadoChckLstLog_Etapa, BC002L17_A426NaoConformidade_Codigo, BC002L17_n426NaoConformidade_Codigo,
               BC002L17_A1853ContagemResultadoChckLstLog_OSCodigo, BC002L17_A811ContagemResultadoChckLstLog_ChckLstCod, BC002L17_n811ContagemResultadoChckLstLog_ChckLstCod, BC002L17_A822ContagemResultadoChckLstLog_UsuarioCod, BC002L17_A823ContagemResultadoChckLstLog_PessoaCod, BC002L17_n823ContagemResultadoChckLstLog_PessoaCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z824ContagemResultadoChckLstLog_Etapa ;
      private short A824ContagemResultadoChckLstLog_Etapa ;
      private short RcdFound207 ;
      private int trnEnded ;
      private int Z820ContagemResultadoChckLstLog_Codigo ;
      private int A820ContagemResultadoChckLstLog_Codigo ;
      private int Z426NaoConformidade_Codigo ;
      private int A426NaoConformidade_Codigo ;
      private int Z1853ContagemResultadoChckLstLog_OSCodigo ;
      private int A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int Z811ContagemResultadoChckLstLog_ChckLstCod ;
      private int A811ContagemResultadoChckLstLog_ChckLstCod ;
      private int Z822ContagemResultadoChckLstLog_UsuarioCod ;
      private int A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int Z823ContagemResultadoChckLstLog_PessoaCod ;
      private int A823ContagemResultadoChckLstLog_PessoaCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z817ContagemResultadoChckLstLog_UsuarioNome ;
      private String A817ContagemResultadoChckLstLog_UsuarioNome ;
      private String sMode207 ;
      private DateTime Z814ContagemResultadoChckLstLog_DataHora ;
      private DateTime A814ContagemResultadoChckLstLog_DataHora ;
      private bool n812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool n817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool n824ContagemResultadoChckLstLog_Etapa ;
      private bool n426NaoConformidade_Codigo ;
      private bool n811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool n823ContagemResultadoChckLstLog_PessoaCod ;
      private bool Gx_longc ;
      private String Z812ContagemResultadoChckLstLog_ChckLstDes ;
      private String A812ContagemResultadoChckLstLog_ChckLstDes ;
      private SdtContagemResultadoChckLstLog bcContagemResultadoChckLstLog ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC002L9_A820ContagemResultadoChckLstLog_Codigo ;
      private DateTime[] BC002L9_A814ContagemResultadoChckLstLog_DataHora ;
      private String[] BC002L9_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] BC002L9_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private String[] BC002L9_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] BC002L9_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private short[] BC002L9_A824ContagemResultadoChckLstLog_Etapa ;
      private bool[] BC002L9_n824ContagemResultadoChckLstLog_Etapa ;
      private int[] BC002L9_A426NaoConformidade_Codigo ;
      private bool[] BC002L9_n426NaoConformidade_Codigo ;
      private int[] BC002L9_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int[] BC002L9_A811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool[] BC002L9_n811ContagemResultadoChckLstLog_ChckLstCod ;
      private int[] BC002L9_A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int[] BC002L9_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] BC002L9_n823ContagemResultadoChckLstLog_PessoaCod ;
      private int[] BC002L5_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private String[] BC002L6_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] BC002L6_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private int[] BC002L4_A426NaoConformidade_Codigo ;
      private bool[] BC002L4_n426NaoConformidade_Codigo ;
      private int[] BC002L7_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] BC002L7_n823ContagemResultadoChckLstLog_PessoaCod ;
      private String[] BC002L8_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] BC002L8_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private int[] BC002L10_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] BC002L3_A820ContagemResultadoChckLstLog_Codigo ;
      private DateTime[] BC002L3_A814ContagemResultadoChckLstLog_DataHora ;
      private short[] BC002L3_A824ContagemResultadoChckLstLog_Etapa ;
      private bool[] BC002L3_n824ContagemResultadoChckLstLog_Etapa ;
      private int[] BC002L3_A426NaoConformidade_Codigo ;
      private bool[] BC002L3_n426NaoConformidade_Codigo ;
      private int[] BC002L3_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int[] BC002L3_A811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool[] BC002L3_n811ContagemResultadoChckLstLog_ChckLstCod ;
      private int[] BC002L3_A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int[] BC002L2_A820ContagemResultadoChckLstLog_Codigo ;
      private DateTime[] BC002L2_A814ContagemResultadoChckLstLog_DataHora ;
      private short[] BC002L2_A824ContagemResultadoChckLstLog_Etapa ;
      private bool[] BC002L2_n824ContagemResultadoChckLstLog_Etapa ;
      private int[] BC002L2_A426NaoConformidade_Codigo ;
      private bool[] BC002L2_n426NaoConformidade_Codigo ;
      private int[] BC002L2_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int[] BC002L2_A811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool[] BC002L2_n811ContagemResultadoChckLstLog_ChckLstCod ;
      private int[] BC002L2_A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int[] BC002L11_A820ContagemResultadoChckLstLog_Codigo ;
      private String[] BC002L14_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] BC002L14_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private int[] BC002L15_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] BC002L15_n823ContagemResultadoChckLstLog_PessoaCod ;
      private String[] BC002L16_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] BC002L16_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private int[] BC002L17_A820ContagemResultadoChckLstLog_Codigo ;
      private DateTime[] BC002L17_A814ContagemResultadoChckLstLog_DataHora ;
      private String[] BC002L17_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] BC002L17_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private String[] BC002L17_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] BC002L17_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private short[] BC002L17_A824ContagemResultadoChckLstLog_Etapa ;
      private bool[] BC002L17_n824ContagemResultadoChckLstLog_Etapa ;
      private int[] BC002L17_A426NaoConformidade_Codigo ;
      private bool[] BC002L17_n426NaoConformidade_Codigo ;
      private int[] BC002L17_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int[] BC002L17_A811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool[] BC002L17_n811ContagemResultadoChckLstLog_ChckLstCod ;
      private int[] BC002L17_A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int[] BC002L17_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] BC002L17_n823ContagemResultadoChckLstLog_PessoaCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class contagemresultadochcklstlog_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC002L9 ;
          prmBC002L9 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L5 ;
          prmBC002L5 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L6 ;
          prmBC002L6 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L4 ;
          prmBC002L4 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L7 ;
          prmBC002L7 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L8 ;
          prmBC002L8 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L10 ;
          prmBC002L10 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L3 ;
          prmBC002L3 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L2 ;
          prmBC002L2 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L11 ;
          prmBC002L11 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoChckLstLog_Etapa",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L12 ;
          prmBC002L12 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoChckLstLog_Etapa",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L13 ;
          prmBC002L13 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L14 ;
          prmBC002L14 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L15 ;
          prmBC002L15 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L16 ;
          prmBC002L16 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC002L17 ;
          prmBC002L17 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC002L2", "SELECT [ContagemResultadoChckLstLog_Codigo], [ContagemResultadoChckLstLog_DataHora], [ContagemResultadoChckLstLog_Etapa], [NaoConformidade_Codigo], [ContagemResultadoChckLstLog_OSCodigo] AS ContagemResultadoChckLstLog_OSCodigo, [ContagemResultadoChckLstLog_ChckLstCod] AS ContagemResultadoChckLstLog_ChckLstCod, [ContagemResultadoChckLstLog_UsuarioCod] AS ContagemResultadoChckLstLog_UsuarioCod FROM [ContagemResultadoChckLstLog] WITH (UPDLOCK) WHERE [ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L2,1,0,true,false )
             ,new CursorDef("BC002L3", "SELECT [ContagemResultadoChckLstLog_Codigo], [ContagemResultadoChckLstLog_DataHora], [ContagemResultadoChckLstLog_Etapa], [NaoConformidade_Codigo], [ContagemResultadoChckLstLog_OSCodigo] AS ContagemResultadoChckLstLog_OSCodigo, [ContagemResultadoChckLstLog_ChckLstCod] AS ContagemResultadoChckLstLog_ChckLstCod, [ContagemResultadoChckLstLog_UsuarioCod] AS ContagemResultadoChckLstLog_UsuarioCod FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE [ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L3,1,0,true,false )
             ,new CursorDef("BC002L4", "SELECT [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L4,1,0,true,false )
             ,new CursorDef("BC002L5", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoChckLstLog_OSCodigo FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoChckLstLog_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L5,1,0,true,false )
             ,new CursorDef("BC002L6", "SELECT [CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @ContagemResultadoChckLstLog_ChckLstCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L6,1,0,true,false )
             ,new CursorDef("BC002L7", "SELECT [Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoChckLstLog_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L7,1,0,true,false )
             ,new CursorDef("BC002L8", "SELECT [Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoChckLstLog_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L8,1,0,true,false )
             ,new CursorDef("BC002L9", "SELECT TM1.[ContagemResultadoChckLstLog_Codigo], TM1.[ContagemResultadoChckLstLog_DataHora], T2.[CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes, T4.[Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome, TM1.[ContagemResultadoChckLstLog_Etapa], TM1.[NaoConformidade_Codigo], TM1.[ContagemResultadoChckLstLog_OSCodigo] AS ContagemResultadoChckLstLog_OSCodigo, TM1.[ContagemResultadoChckLstLog_ChckLstCod] AS ContagemResultadoChckLstLog_ChckLstCod, TM1.[ContagemResultadoChckLstLog_UsuarioCod] AS ContagemResultadoChckLstLog_UsuarioCod, T3.[Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod FROM ((([ContagemResultadoChckLstLog] TM1 WITH (NOLOCK) LEFT JOIN [CheckList] T2 WITH (NOLOCK) ON T2.[CheckList_Codigo] = TM1.[ContagemResultadoChckLstLog_ChckLstCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = TM1.[ContagemResultadoChckLstLog_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo ORDER BY TM1.[ContagemResultadoChckLstLog_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L9,100,0,true,false )
             ,new CursorDef("BC002L10", "SELECT [ContagemResultadoChckLstLog_Codigo] FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE [ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L10,1,0,true,false )
             ,new CursorDef("BC002L11", "INSERT INTO [ContagemResultadoChckLstLog]([ContagemResultadoChckLstLog_DataHora], [ContagemResultadoChckLstLog_Etapa], [NaoConformidade_Codigo], [ContagemResultadoChckLstLog_OSCodigo], [ContagemResultadoChckLstLog_ChckLstCod], [ContagemResultadoChckLstLog_UsuarioCod]) VALUES(@ContagemResultadoChckLstLog_DataHora, @ContagemResultadoChckLstLog_Etapa, @NaoConformidade_Codigo, @ContagemResultadoChckLstLog_OSCodigo, @ContagemResultadoChckLstLog_ChckLstCod, @ContagemResultadoChckLstLog_UsuarioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC002L11)
             ,new CursorDef("BC002L12", "UPDATE [ContagemResultadoChckLstLog] SET [ContagemResultadoChckLstLog_DataHora]=@ContagemResultadoChckLstLog_DataHora, [ContagemResultadoChckLstLog_Etapa]=@ContagemResultadoChckLstLog_Etapa, [NaoConformidade_Codigo]=@NaoConformidade_Codigo, [ContagemResultadoChckLstLog_OSCodigo]=@ContagemResultadoChckLstLog_OSCodigo, [ContagemResultadoChckLstLog_ChckLstCod]=@ContagemResultadoChckLstLog_ChckLstCod, [ContagemResultadoChckLstLog_UsuarioCod]=@ContagemResultadoChckLstLog_UsuarioCod  WHERE [ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo", GxErrorMask.GX_NOMASK,prmBC002L12)
             ,new CursorDef("BC002L13", "DELETE FROM [ContagemResultadoChckLstLog]  WHERE [ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo", GxErrorMask.GX_NOMASK,prmBC002L13)
             ,new CursorDef("BC002L14", "SELECT [CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @ContagemResultadoChckLstLog_ChckLstCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L14,1,0,true,false )
             ,new CursorDef("BC002L15", "SELECT [Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoChckLstLog_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L15,1,0,true,false )
             ,new CursorDef("BC002L16", "SELECT [Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoChckLstLog_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L16,1,0,true,false )
             ,new CursorDef("BC002L17", "SELECT TM1.[ContagemResultadoChckLstLog_Codigo], TM1.[ContagemResultadoChckLstLog_DataHora], T2.[CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes, T4.[Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome, TM1.[ContagemResultadoChckLstLog_Etapa], TM1.[NaoConformidade_Codigo], TM1.[ContagemResultadoChckLstLog_OSCodigo] AS ContagemResultadoChckLstLog_OSCodigo, TM1.[ContagemResultadoChckLstLog_ChckLstCod] AS ContagemResultadoChckLstLog_ChckLstCod, TM1.[ContagemResultadoChckLstLog_UsuarioCod] AS ContagemResultadoChckLstLog_UsuarioCod, T3.[Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod FROM ((([ContagemResultadoChckLstLog] TM1 WITH (NOLOCK) LEFT JOIN [CheckList] T2 WITH (NOLOCK) ON T2.[CheckList_Codigo] = TM1.[ContagemResultadoChckLstLog_ChckLstCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = TM1.[ContagemResultadoChckLstLog_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo ORDER BY TM1.[ContagemResultadoChckLstLog_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC002L17,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                stmt.SetParameter(4, (int)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                stmt.SetParameter(6, (int)parms[8]);
                return;
             case 10 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                stmt.SetParameter(4, (int)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                stmt.SetParameter(6, (int)parms[8]);
                stmt.SetParameter(7, (int)parms[9]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
