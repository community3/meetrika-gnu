/*
               File: PRC_ExisteIntegridadeReferencial
        Description: Existe Integridade Referenciado
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:12.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_existeintegridadereferencial : GXProcedure
   {
      public prc_existeintegridadereferencial( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_existeintegridadereferencial( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicos_Codigo ,
                           String aP1_Tabela ,
                           out bool aP2_Flag )
      {
         this.AV12ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8Tabela = aP1_Tabela;
         this.AV10Flag = false ;
         initialize();
         executePrivate();
         aP2_Flag=this.AV10Flag;
      }

      public bool executeUdp( int aP0_ContratoServicos_Codigo ,
                              String aP1_Tabela )
      {
         this.AV12ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV8Tabela = aP1_Tabela;
         this.AV10Flag = false ;
         initialize();
         executePrivate();
         aP2_Flag=this.AV10Flag;
         return AV10Flag ;
      }

      public void executeSubmit( int aP0_ContratoServicos_Codigo ,
                                 String aP1_Tabela ,
                                 out bool aP2_Flag )
      {
         prc_existeintegridadereferencial objprc_existeintegridadereferencial;
         objprc_existeintegridadereferencial = new prc_existeintegridadereferencial();
         objprc_existeintegridadereferencial.AV12ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_existeintegridadereferencial.AV8Tabela = aP1_Tabela;
         objprc_existeintegridadereferencial.AV10Flag = false ;
         objprc_existeintegridadereferencial.context.SetSubmitInitialConfig(context);
         objprc_existeintegridadereferencial.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_existeintegridadereferencial);
         aP2_Flag=this.AV10Flag;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_existeintegridadereferencial)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( StringUtil.StrCmp(AV8Tabela, "Servico") == 0 )
         {
            /* Using cursor P009J2 */
            pr_default.execute(0, new Object[] {AV12ContratoServicos_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1553ContagemResultado_CntSrvCod = P009J2_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P009J2_n1553ContagemResultado_CntSrvCod[0];
               A456ContagemResultado_Codigo = P009J2_A456ContagemResultado_Codigo[0];
               AV10Flag = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(0);
            }
            pr_default.close(0);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009J2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P009J2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P009J2_A456ContagemResultado_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_existeintegridadereferencial__default(),
            new Object[][] {
                new Object[] {
               P009J2_A1553ContagemResultado_CntSrvCod, P009J2_n1553ContagemResultado_CntSrvCod, P009J2_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV12ContratoServicos_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A456ContagemResultado_Codigo ;
      private String AV8Tabela ;
      private String scmdbuf ;
      private bool AV10Flag ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P009J2_A1553ContagemResultado_CntSrvCod ;
      private bool[] P009J2_n1553ContagemResultado_CntSrvCod ;
      private int[] P009J2_A456ContagemResultado_Codigo ;
      private bool aP2_Flag ;
   }

   public class prc_existeintegridadereferencial__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009J2 ;
          prmP009J2 = new Object[] {
          new Object[] {"@AV12ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009J2", "SELECT TOP 1 [ContagemResultado_CntSrvCod], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_CntSrvCod] = @AV12ContratoServicos_Codigo ORDER BY [ContagemResultado_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009J2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
