/*
               File: Atributos
        Description: Atributos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:45.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class atributos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"TABELA_SISTEMACOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLATABELA_SISTEMACOD1340( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel12"+"_"+"") == 0 )
         {
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_21") == 0 )
         {
            A356Atributos_TabelaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_21( A356Atributos_TabelaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_22") == 0 )
         {
            A747Atributos_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n747Atributos_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A747Atributos_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A747Atributos_MelhoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_22( A747Atributos_MelhoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Atributos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Atributos_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vATRIBUTOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Atributos_Codigo), "ZZZZZ9")));
               AV14Sistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Sistema_Codigo), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbAtributos_TipoDados.Name = "ATRIBUTOS_TIPODADOS";
         cmbAtributos_TipoDados.WebTags = "";
         cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
         cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
         cmbAtributos_TipoDados.addItem("C", "Character", 0);
         cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
         cmbAtributos_TipoDados.addItem("D", "Date", 0);
         cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
         cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
         cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
         cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
         if ( cmbAtributos_TipoDados.ItemCount > 0 )
         {
            A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
            n178Atributos_TipoDados = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
         }
         chkAtributos_PK.Name = "ATRIBUTOS_PK";
         chkAtributos_PK.WebTags = "";
         chkAtributos_PK.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_PK_Internalname, "TitleCaption", chkAtributos_PK.Caption);
         chkAtributos_PK.CheckedValue = "false";
         chkAtributos_FK.Name = "ATRIBUTOS_FK";
         chkAtributos_FK.WebTags = "";
         chkAtributos_FK.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_FK_Internalname, "TitleCaption", chkAtributos_FK.Caption);
         chkAtributos_FK.CheckedValue = "false";
         dynTabela_SistemaCod.Name = "TABELA_SISTEMACOD";
         dynTabela_SistemaCod.WebTags = "";
         chkAtributos_Ativo.Name = "ATRIBUTOS_ATIVO";
         chkAtributos_Ativo.WebTags = "";
         chkAtributos_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_Ativo_Internalname, "TitleCaption", chkAtributos_Ativo.Caption);
         chkAtributos_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Atributos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtAtributos_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public atributos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public atributos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Atributos_Codigo ,
                           ref int aP2_Sistema_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Atributos_Codigo = aP1_Atributos_Codigo;
         this.AV14Sistema_Codigo = aP2_Sistema_Codigo;
         executePrivate();
         aP2_Sistema_Codigo=this.AV14Sistema_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbAtributos_TipoDados = new GXCombobox();
         chkAtributos_PK = new GXCheckbox();
         chkAtributos_FK = new GXCheckbox();
         dynTabela_SistemaCod = new GXCombobox();
         chkAtributos_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbAtributos_TipoDados.ItemCount > 0 )
         {
            A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
            n178Atributos_TipoDados = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
         }
         if ( dynTabela_SistemaCod.ItemCount > 0 )
         {
            A190Tabela_SistemaCod = (int)(NumberUtil.Val( dynTabela_SistemaCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0))), "."));
            n190Tabela_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1340( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1340e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAtributos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ",", "")), ((edtAtributos_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAtributos_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtAtributos_Codigo_Visible, edtAtributos_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Atributos.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1340( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_1340( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_1340e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_56_1340( true) ;
         }
         return  ;
      }

      protected void wb_table3_56_1340e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, "Java", "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1340e( true) ;
         }
         else
         {
            wb_table1_2_1340e( false) ;
         }
      }

      protected void wb_table3_56_1340( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtneliminar_Internalname, "", "Eliminar", bttBtneliminar_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtneliminar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOELIMINAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, bttBtnfechar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtncopiarcolaratributos_Internalname, "", "Copiar e Colar", bttBtncopiarcolaratributos_Jsonclick, 5, "Copiar e Colar", "", StyleString, ClassString, bttBtncopiarcolaratributos_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOCOPIARCOLARATRIBUTOS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "ActionButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnimportarexcel_Internalname, "", "Importar Excel", bttBtnimportarexcel_Jsonclick, 5, "Importar Excel", "", StyleString, ClassString, bttBtnimportarexcel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOIMPORTAREXCEL\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_56_1340e( true) ;
         }
         else
         {
            wb_table3_56_1340e( false) ;
         }
      }

      protected void wb_table2_8_1340( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1340( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1340e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_1340e( true) ;
         }
         else
         {
            wb_table2_8_1340e( false) ;
         }
      }

      protected void wb_table4_13_1340( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_nome_Internalname, "Nome", "", "", lblTextblockatributos_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAtributos_Nome_Internalname, StringUtil.RTrim( A177Atributos_Nome), StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAtributos_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAtributos_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_tipodados_Internalname, "Tipo de Dados", "", "", lblTextblockatributos_tipodados_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_23_1340( true) ;
         }
         return  ;
      }

      protected void wb_table5_23_1340e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_descricao_Internalname, "Descri��o", "", "", lblTextblockatributos_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtAtributos_Descricao_Internalname, A179Atributos_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", 0, 1, edtAtributos_Descricao_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_sistemacod_Internalname, "Sistema", "", "", lblTextblocktabela_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynTabela_SistemaCod, dynTabela_SistemaCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)), 1, dynTabela_SistemaCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynTabela_SistemaCod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", "", "", true, "HLP_Atributos.htm");
            dynTabela_SistemaCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTabela_SistemaCod_Internalname, "Values", (String)(dynTabela_SistemaCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_ativo_Internalname, "Ativo", "", "", lblTextblockatributos_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "AttSemBordaCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkAtributos_Ativo_Internalname, StringUtil.BoolToStr( A180Atributos_Ativo), "", "", 1, chkAtributos_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(53, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1340e( true) ;
         }
         else
         {
            wb_table4_13_1340e( false) ;
         }
      }

      protected void wb_table5_23_1340( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedatributos_tipodados_Internalname, tblTablemergedatributos_tipodados_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbAtributos_TipoDados, cmbAtributos_TipoDados_Internalname, StringUtil.RTrim( A178Atributos_TipoDados), 1, cmbAtributos_TipoDados_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbAtributos_TipoDados.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "", true, "HLP_Atributos.htm");
            cmbAtributos_TipoDados.CurrentValue = StringUtil.RTrim( A178Atributos_TipoDados);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAtributos_TipoDados_Internalname, "Values", (String)(cmbAtributos_TipoDados.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_detalhes_Internalname, "Tamanho", "", "", lblTextblockatributos_detalhes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAtributos_Detalhes_Internalname, StringUtil.RTrim( A390Atributos_Detalhes), StringUtil.RTrim( context.localUtil.Format( A390Atributos_Detalhes, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAtributos_Detalhes_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAtributos_Detalhes_Enabled, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_pk_Internalname, "PK", "", "", lblTextblockatributos_pk_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkAtributos_PK_Internalname, StringUtil.BoolToStr( A400Atributos_PK), "", "", 1, chkAtributos_PK.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(34, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_fk_Internalname, "FK", "", "", lblTextblockatributos_fk_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Atributos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkAtributos_FK_Internalname, StringUtil.BoolToStr( A401Atributos_FK), "", "", 1, chkAtributos_FK.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(38, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_23_1340e( true) ;
         }
         else
         {
            wb_table5_23_1340e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11132 */
         E11132 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A177Atributos_Nome = StringUtil.Upper( cgiGet( edtAtributos_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A177Atributos_Nome", A177Atributos_Nome);
               cmbAtributos_TipoDados.CurrentValue = cgiGet( cmbAtributos_TipoDados_Internalname);
               A178Atributos_TipoDados = cgiGet( cmbAtributos_TipoDados_Internalname);
               n178Atributos_TipoDados = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
               n178Atributos_TipoDados = (String.IsNullOrEmpty(StringUtil.RTrim( A178Atributos_TipoDados)) ? true : false);
               A390Atributos_Detalhes = cgiGet( edtAtributos_Detalhes_Internalname);
               n390Atributos_Detalhes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A390Atributos_Detalhes", A390Atributos_Detalhes);
               n390Atributos_Detalhes = (String.IsNullOrEmpty(StringUtil.RTrim( A390Atributos_Detalhes)) ? true : false);
               A400Atributos_PK = StringUtil.StrToBool( cgiGet( chkAtributos_PK_Internalname));
               n400Atributos_PK = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A400Atributos_PK", A400Atributos_PK);
               n400Atributos_PK = ((false==A400Atributos_PK) ? true : false);
               A401Atributos_FK = StringUtil.StrToBool( cgiGet( chkAtributos_FK_Internalname));
               n401Atributos_FK = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A401Atributos_FK", A401Atributos_FK);
               n401Atributos_FK = ((false==A401Atributos_FK) ? true : false);
               A179Atributos_Descricao = cgiGet( edtAtributos_Descricao_Internalname);
               n179Atributos_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A179Atributos_Descricao", A179Atributos_Descricao);
               n179Atributos_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A179Atributos_Descricao)) ? true : false);
               dynTabela_SistemaCod.CurrentValue = cgiGet( dynTabela_SistemaCod_Internalname);
               A190Tabela_SistemaCod = (int)(NumberUtil.Val( cgiGet( dynTabela_SistemaCod_Internalname), "."));
               n190Tabela_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
               A180Atributos_Ativo = StringUtil.StrToBool( cgiGet( chkAtributos_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A180Atributos_Ativo", A180Atributos_Ativo);
               A176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAtributos_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
               /* Read saved values. */
               Z176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z176Atributos_Codigo"), ",", "."));
               Z177Atributos_Nome = cgiGet( "Z177Atributos_Nome");
               Z390Atributos_Detalhes = cgiGet( "Z390Atributos_Detalhes");
               n390Atributos_Detalhes = (String.IsNullOrEmpty(StringUtil.RTrim( A390Atributos_Detalhes)) ? true : false);
               Z178Atributos_TipoDados = cgiGet( "Z178Atributos_TipoDados");
               n178Atributos_TipoDados = (String.IsNullOrEmpty(StringUtil.RTrim( A178Atributos_TipoDados)) ? true : false);
               Z400Atributos_PK = StringUtil.StrToBool( cgiGet( "Z400Atributos_PK"));
               n400Atributos_PK = ((false==A400Atributos_PK) ? true : false);
               Z401Atributos_FK = StringUtil.StrToBool( cgiGet( "Z401Atributos_FK"));
               n401Atributos_FK = ((false==A401Atributos_FK) ? true : false);
               Z180Atributos_Ativo = StringUtil.StrToBool( cgiGet( "Z180Atributos_Ativo"));
               Z356Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( "Z356Atributos_TabelaCod"), ",", "."));
               Z747Atributos_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z747Atributos_MelhoraCod"), ",", "."));
               n747Atributos_MelhoraCod = ((0==A747Atributos_MelhoraCod) ? true : false);
               A356Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( "Z356Atributos_TabelaCod"), ",", "."));
               A747Atributos_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z747Atributos_MelhoraCod"), ",", "."));
               n747Atributos_MelhoraCod = false;
               n747Atributos_MelhoraCod = ((0==A747Atributos_MelhoraCod) ? true : false);
               O177Atributos_Nome = cgiGet( "O177Atributos_Nome");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N356Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( "N356Atributos_TabelaCod"), ",", "."));
               N747Atributos_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "N747Atributos_MelhoraCod"), ",", "."));
               n747Atributos_MelhoraCod = ((0==A747Atributos_MelhoraCod) ? true : false);
               AV7Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( "vATRIBUTOS_CODIGO"), ",", "."));
               AV13Insert_Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_ATRIBUTOS_TABELACOD"), ",", "."));
               A356Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( "ATRIBUTOS_TABELACOD"), ",", "."));
               AV22Insert_Atributos_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_ATRIBUTOS_MELHORACOD"), ",", "."));
               A747Atributos_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "ATRIBUTOS_MELHORACOD"), ",", "."));
               n747Atributos_MelhoraCod = ((0==A747Atributos_MelhoraCod) ? true : false);
               A357Atributos_TabelaNom = cgiGet( "ATRIBUTOS_TABELANOM");
               n357Atributos_TabelaNom = false;
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               AV23Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Atributos";
               A176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAtributos_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A747Atributos_MelhoraCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A176Atributos_Codigo != Z176Atributos_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("atributos:[SecurityCheckFailed value for]"+"Atributos_Codigo:"+context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("atributos:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("atributos:[SecurityCheckFailed value for]"+"Atributos_TabelaCod:"+context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9"));
                  GXUtil.WriteLog("atributos:[SecurityCheckFailed value for]"+"Atributos_MelhoraCod:"+context.localUtil.Format( (decimal)(A747Atributos_MelhoraCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               forbiddenHiddens2 = "";
               if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
               {
                  forbiddenHiddens2 = forbiddenHiddens2 + context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9");
               }
               hsh2 = cgiGet( "hsh2");
               if ( ( ! ( ( A176Atributos_Codigo != Z176Atributos_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens2, hsh2, GXKey) )
               {
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A176Atributos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode40 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode40;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound40 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_130( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "ATRIBUTOS_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtAtributos_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11132 */
                           E11132 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12132 */
                           E12132 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOELIMINAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13132 */
                           E13132 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14132 */
                           E14132 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCOPIARCOLARATRIBUTOS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15132 */
                           E15132 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOIMPORTAREXCEL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16132 */
                           E16132 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12132 */
            E12132 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1340( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1340( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_130( )
      {
         BeforeValidate1340( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1340( ) ;
            }
            else
            {
               CheckExtendedTable1340( ) ;
               CloseExtendedTableCursors1340( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption130( )
      {
      }

      protected void E11132( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV23Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV24GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24GXV1), 8, 0)));
            while ( AV24GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV24GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Atributos_TabelaCod") == 0 )
               {
                  AV13Insert_Atributos_TabelaCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_Atributos_TabelaCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Atributos_MelhoraCod") == 0 )
               {
                  AV22Insert_Atributos_MelhoraCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Insert_Atributos_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Insert_Atributos_MelhoraCod), 6, 0)));
               }
               AV24GXV1 = (int)(AV24GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24GXV1), 8, 0)));
            }
         }
         edtAtributos_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAtributos_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAtributos_Codigo_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
      }

      protected void E12132( )
      {
         /* After Trn Routine */
         AV10WebSession.Remove("FiltroRecebido");
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            context.wjLoc = formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV14Sistema_Codigo);
            context.wjLocDisableFrm = 1;
         }
         else
         {
            context.wjLoc = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV14Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV14Sistema_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwatributos.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)AV14Sistema_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E13132( )
      {
         /* 'DoEliminar' Routine */
         new prc_desativarregistro(context ).execute(  "Atr",  AV7Atributos_Codigo,  0) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Atributos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vATRIBUTOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Atributos_Codigo), "ZZZZZ9")));
         context.setWebReturnParms(new Object[] {(int)AV14Sistema_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E14132( )
      {
         /* 'DoFechar' Routine */
         AV10WebSession.Remove("FiltroRecebido");
         context.wjLoc = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
      }

      protected void E15132( )
      {
         /* 'DoCopiarColarAtributos' Routine */
         context.wjLoc = formatLink("wp_copiarcolaratributos.aspx") + "?" + UrlEncode("" +A190Tabela_SistemaCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E16132( )
      {
         /* 'DoImportarExcel' Routine */
         new prc_atributosexcel(context ).execute(  AV8WWPContext.gxTpr_Userid,  AV14Sistema_Codigo, out  AV15Linhas) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Sistema_Codigo), 6, 0)));
      }

      protected void ZM1340( short GX_JID )
      {
         if ( ( GX_JID == 20 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z177Atributos_Nome = T00133_A177Atributos_Nome[0];
               Z390Atributos_Detalhes = T00133_A390Atributos_Detalhes[0];
               Z178Atributos_TipoDados = T00133_A178Atributos_TipoDados[0];
               Z400Atributos_PK = T00133_A400Atributos_PK[0];
               Z401Atributos_FK = T00133_A401Atributos_FK[0];
               Z180Atributos_Ativo = T00133_A180Atributos_Ativo[0];
               Z356Atributos_TabelaCod = T00133_A356Atributos_TabelaCod[0];
               Z747Atributos_MelhoraCod = T00133_A747Atributos_MelhoraCod[0];
            }
            else
            {
               Z177Atributos_Nome = A177Atributos_Nome;
               Z390Atributos_Detalhes = A390Atributos_Detalhes;
               Z178Atributos_TipoDados = A178Atributos_TipoDados;
               Z400Atributos_PK = A400Atributos_PK;
               Z401Atributos_FK = A401Atributos_FK;
               Z180Atributos_Ativo = A180Atributos_Ativo;
               Z356Atributos_TabelaCod = A356Atributos_TabelaCod;
               Z747Atributos_MelhoraCod = A747Atributos_MelhoraCod;
            }
         }
         if ( GX_JID == -20 )
         {
            Z176Atributos_Codigo = A176Atributos_Codigo;
            Z177Atributos_Nome = A177Atributos_Nome;
            Z390Atributos_Detalhes = A390Atributos_Detalhes;
            Z179Atributos_Descricao = A179Atributos_Descricao;
            Z178Atributos_TipoDados = A178Atributos_TipoDados;
            Z400Atributos_PK = A400Atributos_PK;
            Z401Atributos_FK = A401Atributos_FK;
            Z180Atributos_Ativo = A180Atributos_Ativo;
            Z356Atributos_TabelaCod = A356Atributos_TabelaCod;
            Z747Atributos_MelhoraCod = A747Atributos_MelhoraCod;
            Z357Atributos_TabelaNom = A357Atributos_TabelaNom;
            Z190Tabela_SistemaCod = A190Tabela_SistemaCod;
         }
      }

      protected void standaloneNotModal( )
      {
         GXATABELA_SISTEMACOD_html1340( ) ;
         edtAtributos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAtributos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAtributos_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV23Pgmname = "Atributos";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Pgmname", AV23Pgmname);
         edtAtributos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAtributos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAtributos_Codigo_Enabled), 5, 0)));
         if ( ! (0==AV7Atributos_Codigo) )
         {
            A176Atributos_Codigo = AV7Atributos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         bttBtncopiarcolaratributos_Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtncopiarcolaratributos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtncopiarcolaratributos_Visible), 5, 0)));
         bttBtn_trn_enter_Visible = (!( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV22Insert_Atributos_MelhoraCod) )
         {
            A747Atributos_MelhoraCod = AV22Insert_Atributos_MelhoraCod;
            n747Atributos_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A747Atributos_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A747Atributos_MelhoraCod), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Atributos_TabelaCod) )
         {
            A356Atributos_TabelaCod = AV13Insert_Atributos_TabelaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
         }
         GXt_boolean1 = false;
         new prc_setnaopodedeletar(context ).execute(  "Atr",  AV7Atributos_Codigo, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Atributos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vATRIBUTOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Atributos_Codigo), "ZZZZZ9")));
         bttBtneliminar_Visible = (( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) &&GXt_boolean1 ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtneliminar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtneliminar_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A180Atributos_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A180Atributos_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A180Atributos_Ativo", A180Atributos_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T00134 */
            pr_default.execute(2, new Object[] {A356Atributos_TabelaCod});
            A357Atributos_TabelaNom = T00134_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = T00134_n357Atributos_TabelaNom[0];
            A190Tabela_SistemaCod = T00134_A190Tabela_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
            n190Tabela_SistemaCod = T00134_n190Tabela_SistemaCod[0];
            pr_default.close(2);
            Dvpanel_tableattributes_Title = "Atributo da tabela: "+A357Atributos_TabelaNom;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
            AV10WebSession.Set("Tabela_Codigo", StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0));
         }
      }

      protected void Load1340( )
      {
         /* Using cursor T00136 */
         pr_default.execute(4, new Object[] {A176Atributos_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound40 = 1;
            A177Atributos_Nome = T00136_A177Atributos_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A177Atributos_Nome", A177Atributos_Nome);
            A390Atributos_Detalhes = T00136_A390Atributos_Detalhes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A390Atributos_Detalhes", A390Atributos_Detalhes);
            n390Atributos_Detalhes = T00136_n390Atributos_Detalhes[0];
            A179Atributos_Descricao = T00136_A179Atributos_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A179Atributos_Descricao", A179Atributos_Descricao);
            n179Atributos_Descricao = T00136_n179Atributos_Descricao[0];
            A178Atributos_TipoDados = T00136_A178Atributos_TipoDados[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
            n178Atributos_TipoDados = T00136_n178Atributos_TipoDados[0];
            A400Atributos_PK = T00136_A400Atributos_PK[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A400Atributos_PK", A400Atributos_PK);
            n400Atributos_PK = T00136_n400Atributos_PK[0];
            A401Atributos_FK = T00136_A401Atributos_FK[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A401Atributos_FK", A401Atributos_FK);
            n401Atributos_FK = T00136_n401Atributos_FK[0];
            A357Atributos_TabelaNom = T00136_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = T00136_n357Atributos_TabelaNom[0];
            A180Atributos_Ativo = T00136_A180Atributos_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A180Atributos_Ativo", A180Atributos_Ativo);
            A356Atributos_TabelaCod = T00136_A356Atributos_TabelaCod[0];
            A747Atributos_MelhoraCod = T00136_A747Atributos_MelhoraCod[0];
            n747Atributos_MelhoraCod = T00136_n747Atributos_MelhoraCod[0];
            A190Tabela_SistemaCod = T00136_A190Tabela_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
            n190Tabela_SistemaCod = T00136_n190Tabela_SistemaCod[0];
            ZM1340( -20) ;
         }
         pr_default.close(4);
         OnLoadActions1340( ) ;
      }

      protected void OnLoadActions1340( )
      {
         Dvpanel_tableattributes_Title = "Atributo da tabela: "+A357Atributos_TabelaNom;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
         AV10WebSession.Set("Tabela_Codigo", StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0));
      }

      protected void CheckExtendedTable1340( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A177Atributos_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "ATRIBUTOS_NOME");
            AnyError = 1;
            GX_FocusControl = edtAtributos_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A178Atributos_TipoDados, "N") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "C") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "VC") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "D") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "DT") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "Bool") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "Blob") == 0 ) || ( StringUtil.StrCmp(A178Atributos_TipoDados, "Outr") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A178Atributos_TipoDados)) ) )
         {
            GX_msglist.addItem("Campo Tipo de Dados fora do intervalo", "OutOfRange", 1, "ATRIBUTOS_TIPODADOS");
            AnyError = 1;
            GX_FocusControl = cmbAtributos_TipoDados_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00134 */
         pr_default.execute(2, new Object[] {A356Atributos_TabelaCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Atributos_Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A357Atributos_TabelaNom = T00134_A357Atributos_TabelaNom[0];
         n357Atributos_TabelaNom = T00134_n357Atributos_TabelaNom[0];
         A190Tabela_SistemaCod = T00134_A190Tabela_SistemaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
         n190Tabela_SistemaCod = T00134_n190Tabela_SistemaCod[0];
         pr_default.close(2);
         Dvpanel_tableattributes_Title = "Atributo da tabela: "+A357Atributos_TabelaNom;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
         AV10WebSession.Set("Tabela_Codigo", StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0));
         /* Using cursor T00135 */
         pr_default.execute(3, new Object[] {n747Atributos_MelhoraCod, A747Atributos_MelhoraCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A747Atributos_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Atributo_Atributo Melhora'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1340( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_21( int A356Atributos_TabelaCod )
      {
         /* Using cursor T00137 */
         pr_default.execute(5, new Object[] {A356Atributos_TabelaCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Atributos_Tabela'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A357Atributos_TabelaNom = T00137_A357Atributos_TabelaNom[0];
         n357Atributos_TabelaNom = T00137_n357Atributos_TabelaNom[0];
         A190Tabela_SistemaCod = T00137_A190Tabela_SistemaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
         n190Tabela_SistemaCod = T00137_n190Tabela_SistemaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A357Atributos_TabelaNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_22( int A747Atributos_MelhoraCod )
      {
         /* Using cursor T00138 */
         pr_default.execute(6, new Object[] {n747Atributos_MelhoraCod, A747Atributos_MelhoraCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A747Atributos_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Atributo_Atributo Melhora'.", "ForeignKeyNotFound", 1, "");
               AnyError = 1;
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey1340( )
      {
         /* Using cursor T00139 */
         pr_default.execute(7, new Object[] {A176Atributos_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound40 = 1;
         }
         else
         {
            RcdFound40 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00133 */
         pr_default.execute(1, new Object[] {A176Atributos_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1340( 20) ;
            RcdFound40 = 1;
            A176Atributos_Codigo = T00133_A176Atributos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
            A177Atributos_Nome = T00133_A177Atributos_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A177Atributos_Nome", A177Atributos_Nome);
            A390Atributos_Detalhes = T00133_A390Atributos_Detalhes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A390Atributos_Detalhes", A390Atributos_Detalhes);
            n390Atributos_Detalhes = T00133_n390Atributos_Detalhes[0];
            A179Atributos_Descricao = T00133_A179Atributos_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A179Atributos_Descricao", A179Atributos_Descricao);
            n179Atributos_Descricao = T00133_n179Atributos_Descricao[0];
            A178Atributos_TipoDados = T00133_A178Atributos_TipoDados[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
            n178Atributos_TipoDados = T00133_n178Atributos_TipoDados[0];
            A400Atributos_PK = T00133_A400Atributos_PK[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A400Atributos_PK", A400Atributos_PK);
            n400Atributos_PK = T00133_n400Atributos_PK[0];
            A401Atributos_FK = T00133_A401Atributos_FK[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A401Atributos_FK", A401Atributos_FK);
            n401Atributos_FK = T00133_n401Atributos_FK[0];
            A180Atributos_Ativo = T00133_A180Atributos_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A180Atributos_Ativo", A180Atributos_Ativo);
            A356Atributos_TabelaCod = T00133_A356Atributos_TabelaCod[0];
            A747Atributos_MelhoraCod = T00133_A747Atributos_MelhoraCod[0];
            n747Atributos_MelhoraCod = T00133_n747Atributos_MelhoraCod[0];
            O177Atributos_Nome = A177Atributos_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A177Atributos_Nome", A177Atributos_Nome);
            Z176Atributos_Codigo = A176Atributos_Codigo;
            sMode40 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1340( ) ;
            if ( AnyError == 1 )
            {
               RcdFound40 = 0;
               InitializeNonKey1340( ) ;
            }
            Gx_mode = sMode40;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound40 = 0;
            InitializeNonKey1340( ) ;
            sMode40 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode40;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1340( ) ;
         if ( RcdFound40 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound40 = 0;
         /* Using cursor T001310 */
         pr_default.execute(8, new Object[] {A176Atributos_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T001310_A176Atributos_Codigo[0] < A176Atributos_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T001310_A176Atributos_Codigo[0] > A176Atributos_Codigo ) ) )
            {
               A176Atributos_Codigo = T001310_A176Atributos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
               RcdFound40 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound40 = 0;
         /* Using cursor T001311 */
         pr_default.execute(9, new Object[] {A176Atributos_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T001311_A176Atributos_Codigo[0] > A176Atributos_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T001311_A176Atributos_Codigo[0] < A176Atributos_Codigo ) ) )
            {
               A176Atributos_Codigo = T001311_A176Atributos_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
               RcdFound40 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1340( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtAtributos_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1340( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound40 == 1 )
            {
               if ( A176Atributos_Codigo != Z176Atributos_Codigo )
               {
                  A176Atributos_Codigo = Z176Atributos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "ATRIBUTOS_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAtributos_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtAtributos_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1340( ) ;
                  GX_FocusControl = edtAtributos_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A176Atributos_Codigo != Z176Atributos_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtAtributos_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1340( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "ATRIBUTOS_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtAtributos_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtAtributos_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1340( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A176Atributos_Codigo != Z176Atributos_Codigo )
         {
            A176Atributos_Codigo = Z176Atributos_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "ATRIBUTOS_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAtributos_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtAtributos_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1340( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00132 */
            pr_default.execute(0, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Atributos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z177Atributos_Nome, T00132_A177Atributos_Nome[0]) != 0 ) || ( StringUtil.StrCmp(Z390Atributos_Detalhes, T00132_A390Atributos_Detalhes[0]) != 0 ) || ( StringUtil.StrCmp(Z178Atributos_TipoDados, T00132_A178Atributos_TipoDados[0]) != 0 ) || ( Z400Atributos_PK != T00132_A400Atributos_PK[0] ) || ( Z401Atributos_FK != T00132_A401Atributos_FK[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z180Atributos_Ativo != T00132_A180Atributos_Ativo[0] ) || ( Z356Atributos_TabelaCod != T00132_A356Atributos_TabelaCod[0] ) || ( Z747Atributos_MelhoraCod != T00132_A747Atributos_MelhoraCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z177Atributos_Nome, T00132_A177Atributos_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("atributos:[seudo value changed for attri]"+"Atributos_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z177Atributos_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00132_A177Atributos_Nome[0]);
               }
               if ( StringUtil.StrCmp(Z390Atributos_Detalhes, T00132_A390Atributos_Detalhes[0]) != 0 )
               {
                  GXUtil.WriteLog("atributos:[seudo value changed for attri]"+"Atributos_Detalhes");
                  GXUtil.WriteLogRaw("Old: ",Z390Atributos_Detalhes);
                  GXUtil.WriteLogRaw("Current: ",T00132_A390Atributos_Detalhes[0]);
               }
               if ( StringUtil.StrCmp(Z178Atributos_TipoDados, T00132_A178Atributos_TipoDados[0]) != 0 )
               {
                  GXUtil.WriteLog("atributos:[seudo value changed for attri]"+"Atributos_TipoDados");
                  GXUtil.WriteLogRaw("Old: ",Z178Atributos_TipoDados);
                  GXUtil.WriteLogRaw("Current: ",T00132_A178Atributos_TipoDados[0]);
               }
               if ( Z400Atributos_PK != T00132_A400Atributos_PK[0] )
               {
                  GXUtil.WriteLog("atributos:[seudo value changed for attri]"+"Atributos_PK");
                  GXUtil.WriteLogRaw("Old: ",Z400Atributos_PK);
                  GXUtil.WriteLogRaw("Current: ",T00132_A400Atributos_PK[0]);
               }
               if ( Z401Atributos_FK != T00132_A401Atributos_FK[0] )
               {
                  GXUtil.WriteLog("atributos:[seudo value changed for attri]"+"Atributos_FK");
                  GXUtil.WriteLogRaw("Old: ",Z401Atributos_FK);
                  GXUtil.WriteLogRaw("Current: ",T00132_A401Atributos_FK[0]);
               }
               if ( Z180Atributos_Ativo != T00132_A180Atributos_Ativo[0] )
               {
                  GXUtil.WriteLog("atributos:[seudo value changed for attri]"+"Atributos_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z180Atributos_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00132_A180Atributos_Ativo[0]);
               }
               if ( Z356Atributos_TabelaCod != T00132_A356Atributos_TabelaCod[0] )
               {
                  GXUtil.WriteLog("atributos:[seudo value changed for attri]"+"Atributos_TabelaCod");
                  GXUtil.WriteLogRaw("Old: ",Z356Atributos_TabelaCod);
                  GXUtil.WriteLogRaw("Current: ",T00132_A356Atributos_TabelaCod[0]);
               }
               if ( Z747Atributos_MelhoraCod != T00132_A747Atributos_MelhoraCod[0] )
               {
                  GXUtil.WriteLog("atributos:[seudo value changed for attri]"+"Atributos_MelhoraCod");
                  GXUtil.WriteLogRaw("Old: ",Z747Atributos_MelhoraCod);
                  GXUtil.WriteLogRaw("Current: ",T00132_A747Atributos_MelhoraCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Atributos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1340( )
      {
         BeforeValidate1340( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1340( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1340( 0) ;
            CheckOptimisticConcurrency1340( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1340( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1340( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001312 */
                     pr_default.execute(10, new Object[] {A177Atributos_Nome, n390Atributos_Detalhes, A390Atributos_Detalhes, n179Atributos_Descricao, A179Atributos_Descricao, n178Atributos_TipoDados, A178Atributos_TipoDados, n400Atributos_PK, A400Atributos_PK, n401Atributos_FK, A401Atributos_FK, A180Atributos_Ativo, A356Atributos_TabelaCod, n747Atributos_MelhoraCod, A747Atributos_MelhoraCod});
                     A176Atributos_Codigo = T001312_A176Atributos_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption130( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1340( ) ;
            }
            EndLevel1340( ) ;
         }
         CloseExtendedTableCursors1340( ) ;
      }

      protected void Update1340( )
      {
         BeforeValidate1340( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1340( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1340( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1340( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1340( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001313 */
                     pr_default.execute(11, new Object[] {A177Atributos_Nome, n390Atributos_Detalhes, A390Atributos_Detalhes, n179Atributos_Descricao, A179Atributos_Descricao, n178Atributos_TipoDados, A178Atributos_TipoDados, n400Atributos_PK, A400Atributos_PK, n401Atributos_FK, A401Atributos_FK, A180Atributos_Ativo, A356Atributos_TabelaCod, n747Atributos_MelhoraCod, A747Atributos_MelhoraCod, A176Atributos_Codigo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Atributos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1340( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1340( ) ;
         }
         CloseExtendedTableCursors1340( ) ;
      }

      protected void DeferredUpdate1340( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1340( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1340( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1340( ) ;
            AfterConfirm1340( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1340( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001314 */
                  pr_default.execute(12, new Object[] {A176Atributos_Codigo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("Atributos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode40 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1340( ) ;
         Gx_mode = sMode40;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1340( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001315 */
            pr_default.execute(13, new Object[] {A356Atributos_TabelaCod});
            A357Atributos_TabelaNom = T001315_A357Atributos_TabelaNom[0];
            n357Atributos_TabelaNom = T001315_n357Atributos_TabelaNom[0];
            A190Tabela_SistemaCod = T001315_A190Tabela_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
            n190Tabela_SistemaCod = T001315_n190Tabela_SistemaCod[0];
            pr_default.close(13);
            Dvpanel_tableattributes_Title = "Atributo da tabela: "+A357Atributos_TabelaNom;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
            AV10WebSession.Set("Tabela_Codigo", StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0));
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T001316 */
            pr_default.execute(14, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Atributos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor T001317 */
            pr_default.execute(15, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item Atributos FSoftware"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
            /* Using cursor T001318 */
            pr_default.execute(16, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(16) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Atributos das Fun��es de APF"+" ("+"Funcao APFAtributos_Funcao APFAtributosMelhora"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(16);
            /* Using cursor T001319 */
            pr_default.execute(17, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(17) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Atributos das Fun��es de APF"+" ("+"Funcao APFAtributos_Atributos"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(17);
            /* Using cursor T001320 */
            pr_default.execute(18, new Object[] {A176Atributos_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Item Atributos FMetrica"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
         }
      }

      protected void EndLevel1340( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1340( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            context.CommitDataStores( "Atributos");
            if ( AnyError == 0 )
            {
               ConfirmValues130( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            context.RollbackDataStores( "Atributos");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1340( )
      {
         /* Scan By routine */
         /* Using cursor T001321 */
         pr_default.execute(19);
         RcdFound40 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound40 = 1;
            A176Atributos_Codigo = T001321_A176Atributos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1340( )
      {
         /* Scan next routine */
         pr_default.readNext(19);
         RcdFound40 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound40 = 1;
            A176Atributos_Codigo = T001321_A176Atributos_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1340( )
      {
         pr_default.close(19);
      }

      protected void AfterConfirm1340( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1340( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1340( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1340( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1340( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1340( )
      {
         /* Before Validate Rules */
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ) && ( StringUtil.StrCmp(A177Atributos_Nome, O177Atributos_Nome) != 0 ) && new prc_existenomecadastrado(context).executeUdp(  "Atr",  A177Atributos_Nome) )
         {
            GX_msglist.addItem("Nome de atributo j� cadastrado!", 1, "ATRIBUTOS_NOME");
            AnyError = 1;
            GX_FocusControl = edtAtributos_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void DisableAttributes1340( )
      {
         edtAtributos_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAtributos_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAtributos_Nome_Enabled), 5, 0)));
         cmbAtributos_TipoDados.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAtributos_TipoDados_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbAtributos_TipoDados.Enabled), 5, 0)));
         edtAtributos_Detalhes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAtributos_Detalhes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAtributos_Detalhes_Enabled), 5, 0)));
         chkAtributos_PK.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_PK_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkAtributos_PK.Enabled), 5, 0)));
         chkAtributos_FK.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_FK_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkAtributos_FK.Enabled), 5, 0)));
         edtAtributos_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAtributos_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAtributos_Descricao_Enabled), 5, 0)));
         dynTabela_SistemaCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTabela_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTabela_SistemaCod.Enabled), 5, 0)));
         chkAtributos_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAtributos_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkAtributos_Ativo.Enabled), 5, 0)));
         edtAtributos_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAtributos_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAtributos_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues130( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117184824");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Atributos_Codigo) + "," + UrlEncode("" +AV14Sistema_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z176Atributos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z176Atributos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z177Atributos_Nome", StringUtil.RTrim( Z177Atributos_Nome));
         GxWebStd.gx_hidden_field( context, "Z390Atributos_Detalhes", StringUtil.RTrim( Z390Atributos_Detalhes));
         GxWebStd.gx_hidden_field( context, "Z178Atributos_TipoDados", StringUtil.RTrim( Z178Atributos_TipoDados));
         GxWebStd.gx_boolean_hidden_field( context, "Z400Atributos_PK", Z400Atributos_PK);
         GxWebStd.gx_boolean_hidden_field( context, "Z401Atributos_FK", Z401Atributos_FK);
         GxWebStd.gx_boolean_hidden_field( context, "Z180Atributos_Ativo", Z180Atributos_Ativo);
         GxWebStd.gx_hidden_field( context, "Z356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z356Atributos_TabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z747Atributos_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z747Atributos_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O177Atributos_Nome", StringUtil.RTrim( O177Atributos_Nome));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N747Atributos_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A747Atributos_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14Sistema_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vATRIBUTOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Atributos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_ATRIBUTOS_TABELACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_Atributos_TabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_TABELACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_ATRIBUTOS_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Insert_Atributos_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A747Atributos_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ATRIBUTOS_TABELANOM", StringUtil.RTrim( A357Atributos_TabelaNom));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV23Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vATRIBUTOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Atributos_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Atributos";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A747Atributos_MelhoraCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("atributos:[SendSecurityCheck value for]"+"Atributos_Codigo:"+context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("atributos:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("atributos:[SendSecurityCheck value for]"+"Atributos_TabelaCod:"+context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9"));
         GXUtil.WriteLog("atributos:[SendSecurityCheck value for]"+"Atributos_MelhoraCod:"+context.localUtil.Format( (decimal)(A747Atributos_MelhoraCod), "ZZZZZ9"));
         forbiddenHiddens2 = "";
         if ( ! ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            forbiddenHiddens2 = forbiddenHiddens2 + context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9");
         }
         GxWebStd.gx_hidden_field( context, "hsh2", GXUtil.GetEncryptedHash( forbiddenHiddens2, GXKey));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Atributos_Codigo) + "," + UrlEncode("" +AV14Sistema_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Atributos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Atributos" ;
      }

      protected void InitializeNonKey1340( )
      {
         A356Atributos_TabelaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
         A747Atributos_MelhoraCod = 0;
         n747Atributos_MelhoraCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A747Atributos_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A747Atributos_MelhoraCod), 6, 0)));
         A177Atributos_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A177Atributos_Nome", A177Atributos_Nome);
         A390Atributos_Detalhes = "";
         n390Atributos_Detalhes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A390Atributos_Detalhes", A390Atributos_Detalhes);
         n390Atributos_Detalhes = (String.IsNullOrEmpty(StringUtil.RTrim( A390Atributos_Detalhes)) ? true : false);
         A179Atributos_Descricao = "";
         n179Atributos_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A179Atributos_Descricao", A179Atributos_Descricao);
         n179Atributos_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A179Atributos_Descricao)) ? true : false);
         A178Atributos_TipoDados = "";
         n178Atributos_TipoDados = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
         n178Atributos_TipoDados = (String.IsNullOrEmpty(StringUtil.RTrim( A178Atributos_TipoDados)) ? true : false);
         A400Atributos_PK = false;
         n400Atributos_PK = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A400Atributos_PK", A400Atributos_PK);
         n400Atributos_PK = ((false==A400Atributos_PK) ? true : false);
         A401Atributos_FK = false;
         n401Atributos_FK = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A401Atributos_FK", A401Atributos_FK);
         n401Atributos_FK = ((false==A401Atributos_FK) ? true : false);
         A357Atributos_TabelaNom = "";
         n357Atributos_TabelaNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A357Atributos_TabelaNom", A357Atributos_TabelaNom);
         A180Atributos_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A180Atributos_Ativo", A180Atributos_Ativo);
         O177Atributos_Nome = A177Atributos_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A177Atributos_Nome", A177Atributos_Nome);
         Z177Atributos_Nome = "";
         Z390Atributos_Detalhes = "";
         Z178Atributos_TipoDados = "";
         Z400Atributos_PK = false;
         Z401Atributos_FK = false;
         Z180Atributos_Ativo = false;
         Z356Atributos_TabelaCod = 0;
         Z747Atributos_MelhoraCod = 0;
      }

      protected void InitAll1340( )
      {
         A176Atributos_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
         InitializeNonKey1340( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A180Atributos_Ativo = i180Atributos_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A180Atributos_Ativo", A180Atributos_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117184859");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("atributos.js", "?20203117184860");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockatributos_nome_Internalname = "TEXTBLOCKATRIBUTOS_NOME";
         edtAtributos_Nome_Internalname = "ATRIBUTOS_NOME";
         lblTextblockatributos_tipodados_Internalname = "TEXTBLOCKATRIBUTOS_TIPODADOS";
         cmbAtributos_TipoDados_Internalname = "ATRIBUTOS_TIPODADOS";
         lblTextblockatributos_detalhes_Internalname = "TEXTBLOCKATRIBUTOS_DETALHES";
         edtAtributos_Detalhes_Internalname = "ATRIBUTOS_DETALHES";
         lblTextblockatributos_pk_Internalname = "TEXTBLOCKATRIBUTOS_PK";
         chkAtributos_PK_Internalname = "ATRIBUTOS_PK";
         lblTextblockatributos_fk_Internalname = "TEXTBLOCKATRIBUTOS_FK";
         chkAtributos_FK_Internalname = "ATRIBUTOS_FK";
         tblTablemergedatributos_tipodados_Internalname = "TABLEMERGEDATRIBUTOS_TIPODADOS";
         lblTextblockatributos_descricao_Internalname = "TEXTBLOCKATRIBUTOS_DESCRICAO";
         edtAtributos_Descricao_Internalname = "ATRIBUTOS_DESCRICAO";
         lblTextblocktabela_sistemacod_Internalname = "TEXTBLOCKTABELA_SISTEMACOD";
         dynTabela_SistemaCod_Internalname = "TABELA_SISTEMACOD";
         lblTextblockatributos_ativo_Internalname = "TEXTBLOCKATRIBUTOS_ATIVO";
         chkAtributos_Ativo_Internalname = "ATRIBUTOS_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtneliminar_Internalname = "BTNELIMINAR";
         bttBtnfechar_Internalname = "BTNFECHAR";
         bttBtncopiarcolaratributos_Internalname = "BTNCOPIARCOLARATRIBUTOS";
         bttBtnimportarexcel_Internalname = "BTNIMPORTAREXCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         edtAtributos_Codigo_Internalname = "ATRIBUTOS_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Atributos";
         Dvpanel_tableattributes_Title = "Atributos";
         chkAtributos_FK.Enabled = 1;
         chkAtributos_PK.Enabled = 1;
         edtAtributos_Detalhes_Jsonclick = "";
         edtAtributos_Detalhes_Enabled = 1;
         cmbAtributos_TipoDados_Jsonclick = "";
         cmbAtributos_TipoDados.Enabled = 1;
         chkAtributos_Ativo.Enabled = 1;
         dynTabela_SistemaCod_Jsonclick = "";
         dynTabela_SistemaCod.Enabled = 0;
         edtAtributos_Descricao_Enabled = 1;
         edtAtributos_Nome_Jsonclick = "";
         edtAtributos_Nome_Enabled = 1;
         bttBtnimportarexcel_Visible = 1;
         bttBtncopiarcolaratributos_Visible = 1;
         bttBtnfechar_Visible = 1;
         bttBtneliminar_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         lblTbjava_Visible = 1;
         edtAtributos_Codigo_Jsonclick = "";
         edtAtributos_Codigo_Enabled = 0;
         edtAtributos_Codigo_Visible = 1;
         chkAtributos_Ativo.Caption = "";
         chkAtributos_FK.Caption = "";
         chkAtributos_PK.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLATABELA_SISTEMACOD1340( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATABELA_SISTEMACOD_data1340( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATABELA_SISTEMACOD_html1340( )
      {
         int gxdynajaxvalue ;
         GXDLATABELA_SISTEMACOD_data1340( ) ;
         gxdynajaxindex = 1;
         dynTabela_SistemaCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTabela_SistemaCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATABELA_SISTEMACOD_data1340( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor T001322 */
         pr_default.execute(20);
         while ( (pr_default.getStatus(20) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001322_A190Tabela_SistemaCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(T001322_A191Tabela_SistemaDes[0]);
            pr_default.readNext(20);
         }
         pr_default.close(20);
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV14Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12132',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV14Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'AV14Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOELIMINAR'","{handler:'E13132',iparms:[{av:'AV7Atributos_Codigo',fld:'vATRIBUTOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E14132',iparms:[{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOCOPIARCOLARATRIBUTOS'","{handler:'E15132',iparms:[{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOIMPORTAREXCEL'","{handler:'E16132',iparms:[{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV14Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z177Atributos_Nome = "";
         Z390Atributos_Detalhes = "";
         Z178Atributos_TipoDados = "";
         O177Atributos_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A178Atributos_TipoDados = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbjava_Jsonclick = "";
         TempTags = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtneliminar_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         bttBtncopiarcolaratributos_Jsonclick = "";
         bttBtnimportarexcel_Jsonclick = "";
         lblTextblockatributos_nome_Jsonclick = "";
         A177Atributos_Nome = "";
         lblTextblockatributos_tipodados_Jsonclick = "";
         lblTextblockatributos_descricao_Jsonclick = "";
         A179Atributos_Descricao = "";
         lblTextblocktabela_sistemacod_Jsonclick = "";
         lblTextblockatributos_ativo_Jsonclick = "";
         lblTextblockatributos_detalhes_Jsonclick = "";
         A390Atributos_Detalhes = "";
         lblTextblockatributos_pk_Jsonclick = "";
         lblTextblockatributos_fk_Jsonclick = "";
         A357Atributos_TabelaNom = "";
         AV23Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         forbiddenHiddens2 = "";
         hsh2 = "";
         sMode40 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z179Atributos_Descricao = "";
         Z357Atributos_TabelaNom = "";
         T00134_A357Atributos_TabelaNom = new String[] {""} ;
         T00134_n357Atributos_TabelaNom = new bool[] {false} ;
         T00134_A190Tabela_SistemaCod = new int[1] ;
         T00134_n190Tabela_SistemaCod = new bool[] {false} ;
         T00136_A176Atributos_Codigo = new int[1] ;
         T00136_A177Atributos_Nome = new String[] {""} ;
         T00136_A390Atributos_Detalhes = new String[] {""} ;
         T00136_n390Atributos_Detalhes = new bool[] {false} ;
         T00136_A179Atributos_Descricao = new String[] {""} ;
         T00136_n179Atributos_Descricao = new bool[] {false} ;
         T00136_A178Atributos_TipoDados = new String[] {""} ;
         T00136_n178Atributos_TipoDados = new bool[] {false} ;
         T00136_A400Atributos_PK = new bool[] {false} ;
         T00136_n400Atributos_PK = new bool[] {false} ;
         T00136_A401Atributos_FK = new bool[] {false} ;
         T00136_n401Atributos_FK = new bool[] {false} ;
         T00136_A357Atributos_TabelaNom = new String[] {""} ;
         T00136_n357Atributos_TabelaNom = new bool[] {false} ;
         T00136_A180Atributos_Ativo = new bool[] {false} ;
         T00136_A356Atributos_TabelaCod = new int[1] ;
         T00136_A747Atributos_MelhoraCod = new int[1] ;
         T00136_n747Atributos_MelhoraCod = new bool[] {false} ;
         T00136_A190Tabela_SistemaCod = new int[1] ;
         T00136_n190Tabela_SistemaCod = new bool[] {false} ;
         T00135_A747Atributos_MelhoraCod = new int[1] ;
         T00135_n747Atributos_MelhoraCod = new bool[] {false} ;
         T00137_A357Atributos_TabelaNom = new String[] {""} ;
         T00137_n357Atributos_TabelaNom = new bool[] {false} ;
         T00137_A190Tabela_SistemaCod = new int[1] ;
         T00137_n190Tabela_SistemaCod = new bool[] {false} ;
         T00138_A747Atributos_MelhoraCod = new int[1] ;
         T00138_n747Atributos_MelhoraCod = new bool[] {false} ;
         T00139_A176Atributos_Codigo = new int[1] ;
         T00133_A176Atributos_Codigo = new int[1] ;
         T00133_A177Atributos_Nome = new String[] {""} ;
         T00133_A390Atributos_Detalhes = new String[] {""} ;
         T00133_n390Atributos_Detalhes = new bool[] {false} ;
         T00133_A179Atributos_Descricao = new String[] {""} ;
         T00133_n179Atributos_Descricao = new bool[] {false} ;
         T00133_A178Atributos_TipoDados = new String[] {""} ;
         T00133_n178Atributos_TipoDados = new bool[] {false} ;
         T00133_A400Atributos_PK = new bool[] {false} ;
         T00133_n400Atributos_PK = new bool[] {false} ;
         T00133_A401Atributos_FK = new bool[] {false} ;
         T00133_n401Atributos_FK = new bool[] {false} ;
         T00133_A180Atributos_Ativo = new bool[] {false} ;
         T00133_A356Atributos_TabelaCod = new int[1] ;
         T00133_A747Atributos_MelhoraCod = new int[1] ;
         T00133_n747Atributos_MelhoraCod = new bool[] {false} ;
         T001310_A176Atributos_Codigo = new int[1] ;
         T001311_A176Atributos_Codigo = new int[1] ;
         T00132_A176Atributos_Codigo = new int[1] ;
         T00132_A177Atributos_Nome = new String[] {""} ;
         T00132_A390Atributos_Detalhes = new String[] {""} ;
         T00132_n390Atributos_Detalhes = new bool[] {false} ;
         T00132_A179Atributos_Descricao = new String[] {""} ;
         T00132_n179Atributos_Descricao = new bool[] {false} ;
         T00132_A178Atributos_TipoDados = new String[] {""} ;
         T00132_n178Atributos_TipoDados = new bool[] {false} ;
         T00132_A400Atributos_PK = new bool[] {false} ;
         T00132_n400Atributos_PK = new bool[] {false} ;
         T00132_A401Atributos_FK = new bool[] {false} ;
         T00132_n401Atributos_FK = new bool[] {false} ;
         T00132_A180Atributos_Ativo = new bool[] {false} ;
         T00132_A356Atributos_TabelaCod = new int[1] ;
         T00132_A747Atributos_MelhoraCod = new int[1] ;
         T00132_n747Atributos_MelhoraCod = new bool[] {false} ;
         T001312_A176Atributos_Codigo = new int[1] ;
         T001315_A357Atributos_TabelaNom = new String[] {""} ;
         T001315_n357Atributos_TabelaNom = new bool[] {false} ;
         T001315_A190Tabela_SistemaCod = new int[1] ;
         T001315_n190Tabela_SistemaCod = new bool[] {false} ;
         T001316_A747Atributos_MelhoraCod = new int[1] ;
         T001316_n747Atributos_MelhoraCod = new bool[] {false} ;
         T001317_A224ContagemItem_Lancamento = new int[1] ;
         T001317_A379ContagemItem_AtributosCod = new int[1] ;
         T001318_A165FuncaoAPF_Codigo = new int[1] ;
         T001318_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         T001319_A165FuncaoAPF_Codigo = new int[1] ;
         T001319_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         T001320_A261ContagemItemAtributosFMetrica_ItemContagemLan = new int[1] ;
         T001320_A249ContagemItem_FMetricasAtributosCod = new int[1] ;
         T001321_A176Atributos_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T001322_A172Tabela_Codigo = new int[1] ;
         T001322_A190Tabela_SistemaCod = new int[1] ;
         T001322_n190Tabela_SistemaCod = new bool[] {false} ;
         T001322_A191Tabela_SistemaDes = new String[] {""} ;
         T001322_n191Tabela_SistemaDes = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.atributos__default(),
            new Object[][] {
                new Object[] {
               T00132_A176Atributos_Codigo, T00132_A177Atributos_Nome, T00132_A390Atributos_Detalhes, T00132_n390Atributos_Detalhes, T00132_A179Atributos_Descricao, T00132_n179Atributos_Descricao, T00132_A178Atributos_TipoDados, T00132_n178Atributos_TipoDados, T00132_A400Atributos_PK, T00132_n400Atributos_PK,
               T00132_A401Atributos_FK, T00132_n401Atributos_FK, T00132_A180Atributos_Ativo, T00132_A356Atributos_TabelaCod, T00132_A747Atributos_MelhoraCod, T00132_n747Atributos_MelhoraCod
               }
               , new Object[] {
               T00133_A176Atributos_Codigo, T00133_A177Atributos_Nome, T00133_A390Atributos_Detalhes, T00133_n390Atributos_Detalhes, T00133_A179Atributos_Descricao, T00133_n179Atributos_Descricao, T00133_A178Atributos_TipoDados, T00133_n178Atributos_TipoDados, T00133_A400Atributos_PK, T00133_n400Atributos_PK,
               T00133_A401Atributos_FK, T00133_n401Atributos_FK, T00133_A180Atributos_Ativo, T00133_A356Atributos_TabelaCod, T00133_A747Atributos_MelhoraCod, T00133_n747Atributos_MelhoraCod
               }
               , new Object[] {
               T00134_A357Atributos_TabelaNom, T00134_n357Atributos_TabelaNom, T00134_A190Tabela_SistemaCod, T00134_n190Tabela_SistemaCod
               }
               , new Object[] {
               T00135_A747Atributos_MelhoraCod
               }
               , new Object[] {
               T00136_A176Atributos_Codigo, T00136_A177Atributos_Nome, T00136_A390Atributos_Detalhes, T00136_n390Atributos_Detalhes, T00136_A179Atributos_Descricao, T00136_n179Atributos_Descricao, T00136_A178Atributos_TipoDados, T00136_n178Atributos_TipoDados, T00136_A400Atributos_PK, T00136_n400Atributos_PK,
               T00136_A401Atributos_FK, T00136_n401Atributos_FK, T00136_A357Atributos_TabelaNom, T00136_n357Atributos_TabelaNom, T00136_A180Atributos_Ativo, T00136_A356Atributos_TabelaCod, T00136_A747Atributos_MelhoraCod, T00136_n747Atributos_MelhoraCod, T00136_A190Tabela_SistemaCod, T00136_n190Tabela_SistemaCod
               }
               , new Object[] {
               T00137_A357Atributos_TabelaNom, T00137_n357Atributos_TabelaNom, T00137_A190Tabela_SistemaCod, T00137_n190Tabela_SistemaCod
               }
               , new Object[] {
               T00138_A747Atributos_MelhoraCod
               }
               , new Object[] {
               T00139_A176Atributos_Codigo
               }
               , new Object[] {
               T001310_A176Atributos_Codigo
               }
               , new Object[] {
               T001311_A176Atributos_Codigo
               }
               , new Object[] {
               T001312_A176Atributos_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001315_A357Atributos_TabelaNom, T001315_n357Atributos_TabelaNom, T001315_A190Tabela_SistemaCod, T001315_n190Tabela_SistemaCod
               }
               , new Object[] {
               T001316_A747Atributos_MelhoraCod
               }
               , new Object[] {
               T001317_A224ContagemItem_Lancamento, T001317_A379ContagemItem_AtributosCod
               }
               , new Object[] {
               T001318_A165FuncaoAPF_Codigo, T001318_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               T001319_A165FuncaoAPF_Codigo, T001319_A364FuncaoAPFAtributos_AtributosCod
               }
               , new Object[] {
               T001320_A261ContagemItemAtributosFMetrica_ItemContagemLan, T001320_A249ContagemItem_FMetricasAtributosCod
               }
               , new Object[] {
               T001321_A176Atributos_Codigo
               }
               , new Object[] {
               T001322_A172Tabela_Codigo, T001322_A190Tabela_SistemaCod, T001322_A191Tabela_SistemaDes, T001322_n191Tabela_SistemaDes
               }
            }
         );
         Z180Atributos_Ativo = true;
         A180Atributos_Ativo = true;
         i180Atributos_Ativo = true;
         AV23Pgmname = "Atributos";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound40 ;
      private short AV15Linhas ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7Atributos_Codigo ;
      private int wcpOAV14Sistema_Codigo ;
      private int Z176Atributos_Codigo ;
      private int Z356Atributos_TabelaCod ;
      private int Z747Atributos_MelhoraCod ;
      private int N356Atributos_TabelaCod ;
      private int N747Atributos_MelhoraCod ;
      private int A356Atributos_TabelaCod ;
      private int A747Atributos_MelhoraCod ;
      private int AV7Atributos_Codigo ;
      private int AV14Sistema_Codigo ;
      private int trnEnded ;
      private int A190Tabela_SistemaCod ;
      private int A176Atributos_Codigo ;
      private int edtAtributos_Codigo_Enabled ;
      private int edtAtributos_Codigo_Visible ;
      private int lblTbjava_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtneliminar_Visible ;
      private int bttBtnfechar_Visible ;
      private int bttBtncopiarcolaratributos_Visible ;
      private int bttBtnimportarexcel_Visible ;
      private int edtAtributos_Nome_Enabled ;
      private int edtAtributos_Descricao_Enabled ;
      private int edtAtributos_Detalhes_Enabled ;
      private int AV13Insert_Atributos_TabelaCod ;
      private int AV22Insert_Atributos_MelhoraCod ;
      private int AV24GXV1 ;
      private int Z190Tabela_SistemaCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z177Atributos_Nome ;
      private String Z390Atributos_Detalhes ;
      private String Z178Atributos_TipoDados ;
      private String O177Atributos_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String A178Atributos_TipoDados ;
      private String chkAtributos_PK_Internalname ;
      private String chkAtributos_FK_Internalname ;
      private String chkAtributos_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtAtributos_Nome_Internalname ;
      private String edtAtributos_Codigo_Internalname ;
      private String edtAtributos_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtneliminar_Internalname ;
      private String bttBtneliminar_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String bttBtncopiarcolaratributos_Internalname ;
      private String bttBtncopiarcolaratributos_Jsonclick ;
      private String bttBtnimportarexcel_Internalname ;
      private String bttBtnimportarexcel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockatributos_nome_Internalname ;
      private String lblTextblockatributos_nome_Jsonclick ;
      private String A177Atributos_Nome ;
      private String edtAtributos_Nome_Jsonclick ;
      private String lblTextblockatributos_tipodados_Internalname ;
      private String lblTextblockatributos_tipodados_Jsonclick ;
      private String lblTextblockatributos_descricao_Internalname ;
      private String lblTextblockatributos_descricao_Jsonclick ;
      private String edtAtributos_Descricao_Internalname ;
      private String lblTextblocktabela_sistemacod_Internalname ;
      private String lblTextblocktabela_sistemacod_Jsonclick ;
      private String dynTabela_SistemaCod_Internalname ;
      private String dynTabela_SistemaCod_Jsonclick ;
      private String lblTextblockatributos_ativo_Internalname ;
      private String lblTextblockatributos_ativo_Jsonclick ;
      private String tblTablemergedatributos_tipodados_Internalname ;
      private String cmbAtributos_TipoDados_Internalname ;
      private String cmbAtributos_TipoDados_Jsonclick ;
      private String lblTextblockatributos_detalhes_Internalname ;
      private String lblTextblockatributos_detalhes_Jsonclick ;
      private String edtAtributos_Detalhes_Internalname ;
      private String A390Atributos_Detalhes ;
      private String edtAtributos_Detalhes_Jsonclick ;
      private String lblTextblockatributos_pk_Internalname ;
      private String lblTextblockatributos_pk_Jsonclick ;
      private String lblTextblockatributos_fk_Internalname ;
      private String lblTextblockatributos_fk_Jsonclick ;
      private String A357Atributos_TabelaNom ;
      private String AV23Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String forbiddenHiddens2 ;
      private String hsh2 ;
      private String sMode40 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z357Atributos_TabelaNom ;
      private String Dvpanel_tableattributes_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool Z400Atributos_PK ;
      private bool Z401Atributos_FK ;
      private bool Z180Atributos_Ativo ;
      private bool entryPointCalled ;
      private bool n747Atributos_MelhoraCod ;
      private bool toggleJsOutput ;
      private bool n178Atributos_TipoDados ;
      private bool wbErr ;
      private bool n190Tabela_SistemaCod ;
      private bool A180Atributos_Ativo ;
      private bool A400Atributos_PK ;
      private bool A401Atributos_FK ;
      private bool n390Atributos_Detalhes ;
      private bool n400Atributos_PK ;
      private bool n401Atributos_FK ;
      private bool n179Atributos_Descricao ;
      private bool n357Atributos_TabelaNom ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool GXt_boolean1 ;
      private bool Gx_longc ;
      private bool i180Atributos_Ativo ;
      private String A179Atributos_Descricao ;
      private String Z179Atributos_Descricao ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Sistema_Codigo ;
      private GXCombobox cmbAtributos_TipoDados ;
      private GXCheckbox chkAtributos_PK ;
      private GXCheckbox chkAtributos_FK ;
      private GXCombobox dynTabela_SistemaCod ;
      private GXCheckbox chkAtributos_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T00134_A357Atributos_TabelaNom ;
      private bool[] T00134_n357Atributos_TabelaNom ;
      private int[] T00134_A190Tabela_SistemaCod ;
      private bool[] T00134_n190Tabela_SistemaCod ;
      private int[] T00136_A176Atributos_Codigo ;
      private String[] T00136_A177Atributos_Nome ;
      private String[] T00136_A390Atributos_Detalhes ;
      private bool[] T00136_n390Atributos_Detalhes ;
      private String[] T00136_A179Atributos_Descricao ;
      private bool[] T00136_n179Atributos_Descricao ;
      private String[] T00136_A178Atributos_TipoDados ;
      private bool[] T00136_n178Atributos_TipoDados ;
      private bool[] T00136_A400Atributos_PK ;
      private bool[] T00136_n400Atributos_PK ;
      private bool[] T00136_A401Atributos_FK ;
      private bool[] T00136_n401Atributos_FK ;
      private String[] T00136_A357Atributos_TabelaNom ;
      private bool[] T00136_n357Atributos_TabelaNom ;
      private bool[] T00136_A180Atributos_Ativo ;
      private int[] T00136_A356Atributos_TabelaCod ;
      private int[] T00136_A747Atributos_MelhoraCod ;
      private bool[] T00136_n747Atributos_MelhoraCod ;
      private int[] T00136_A190Tabela_SistemaCod ;
      private bool[] T00136_n190Tabela_SistemaCod ;
      private int[] T00135_A747Atributos_MelhoraCod ;
      private bool[] T00135_n747Atributos_MelhoraCod ;
      private String[] T00137_A357Atributos_TabelaNom ;
      private bool[] T00137_n357Atributos_TabelaNom ;
      private int[] T00137_A190Tabela_SistemaCod ;
      private bool[] T00137_n190Tabela_SistemaCod ;
      private int[] T00138_A747Atributos_MelhoraCod ;
      private bool[] T00138_n747Atributos_MelhoraCod ;
      private int[] T00139_A176Atributos_Codigo ;
      private int[] T00133_A176Atributos_Codigo ;
      private String[] T00133_A177Atributos_Nome ;
      private String[] T00133_A390Atributos_Detalhes ;
      private bool[] T00133_n390Atributos_Detalhes ;
      private String[] T00133_A179Atributos_Descricao ;
      private bool[] T00133_n179Atributos_Descricao ;
      private String[] T00133_A178Atributos_TipoDados ;
      private bool[] T00133_n178Atributos_TipoDados ;
      private bool[] T00133_A400Atributos_PK ;
      private bool[] T00133_n400Atributos_PK ;
      private bool[] T00133_A401Atributos_FK ;
      private bool[] T00133_n401Atributos_FK ;
      private bool[] T00133_A180Atributos_Ativo ;
      private int[] T00133_A356Atributos_TabelaCod ;
      private int[] T00133_A747Atributos_MelhoraCod ;
      private bool[] T00133_n747Atributos_MelhoraCod ;
      private int[] T001310_A176Atributos_Codigo ;
      private int[] T001311_A176Atributos_Codigo ;
      private int[] T00132_A176Atributos_Codigo ;
      private String[] T00132_A177Atributos_Nome ;
      private String[] T00132_A390Atributos_Detalhes ;
      private bool[] T00132_n390Atributos_Detalhes ;
      private String[] T00132_A179Atributos_Descricao ;
      private bool[] T00132_n179Atributos_Descricao ;
      private String[] T00132_A178Atributos_TipoDados ;
      private bool[] T00132_n178Atributos_TipoDados ;
      private bool[] T00132_A400Atributos_PK ;
      private bool[] T00132_n400Atributos_PK ;
      private bool[] T00132_A401Atributos_FK ;
      private bool[] T00132_n401Atributos_FK ;
      private bool[] T00132_A180Atributos_Ativo ;
      private int[] T00132_A356Atributos_TabelaCod ;
      private int[] T00132_A747Atributos_MelhoraCod ;
      private bool[] T00132_n747Atributos_MelhoraCod ;
      private int[] T001312_A176Atributos_Codigo ;
      private String[] T001315_A357Atributos_TabelaNom ;
      private bool[] T001315_n357Atributos_TabelaNom ;
      private int[] T001315_A190Tabela_SistemaCod ;
      private bool[] T001315_n190Tabela_SistemaCod ;
      private int[] T001316_A747Atributos_MelhoraCod ;
      private bool[] T001316_n747Atributos_MelhoraCod ;
      private int[] T001317_A224ContagemItem_Lancamento ;
      private int[] T001317_A379ContagemItem_AtributosCod ;
      private int[] T001318_A165FuncaoAPF_Codigo ;
      private int[] T001318_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] T001319_A165FuncaoAPF_Codigo ;
      private int[] T001319_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] T001320_A261ContagemItemAtributosFMetrica_ItemContagemLan ;
      private int[] T001320_A249ContagemItem_FMetricasAtributosCod ;
      private int[] T001321_A176Atributos_Codigo ;
      private int[] T001322_A172Tabela_Codigo ;
      private int[] T001322_A190Tabela_SistemaCod ;
      private bool[] T001322_n190Tabela_SistemaCod ;
      private String[] T001322_A191Tabela_SistemaDes ;
      private bool[] T001322_n191Tabela_SistemaDes ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class atributos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00136 ;
          prmT00136 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00134 ;
          prmT00134 = new Object[] {
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00135 ;
          prmT00135 = new Object[] {
          new Object[] {"@Atributos_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00137 ;
          prmT00137 = new Object[] {
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00138 ;
          prmT00138 = new Object[] {
          new Object[] {"@Atributos_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00139 ;
          prmT00139 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00133 ;
          prmT00133 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001310 ;
          prmT001310 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001311 ;
          prmT001311 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00132 ;
          prmT00132 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001312 ;
          prmT001312 = new Object[] {
          new Object[] {"@Atributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Atributos_Detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@Atributos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Atributos_TipoDados",SqlDbType.Char,4,0} ,
          new Object[] {"@Atributos_PK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_FK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Atributos_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001313 ;
          prmT001313 = new Object[] {
          new Object[] {"@Atributos_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Atributos_Detalhes",SqlDbType.Char,10,0} ,
          new Object[] {"@Atributos_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Atributos_TipoDados",SqlDbType.Char,4,0} ,
          new Object[] {"@Atributos_PK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_FK",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Atributos_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001314 ;
          prmT001314 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001315 ;
          prmT001315 = new Object[] {
          new Object[] {"@Atributos_TabelaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001316 ;
          prmT001316 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001317 ;
          prmT001317 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001318 ;
          prmT001318 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001319 ;
          prmT001319 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001320 ;
          prmT001320 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001321 ;
          prmT001321 = new Object[] {
          } ;
          Object[] prmT001322 ;
          prmT001322 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T00132", "SELECT [Atributos_Codigo], [Atributos_Nome], [Atributos_Detalhes], [Atributos_Descricao], [Atributos_TipoDados], [Atributos_PK], [Atributos_FK], [Atributos_Ativo], [Atributos_TabelaCod] AS Atributos_TabelaCod, [Atributos_MelhoraCod] AS Atributos_MelhoraCod FROM [Atributos] WITH (UPDLOCK) WHERE [Atributos_Codigo] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00132,1,0,true,false )
             ,new CursorDef("T00133", "SELECT [Atributos_Codigo], [Atributos_Nome], [Atributos_Detalhes], [Atributos_Descricao], [Atributos_TipoDados], [Atributos_PK], [Atributos_FK], [Atributos_Ativo], [Atributos_TabelaCod] AS Atributos_TabelaCod, [Atributos_MelhoraCod] AS Atributos_MelhoraCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00133,1,0,true,false )
             ,new CursorDef("T00134", "SELECT [Tabela_Nome] AS Atributos_TabelaNom, [Tabela_SistemaCod] AS Tabela_SistemaCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Atributos_TabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00134,1,0,true,false )
             ,new CursorDef("T00135", "SELECT [Atributos_Codigo] AS Atributos_MelhoraCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @Atributos_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00135,1,0,true,false )
             ,new CursorDef("T00136", "SELECT TM1.[Atributos_Codigo], TM1.[Atributos_Nome], TM1.[Atributos_Detalhes], TM1.[Atributos_Descricao], TM1.[Atributos_TipoDados], TM1.[Atributos_PK], TM1.[Atributos_FK], T2.[Tabela_Nome] AS Atributos_TabelaNom, TM1.[Atributos_Ativo], TM1.[Atributos_TabelaCod] AS Atributos_TabelaCod, TM1.[Atributos_MelhoraCod] AS Atributos_MelhoraCod, T2.[Tabela_SistemaCod] AS Tabela_SistemaCod FROM ([Atributos] TM1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = TM1.[Atributos_TabelaCod]) WHERE TM1.[Atributos_Codigo] = @Atributos_Codigo ORDER BY TM1.[Atributos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00136,100,0,true,false )
             ,new CursorDef("T00137", "SELECT [Tabela_Nome] AS Atributos_TabelaNom, [Tabela_SistemaCod] AS Tabela_SistemaCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Atributos_TabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00137,1,0,true,false )
             ,new CursorDef("T00138", "SELECT [Atributos_Codigo] AS Atributos_MelhoraCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @Atributos_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00138,1,0,true,false )
             ,new CursorDef("T00139", "SELECT [Atributos_Codigo] FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @Atributos_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00139,1,0,true,false )
             ,new CursorDef("T001310", "SELECT TOP 1 [Atributos_Codigo] FROM [Atributos] WITH (NOLOCK) WHERE ( [Atributos_Codigo] > @Atributos_Codigo) ORDER BY [Atributos_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001310,1,0,true,true )
             ,new CursorDef("T001311", "SELECT TOP 1 [Atributos_Codigo] FROM [Atributos] WITH (NOLOCK) WHERE ( [Atributos_Codigo] < @Atributos_Codigo) ORDER BY [Atributos_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001311,1,0,true,true )
             ,new CursorDef("T001312", "INSERT INTO [Atributos]([Atributos_Nome], [Atributos_Detalhes], [Atributos_Descricao], [Atributos_TipoDados], [Atributos_PK], [Atributos_FK], [Atributos_Ativo], [Atributos_TabelaCod], [Atributos_MelhoraCod]) VALUES(@Atributos_Nome, @Atributos_Detalhes, @Atributos_Descricao, @Atributos_TipoDados, @Atributos_PK, @Atributos_FK, @Atributos_Ativo, @Atributos_TabelaCod, @Atributos_MelhoraCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001312)
             ,new CursorDef("T001313", "UPDATE [Atributos] SET [Atributos_Nome]=@Atributos_Nome, [Atributos_Detalhes]=@Atributos_Detalhes, [Atributos_Descricao]=@Atributos_Descricao, [Atributos_TipoDados]=@Atributos_TipoDados, [Atributos_PK]=@Atributos_PK, [Atributos_FK]=@Atributos_FK, [Atributos_Ativo]=@Atributos_Ativo, [Atributos_TabelaCod]=@Atributos_TabelaCod, [Atributos_MelhoraCod]=@Atributos_MelhoraCod  WHERE [Atributos_Codigo] = @Atributos_Codigo", GxErrorMask.GX_NOMASK,prmT001313)
             ,new CursorDef("T001314", "DELETE FROM [Atributos]  WHERE [Atributos_Codigo] = @Atributos_Codigo", GxErrorMask.GX_NOMASK,prmT001314)
             ,new CursorDef("T001315", "SELECT [Tabela_Nome] AS Atributos_TabelaNom, [Tabela_SistemaCod] AS Tabela_SistemaCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Atributos_TabelaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001315,1,0,true,false )
             ,new CursorDef("T001316", "SELECT TOP 1 [Atributos_Codigo] AS Atributos_MelhoraCod FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_MelhoraCod] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001316,1,0,true,true )
             ,new CursorDef("T001317", "SELECT TOP 1 [ContagemItem_Lancamento], [ContagemItem_AtributosCod] FROM [ContagemItemAtributosFSoftware] WITH (NOLOCK) WHERE [ContagemItem_AtributosCod] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001317,1,0,true,true )
             ,new CursorDef("T001318", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPFAtributos_MelhoraCod] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001318,1,0,true,true )
             ,new CursorDef("T001319", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPFAtributos_AtributosCod] FROM [FuncoesAPFAtributos] WITH (NOLOCK) WHERE [FuncaoAPFAtributos_AtributosCod] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001319,1,0,true,true )
             ,new CursorDef("T001320", "SELECT TOP 1 [ContagemItemAtributosFMetrica_ItemContagemLan], [ContagemItem_FMetricasAtributosCod] FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) WHERE [ContagemItem_FMetricasAtributosCod] = @Atributos_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001320,1,0,true,true )
             ,new CursorDef("T001321", "SELECT [Atributos_Codigo] FROM [Atributos] WITH (NOLOCK) ORDER BY [Atributos_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001321,100,0,true,false )
             ,new CursorDef("T001322", "SELECT T1.[Tabela_Codigo], T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T2.[Sistema_Nome] AS Tabela_SistemaDes FROM ([Tabela] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Tabela_SistemaCod]) ORDER BY T2.[Sistema_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001322,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((bool[]) buf[12])[0] = rslt.getBool(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((bool[]) buf[12])[0] = rslt.getBool(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((bool[]) buf[14])[0] = rslt.getBool(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[10]);
                }
                stmt.SetParameter(7, (bool)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(5, (bool)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(6, (bool)parms[10]);
                }
                stmt.SetParameter(7, (bool)parms[11]);
                stmt.SetParameter(8, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[14]);
                }
                stmt.SetParameter(10, (int)parms[15]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
