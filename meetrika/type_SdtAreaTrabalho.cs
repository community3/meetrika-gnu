/*
               File: type_SdtAreaTrabalho
        Description: �rea de Trabalho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:19:27.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "AreaTrabalho" )]
   [XmlType(TypeName =  "AreaTrabalho" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtAreaTrabalho : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAreaTrabalho( )
      {
         /* Constructor for serialization */
         gxTv_SdtAreaTrabalho_Areatrabalho_descricao = "";
         gxTv_SdtAreaTrabalho_Organizacao_nome = "";
         gxTv_SdtAreaTrabalho_Estado_uf = "";
         gxTv_SdtAreaTrabalho_Estado_nome = "";
         gxTv_SdtAreaTrabalho_Municipio_nome = "";
         gxTv_SdtAreaTrabalho_Contratante_fax = "";
         gxTv_SdtAreaTrabalho_Contratante_ramal = "";
         gxTv_SdtAreaTrabalho_Contratante_telefone = "";
         gxTv_SdtAreaTrabalho_Contratante_email = "";
         gxTv_SdtAreaTrabalho_Contratante_website = "";
         gxTv_SdtAreaTrabalho_Contratante_cnpj = "";
         gxTv_SdtAreaTrabalho_Contratante_ie = "";
         gxTv_SdtAreaTrabalho_Contratante_nomefantasia = "";
         gxTv_SdtAreaTrabalho_Contratante_razaosocial = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdahost = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdauser = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdapass = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdakey = "";
         gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal = "";
         gxTv_SdtAreaTrabalho_Mode = "";
         gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z = "";
         gxTv_SdtAreaTrabalho_Organizacao_nome_Z = "";
         gxTv_SdtAreaTrabalho_Estado_uf_Z = "";
         gxTv_SdtAreaTrabalho_Estado_nome_Z = "";
         gxTv_SdtAreaTrabalho_Municipio_nome_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_fax_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_ramal_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_telefone_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_email_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_website_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_cnpj_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_ie_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z = "";
         gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z = "";
      }

      public SdtAreaTrabalho( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV5AreaTrabalho_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV5AreaTrabalho_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"AreaTrabalho_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "AreaTrabalho");
         metadata.Set("BT", "AreaTrabalho");
         metadata.Set("PK", "[ \"AreaTrabalho_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"AreaTrabalho_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Contratante_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"Organizacao_Codigo\" ],\"FKMap\":[ \"AreaTrabalho_OrganizacaoCod-Organizacao_Codigo\" ] },{ \"FK\":[ \"Servico_Codigo\" ],\"FKMap\":[ \"AreaTrabalho_ServicoPadrao-Servico_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_organizacaocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Organizacao_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_pessoacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Estado_uf_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Estado_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Municipio_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Municipio_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_fax_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ramal_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_telefone_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_email_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_website_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_cnpj_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ie_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_nomefantasia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_razaosocial_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdahost_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdauser_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdapass_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdakey_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdaaut_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdaport_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdasec_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_contagensqtdgeral_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_calculopfinal_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_servicopadrao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_validaosfm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_diasparapagar_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_contratadaupdbslcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_tipoplanilha_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_ss_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_verta_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_selusrprestadora_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_organizacaocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Organizacao_nome_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Municipio_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_fax_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_ramal_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_email_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_website_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_cnpj_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_razaosocial_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdahost_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdauser_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdapass_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdakey_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdaaut_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdaport_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contratante_emailsdasec_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_contagensqtdgeral_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_servicopadrao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_validaosfm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_diasparapagar_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_contratadaupdbslcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_tipoplanilha_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_ss_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_verta_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_selusrprestadora_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtAreaTrabalho deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtAreaTrabalho)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtAreaTrabalho obj ;
         obj = this;
         obj.gxTpr_Areatrabalho_codigo = deserialized.gxTpr_Areatrabalho_codigo;
         obj.gxTpr_Areatrabalho_descricao = deserialized.gxTpr_Areatrabalho_descricao;
         obj.gxTpr_Areatrabalho_organizacaocod = deserialized.gxTpr_Areatrabalho_organizacaocod;
         obj.gxTpr_Organizacao_nome = deserialized.gxTpr_Organizacao_nome;
         obj.gxTpr_Contratante_codigo = deserialized.gxTpr_Contratante_codigo;
         obj.gxTpr_Contratante_pessoacod = deserialized.gxTpr_Contratante_pessoacod;
         obj.gxTpr_Estado_uf = deserialized.gxTpr_Estado_uf;
         obj.gxTpr_Estado_nome = deserialized.gxTpr_Estado_nome;
         obj.gxTpr_Municipio_codigo = deserialized.gxTpr_Municipio_codigo;
         obj.gxTpr_Municipio_nome = deserialized.gxTpr_Municipio_nome;
         obj.gxTpr_Contratante_fax = deserialized.gxTpr_Contratante_fax;
         obj.gxTpr_Contratante_ramal = deserialized.gxTpr_Contratante_ramal;
         obj.gxTpr_Contratante_telefone = deserialized.gxTpr_Contratante_telefone;
         obj.gxTpr_Contratante_email = deserialized.gxTpr_Contratante_email;
         obj.gxTpr_Contratante_website = deserialized.gxTpr_Contratante_website;
         obj.gxTpr_Contratante_cnpj = deserialized.gxTpr_Contratante_cnpj;
         obj.gxTpr_Contratante_ie = deserialized.gxTpr_Contratante_ie;
         obj.gxTpr_Contratante_nomefantasia = deserialized.gxTpr_Contratante_nomefantasia;
         obj.gxTpr_Contratante_razaosocial = deserialized.gxTpr_Contratante_razaosocial;
         obj.gxTpr_Contratante_emailsdahost = deserialized.gxTpr_Contratante_emailsdahost;
         obj.gxTpr_Contratante_emailsdauser = deserialized.gxTpr_Contratante_emailsdauser;
         obj.gxTpr_Contratante_emailsdapass = deserialized.gxTpr_Contratante_emailsdapass;
         obj.gxTpr_Contratante_emailsdakey = deserialized.gxTpr_Contratante_emailsdakey;
         obj.gxTpr_Contratante_emailsdaaut = deserialized.gxTpr_Contratante_emailsdaaut;
         obj.gxTpr_Contratante_emailsdaport = deserialized.gxTpr_Contratante_emailsdaport;
         obj.gxTpr_Contratante_emailsdasec = deserialized.gxTpr_Contratante_emailsdasec;
         obj.gxTpr_Areatrabalho_contagensqtdgeral = deserialized.gxTpr_Areatrabalho_contagensqtdgeral;
         obj.gxTpr_Areatrabalho_calculopfinal = deserialized.gxTpr_Areatrabalho_calculopfinal;
         obj.gxTpr_Areatrabalho_servicopadrao = deserialized.gxTpr_Areatrabalho_servicopadrao;
         obj.gxTpr_Areatrabalho_validaosfm = deserialized.gxTpr_Areatrabalho_validaosfm;
         obj.gxTpr_Areatrabalho_diasparapagar = deserialized.gxTpr_Areatrabalho_diasparapagar;
         obj.gxTpr_Areatrabalho_contratadaupdbslcod = deserialized.gxTpr_Areatrabalho_contratadaupdbslcod;
         obj.gxTpr_Areatrabalho_tipoplanilha = deserialized.gxTpr_Areatrabalho_tipoplanilha;
         obj.gxTpr_Areatrabalho_ativo = deserialized.gxTpr_Areatrabalho_ativo;
         obj.gxTpr_Areatrabalho_ss_codigo = deserialized.gxTpr_Areatrabalho_ss_codigo;
         obj.gxTpr_Areatrabalho_verta = deserialized.gxTpr_Areatrabalho_verta;
         obj.gxTpr_Areatrabalho_selusrprestadora = deserialized.gxTpr_Areatrabalho_selusrprestadora;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Areatrabalho_codigo_Z = deserialized.gxTpr_Areatrabalho_codigo_Z;
         obj.gxTpr_Areatrabalho_descricao_Z = deserialized.gxTpr_Areatrabalho_descricao_Z;
         obj.gxTpr_Areatrabalho_organizacaocod_Z = deserialized.gxTpr_Areatrabalho_organizacaocod_Z;
         obj.gxTpr_Organizacao_nome_Z = deserialized.gxTpr_Organizacao_nome_Z;
         obj.gxTpr_Contratante_codigo_Z = deserialized.gxTpr_Contratante_codigo_Z;
         obj.gxTpr_Contratante_pessoacod_Z = deserialized.gxTpr_Contratante_pessoacod_Z;
         obj.gxTpr_Estado_uf_Z = deserialized.gxTpr_Estado_uf_Z;
         obj.gxTpr_Estado_nome_Z = deserialized.gxTpr_Estado_nome_Z;
         obj.gxTpr_Municipio_codigo_Z = deserialized.gxTpr_Municipio_codigo_Z;
         obj.gxTpr_Municipio_nome_Z = deserialized.gxTpr_Municipio_nome_Z;
         obj.gxTpr_Contratante_fax_Z = deserialized.gxTpr_Contratante_fax_Z;
         obj.gxTpr_Contratante_ramal_Z = deserialized.gxTpr_Contratante_ramal_Z;
         obj.gxTpr_Contratante_telefone_Z = deserialized.gxTpr_Contratante_telefone_Z;
         obj.gxTpr_Contratante_email_Z = deserialized.gxTpr_Contratante_email_Z;
         obj.gxTpr_Contratante_website_Z = deserialized.gxTpr_Contratante_website_Z;
         obj.gxTpr_Contratante_cnpj_Z = deserialized.gxTpr_Contratante_cnpj_Z;
         obj.gxTpr_Contratante_ie_Z = deserialized.gxTpr_Contratante_ie_Z;
         obj.gxTpr_Contratante_nomefantasia_Z = deserialized.gxTpr_Contratante_nomefantasia_Z;
         obj.gxTpr_Contratante_razaosocial_Z = deserialized.gxTpr_Contratante_razaosocial_Z;
         obj.gxTpr_Contratante_emailsdahost_Z = deserialized.gxTpr_Contratante_emailsdahost_Z;
         obj.gxTpr_Contratante_emailsdauser_Z = deserialized.gxTpr_Contratante_emailsdauser_Z;
         obj.gxTpr_Contratante_emailsdapass_Z = deserialized.gxTpr_Contratante_emailsdapass_Z;
         obj.gxTpr_Contratante_emailsdakey_Z = deserialized.gxTpr_Contratante_emailsdakey_Z;
         obj.gxTpr_Contratante_emailsdaaut_Z = deserialized.gxTpr_Contratante_emailsdaaut_Z;
         obj.gxTpr_Contratante_emailsdaport_Z = deserialized.gxTpr_Contratante_emailsdaport_Z;
         obj.gxTpr_Contratante_emailsdasec_Z = deserialized.gxTpr_Contratante_emailsdasec_Z;
         obj.gxTpr_Areatrabalho_contagensqtdgeral_Z = deserialized.gxTpr_Areatrabalho_contagensqtdgeral_Z;
         obj.gxTpr_Areatrabalho_calculopfinal_Z = deserialized.gxTpr_Areatrabalho_calculopfinal_Z;
         obj.gxTpr_Areatrabalho_servicopadrao_Z = deserialized.gxTpr_Areatrabalho_servicopadrao_Z;
         obj.gxTpr_Areatrabalho_validaosfm_Z = deserialized.gxTpr_Areatrabalho_validaosfm_Z;
         obj.gxTpr_Areatrabalho_diasparapagar_Z = deserialized.gxTpr_Areatrabalho_diasparapagar_Z;
         obj.gxTpr_Areatrabalho_contratadaupdbslcod_Z = deserialized.gxTpr_Areatrabalho_contratadaupdbslcod_Z;
         obj.gxTpr_Areatrabalho_tipoplanilha_Z = deserialized.gxTpr_Areatrabalho_tipoplanilha_Z;
         obj.gxTpr_Areatrabalho_ativo_Z = deserialized.gxTpr_Areatrabalho_ativo_Z;
         obj.gxTpr_Areatrabalho_ss_codigo_Z = deserialized.gxTpr_Areatrabalho_ss_codigo_Z;
         obj.gxTpr_Areatrabalho_verta_Z = deserialized.gxTpr_Areatrabalho_verta_Z;
         obj.gxTpr_Areatrabalho_selusrprestadora_Z = deserialized.gxTpr_Areatrabalho_selusrprestadora_Z;
         obj.gxTpr_Areatrabalho_organizacaocod_N = deserialized.gxTpr_Areatrabalho_organizacaocod_N;
         obj.gxTpr_Organizacao_nome_N = deserialized.gxTpr_Organizacao_nome_N;
         obj.gxTpr_Contratante_codigo_N = deserialized.gxTpr_Contratante_codigo_N;
         obj.gxTpr_Municipio_codigo_N = deserialized.gxTpr_Municipio_codigo_N;
         obj.gxTpr_Contratante_fax_N = deserialized.gxTpr_Contratante_fax_N;
         obj.gxTpr_Contratante_ramal_N = deserialized.gxTpr_Contratante_ramal_N;
         obj.gxTpr_Contratante_email_N = deserialized.gxTpr_Contratante_email_N;
         obj.gxTpr_Contratante_website_N = deserialized.gxTpr_Contratante_website_N;
         obj.gxTpr_Contratante_cnpj_N = deserialized.gxTpr_Contratante_cnpj_N;
         obj.gxTpr_Contratante_razaosocial_N = deserialized.gxTpr_Contratante_razaosocial_N;
         obj.gxTpr_Contratante_emailsdahost_N = deserialized.gxTpr_Contratante_emailsdahost_N;
         obj.gxTpr_Contratante_emailsdauser_N = deserialized.gxTpr_Contratante_emailsdauser_N;
         obj.gxTpr_Contratante_emailsdapass_N = deserialized.gxTpr_Contratante_emailsdapass_N;
         obj.gxTpr_Contratante_emailsdakey_N = deserialized.gxTpr_Contratante_emailsdakey_N;
         obj.gxTpr_Contratante_emailsdaaut_N = deserialized.gxTpr_Contratante_emailsdaaut_N;
         obj.gxTpr_Contratante_emailsdaport_N = deserialized.gxTpr_Contratante_emailsdaport_N;
         obj.gxTpr_Contratante_emailsdasec_N = deserialized.gxTpr_Contratante_emailsdasec_N;
         obj.gxTpr_Areatrabalho_contagensqtdgeral_N = deserialized.gxTpr_Areatrabalho_contagensqtdgeral_N;
         obj.gxTpr_Areatrabalho_servicopadrao_N = deserialized.gxTpr_Areatrabalho_servicopadrao_N;
         obj.gxTpr_Areatrabalho_validaosfm_N = deserialized.gxTpr_Areatrabalho_validaosfm_N;
         obj.gxTpr_Areatrabalho_diasparapagar_N = deserialized.gxTpr_Areatrabalho_diasparapagar_N;
         obj.gxTpr_Areatrabalho_contratadaupdbslcod_N = deserialized.gxTpr_Areatrabalho_contratadaupdbslcod_N;
         obj.gxTpr_Areatrabalho_tipoplanilha_N = deserialized.gxTpr_Areatrabalho_tipoplanilha_N;
         obj.gxTpr_Areatrabalho_ss_codigo_N = deserialized.gxTpr_Areatrabalho_ss_codigo_N;
         obj.gxTpr_Areatrabalho_verta_N = deserialized.gxTpr_Areatrabalho_verta_N;
         obj.gxTpr_Areatrabalho_selusrprestadora_N = deserialized.gxTpr_Areatrabalho_selusrprestadora_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Codigo") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Descricao") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_OrganizacaoCod") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Organizacao_Nome") )
               {
                  gxTv_SdtAreaTrabalho_Organizacao_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Codigo") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PessoaCod") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_pessoacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Estado_UF") )
               {
                  gxTv_SdtAreaTrabalho_Estado_uf = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Estado_Nome") )
               {
                  gxTv_SdtAreaTrabalho_Estado_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Municipio_Codigo") )
               {
                  gxTv_SdtAreaTrabalho_Municipio_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Municipio_Nome") )
               {
                  gxTv_SdtAreaTrabalho_Municipio_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Fax") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_fax = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Ramal") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_ramal = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Telefone") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_telefone = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Email") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_email = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_WebSite") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_website = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_CNPJ") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_cnpj = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_IE") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_ie = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_NomeFantasia") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_nomefantasia = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RazaoSocial") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_razaosocial = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaHost") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdahost = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaUser") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdauser = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPass") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdapass = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaKey") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdakey = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaAut") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdaaut = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPort") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdaport = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaSec") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdasec = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ContagensQtdGeral") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_CalculoPFinal") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ServicoPadrao") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ValidaOSFM") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_DiasParaPagar") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ContratadaUpdBslCod") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_TipoPlanilha") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Ativo") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_SS_Codigo") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_VerTA") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_verta = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_SelUsrPrestadora") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtAreaTrabalho_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtAreaTrabalho_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Codigo_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Descricao_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_OrganizacaoCod_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Organizacao_Nome_Z") )
               {
                  gxTv_SdtAreaTrabalho_Organizacao_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Codigo_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_PessoaCod_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_pessoacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Estado_UF_Z") )
               {
                  gxTv_SdtAreaTrabalho_Estado_uf_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Estado_Nome_Z") )
               {
                  gxTv_SdtAreaTrabalho_Estado_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Municipio_Codigo_Z") )
               {
                  gxTv_SdtAreaTrabalho_Municipio_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Municipio_Nome_Z") )
               {
                  gxTv_SdtAreaTrabalho_Municipio_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Fax_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_fax_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Ramal_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_ramal_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Telefone_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_telefone_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Email_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_email_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_WebSite_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_website_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_CNPJ_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_cnpj_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_IE_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_ie_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_NomeFantasia_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RazaoSocial_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaHost_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaUser_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPass_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaKey_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaAut_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPort_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdaport_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaSec_Z") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdasec_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ContagensQtdGeral_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_CalculoPFinal_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ServicoPadrao_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ValidaOSFM_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_DiasParaPagar_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ContratadaUpdBslCod_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_TipoPlanilha_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Ativo_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_SS_Codigo_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_VerTA_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_verta_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_SelUsrPrestadora_Z") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_OrganizacaoCod_N") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Organizacao_Nome_N") )
               {
                  gxTv_SdtAreaTrabalho_Organizacao_nome_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Codigo_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Municipio_Codigo_N") )
               {
                  gxTv_SdtAreaTrabalho_Municipio_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Fax_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_fax_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Ramal_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_ramal_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Email_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_email_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_WebSite_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_website_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_CNPJ_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_cnpj_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RazaoSocial_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_razaosocial_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaHost_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdahost_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaUser_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdauser_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPass_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdapass_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaKey_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdakey_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaAut_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaPort_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdaport_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_EmailSdaSec_N") )
               {
                  gxTv_SdtAreaTrabalho_Contratante_emailsdasec_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ContagensQtdGeral_N") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ServicoPadrao_N") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ValidaOSFM_N") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_DiasParaPagar_N") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_ContratadaUpdBslCod_N") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_TipoPlanilha_N") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_SS_Codigo_N") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_VerTA_N") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_verta_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_SelUsrPrestadora_N") )
               {
                  gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "AreaTrabalho";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("AreaTrabalho_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_Descricao", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Areatrabalho_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_OrganizacaoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Organizacao_Nome", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Organizacao_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_PessoaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_pessoacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Estado_UF", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Estado_uf));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Estado_Nome", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Estado_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Municipio_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Municipio_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Municipio_Nome", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Municipio_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_Fax", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_fax));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_Ramal", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_ramal));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_Telefone", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_telefone));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_Email", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_email));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_WebSite", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_website));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_CNPJ", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_cnpj));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_IE", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_ie));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_NomeFantasia", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_nomefantasia));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_RazaoSocial", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_razaosocial));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_EmailSdaHost", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_emailsdahost));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_EmailSdaUser", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_emailsdauser));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_EmailSdaPass", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_emailsdapass));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_EmailSdaKey", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_emailsdakey));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_EmailSdaAut", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAreaTrabalho_Contratante_emailsdaaut)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_EmailSdaPort", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_emailsdaport), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Contratante_EmailSdaSec", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_emailsdasec), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_ContagensQtdGeral", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral), 3, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_CalculoPFinal", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_ServicoPadrao", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_ValidaOSFM", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_DiasParaPagar", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_ContratadaUpdBslCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_TipoPlanilha", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAreaTrabalho_Areatrabalho_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_SS_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_VerTA", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_verta), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_SelUsrPrestadora", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_Descricao_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_OrganizacaoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Organizacao_Nome_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Organizacao_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_PessoaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_pessoacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Estado_UF_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Estado_uf_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Estado_Nome_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Estado_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Municipio_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Municipio_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Municipio_Nome_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Municipio_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_Fax_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_fax_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_Ramal_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_ramal_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_Telefone_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_telefone_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_Email_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_email_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_WebSite_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_website_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_CNPJ_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_cnpj_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_IE_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_ie_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_NomeFantasia_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_RazaoSocial_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaHost_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaUser_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaPass_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaKey_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaAut_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaPort_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_emailsdaport_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaSec_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_emailsdasec_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_ContagensQtdGeral_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_Z), 3, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_CalculoPFinal_Z", StringUtil.RTrim( gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_ServicoPadrao_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_ValidaOSFM_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_DiasParaPagar_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_ContratadaUpdBslCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_TipoPlanilha_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_Z), 2, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAreaTrabalho_Areatrabalho_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_SS_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_VerTA_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_verta_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_SelUsrPrestadora_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_OrganizacaoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Organizacao_Nome_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Organizacao_nome_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Municipio_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Municipio_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_Fax_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_fax_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_Ramal_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_ramal_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_Email_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_email_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_WebSite_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_website_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_CNPJ_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_cnpj_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_RazaoSocial_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_razaosocial_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaHost_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_emailsdahost_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaUser_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_emailsdauser_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaPass_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_emailsdapass_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaKey_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_emailsdakey_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaAut_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaPort_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_emailsdaport_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Contratante_EmailSdaSec_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Contratante_emailsdasec_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_ContagensQtdGeral_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_ServicoPadrao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_ValidaOSFM_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_DiasParaPagar_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_ContratadaUpdBslCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_TipoPlanilha_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_SS_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_VerTA_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_verta_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_SelUsrPrestadora_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("AreaTrabalho_Codigo", gxTv_SdtAreaTrabalho_Areatrabalho_codigo, false);
         AddObjectProperty("AreaTrabalho_Descricao", gxTv_SdtAreaTrabalho_Areatrabalho_descricao, false);
         AddObjectProperty("AreaTrabalho_OrganizacaoCod", gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod, false);
         AddObjectProperty("Organizacao_Nome", gxTv_SdtAreaTrabalho_Organizacao_nome, false);
         AddObjectProperty("Contratante_Codigo", gxTv_SdtAreaTrabalho_Contratante_codigo, false);
         AddObjectProperty("Contratante_PessoaCod", gxTv_SdtAreaTrabalho_Contratante_pessoacod, false);
         AddObjectProperty("Estado_UF", gxTv_SdtAreaTrabalho_Estado_uf, false);
         AddObjectProperty("Estado_Nome", gxTv_SdtAreaTrabalho_Estado_nome, false);
         AddObjectProperty("Municipio_Codigo", gxTv_SdtAreaTrabalho_Municipio_codigo, false);
         AddObjectProperty("Municipio_Nome", gxTv_SdtAreaTrabalho_Municipio_nome, false);
         AddObjectProperty("Contratante_Fax", gxTv_SdtAreaTrabalho_Contratante_fax, false);
         AddObjectProperty("Contratante_Ramal", gxTv_SdtAreaTrabalho_Contratante_ramal, false);
         AddObjectProperty("Contratante_Telefone", gxTv_SdtAreaTrabalho_Contratante_telefone, false);
         AddObjectProperty("Contratante_Email", gxTv_SdtAreaTrabalho_Contratante_email, false);
         AddObjectProperty("Contratante_WebSite", gxTv_SdtAreaTrabalho_Contratante_website, false);
         AddObjectProperty("Contratante_CNPJ", gxTv_SdtAreaTrabalho_Contratante_cnpj, false);
         AddObjectProperty("Contratante_IE", gxTv_SdtAreaTrabalho_Contratante_ie, false);
         AddObjectProperty("Contratante_NomeFantasia", gxTv_SdtAreaTrabalho_Contratante_nomefantasia, false);
         AddObjectProperty("Contratante_RazaoSocial", gxTv_SdtAreaTrabalho_Contratante_razaosocial, false);
         AddObjectProperty("Contratante_EmailSdaHost", gxTv_SdtAreaTrabalho_Contratante_emailsdahost, false);
         AddObjectProperty("Contratante_EmailSdaUser", gxTv_SdtAreaTrabalho_Contratante_emailsdauser, false);
         AddObjectProperty("Contratante_EmailSdaPass", gxTv_SdtAreaTrabalho_Contratante_emailsdapass, false);
         AddObjectProperty("Contratante_EmailSdaKey", gxTv_SdtAreaTrabalho_Contratante_emailsdakey, false);
         AddObjectProperty("Contratante_EmailSdaAut", gxTv_SdtAreaTrabalho_Contratante_emailsdaaut, false);
         AddObjectProperty("Contratante_EmailSdaPort", gxTv_SdtAreaTrabalho_Contratante_emailsdaport, false);
         AddObjectProperty("Contratante_EmailSdaSec", gxTv_SdtAreaTrabalho_Contratante_emailsdasec, false);
         AddObjectProperty("AreaTrabalho_ContagensQtdGeral", gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral, false);
         AddObjectProperty("AreaTrabalho_CalculoPFinal", gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal, false);
         AddObjectProperty("AreaTrabalho_ServicoPadrao", gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao, false);
         AddObjectProperty("AreaTrabalho_ValidaOSFM", gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm, false);
         AddObjectProperty("AreaTrabalho_DiasParaPagar", gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar, false);
         AddObjectProperty("AreaTrabalho_ContratadaUpdBslCod", gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod, false);
         AddObjectProperty("AreaTrabalho_TipoPlanilha", gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha, false);
         AddObjectProperty("AreaTrabalho_Ativo", gxTv_SdtAreaTrabalho_Areatrabalho_ativo, false);
         AddObjectProperty("AreaTrabalho_SS_Codigo", gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo, false);
         AddObjectProperty("AreaTrabalho_VerTA", gxTv_SdtAreaTrabalho_Areatrabalho_verta, false);
         AddObjectProperty("AreaTrabalho_SelUsrPrestadora", gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtAreaTrabalho_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtAreaTrabalho_Initialized, false);
            AddObjectProperty("AreaTrabalho_Codigo_Z", gxTv_SdtAreaTrabalho_Areatrabalho_codigo_Z, false);
            AddObjectProperty("AreaTrabalho_Descricao_Z", gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z, false);
            AddObjectProperty("AreaTrabalho_OrganizacaoCod_Z", gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_Z, false);
            AddObjectProperty("Organizacao_Nome_Z", gxTv_SdtAreaTrabalho_Organizacao_nome_Z, false);
            AddObjectProperty("Contratante_Codigo_Z", gxTv_SdtAreaTrabalho_Contratante_codigo_Z, false);
            AddObjectProperty("Contratante_PessoaCod_Z", gxTv_SdtAreaTrabalho_Contratante_pessoacod_Z, false);
            AddObjectProperty("Estado_UF_Z", gxTv_SdtAreaTrabalho_Estado_uf_Z, false);
            AddObjectProperty("Estado_Nome_Z", gxTv_SdtAreaTrabalho_Estado_nome_Z, false);
            AddObjectProperty("Municipio_Codigo_Z", gxTv_SdtAreaTrabalho_Municipio_codigo_Z, false);
            AddObjectProperty("Municipio_Nome_Z", gxTv_SdtAreaTrabalho_Municipio_nome_Z, false);
            AddObjectProperty("Contratante_Fax_Z", gxTv_SdtAreaTrabalho_Contratante_fax_Z, false);
            AddObjectProperty("Contratante_Ramal_Z", gxTv_SdtAreaTrabalho_Contratante_ramal_Z, false);
            AddObjectProperty("Contratante_Telefone_Z", gxTv_SdtAreaTrabalho_Contratante_telefone_Z, false);
            AddObjectProperty("Contratante_Email_Z", gxTv_SdtAreaTrabalho_Contratante_email_Z, false);
            AddObjectProperty("Contratante_WebSite_Z", gxTv_SdtAreaTrabalho_Contratante_website_Z, false);
            AddObjectProperty("Contratante_CNPJ_Z", gxTv_SdtAreaTrabalho_Contratante_cnpj_Z, false);
            AddObjectProperty("Contratante_IE_Z", gxTv_SdtAreaTrabalho_Contratante_ie_Z, false);
            AddObjectProperty("Contratante_NomeFantasia_Z", gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z, false);
            AddObjectProperty("Contratante_RazaoSocial_Z", gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z, false);
            AddObjectProperty("Contratante_EmailSdaHost_Z", gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z, false);
            AddObjectProperty("Contratante_EmailSdaUser_Z", gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z, false);
            AddObjectProperty("Contratante_EmailSdaPass_Z", gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z, false);
            AddObjectProperty("Contratante_EmailSdaKey_Z", gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z, false);
            AddObjectProperty("Contratante_EmailSdaAut_Z", gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_Z, false);
            AddObjectProperty("Contratante_EmailSdaPort_Z", gxTv_SdtAreaTrabalho_Contratante_emailsdaport_Z, false);
            AddObjectProperty("Contratante_EmailSdaSec_Z", gxTv_SdtAreaTrabalho_Contratante_emailsdasec_Z, false);
            AddObjectProperty("AreaTrabalho_ContagensQtdGeral_Z", gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_Z, false);
            AddObjectProperty("AreaTrabalho_CalculoPFinal_Z", gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z, false);
            AddObjectProperty("AreaTrabalho_ServicoPadrao_Z", gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_Z, false);
            AddObjectProperty("AreaTrabalho_ValidaOSFM_Z", gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_Z, false);
            AddObjectProperty("AreaTrabalho_DiasParaPagar_Z", gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_Z, false);
            AddObjectProperty("AreaTrabalho_ContratadaUpdBslCod_Z", gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_Z, false);
            AddObjectProperty("AreaTrabalho_TipoPlanilha_Z", gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_Z, false);
            AddObjectProperty("AreaTrabalho_Ativo_Z", gxTv_SdtAreaTrabalho_Areatrabalho_ativo_Z, false);
            AddObjectProperty("AreaTrabalho_SS_Codigo_Z", gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_Z, false);
            AddObjectProperty("AreaTrabalho_VerTA_Z", gxTv_SdtAreaTrabalho_Areatrabalho_verta_Z, false);
            AddObjectProperty("AreaTrabalho_SelUsrPrestadora_Z", gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_Z, false);
            AddObjectProperty("AreaTrabalho_OrganizacaoCod_N", gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_N, false);
            AddObjectProperty("Organizacao_Nome_N", gxTv_SdtAreaTrabalho_Organizacao_nome_N, false);
            AddObjectProperty("Contratante_Codigo_N", gxTv_SdtAreaTrabalho_Contratante_codigo_N, false);
            AddObjectProperty("Municipio_Codigo_N", gxTv_SdtAreaTrabalho_Municipio_codigo_N, false);
            AddObjectProperty("Contratante_Fax_N", gxTv_SdtAreaTrabalho_Contratante_fax_N, false);
            AddObjectProperty("Contratante_Ramal_N", gxTv_SdtAreaTrabalho_Contratante_ramal_N, false);
            AddObjectProperty("Contratante_Email_N", gxTv_SdtAreaTrabalho_Contratante_email_N, false);
            AddObjectProperty("Contratante_WebSite_N", gxTv_SdtAreaTrabalho_Contratante_website_N, false);
            AddObjectProperty("Contratante_CNPJ_N", gxTv_SdtAreaTrabalho_Contratante_cnpj_N, false);
            AddObjectProperty("Contratante_RazaoSocial_N", gxTv_SdtAreaTrabalho_Contratante_razaosocial_N, false);
            AddObjectProperty("Contratante_EmailSdaHost_N", gxTv_SdtAreaTrabalho_Contratante_emailsdahost_N, false);
            AddObjectProperty("Contratante_EmailSdaUser_N", gxTv_SdtAreaTrabalho_Contratante_emailsdauser_N, false);
            AddObjectProperty("Contratante_EmailSdaPass_N", gxTv_SdtAreaTrabalho_Contratante_emailsdapass_N, false);
            AddObjectProperty("Contratante_EmailSdaKey_N", gxTv_SdtAreaTrabalho_Contratante_emailsdakey_N, false);
            AddObjectProperty("Contratante_EmailSdaAut_N", gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_N, false);
            AddObjectProperty("Contratante_EmailSdaPort_N", gxTv_SdtAreaTrabalho_Contratante_emailsdaport_N, false);
            AddObjectProperty("Contratante_EmailSdaSec_N", gxTv_SdtAreaTrabalho_Contratante_emailsdasec_N, false);
            AddObjectProperty("AreaTrabalho_ContagensQtdGeral_N", gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_N, false);
            AddObjectProperty("AreaTrabalho_ServicoPadrao_N", gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_N, false);
            AddObjectProperty("AreaTrabalho_ValidaOSFM_N", gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_N, false);
            AddObjectProperty("AreaTrabalho_DiasParaPagar_N", gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_N, false);
            AddObjectProperty("AreaTrabalho_ContratadaUpdBslCod_N", gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_N, false);
            AddObjectProperty("AreaTrabalho_TipoPlanilha_N", gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_N, false);
            AddObjectProperty("AreaTrabalho_SS_Codigo_N", gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_N, false);
            AddObjectProperty("AreaTrabalho_VerTA_N", gxTv_SdtAreaTrabalho_Areatrabalho_verta_N, false);
            AddObjectProperty("AreaTrabalho_SelUsrPrestadora_N", gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Codigo" )]
      [  XmlElement( ElementName = "AreaTrabalho_Codigo"   )]
      public int gxTpr_Areatrabalho_codigo
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_codigo ;
         }

         set {
            if ( gxTv_SdtAreaTrabalho_Areatrabalho_codigo != value )
            {
               gxTv_SdtAreaTrabalho_Mode = "INS";
               this.gxTv_SdtAreaTrabalho_Areatrabalho_codigo_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Organizacao_nome_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_codigo_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_pessoacod_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Estado_uf_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Estado_nome_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Municipio_codigo_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Municipio_nome_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_fax_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_ramal_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_telefone_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_email_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_website_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_cnpj_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_ie_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_emailsdaport_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Contratante_emailsdasec_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_ativo_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_verta_Z_SetNull( );
               this.gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_Z_SetNull( );
            }
            gxTv_SdtAreaTrabalho_Areatrabalho_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "AreaTrabalho_Descricao" )]
      [  XmlElement( ElementName = "AreaTrabalho_Descricao"   )]
      public String gxTpr_Areatrabalho_descricao
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_descricao ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "AreaTrabalho_OrganizacaoCod" )]
      [  XmlElement( ElementName = "AreaTrabalho_OrganizacaoCod"   )]
      public int gxTpr_Areatrabalho_organizacaocod
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_N = 0;
            gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_N = 1;
         gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Organizacao_Nome" )]
      [  XmlElement( ElementName = "Organizacao_Nome"   )]
      public String gxTpr_Organizacao_nome
      {
         get {
            return gxTv_SdtAreaTrabalho_Organizacao_nome ;
         }

         set {
            gxTv_SdtAreaTrabalho_Organizacao_nome_N = 0;
            gxTv_SdtAreaTrabalho_Organizacao_nome = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Organizacao_nome_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Organizacao_nome_N = 1;
         gxTv_SdtAreaTrabalho_Organizacao_nome = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Organizacao_nome_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Codigo" )]
      [  XmlElement( ElementName = "Contratante_Codigo"   )]
      public int gxTpr_Contratante_codigo
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_codigo ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_codigo_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_codigo = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_codigo_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_codigo_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PessoaCod" )]
      [  XmlElement( ElementName = "Contratante_PessoaCod"   )]
      public int gxTpr_Contratante_pessoacod
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_pessoacod ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_pessoacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Estado_UF" )]
      [  XmlElement( ElementName = "Estado_UF"   )]
      public String gxTpr_Estado_uf
      {
         get {
            return gxTv_SdtAreaTrabalho_Estado_uf ;
         }

         set {
            gxTv_SdtAreaTrabalho_Estado_uf = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Estado_Nome" )]
      [  XmlElement( ElementName = "Estado_Nome"   )]
      public String gxTpr_Estado_nome
      {
         get {
            return gxTv_SdtAreaTrabalho_Estado_nome ;
         }

         set {
            gxTv_SdtAreaTrabalho_Estado_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Municipio_Codigo" )]
      [  XmlElement( ElementName = "Municipio_Codigo"   )]
      public int gxTpr_Municipio_codigo
      {
         get {
            return gxTv_SdtAreaTrabalho_Municipio_codigo ;
         }

         set {
            gxTv_SdtAreaTrabalho_Municipio_codigo_N = 0;
            gxTv_SdtAreaTrabalho_Municipio_codigo = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Municipio_codigo_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Municipio_codigo_N = 1;
         gxTv_SdtAreaTrabalho_Municipio_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Municipio_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Municipio_Nome" )]
      [  XmlElement( ElementName = "Municipio_Nome"   )]
      public String gxTpr_Municipio_nome
      {
         get {
            return gxTv_SdtAreaTrabalho_Municipio_nome ;
         }

         set {
            gxTv_SdtAreaTrabalho_Municipio_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_Fax" )]
      [  XmlElement( ElementName = "Contratante_Fax"   )]
      public String gxTpr_Contratante_fax
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_fax ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_fax_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_fax = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_fax_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_fax_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_fax = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_fax_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Ramal" )]
      [  XmlElement( ElementName = "Contratante_Ramal"   )]
      public String gxTpr_Contratante_ramal
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_ramal ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_ramal_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_ramal = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_ramal_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_ramal_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_ramal = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_ramal_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Telefone" )]
      [  XmlElement( ElementName = "Contratante_Telefone"   )]
      public String gxTpr_Contratante_telefone
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_telefone ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_telefone = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_Email" )]
      [  XmlElement( ElementName = "Contratante_Email"   )]
      public String gxTpr_Contratante_email
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_email ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_email_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_email = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_email_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_email_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_email = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_email_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_WebSite" )]
      [  XmlElement( ElementName = "Contratante_WebSite"   )]
      public String gxTpr_Contratante_website
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_website ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_website_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_website = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_website_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_website_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_website = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_website_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_CNPJ" )]
      [  XmlElement( ElementName = "Contratante_CNPJ"   )]
      public String gxTpr_Contratante_cnpj
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_cnpj ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_cnpj_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_cnpj = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_cnpj_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_cnpj_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_cnpj = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_cnpj_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_IE" )]
      [  XmlElement( ElementName = "Contratante_IE"   )]
      public String gxTpr_Contratante_ie
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_ie ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_ie = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_NomeFantasia" )]
      [  XmlElement( ElementName = "Contratante_NomeFantasia"   )]
      public String gxTpr_Contratante_nomefantasia
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_nomefantasia ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_nomefantasia = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_RazaoSocial" )]
      [  XmlElement( ElementName = "Contratante_RazaoSocial"   )]
      public String gxTpr_Contratante_razaosocial
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_razaosocial ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_razaosocial_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_razaosocial = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_razaosocial_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_razaosocial_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_razaosocial = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_razaosocial_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaHost" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaHost"   )]
      public String gxTpr_Contratante_emailsdahost
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdahost ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdahost_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_emailsdahost = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdahost_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdahost_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_emailsdahost = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdahost_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaUser" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaUser"   )]
      public String gxTpr_Contratante_emailsdauser
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdauser ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdauser_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_emailsdauser = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdauser_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdauser_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_emailsdauser = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdauser_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPass" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPass"   )]
      public String gxTpr_Contratante_emailsdapass
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdapass ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdapass_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_emailsdapass = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdapass_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdapass_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_emailsdapass = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdapass_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaKey" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaKey"   )]
      public String gxTpr_Contratante_emailsdakey
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdakey ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdakey_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_emailsdakey = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdakey_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdakey_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_emailsdakey = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdakey_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaAut" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaAut"   )]
      public bool gxTpr_Contratante_emailsdaaut
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdaaut ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_emailsdaaut = value;
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_emailsdaaut = false;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPort" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPort"   )]
      public short gxTpr_Contratante_emailsdaport
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdaport ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdaport_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_emailsdaport = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdaport_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdaport_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_emailsdaport = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdaport_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaSec" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaSec"   )]
      public short gxTpr_Contratante_emailsdasec
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdasec ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdasec_N = 0;
            gxTv_SdtAreaTrabalho_Contratante_emailsdasec = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdasec_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdasec_N = 1;
         gxTv_SdtAreaTrabalho_Contratante_emailsdasec = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdasec_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_ContagensQtdGeral" )]
      [  XmlElement( ElementName = "AreaTrabalho_ContagensQtdGeral"   )]
      public short gxTpr_Areatrabalho_contagensqtdgeral
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_N = 0;
            gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_N = 1;
         gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_CalculoPFinal" )]
      [  XmlElement( ElementName = "AreaTrabalho_CalculoPFinal"   )]
      public String gxTpr_Areatrabalho_calculopfinal
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal = (String)(value);
         }

      }

      [  SoapElement( ElementName = "AreaTrabalho_ServicoPadrao" )]
      [  XmlElement( ElementName = "AreaTrabalho_ServicoPadrao"   )]
      public int gxTpr_Areatrabalho_servicopadrao
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_N = 0;
            gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_N = 1;
         gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_ValidaOSFM" )]
      [  XmlElement( ElementName = "AreaTrabalho_ValidaOSFM"   )]
      public bool gxTpr_Areatrabalho_validaosfm
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_N = 0;
            gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm = value;
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_N = 1;
         gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm = false;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_DiasParaPagar" )]
      [  XmlElement( ElementName = "AreaTrabalho_DiasParaPagar"   )]
      public short gxTpr_Areatrabalho_diasparapagar
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_N = 0;
            gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_N = 1;
         gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_ContratadaUpdBslCod" )]
      [  XmlElement( ElementName = "AreaTrabalho_ContratadaUpdBslCod"   )]
      public int gxTpr_Areatrabalho_contratadaupdbslcod
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_N = 0;
            gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_N = 1;
         gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_TipoPlanilha" )]
      [  XmlElement( ElementName = "AreaTrabalho_TipoPlanilha"   )]
      public short gxTpr_Areatrabalho_tipoplanilha
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_N = 0;
            gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_N = 1;
         gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Ativo" )]
      [  XmlElement( ElementName = "AreaTrabalho_Ativo"   )]
      public bool gxTpr_Areatrabalho_ativo
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_ativo ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_ativo = value;
         }

      }

      [  SoapElement( ElementName = "AreaTrabalho_SS_Codigo" )]
      [  XmlElement( ElementName = "AreaTrabalho_SS_Codigo"   )]
      public int gxTpr_Areatrabalho_ss_codigo
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_N = 0;
            gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_N = 1;
         gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_VerTA" )]
      [  XmlElement( ElementName = "AreaTrabalho_VerTA"   )]
      public short gxTpr_Areatrabalho_verta
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_verta ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_verta_N = 0;
            gxTv_SdtAreaTrabalho_Areatrabalho_verta = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_verta_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_verta_N = 1;
         gxTv_SdtAreaTrabalho_Areatrabalho_verta = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_verta_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_SelUsrPrestadora" )]
      [  XmlElement( ElementName = "AreaTrabalho_SelUsrPrestadora"   )]
      public bool gxTpr_Areatrabalho_selusrprestadora
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_N = 0;
            gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora = value;
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_N = 1;
         gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora = false;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtAreaTrabalho_Mode ;
         }

         set {
            gxTv_SdtAreaTrabalho_Mode = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Mode_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Mode = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtAreaTrabalho_Initialized ;
         }

         set {
            gxTv_SdtAreaTrabalho_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Initialized_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Codigo_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_Codigo_Z"   )]
      public int gxTpr_Areatrabalho_codigo_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_codigo_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_codigo_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Descricao_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_Descricao_Z"   )]
      public String gxTpr_Areatrabalho_descricao_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_OrganizacaoCod_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_OrganizacaoCod_Z"   )]
      public int gxTpr_Areatrabalho_organizacaocod_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Organizacao_Nome_Z" )]
      [  XmlElement( ElementName = "Organizacao_Nome_Z"   )]
      public String gxTpr_Organizacao_nome_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Organizacao_nome_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Organizacao_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Organizacao_nome_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Organizacao_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Organizacao_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Codigo_Z" )]
      [  XmlElement( ElementName = "Contratante_Codigo_Z"   )]
      public int gxTpr_Contratante_codigo_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_codigo_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_codigo_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_PessoaCod_Z" )]
      [  XmlElement( ElementName = "Contratante_PessoaCod_Z"   )]
      public int gxTpr_Contratante_pessoacod_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_pessoacod_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_pessoacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_pessoacod_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_pessoacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_pessoacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Estado_UF_Z" )]
      [  XmlElement( ElementName = "Estado_UF_Z"   )]
      public String gxTpr_Estado_uf_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Estado_uf_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Estado_uf_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Estado_uf_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Estado_uf_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Estado_uf_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Estado_Nome_Z" )]
      [  XmlElement( ElementName = "Estado_Nome_Z"   )]
      public String gxTpr_Estado_nome_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Estado_nome_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Estado_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Estado_nome_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Estado_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Estado_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Municipio_Codigo_Z" )]
      [  XmlElement( ElementName = "Municipio_Codigo_Z"   )]
      public int gxTpr_Municipio_codigo_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Municipio_codigo_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Municipio_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Municipio_codigo_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Municipio_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Municipio_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Municipio_Nome_Z" )]
      [  XmlElement( ElementName = "Municipio_Nome_Z"   )]
      public String gxTpr_Municipio_nome_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Municipio_nome_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Municipio_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Municipio_nome_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Municipio_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Municipio_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Fax_Z" )]
      [  XmlElement( ElementName = "Contratante_Fax_Z"   )]
      public String gxTpr_Contratante_fax_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_fax_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_fax_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_fax_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_fax_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_fax_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Ramal_Z" )]
      [  XmlElement( ElementName = "Contratante_Ramal_Z"   )]
      public String gxTpr_Contratante_ramal_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_ramal_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_ramal_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_ramal_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_ramal_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_ramal_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Telefone_Z" )]
      [  XmlElement( ElementName = "Contratante_Telefone_Z"   )]
      public String gxTpr_Contratante_telefone_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_telefone_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_telefone_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_telefone_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_telefone_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_telefone_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Email_Z" )]
      [  XmlElement( ElementName = "Contratante_Email_Z"   )]
      public String gxTpr_Contratante_email_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_email_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_email_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_email_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_email_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_email_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_WebSite_Z" )]
      [  XmlElement( ElementName = "Contratante_WebSite_Z"   )]
      public String gxTpr_Contratante_website_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_website_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_website_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_website_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_website_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_website_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_CNPJ_Z" )]
      [  XmlElement( ElementName = "Contratante_CNPJ_Z"   )]
      public String gxTpr_Contratante_cnpj_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_cnpj_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_cnpj_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_cnpj_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_cnpj_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_cnpj_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_IE_Z" )]
      [  XmlElement( ElementName = "Contratante_IE_Z"   )]
      public String gxTpr_Contratante_ie_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_ie_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_ie_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_ie_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_ie_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_ie_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_NomeFantasia_Z" )]
      [  XmlElement( ElementName = "Contratante_NomeFantasia_Z"   )]
      public String gxTpr_Contratante_nomefantasia_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_RazaoSocial_Z" )]
      [  XmlElement( ElementName = "Contratante_RazaoSocial_Z"   )]
      public String gxTpr_Contratante_razaosocial_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaHost_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaHost_Z"   )]
      public String gxTpr_Contratante_emailsdahost_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaUser_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaUser_Z"   )]
      public String gxTpr_Contratante_emailsdauser_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPass_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPass_Z"   )]
      public String gxTpr_Contratante_emailsdapass_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaKey_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaKey_Z"   )]
      public String gxTpr_Contratante_emailsdakey_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaAut_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaAut_Z"   )]
      public bool gxTpr_Contratante_emailsdaaut_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_Z = value;
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_Z = false;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPort_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPort_Z"   )]
      public short gxTpr_Contratante_emailsdaport_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdaport_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdaport_Z = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdaport_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdaport_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdaport_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaSec_Z" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaSec_Z"   )]
      public short gxTpr_Contratante_emailsdasec_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdasec_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdasec_Z = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdasec_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdasec_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdasec_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_ContagensQtdGeral_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_ContagensQtdGeral_Z"   )]
      public short gxTpr_Areatrabalho_contagensqtdgeral_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_Z = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_CalculoPFinal_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_CalculoPFinal_Z"   )]
      public String gxTpr_Areatrabalho_calculopfinal_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z = (String)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z = "";
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_ServicoPadrao_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_ServicoPadrao_Z"   )]
      public int gxTpr_Areatrabalho_servicopadrao_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_Z = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_ValidaOSFM_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_ValidaOSFM_Z"   )]
      public bool gxTpr_Areatrabalho_validaosfm_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_Z = value;
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_Z = false;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_DiasParaPagar_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_DiasParaPagar_Z"   )]
      public short gxTpr_Areatrabalho_diasparapagar_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_Z = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_ContratadaUpdBslCod_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_ContratadaUpdBslCod_Z"   )]
      public int gxTpr_Areatrabalho_contratadaupdbslcod_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_TipoPlanilha_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_TipoPlanilha_Z"   )]
      public short gxTpr_Areatrabalho_tipoplanilha_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_Z = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Ativo_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_Ativo_Z"   )]
      public bool gxTpr_Areatrabalho_ativo_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_ativo_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_ativo_Z = value;
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_ativo_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_SS_Codigo_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_SS_Codigo_Z"   )]
      public int gxTpr_Areatrabalho_ss_codigo_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_VerTA_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_VerTA_Z"   )]
      public short gxTpr_Areatrabalho_verta_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_verta_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_verta_Z = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_verta_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_verta_Z = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_verta_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_SelUsrPrestadora_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_SelUsrPrestadora_Z"   )]
      public bool gxTpr_Areatrabalho_selusrprestadora_Z
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_Z ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_Z = value;
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_Z_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_Z = false;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_OrganizacaoCod_N" )]
      [  XmlElement( ElementName = "AreaTrabalho_OrganizacaoCod_N"   )]
      public short gxTpr_Areatrabalho_organizacaocod_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Organizacao_Nome_N" )]
      [  XmlElement( ElementName = "Organizacao_Nome_N"   )]
      public short gxTpr_Organizacao_nome_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Organizacao_nome_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Organizacao_nome_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Organizacao_nome_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Organizacao_nome_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Organizacao_nome_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Codigo_N" )]
      [  XmlElement( ElementName = "Contratante_Codigo_N"   )]
      public short gxTpr_Contratante_codigo_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_codigo_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_codigo_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Municipio_Codigo_N" )]
      [  XmlElement( ElementName = "Municipio_Codigo_N"   )]
      public short gxTpr_Municipio_codigo_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Municipio_codigo_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Municipio_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Municipio_codigo_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Municipio_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Municipio_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Fax_N" )]
      [  XmlElement( ElementName = "Contratante_Fax_N"   )]
      public short gxTpr_Contratante_fax_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_fax_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_fax_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_fax_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_fax_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_fax_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Ramal_N" )]
      [  XmlElement( ElementName = "Contratante_Ramal_N"   )]
      public short gxTpr_Contratante_ramal_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_ramal_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_ramal_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_ramal_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_ramal_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_ramal_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_Email_N" )]
      [  XmlElement( ElementName = "Contratante_Email_N"   )]
      public short gxTpr_Contratante_email_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_email_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_email_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_email_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_email_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_email_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_WebSite_N" )]
      [  XmlElement( ElementName = "Contratante_WebSite_N"   )]
      public short gxTpr_Contratante_website_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_website_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_website_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_website_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_website_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_website_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_CNPJ_N" )]
      [  XmlElement( ElementName = "Contratante_CNPJ_N"   )]
      public short gxTpr_Contratante_cnpj_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_cnpj_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_cnpj_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_cnpj_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_cnpj_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_cnpj_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_RazaoSocial_N" )]
      [  XmlElement( ElementName = "Contratante_RazaoSocial_N"   )]
      public short gxTpr_Contratante_razaosocial_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_razaosocial_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_razaosocial_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_razaosocial_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_razaosocial_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_razaosocial_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaHost_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaHost_N"   )]
      public short gxTpr_Contratante_emailsdahost_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdahost_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdahost_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdahost_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdahost_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdahost_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaUser_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaUser_N"   )]
      public short gxTpr_Contratante_emailsdauser_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdauser_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdauser_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdauser_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdauser_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdauser_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPass_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPass_N"   )]
      public short gxTpr_Contratante_emailsdapass_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdapass_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdapass_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdapass_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdapass_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdapass_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaKey_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaKey_N"   )]
      public short gxTpr_Contratante_emailsdakey_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdakey_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdakey_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdakey_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdakey_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdakey_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaAut_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaAut_N"   )]
      public short gxTpr_Contratante_emailsdaaut_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaPort_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaPort_N"   )]
      public short gxTpr_Contratante_emailsdaport_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdaport_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdaport_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdaport_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdaport_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdaport_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contratante_EmailSdaSec_N" )]
      [  XmlElement( ElementName = "Contratante_EmailSdaSec_N"   )]
      public short gxTpr_Contratante_emailsdasec_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Contratante_emailsdasec_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Contratante_emailsdasec_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Contratante_emailsdasec_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Contratante_emailsdasec_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Contratante_emailsdasec_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_ContagensQtdGeral_N" )]
      [  XmlElement( ElementName = "AreaTrabalho_ContagensQtdGeral_N"   )]
      public short gxTpr_Areatrabalho_contagensqtdgeral_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_ServicoPadrao_N" )]
      [  XmlElement( ElementName = "AreaTrabalho_ServicoPadrao_N"   )]
      public short gxTpr_Areatrabalho_servicopadrao_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_ValidaOSFM_N" )]
      [  XmlElement( ElementName = "AreaTrabalho_ValidaOSFM_N"   )]
      public short gxTpr_Areatrabalho_validaosfm_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_DiasParaPagar_N" )]
      [  XmlElement( ElementName = "AreaTrabalho_DiasParaPagar_N"   )]
      public short gxTpr_Areatrabalho_diasparapagar_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_ContratadaUpdBslCod_N" )]
      [  XmlElement( ElementName = "AreaTrabalho_ContratadaUpdBslCod_N"   )]
      public short gxTpr_Areatrabalho_contratadaupdbslcod_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_TipoPlanilha_N" )]
      [  XmlElement( ElementName = "AreaTrabalho_TipoPlanilha_N"   )]
      public short gxTpr_Areatrabalho_tipoplanilha_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_SS_Codigo_N" )]
      [  XmlElement( ElementName = "AreaTrabalho_SS_Codigo_N"   )]
      public short gxTpr_Areatrabalho_ss_codigo_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_VerTA_N" )]
      [  XmlElement( ElementName = "AreaTrabalho_VerTA_N"   )]
      public short gxTpr_Areatrabalho_verta_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_verta_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_verta_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_verta_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_verta_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_verta_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_SelUsrPrestadora_N" )]
      [  XmlElement( ElementName = "AreaTrabalho_SelUsrPrestadora_N"   )]
      public short gxTpr_Areatrabalho_selusrprestadora_N
      {
         get {
            return gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_N ;
         }

         set {
            gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_N = (short)(value);
         }

      }

      public void gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_N_SetNull( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_N = 0;
         return  ;
      }

      public bool gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtAreaTrabalho_Areatrabalho_descricao = "";
         gxTv_SdtAreaTrabalho_Organizacao_nome = "";
         gxTv_SdtAreaTrabalho_Estado_uf = "";
         gxTv_SdtAreaTrabalho_Estado_nome = "";
         gxTv_SdtAreaTrabalho_Municipio_nome = "";
         gxTv_SdtAreaTrabalho_Contratante_fax = "";
         gxTv_SdtAreaTrabalho_Contratante_ramal = "";
         gxTv_SdtAreaTrabalho_Contratante_telefone = "";
         gxTv_SdtAreaTrabalho_Contratante_email = "";
         gxTv_SdtAreaTrabalho_Contratante_website = "";
         gxTv_SdtAreaTrabalho_Contratante_cnpj = "";
         gxTv_SdtAreaTrabalho_Contratante_ie = "";
         gxTv_SdtAreaTrabalho_Contratante_nomefantasia = "";
         gxTv_SdtAreaTrabalho_Contratante_razaosocial = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdahost = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdauser = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdapass = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdakey = "";
         gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal = "MB";
         gxTv_SdtAreaTrabalho_Mode = "";
         gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z = "";
         gxTv_SdtAreaTrabalho_Organizacao_nome_Z = "";
         gxTv_SdtAreaTrabalho_Estado_uf_Z = "";
         gxTv_SdtAreaTrabalho_Estado_nome_Z = "";
         gxTv_SdtAreaTrabalho_Municipio_nome_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_fax_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_ramal_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_telefone_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_email_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_website_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_cnpj_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_ie_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z = "";
         gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z = "";
         gxTv_SdtAreaTrabalho_Contratante_emailsdaaut = false;
         gxTv_SdtAreaTrabalho_Areatrabalho_ativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "areatrabalho", "GeneXus.Programs.areatrabalho_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtAreaTrabalho_Contratante_emailsdaport ;
      private short gxTv_SdtAreaTrabalho_Contratante_emailsdasec ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_verta ;
      private short gxTv_SdtAreaTrabalho_Initialized ;
      private short gxTv_SdtAreaTrabalho_Contratante_emailsdaport_Z ;
      private short gxTv_SdtAreaTrabalho_Contratante_emailsdasec_Z ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_Z ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_Z ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_Z ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_verta_Z ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_N ;
      private short gxTv_SdtAreaTrabalho_Organizacao_nome_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_codigo_N ;
      private short gxTv_SdtAreaTrabalho_Municipio_codigo_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_fax_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_ramal_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_email_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_website_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_cnpj_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_razaosocial_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_emailsdahost_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_emailsdauser_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_emailsdapass_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_emailsdakey_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_emailsdaport_N ;
      private short gxTv_SdtAreaTrabalho_Contratante_emailsdasec_N ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_contagensqtdgeral_N ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_N ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_N ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_diasparapagar_N ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_N ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_tipoplanilha_N ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_N ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_verta_N ;
      private short gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtAreaTrabalho_Areatrabalho_codigo ;
      private int gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod ;
      private int gxTv_SdtAreaTrabalho_Contratante_codigo ;
      private int gxTv_SdtAreaTrabalho_Contratante_pessoacod ;
      private int gxTv_SdtAreaTrabalho_Municipio_codigo ;
      private int gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao ;
      private int gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod ;
      private int gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo ;
      private int gxTv_SdtAreaTrabalho_Areatrabalho_codigo_Z ;
      private int gxTv_SdtAreaTrabalho_Areatrabalho_organizacaocod_Z ;
      private int gxTv_SdtAreaTrabalho_Contratante_codigo_Z ;
      private int gxTv_SdtAreaTrabalho_Contratante_pessoacod_Z ;
      private int gxTv_SdtAreaTrabalho_Municipio_codigo_Z ;
      private int gxTv_SdtAreaTrabalho_Areatrabalho_servicopadrao_Z ;
      private int gxTv_SdtAreaTrabalho_Areatrabalho_contratadaupdbslcod_Z ;
      private int gxTv_SdtAreaTrabalho_Areatrabalho_ss_codigo_Z ;
      private String gxTv_SdtAreaTrabalho_Organizacao_nome ;
      private String gxTv_SdtAreaTrabalho_Estado_uf ;
      private String gxTv_SdtAreaTrabalho_Estado_nome ;
      private String gxTv_SdtAreaTrabalho_Municipio_nome ;
      private String gxTv_SdtAreaTrabalho_Contratante_fax ;
      private String gxTv_SdtAreaTrabalho_Contratante_ramal ;
      private String gxTv_SdtAreaTrabalho_Contratante_telefone ;
      private String gxTv_SdtAreaTrabalho_Contratante_ie ;
      private String gxTv_SdtAreaTrabalho_Contratante_nomefantasia ;
      private String gxTv_SdtAreaTrabalho_Contratante_razaosocial ;
      private String gxTv_SdtAreaTrabalho_Contratante_emailsdakey ;
      private String gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal ;
      private String gxTv_SdtAreaTrabalho_Mode ;
      private String gxTv_SdtAreaTrabalho_Organizacao_nome_Z ;
      private String gxTv_SdtAreaTrabalho_Estado_uf_Z ;
      private String gxTv_SdtAreaTrabalho_Estado_nome_Z ;
      private String gxTv_SdtAreaTrabalho_Municipio_nome_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_fax_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_ramal_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_telefone_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_ie_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_nomefantasia_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_razaosocial_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_emailsdakey_Z ;
      private String gxTv_SdtAreaTrabalho_Areatrabalho_calculopfinal_Z ;
      private String sTagName ;
      private bool gxTv_SdtAreaTrabalho_Contratante_emailsdaaut ;
      private bool gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm ;
      private bool gxTv_SdtAreaTrabalho_Areatrabalho_ativo ;
      private bool gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora ;
      private bool gxTv_SdtAreaTrabalho_Contratante_emailsdaaut_Z ;
      private bool gxTv_SdtAreaTrabalho_Areatrabalho_validaosfm_Z ;
      private bool gxTv_SdtAreaTrabalho_Areatrabalho_ativo_Z ;
      private bool gxTv_SdtAreaTrabalho_Areatrabalho_selusrprestadora_Z ;
      private String gxTv_SdtAreaTrabalho_Areatrabalho_descricao ;
      private String gxTv_SdtAreaTrabalho_Contratante_email ;
      private String gxTv_SdtAreaTrabalho_Contratante_website ;
      private String gxTv_SdtAreaTrabalho_Contratante_cnpj ;
      private String gxTv_SdtAreaTrabalho_Contratante_emailsdahost ;
      private String gxTv_SdtAreaTrabalho_Contratante_emailsdauser ;
      private String gxTv_SdtAreaTrabalho_Contratante_emailsdapass ;
      private String gxTv_SdtAreaTrabalho_Areatrabalho_descricao_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_email_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_website_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_cnpj_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_emailsdahost_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_emailsdauser_Z ;
      private String gxTv_SdtAreaTrabalho_Contratante_emailsdapass_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"AreaTrabalho", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtAreaTrabalho_RESTInterface : GxGenericCollectionItem<SdtAreaTrabalho>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtAreaTrabalho_RESTInterface( ) : base()
      {
      }

      public SdtAreaTrabalho_RESTInterface( SdtAreaTrabalho psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "AreaTrabalho_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Areatrabalho_codigo
      {
         get {
            return sdt.gxTpr_Areatrabalho_codigo ;
         }

         set {
            sdt.gxTpr_Areatrabalho_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_Descricao" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Areatrabalho_descricao
      {
         get {
            return sdt.gxTpr_Areatrabalho_descricao ;
         }

         set {
            sdt.gxTpr_Areatrabalho_descricao = (String)(value);
         }

      }

      [DataMember( Name = "AreaTrabalho_OrganizacaoCod" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Areatrabalho_organizacaocod
      {
         get {
            return sdt.gxTpr_Areatrabalho_organizacaocod ;
         }

         set {
            sdt.gxTpr_Areatrabalho_organizacaocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Organizacao_Nome" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Organizacao_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Organizacao_nome) ;
         }

         set {
            sdt.gxTpr_Organizacao_nome = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Codigo" , Order = 4 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratante_codigo
      {
         get {
            return sdt.gxTpr_Contratante_codigo ;
         }

         set {
            sdt.gxTpr_Contratante_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_PessoaCod" , Order = 5 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contratante_pessoacod
      {
         get {
            return sdt.gxTpr_Contratante_pessoacod ;
         }

         set {
            sdt.gxTpr_Contratante_pessoacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Estado_UF" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Estado_uf
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Estado_uf) ;
         }

         set {
            sdt.gxTpr_Estado_uf = (String)(value);
         }

      }

      [DataMember( Name = "Estado_Nome" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Estado_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Estado_nome) ;
         }

         set {
            sdt.gxTpr_Estado_nome = (String)(value);
         }

      }

      [DataMember( Name = "Municipio_Codigo" , Order = 8 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Municipio_codigo
      {
         get {
            return sdt.gxTpr_Municipio_codigo ;
         }

         set {
            sdt.gxTpr_Municipio_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Municipio_Nome" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Municipio_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Municipio_nome) ;
         }

         set {
            sdt.gxTpr_Municipio_nome = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Fax" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Contratante_fax
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_fax) ;
         }

         set {
            sdt.gxTpr_Contratante_fax = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Ramal" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Contratante_ramal
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_ramal) ;
         }

         set {
            sdt.gxTpr_Contratante_ramal = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Telefone" , Order = 12 )]
      [GxSeudo()]
      public String gxTpr_Contratante_telefone
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_telefone) ;
         }

         set {
            sdt.gxTpr_Contratante_telefone = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Email" , Order = 13 )]
      [GxSeudo()]
      public String gxTpr_Contratante_email
      {
         get {
            return sdt.gxTpr_Contratante_email ;
         }

         set {
            sdt.gxTpr_Contratante_email = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_WebSite" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Contratante_website
      {
         get {
            return sdt.gxTpr_Contratante_website ;
         }

         set {
            sdt.gxTpr_Contratante_website = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_CNPJ" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Contratante_cnpj
      {
         get {
            return sdt.gxTpr_Contratante_cnpj ;
         }

         set {
            sdt.gxTpr_Contratante_cnpj = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_IE" , Order = 16 )]
      [GxSeudo()]
      public String gxTpr_Contratante_ie
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_ie) ;
         }

         set {
            sdt.gxTpr_Contratante_ie = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_NomeFantasia" , Order = 17 )]
      [GxSeudo()]
      public String gxTpr_Contratante_nomefantasia
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_nomefantasia) ;
         }

         set {
            sdt.gxTpr_Contratante_nomefantasia = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_RazaoSocial" , Order = 18 )]
      [GxSeudo()]
      public String gxTpr_Contratante_razaosocial
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_razaosocial) ;
         }

         set {
            sdt.gxTpr_Contratante_razaosocial = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_EmailSdaHost" , Order = 19 )]
      [GxSeudo()]
      public String gxTpr_Contratante_emailsdahost
      {
         get {
            return sdt.gxTpr_Contratante_emailsdahost ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdahost = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_EmailSdaUser" , Order = 20 )]
      [GxSeudo()]
      public String gxTpr_Contratante_emailsdauser
      {
         get {
            return sdt.gxTpr_Contratante_emailsdauser ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdauser = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_EmailSdaPass" , Order = 21 )]
      [GxSeudo()]
      public String gxTpr_Contratante_emailsdapass
      {
         get {
            return sdt.gxTpr_Contratante_emailsdapass ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdapass = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_EmailSdaKey" , Order = 22 )]
      [GxSeudo()]
      public String gxTpr_Contratante_emailsdakey
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_emailsdakey) ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdakey = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_EmailSdaAut" , Order = 23 )]
      [GxSeudo()]
      public bool gxTpr_Contratante_emailsdaaut
      {
         get {
            return sdt.gxTpr_Contratante_emailsdaaut ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdaaut = value;
         }

      }

      [DataMember( Name = "Contratante_EmailSdaPort" , Order = 24 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratante_emailsdaport
      {
         get {
            return sdt.gxTpr_Contratante_emailsdaport ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdaport = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_EmailSdaSec" , Order = 25 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contratante_emailsdasec
      {
         get {
            return sdt.gxTpr_Contratante_emailsdasec ;
         }

         set {
            sdt.gxTpr_Contratante_emailsdasec = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_ContagensQtdGeral" , Order = 26 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Areatrabalho_contagensqtdgeral
      {
         get {
            return sdt.gxTpr_Areatrabalho_contagensqtdgeral ;
         }

         set {
            sdt.gxTpr_Areatrabalho_contagensqtdgeral = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_CalculoPFinal" , Order = 27 )]
      [GxSeudo()]
      public String gxTpr_Areatrabalho_calculopfinal
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Areatrabalho_calculopfinal) ;
         }

         set {
            sdt.gxTpr_Areatrabalho_calculopfinal = (String)(value);
         }

      }

      [DataMember( Name = "AreaTrabalho_ServicoPadrao" , Order = 28 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Areatrabalho_servicopadrao
      {
         get {
            return sdt.gxTpr_Areatrabalho_servicopadrao ;
         }

         set {
            sdt.gxTpr_Areatrabalho_servicopadrao = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_ValidaOSFM" , Order = 29 )]
      [GxSeudo()]
      public bool gxTpr_Areatrabalho_validaosfm
      {
         get {
            return sdt.gxTpr_Areatrabalho_validaosfm ;
         }

         set {
            sdt.gxTpr_Areatrabalho_validaosfm = value;
         }

      }

      [DataMember( Name = "AreaTrabalho_DiasParaPagar" , Order = 30 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Areatrabalho_diasparapagar
      {
         get {
            return sdt.gxTpr_Areatrabalho_diasparapagar ;
         }

         set {
            sdt.gxTpr_Areatrabalho_diasparapagar = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_ContratadaUpdBslCod" , Order = 31 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Areatrabalho_contratadaupdbslcod
      {
         get {
            return sdt.gxTpr_Areatrabalho_contratadaupdbslcod ;
         }

         set {
            sdt.gxTpr_Areatrabalho_contratadaupdbslcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_TipoPlanilha" , Order = 32 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Areatrabalho_tipoplanilha
      {
         get {
            return sdt.gxTpr_Areatrabalho_tipoplanilha ;
         }

         set {
            sdt.gxTpr_Areatrabalho_tipoplanilha = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_Ativo" , Order = 33 )]
      [GxSeudo()]
      public bool gxTpr_Areatrabalho_ativo
      {
         get {
            return sdt.gxTpr_Areatrabalho_ativo ;
         }

         set {
            sdt.gxTpr_Areatrabalho_ativo = value;
         }

      }

      [DataMember( Name = "AreaTrabalho_SS_Codigo" , Order = 34 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Areatrabalho_ss_codigo
      {
         get {
            return sdt.gxTpr_Areatrabalho_ss_codigo ;
         }

         set {
            sdt.gxTpr_Areatrabalho_ss_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_VerTA" , Order = 35 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Areatrabalho_verta
      {
         get {
            return sdt.gxTpr_Areatrabalho_verta ;
         }

         set {
            sdt.gxTpr_Areatrabalho_verta = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_SelUsrPrestadora" , Order = 36 )]
      [GxSeudo()]
      public bool gxTpr_Areatrabalho_selusrprestadora
      {
         get {
            return sdt.gxTpr_Areatrabalho_selusrprestadora ;
         }

         set {
            sdt.gxTpr_Areatrabalho_selusrprestadora = value;
         }

      }

      public SdtAreaTrabalho sdt
      {
         get {
            return (SdtAreaTrabalho)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtAreaTrabalho() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 102 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
