/*
               File: type_SdtGAMApplication
        Description: GAMApplication
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 21:58:9.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMApplication : GxUserType, IGxExternalObject
   {
      public SdtGAMApplication( )
      {
         initialize();
      }

      public SdtGAMApplication( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public SdtGAMApplication get( )
      {
         SdtGAMApplication returnget ;
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returnget = new SdtGAMApplication(context);
         Artech.Security.GAMApplication externalParm0 ;
         externalParm0 = GAMApplication_externalReference.Get();
         returnget.ExternalInstance = externalParm0;
         return returnget ;
      }

      public String getguid( )
      {
         String returngetguid ;
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returngetguid = "";
         returngetguid = (String)(GAMApplication_externalReference.GetGUID());
         return returngetguid ;
      }

      public IGxCollection getpermissions( SdtGAMApplicationPermissionFilter gxTp_PermissionFilter ,
                                           out IGxCollection gxTp_Errors )
      {
         IGxCollection returngetpermissions ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returngetpermissions = new GxExternalCollection( context, "SdtGAMApplicationPermission", "GeneXus.Programs");
         System.Collections.Generic.List<Artech.Security.GAMApplicationPermission> externalParm0 ;
         Artech.Security.GAMApplicationPermissionFilter externalParm1 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm2 ;
         externalParm1 = (Artech.Security.GAMApplicationPermissionFilter)(gxTp_PermissionFilter.ExternalInstance);
         externalParm0 = GAMApplication_externalReference.GetPermissions(externalParm1, out externalParm2);
         returngetpermissions.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMApplicationPermission>), externalParm0);
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm2);
         return returngetpermissions ;
      }

      public SdtGAMApplicationPermission getpermissionbyguid( String gxTp_PermissionId ,
                                                              out IGxCollection gxTp_Errors )
      {
         SdtGAMApplicationPermission returngetpermissionbyguid ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returngetpermissionbyguid = new SdtGAMApplicationPermission(context);
         Artech.Security.GAMApplicationPermission externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         externalParm0 = GAMApplication_externalReference.GetPermissionByGUID(gxTp_PermissionId, out externalParm1);
         returngetpermissionbyguid.ExternalInstance = externalParm0;
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returngetpermissionbyguid ;
      }

      public SdtGAMApplicationPermission getpermissionbyname( String gxTp_PermissionName ,
                                                              out IGxCollection gxTp_Errors )
      {
         SdtGAMApplicationPermission returngetpermissionbyname ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returngetpermissionbyname = new SdtGAMApplicationPermission(context);
         Artech.Security.GAMApplicationPermission externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         externalParm0 = GAMApplication_externalReference.GetPermissionByName(gxTp_PermissionName, out externalParm1);
         returngetpermissionbyname.ExternalInstance = externalParm0;
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returngetpermissionbyname ;
      }

      public IGxCollection getpermissionchildren( String gxTp_PermissionId ,
                                                  SdtGAMApplicationPermissionFilter gxTp_PermissionFilter ,
                                                  out IGxCollection gxTp_Errors )
      {
         IGxCollection returngetpermissionchildren ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returngetpermissionchildren = new GxExternalCollection( context, "SdtGAMApplicationPermission", "GeneXus.Programs");
         System.Collections.Generic.List<Artech.Security.GAMApplicationPermission> externalParm0 ;
         Artech.Security.GAMApplicationPermissionFilter externalParm1 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm2 ;
         externalParm1 = (Artech.Security.GAMApplicationPermissionFilter)(gxTp_PermissionFilter.ExternalInstance);
         externalParm0 = GAMApplication_externalReference.GetPermissionChildren(gxTp_PermissionId, externalParm1, out externalParm2);
         returngetpermissionchildren.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMApplicationPermission>), externalParm0);
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm2);
         return returngetpermissionchildren ;
      }

      public IGxCollection getunassignedpermissiontoparent( String gxTp_PermissionId ,
                                                            SdtGAMApplicationPermissionFilter gxTp_PermissionFilter ,
                                                            out IGxCollection gxTp_Errors )
      {
         IGxCollection returngetunassignedpermissiontoparent ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returngetunassignedpermissiontoparent = new GxExternalCollection( context, "SdtGAMApplicationPermission", "GeneXus.Programs");
         System.Collections.Generic.List<Artech.Security.GAMApplicationPermission> externalParm0 ;
         Artech.Security.GAMApplicationPermissionFilter externalParm1 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm2 ;
         externalParm1 = (Artech.Security.GAMApplicationPermissionFilter)(gxTp_PermissionFilter.ExternalInstance);
         externalParm0 = GAMApplication_externalReference.GetUnassignedPermissionToParent(gxTp_PermissionId, externalParm1, out externalParm2);
         returngetunassignedpermissiontoparent.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMApplicationPermission>), externalParm0);
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm2);
         return returngetunassignedpermissiontoparent ;
      }

      public bool addpermission( SdtGAMApplicationPermission gxTp_Permission ,
                                 out IGxCollection gxTp_Errors )
      {
         bool returnaddpermission ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returnaddpermission = false;
         Artech.Security.GAMApplicationPermission externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         externalParm0 = (Artech.Security.GAMApplicationPermission)(gxTp_Permission.ExternalInstance);
         returnaddpermission = (bool)(GAMApplication_externalReference.AddPermission(externalParm0, out externalParm1));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returnaddpermission ;
      }

      public bool updatepermission( SdtGAMApplicationPermission gxTp_Permission ,
                                    out IGxCollection gxTp_Errors )
      {
         bool returnupdatepermission ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returnupdatepermission = false;
         Artech.Security.GAMApplicationPermission externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         externalParm0 = (Artech.Security.GAMApplicationPermission)(gxTp_Permission.ExternalInstance);
         returnupdatepermission = (bool)(GAMApplication_externalReference.UpdatePermission(externalParm0, out externalParm1));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returnupdatepermission ;
      }

      public bool deletepermission( SdtGAMApplicationPermission gxTp_Permission ,
                                    out IGxCollection gxTp_Errors )
      {
         bool returndeletepermission ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returndeletepermission = false;
         Artech.Security.GAMApplicationPermission externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         externalParm0 = (Artech.Security.GAMApplicationPermission)(gxTp_Permission.ExternalInstance);
         returndeletepermission = (bool)(GAMApplication_externalReference.DeletePermission(externalParm0, out externalParm1));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returndeletepermission ;
      }

      public bool addpermissionchild( String gxTp_PermissionId ,
                                      String gxTp_PermissionChildId ,
                                      out IGxCollection gxTp_Errors )
      {
         bool returnaddpermissionchild ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returnaddpermissionchild = false;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         returnaddpermissionchild = (bool)(GAMApplication_externalReference.AddPermissionChild(gxTp_PermissionId, gxTp_PermissionChildId, out externalParm0));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returnaddpermissionchild ;
      }

      public bool deletepermissionchild( String gxTp_PermissionId ,
                                         String gxTp_PermissionChildId ,
                                         out IGxCollection gxTp_Errors )
      {
         bool returndeletepermissionchild ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returndeletepermissionchild = false;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         returndeletepermissionchild = (bool)(GAMApplication_externalReference.DeletePermissionChild(gxTp_PermissionId, gxTp_PermissionChildId, out externalParm0));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returndeletepermissionchild ;
      }

      public bool revokeclient( out IGxCollection gxTp_Errors )
      {
         bool returnrevokeclient ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returnrevokeclient = false;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         returnrevokeclient = (bool)(GAMApplication_externalReference.RevokeClient(out externalParm0));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returnrevokeclient ;
      }

      public bool authorizeclient( out IGxCollection gxTp_Errors )
      {
         bool returnauthorizeclient ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returnauthorizeclient = false;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         returnauthorizeclient = (bool)(GAMApplication_externalReference.AuthorizeClient(out externalParm0));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returnauthorizeclient ;
      }

      public bool checkpermission( String gxTp_PermissionName )
      {
         bool returncheckpermission ;
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returncheckpermission = false;
         returncheckpermission = (bool)(GAMApplication_externalReference.CheckPermission(gxTp_PermissionName));
         return returncheckpermission ;
      }

      public IGxCollection getroles( out IGxCollection gxTp_Errors )
      {
         IGxCollection returngetroles ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returngetroles = new GxExternalCollection( context, "SdtGAMRole", "GeneXus.Programs");
         System.Collections.Generic.List<Artech.Security.GAMRole> externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         externalParm0 = GAMApplication_externalReference.GetRoles(out externalParm1);
         returngetroles.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMRole>), externalParm0);
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returngetroles ;
      }

      public IGxCollection getallroles( out IGxCollection gxTp_Errors )
      {
         IGxCollection returngetallroles ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returngetallroles = new GxExternalCollection( context, "SdtGAMRole", "GeneXus.Programs");
         System.Collections.Generic.List<Artech.Security.GAMRole> externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         externalParm0 = GAMApplication_externalReference.GetAllRoles(out externalParm1);
         returngetallroles.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMRole>), externalParm0);
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returngetallroles ;
      }

      public bool addrole( SdtGAMRole gxTp_Role ,
                           out IGxCollection gxTp_Errors )
      {
         bool returnaddrole ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returnaddrole = false;
         Artech.Security.GAMRole externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         externalParm0 = (Artech.Security.GAMRole)(gxTp_Role.ExternalInstance);
         returnaddrole = (bool)(GAMApplication_externalReference.AddRole(externalParm0, out externalParm1));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returnaddrole ;
      }

      public bool addrolebyid( long gxTp_RoleId ,
                               out IGxCollection gxTp_Errors )
      {
         bool returnaddrolebyid ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returnaddrolebyid = false;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         returnaddrolebyid = (bool)(GAMApplication_externalReference.AddRoleById(gxTp_RoleId, out externalParm0));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returnaddrolebyid ;
      }

      public bool deleterole( SdtGAMRole gxTp_Role ,
                              out IGxCollection gxTp_Errors )
      {
         bool returndeleterole ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returndeleterole = false;
         Artech.Security.GAMRole externalParm0 ;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm1 ;
         externalParm0 = (Artech.Security.GAMRole)(gxTp_Role.ExternalInstance);
         returndeleterole = (bool)(GAMApplication_externalReference.DeleteRole(externalParm0, out externalParm1));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm1);
         return returndeleterole ;
      }

      public bool deleterolebyid( long gxTp_RoleId ,
                                  out IGxCollection gxTp_Errors )
      {
         bool returndeleterolebyid ;
         gxTp_Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returndeleterolebyid = false;
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         returndeleterolebyid = (bool)(GAMApplication_externalReference.DeleteRoleById(gxTp_RoleId, out externalParm0));
         gxTp_Errors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returndeleterolebyid ;
      }

      public void load( long gxTp_Id )
      {
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         GAMApplication_externalReference.Load(gxTp_Id);
         return  ;
      }

      public void save( )
      {
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         GAMApplication_externalReference.Save();
         return  ;
      }

      public void delete( )
      {
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         GAMApplication_externalReference.Delete();
         return  ;
      }

      public bool success( )
      {
         bool returnsuccess ;
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returnsuccess = false;
         returnsuccess = (bool)(GAMApplication_externalReference.Success());
         return returnsuccess ;
      }

      public bool fail( )
      {
         bool returnfail ;
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returnfail = false;
         returnfail = (bool)(GAMApplication_externalReference.Fail());
         return returnfail ;
      }

      public IGxCollection geterrors( )
      {
         IGxCollection returngeterrors ;
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returngeterrors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         System.Collections.Generic.List<Artech.Security.GAMError> externalParm0 ;
         externalParm0 = GAMApplication_externalReference.GetErrors();
         returngeterrors.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMError>), externalParm0);
         return returngeterrors ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMApplication_externalReference == null )
         {
            GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
         }
         returntostring = "";
         returntostring = (String)(GAMApplication_externalReference.ToString());
         return returntostring ;
      }

      public long gxTpr_Id
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.Id ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.Id = value;
         }

      }

      public String gxTpr_Guid
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.GUID ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.GUID = value;
         }

      }

      public String gxTpr_Version
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.Version ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.Version = value;
         }

      }

      public String gxTpr_Name
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.Name ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.Name = value;
         }

      }

      public String gxTpr_Description
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.Description ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.Description = value;
         }

      }

      public SdtGAMApplicationEnvironment gxTpr_Environment
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            SdtGAMApplicationEnvironment intValue ;
            intValue = new SdtGAMApplicationEnvironment(context);
            Artech.Security.GAMApplicationEnvironment externalParm0 ;
            externalParm0 = GAMApplication_externalReference.Environment;
            intValue.ExternalInstance = externalParm0;
            return intValue ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            SdtGAMApplicationEnvironment intValue ;
            Artech.Security.GAMApplicationEnvironment externalParm1 ;
            intValue = value;
            externalParm1 = (Artech.Security.GAMApplicationEnvironment)(intValue.ExternalInstance);
            GAMApplication_externalReference.Environment = externalParm1;
         }

      }

      public String gxTpr_Companyname
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.CompanyName ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.CompanyName = value;
         }

      }

      public String gxTpr_Copyright
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.Copyright ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.Copyright = value;
         }

      }

      public bool gxTpr_Isbaseapplication
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.IsBaseApplication ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.IsBaseApplication = value;
         }

      }

      public long gxTpr_Applicationbase
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ApplicationBase ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ApplicationBase = value;
         }

      }

      public long gxTpr_Applicationparent
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ApplicationParent ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ApplicationParent = value;
         }

      }

      public bool gxTpr_Returnmenuoptionswithoutpermission
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ReturnMenuOptionsWithoutPermission ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ReturnMenuOptionsWithoutPermission = value;
         }

      }

      public bool gxTpr_Accessrequirespermission
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.AccessRequiresPermission ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.AccessRequiresPermission = value;
         }

      }

      public int gxTpr_Order
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.Order ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.Order = value;
         }

      }

      public short gxTpr_Type
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.Type ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.Type = value;
         }

      }

      public bool gxTpr_Isauthorizationdelegated
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.IsAuthorizationDelegated ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.IsAuthorizationDelegated = value;
         }

      }

      public SdtGAMApplicationDelegateAuthorization gxTpr_Delegateauthorization
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            SdtGAMApplicationDelegateAuthorization intValue ;
            intValue = new SdtGAMApplicationDelegateAuthorization(context);
            Artech.Security.GAMApplicationDelegateAuthorization externalParm2 ;
            externalParm2 = GAMApplication_externalReference.DelegateAuthorization;
            intValue.ExternalInstance = externalParm2;
            return intValue ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            SdtGAMApplicationDelegateAuthorization intValue ;
            Artech.Security.GAMApplicationDelegateAuthorization externalParm3 ;
            intValue = value;
            externalParm3 = (Artech.Security.GAMApplicationDelegateAuthorization)(intValue.ExternalInstance);
            GAMApplication_externalReference.DelegateAuthorization = externalParm3;
         }

      }

      public bool gxTpr_Clientautoregisteranomymoususer
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientAutoRegisterAnomymousUser ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientAutoRegisterAnomymousUser = value;
         }

      }

      public String gxTpr_Clientid
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientId ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientId = value;
         }

      }

      public String gxTpr_Clientsecret
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientSecret ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientSecret = value;
         }

      }

      public bool gxTpr_Clientaccessuniquebyuser
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientAccessUniqueByUser ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientAccessUniqueByUser = value;
         }

      }

      public DateTime gxTpr_Clientrevoked
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientRevoked ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientRevoked = value;
         }

      }

      public bool gxTpr_Clientallowremoteauthentication
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientAllowRemoteAuthentication ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientAllowRemoteAuthentication = value;
         }

      }

      public bool gxTpr_Clientallowgetuseradditionaldata
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientAllowGetUserAdditionalData ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientAllowGetUserAdditionalData = value;
         }

      }

      public bool gxTpr_Clientallowgetuserroles
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientAllowGetUserRoles ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientAllowGetUserRoles = value;
         }

      }

      public String gxTpr_Clientlocalloginurl
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientLocalLoginURL ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientLocalLoginURL = value;
         }

      }

      public String gxTpr_Clientcallbackurl
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientCallbackURL ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientCallbackURL = value;
         }

      }

      public String gxTpr_Clientimageurl
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientImageURL ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientImageURL = value;
         }

      }

      public String gxTpr_Clientencryptionkey
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.ClientEncryptionKey ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.ClientEncryptionKey = value;
         }

      }

      public IGxCollection gxTpr_Descriptions
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMDescription", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm4 ;
            externalParm4 = GAMApplication_externalReference.Descriptions;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), externalParm4);
            return intValue ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMDescription> externalParm5 ;
            intValue = value;
            externalParm5 = (System.Collections.Generic.List<Artech.Security.GAMDescription>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMDescription>), intValue.ExternalInstance);
            GAMApplication_externalReference.Descriptions = externalParm5;
         }

      }

      public IGxCollection gxTpr_Properties
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            IGxCollection intValue ;
            intValue = new GxExternalCollection( context, "SdtGAMProperty", "GeneXus.Programs");
            System.Collections.Generic.List<Artech.Security.GAMProperty> externalParm6 ;
            externalParm6 = GAMApplication_externalReference.Properties;
            intValue.ExternalInstance = (IList)CollectionUtils.ConvertToInternal( typeof(System.Collections.Generic.List<Artech.Security.GAMProperty>), externalParm6);
            return intValue ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            IGxCollection intValue ;
            System.Collections.Generic.List<Artech.Security.GAMProperty> externalParm7 ;
            intValue = value;
            externalParm7 = (System.Collections.Generic.List<Artech.Security.GAMProperty>)CollectionUtils.ConvertToExternal( typeof(System.Collections.Generic.List<Artech.Security.GAMProperty>), intValue.ExternalInstance);
            GAMApplication_externalReference.Properties = externalParm7;
         }

      }

      public DateTime gxTpr_Datecreated
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.DateCreated ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.DateCreated = value;
         }

      }

      public String gxTpr_Usercreated
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.UserCreated ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.UserCreated = value;
         }

      }

      public DateTime gxTpr_Dateupdated
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.DateUpdated ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.DateUpdated = value;
         }

      }

      public String gxTpr_Userupdated
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference.UserUpdated ;
         }

         set {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            GAMApplication_externalReference.UserUpdated = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMApplication_externalReference == null )
            {
               GAMApplication_externalReference = new Artech.Security.GAMApplication(context);
            }
            return GAMApplication_externalReference ;
         }

         set {
            GAMApplication_externalReference = (Artech.Security.GAMApplication)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMApplication GAMApplication_externalReference=null ;
   }

}
