/*
               File: ContratoDadosCertameGeneral
        Description: Contrato Dados Certame General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:43.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratodadoscertamegeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratodadoscertamegeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratodadoscertamegeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoDadosCertame_Codigo )
      {
         this.A314ContratoDadosCertame_Codigo = aP0_ContratoDadosCertame_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A314ContratoDadosCertame_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A314ContratoDadosCertame_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA7I2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ContratoDadosCertameGeneral";
               context.Gx_err = 0;
               WS7I2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Dados Certame General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117194391");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratodadoscertamegeneral.aspx") + "?" + UrlEncode("" +A314ContratoDadosCertame_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA314ContratoDadosCertame_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATODADOSCERTAME_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATODADOSCERTAME_DATA", GetSecureSignedToken( sPrefix, A311ContratoDadosCertame_Data));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATODADOSCERTAME_MODALIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A307ContratoDadosCertame_Modalidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATODADOSCERTAME_NUMERO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A308ContratoDadosCertame_Numero), "ZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATODADOSCERTAME_SITE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATODADOSCERTAME_UASG", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A310ContratoDadosCertame_Uasg), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATODADOSCERTAME_DATAHOMOLOGACAO", GetSecureSignedToken( sPrefix, A312ContratoDadosCertame_DataHomologacao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATODADOSCERTAME_DATAADJUDICACAO", GetSecureSignedToken( sPrefix, A313ContratoDadosCertame_DataAdjudicacao));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContratoDadosCertameGeneral";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, ""));
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratodadoscertamegeneral:[SendSecurityCheck value for]"+"ContratoDadosCertame_Site:"+StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, "")));
      }

      protected void RenderHtmlCloseForm7I2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratodadoscertamegeneral.js", "?20203117194394");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoDadosCertameGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Dados Certame General" ;
      }

      protected void WB7I0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratodadoscertamegeneral.aspx");
            }
            wb_table1_2_7I2( true) ;
         }
         else
         {
            wb_table1_2_7I2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_7I2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START7I2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Dados Certame General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP7I0( ) ;
            }
         }
      }

      protected void WS7I2( )
      {
         START7I2( ) ;
         EVT7I2( ) ;
      }

      protected void EVT7I2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E117I2 */
                                    E117I2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E127I2 */
                                    E127I2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E137I2 */
                                    E137I2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E147I2 */
                                    E147I2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP7I0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE7I2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm7I2( ) ;
            }
         }
      }

      protected void PA7I2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF7I2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoDadosCertameGeneral";
         context.Gx_err = 0;
      }

      protected void RF7I2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H007I2 */
            pr_default.execute(0, new Object[] {A314ContratoDadosCertame_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A40Contratada_PessoaCod = H007I2_A40Contratada_PessoaCod[0];
               A74Contrato_Codigo = H007I2_A74Contrato_Codigo[0];
               A39Contratada_Codigo = H007I2_A39Contratada_Codigo[0];
               A313ContratoDadosCertame_DataAdjudicacao = H007I2_A313ContratoDadosCertame_DataAdjudicacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A313ContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_DATAADJUDICACAO", GetSecureSignedToken( sPrefix, A313ContratoDadosCertame_DataAdjudicacao));
               n313ContratoDadosCertame_DataAdjudicacao = H007I2_n313ContratoDadosCertame_DataAdjudicacao[0];
               A312ContratoDadosCertame_DataHomologacao = H007I2_A312ContratoDadosCertame_DataHomologacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A312ContratoDadosCertame_DataHomologacao", context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_DATAHOMOLOGACAO", GetSecureSignedToken( sPrefix, A312ContratoDadosCertame_DataHomologacao));
               n312ContratoDadosCertame_DataHomologacao = H007I2_n312ContratoDadosCertame_DataHomologacao[0];
               A310ContratoDadosCertame_Uasg = H007I2_A310ContratoDadosCertame_Uasg[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A310ContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_UASG", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A310ContratoDadosCertame_Uasg), "ZZZ9")));
               n310ContratoDadosCertame_Uasg = H007I2_n310ContratoDadosCertame_Uasg[0];
               A309ContratoDadosCertame_Site = H007I2_A309ContratoDadosCertame_Site[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A309ContratoDadosCertame_Site", A309ContratoDadosCertame_Site);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_SITE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, ""))));
               n309ContratoDadosCertame_Site = H007I2_n309ContratoDadosCertame_Site[0];
               A308ContratoDadosCertame_Numero = H007I2_A308ContratoDadosCertame_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A308ContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A308ContratoDadosCertame_Numero), 10, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_NUMERO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A308ContratoDadosCertame_Numero), "ZZZZZZZZZ9")));
               n308ContratoDadosCertame_Numero = H007I2_n308ContratoDadosCertame_Numero[0];
               A307ContratoDadosCertame_Modalidade = H007I2_A307ContratoDadosCertame_Modalidade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A307ContratoDadosCertame_Modalidade", A307ContratoDadosCertame_Modalidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_MODALIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A307ContratoDadosCertame_Modalidade, ""))));
               A311ContratoDadosCertame_Data = H007I2_A311ContratoDadosCertame_Data[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A311ContratoDadosCertame_Data", context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_DATA", GetSecureSignedToken( sPrefix, A311ContratoDadosCertame_Data));
               A42Contratada_PessoaCNPJ = H007I2_A42Contratada_PessoaCNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               n42Contratada_PessoaCNPJ = H007I2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H007I2_A41Contratada_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               n41Contratada_PessoaNom = H007I2_n41Contratada_PessoaNom[0];
               A79Contrato_Ano = H007I2_A79Contrato_Ano[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A78Contrato_NumeroAta = H007I2_A78Contrato_NumeroAta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               n78Contrato_NumeroAta = H007I2_n78Contrato_NumeroAta[0];
               A77Contrato_Numero = H007I2_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
               A39Contratada_Codigo = H007I2_A39Contratada_Codigo[0];
               A79Contrato_Ano = H007I2_A79Contrato_Ano[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
               A78Contrato_NumeroAta = H007I2_A78Contrato_NumeroAta[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
               n78Contrato_NumeroAta = H007I2_n78Contrato_NumeroAta[0];
               A77Contrato_Numero = H007I2_A77Contrato_Numero[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
               A40Contratada_PessoaCod = H007I2_A40Contratada_PessoaCod[0];
               A42Contratada_PessoaCNPJ = H007I2_A42Contratada_PessoaCNPJ[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
               n42Contratada_PessoaCNPJ = H007I2_n42Contratada_PessoaCNPJ[0];
               A41Contratada_PessoaNom = H007I2_A41Contratada_PessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
               n41Contratada_PessoaNom = H007I2_n41Contratada_PessoaNom[0];
               /* Execute user event: E127I2 */
               E127I2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB7I0( ) ;
         }
      }

      protected void STRUP7I0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ContratoDadosCertameGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E117I2 */
         E117I2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A77Contrato_Numero = cgiGet( edtContrato_Numero_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A77Contrato_Numero", A77Contrato_Numero);
            A78Contrato_NumeroAta = cgiGet( edtContrato_NumeroAta_Internalname);
            n78Contrato_NumeroAta = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A78Contrato_NumeroAta", A78Contrato_NumeroAta);
            A79Contrato_Ano = (short)(context.localUtil.CToN( cgiGet( edtContrato_Ano_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A79Contrato_Ano", StringUtil.LTrim( StringUtil.Str( (decimal)(A79Contrato_Ano), 4, 0)));
            A41Contratada_PessoaNom = StringUtil.Upper( cgiGet( edtContratada_PessoaNom_Internalname));
            n41Contratada_PessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A41Contratada_PessoaNom", A41Contratada_PessoaNom);
            A42Contratada_PessoaCNPJ = cgiGet( edtContratada_PessoaCNPJ_Internalname);
            n42Contratada_PessoaCNPJ = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A42Contratada_PessoaCNPJ", A42Contratada_PessoaCNPJ);
            A311ContratoDadosCertame_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoDadosCertame_Data_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A311ContratoDadosCertame_Data", context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_DATA", GetSecureSignedToken( sPrefix, A311ContratoDadosCertame_Data));
            A307ContratoDadosCertame_Modalidade = cgiGet( edtContratoDadosCertame_Modalidade_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A307ContratoDadosCertame_Modalidade", A307ContratoDadosCertame_Modalidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_MODALIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A307ContratoDadosCertame_Modalidade, ""))));
            A308ContratoDadosCertame_Numero = (long)(context.localUtil.CToN( cgiGet( edtContratoDadosCertame_Numero_Internalname), ",", "."));
            n308ContratoDadosCertame_Numero = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A308ContratoDadosCertame_Numero", StringUtil.LTrim( StringUtil.Str( (decimal)(A308ContratoDadosCertame_Numero), 10, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_NUMERO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A308ContratoDadosCertame_Numero), "ZZZZZZZZZ9")));
            A309ContratoDadosCertame_Site = cgiGet( edtContratoDadosCertame_Site_Internalname);
            n309ContratoDadosCertame_Site = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A309ContratoDadosCertame_Site", A309ContratoDadosCertame_Site);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_SITE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, ""))));
            A310ContratoDadosCertame_Uasg = (short)(context.localUtil.CToN( cgiGet( edtContratoDadosCertame_Uasg_Internalname), ",", "."));
            n310ContratoDadosCertame_Uasg = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A310ContratoDadosCertame_Uasg", StringUtil.LTrim( StringUtil.Str( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_UASG", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A310ContratoDadosCertame_Uasg), "ZZZ9")));
            A312ContratoDadosCertame_DataHomologacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoDadosCertame_DataHomologacao_Internalname), 0));
            n312ContratoDadosCertame_DataHomologacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A312ContratoDadosCertame_DataHomologacao", context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_DATAHOMOLOGACAO", GetSecureSignedToken( sPrefix, A312ContratoDadosCertame_DataHomologacao));
            A313ContratoDadosCertame_DataAdjudicacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContratoDadosCertame_DataAdjudicacao_Internalname), 0));
            n313ContratoDadosCertame_DataAdjudicacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A313ContratoDadosCertame_DataAdjudicacao", context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_DATAADJUDICACAO", GetSecureSignedToken( sPrefix, A313ContratoDadosCertame_DataAdjudicacao));
            /* Read saved values. */
            wcpOA314ContratoDadosCertame_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA314ContratoDadosCertame_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContratoDadosCertameGeneral";
            A309ContratoDadosCertame_Site = cgiGet( edtContratoDadosCertame_Site_Internalname);
            n309ContratoDadosCertame_Site = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A309ContratoDadosCertame_Site", A309ContratoDadosCertame_Site);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTRATODADOSCERTAME_SITE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, ""))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, ""));
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contratodadoscertamegeneral:[SecurityCheckFailed value for]"+"ContratoDadosCertame_Site:"+StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, "")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E117I2 */
         E117I2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E117I2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E127I2( )
      {
         /* Load Routine */
         edtContratoDadosCertame_Site_Linktarget = "_blank";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoDadosCertame_Site_Internalname, "Linktarget", edtContratoDadosCertame_Site_Linktarget);
         edtContratoDadosCertame_Site_Link = A309ContratoDadosCertame_Site;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoDadosCertame_Site_Internalname, "Link", edtContratoDadosCertame_Site_Link);
         edtContrato_NumeroAta_Link = formatLink("viewcontrato.aspx") + "?" + UrlEncode("" +A74Contrato_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_NumeroAta_Internalname, "Link", edtContrato_NumeroAta_Link);
         edtContratada_PessoaNom_Link = formatLink("viewcontratada.aspx") + "?" + UrlEncode("" +A39Contratada_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratada_PessoaNom_Internalname, "Link", edtContratada_PessoaNom_Link);
      }

      protected void E137I2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contratodadoscertame.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A314ContratoDadosCertame_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E147I2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contratodadoscertame.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A314ContratoDadosCertame_Codigo) + "," + UrlEncode("" +A74Contrato_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoDadosCertame";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContratoDadosCertame_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoDadosCertame_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_7I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_7I2( true) ;
         }
         else
         {
            wb_table2_8_7I2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_7I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_77_7I2( true) ;
         }
         else
         {
            wb_table3_77_7I2( false) ;
         }
         return  ;
      }

      protected void wb_table3_77_7I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_7I2e( true) ;
         }
         else
         {
            wb_table1_2_7I2e( false) ;
         }
      }

      protected void wb_table3_77_7I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_77_7I2e( true) ;
         }
         else
         {
            wb_table3_77_7I2e( false) ;
         }
      }

      protected void wb_table2_8_7I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup1_Internalname, "do Contrato", 1, 0, "px", 0, "px", "Group", "", "HLP_ContratoDadosCertameGeneral.htm");
            wb_table4_12_7I2( true) ;
         }
         else
         {
            wb_table4_12_7I2( false) ;
         }
         return  ;
      }

      protected void wb_table4_12_7I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_data_Internalname, "Data", "", "", lblTextblockcontratodadoscertame_data_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContratoDadosCertame_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_Data_Internalname, context.localUtil.Format(A311ContratoDadosCertame_Data, "99/99/99"), context.localUtil.Format( A311ContratoDadosCertame_Data, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_Data_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoDadosCertameGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContratoDadosCertame_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_modalidade_Internalname, "Modalidade", "", "", lblTextblockcontratodadoscertame_modalidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_Modalidade_Internalname, A307ContratoDadosCertame_Modalidade, StringUtil.RTrim( context.localUtil.Format( A307ContratoDadosCertame_Modalidade, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_Modalidade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_numero_Internalname, "Numero", "", "", lblTextblockcontratodadoscertame_numero_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_Numero_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A308ContratoDadosCertame_Numero), 10, 0, ",", "")), context.localUtil.Format( (decimal)(A308ContratoDadosCertame_Numero), "ZZZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_site_Internalname, "Site", "", "", lblTextblockcontratodadoscertame_site_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_Site_Internalname, A309ContratoDadosCertame_Site, StringUtil.RTrim( context.localUtil.Format( A309ContratoDadosCertame_Site, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratoDadosCertame_Site_Link, edtContratoDadosCertame_Site_Linktarget, "", "", edtContratoDadosCertame_Site_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 360, "px", 1, "row", 1000, 0, 0, 0, 1, -1, -1, true, "URLString", "left", true, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_uasg_Internalname, "Uasg", "", "", lblTextblockcontratodadoscertame_uasg_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_Uasg_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A310ContratoDadosCertame_Uasg), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A310ContratoDadosCertame_Uasg), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_Uasg_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_datahomologacao_Internalname, "Data de Homologa��o", "", "", lblTextblockcontratodadoscertame_datahomologacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_67_7I2( true) ;
         }
         else
         {
            wb_table5_67_7I2( false) ;
         }
         return  ;
      }

      protected void wb_table5_67_7I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_7I2e( true) ;
         }
         else
         {
            wb_table2_8_7I2e( false) ;
         }
      }

      protected void wb_table5_67_7I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratodadoscertame_datahomologacao_Internalname, tblTablemergedcontratodadoscertame_datahomologacao_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContratoDadosCertame_DataHomologacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_DataHomologacao_Internalname, context.localUtil.Format(A312ContratoDadosCertame_DataHomologacao, "99/99/99"), context.localUtil.Format( A312ContratoDadosCertame_DataHomologacao, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_DataHomologacao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoDadosCertameGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContratoDadosCertame_DataHomologacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratodadoscertame_dataadjudicacao_Internalname, "Data de Adjudica��o", "", "", lblTextblockcontratodadoscertame_dataadjudicacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContratoDadosCertame_DataAdjudicacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContratoDadosCertame_DataAdjudicacao_Internalname, context.localUtil.Format(A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"), context.localUtil.Format( A313ContratoDadosCertame_DataAdjudicacao, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoDadosCertame_DataAdjudicacao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContratoDadosCertameGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContratoDadosCertame_DataAdjudicacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_67_7I2e( true) ;
         }
         else
         {
            wb_table5_67_7I2e( false) ;
         }
      }

      protected void wb_table4_12_7I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblContrato_Internalname, tblContrato_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numero_Internalname, "N� do Contrato", "", "", lblTextblockcontrato_numero_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_17_7I2( true) ;
         }
         else
         {
            wb_table6_17_7I2( false) ;
         }
         return  ;
      }

      protected void wb_table6_17_7I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoanom_Internalname, "Contratada", "", "", lblTextblockcontratada_pessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_33_7I2( true) ;
         }
         else
         {
            wb_table7_33_7I2( false) ;
         }
         return  ;
      }

      protected void wb_table7_33_7I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_12_7I2e( true) ;
         }
         else
         {
            wb_table4_12_7I2e( false) ;
         }
      }

      protected void wb_table7_33_7I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratada_pessoanom_Internalname, tblTablemergedcontratada_pessoanom_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaNom_Internalname, StringUtil.RTrim( A41Contratada_PessoaNom), StringUtil.RTrim( context.localUtil.Format( A41Contratada_PessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContratada_PessoaNom_Link, "", "", "", edtContratada_PessoaNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratada_pessoacnpj_Internalname, "CNPJ", "", "", lblTextblockcontratada_pessoacnpj_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratada_PessoaCNPJ_Internalname, A42Contratada_PessoaCNPJ, StringUtil.RTrim( context.localUtil.Format( A42Contratada_PessoaCNPJ, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratada_PessoaCNPJ_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_33_7I2e( true) ;
         }
         else
         {
            wb_table7_33_7I2e( false) ;
         }
      }

      protected void wb_table6_17_7I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontrato_numero_Internalname, tblTablemergedcontrato_numero_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Numero_Internalname, StringUtil.RTrim( A77Contrato_Numero), StringUtil.RTrim( context.localUtil.Format( A77Contrato_Numero, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Numero_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "NumeroContrato", "left", true, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_numeroata_Internalname, "N� da Ata", "", "", lblTextblockcontrato_numeroata_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_NumeroAta_Internalname, StringUtil.RTrim( A78Contrato_NumeroAta), StringUtil.RTrim( context.localUtil.Format( A78Contrato_NumeroAta, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContrato_NumeroAta_Link, "", "", "", edtContrato_NumeroAta_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "NumeroAta", "left", true, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontrato_ano_Internalname, "Ano", "", "", lblTextblockcontrato_ano_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Ano_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A79Contrato_Ano), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A79Contrato_Ano), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Ano_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Ano", "right", false, "HLP_ContratoDadosCertameGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_17_7I2e( true) ;
         }
         else
         {
            wb_table6_17_7I2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A314ContratoDadosCertame_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA7I2( ) ;
         WS7I2( ) ;
         WE7I2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA314ContratoDadosCertame_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA7I2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratodadoscertamegeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA7I2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A314ContratoDadosCertame_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0)));
         }
         wcpOA314ContratoDadosCertame_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA314ContratoDadosCertame_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A314ContratoDadosCertame_Codigo != wcpOA314ContratoDadosCertame_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA314ContratoDadosCertame_Codigo = A314ContratoDadosCertame_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA314ContratoDadosCertame_Codigo = cgiGet( sPrefix+"A314ContratoDadosCertame_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA314ContratoDadosCertame_Codigo) > 0 )
         {
            A314ContratoDadosCertame_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA314ContratoDadosCertame_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A314ContratoDadosCertame_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0)));
         }
         else
         {
            A314ContratoDadosCertame_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A314ContratoDadosCertame_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA7I2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS7I2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS7I2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A314ContratoDadosCertame_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A314ContratoDadosCertame_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA314ContratoDadosCertame_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A314ContratoDadosCertame_Codigo_CTRL", StringUtil.RTrim( sCtrlA314ContratoDadosCertame_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE7I2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117194464");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratodadoscertamegeneral.js", "?20203117194464");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontrato_numero_Internalname = sPrefix+"TEXTBLOCKCONTRATO_NUMERO";
         edtContrato_Numero_Internalname = sPrefix+"CONTRATO_NUMERO";
         lblTextblockcontrato_numeroata_Internalname = sPrefix+"TEXTBLOCKCONTRATO_NUMEROATA";
         edtContrato_NumeroAta_Internalname = sPrefix+"CONTRATO_NUMEROATA";
         lblTextblockcontrato_ano_Internalname = sPrefix+"TEXTBLOCKCONTRATO_ANO";
         edtContrato_Ano_Internalname = sPrefix+"CONTRATO_ANO";
         tblTablemergedcontrato_numero_Internalname = sPrefix+"TABLEMERGEDCONTRATO_NUMERO";
         lblTextblockcontratada_pessoanom_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_PESSOANOM";
         edtContratada_PessoaNom_Internalname = sPrefix+"CONTRATADA_PESSOANOM";
         lblTextblockcontratada_pessoacnpj_Internalname = sPrefix+"TEXTBLOCKCONTRATADA_PESSOACNPJ";
         edtContratada_PessoaCNPJ_Internalname = sPrefix+"CONTRATADA_PESSOACNPJ";
         tblTablemergedcontratada_pessoanom_Internalname = sPrefix+"TABLEMERGEDCONTRATADA_PESSOANOM";
         tblContrato_Internalname = sPrefix+"CONTRATO";
         grpUnnamedgroup1_Internalname = sPrefix+"UNNAMEDGROUP1";
         lblTextblockcontratodadoscertame_data_Internalname = sPrefix+"TEXTBLOCKCONTRATODADOSCERTAME_DATA";
         edtContratoDadosCertame_Data_Internalname = sPrefix+"CONTRATODADOSCERTAME_DATA";
         lblTextblockcontratodadoscertame_modalidade_Internalname = sPrefix+"TEXTBLOCKCONTRATODADOSCERTAME_MODALIDADE";
         edtContratoDadosCertame_Modalidade_Internalname = sPrefix+"CONTRATODADOSCERTAME_MODALIDADE";
         lblTextblockcontratodadoscertame_numero_Internalname = sPrefix+"TEXTBLOCKCONTRATODADOSCERTAME_NUMERO";
         edtContratoDadosCertame_Numero_Internalname = sPrefix+"CONTRATODADOSCERTAME_NUMERO";
         lblTextblockcontratodadoscertame_site_Internalname = sPrefix+"TEXTBLOCKCONTRATODADOSCERTAME_SITE";
         edtContratoDadosCertame_Site_Internalname = sPrefix+"CONTRATODADOSCERTAME_SITE";
         lblTextblockcontratodadoscertame_uasg_Internalname = sPrefix+"TEXTBLOCKCONTRATODADOSCERTAME_UASG";
         edtContratoDadosCertame_Uasg_Internalname = sPrefix+"CONTRATODADOSCERTAME_UASG";
         lblTextblockcontratodadoscertame_datahomologacao_Internalname = sPrefix+"TEXTBLOCKCONTRATODADOSCERTAME_DATAHOMOLOGACAO";
         edtContratoDadosCertame_DataHomologacao_Internalname = sPrefix+"CONTRATODADOSCERTAME_DATAHOMOLOGACAO";
         lblTextblockcontratodadoscertame_dataadjudicacao_Internalname = sPrefix+"TEXTBLOCKCONTRATODADOSCERTAME_DATAADJUDICACAO";
         edtContratoDadosCertame_DataAdjudicacao_Internalname = sPrefix+"CONTRATODADOSCERTAME_DATAADJUDICACAO";
         tblTablemergedcontratodadoscertame_datahomologacao_Internalname = sPrefix+"TABLEMERGEDCONTRATODADOSCERTAME_DATAHOMOLOGACAO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContrato_Ano_Jsonclick = "";
         edtContrato_NumeroAta_Jsonclick = "";
         edtContrato_Numero_Jsonclick = "";
         edtContratada_PessoaCNPJ_Jsonclick = "";
         edtContratada_PessoaNom_Jsonclick = "";
         edtContratoDadosCertame_DataAdjudicacao_Jsonclick = "";
         edtContratoDadosCertame_DataHomologacao_Jsonclick = "";
         edtContratoDadosCertame_Uasg_Jsonclick = "";
         edtContratoDadosCertame_Site_Jsonclick = "";
         edtContratoDadosCertame_Numero_Jsonclick = "";
         edtContratoDadosCertame_Modalidade_Jsonclick = "";
         edtContratoDadosCertame_Data_Jsonclick = "";
         bttBtndelete_Visible = 1;
         bttBtnupdate_Visible = 1;
         edtContratada_PessoaNom_Link = "";
         edtContrato_NumeroAta_Link = "";
         edtContratoDadosCertame_Site_Link = "";
         edtContratoDadosCertame_Site_Linktarget = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E137I2',iparms:[{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E147I2',iparms:[{av:'A314ContratoDadosCertame_Codigo',fld:'CONTRATODADOSCERTAME_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A311ContratoDadosCertame_Data = DateTime.MinValue;
         A307ContratoDadosCertame_Modalidade = "";
         A309ContratoDadosCertame_Site = "";
         A312ContratoDadosCertame_DataHomologacao = DateTime.MinValue;
         A313ContratoDadosCertame_DataAdjudicacao = DateTime.MinValue;
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H007I2_A40Contratada_PessoaCod = new int[1] ;
         H007I2_A314ContratoDadosCertame_Codigo = new int[1] ;
         H007I2_A74Contrato_Codigo = new int[1] ;
         H007I2_A39Contratada_Codigo = new int[1] ;
         H007I2_A313ContratoDadosCertame_DataAdjudicacao = new DateTime[] {DateTime.MinValue} ;
         H007I2_n313ContratoDadosCertame_DataAdjudicacao = new bool[] {false} ;
         H007I2_A312ContratoDadosCertame_DataHomologacao = new DateTime[] {DateTime.MinValue} ;
         H007I2_n312ContratoDadosCertame_DataHomologacao = new bool[] {false} ;
         H007I2_A310ContratoDadosCertame_Uasg = new short[1] ;
         H007I2_n310ContratoDadosCertame_Uasg = new bool[] {false} ;
         H007I2_A309ContratoDadosCertame_Site = new String[] {""} ;
         H007I2_n309ContratoDadosCertame_Site = new bool[] {false} ;
         H007I2_A308ContratoDadosCertame_Numero = new long[1] ;
         H007I2_n308ContratoDadosCertame_Numero = new bool[] {false} ;
         H007I2_A307ContratoDadosCertame_Modalidade = new String[] {""} ;
         H007I2_A311ContratoDadosCertame_Data = new DateTime[] {DateTime.MinValue} ;
         H007I2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         H007I2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         H007I2_A41Contratada_PessoaNom = new String[] {""} ;
         H007I2_n41Contratada_PessoaNom = new bool[] {false} ;
         H007I2_A79Contrato_Ano = new short[1] ;
         H007I2_A78Contrato_NumeroAta = new String[] {""} ;
         H007I2_n78Contrato_NumeroAta = new bool[] {false} ;
         H007I2_A77Contrato_Numero = new String[] {""} ;
         A42Contratada_PessoaCNPJ = "";
         A41Contratada_PessoaNom = "";
         A78Contrato_NumeroAta = "";
         A77Contrato_Numero = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontratodadoscertame_data_Jsonclick = "";
         lblTextblockcontratodadoscertame_modalidade_Jsonclick = "";
         lblTextblockcontratodadoscertame_numero_Jsonclick = "";
         lblTextblockcontratodadoscertame_site_Jsonclick = "";
         lblTextblockcontratodadoscertame_uasg_Jsonclick = "";
         lblTextblockcontratodadoscertame_datahomologacao_Jsonclick = "";
         lblTextblockcontratodadoscertame_dataadjudicacao_Jsonclick = "";
         lblTextblockcontrato_numero_Jsonclick = "";
         lblTextblockcontratada_pessoanom_Jsonclick = "";
         lblTextblockcontratada_pessoacnpj_Jsonclick = "";
         lblTextblockcontrato_numeroata_Jsonclick = "";
         lblTextblockcontrato_ano_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA314ContratoDadosCertame_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratodadoscertamegeneral__default(),
            new Object[][] {
                new Object[] {
               H007I2_A40Contratada_PessoaCod, H007I2_A314ContratoDadosCertame_Codigo, H007I2_A74Contrato_Codigo, H007I2_A39Contratada_Codigo, H007I2_A313ContratoDadosCertame_DataAdjudicacao, H007I2_n313ContratoDadosCertame_DataAdjudicacao, H007I2_A312ContratoDadosCertame_DataHomologacao, H007I2_n312ContratoDadosCertame_DataHomologacao, H007I2_A310ContratoDadosCertame_Uasg, H007I2_n310ContratoDadosCertame_Uasg,
               H007I2_A309ContratoDadosCertame_Site, H007I2_n309ContratoDadosCertame_Site, H007I2_A308ContratoDadosCertame_Numero, H007I2_n308ContratoDadosCertame_Numero, H007I2_A307ContratoDadosCertame_Modalidade, H007I2_A311ContratoDadosCertame_Data, H007I2_A42Contratada_PessoaCNPJ, H007I2_n42Contratada_PessoaCNPJ, H007I2_A41Contratada_PessoaNom, H007I2_n41Contratada_PessoaNom,
               H007I2_A79Contrato_Ano, H007I2_A78Contrato_NumeroAta, H007I2_n78Contrato_NumeroAta, H007I2_A77Contrato_Numero
               }
            }
         );
         AV14Pgmname = "ContratoDadosCertameGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ContratoDadosCertameGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A310ContratoDadosCertame_Uasg ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A79Contrato_Ano ;
      private short nGXWrapped ;
      private int A314ContratoDadosCertame_Codigo ;
      private int wcpOA314ContratoDadosCertame_Codigo ;
      private int A74Contrato_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int AV7ContratoDadosCertame_Codigo ;
      private int idxLst ;
      private long A308ContratoDadosCertame_Numero ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String A78Contrato_NumeroAta ;
      private String A77Contrato_Numero ;
      private String edtContrato_Numero_Internalname ;
      private String edtContrato_NumeroAta_Internalname ;
      private String edtContrato_Ano_Internalname ;
      private String edtContratada_PessoaNom_Internalname ;
      private String edtContratada_PessoaCNPJ_Internalname ;
      private String edtContratoDadosCertame_Data_Internalname ;
      private String edtContratoDadosCertame_Modalidade_Internalname ;
      private String edtContratoDadosCertame_Numero_Internalname ;
      private String edtContratoDadosCertame_Site_Internalname ;
      private String edtContratoDadosCertame_Uasg_Internalname ;
      private String edtContratoDadosCertame_DataHomologacao_Internalname ;
      private String edtContratoDadosCertame_DataAdjudicacao_Internalname ;
      private String hsh ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtContratoDadosCertame_Site_Linktarget ;
      private String edtContratoDadosCertame_Site_Link ;
      private String edtContrato_NumeroAta_Link ;
      private String edtContratada_PessoaNom_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String grpUnnamedgroup1_Internalname ;
      private String lblTextblockcontratodadoscertame_data_Internalname ;
      private String lblTextblockcontratodadoscertame_data_Jsonclick ;
      private String edtContratoDadosCertame_Data_Jsonclick ;
      private String lblTextblockcontratodadoscertame_modalidade_Internalname ;
      private String lblTextblockcontratodadoscertame_modalidade_Jsonclick ;
      private String edtContratoDadosCertame_Modalidade_Jsonclick ;
      private String lblTextblockcontratodadoscertame_numero_Internalname ;
      private String lblTextblockcontratodadoscertame_numero_Jsonclick ;
      private String edtContratoDadosCertame_Numero_Jsonclick ;
      private String lblTextblockcontratodadoscertame_site_Internalname ;
      private String lblTextblockcontratodadoscertame_site_Jsonclick ;
      private String edtContratoDadosCertame_Site_Jsonclick ;
      private String lblTextblockcontratodadoscertame_uasg_Internalname ;
      private String lblTextblockcontratodadoscertame_uasg_Jsonclick ;
      private String edtContratoDadosCertame_Uasg_Jsonclick ;
      private String lblTextblockcontratodadoscertame_datahomologacao_Internalname ;
      private String lblTextblockcontratodadoscertame_datahomologacao_Jsonclick ;
      private String tblTablemergedcontratodadoscertame_datahomologacao_Internalname ;
      private String edtContratoDadosCertame_DataHomologacao_Jsonclick ;
      private String lblTextblockcontratodadoscertame_dataadjudicacao_Internalname ;
      private String lblTextblockcontratodadoscertame_dataadjudicacao_Jsonclick ;
      private String edtContratoDadosCertame_DataAdjudicacao_Jsonclick ;
      private String tblContrato_Internalname ;
      private String lblTextblockcontrato_numero_Internalname ;
      private String lblTextblockcontrato_numero_Jsonclick ;
      private String lblTextblockcontratada_pessoanom_Internalname ;
      private String lblTextblockcontratada_pessoanom_Jsonclick ;
      private String tblTablemergedcontratada_pessoanom_Internalname ;
      private String edtContratada_PessoaNom_Jsonclick ;
      private String lblTextblockcontratada_pessoacnpj_Internalname ;
      private String lblTextblockcontratada_pessoacnpj_Jsonclick ;
      private String edtContratada_PessoaCNPJ_Jsonclick ;
      private String tblTablemergedcontrato_numero_Internalname ;
      private String edtContrato_Numero_Jsonclick ;
      private String lblTextblockcontrato_numeroata_Internalname ;
      private String lblTextblockcontrato_numeroata_Jsonclick ;
      private String edtContrato_NumeroAta_Jsonclick ;
      private String lblTextblockcontrato_ano_Internalname ;
      private String lblTextblockcontrato_ano_Jsonclick ;
      private String edtContrato_Ano_Jsonclick ;
      private String sCtrlA314ContratoDadosCertame_Codigo ;
      private DateTime A311ContratoDadosCertame_Data ;
      private DateTime A312ContratoDadosCertame_DataHomologacao ;
      private DateTime A313ContratoDadosCertame_DataAdjudicacao ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n313ContratoDadosCertame_DataAdjudicacao ;
      private bool n312ContratoDadosCertame_DataHomologacao ;
      private bool n310ContratoDadosCertame_Uasg ;
      private bool n309ContratoDadosCertame_Site ;
      private bool n308ContratoDadosCertame_Numero ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool n78Contrato_NumeroAta ;
      private bool returnInSub ;
      private String A307ContratoDadosCertame_Modalidade ;
      private String A309ContratoDadosCertame_Site ;
      private String A42Contratada_PessoaCNPJ ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H007I2_A40Contratada_PessoaCod ;
      private int[] H007I2_A314ContratoDadosCertame_Codigo ;
      private int[] H007I2_A74Contrato_Codigo ;
      private int[] H007I2_A39Contratada_Codigo ;
      private DateTime[] H007I2_A313ContratoDadosCertame_DataAdjudicacao ;
      private bool[] H007I2_n313ContratoDadosCertame_DataAdjudicacao ;
      private DateTime[] H007I2_A312ContratoDadosCertame_DataHomologacao ;
      private bool[] H007I2_n312ContratoDadosCertame_DataHomologacao ;
      private short[] H007I2_A310ContratoDadosCertame_Uasg ;
      private bool[] H007I2_n310ContratoDadosCertame_Uasg ;
      private String[] H007I2_A309ContratoDadosCertame_Site ;
      private bool[] H007I2_n309ContratoDadosCertame_Site ;
      private long[] H007I2_A308ContratoDadosCertame_Numero ;
      private bool[] H007I2_n308ContratoDadosCertame_Numero ;
      private String[] H007I2_A307ContratoDadosCertame_Modalidade ;
      private DateTime[] H007I2_A311ContratoDadosCertame_Data ;
      private String[] H007I2_A42Contratada_PessoaCNPJ ;
      private bool[] H007I2_n42Contratada_PessoaCNPJ ;
      private String[] H007I2_A41Contratada_PessoaNom ;
      private bool[] H007I2_n41Contratada_PessoaNom ;
      private short[] H007I2_A79Contrato_Ano ;
      private String[] H007I2_A78Contrato_NumeroAta ;
      private bool[] H007I2_n78Contrato_NumeroAta ;
      private String[] H007I2_A77Contrato_Numero ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contratodadoscertamegeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH007I2 ;
          prmH007I2 = new Object[] {
          new Object[] {"@ContratoDadosCertame_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H007I2", "SELECT T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoDadosCertame_Codigo], T1.[Contrato_Codigo], T2.[Contratada_Codigo], T1.[ContratoDadosCertame_DataAdjudicacao], T1.[ContratoDadosCertame_DataHomologacao], T1.[ContratoDadosCertame_Uasg], T1.[ContratoDadosCertame_Site], T1.[ContratoDadosCertame_Numero], T1.[ContratoDadosCertame_Modalidade], T1.[ContratoDadosCertame_Data], T4.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contrato_Ano], T2.[Contrato_NumeroAta], T2.[Contrato_Numero] FROM ((([ContratoDadosCertame] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) WHERE T1.[ContratoDadosCertame_Codigo] = @ContratoDadosCertame_Codigo ORDER BY T1.[ContratoDadosCertame_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007I2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((short[]) buf[8])[0] = rslt.getShort(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((long[]) buf[12])[0] = rslt.getLong(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getVarchar(10) ;
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(11) ;
                ((String[]) buf[16])[0] = rslt.getVarchar(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((String[]) buf[18])[0] = rslt.getString(13, 100) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((short[]) buf[20])[0] = rslt.getShort(14) ;
                ((String[]) buf[21])[0] = rslt.getString(15, 10) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((String[]) buf[23])[0] = rslt.getString(16, 20) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
