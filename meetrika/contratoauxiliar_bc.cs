/*
               File: ContratoAuxiliar_BC
        Description: Auxiliar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:30:0.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoauxiliar_bc : GXHttpHandler, IGxSilentTrn
   {
      public contratoauxiliar_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratoauxiliar_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4J200( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4J200( ) ;
         standaloneModal( ) ;
         AddRow4J200( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1824ContratoAuxiliar_ContratoCod = A1824ContratoAuxiliar_ContratoCod;
               Z1825ContratoAuxiliar_UsuarioCod = A1825ContratoAuxiliar_UsuarioCod;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4J0( )
      {
         BeforeValidate4J200( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4J200( ) ;
            }
            else
            {
               CheckExtendedTable4J200( ) ;
               if ( AnyError == 0 )
               {
                  ZM4J200( 2) ;
                  ZM4J200( 3) ;
               }
               CloseExtendedTableCursors4J200( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM4J200( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -1 )
         {
            Z1824ContratoAuxiliar_ContratoCod = A1824ContratoAuxiliar_ContratoCod;
            Z1825ContratoAuxiliar_UsuarioCod = A1825ContratoAuxiliar_UsuarioCod;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load4J200( )
      {
         /* Using cursor BC004J6 */
         pr_default.execute(4, new Object[] {A1824ContratoAuxiliar_ContratoCod, A1825ContratoAuxiliar_UsuarioCod});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound200 = 1;
            ZM4J200( -1) ;
         }
         pr_default.close(4);
         OnLoadActions4J200( ) ;
      }

      protected void OnLoadActions4J200( )
      {
      }

      protected void CheckExtendedTable4J200( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004J4 */
         pr_default.execute(2, new Object[] {A1824ContratoAuxiliar_ContratoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Auxiliar_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOAUXILIAR_CONTRATOCOD");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC004J5 */
         pr_default.execute(3, new Object[] {A1825ContratoAuxiliar_UsuarioCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Auxiliar_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATOAUXILIAR_USUARIOCOD");
            AnyError = 1;
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4J200( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4J200( )
      {
         /* Using cursor BC004J7 */
         pr_default.execute(5, new Object[] {A1824ContratoAuxiliar_ContratoCod, A1825ContratoAuxiliar_UsuarioCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound200 = 1;
         }
         else
         {
            RcdFound200 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004J3 */
         pr_default.execute(1, new Object[] {A1824ContratoAuxiliar_ContratoCod, A1825ContratoAuxiliar_UsuarioCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4J200( 1) ;
            RcdFound200 = 1;
            A1824ContratoAuxiliar_ContratoCod = BC004J3_A1824ContratoAuxiliar_ContratoCod[0];
            A1825ContratoAuxiliar_UsuarioCod = BC004J3_A1825ContratoAuxiliar_UsuarioCod[0];
            Z1824ContratoAuxiliar_ContratoCod = A1824ContratoAuxiliar_ContratoCod;
            Z1825ContratoAuxiliar_UsuarioCod = A1825ContratoAuxiliar_UsuarioCod;
            sMode200 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4J200( ) ;
            if ( AnyError == 1 )
            {
               RcdFound200 = 0;
               InitializeNonKey4J200( ) ;
            }
            Gx_mode = sMode200;
         }
         else
         {
            RcdFound200 = 0;
            InitializeNonKey4J200( ) ;
            sMode200 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode200;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4J200( ) ;
         if ( RcdFound200 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4J0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4J200( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004J2 */
            pr_default.execute(0, new Object[] {A1824ContratoAuxiliar_ContratoCod, A1825ContratoAuxiliar_UsuarioCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoAuxiliar"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoAuxiliar"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4J200( )
      {
         BeforeValidate4J200( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4J200( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4J200( 0) ;
            CheckOptimisticConcurrency4J200( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4J200( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4J200( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004J8 */
                     pr_default.execute(6, new Object[] {A1824ContratoAuxiliar_ContratoCod, A1825ContratoAuxiliar_UsuarioCod});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoAuxiliar") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4J200( ) ;
            }
            EndLevel4J200( ) ;
         }
         CloseExtendedTableCursors4J200( ) ;
      }

      protected void Update4J200( )
      {
         BeforeValidate4J200( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4J200( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4J200( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4J200( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4J200( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ContratoAuxiliar] */
                     DeferredUpdate4J200( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4J200( ) ;
         }
         CloseExtendedTableCursors4J200( ) ;
      }

      protected void DeferredUpdate4J200( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4J200( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4J200( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4J200( ) ;
            AfterConfirm4J200( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4J200( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004J9 */
                  pr_default.execute(7, new Object[] {A1824ContratoAuxiliar_ContratoCod, A1825ContratoAuxiliar_UsuarioCod});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoAuxiliar") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode200 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4J200( ) ;
         Gx_mode = sMode200;
      }

      protected void OnDeleteControls4J200( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel4J200( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4J200( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4J200( )
      {
         /* Using cursor BC004J10 */
         pr_default.execute(8, new Object[] {A1824ContratoAuxiliar_ContratoCod, A1825ContratoAuxiliar_UsuarioCod});
         RcdFound200 = 0;
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound200 = 1;
            A1824ContratoAuxiliar_ContratoCod = BC004J10_A1824ContratoAuxiliar_ContratoCod[0];
            A1825ContratoAuxiliar_UsuarioCod = BC004J10_A1825ContratoAuxiliar_UsuarioCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4J200( )
      {
         /* Scan next routine */
         pr_default.readNext(8);
         RcdFound200 = 0;
         ScanKeyLoad4J200( ) ;
      }

      protected void ScanKeyLoad4J200( )
      {
         sMode200 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound200 = 1;
            A1824ContratoAuxiliar_ContratoCod = BC004J10_A1824ContratoAuxiliar_ContratoCod[0];
            A1825ContratoAuxiliar_UsuarioCod = BC004J10_A1825ContratoAuxiliar_UsuarioCod[0];
         }
         Gx_mode = sMode200;
      }

      protected void ScanKeyEnd4J200( )
      {
         pr_default.close(8);
      }

      protected void AfterConfirm4J200( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4J200( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4J200( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4J200( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4J200( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4J200( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4J200( )
      {
      }

      protected void AddRow4J200( )
      {
         VarsToRow200( bcContratoAuxiliar) ;
      }

      protected void ReadRow4J200( )
      {
         RowToVars200( bcContratoAuxiliar, 1) ;
      }

      protected void InitializeNonKey4J200( )
      {
      }

      protected void InitAll4J200( )
      {
         A1824ContratoAuxiliar_ContratoCod = 0;
         A1825ContratoAuxiliar_UsuarioCod = 0;
         InitializeNonKey4J200( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow200( SdtContratoAuxiliar obj200 )
      {
         obj200.gxTpr_Mode = Gx_mode;
         obj200.gxTpr_Contratoauxiliar_contratocod = A1824ContratoAuxiliar_ContratoCod;
         obj200.gxTpr_Contratoauxiliar_usuariocod = A1825ContratoAuxiliar_UsuarioCod;
         obj200.gxTpr_Contratoauxiliar_contratocod_Z = Z1824ContratoAuxiliar_ContratoCod;
         obj200.gxTpr_Contratoauxiliar_usuariocod_Z = Z1825ContratoAuxiliar_UsuarioCod;
         obj200.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow200( SdtContratoAuxiliar obj200 )
      {
         obj200.gxTpr_Contratoauxiliar_contratocod = A1824ContratoAuxiliar_ContratoCod;
         obj200.gxTpr_Contratoauxiliar_usuariocod = A1825ContratoAuxiliar_UsuarioCod;
         return  ;
      }

      public void RowToVars200( SdtContratoAuxiliar obj200 ,
                                int forceLoad )
      {
         Gx_mode = obj200.gxTpr_Mode;
         A1824ContratoAuxiliar_ContratoCod = obj200.gxTpr_Contratoauxiliar_contratocod;
         A1825ContratoAuxiliar_UsuarioCod = obj200.gxTpr_Contratoauxiliar_usuariocod;
         Z1824ContratoAuxiliar_ContratoCod = obj200.gxTpr_Contratoauxiliar_contratocod_Z;
         Z1825ContratoAuxiliar_UsuarioCod = obj200.gxTpr_Contratoauxiliar_usuariocod_Z;
         Gx_mode = obj200.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1824ContratoAuxiliar_ContratoCod = (int)getParm(obj,0);
         A1825ContratoAuxiliar_UsuarioCod = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4J200( ) ;
         ScanKeyStart4J200( ) ;
         if ( RcdFound200 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004J11 */
            pr_default.execute(9, new Object[] {A1824ContratoAuxiliar_ContratoCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Auxiliar_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOAUXILIAR_CONTRATOCOD");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC004J12 */
            pr_default.execute(10, new Object[] {A1825ContratoAuxiliar_UsuarioCod});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Auxiliar_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATOAUXILIAR_USUARIOCOD");
               AnyError = 1;
            }
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z1824ContratoAuxiliar_ContratoCod = A1824ContratoAuxiliar_ContratoCod;
            Z1825ContratoAuxiliar_UsuarioCod = A1825ContratoAuxiliar_UsuarioCod;
         }
         ZM4J200( -1) ;
         OnLoadActions4J200( ) ;
         AddRow4J200( ) ;
         ScanKeyEnd4J200( ) ;
         if ( RcdFound200 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars200( bcContratoAuxiliar, 0) ;
         ScanKeyStart4J200( ) ;
         if ( RcdFound200 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004J11 */
            pr_default.execute(9, new Object[] {A1824ContratoAuxiliar_ContratoCod});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Auxiliar_Contrato'.", "ForeignKeyNotFound", 1, "CONTRATOAUXILIAR_CONTRATOCOD");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC004J12 */
            pr_default.execute(10, new Object[] {A1825ContratoAuxiliar_UsuarioCod});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Contrato Auxiliar_Usuario'.", "ForeignKeyNotFound", 1, "CONTRATOAUXILIAR_USUARIOCOD");
               AnyError = 1;
            }
            pr_default.close(10);
         }
         else
         {
            Gx_mode = "UPD";
            Z1824ContratoAuxiliar_ContratoCod = A1824ContratoAuxiliar_ContratoCod;
            Z1825ContratoAuxiliar_UsuarioCod = A1825ContratoAuxiliar_UsuarioCod;
         }
         ZM4J200( -1) ;
         OnLoadActions4J200( ) ;
         AddRow4J200( ) ;
         ScanKeyEnd4J200( ) ;
         if ( RcdFound200 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars200( bcContratoAuxiliar, 0) ;
         nKeyPressed = 1;
         GetKey4J200( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4J200( ) ;
         }
         else
         {
            if ( RcdFound200 == 1 )
            {
               if ( ( A1824ContratoAuxiliar_ContratoCod != Z1824ContratoAuxiliar_ContratoCod ) || ( A1825ContratoAuxiliar_UsuarioCod != Z1825ContratoAuxiliar_UsuarioCod ) )
               {
                  A1824ContratoAuxiliar_ContratoCod = Z1824ContratoAuxiliar_ContratoCod;
                  A1825ContratoAuxiliar_UsuarioCod = Z1825ContratoAuxiliar_UsuarioCod;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4J200( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A1824ContratoAuxiliar_ContratoCod != Z1824ContratoAuxiliar_ContratoCod ) || ( A1825ContratoAuxiliar_UsuarioCod != Z1825ContratoAuxiliar_UsuarioCod ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4J200( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4J200( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow200( bcContratoAuxiliar) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars200( bcContratoAuxiliar, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4J200( ) ;
         if ( RcdFound200 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A1824ContratoAuxiliar_ContratoCod != Z1824ContratoAuxiliar_ContratoCod ) || ( A1825ContratoAuxiliar_UsuarioCod != Z1825ContratoAuxiliar_UsuarioCod ) )
            {
               A1824ContratoAuxiliar_ContratoCod = Z1824ContratoAuxiliar_ContratoCod;
               A1825ContratoAuxiliar_UsuarioCod = Z1825ContratoAuxiliar_UsuarioCod;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A1824ContratoAuxiliar_ContratoCod != Z1824ContratoAuxiliar_ContratoCod ) || ( A1825ContratoAuxiliar_UsuarioCod != Z1825ContratoAuxiliar_UsuarioCod ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
         context.RollbackDataStores( "ContratoAuxiliar_BC");
         VarsToRow200( bcContratoAuxiliar) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcContratoAuxiliar.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcContratoAuxiliar.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcContratoAuxiliar )
         {
            bcContratoAuxiliar = (SdtContratoAuxiliar)(sdt);
            if ( StringUtil.StrCmp(bcContratoAuxiliar.gxTpr_Mode, "") == 0 )
            {
               bcContratoAuxiliar.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow200( bcContratoAuxiliar) ;
            }
            else
            {
               RowToVars200( bcContratoAuxiliar, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcContratoAuxiliar.gxTpr_Mode, "") == 0 )
            {
               bcContratoAuxiliar.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars200( bcContratoAuxiliar, 1) ;
         return  ;
      }

      public SdtContratoAuxiliar ContratoAuxiliar_BC
      {
         get {
            return bcContratoAuxiliar ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         BC004J6_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         BC004J6_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         BC004J4_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         BC004J5_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         BC004J7_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         BC004J7_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         BC004J3_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         BC004J3_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         sMode200 = "";
         BC004J2_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         BC004J2_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         BC004J10_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         BC004J10_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC004J11_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         BC004J12_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoauxiliar_bc__default(),
            new Object[][] {
                new Object[] {
               BC004J2_A1824ContratoAuxiliar_ContratoCod, BC004J2_A1825ContratoAuxiliar_UsuarioCod
               }
               , new Object[] {
               BC004J3_A1824ContratoAuxiliar_ContratoCod, BC004J3_A1825ContratoAuxiliar_UsuarioCod
               }
               , new Object[] {
               BC004J4_A1824ContratoAuxiliar_ContratoCod
               }
               , new Object[] {
               BC004J5_A1825ContratoAuxiliar_UsuarioCod
               }
               , new Object[] {
               BC004J6_A1824ContratoAuxiliar_ContratoCod, BC004J6_A1825ContratoAuxiliar_UsuarioCod
               }
               , new Object[] {
               BC004J7_A1824ContratoAuxiliar_ContratoCod, BC004J7_A1825ContratoAuxiliar_UsuarioCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004J10_A1824ContratoAuxiliar_ContratoCod, BC004J10_A1825ContratoAuxiliar_UsuarioCod
               }
               , new Object[] {
               BC004J11_A1824ContratoAuxiliar_ContratoCod
               }
               , new Object[] {
               BC004J12_A1825ContratoAuxiliar_UsuarioCod
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound200 ;
      private int trnEnded ;
      private int Z1824ContratoAuxiliar_ContratoCod ;
      private int A1824ContratoAuxiliar_ContratoCod ;
      private int Z1825ContratoAuxiliar_UsuarioCod ;
      private int A1825ContratoAuxiliar_UsuarioCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode200 ;
      private SdtContratoAuxiliar bcContratoAuxiliar ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC004J6_A1824ContratoAuxiliar_ContratoCod ;
      private int[] BC004J6_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] BC004J4_A1824ContratoAuxiliar_ContratoCod ;
      private int[] BC004J5_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] BC004J7_A1824ContratoAuxiliar_ContratoCod ;
      private int[] BC004J7_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] BC004J3_A1824ContratoAuxiliar_ContratoCod ;
      private int[] BC004J3_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] BC004J2_A1824ContratoAuxiliar_ContratoCod ;
      private int[] BC004J2_A1825ContratoAuxiliar_UsuarioCod ;
      private int[] BC004J10_A1824ContratoAuxiliar_ContratoCod ;
      private int[] BC004J10_A1825ContratoAuxiliar_UsuarioCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC004J11_A1824ContratoAuxiliar_ContratoCod ;
      private int[] BC004J12_A1825ContratoAuxiliar_UsuarioCod ;
   }

   public class contratoauxiliar_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004J6 ;
          prmBC004J6 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004J4 ;
          prmBC004J4 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004J5 ;
          prmBC004J5 = new Object[] {
          new Object[] {"@ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004J7 ;
          prmBC004J7 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004J3 ;
          prmBC004J3 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004J2 ;
          prmBC004J2 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004J8 ;
          prmBC004J8 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004J9 ;
          prmBC004J9 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004J10 ;
          prmBC004J10 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004J11 ;
          prmBC004J11 = new Object[] {
          new Object[] {"@ContratoAuxiliar_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004J12 ;
          prmBC004J12 = new Object[] {
          new Object[] {"@ContratoAuxiliar_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004J2", "SELECT [ContratoAuxiliar_ContratoCod] AS ContratoAuxiliar_ContratoCod, [ContratoAuxiliar_UsuarioCod] AS ContratoAuxiliar_UsuarioCod FROM [ContratoAuxiliar] WITH (UPDLOCK) WHERE [ContratoAuxiliar_ContratoCod] = @ContratoAuxiliar_ContratoCod AND [ContratoAuxiliar_UsuarioCod] = @ContratoAuxiliar_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004J2,1,0,true,false )
             ,new CursorDef("BC004J3", "SELECT [ContratoAuxiliar_ContratoCod] AS ContratoAuxiliar_ContratoCod, [ContratoAuxiliar_UsuarioCod] AS ContratoAuxiliar_UsuarioCod FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_ContratoCod] = @ContratoAuxiliar_ContratoCod AND [ContratoAuxiliar_UsuarioCod] = @ContratoAuxiliar_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004J3,1,0,true,false )
             ,new CursorDef("BC004J4", "SELECT [Contrato_Codigo] AS ContratoAuxiliar_ContratoCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoAuxiliar_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004J4,1,0,true,false )
             ,new CursorDef("BC004J5", "SELECT [Usuario_Codigo] AS ContratoAuxiliar_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratoAuxiliar_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004J5,1,0,true,false )
             ,new CursorDef("BC004J6", "SELECT TM1.[ContratoAuxiliar_ContratoCod] AS ContratoAuxiliar_ContratoCod, TM1.[ContratoAuxiliar_UsuarioCod] AS ContratoAuxiliar_UsuarioCod FROM [ContratoAuxiliar] TM1 WITH (NOLOCK) WHERE TM1.[ContratoAuxiliar_ContratoCod] = @ContratoAuxiliar_ContratoCod and TM1.[ContratoAuxiliar_UsuarioCod] = @ContratoAuxiliar_UsuarioCod ORDER BY TM1.[ContratoAuxiliar_ContratoCod], TM1.[ContratoAuxiliar_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004J6,100,0,true,false )
             ,new CursorDef("BC004J7", "SELECT [ContratoAuxiliar_ContratoCod] AS ContratoAuxiliar_ContratoCod, [ContratoAuxiliar_UsuarioCod] AS ContratoAuxiliar_UsuarioCod FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_ContratoCod] = @ContratoAuxiliar_ContratoCod AND [ContratoAuxiliar_UsuarioCod] = @ContratoAuxiliar_UsuarioCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004J7,1,0,true,false )
             ,new CursorDef("BC004J8", "INSERT INTO [ContratoAuxiliar]([ContratoAuxiliar_ContratoCod], [ContratoAuxiliar_UsuarioCod]) VALUES(@ContratoAuxiliar_ContratoCod, @ContratoAuxiliar_UsuarioCod)", GxErrorMask.GX_NOMASK,prmBC004J8)
             ,new CursorDef("BC004J9", "DELETE FROM [ContratoAuxiliar]  WHERE [ContratoAuxiliar_ContratoCod] = @ContratoAuxiliar_ContratoCod AND [ContratoAuxiliar_UsuarioCod] = @ContratoAuxiliar_UsuarioCod", GxErrorMask.GX_NOMASK,prmBC004J9)
             ,new CursorDef("BC004J10", "SELECT TM1.[ContratoAuxiliar_ContratoCod] AS ContratoAuxiliar_ContratoCod, TM1.[ContratoAuxiliar_UsuarioCod] AS ContratoAuxiliar_UsuarioCod FROM [ContratoAuxiliar] TM1 WITH (NOLOCK) WHERE TM1.[ContratoAuxiliar_ContratoCod] = @ContratoAuxiliar_ContratoCod and TM1.[ContratoAuxiliar_UsuarioCod] = @ContratoAuxiliar_UsuarioCod ORDER BY TM1.[ContratoAuxiliar_ContratoCod], TM1.[ContratoAuxiliar_UsuarioCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004J10,100,0,true,false )
             ,new CursorDef("BC004J11", "SELECT [Contrato_Codigo] AS ContratoAuxiliar_ContratoCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoAuxiliar_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004J11,1,0,true,false )
             ,new CursorDef("BC004J12", "SELECT [Usuario_Codigo] AS ContratoAuxiliar_UsuarioCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContratoAuxiliar_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004J12,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
