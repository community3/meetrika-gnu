/*
               File: PRC_PrepostoGestor
        Description: PRC_Preposto Gestor
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:5.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_prepostogestor : GXProcedure
   {
      public prc_prepostogestor( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_prepostogestor( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoGestor_ContratoCod ,
                           int aP1_NewPreposto_Codigo ,
                           int aP2_OldPreposto_Codigo )
      {
         this.A1078ContratoGestor_ContratoCod = aP0_ContratoGestor_ContratoCod;
         this.AV8NewPreposto_Codigo = aP1_NewPreposto_Codigo;
         this.AV9OldPreposto_Codigo = aP2_OldPreposto_Codigo;
         initialize();
         executePrivate();
         aP0_ContratoGestor_ContratoCod=this.A1078ContratoGestor_ContratoCod;
      }

      public void executeSubmit( ref int aP0_ContratoGestor_ContratoCod ,
                                 int aP1_NewPreposto_Codigo ,
                                 int aP2_OldPreposto_Codigo )
      {
         prc_prepostogestor objprc_prepostogestor;
         objprc_prepostogestor = new prc_prepostogestor();
         objprc_prepostogestor.A1078ContratoGestor_ContratoCod = aP0_ContratoGestor_ContratoCod;
         objprc_prepostogestor.AV8NewPreposto_Codigo = aP1_NewPreposto_Codigo;
         objprc_prepostogestor.AV9OldPreposto_Codigo = aP2_OldPreposto_Codigo;
         objprc_prepostogestor.context.SetSubmitInitialConfig(context);
         objprc_prepostogestor.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_prepostogestor);
         aP0_ContratoGestor_ContratoCod=this.A1078ContratoGestor_ContratoCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_prepostogestor)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV8NewPreposto_Codigo > 0 )
         {
            AV12GXLvl3 = 0;
            /* Using cursor P00VM2 */
            pr_default.execute(0, new Object[] {A1078ContratoGestor_ContratoCod, AV8NewPreposto_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1079ContratoGestor_UsuarioCod = P00VM2_A1079ContratoGestor_UsuarioCod[0];
               AV12GXLvl3 = 1;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV12GXLvl3 == 0 )
            {
               /*
                  INSERT RECORD ON TABLE ContratoGestor

               */
               A1079ContratoGestor_UsuarioCod = AV8NewPreposto_Codigo;
               /* Using cursor P00VM3 */
               pr_default.execute(1, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
               pr_default.close(1);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
               if ( (pr_default.getStatus(1) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
            }
         }
         if ( AV9OldPreposto_Codigo > 0 )
         {
            /* Using cursor P00VM4 */
            pr_default.execute(2, new Object[] {A1078ContratoGestor_ContratoCod, AV9OldPreposto_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1079ContratoGestor_UsuarioCod = P00VM4_A1079ContratoGestor_UsuarioCod[0];
               /* Using cursor P00VM5 */
               pr_default.execute(3, new Object[] {A1078ContratoGestor_ContratoCod, A1079ContratoGestor_UsuarioCod});
               pr_default.close(3);
               dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_PrepostoGestor");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00VM2_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00VM2_A1079ContratoGestor_UsuarioCod = new int[1] ;
         Gx_emsg = "";
         P00VM4_A1078ContratoGestor_ContratoCod = new int[1] ;
         P00VM4_A1079ContratoGestor_UsuarioCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_prepostogestor__default(),
            new Object[][] {
                new Object[] {
               P00VM2_A1078ContratoGestor_ContratoCod, P00VM2_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               }
               , new Object[] {
               P00VM4_A1078ContratoGestor_ContratoCod, P00VM4_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12GXLvl3 ;
      private int A1078ContratoGestor_ContratoCod ;
      private int AV8NewPreposto_Codigo ;
      private int AV9OldPreposto_Codigo ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int GX_INS129 ;
      private String scmdbuf ;
      private String Gx_emsg ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoGestor_ContratoCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00VM2_A1078ContratoGestor_ContratoCod ;
      private int[] P00VM2_A1079ContratoGestor_UsuarioCod ;
      private int[] P00VM4_A1078ContratoGestor_ContratoCod ;
      private int[] P00VM4_A1079ContratoGestor_UsuarioCod ;
   }

   public class prc_prepostogestor__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VM2 ;
          prmP00VM2 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV8NewPreposto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VM3 ;
          prmP00VM3 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VM4 ;
          prmP00VM4 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9OldPreposto_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VM5 ;
          prmP00VM5 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VM2", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod and [ContratoGestor_UsuarioCod] = @AV8NewPreposto_Codigo ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VM2,1,0,false,true )
             ,new CursorDef("P00VM3", "INSERT INTO [ContratoGestor]([ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod], [ContratoGestor_Tipo]) VALUES(@ContratoGestor_ContratoCod, @ContratoGestor_UsuarioCod, convert(int, 0))", GxErrorMask.GX_NOMASK,prmP00VM3)
             ,new CursorDef("P00VM4", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (UPDLOCK) WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod and [ContratoGestor_UsuarioCod] = @AV9OldPreposto_Codigo ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VM4,1,0,true,true )
             ,new CursorDef("P00VM5", "DELETE FROM [ContratoGestor]  WHERE [ContratoGestor_ContratoCod] = @ContratoGestor_ContratoCod AND [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00VM5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
