/*
               File: PRC_RdmCompativel
        Description: Redmine Compativel com estrutura esperada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:17.12
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_rdmcompativel : GXProcedure
   {
      public prc_rdmcompativel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_rdmcompativel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_XML ,
                           out String aP1_txt ,
                           out bool aP2_Compativel )
      {
         this.AV9XML = aP0_XML;
         this.AV12txt = "" ;
         this.AV10Compativel = false ;
         initialize();
         executePrivate();
         aP1_txt=this.AV12txt;
         aP2_Compativel=this.AV10Compativel;
      }

      public bool executeUdp( String aP0_XML ,
                              out String aP1_txt )
      {
         this.AV9XML = aP0_XML;
         this.AV12txt = "" ;
         this.AV10Compativel = false ;
         initialize();
         executePrivate();
         aP1_txt=this.AV12txt;
         aP2_Compativel=this.AV10Compativel;
         return AV10Compativel ;
      }

      public void executeSubmit( String aP0_XML ,
                                 out String aP1_txt ,
                                 out bool aP2_Compativel )
      {
         prc_rdmcompativel objprc_rdmcompativel;
         objprc_rdmcompativel = new prc_rdmcompativel();
         objprc_rdmcompativel.AV9XML = aP0_XML;
         objprc_rdmcompativel.AV12txt = "" ;
         objprc_rdmcompativel.AV10Compativel = false ;
         objprc_rdmcompativel.context.SetSubmitInitialConfig(context);
         objprc_rdmcompativel.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_rdmcompativel);
         aP1_txt=this.AV12txt;
         aP2_Compativel=this.AV10Compativel;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_rdmcompativel)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10Compativel = true;
         AV11XMLReader.OpenFromString(AV9XML);
         AV13Elements.Add("issues", 0);
         AV13Elements.Add("issue", 0);
         AV13Elements.Add("id", 0);
         AV13Elements.Add("project", 0);
         AV13Elements.Add("tracker", 0);
         AV13Elements.Add("status", 0);
         AV13Elements.Add("priority", 0);
         AV13Elements.Add("author", 0);
         AV13Elements.Add("assigned_to", 0);
         AV13Elements.Add("subject", 0);
         AV13Elements.Add("description", 0);
         AV13Elements.Add("start_date", 0);
         AV13Elements.Add("due_date", 0);
         AV13Elements.Add("done_ratio", 0);
         AV13Elements.Add("estimated_hours", 0);
         AV13Elements.Add("custom_fields", 0);
         AV13Elements.Add("created_on", 0);
         AV13Elements.Add("updated_on", 0);
         AV14i = 1;
         while ( AV14i <= AV13Elements.Count )
         {
            AV15r = AV11XMLReader.ReadType(1, AV13Elements.GetString(AV14i));
            if ( AV15r == 0 )
            {
               AV12txt = AV13Elements.GetString(AV14i);
               AV10Compativel = false;
               this.cleanup();
               if (true) return;
            }
            if ( StringUtil.StrCmp(AV11XMLReader.Name, "custom fields") == 0 )
            {
               AV11XMLReader.Read();
               while ( StringUtil.StrCmp(AV11XMLReader.Name, "custom fields") != 0 )
               {
                  AV11XMLReader.Read();
               }
            }
            AV14i = (short)(AV14i+1);
         }
         AV15r = AV11XMLReader.ReadType(2, "issue");
         if ( AV15r == 0 )
         {
            AV12txt = "/issue";
            AV10Compativel = false;
            this.cleanup();
            if (true) return;
         }
         AV15r = AV11XMLReader.ReadType(2, "issues");
         if ( AV15r == 0 )
         {
            AV12txt = "/issues";
            AV10Compativel = false;
            this.cleanup();
            if (true) return;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV11XMLReader = new GXXMLReader(context.GetPhysicalPath());
         AV13Elements = new GxSimpleCollection();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14i ;
      private short AV15r ;
      private String AV12txt ;
      private bool AV10Compativel ;
      private String AV9XML ;
      private String aP1_txt ;
      private bool aP2_Compativel ;
      private GXXMLReader AV11XMLReader ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13Elements ;
   }

}
