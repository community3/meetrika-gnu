/*
               File: GetWWContratoServicosTelasFilterData
        Description: Get WWContrato Servicos Telas Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:16:17.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwcontratoservicostelasfilterdata : GXProcedure
   {
      public getwwcontratoservicostelasfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwcontratoservicostelasfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwcontratoservicostelasfilterdata objgetwwcontratoservicostelasfilterdata;
         objgetwwcontratoservicostelasfilterdata = new getwwcontratoservicostelasfilterdata();
         objgetwwcontratoservicostelasfilterdata.AV24DDOName = aP0_DDOName;
         objgetwwcontratoservicostelasfilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetwwcontratoservicostelasfilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetwwcontratoservicostelasfilterdata.AV28OptionsJson = "" ;
         objgetwwcontratoservicostelasfilterdata.AV31OptionsDescJson = "" ;
         objgetwwcontratoservicostelasfilterdata.AV33OptionIndexesJson = "" ;
         objgetwwcontratoservicostelasfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwcontratoservicostelasfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwcontratoservicostelasfilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwcontratoservicostelasfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATOSERVICOSTELAS_SERVICOSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSTELAS_SERVICOSIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATOSERVICOSTELAS_TELA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSTELAS_TELAOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATOSERVICOSTELAS_LINK") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSTELAS_LINKOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_CONTRATOSERVICOSTELAS_PARMS") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOSERVICOSTELAS_PARMSOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("WWContratoServicosTelasGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWContratoServicosTelasGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("WWContratoServicosTelasGridState"), "");
         }
         AV53GXV1 = 1;
         while ( AV53GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV53GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV10TFContratada_PessoaNom = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV11TFContratada_PessoaNom_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_SERVICOSIGLA") == 0 )
            {
               AV12TFContratoServicosTelas_ServicoSigla = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_SERVICOSIGLA_SEL") == 0 )
            {
               AV13TFContratoServicosTelas_ServicoSigla_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_TELA") == 0 )
            {
               AV14TFContratoServicosTelas_Tela = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_TELA_SEL") == 0 )
            {
               AV15TFContratoServicosTelas_Tela_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_LINK") == 0 )
            {
               AV16TFContratoServicosTelas_Link = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_LINK_SEL") == 0 )
            {
               AV17TFContratoServicosTelas_Link_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_PARMS") == 0 )
            {
               AV18TFContratoServicosTelas_Parms = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_PARMS_SEL") == 0 )
            {
               AV19TFContratoServicosTelas_Parms_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSTELAS_STATUS_SEL") == 0 )
            {
               AV20TFContratoServicosTelas_Status_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV21TFContratoServicosTelas_Status_Sels.FromJSonString(AV20TFContratoServicosTelas_Status_SelsJson);
            }
            AV53GXV1 = (int)(AV53GXV1+1);
         }
         if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV39GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV39GridStateDynamicFilter.gxTpr_Operator;
               AV42ContratoServicosTelas_ServicoCod1 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV43DynamicFiltersEnabled2 = true;
               AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(2));
               AV44DynamicFiltersSelector2 = AV39GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
               {
                  AV45DynamicFiltersOperator2 = AV39GridStateDynamicFilter.gxTpr_Operator;
                  AV46ContratoServicosTelas_ServicoCod2 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV47DynamicFiltersEnabled3 = true;
                  AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(3));
                  AV48DynamicFiltersSelector3 = AV39GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV48DynamicFiltersSelector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 )
                  {
                     AV49DynamicFiltersOperator3 = AV39GridStateDynamicFilter.gxTpr_Operator;
                     AV50ContratoServicosTelas_ServicoCod3 = (int)(NumberUtil.Val( AV39GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV10TFContratada_PessoaNom = AV22SearchTxt;
         AV11TFContratada_PessoaNom_Sel = "";
         AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 = AV42ContratoServicosTelas_ServicoCod1;
         AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 = AV46ContratoServicosTelas_ServicoCod2;
         AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 = AV47DynamicFiltersEnabled3;
         AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 = AV48DynamicFiltersSelector3;
         AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 = AV49DynamicFiltersOperator3;
         AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 = AV50ContratoServicosTelas_ServicoCod3;
         AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = AV10TFContratada_PessoaNom;
         AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel = AV11TFContratada_PessoaNom_Sel;
         AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = AV12TFContratoServicosTelas_ServicoSigla;
         AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel = AV13TFContratoServicosTelas_ServicoSigla_Sel;
         AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = AV14TFContratoServicosTelas_Tela;
         AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel = AV15TFContratoServicosTelas_Tela_Sel;
         AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = AV16TFContratoServicosTelas_Link;
         AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel = AV17TFContratoServicosTelas_Link_Sel;
         AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = AV18TFContratoServicosTelas_Parms;
         AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel = AV19TFContratoServicosTelas_Parms_Sel;
         AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels = AV21TFContratoServicosTelas_Status_Sels;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels ,
                                              AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 ,
                                              AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 ,
                                              AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 ,
                                              AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 ,
                                              AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 ,
                                              AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 ,
                                              AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 ,
                                              AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 ,
                                              AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 ,
                                              AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 ,
                                              AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 ,
                                              AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel ,
                                              AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ,
                                              AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel ,
                                              AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ,
                                              AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel ,
                                              AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ,
                                              AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel ,
                                              AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ,
                                              AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel ,
                                              AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ,
                                              AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels.Count ,
                                              A925ContratoServicosTelas_ServicoCod ,
                                              A41Contratada_PessoaNom ,
                                              A937ContratoServicosTelas_ServicoSigla ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom), 100, "%");
         lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla), 15, "%");
         lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = StringUtil.PadR( StringUtil.RTrim( AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela), 50, "%");
         lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link), "%", "");
         lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = StringUtil.Concat( StringUtil.RTrim( AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms), "%", "");
         /* Using cursor P00P92 */
         pr_default.execute(0, new Object[] {AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom, AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel, lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla, AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel, lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela, AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel, lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link, AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel, lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms, AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKP92 = false;
            A926ContratoServicosTelas_ContratoCod = P00P92_A926ContratoServicosTelas_ContratoCod[0];
            A74Contrato_Codigo = P00P92_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00P92_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00P92_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00P92_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00P92_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00P92_n41Contratada_PessoaNom[0];
            A932ContratoServicosTelas_Status = P00P92_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00P92_n932ContratoServicosTelas_Status[0];
            A929ContratoServicosTelas_Parms = P00P92_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00P92_n929ContratoServicosTelas_Parms[0];
            A928ContratoServicosTelas_Link = P00P92_A928ContratoServicosTelas_Link[0];
            A931ContratoServicosTelas_Tela = P00P92_A931ContratoServicosTelas_Tela[0];
            A937ContratoServicosTelas_ServicoSigla = P00P92_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00P92_n937ContratoServicosTelas_ServicoSigla[0];
            A925ContratoServicosTelas_ServicoCod = P00P92_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00P92_n925ContratoServicosTelas_ServicoCod[0];
            A938ContratoServicosTelas_Sequencial = P00P92_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00P92_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00P92_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = P00P92_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00P92_n925ContratoServicosTelas_ServicoCod[0];
            A39Contratada_Codigo = P00P92_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00P92_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00P92_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00P92_n41Contratada_PessoaNom[0];
            A937ContratoServicosTelas_ServicoSigla = P00P92_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00P92_n937ContratoServicosTelas_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00P92_A41Contratada_PessoaNom[0], A41Contratada_PessoaNom) == 0 ) )
            {
               BRKP92 = false;
               A926ContratoServicosTelas_ContratoCod = P00P92_A926ContratoServicosTelas_ContratoCod[0];
               A74Contrato_Codigo = P00P92_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00P92_n74Contrato_Codigo[0];
               A39Contratada_Codigo = P00P92_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00P92_A40Contratada_PessoaCod[0];
               A938ContratoServicosTelas_Sequencial = P00P92_A938ContratoServicosTelas_Sequencial[0];
               A74Contrato_Codigo = P00P92_A74Contrato_Codigo[0];
               n74Contrato_Codigo = P00P92_n74Contrato_Codigo[0];
               A39Contratada_Codigo = P00P92_A39Contratada_Codigo[0];
               A40Contratada_PessoaCod = P00P92_A40Contratada_PessoaCod[0];
               AV34count = (long)(AV34count+1);
               BRKP92 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV26Option = A41Contratada_PessoaNom;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP92 )
            {
               BRKP92 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOSERVICOSTELAS_SERVICOSIGLAOPTIONS' Routine */
         AV12TFContratoServicosTelas_ServicoSigla = AV22SearchTxt;
         AV13TFContratoServicosTelas_ServicoSigla_Sel = "";
         AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 = AV42ContratoServicosTelas_ServicoCod1;
         AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 = AV46ContratoServicosTelas_ServicoCod2;
         AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 = AV47DynamicFiltersEnabled3;
         AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 = AV48DynamicFiltersSelector3;
         AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 = AV49DynamicFiltersOperator3;
         AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 = AV50ContratoServicosTelas_ServicoCod3;
         AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = AV10TFContratada_PessoaNom;
         AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel = AV11TFContratada_PessoaNom_Sel;
         AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = AV12TFContratoServicosTelas_ServicoSigla;
         AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel = AV13TFContratoServicosTelas_ServicoSigla_Sel;
         AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = AV14TFContratoServicosTelas_Tela;
         AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel = AV15TFContratoServicosTelas_Tela_Sel;
         AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = AV16TFContratoServicosTelas_Link;
         AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel = AV17TFContratoServicosTelas_Link_Sel;
         AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = AV18TFContratoServicosTelas_Parms;
         AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel = AV19TFContratoServicosTelas_Parms_Sel;
         AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels = AV21TFContratoServicosTelas_Status_Sels;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels ,
                                              AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 ,
                                              AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 ,
                                              AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 ,
                                              AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 ,
                                              AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 ,
                                              AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 ,
                                              AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 ,
                                              AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 ,
                                              AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 ,
                                              AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 ,
                                              AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 ,
                                              AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel ,
                                              AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ,
                                              AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel ,
                                              AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ,
                                              AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel ,
                                              AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ,
                                              AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel ,
                                              AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ,
                                              AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel ,
                                              AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ,
                                              AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels.Count ,
                                              A925ContratoServicosTelas_ServicoCod ,
                                              A41Contratada_PessoaNom ,
                                              A937ContratoServicosTelas_ServicoSigla ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom), 100, "%");
         lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla), 15, "%");
         lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = StringUtil.PadR( StringUtil.RTrim( AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela), 50, "%");
         lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link), "%", "");
         lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = StringUtil.Concat( StringUtil.RTrim( AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms), "%", "");
         /* Using cursor P00P93 */
         pr_default.execute(1, new Object[] {AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom, AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel, lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla, AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel, lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela, AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel, lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link, AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel, lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms, AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKP94 = false;
            A926ContratoServicosTelas_ContratoCod = P00P93_A926ContratoServicosTelas_ContratoCod[0];
            A74Contrato_Codigo = P00P93_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00P93_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00P93_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00P93_A40Contratada_PessoaCod[0];
            A937ContratoServicosTelas_ServicoSigla = P00P93_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00P93_n937ContratoServicosTelas_ServicoSigla[0];
            A932ContratoServicosTelas_Status = P00P93_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00P93_n932ContratoServicosTelas_Status[0];
            A929ContratoServicosTelas_Parms = P00P93_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00P93_n929ContratoServicosTelas_Parms[0];
            A928ContratoServicosTelas_Link = P00P93_A928ContratoServicosTelas_Link[0];
            A931ContratoServicosTelas_Tela = P00P93_A931ContratoServicosTelas_Tela[0];
            A41Contratada_PessoaNom = P00P93_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00P93_n41Contratada_PessoaNom[0];
            A925ContratoServicosTelas_ServicoCod = P00P93_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00P93_n925ContratoServicosTelas_ServicoCod[0];
            A938ContratoServicosTelas_Sequencial = P00P93_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00P93_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00P93_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = P00P93_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00P93_n925ContratoServicosTelas_ServicoCod[0];
            A39Contratada_Codigo = P00P93_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00P93_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00P93_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00P93_n41Contratada_PessoaNom[0];
            A937ContratoServicosTelas_ServicoSigla = P00P93_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00P93_n937ContratoServicosTelas_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00P93_A937ContratoServicosTelas_ServicoSigla[0], A937ContratoServicosTelas_ServicoSigla) == 0 ) )
            {
               BRKP94 = false;
               A926ContratoServicosTelas_ContratoCod = P00P93_A926ContratoServicosTelas_ContratoCod[0];
               A925ContratoServicosTelas_ServicoCod = P00P93_A925ContratoServicosTelas_ServicoCod[0];
               n925ContratoServicosTelas_ServicoCod = P00P93_n925ContratoServicosTelas_ServicoCod[0];
               A938ContratoServicosTelas_Sequencial = P00P93_A938ContratoServicosTelas_Sequencial[0];
               A925ContratoServicosTelas_ServicoCod = P00P93_A925ContratoServicosTelas_ServicoCod[0];
               n925ContratoServicosTelas_ServicoCod = P00P93_n925ContratoServicosTelas_ServicoCod[0];
               AV34count = (long)(AV34count+1);
               BRKP94 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A937ContratoServicosTelas_ServicoSigla)) )
            {
               AV26Option = A937ContratoServicosTelas_ServicoSigla;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP94 )
            {
               BRKP94 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOSERVICOSTELAS_TELAOPTIONS' Routine */
         AV14TFContratoServicosTelas_Tela = AV22SearchTxt;
         AV15TFContratoServicosTelas_Tela_Sel = "";
         AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 = AV42ContratoServicosTelas_ServicoCod1;
         AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 = AV46ContratoServicosTelas_ServicoCod2;
         AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 = AV47DynamicFiltersEnabled3;
         AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 = AV48DynamicFiltersSelector3;
         AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 = AV49DynamicFiltersOperator3;
         AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 = AV50ContratoServicosTelas_ServicoCod3;
         AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = AV10TFContratada_PessoaNom;
         AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel = AV11TFContratada_PessoaNom_Sel;
         AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = AV12TFContratoServicosTelas_ServicoSigla;
         AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel = AV13TFContratoServicosTelas_ServicoSigla_Sel;
         AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = AV14TFContratoServicosTelas_Tela;
         AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel = AV15TFContratoServicosTelas_Tela_Sel;
         AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = AV16TFContratoServicosTelas_Link;
         AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel = AV17TFContratoServicosTelas_Link_Sel;
         AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = AV18TFContratoServicosTelas_Parms;
         AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel = AV19TFContratoServicosTelas_Parms_Sel;
         AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels = AV21TFContratoServicosTelas_Status_Sels;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels ,
                                              AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 ,
                                              AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 ,
                                              AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 ,
                                              AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 ,
                                              AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 ,
                                              AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 ,
                                              AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 ,
                                              AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 ,
                                              AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 ,
                                              AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 ,
                                              AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 ,
                                              AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel ,
                                              AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ,
                                              AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel ,
                                              AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ,
                                              AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel ,
                                              AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ,
                                              AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel ,
                                              AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ,
                                              AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel ,
                                              AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ,
                                              AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels.Count ,
                                              A925ContratoServicosTelas_ServicoCod ,
                                              A41Contratada_PessoaNom ,
                                              A937ContratoServicosTelas_ServicoSigla ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom), 100, "%");
         lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla), 15, "%");
         lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = StringUtil.PadR( StringUtil.RTrim( AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela), 50, "%");
         lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link), "%", "");
         lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = StringUtil.Concat( StringUtil.RTrim( AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms), "%", "");
         /* Using cursor P00P94 */
         pr_default.execute(2, new Object[] {AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom, AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel, lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla, AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel, lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela, AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel, lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link, AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel, lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms, AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKP96 = false;
            A926ContratoServicosTelas_ContratoCod = P00P94_A926ContratoServicosTelas_ContratoCod[0];
            A74Contrato_Codigo = P00P94_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00P94_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00P94_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00P94_A40Contratada_PessoaCod[0];
            A931ContratoServicosTelas_Tela = P00P94_A931ContratoServicosTelas_Tela[0];
            A932ContratoServicosTelas_Status = P00P94_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00P94_n932ContratoServicosTelas_Status[0];
            A929ContratoServicosTelas_Parms = P00P94_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00P94_n929ContratoServicosTelas_Parms[0];
            A928ContratoServicosTelas_Link = P00P94_A928ContratoServicosTelas_Link[0];
            A937ContratoServicosTelas_ServicoSigla = P00P94_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00P94_n937ContratoServicosTelas_ServicoSigla[0];
            A41Contratada_PessoaNom = P00P94_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00P94_n41Contratada_PessoaNom[0];
            A925ContratoServicosTelas_ServicoCod = P00P94_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00P94_n925ContratoServicosTelas_ServicoCod[0];
            A938ContratoServicosTelas_Sequencial = P00P94_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00P94_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00P94_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = P00P94_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00P94_n925ContratoServicosTelas_ServicoCod[0];
            A39Contratada_Codigo = P00P94_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00P94_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00P94_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00P94_n41Contratada_PessoaNom[0];
            A937ContratoServicosTelas_ServicoSigla = P00P94_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00P94_n937ContratoServicosTelas_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00P94_A931ContratoServicosTelas_Tela[0], A931ContratoServicosTelas_Tela) == 0 ) )
            {
               BRKP96 = false;
               A926ContratoServicosTelas_ContratoCod = P00P94_A926ContratoServicosTelas_ContratoCod[0];
               A938ContratoServicosTelas_Sequencial = P00P94_A938ContratoServicosTelas_Sequencial[0];
               AV34count = (long)(AV34count+1);
               BRKP96 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A931ContratoServicosTelas_Tela)) )
            {
               AV26Option = A931ContratoServicosTelas_Tela;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP96 )
            {
               BRKP96 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATOSERVICOSTELAS_LINKOPTIONS' Routine */
         AV16TFContratoServicosTelas_Link = AV22SearchTxt;
         AV17TFContratoServicosTelas_Link_Sel = "";
         AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 = AV42ContratoServicosTelas_ServicoCod1;
         AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 = AV46ContratoServicosTelas_ServicoCod2;
         AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 = AV47DynamicFiltersEnabled3;
         AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 = AV48DynamicFiltersSelector3;
         AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 = AV49DynamicFiltersOperator3;
         AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 = AV50ContratoServicosTelas_ServicoCod3;
         AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = AV10TFContratada_PessoaNom;
         AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel = AV11TFContratada_PessoaNom_Sel;
         AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = AV12TFContratoServicosTelas_ServicoSigla;
         AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel = AV13TFContratoServicosTelas_ServicoSigla_Sel;
         AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = AV14TFContratoServicosTelas_Tela;
         AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel = AV15TFContratoServicosTelas_Tela_Sel;
         AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = AV16TFContratoServicosTelas_Link;
         AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel = AV17TFContratoServicosTelas_Link_Sel;
         AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = AV18TFContratoServicosTelas_Parms;
         AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel = AV19TFContratoServicosTelas_Parms_Sel;
         AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels = AV21TFContratoServicosTelas_Status_Sels;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels ,
                                              AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 ,
                                              AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 ,
                                              AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 ,
                                              AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 ,
                                              AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 ,
                                              AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 ,
                                              AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 ,
                                              AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 ,
                                              AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 ,
                                              AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 ,
                                              AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 ,
                                              AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel ,
                                              AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ,
                                              AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel ,
                                              AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ,
                                              AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel ,
                                              AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ,
                                              AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel ,
                                              AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ,
                                              AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel ,
                                              AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ,
                                              AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels.Count ,
                                              A925ContratoServicosTelas_ServicoCod ,
                                              A41Contratada_PessoaNom ,
                                              A937ContratoServicosTelas_ServicoSigla ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom), 100, "%");
         lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla), 15, "%");
         lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = StringUtil.PadR( StringUtil.RTrim( AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela), 50, "%");
         lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link), "%", "");
         lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = StringUtil.Concat( StringUtil.RTrim( AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms), "%", "");
         /* Using cursor P00P95 */
         pr_default.execute(3, new Object[] {AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom, AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel, lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla, AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel, lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela, AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel, lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link, AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel, lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms, AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKP98 = false;
            A926ContratoServicosTelas_ContratoCod = P00P95_A926ContratoServicosTelas_ContratoCod[0];
            A74Contrato_Codigo = P00P95_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00P95_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00P95_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00P95_A40Contratada_PessoaCod[0];
            A928ContratoServicosTelas_Link = P00P95_A928ContratoServicosTelas_Link[0];
            A932ContratoServicosTelas_Status = P00P95_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00P95_n932ContratoServicosTelas_Status[0];
            A929ContratoServicosTelas_Parms = P00P95_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00P95_n929ContratoServicosTelas_Parms[0];
            A931ContratoServicosTelas_Tela = P00P95_A931ContratoServicosTelas_Tela[0];
            A937ContratoServicosTelas_ServicoSigla = P00P95_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00P95_n937ContratoServicosTelas_ServicoSigla[0];
            A41Contratada_PessoaNom = P00P95_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00P95_n41Contratada_PessoaNom[0];
            A925ContratoServicosTelas_ServicoCod = P00P95_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00P95_n925ContratoServicosTelas_ServicoCod[0];
            A938ContratoServicosTelas_Sequencial = P00P95_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00P95_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00P95_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = P00P95_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00P95_n925ContratoServicosTelas_ServicoCod[0];
            A39Contratada_Codigo = P00P95_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00P95_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00P95_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00P95_n41Contratada_PessoaNom[0];
            A937ContratoServicosTelas_ServicoSigla = P00P95_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00P95_n937ContratoServicosTelas_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00P95_A928ContratoServicosTelas_Link[0], A928ContratoServicosTelas_Link) == 0 ) )
            {
               BRKP98 = false;
               A926ContratoServicosTelas_ContratoCod = P00P95_A926ContratoServicosTelas_ContratoCod[0];
               A938ContratoServicosTelas_Sequencial = P00P95_A938ContratoServicosTelas_Sequencial[0];
               AV34count = (long)(AV34count+1);
               BRKP98 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A928ContratoServicosTelas_Link)) )
            {
               AV26Option = A928ContratoServicosTelas_Link;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP98 )
            {
               BRKP98 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADCONTRATOSERVICOSTELAS_PARMSOPTIONS' Routine */
         AV18TFContratoServicosTelas_Parms = AV22SearchTxt;
         AV19TFContratoServicosTelas_Parms_Sel = "";
         AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 = AV42ContratoServicosTelas_ServicoCod1;
         AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 = AV46ContratoServicosTelas_ServicoCod2;
         AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 = AV47DynamicFiltersEnabled3;
         AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 = AV48DynamicFiltersSelector3;
         AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 = AV49DynamicFiltersOperator3;
         AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 = AV50ContratoServicosTelas_ServicoCod3;
         AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = AV10TFContratada_PessoaNom;
         AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel = AV11TFContratada_PessoaNom_Sel;
         AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = AV12TFContratoServicosTelas_ServicoSigla;
         AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel = AV13TFContratoServicosTelas_ServicoSigla_Sel;
         AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = AV14TFContratoServicosTelas_Tela;
         AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel = AV15TFContratoServicosTelas_Tela_Sel;
         AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = AV16TFContratoServicosTelas_Link;
         AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel = AV17TFContratoServicosTelas_Link_Sel;
         AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = AV18TFContratoServicosTelas_Parms;
         AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel = AV19TFContratoServicosTelas_Parms_Sel;
         AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels = AV21TFContratoServicosTelas_Status_Sels;
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              A932ContratoServicosTelas_Status ,
                                              AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels ,
                                              AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 ,
                                              AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 ,
                                              AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 ,
                                              AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 ,
                                              AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 ,
                                              AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 ,
                                              AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 ,
                                              AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 ,
                                              AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 ,
                                              AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 ,
                                              AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 ,
                                              AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel ,
                                              AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ,
                                              AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel ,
                                              AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ,
                                              AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel ,
                                              AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ,
                                              AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel ,
                                              AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ,
                                              AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel ,
                                              AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ,
                                              AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels.Count ,
                                              A925ContratoServicosTelas_ServicoCod ,
                                              A41Contratada_PessoaNom ,
                                              A937ContratoServicosTelas_ServicoSigla ,
                                              A931ContratoServicosTelas_Tela ,
                                              A928ContratoServicosTelas_Link ,
                                              A929ContratoServicosTelas_Parms },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom), 100, "%");
         lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = StringUtil.PadR( StringUtil.RTrim( AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla), 15, "%");
         lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = StringUtil.PadR( StringUtil.RTrim( AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela), 50, "%");
         lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = StringUtil.Concat( StringUtil.RTrim( AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link), "%", "");
         lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = StringUtil.Concat( StringUtil.RTrim( AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms), "%", "");
         /* Using cursor P00P96 */
         pr_default.execute(4, new Object[] {AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3, lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom, AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel, lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla, AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel, lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela, AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel, lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link, AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel, lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms, AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKP910 = false;
            A926ContratoServicosTelas_ContratoCod = P00P96_A926ContratoServicosTelas_ContratoCod[0];
            A74Contrato_Codigo = P00P96_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00P96_n74Contrato_Codigo[0];
            A39Contratada_Codigo = P00P96_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00P96_A40Contratada_PessoaCod[0];
            A929ContratoServicosTelas_Parms = P00P96_A929ContratoServicosTelas_Parms[0];
            n929ContratoServicosTelas_Parms = P00P96_n929ContratoServicosTelas_Parms[0];
            A932ContratoServicosTelas_Status = P00P96_A932ContratoServicosTelas_Status[0];
            n932ContratoServicosTelas_Status = P00P96_n932ContratoServicosTelas_Status[0];
            A928ContratoServicosTelas_Link = P00P96_A928ContratoServicosTelas_Link[0];
            A931ContratoServicosTelas_Tela = P00P96_A931ContratoServicosTelas_Tela[0];
            A937ContratoServicosTelas_ServicoSigla = P00P96_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00P96_n937ContratoServicosTelas_ServicoSigla[0];
            A41Contratada_PessoaNom = P00P96_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00P96_n41Contratada_PessoaNom[0];
            A925ContratoServicosTelas_ServicoCod = P00P96_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00P96_n925ContratoServicosTelas_ServicoCod[0];
            A938ContratoServicosTelas_Sequencial = P00P96_A938ContratoServicosTelas_Sequencial[0];
            A74Contrato_Codigo = P00P96_A74Contrato_Codigo[0];
            n74Contrato_Codigo = P00P96_n74Contrato_Codigo[0];
            A925ContratoServicosTelas_ServicoCod = P00P96_A925ContratoServicosTelas_ServicoCod[0];
            n925ContratoServicosTelas_ServicoCod = P00P96_n925ContratoServicosTelas_ServicoCod[0];
            A39Contratada_Codigo = P00P96_A39Contratada_Codigo[0];
            A40Contratada_PessoaCod = P00P96_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = P00P96_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00P96_n41Contratada_PessoaNom[0];
            A937ContratoServicosTelas_ServicoSigla = P00P96_A937ContratoServicosTelas_ServicoSigla[0];
            n937ContratoServicosTelas_ServicoSigla = P00P96_n937ContratoServicosTelas_ServicoSigla[0];
            AV34count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00P96_A929ContratoServicosTelas_Parms[0], A929ContratoServicosTelas_Parms) == 0 ) )
            {
               BRKP910 = false;
               A926ContratoServicosTelas_ContratoCod = P00P96_A926ContratoServicosTelas_ContratoCod[0];
               A938ContratoServicosTelas_Sequencial = P00P96_A938ContratoServicosTelas_Sequencial[0];
               AV34count = (long)(AV34count+1);
               BRKP910 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A929ContratoServicosTelas_Parms)) )
            {
               AV26Option = A929ContratoServicosTelas_Parms;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKP910 )
            {
               BRKP910 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratada_PessoaNom = "";
         AV11TFContratada_PessoaNom_Sel = "";
         AV12TFContratoServicosTelas_ServicoSigla = "";
         AV13TFContratoServicosTelas_ServicoSigla_Sel = "";
         AV14TFContratoServicosTelas_Tela = "";
         AV15TFContratoServicosTelas_Tela_Sel = "";
         AV16TFContratoServicosTelas_Link = "";
         AV17TFContratoServicosTelas_Link_Sel = "";
         AV18TFContratoServicosTelas_Parms = "";
         AV19TFContratoServicosTelas_Parms_Sel = "";
         AV20TFContratoServicosTelas_Status_SelsJson = "";
         AV21TFContratoServicosTelas_Status_Sels = new GxSimpleCollection();
         AV39GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV44DynamicFiltersSelector2 = "";
         AV48DynamicFiltersSelector3 = "";
         AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 = "";
         AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 = "";
         AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 = "";
         AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = "";
         AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel = "";
         AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = "";
         AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel = "";
         AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = "";
         AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel = "";
         AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = "";
         AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel = "";
         AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = "";
         AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel = "";
         AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom = "";
         lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla = "";
         lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela = "";
         lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link = "";
         lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms = "";
         A932ContratoServicosTelas_Status = "";
         A41Contratada_PessoaNom = "";
         A937ContratoServicosTelas_ServicoSigla = "";
         A931ContratoServicosTelas_Tela = "";
         A928ContratoServicosTelas_Link = "";
         A929ContratoServicosTelas_Parms = "";
         P00P92_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00P92_A74Contrato_Codigo = new int[1] ;
         P00P92_n74Contrato_Codigo = new bool[] {false} ;
         P00P92_A39Contratada_Codigo = new int[1] ;
         P00P92_A40Contratada_PessoaCod = new int[1] ;
         P00P92_A41Contratada_PessoaNom = new String[] {""} ;
         P00P92_n41Contratada_PessoaNom = new bool[] {false} ;
         P00P92_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00P92_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00P92_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00P92_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00P92_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00P92_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00P92_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         P00P92_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         P00P92_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         P00P92_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         P00P92_A938ContratoServicosTelas_Sequencial = new short[1] ;
         AV26Option = "";
         P00P93_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00P93_A74Contrato_Codigo = new int[1] ;
         P00P93_n74Contrato_Codigo = new bool[] {false} ;
         P00P93_A39Contratada_Codigo = new int[1] ;
         P00P93_A40Contratada_PessoaCod = new int[1] ;
         P00P93_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         P00P93_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         P00P93_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00P93_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00P93_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00P93_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00P93_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00P93_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00P93_A41Contratada_PessoaNom = new String[] {""} ;
         P00P93_n41Contratada_PessoaNom = new bool[] {false} ;
         P00P93_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         P00P93_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         P00P93_A938ContratoServicosTelas_Sequencial = new short[1] ;
         P00P94_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00P94_A74Contrato_Codigo = new int[1] ;
         P00P94_n74Contrato_Codigo = new bool[] {false} ;
         P00P94_A39Contratada_Codigo = new int[1] ;
         P00P94_A40Contratada_PessoaCod = new int[1] ;
         P00P94_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00P94_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00P94_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00P94_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00P94_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00P94_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00P94_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         P00P94_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         P00P94_A41Contratada_PessoaNom = new String[] {""} ;
         P00P94_n41Contratada_PessoaNom = new bool[] {false} ;
         P00P94_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         P00P94_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         P00P94_A938ContratoServicosTelas_Sequencial = new short[1] ;
         P00P95_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00P95_A74Contrato_Codigo = new int[1] ;
         P00P95_n74Contrato_Codigo = new bool[] {false} ;
         P00P95_A39Contratada_Codigo = new int[1] ;
         P00P95_A40Contratada_PessoaCod = new int[1] ;
         P00P95_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00P95_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00P95_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00P95_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00P95_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00P95_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00P95_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         P00P95_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         P00P95_A41Contratada_PessoaNom = new String[] {""} ;
         P00P95_n41Contratada_PessoaNom = new bool[] {false} ;
         P00P95_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         P00P95_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         P00P95_A938ContratoServicosTelas_Sequencial = new short[1] ;
         P00P96_A926ContratoServicosTelas_ContratoCod = new int[1] ;
         P00P96_A74Contrato_Codigo = new int[1] ;
         P00P96_n74Contrato_Codigo = new bool[] {false} ;
         P00P96_A39Contratada_Codigo = new int[1] ;
         P00P96_A40Contratada_PessoaCod = new int[1] ;
         P00P96_A929ContratoServicosTelas_Parms = new String[] {""} ;
         P00P96_n929ContratoServicosTelas_Parms = new bool[] {false} ;
         P00P96_A932ContratoServicosTelas_Status = new String[] {""} ;
         P00P96_n932ContratoServicosTelas_Status = new bool[] {false} ;
         P00P96_A928ContratoServicosTelas_Link = new String[] {""} ;
         P00P96_A931ContratoServicosTelas_Tela = new String[] {""} ;
         P00P96_A937ContratoServicosTelas_ServicoSigla = new String[] {""} ;
         P00P96_n937ContratoServicosTelas_ServicoSigla = new bool[] {false} ;
         P00P96_A41Contratada_PessoaNom = new String[] {""} ;
         P00P96_n41Contratada_PessoaNom = new bool[] {false} ;
         P00P96_A925ContratoServicosTelas_ServicoCod = new int[1] ;
         P00P96_n925ContratoServicosTelas_ServicoCod = new bool[] {false} ;
         P00P96_A938ContratoServicosTelas_Sequencial = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwcontratoservicostelasfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00P92_A926ContratoServicosTelas_ContratoCod, P00P92_A74Contrato_Codigo, P00P92_n74Contrato_Codigo, P00P92_A39Contratada_Codigo, P00P92_A40Contratada_PessoaCod, P00P92_A41Contratada_PessoaNom, P00P92_n41Contratada_PessoaNom, P00P92_A932ContratoServicosTelas_Status, P00P92_n932ContratoServicosTelas_Status, P00P92_A929ContratoServicosTelas_Parms,
               P00P92_n929ContratoServicosTelas_Parms, P00P92_A928ContratoServicosTelas_Link, P00P92_A931ContratoServicosTelas_Tela, P00P92_A937ContratoServicosTelas_ServicoSigla, P00P92_n937ContratoServicosTelas_ServicoSigla, P00P92_A925ContratoServicosTelas_ServicoCod, P00P92_n925ContratoServicosTelas_ServicoCod, P00P92_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               P00P93_A926ContratoServicosTelas_ContratoCod, P00P93_A74Contrato_Codigo, P00P93_n74Contrato_Codigo, P00P93_A39Contratada_Codigo, P00P93_A40Contratada_PessoaCod, P00P93_A937ContratoServicosTelas_ServicoSigla, P00P93_n937ContratoServicosTelas_ServicoSigla, P00P93_A932ContratoServicosTelas_Status, P00P93_n932ContratoServicosTelas_Status, P00P93_A929ContratoServicosTelas_Parms,
               P00P93_n929ContratoServicosTelas_Parms, P00P93_A928ContratoServicosTelas_Link, P00P93_A931ContratoServicosTelas_Tela, P00P93_A41Contratada_PessoaNom, P00P93_n41Contratada_PessoaNom, P00P93_A925ContratoServicosTelas_ServicoCod, P00P93_n925ContratoServicosTelas_ServicoCod, P00P93_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               P00P94_A926ContratoServicosTelas_ContratoCod, P00P94_A74Contrato_Codigo, P00P94_n74Contrato_Codigo, P00P94_A39Contratada_Codigo, P00P94_A40Contratada_PessoaCod, P00P94_A931ContratoServicosTelas_Tela, P00P94_A932ContratoServicosTelas_Status, P00P94_n932ContratoServicosTelas_Status, P00P94_A929ContratoServicosTelas_Parms, P00P94_n929ContratoServicosTelas_Parms,
               P00P94_A928ContratoServicosTelas_Link, P00P94_A937ContratoServicosTelas_ServicoSigla, P00P94_n937ContratoServicosTelas_ServicoSigla, P00P94_A41Contratada_PessoaNom, P00P94_n41Contratada_PessoaNom, P00P94_A925ContratoServicosTelas_ServicoCod, P00P94_n925ContratoServicosTelas_ServicoCod, P00P94_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               P00P95_A926ContratoServicosTelas_ContratoCod, P00P95_A74Contrato_Codigo, P00P95_n74Contrato_Codigo, P00P95_A39Contratada_Codigo, P00P95_A40Contratada_PessoaCod, P00P95_A928ContratoServicosTelas_Link, P00P95_A932ContratoServicosTelas_Status, P00P95_n932ContratoServicosTelas_Status, P00P95_A929ContratoServicosTelas_Parms, P00P95_n929ContratoServicosTelas_Parms,
               P00P95_A931ContratoServicosTelas_Tela, P00P95_A937ContratoServicosTelas_ServicoSigla, P00P95_n937ContratoServicosTelas_ServicoSigla, P00P95_A41Contratada_PessoaNom, P00P95_n41Contratada_PessoaNom, P00P95_A925ContratoServicosTelas_ServicoCod, P00P95_n925ContratoServicosTelas_ServicoCod, P00P95_A938ContratoServicosTelas_Sequencial
               }
               , new Object[] {
               P00P96_A926ContratoServicosTelas_ContratoCod, P00P96_A74Contrato_Codigo, P00P96_n74Contrato_Codigo, P00P96_A39Contratada_Codigo, P00P96_A40Contratada_PessoaCod, P00P96_A929ContratoServicosTelas_Parms, P00P96_n929ContratoServicosTelas_Parms, P00P96_A932ContratoServicosTelas_Status, P00P96_n932ContratoServicosTelas_Status, P00P96_A928ContratoServicosTelas_Link,
               P00P96_A931ContratoServicosTelas_Tela, P00P96_A937ContratoServicosTelas_ServicoSigla, P00P96_n937ContratoServicosTelas_ServicoSigla, P00P96_A41Contratada_PessoaNom, P00P96_n41Contratada_PessoaNom, P00P96_A925ContratoServicosTelas_ServicoCod, P00P96_n925ContratoServicosTelas_ServicoCod, P00P96_A938ContratoServicosTelas_Sequencial
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV41DynamicFiltersOperator1 ;
      private short AV45DynamicFiltersOperator2 ;
      private short AV49DynamicFiltersOperator3 ;
      private short AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 ;
      private short AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 ;
      private short AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 ;
      private short A938ContratoServicosTelas_Sequencial ;
      private int AV53GXV1 ;
      private int AV42ContratoServicosTelas_ServicoCod1 ;
      private int AV46ContratoServicosTelas_ServicoCod2 ;
      private int AV50ContratoServicosTelas_ServicoCod3 ;
      private int AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 ;
      private int AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 ;
      private int AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 ;
      private int AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels_Count ;
      private int A925ContratoServicosTelas_ServicoCod ;
      private int A926ContratoServicosTelas_ContratoCod ;
      private int A74Contrato_Codigo ;
      private int A39Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private long AV34count ;
      private String AV10TFContratada_PessoaNom ;
      private String AV11TFContratada_PessoaNom_Sel ;
      private String AV12TFContratoServicosTelas_ServicoSigla ;
      private String AV13TFContratoServicosTelas_ServicoSigla_Sel ;
      private String AV14TFContratoServicosTelas_Tela ;
      private String AV15TFContratoServicosTelas_Tela_Sel ;
      private String AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ;
      private String AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel ;
      private String AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ;
      private String AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel ;
      private String AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ;
      private String AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel ;
      private String scmdbuf ;
      private String lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ;
      private String lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ;
      private String lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ;
      private String A932ContratoServicosTelas_Status ;
      private String A41Contratada_PessoaNom ;
      private String A937ContratoServicosTelas_ServicoSigla ;
      private String A931ContratoServicosTelas_Tela ;
      private bool returnInSub ;
      private bool AV43DynamicFiltersEnabled2 ;
      private bool AV47DynamicFiltersEnabled3 ;
      private bool AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 ;
      private bool AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 ;
      private bool BRKP92 ;
      private bool n74Contrato_Codigo ;
      private bool n41Contratada_PessoaNom ;
      private bool n932ContratoServicosTelas_Status ;
      private bool n929ContratoServicosTelas_Parms ;
      private bool n937ContratoServicosTelas_ServicoSigla ;
      private bool n925ContratoServicosTelas_ServicoCod ;
      private bool BRKP94 ;
      private bool BRKP96 ;
      private bool BRKP98 ;
      private bool BRKP910 ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV20TFContratoServicosTelas_Status_SelsJson ;
      private String A929ContratoServicosTelas_Parms ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV16TFContratoServicosTelas_Link ;
      private String AV17TFContratoServicosTelas_Link_Sel ;
      private String AV18TFContratoServicosTelas_Parms ;
      private String AV19TFContratoServicosTelas_Parms_Sel ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV44DynamicFiltersSelector2 ;
      private String AV48DynamicFiltersSelector3 ;
      private String AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 ;
      private String AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 ;
      private String AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 ;
      private String AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ;
      private String AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel ;
      private String AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ;
      private String AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel ;
      private String lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ;
      private String lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ;
      private String A928ContratoServicosTelas_Link ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00P92_A926ContratoServicosTelas_ContratoCod ;
      private int[] P00P92_A74Contrato_Codigo ;
      private bool[] P00P92_n74Contrato_Codigo ;
      private int[] P00P92_A39Contratada_Codigo ;
      private int[] P00P92_A40Contratada_PessoaCod ;
      private String[] P00P92_A41Contratada_PessoaNom ;
      private bool[] P00P92_n41Contratada_PessoaNom ;
      private String[] P00P92_A932ContratoServicosTelas_Status ;
      private bool[] P00P92_n932ContratoServicosTelas_Status ;
      private String[] P00P92_A929ContratoServicosTelas_Parms ;
      private bool[] P00P92_n929ContratoServicosTelas_Parms ;
      private String[] P00P92_A928ContratoServicosTelas_Link ;
      private String[] P00P92_A931ContratoServicosTelas_Tela ;
      private String[] P00P92_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] P00P92_n937ContratoServicosTelas_ServicoSigla ;
      private int[] P00P92_A925ContratoServicosTelas_ServicoCod ;
      private bool[] P00P92_n925ContratoServicosTelas_ServicoCod ;
      private short[] P00P92_A938ContratoServicosTelas_Sequencial ;
      private int[] P00P93_A926ContratoServicosTelas_ContratoCod ;
      private int[] P00P93_A74Contrato_Codigo ;
      private bool[] P00P93_n74Contrato_Codigo ;
      private int[] P00P93_A39Contratada_Codigo ;
      private int[] P00P93_A40Contratada_PessoaCod ;
      private String[] P00P93_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] P00P93_n937ContratoServicosTelas_ServicoSigla ;
      private String[] P00P93_A932ContratoServicosTelas_Status ;
      private bool[] P00P93_n932ContratoServicosTelas_Status ;
      private String[] P00P93_A929ContratoServicosTelas_Parms ;
      private bool[] P00P93_n929ContratoServicosTelas_Parms ;
      private String[] P00P93_A928ContratoServicosTelas_Link ;
      private String[] P00P93_A931ContratoServicosTelas_Tela ;
      private String[] P00P93_A41Contratada_PessoaNom ;
      private bool[] P00P93_n41Contratada_PessoaNom ;
      private int[] P00P93_A925ContratoServicosTelas_ServicoCod ;
      private bool[] P00P93_n925ContratoServicosTelas_ServicoCod ;
      private short[] P00P93_A938ContratoServicosTelas_Sequencial ;
      private int[] P00P94_A926ContratoServicosTelas_ContratoCod ;
      private int[] P00P94_A74Contrato_Codigo ;
      private bool[] P00P94_n74Contrato_Codigo ;
      private int[] P00P94_A39Contratada_Codigo ;
      private int[] P00P94_A40Contratada_PessoaCod ;
      private String[] P00P94_A931ContratoServicosTelas_Tela ;
      private String[] P00P94_A932ContratoServicosTelas_Status ;
      private bool[] P00P94_n932ContratoServicosTelas_Status ;
      private String[] P00P94_A929ContratoServicosTelas_Parms ;
      private bool[] P00P94_n929ContratoServicosTelas_Parms ;
      private String[] P00P94_A928ContratoServicosTelas_Link ;
      private String[] P00P94_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] P00P94_n937ContratoServicosTelas_ServicoSigla ;
      private String[] P00P94_A41Contratada_PessoaNom ;
      private bool[] P00P94_n41Contratada_PessoaNom ;
      private int[] P00P94_A925ContratoServicosTelas_ServicoCod ;
      private bool[] P00P94_n925ContratoServicosTelas_ServicoCod ;
      private short[] P00P94_A938ContratoServicosTelas_Sequencial ;
      private int[] P00P95_A926ContratoServicosTelas_ContratoCod ;
      private int[] P00P95_A74Contrato_Codigo ;
      private bool[] P00P95_n74Contrato_Codigo ;
      private int[] P00P95_A39Contratada_Codigo ;
      private int[] P00P95_A40Contratada_PessoaCod ;
      private String[] P00P95_A928ContratoServicosTelas_Link ;
      private String[] P00P95_A932ContratoServicosTelas_Status ;
      private bool[] P00P95_n932ContratoServicosTelas_Status ;
      private String[] P00P95_A929ContratoServicosTelas_Parms ;
      private bool[] P00P95_n929ContratoServicosTelas_Parms ;
      private String[] P00P95_A931ContratoServicosTelas_Tela ;
      private String[] P00P95_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] P00P95_n937ContratoServicosTelas_ServicoSigla ;
      private String[] P00P95_A41Contratada_PessoaNom ;
      private bool[] P00P95_n41Contratada_PessoaNom ;
      private int[] P00P95_A925ContratoServicosTelas_ServicoCod ;
      private bool[] P00P95_n925ContratoServicosTelas_ServicoCod ;
      private short[] P00P95_A938ContratoServicosTelas_Sequencial ;
      private int[] P00P96_A926ContratoServicosTelas_ContratoCod ;
      private int[] P00P96_A74Contrato_Codigo ;
      private bool[] P00P96_n74Contrato_Codigo ;
      private int[] P00P96_A39Contratada_Codigo ;
      private int[] P00P96_A40Contratada_PessoaCod ;
      private String[] P00P96_A929ContratoServicosTelas_Parms ;
      private bool[] P00P96_n929ContratoServicosTelas_Parms ;
      private String[] P00P96_A932ContratoServicosTelas_Status ;
      private bool[] P00P96_n932ContratoServicosTelas_Status ;
      private String[] P00P96_A928ContratoServicosTelas_Link ;
      private String[] P00P96_A931ContratoServicosTelas_Tela ;
      private String[] P00P96_A937ContratoServicosTelas_ServicoSigla ;
      private bool[] P00P96_n937ContratoServicosTelas_ServicoSigla ;
      private String[] P00P96_A41Contratada_PessoaNom ;
      private bool[] P00P96_n41Contratada_PessoaNom ;
      private int[] P00P96_A925ContratoServicosTelas_ServicoCod ;
      private bool[] P00P96_n925ContratoServicosTelas_ServicoCod ;
      private short[] P00P96_A938ContratoServicosTelas_Sequencial ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21TFContratoServicosTelas_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV39GridStateDynamicFilter ;
   }

   public class getwwcontratoservicostelasfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00P92( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels ,
                                             String AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 ,
                                             short AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 ,
                                             int AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 ,
                                             bool AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 ,
                                             String AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 ,
                                             short AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 ,
                                             int AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 ,
                                             bool AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 ,
                                             String AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 ,
                                             short AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 ,
                                             int AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 ,
                                             String AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel ,
                                             String AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ,
                                             String AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel ,
                                             String AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ,
                                             String AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel ,
                                             String AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ,
                                             String AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel ,
                                             String AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ,
                                             String AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel ,
                                             String AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ,
                                             int AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [19] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T5.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Tela], T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod, T1.[ContratoServicosTelas_Sequencial] FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] like @lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] = @AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T5.[Pessoa_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00P93( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels ,
                                             String AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 ,
                                             short AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 ,
                                             int AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 ,
                                             bool AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 ,
                                             String AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 ,
                                             short AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 ,
                                             int AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 ,
                                             bool AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 ,
                                             String AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 ,
                                             short AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 ,
                                             int AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 ,
                                             String AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel ,
                                             String AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ,
                                             String AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel ,
                                             String AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ,
                                             String AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel ,
                                             String AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ,
                                             String AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel ,
                                             String AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ,
                                             String AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel ,
                                             String AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ,
                                             int AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [19] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Tela], T5.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod, T1.[ContratoServicosTelas_Sequencial] FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] like @lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] = @AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T6.[Servico_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00P94( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels ,
                                             String AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 ,
                                             short AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 ,
                                             int AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 ,
                                             bool AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 ,
                                             String AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 ,
                                             short AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 ,
                                             int AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 ,
                                             bool AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 ,
                                             String AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 ,
                                             short AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 ,
                                             int AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 ,
                                             String AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel ,
                                             String AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ,
                                             String AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel ,
                                             String AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ,
                                             String AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel ,
                                             String AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ,
                                             String AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel ,
                                             String AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ,
                                             String AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel ,
                                             String AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ,
                                             int AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [19] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_Tela], T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Link], T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, T5.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod, T1.[ContratoServicosTelas_Sequencial] FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] like @lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] = @AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosTelas_Tela]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00P95( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels ,
                                             String AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 ,
                                             short AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 ,
                                             int AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 ,
                                             bool AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 ,
                                             String AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 ,
                                             short AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 ,
                                             int AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 ,
                                             bool AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 ,
                                             String AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 ,
                                             short AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 ,
                                             int AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 ,
                                             String AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel ,
                                             String AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ,
                                             String AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel ,
                                             String AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ,
                                             String AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel ,
                                             String AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ,
                                             String AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel ,
                                             String AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ,
                                             String AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel ,
                                             String AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ,
                                             int AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [19] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Tela], T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, T5.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod, T1.[ContratoServicosTelas_Sequencial] FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] like @lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] = @AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosTelas_Link]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00P96( IGxContext context ,
                                             String A932ContratoServicosTelas_Status ,
                                             IGxCollection AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels ,
                                             String AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1 ,
                                             short AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 ,
                                             int AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1 ,
                                             bool AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 ,
                                             String AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2 ,
                                             short AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 ,
                                             int AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2 ,
                                             bool AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 ,
                                             String AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3 ,
                                             short AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 ,
                                             int AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3 ,
                                             String AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel ,
                                             String AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom ,
                                             String AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel ,
                                             String AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla ,
                                             String AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel ,
                                             String AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela ,
                                             String AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel ,
                                             String AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link ,
                                             String AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel ,
                                             String AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms ,
                                             int AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels_Count ,
                                             int A925ContratoServicosTelas_ServicoCod ,
                                             String A41Contratada_PessoaNom ,
                                             String A937ContratoServicosTelas_ServicoSigla ,
                                             String A931ContratoServicosTelas_Tela ,
                                             String A928ContratoServicosTelas_Link ,
                                             String A929ContratoServicosTelas_Parms )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [19] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoServicosTelas_ContratoCod] AS ContratoServicosTelas_ContratoCod, T2.[Contrato_Codigo], T3.[Contratada_Codigo], T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[ContratoServicosTelas_Parms], T1.[ContratoServicosTelas_Status], T1.[ContratoServicosTelas_Link], T1.[ContratoServicosTelas_Tela], T6.[Servico_Sigla] AS ContratoServicosTelas_ServicoSigla, T5.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Servico_Codigo] AS ContratoServicosTelas_ServicoCod, T1.[ContratoServicosTelas_Sequencial] FROM ((((([ContratoServicosTelas] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosTelas_ContratoCod]) LEFT JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) LEFT JOIN [Pessoa] T5 WITH (NOLOCK) ON T5.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T2.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int9[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int9[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV55WWContratoServicosTelasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV56WWContratoServicosTelasDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1)";
            }
         }
         else
         {
            GXv_int9[2] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int9[3] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( AV58WWContratoServicosTelasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWContratoServicosTelasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV60WWContratoServicosTelasDS_6_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2)";
            }
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] < @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] < @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] = @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] = @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( AV62WWContratoServicosTelasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV63WWContratoServicosTelasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSTELAS_SERVICOCOD") == 0 ) && ( AV64WWContratoServicosTelasDS_10_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Codigo] > @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Codigo] > @AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3)";
            }
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] like @lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] like @lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom)";
            }
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T5.[Pessoa_Nome] = @AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T5.[Pessoa_Nome] = @AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] like @lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] like @lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla)";
            }
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T6.[Servico_Sigla] = @AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T6.[Servico_Sigla] = @AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel)";
            }
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] like @lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] like @lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela)";
            }
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Tela] = @AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Tela] = @AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel)";
            }
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] like @lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] like @lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link)";
            }
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Link] = @AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Link] = @AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel)";
            }
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] like @lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] like @lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms)";
            }
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoServicosTelas_Parms] = @AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoServicosTelas_Parms] = @AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel)";
            }
         }
         else
         {
            GXv_int9[18] = 1;
         }
         if ( AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV76WWContratoServicosTelasDS_22_Tfcontratoservicostelas_status_sels, "T1.[ContratoServicosTelas_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoServicosTelas_Parms]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00P92(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] );
               case 1 :
                     return conditional_P00P93(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] );
               case 2 :
                     return conditional_P00P94(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] );
               case 3 :
                     return conditional_P00P95(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] );
               case 4 :
                     return conditional_P00P96(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (int)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00P92 ;
          prmP00P92 = new Object[] {
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00P93 ;
          prmP00P93 = new Object[] {
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00P94 ;
          prmP00P94 = new Object[] {
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00P95 ;
          prmP00P95 = new Object[] {
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00P96 ;
          prmP00P96 = new Object[] {
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV57WWContratoServicosTelasDS_3_Contratoservicostelas_servicocod1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV61WWContratoServicosTelasDS_7_Contratoservicostelas_servicocod2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65WWContratoServicosTelasDS_11_Contratoservicostelas_servicocod3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV66WWContratoServicosTelasDS_12_Tfcontratada_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV67WWContratoServicosTelasDS_13_Tfcontratada_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68WWContratoServicosTelasDS_14_Tfcontratoservicostelas_servicosigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV69WWContratoServicosTelasDS_15_Tfcontratoservicostelas_servicosigla_sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV70WWContratoServicosTelasDS_16_Tfcontratoservicostelas_tela",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWContratoServicosTelasDS_17_Tfcontratoservicostelas_tela_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWContratoServicosTelasDS_18_Tfcontratoservicostelas_link",SqlDbType.VarChar,80,0} ,
          new Object[] {"@AV73WWContratoServicosTelasDS_19_Tfcontratoservicostelas_link_sel",SqlDbType.VarChar,80,0} ,
          new Object[] {"@lV74WWContratoServicosTelasDS_20_Tfcontratoservicostelas_parms",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV75WWContratoServicosTelasDS_21_Tfcontratoservicostelas_parms_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00P92", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P92,100,0,true,false )
             ,new CursorDef("P00P93", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P93,100,0,true,false )
             ,new CursorDef("P00P94", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P94,100,0,true,false )
             ,new CursorDef("P00P95", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P95,100,0,true,false )
             ,new CursorDef("P00P96", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00P96,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 15) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[10])[0] = rslt.getString(8, 50) ;
                ((String[]) buf[11])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((String[]) buf[13])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((int[]) buf[15])[0] = rslt.getInt(11) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(11);
                ((short[]) buf[17])[0] = rslt.getShort(12) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwcontratoservicostelasfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwcontratoservicostelasfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwcontratoservicostelasfilterdata") )
          {
             return  ;
          }
          getwwcontratoservicostelasfilterdata worker = new getwwcontratoservicostelasfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
