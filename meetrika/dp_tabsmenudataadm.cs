/*
               File: DP_TabsMenuDataAdm
        Description: Tabs Menu Data Administrator
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:38.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_tabsmenudataadm : GXProcedure
   {
      public dp_tabsmenudataadm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_tabsmenudataadm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out IGxCollection aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "TabsMenuData.TabsMenuDataItem", "GxEv3Up14_Meetrika", "SdtTabsMenuData_TabsMenuDataItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "TabsMenuData.TabsMenuDataItem", "GxEv3Up14_Meetrika", "SdtTabsMenuData_TabsMenuDataItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out IGxCollection aP0_Gxm2rootcol )
      {
         dp_tabsmenudataadm objdp_tabsmenudataadm;
         objdp_tabsmenudataadm = new dp_tabsmenudataadm();
         objdp_tabsmenudataadm.Gxm2rootcol = new GxObjectCollection( context, "TabsMenuData.TabsMenuDataItem", "GxEv3Up14_Meetrika", "SdtTabsMenuData_TabsMenuDataItem", "GeneXus.Programs") ;
         objdp_tabsmenudataadm.context.SetSubmitInitialConfig(context);
         objdp_tabsmenudataadm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_tabsmenudataadm);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_tabsmenudataadm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( new SdtGAMRepository(context).checkpermission("is_gam_administrator") )
         {
            Gxm1tabsmenudata = new SdtTabsMenuData_TabsMenuDataItem(context);
            Gxm2rootcol.Add(Gxm1tabsmenudata, 0);
            Gxm1tabsmenudata.gxTpr_Menuid = 1;
            Gxm1tabsmenudata.gxTpr_Menutitle = "Administrativo";
            Gxm1tabsmenudata.gxTpr_Menudescription = "Administrativo";
            Gxm3tabsmenudata_sections = new SdtTabsMenuData_TabsMenuDataItem_SectionsItem(context);
            Gxm1tabsmenudata.gxTpr_Sections.Add(Gxm3tabsmenudata_sections, 0);
            Gxm3tabsmenudata_sections.gxTpr_Sectionid = 10;
            Gxm3tabsmenudata_sections.gxTpr_Sectiontitle = "GENEXUS Acess Manager - GAM";
            Gxm3tabsmenudata_sections.gxTpr_Sectiondescription = "GENEXUS Acess Manager - GAM";
            Gxm3tabsmenudata_sections.gxTpr_Sectionurl = "gamexamplewwusers.aspx";
            Gxm3tabsmenudata_sections = new SdtTabsMenuData_TabsMenuDataItem_SectionsItem(context);
            Gxm1tabsmenudata.gxTpr_Sections.Add(Gxm3tabsmenudata_sections, 0);
            Gxm3tabsmenudata_sections.gxTpr_Sectionid = 20;
            Gxm3tabsmenudata_sections.gxTpr_Sectiontitle = "Parámetros do Sistema";
            Gxm3tabsmenudata_sections.gxTpr_Sectiondescription = "Parámetros";
            Gxm3tabsmenudata_sections.gxTpr_Sectionurl = "wwparametrossistema.aspx";
            Gxm3tabsmenudata_sections = new SdtTabsMenuData_TabsMenuDataItem_SectionsItem(context);
            Gxm1tabsmenudata.gxTpr_Sections.Add(Gxm3tabsmenudata_sections, 0);
            Gxm3tabsmenudata_sections.gxTpr_Sectionid = 30;
            Gxm3tabsmenudata_sections.gxTpr_Sectiontitle = "Perfis";
            Gxm3tabsmenudata_sections.gxTpr_Sectiondescription = "Perfis";
            Gxm3tabsmenudata_sections.gxTpr_Sectionurl = "wwperfil.aspx";
            Gxm3tabsmenudata_sections = new SdtTabsMenuData_TabsMenuDataItem_SectionsItem(context);
            Gxm1tabsmenudata.gxTpr_Sections.Add(Gxm3tabsmenudata_sections, 0);
            Gxm3tabsmenudata_sections.gxTpr_Sectionid = 40;
            Gxm3tabsmenudata_sections.gxTpr_Sectiontitle = "Menu";
            Gxm3tabsmenudata_sections.gxTpr_Sectiondescription = "Menu";
            Gxm3tabsmenudata_sections.gxTpr_Sectionurl = "wwmenu.aspx";
            Gxm3tabsmenudata_sections = new SdtTabsMenuData_TabsMenuDataItem_SectionsItem(context);
            Gxm1tabsmenudata.gxTpr_Sections.Add(Gxm3tabsmenudata_sections, 0);
            Gxm3tabsmenudata_sections.gxTpr_Sectionid = 50;
            Gxm3tabsmenudata_sections.gxTpr_Sectiontitle = "About";
            Gxm3tabsmenudata_sections.gxTpr_Sectiondescription = "About";
            Gxm3tabsmenudata_sections.gxTpr_Sectionurl = "about.aspx";
         }
         /* Using cursor P00042 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A285Menu_PaiCod = P00042_A285Menu_PaiCod[0];
            n285Menu_PaiCod = P00042_n285Menu_PaiCod[0];
            A284Menu_Ativo = P00042_A284Menu_Ativo[0];
            A280Menu_Tipo = P00042_A280Menu_Tipo[0];
            A283Menu_Ordem = P00042_A283Menu_Ordem[0];
            A279Menu_Descricao = P00042_A279Menu_Descricao[0];
            n279Menu_Descricao = P00042_n279Menu_Descricao[0];
            A281Menu_Link = P00042_A281Menu_Link[0];
            n281Menu_Link = P00042_n281Menu_Link[0];
            A278Menu_Nome = P00042_A278Menu_Nome[0];
            A277Menu_Codigo = P00042_A277Menu_Codigo[0];
            Gxm1tabsmenudata = new SdtTabsMenuData_TabsMenuDataItem(context);
            Gxm2rootcol.Add(Gxm1tabsmenudata, 0);
            Gxm1tabsmenudata.gxTpr_Menuid = (short)(A277Menu_Codigo);
            AV6Menu_PaiCod = A277Menu_Codigo;
            Gxm1tabsmenudata.gxTpr_Menutitle = A278Menu_Nome;
            Gxm1tabsmenudata.gxTpr_Menudescription = A279Menu_Descricao;
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A281Menu_Link)) )
            {
               Gxm1tabsmenudata.gxTpr_Menuurl = A281Menu_Link;
            }
            Gxm1tabsmenudata.gxTpr_Menuorder = A283Menu_Ordem;
            GXt_objcol_SdtSectionsItem1 = AV9Sections;
            new dp_tabsmenudatasectionsadm(context ).execute(  AV6Menu_PaiCod, out  GXt_objcol_SdtSectionsItem1) ;
            AV9Sections = GXt_objcol_SdtSectionsItem1;
            AV9Sections.Sort("SectionOrder");
            AV18GXV1 = 1;
            while ( AV18GXV1 <= AV9Sections.Count )
            {
               AV8Section = ((SdtSectionsItem)AV9Sections.Item(AV18GXV1));
               Gxm3tabsmenudata_sections = new SdtTabsMenuData_TabsMenuDataItem_SectionsItem(context);
               Gxm1tabsmenudata.gxTpr_Sections.Add(Gxm3tabsmenudata_sections, 0);
               Gxm3tabsmenudata_sections.gxTpr_Sectionid = AV8Section.gxTpr_Sectionid;
               Gxm3tabsmenudata_sections.gxTpr_Sectiontitle = AV8Section.gxTpr_Sectiontitle;
               Gxm3tabsmenudata_sections.gxTpr_Sectiondescription = AV8Section.gxTpr_Sectiondescription;
               Gxm3tabsmenudata_sections.gxTpr_Sectionurl = AV8Section.gxTpr_Sectionurl;
               Gxm3tabsmenudata_sections.gxTpr_Sectionorder = AV8Section.gxTpr_Sectionorder;
               AV18GXV1 = (int)(AV18GXV1+1);
            }
            Gxm3tabsmenudata_sections = new SdtTabsMenuData_TabsMenuDataItem_SectionsItem(context);
            Gxm1tabsmenudata.gxTpr_Sections.Add(Gxm3tabsmenudata_sections, 0);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1tabsmenudata = new SdtTabsMenuData_TabsMenuDataItem(context);
         Gxm3tabsmenudata_sections = new SdtTabsMenuData_TabsMenuDataItem_SectionsItem(context);
         scmdbuf = "";
         P00042_A285Menu_PaiCod = new int[1] ;
         P00042_n285Menu_PaiCod = new bool[] {false} ;
         P00042_A284Menu_Ativo = new bool[] {false} ;
         P00042_A280Menu_Tipo = new short[1] ;
         P00042_A283Menu_Ordem = new short[1] ;
         P00042_A279Menu_Descricao = new String[] {""} ;
         P00042_n279Menu_Descricao = new bool[] {false} ;
         P00042_A281Menu_Link = new String[] {""} ;
         P00042_n281Menu_Link = new bool[] {false} ;
         P00042_A278Menu_Nome = new String[] {""} ;
         P00042_A277Menu_Codigo = new int[1] ;
         A279Menu_Descricao = "";
         A281Menu_Link = "";
         A278Menu_Nome = "";
         AV9Sections = new GxObjectCollection( context, "SectionsItem", "GxEv3Up14_Meetrika", "SdtSectionsItem", "GeneXus.Programs");
         GXt_objcol_SdtSectionsItem1 = new GxObjectCollection( context, "SectionsItem", "GxEv3Up14_Meetrika", "SdtSectionsItem", "GeneXus.Programs");
         AV8Section = new SdtSectionsItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_tabsmenudataadm__default(),
            new Object[][] {
                new Object[] {
               P00042_A285Menu_PaiCod, P00042_n285Menu_PaiCod, P00042_A284Menu_Ativo, P00042_A280Menu_Tipo, P00042_A283Menu_Ordem, P00042_A279Menu_Descricao, P00042_n279Menu_Descricao, P00042_A281Menu_Link, P00042_n281Menu_Link, P00042_A278Menu_Nome,
               P00042_A277Menu_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A280Menu_Tipo ;
      private short A283Menu_Ordem ;
      private int A285Menu_PaiCod ;
      private int A277Menu_Codigo ;
      private int AV6Menu_PaiCod ;
      private int AV18GXV1 ;
      private String scmdbuf ;
      private String A278Menu_Nome ;
      private bool n285Menu_PaiCod ;
      private bool A284Menu_Ativo ;
      private bool n279Menu_Descricao ;
      private bool n281Menu_Link ;
      private String A279Menu_Descricao ;
      private String A281Menu_Link ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00042_A285Menu_PaiCod ;
      private bool[] P00042_n285Menu_PaiCod ;
      private bool[] P00042_A284Menu_Ativo ;
      private short[] P00042_A280Menu_Tipo ;
      private short[] P00042_A283Menu_Ordem ;
      private String[] P00042_A279Menu_Descricao ;
      private bool[] P00042_n279Menu_Descricao ;
      private String[] P00042_A281Menu_Link ;
      private bool[] P00042_n281Menu_Link ;
      private String[] P00042_A278Menu_Nome ;
      private int[] P00042_A277Menu_Codigo ;
      private IGxCollection aP0_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSectionsItem ))]
      private IGxCollection AV9Sections ;
      [ObjectCollection(ItemType=typeof( SdtSectionsItem ))]
      private IGxCollection GXt_objcol_SdtSectionsItem1 ;
      [ObjectCollection(ItemType=typeof( SdtTabsMenuData_TabsMenuDataItem ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSectionsItem AV8Section ;
      private SdtTabsMenuData_TabsMenuDataItem Gxm1tabsmenudata ;
      private SdtTabsMenuData_TabsMenuDataItem_SectionsItem Gxm3tabsmenudata_sections ;
   }

   public class dp_tabsmenudataadm__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00042 ;
          prmP00042 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00042", "SELECT DISTINCT NULL AS [Menu_PaiCod], NULL AS [Menu_Ativo], NULL AS [Menu_Tipo], [Menu_Ordem], [Menu_Descricao], [Menu_Link], [Menu_Nome], [Menu_Codigo] FROM ( SELECT TOP(100) PERCENT [Menu_PaiCod], [Menu_Ativo], [Menu_Tipo], [Menu_Ordem], [Menu_Descricao], [Menu_Link], [Menu_Nome], [Menu_Codigo] FROM [Menu] WITH (NOLOCK) WHERE (([Menu_PaiCod] = convert(int, 0)) or [Menu_PaiCod] IS NULL) AND ([Menu_Tipo] = 1) AND ([Menu_Ativo] = 1) ORDER BY [Menu_Codigo]) DistinctT ORDER BY [Menu_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00042,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 30) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
