function jqSelect()
{
	this.Theme;
	this.SelectedIcon;
	this.UnselectedIcon;
	this.SelectedColor;
	this.SelectingColor;
	this.SelectedTextColor;
	this.SelectingTextColor;
	this.OptionList;
	this.ItemWidth;
	this.ItemHeight;
	this.Width;
	this.Height;
	this.ReadOnly;
	this.Visible;

	// Databinding for property OptionList
	this.SetOptionList = function(data)
	{
		///UserCodeRegionStart:[SetOptionList] (do not remove this comment.)
		this.OptionList = data;

		///UserCodeRegionEnd: (do not remove this comment.)
	}

	// Databinding for property OptionList
	this.GetOptionList = function()
	{
		///UserCodeRegionStart:[GetOptionList] (do not remove this comment.)
		return this.OptionList;
		
		///UserCodeRegionEnd: (do not remove this comment.)
	}

	this.show = function()
	{
		///UserCodeRegionStart:[show] (do not remove this comment.)

		var mthis = this;
		var buffer = '';
		var controlName = 'jqSelect' + this.ContainerName;
		var isVisible = (this.Visible)?'display: inline;':'display: none;';
		var iconSelected = this.SelectedIcon;
		var iconUnselect = this.UnselectedIcon;
		
		var fileName = gx.util.resourceUrl(gx.basePath + gx.staticDirectory + '/jQueryUI/css/' + this.Theme + '/jquery-ui-1.8.16.custom.css',true);
		this.loadjscssfile(fileName,'css');
		
		buffer += '<style>\n';
		buffer += '#' + this.ContainerName +  '{ width:' + this.Width + 'px; height:' + this.Height + 'px; overflow-y: auto; }\n';
		buffer += '#' + controlName +' .ui-selecting { background: ' + this.SelectingColor.Html + '; color: ' + this.SelectingTextColor.Html + '}\n';
		buffer += '#' + controlName +' .ui-selected { background: ' + this.SelectedColor.Html + '; color: ' + this.SelectedTextColor.Html + '; }\n';
		buffer += '#' + controlName +' { list-style-type: none; margin: 0; padding: 0; width: 100%; }\n';
		buffer += '#' + controlName +' li { margin: 2px; padding: 0.3em; font-size: 1.0em; height: ' + this.ItemHeight + 'px; width: ' + this.ItemWidth + 'px; float: left; overflow: hidden;}\n';
		buffer += '#' + controlName +' li .ui-icon { float: left; }\n';
		buffer += '</style>\n';
		
		buffer += '<div class="' + this.Theme + '" style="' + isVisible + '">';
		buffer += '<ol id="' + controlName + '">';
		for (var i=0;this.OptionList[i]!=undefined;i++) {
			buffer += '<li class="ui-widget-content" ';
			buffer += 'id="' + controlName + '_' + this.OptionList[i].Id + '">';
			buffer += '<span class="ui-icon"></span>';
			buffer += this.OptionList[i].Descr;
			buffer += '</li>';
		}
		buffer += '</ol>';
		buffer += '</div>';
		this.setHtml(buffer);		
		
		$(function() {
			$( "#" + controlName ).selectable({
				filter: 'li',
				tolerance: 'touch',
				stop: function() {
					mthis.ShowIcons( "#" + controlName, iconSelected, iconUnselect);
					$( "#" + controlName + " li" ).each( function(index) {
						var liId = this.id.substring( controlName.length + 1);
						for (var i=0;mthis.OptionList[i]!=undefined;i++) {
							if (liId==mthis.OptionList[i].Id) {
								mthis.OptionList[i].Selected = $(this).hasClass('ui-selected');
							};
						};
					});
					if (mthis.Click) {
						mthis.Click();
					}
				}
			});
			if (mthis.Rounding>"")
				$( "#" + controlName + " li" ).corner(mthis.Rounding);
			$( "#" + controlName + " li" ).each( function(index) {
				var liId = this.id.substring( controlName.length + 1);
				for (var i=0;mthis.OptionList[i]!=undefined;i++) {
					if (liId==mthis.OptionList[i].Id && mthis.OptionList[i].Selected==true) {
						$(this).addClass('ui-selected');
					};
				};
			});
			mthis.ShowIcons( "#" + controlName, iconSelected, iconUnselect);
		});	
			
		///UserCodeRegionEnd: (do not remove this comment.)
	}
	///UserCodeRegionStart:[User Functions] (do not remove this comment.)

	this.loadjscssfile = function (filename, filetype){
 		if (filetype=="js"){ //if filename is a external JavaScript file
			var fileref=document.createElement('script');
			fileref.setAttribute("type","text/javascript");
			fileref.setAttribute("src", filename);
		}
		else if (filetype=="css"){ //if filename is an external CSS file
			var fileref=document.createElement("link");
			fileref.setAttribute("rel", "stylesheet");
			fileref.setAttribute("type", "text/css");
			fileref.setAttribute("href", filename);
		}
		document.getElementsByTagName("head")[0].appendChild(fileref);
 	}
	
	this.ShowIcons = function( controlName, iconSelected, iconUnselect ) {
		$( controlName + " li" ).each(function(index) {
			$(this).children('span').removeClass( iconSelected );
			$(this).children('span').removeClass( iconUnselect );
			if ($(this).hasClass('ui-selected')) {
				//icone selecionado
				$(this).children('span').addClass( iconSelected );
			} else {
				//icone n�o selecionado
				$(this).children('span').addClass( iconUnselect );
			};
		});
	}
	
	///UserCodeRegionEnd: (do not remove this comment.):
}
