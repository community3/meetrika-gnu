/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:19:13.34
*/
gx.evt.autoSkip = false;
gx.define('usuario', false, function () {
   this.ServerClass =  "usuario" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("trn");
   this.hasEnterEvent = true;
   this.skipOnEnter = false;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV7Usuario_Codigo=gx.fn.getIntegerValue("vUSUARIO_CODIGO",'.') ;
      this.AV25Insert_Usuario_CargoCod=gx.fn.getIntegerValue("vINSERT_USUARIO_CARGOCOD",'.') ;
      this.A1073Usuario_CargoCod=gx.fn.getIntegerValue("USUARIO_CARGOCOD",'.') ;
      this.AV14Insert_Usuario_PessoaCod=gx.fn.getIntegerValue("vINSERT_USUARIO_PESSOACOD",'.') ;
      this.A1075Usuario_CargoUOCod=gx.fn.getIntegerValue("USUARIO_CARGOUOCOD",'.') ;
      this.Gx_BScreen=gx.fn.getIntegerValue("vGXBSCREEN",'.') ;
      this.A538Usuario_EhGestor=gx.fn.getControlValue("USUARIO_EHGESTOR") ;
      this.AV28AuditingObject=gx.fn.getControlValue("vAUDITINGOBJECT") ;
      this.A2Usuario_Nome=gx.fn.getControlValue("USUARIO_NOME") ;
      this.A40000Usuario_Foto_GXI=gx.fn.getControlValue("USUARIO_FOTO_GXI") ;
      this.A2016Usuario_UltimaArea=gx.fn.getIntegerValue("USUARIO_ULTIMAAREA",'.') ;
      this.A1076Usuario_CargoUONom=gx.fn.getControlValue("USUARIO_CARGOUONOM") ;
      this.AV32Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.Gx_mode=gx.fn.getControlValue("vMODE") ;
      this.AV9TrnContext=gx.fn.getControlValue("vTRNCONTEXT") ;
      this.AV8WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
   };
   this.Valid_Usuario_codigo=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Usuario_codigo",["gx.O.A1Usuario_Codigo", "gx.O.A1083Usuario_Entidade", "gx.O.A290Usuario_EhAuditorFM"],["A1083Usuario_Entidade", "A290Usuario_EhAuditorFM"]);
      return true;
   }
   this.Validv_Email=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vEMAIL");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Usuario_ehcontratada=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("USUARIO_EHCONTRATADA");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Usuario_ehcontratante=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("USUARIO_EHCONTRATANTE");
         this.AnyError  = 0;
         if ( this.A292Usuario_EhContratante && this.A291Usuario_EhContratada )
         {
            try {
               gxballoon.setError("Não pode ser Contratante e Contratada ao mesmo tempo!");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Usuario_email=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("USUARIO_EMAIL");
         this.AnyError  = 0;
         if ( ! ( gx.util.regExp.isMatch(this.A1647Usuario_Email, "^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") || ((''==this.A1647Usuario_Email)) ) )
         {
            try {
               gxballoon.setError("O valor de Usuario_Email não coincide com o padrão especificado");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Usuario_pessoacod=function()
   {
      gx.ajax.validSrvEvt("dyncall","Valid_Usuario_pessoacod",["gx.O.A57Usuario_PessoaCod", "gx.O.A58Usuario_PessoaNom", "gx.O.A59Usuario_PessoaTip", "gx.O.A325Usuario_PessoaDoc", "gx.O.A2095Usuario_PessoaTelefone"],["A58Usuario_PessoaNom", "A59Usuario_PessoaTip", "A325Usuario_PessoaDoc", "A2095Usuario_PessoaTelefone"]);
      return true;
   }
   this.s112_client=function()
   {
      gx.fn.setCtrlProperty("vCONTRATANTEUSUARIO_EHFISCAL","Visible", false );
      gx.fn.setCtrlProperty("CONTRATANTEUSUARIO_EHFISCAL_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKCONTRATANTEUSUARIO_EHFISCAL_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("vCONTRATADAUSUARIO_CSTUNTPRDNRM","Visible", false );
      gx.fn.setCtrlProperty("CONTRATADAUSUARIO_CSTUNTPRDNRM_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDNRM_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("vCONTRATADAUSUARIO_CSTUNTPRDEXT","Visible", false );
      gx.fn.setCtrlProperty("CONTRATADAUSUARIO_CSTUNTPRDEXT_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDEXT_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("USUARIO_USERGAMGUID","Visible", false );
      gx.fn.setCtrlProperty("USUARIO_USERGAMGUID_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKUSUARIO_USERGAMGUID_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("USUARIO_EHCONTADOR","Visible", false );
      gx.fn.setCtrlProperty("USUARIO_EHCONTADOR_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKUSUARIO_EHCONTADOR_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("USUARIO_EHAUDITORFM","Visible", false );
      gx.fn.setCtrlProperty("USUARIO_EHAUDITORFM_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKUSUARIO_EHAUDITORFM_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("USUARIO_EHFINANCEIRO","Visible", false );
      gx.fn.setCtrlProperty("USUARIO_EHFINANCEIRO_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKUSUARIO_EHFINANCEIRO_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("USUARIO_EHCONTRATADA","Visible", false );
      gx.fn.setCtrlProperty("USUARIO_EHCONTRATADA_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKUSUARIO_EHCONTRATADA_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("USUARIO_EHCONTRATANTE","Visible", false );
      gx.fn.setCtrlProperty("USUARIO_EHCONTRATANTE_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKUSUARIO_EHCONTRATANTE_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("USUARIO_EHPREPOSTO","Visible", false );
      gx.fn.setCtrlProperty("USUARIO_EHPREPOSTO_CELL","Class", "Invisible" );
      gx.fn.setCtrlProperty("TEXTBLOCKUSUARIO_EHPREPOSTO_CELL","Class", "Invisible" );
   };
   this.e11011_client=function()
   {
      this.clearMessages();
      gx.popup.openUrl(gx.http.formatLink("wp_selecionauo.aspx",[this.AV26UnidadeOrganizacional_Codigo]), ["AV26UnidadeOrganizacional_Codigo"]);
      this.refreshOutputs([{av:'AV26UnidadeOrganizacional_Codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0}]);
   };
   this.e13012_client=function()
   {
      this.executeServerEvent("AFTER TRN", true, null, false, false);
   };
   this.e14011_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e15011_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,10,18,21,23,26,28,30,32,34,36,39,41,44,46,48,50,55,57,59,61,66,68,70,72,74,76,79,81,83,85,87,89,92,94,96,98,100,102,105,107,109,111,113,115,118,120,122,124,126,128,141,143,150,158,159,160,161];
   this.GXLastCtrlId =161;
   this.DVPANEL_TABLEATTRIBUTESContainer = gx.uc.getNew(this, 16, 0, "BootstrapPanel", "DVPANEL_TABLEATTRIBUTESContainer", "Dvpanel_tableattributes");
   var DVPANEL_TABLEATTRIBUTESContainer = this.DVPANEL_TABLEATTRIBUTESContainer;
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Width", "Width", "100%", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Height", "Height", "100", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoWidth", "Autowidth", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoHeight", "Autoheight", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Cls", "Cls", "GXUI-DVelop-Panel", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowHeader", "Showheader", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Title", "Title", "Usuário", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsible", "Collapsible", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Collapsed", "Collapsed", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("ShowCollapseIcon", "Showcollapseicon", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("IconPosition", "Iconposition", "left", "str");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("AutoScroll", "Autoscroll", false, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Visible", "Visible", true, "bool");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Enabled", "Enabled", true, "boolean");
   DVPANEL_TABLEATTRIBUTESContainer.setProp("Class", "Class", "", "char");
   DVPANEL_TABLEATTRIBUTESContainer.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(DVPANEL_TABLEATTRIBUTESContainer);
   GXValidFnc[2]={fld:"TABLEMAIN",grid:0};
   GXValidFnc[5]={fld:"TABLETITLE",grid:0};
   GXValidFnc[10]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[18]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[21]={fld:"TEXTBLOCKUNIDADEORGANIZACIONAL_CODIGO", format:0,grid:0};
   GXValidFnc[23]={fld:"TABLEMERGEDUNIDADEORGANIZACIONAL_CODIGO",grid:0};
   GXValidFnc[26]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vUNIDADEORGANIZACIONAL_CODIGO",gxz:"ZV26UnidadeOrganizacional_Codigo",gxold:"OV26UnidadeOrganizacional_Codigo",gxvar:"AV26UnidadeOrganizacional_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"dyncombo",v2v:function(Value){if(Value!==undefined)gx.O.AV26UnidadeOrganizacional_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV26UnidadeOrganizacional_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("vUNIDADEORGANIZACIONAL_CODIGO",gx.O.AV26UnidadeOrganizacional_Codigo)},c2v:function(){if(this.val()!==undefined)gx.O.AV26UnidadeOrganizacional_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vUNIDADEORGANIZACIONAL_CODIGO",'.')},nac:gx.falseFn};
   GXValidFnc[28]={fld:"SELECTUO",grid:0};
   GXValidFnc[30]={fld:"TEXTBLOCKUSUARIO_CARGONOM", format:0,grid:0};
   GXValidFnc[32]={lvl:0,type:"svchar",len:80,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_CARGONOM",gxz:"Z1074Usuario_CargoNom",gxold:"O1074Usuario_CargoNom",gxvar:"A1074Usuario_CargoNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1074Usuario_CargoNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1074Usuario_CargoNom=Value},v2c:function(){gx.fn.setControlValue("USUARIO_CARGONOM",gx.O.A1074Usuario_CargoNom,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1074Usuario_CargoNom=this.val()},val:function(){return gx.fn.getControlValue("USUARIO_CARGONOM")},nac:gx.falseFn};
   GXValidFnc[34]={fld:"TEXTBLOCKUSUARIO_ENTIDADE", format:0,grid:0};
   GXValidFnc[36]={lvl:0,type:"char",len:50,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_ENTIDADE",gxz:"Z1083Usuario_Entidade",gxold:"O1083Usuario_Entidade",gxvar:"A1083Usuario_Entidade",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1083Usuario_Entidade=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1083Usuario_Entidade=Value},v2c:function(){gx.fn.setControlValue("USUARIO_ENTIDADE",gx.O.A1083Usuario_Entidade,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1083Usuario_Entidade=this.val()},val:function(){return gx.fn.getControlValue("USUARIO_ENTIDADE")},nac:gx.falseFn};
   this.declareDomainHdlr( 36 , function() {
   });
   GXValidFnc[39]={fld:"TEXTBLOCKUSUARIO_PESSOANOM", format:0,grid:0};
   GXValidFnc[41]={lvl:0,type:"char",len:100,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_PESSOANOM",gxz:"Z58Usuario_PessoaNom",gxold:"O58Usuario_PessoaNom",gxvar:"A58Usuario_PessoaNom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A58Usuario_PessoaNom=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z58Usuario_PessoaNom=Value},v2c:function(){gx.fn.setControlValue("USUARIO_PESSOANOM",gx.O.A58Usuario_PessoaNom,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A58Usuario_PessoaNom=this.val()},val:function(){return gx.fn.getControlValue("USUARIO_PESSOANOM")},nac:gx.falseFn};
   this.declareDomainHdlr( 41 , function() {
   });
   GXValidFnc[44]={fld:"TEXTBLOCKUSUARIO_PESSOATIP", format:0,grid:0};
   GXValidFnc[46]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_PESSOATIP",gxz:"Z59Usuario_PessoaTip",gxold:"O59Usuario_PessoaTip",gxvar:"A59Usuario_PessoaTip",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A59Usuario_PessoaTip=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z59Usuario_PessoaTip=Value},v2c:function(){gx.fn.setComboBoxValue("USUARIO_PESSOATIP",gx.O.A59Usuario_PessoaTip);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A59Usuario_PessoaTip=this.val()},val:function(){return gx.fn.getControlValue("USUARIO_PESSOATIP")},nac:gx.falseFn};
   this.declareDomainHdlr( 46 , function() {
   });
   GXValidFnc[48]={fld:"TEXTBLOCKUSUARIO_PESSOADOC", format:0,grid:0};
   GXValidFnc[50]={lvl:0,type:"svchar",len:15,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_PESSOADOC",gxz:"Z325Usuario_PessoaDoc",gxold:"O325Usuario_PessoaDoc",gxvar:"A325Usuario_PessoaDoc",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A325Usuario_PessoaDoc=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z325Usuario_PessoaDoc=Value},v2c:function(){gx.fn.setControlValue("USUARIO_PESSOADOC",gx.O.A325Usuario_PessoaDoc,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A325Usuario_PessoaDoc=this.val()},val:function(){return gx.fn.getControlValue("USUARIO_PESSOADOC")},nac:gx.falseFn};
   this.declareDomainHdlr( 50 , function() {
   });
   GXValidFnc[55]={fld:"TEXTBLOCKEMAIL", format:0,grid:0};
   GXValidFnc[57]={lvl:0,type:"svchar",len:50,dec:0,sign:false,pic:"@!",ro:0,grid:0,gxgrid:null,fnc:this.Validv_Email,isvalid:null,rgrid:[],fld:"vEMAIL",gxz:"ZV18Email",gxold:"OV18Email",gxvar:"AV18Email",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV18Email=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV18Email=Value},v2c:function(){gx.fn.setControlValue("vEMAIL",gx.O.AV18Email,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV18Email=this.val()},val:function(){return gx.fn.getControlValue("vEMAIL")},nac:gx.falseFn};
   this.declareDomainHdlr( 57 , function() {
   });
   GXValidFnc[59]={fld:"TEXTBLOCKCONTRATANTEUSUARIO_EHFISCAL", format:0,grid:0};
   GXValidFnc[61]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTRATANTEUSUARIO_EHFISCAL",gxz:"ZV29ContratanteUsuario_EhFiscal",gxold:"OV29ContratanteUsuario_EhFiscal",gxvar:"AV29ContratanteUsuario_EhFiscal",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV29ContratanteUsuario_EhFiscal=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV29ContratanteUsuario_EhFiscal=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vCONTRATANTEUSUARIO_EHFISCAL",gx.O.AV29ContratanteUsuario_EhFiscal,true)},c2v:function(){if(this.val()!==undefined)gx.O.AV29ContratanteUsuario_EhFiscal=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vCONTRATANTEUSUARIO_EHFISCAL")},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[66]={fld:"TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDNRM", format:0,grid:0};
   GXValidFnc[68]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTRATADAUSUARIO_CSTUNTPRDNRM",gxz:"ZV20ContratadaUsuario_CstUntPrdNrm",gxold:"OV20ContratadaUsuario_CstUntPrdNrm",gxvar:"AV20ContratadaUsuario_CstUntPrdNrm",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV20ContratadaUsuario_CstUntPrdNrm=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV20ContratadaUsuario_CstUntPrdNrm=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vCONTRATADAUSUARIO_CSTUNTPRDNRM",gx.O.AV20ContratadaUsuario_CstUntPrdNrm,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV20ContratadaUsuario_CstUntPrdNrm=this.val()},val:function(){return gx.fn.getDecimalValue("vCONTRATADAUSUARIO_CSTUNTPRDNRM",'.',',')},nac:gx.falseFn};
   GXValidFnc[70]={fld:"TEXTBLOCKCONTRATADAUSUARIO_CSTUNTPRDEXT", format:0,grid:0};
   GXValidFnc[72]={lvl:0,type:"decimal",len:18,dec:5,sign:true,pic:"ZZZ,ZZZ,ZZZ,ZZ9.99",ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vCONTRATADAUSUARIO_CSTUNTPRDEXT",gxz:"ZV21ContratadaUsuario_CstUntPrdExt",gxold:"OV21ContratadaUsuario_CstUntPrdExt",gxvar:"AV21ContratadaUsuario_CstUntPrdExt",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV21ContratadaUsuario_CstUntPrdExt=gx.fn.toDecimalValue(Value,',','.')},v2z:function(Value){if(Value!==undefined)gx.O.ZV21ContratadaUsuario_CstUntPrdExt=gx.fn.toDecimalValue(Value,'.',',')},v2c:function(){gx.fn.setDecimalValue("vCONTRATADAUSUARIO_CSTUNTPRDEXT",gx.O.AV21ContratadaUsuario_CstUntPrdExt,5,',')},c2v:function(){if(this.val()!==undefined)gx.O.AV21ContratadaUsuario_CstUntPrdExt=this.val()},val:function(){return gx.fn.getDecimalValue("vCONTRATADAUSUARIO_CSTUNTPRDEXT",'.',',')},nac:gx.falseFn};
   GXValidFnc[74]={fld:"TEXTBLOCKUSUARIO_USERGAMGUID", format:0,grid:0};
   GXValidFnc[76]={lvl:0,type:"char",len:40,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_USERGAMGUID",gxz:"Z341Usuario_UserGamGuid",gxold:"O341Usuario_UserGamGuid",gxvar:"A341Usuario_UserGamGuid",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A341Usuario_UserGamGuid=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z341Usuario_UserGamGuid=Value},v2c:function(){gx.fn.setControlValue("USUARIO_USERGAMGUID",gx.O.A341Usuario_UserGamGuid,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A341Usuario_UserGamGuid=this.val()},val:function(){return gx.fn.getControlValue("USUARIO_USERGAMGUID")},nac:gx.falseFn};
   this.declareDomainHdlr( 76 , function() {
   });
   GXValidFnc[79]={fld:"TEXTBLOCKUSUARIO_EHCONTADOR", format:0,grid:0};
   GXValidFnc[81]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_EHCONTADOR",gxz:"Z289Usuario_EhContador",gxold:"O289Usuario_EhContador",gxvar:"A289Usuario_EhContador",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A289Usuario_EhContador=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z289Usuario_EhContador=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("USUARIO_EHCONTADOR",gx.O.A289Usuario_EhContador,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A289Usuario_EhContador=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("USUARIO_EHCONTADOR")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 81 , function() {
   });
   GXValidFnc[83]={fld:"TEXTBLOCKUSUARIO_EHAUDITORFM", format:0,grid:0};
   GXValidFnc[85]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_EHAUDITORFM",gxz:"Z290Usuario_EhAuditorFM",gxold:"O290Usuario_EhAuditorFM",gxvar:"A290Usuario_EhAuditorFM",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A290Usuario_EhAuditorFM=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z290Usuario_EhAuditorFM=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("USUARIO_EHAUDITORFM",gx.O.A290Usuario_EhAuditorFM,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A290Usuario_EhAuditorFM=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("USUARIO_EHAUDITORFM")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 85 , function() {
   });
   GXValidFnc[87]={fld:"TEXTBLOCKUSUARIO_EHFINANCEIRO", format:0,grid:0};
   GXValidFnc[89]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_EHFINANCEIRO",gxz:"Z293Usuario_EhFinanceiro",gxold:"O293Usuario_EhFinanceiro",gxvar:"A293Usuario_EhFinanceiro",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A293Usuario_EhFinanceiro=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z293Usuario_EhFinanceiro=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("USUARIO_EHFINANCEIRO",gx.O.A293Usuario_EhFinanceiro,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A293Usuario_EhFinanceiro=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("USUARIO_EHFINANCEIRO")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 89 , function() {
   });
   GXValidFnc[92]={fld:"TEXTBLOCKUSUARIO_EHCONTRATADA", format:0,grid:0};
   GXValidFnc[94]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Usuario_ehcontratada,isvalid:null,rgrid:[],fld:"USUARIO_EHCONTRATADA",gxz:"Z291Usuario_EhContratada",gxold:"O291Usuario_EhContratada",gxvar:"A291Usuario_EhContratada",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A291Usuario_EhContratada=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z291Usuario_EhContratada=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("USUARIO_EHCONTRATADA",gx.O.A291Usuario_EhContratada,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A291Usuario_EhContratada=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("USUARIO_EHCONTRATADA")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 94 , function() {
   });
   GXValidFnc[96]={fld:"TEXTBLOCKUSUARIO_EHCONTRATANTE", format:0,grid:0};
   GXValidFnc[98]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Usuario_ehcontratante,isvalid:null,rgrid:[],fld:"USUARIO_EHCONTRATANTE",gxz:"Z292Usuario_EhContratante",gxold:"O292Usuario_EhContratante",gxvar:"A292Usuario_EhContratante",ucs:[],op:[94,98],ip:[94,98],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A292Usuario_EhContratante=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z292Usuario_EhContratante=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("USUARIO_EHCONTRATANTE",gx.O.A292Usuario_EhContratante,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A292Usuario_EhContratante=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("USUARIO_EHCONTRATANTE")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 98 , function() {
   });
   GXValidFnc[100]={fld:"TEXTBLOCKUSUARIO_EHPREPOSTO", format:0,grid:0};
   GXValidFnc[102]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_EHPREPOSTO",gxz:"Z1093Usuario_EhPreposto",gxold:"O1093Usuario_EhPreposto",gxvar:"A1093Usuario_EhPreposto",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A1093Usuario_EhPreposto=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1093Usuario_EhPreposto=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("USUARIO_EHPREPOSTO",gx.O.A1093Usuario_EhPreposto,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1093Usuario_EhPreposto=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("USUARIO_EHPREPOSTO")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 102 , function() {
   });
   GXValidFnc[105]={fld:"TEXTBLOCKUSUARIO_NOTIFICAR", format:0,grid:0};
   GXValidFnc[107]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_NOTIFICAR",gxz:"Z1235Usuario_Notificar",gxold:"O1235Usuario_Notificar",gxvar:"A1235Usuario_Notificar",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1235Usuario_Notificar=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1235Usuario_Notificar=Value},v2c:function(){gx.fn.setComboBoxValue("USUARIO_NOTIFICAR",gx.O.A1235Usuario_Notificar)},c2v:function(){if(this.val()!==undefined)gx.O.A1235Usuario_Notificar=this.val()},val:function(){return gx.fn.getControlValue("USUARIO_NOTIFICAR")},nac:gx.falseFn};
   GXValidFnc[109]={fld:"TEXTBLOCKUSUARIO_EMAIL", format:0,grid:0};
   GXValidFnc[111]={lvl:0,type:"svchar",len:100,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Valid_Usuario_email,isvalid:null,rgrid:[],fld:"USUARIO_EMAIL",gxz:"Z1647Usuario_Email",gxold:"O1647Usuario_Email",gxvar:"A1647Usuario_Email",ucs:[],op:[111],ip:[111],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1647Usuario_Email=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1647Usuario_Email=Value},v2c:function(){gx.fn.setControlValue("USUARIO_EMAIL",gx.O.A1647Usuario_Email,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1647Usuario_Email=this.val()},val:function(){return gx.fn.getControlValue("USUARIO_EMAIL")},nac:gx.falseFn};
   this.declareDomainHdlr( 111 , function() {
      gx.fn.setCtrlProperty("USUARIO_EMAIL","Link", (!gx.fn.getCtrlProperty("USUARIO_EMAIL","Enabled") ? "mailto:"+this.A1647Usuario_Email : "") );
   });
   GXValidFnc[113]={fld:"TEXTBLOCKUSUARIO_PESSOATELEFONE", format:0,grid:0};
   GXValidFnc[115]={lvl:0,type:"char",len:15,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_PESSOATELEFONE",gxz:"Z2095Usuario_PessoaTelefone",gxold:"O2095Usuario_PessoaTelefone",gxvar:"A2095Usuario_PessoaTelefone",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A2095Usuario_PessoaTelefone=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z2095Usuario_PessoaTelefone=Value},v2c:function(){gx.fn.setControlValue("USUARIO_PESSOATELEFONE",gx.O.A2095Usuario_PessoaTelefone,0)},c2v:function(){if(this.val()!==undefined)gx.O.A2095Usuario_PessoaTelefone=this.val()},val:function(){return gx.fn.getControlValue("USUARIO_PESSOATELEFONE")},nac:gx.falseFn};
   GXValidFnc[118]={fld:"TEXTBLOCKUSUARIO_FOTO", format:0,grid:0};
   GXValidFnc[120]={lvl:0,type:"bits",len:1024,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_FOTO",gxz:"Z1716Usuario_Foto",gxold:"O1716Usuario_Foto",gxvar:"A1716Usuario_Foto",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1716Usuario_Foto=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1716Usuario_Foto=Value},v2c:function(){gx.fn.setMultimediaValue("USUARIO_FOTO",gx.O.A1716Usuario_Foto,gx.O.A40000Usuario_Foto_GXI)},c2v:function(){gx.O.A40000Usuario_Foto_GXI=this.val_GXI();if(this.val()!==undefined)gx.O.A1716Usuario_Foto=this.val()},val:function(){return gx.fn.getBlobValue("USUARIO_FOTO")},val_GXI:function(){return gx.fn.getControlValue("USUARIO_FOTO_GXI")}, gxvar_GXI:'A40000Usuario_Foto_GXI',nac:gx.falseFn};
   GXValidFnc[122]={fld:"TEXTBLOCKUSUARIO_CRTFPATH", format:0,grid:0};
   GXValidFnc[124]={lvl:0,type:"svchar",len:100,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_CRTFPATH",gxz:"Z1017Usuario_CrtfPath",gxold:"O1017Usuario_CrtfPath",gxvar:"A1017Usuario_CrtfPath",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1017Usuario_CrtfPath=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1017Usuario_CrtfPath=Value},v2c:function(){gx.fn.setControlValue("USUARIO_CRTFPATH",gx.O.A1017Usuario_CrtfPath,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1017Usuario_CrtfPath=this.val()},val:function(){return gx.fn.getControlValue("USUARIO_CRTFPATH")},nac:gx.falseFn};
   GXValidFnc[126]={fld:"TEXTBLOCKUSUARIO_DEFERIAS", format:0,grid:0};
   GXValidFnc[128]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_DEFERIAS",gxz:"Z1908Usuario_DeFerias",gxold:"O1908Usuario_DeFerias",gxvar:"A1908Usuario_DeFerias",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A1908Usuario_DeFerias=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1908Usuario_DeFerias=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("USUARIO_DEFERIAS",gx.O.A1908Usuario_DeFerias,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1908Usuario_DeFerias=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("USUARIO_DEFERIAS")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 128 , function() {
   });
   GXValidFnc[141]={fld:"TEXTBLOCKUSUARIO_ATIVO", format:0,grid:0};
   GXValidFnc[143]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"USUARIO_ATIVO",gxz:"Z54Usuario_Ativo",gxold:"O54Usuario_Ativo",gxvar:"A54Usuario_Ativo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A54Usuario_Ativo=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z54Usuario_Ativo=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("USUARIO_ATIVO",gx.O.A54Usuario_Ativo,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A54Usuario_Ativo=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("USUARIO_ATIVO")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 143 , function() {
   });
   GXValidFnc[150]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[158]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vEHCONTRATADA_FLAG",gxz:"ZV23EhContratada_Flag",gxold:"OV23EhContratada_Flag",gxvar:"AV23EhContratada_Flag",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV23EhContratada_Flag=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV23EhContratada_Flag=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vEHCONTRATADA_FLAG",gx.O.AV23EhContratada_Flag,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV23EhContratada_Flag=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vEHCONTRATADA_FLAG")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 158 , function() {
   });
   GXValidFnc[159]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vENTIDADE_CODIGO",gxz:"ZV24Entidade_Codigo",gxold:"OV24Entidade_Codigo",gxvar:"AV24Entidade_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV24Entidade_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV24Entidade_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("vENTIDADE_CODIGO",gx.O.AV24Entidade_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV24Entidade_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("vENTIDADE_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 159 , function() {
   });
   GXValidFnc[160]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Usuario_codigo,isvalid:null,rgrid:[],fld:"USUARIO_CODIGO",gxz:"Z1Usuario_Codigo",gxold:"O1Usuario_Codigo",gxvar:"A1Usuario_Codigo",ucs:[],op:[85,36],ip:[85,36,160],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A1Usuario_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1Usuario_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("USUARIO_CODIGO",gx.O.A1Usuario_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1Usuario_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("USUARIO_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 160 , function() {
   });
   GXValidFnc[161]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Usuario_pessoacod,isvalid:null,rgrid:[],fld:"USUARIO_PESSOACOD",gxz:"Z57Usuario_PessoaCod",gxold:"O57Usuario_PessoaCod",gxvar:"A57Usuario_PessoaCod",ucs:[],op:[115,50,46,41],ip:[115,50,46,41,161],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A57Usuario_PessoaCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z57Usuario_PessoaCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("USUARIO_PESSOACOD",gx.O.A57Usuario_PessoaCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A57Usuario_PessoaCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("USUARIO_PESSOACOD",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 161 , function() {
   });
   this.AV26UnidadeOrganizacional_Codigo = 0 ;
   this.ZV26UnidadeOrganizacional_Codigo = 0 ;
   this.OV26UnidadeOrganizacional_Codigo = 0 ;
   this.A1074Usuario_CargoNom = "" ;
   this.Z1074Usuario_CargoNom = "" ;
   this.O1074Usuario_CargoNom = "" ;
   this.A1083Usuario_Entidade = "" ;
   this.Z1083Usuario_Entidade = "" ;
   this.O1083Usuario_Entidade = "" ;
   this.A58Usuario_PessoaNom = "" ;
   this.Z58Usuario_PessoaNom = "" ;
   this.O58Usuario_PessoaNom = "" ;
   this.A59Usuario_PessoaTip = "" ;
   this.Z59Usuario_PessoaTip = "" ;
   this.O59Usuario_PessoaTip = "" ;
   this.A325Usuario_PessoaDoc = "" ;
   this.Z325Usuario_PessoaDoc = "" ;
   this.O325Usuario_PessoaDoc = "" ;
   this.AV18Email = "" ;
   this.ZV18Email = "" ;
   this.OV18Email = "" ;
   this.AV29ContratanteUsuario_EhFiscal = false ;
   this.ZV29ContratanteUsuario_EhFiscal = false ;
   this.OV29ContratanteUsuario_EhFiscal = false ;
   this.AV20ContratadaUsuario_CstUntPrdNrm = 0 ;
   this.ZV20ContratadaUsuario_CstUntPrdNrm = 0 ;
   this.OV20ContratadaUsuario_CstUntPrdNrm = 0 ;
   this.AV21ContratadaUsuario_CstUntPrdExt = 0 ;
   this.ZV21ContratadaUsuario_CstUntPrdExt = 0 ;
   this.OV21ContratadaUsuario_CstUntPrdExt = 0 ;
   this.A341Usuario_UserGamGuid = "" ;
   this.Z341Usuario_UserGamGuid = "" ;
   this.O341Usuario_UserGamGuid = "" ;
   this.A289Usuario_EhContador = false ;
   this.Z289Usuario_EhContador = false ;
   this.O289Usuario_EhContador = false ;
   this.A290Usuario_EhAuditorFM = false ;
   this.Z290Usuario_EhAuditorFM = false ;
   this.O290Usuario_EhAuditorFM = false ;
   this.A293Usuario_EhFinanceiro = false ;
   this.Z293Usuario_EhFinanceiro = false ;
   this.O293Usuario_EhFinanceiro = false ;
   this.A291Usuario_EhContratada = false ;
   this.Z291Usuario_EhContratada = false ;
   this.O291Usuario_EhContratada = false ;
   this.A292Usuario_EhContratante = false ;
   this.Z292Usuario_EhContratante = false ;
   this.O292Usuario_EhContratante = false ;
   this.A1093Usuario_EhPreposto = false ;
   this.Z1093Usuario_EhPreposto = false ;
   this.O1093Usuario_EhPreposto = false ;
   this.A1235Usuario_Notificar = "" ;
   this.Z1235Usuario_Notificar = "" ;
   this.O1235Usuario_Notificar = "" ;
   this.A1647Usuario_Email = "" ;
   this.Z1647Usuario_Email = "" ;
   this.O1647Usuario_Email = "" ;
   this.A2095Usuario_PessoaTelefone = "" ;
   this.Z2095Usuario_PessoaTelefone = "" ;
   this.O2095Usuario_PessoaTelefone = "" ;
   this.A40000Usuario_Foto_GXI = "" ;
   this.A1716Usuario_Foto = "" ;
   this.Z1716Usuario_Foto = "" ;
   this.O1716Usuario_Foto = "" ;
   this.A1017Usuario_CrtfPath = "" ;
   this.Z1017Usuario_CrtfPath = "" ;
   this.O1017Usuario_CrtfPath = "" ;
   this.A1908Usuario_DeFerias = false ;
   this.Z1908Usuario_DeFerias = false ;
   this.O1908Usuario_DeFerias = false ;
   this.A54Usuario_Ativo = false ;
   this.Z54Usuario_Ativo = false ;
   this.O54Usuario_Ativo = false ;
   this.AV23EhContratada_Flag = false ;
   this.ZV23EhContratada_Flag = false ;
   this.OV23EhContratada_Flag = false ;
   this.AV24Entidade_Codigo = 0 ;
   this.ZV24Entidade_Codigo = 0 ;
   this.OV24Entidade_Codigo = 0 ;
   this.A1Usuario_Codigo = 0 ;
   this.Z1Usuario_Codigo = 0 ;
   this.O1Usuario_Codigo = 0 ;
   this.A57Usuario_PessoaCod = 0 ;
   this.Z57Usuario_PessoaCod = 0 ;
   this.O57Usuario_PessoaCod = 0 ;
   this.A40000Usuario_Foto_GXI = "" ;
   this.AV8WWPContext = {} ;
   this.AV9TrnContext = {} ;
   this.AV33GXV1 = 0 ;
   this.AV25Insert_Usuario_CargoCod = 0 ;
   this.AV14Insert_Usuario_PessoaCod = 0 ;
   this.AV27Usuario_UserGamGuid = "" ;
   this.AV15GamUser = {} ;
   this.Gx_msg = "" ;
   this.AV23EhContratada_Flag = false ;
   this.AV24Entidade_Codigo = 0 ;
   this.AV18Email = "" ;
   this.AV22ContratadaUsuario = {} ;
   this.AV20ContratadaUsuario_CstUntPrdNrm = 0 ;
   this.AV21ContratadaUsuario_CstUntPrdExt = 0 ;
   this.AV7Usuario_Codigo = 0 ;
   this.AV29ContratanteUsuario_EhFiscal = false ;
   this.AV12TrnContextAtt = {} ;
   this.AV10WebSession = {} ;
   this.A1Usuario_Codigo = 0 ;
   this.A1073Usuario_CargoCod = 0 ;
   this.A57Usuario_PessoaCod = 0 ;
   this.AV26UnidadeOrganizacional_Codigo = 0 ;
   this.AV28AuditingObject = {} ;
   this.Gx_BScreen = 0 ;
   this.AV32Pgmname = "" ;
   this.A290Usuario_EhAuditorFM = false ;
   this.A1083Usuario_Entidade = "" ;
   this.A1074Usuario_CargoNom = "" ;
   this.A1075Usuario_CargoUOCod = 0 ;
   this.A1076Usuario_CargoUONom = "" ;
   this.A58Usuario_PessoaNom = "" ;
   this.A59Usuario_PessoaTip = "" ;
   this.A325Usuario_PessoaDoc = "" ;
   this.A2095Usuario_PessoaTelefone = "" ;
   this.A341Usuario_UserGamGuid = "" ;
   this.A2Usuario_Nome = "" ;
   this.A289Usuario_EhContador = false ;
   this.A291Usuario_EhContratada = false ;
   this.A292Usuario_EhContratante = false ;
   this.A293Usuario_EhFinanceiro = false ;
   this.A538Usuario_EhGestor = false ;
   this.A1093Usuario_EhPreposto = false ;
   this.A1017Usuario_CrtfPath = "" ;
   this.A1235Usuario_Notificar = "" ;
   this.A54Usuario_Ativo = false ;
   this.A1647Usuario_Email = "" ;
   this.A1716Usuario_Foto = "" ;
   this.A1908Usuario_DeFerias = false ;
   this.A2016Usuario_UltimaArea = 0 ;
   this.Gx_mode = "" ;
   this.Events = {"e13012_client": ["AFTER TRN", true] ,"e14011_client": ["ENTER", true] ,"e15011_client": ["CANCEL", true] ,"e11011_client": ["'DOSELECTUO'", false]};
   this.EvtParms["ENTER"] = [[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],[]];
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["AFTER TRN"] = [[{av:'AV28AuditingObject',fld:'vAUDITINGOBJECT',pic:'',nv:null},{av:'AV32Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null},{av:'AV8WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV23EhContratada_Flag',fld:'vEHCONTRATADA_FLAG',pic:'',nv:false},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV29ContratanteUsuario_EhFiscal',fld:'vCONTRATANTEUSUARIO_EHFISCAL',pic:'',nv:false},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'AV18Email',fld:'vEMAIL',pic:'@!',nv:''},{av:'AV24Entidade_Codigo',fld:'vENTIDADE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20ContratadaUsuario_CstUntPrdNrm',fld:'vCONTRATADAUSUARIO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV21ContratadaUsuario_CstUntPrdExt',fld:'vCONTRATADAUSUARIO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0}],[{av:'AV29ContratanteUsuario_EhFiscal',fld:'vCONTRATANTEUSUARIO_EHFISCAL',pic:'',nv:false},{av:'AV7Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV18Email',fld:'vEMAIL',pic:'@!',nv:''},{av:'A341Usuario_UserGamGuid',fld:'USUARIO_USERGAMGUID',pic:'',nv:''},{av:'AV24Entidade_Codigo',fld:'vENTIDADE_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["'DOSELECTUO'"] = [[{av:'AV26UnidadeOrganizacional_Codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0}],[{av:'AV26UnidadeOrganizacional_Codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0}]];
   this.EnterCtrl = ["BTN_TRN_ENTER"];
   this.setVCMap("AV7Usuario_Codigo", "vUSUARIO_CODIGO", 0, "int");
   this.setVCMap("AV25Insert_Usuario_CargoCod", "vINSERT_USUARIO_CARGOCOD", 0, "int");
   this.setVCMap("A1073Usuario_CargoCod", "USUARIO_CARGOCOD", 0, "int");
   this.setVCMap("AV14Insert_Usuario_PessoaCod", "vINSERT_USUARIO_PESSOACOD", 0, "int");
   this.setVCMap("A1075Usuario_CargoUOCod", "USUARIO_CARGOUOCOD", 0, "int");
   this.setVCMap("Gx_BScreen", "vGXBSCREEN", 0, "int");
   this.setVCMap("A538Usuario_EhGestor", "USUARIO_EHGESTOR", 0, "boolean");
   this.setVCMap("AV28AuditingObject", "vAUDITINGOBJECT", 0, "WWPBaseObjects\AuditingObject");
   this.setVCMap("A2Usuario_Nome", "USUARIO_NOME", 0, "char");
   this.setVCMap("A40000Usuario_Foto_GXI", "USUARIO_FOTO_GXI", 0, "svchar");
   this.setVCMap("A2016Usuario_UltimaArea", "USUARIO_ULTIMAAREA", 0, "int");
   this.setVCMap("A1076Usuario_CargoUONom", "USUARIO_CARGOUONOM", 0, "char");
   this.setVCMap("AV32Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("Gx_mode", "vMODE", 0, "char");
   this.setVCMap("AV9TrnContext", "vTRNCONTEXT", 0, "WWPBaseObjects\WWPTransactionContext");
   this.setVCMap("AV8WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.InitStandaloneVars( );
});
gx.createParentObj(usuario);
