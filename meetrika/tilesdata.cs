/*
               File: TilesData
        Description: Tiles Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:37.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tilesdata : GXProcedure
   {
      public tilesdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public tilesdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out IGxCollection aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "TileSDT", "GxEv3Up14_Meetrika", "SdtTileSDT", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "TileSDT", "GxEv3Up14_Meetrika", "SdtTileSDT", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out IGxCollection aP0_Gxm2rootcol )
      {
         tilesdata objtilesdata;
         objtilesdata = new tilesdata();
         objtilesdata.Gxm2rootcol = new GxObjectCollection( context, "TileSDT", "GxEv3Up14_Meetrika", "SdtTileSDT", "GeneXus.Programs") ;
         objtilesdata.context.SetSubmitInitialConfig(context);
         objtilesdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objtilesdata);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((tilesdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         Gxm1tilesdt = new SdtTileSDT(context);
         Gxm2rootcol.Add(Gxm1tilesdt, 0);
         Gxm1tilesdt.gxTpr_Description = "";
         Gxm1tilesdt.gxTpr_Image = "";
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1tilesdt = new SdtTileSDT(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private IGxCollection aP0_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtTileSDT ))]
      private IGxCollection Gxm2rootcol ;
      private SdtTileSDT Gxm1tilesdt ;
   }

   [ServiceContract(Namespace = "GeneXus.Programs.tilesdata_services")]
   [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
   [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
   public class tilesdata_services : GxRestService
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      [OperationContract]
      [WebInvoke(Method =  "GET" ,
      	BodyStyle =  WebMessageBodyStyle.Bare  ,
      	ResponseFormat = WebMessageFormat.Json,
      	UriTemplate = "/?")]
      public GxGenericCollection<SdtTileSDT_RESTInterface> execute( )
      {
         GxGenericCollection<SdtTileSDT_RESTInterface> Gxm2rootcol = new GxGenericCollection<SdtTileSDT_RESTInterface>() ;
         try
         {
            permissionPrefix = "";
            if ( ! IsAuthenticated() )
            {
               return null ;
            }
            if ( ! ProcessHeaders("tilesdata") )
            {
               return null ;
            }
            tilesdata worker = new tilesdata(context) ;
            worker.IsMain = RunAsMain ;
            IGxCollection gxrGxm2rootcol = new GxObjectCollection() ;
            worker.execute(out gxrGxm2rootcol );
            worker.cleanup( );
            Gxm2rootcol = new GxGenericCollection<SdtTileSDT_RESTInterface>(gxrGxm2rootcol) ;
            return Gxm2rootcol ;
         }
         catch ( Exception e )
         {
            WebException(e);
         }
         finally
         {
            Cleanup();
         }
         return null ;
      }

   }

}
