/*
               File: WP_AplicarDeflator
        Description: Aplicar Deflator
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:43:59.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_aplicardeflator : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_aplicardeflator( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_aplicardeflator( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FltContratada_Codigo ,
                           int aP1_FltServico_Codigo ,
                           DateTime aP2_FltDataIni ,
                           DateTime aP3_FltDataFim ,
                           ref String aP4_StatusDmn ,
                           ref int aP5_ContadorFMCod ,
                           ref bool aP6_Confirmado )
      {
         this.AV14FltContratada_Codigo = aP0_FltContratada_Codigo;
         this.AV11FltServico_Codigo = aP1_FltServico_Codigo;
         this.AV12FltDataIni = aP2_FltDataIni;
         this.AV13FltDataFim = aP3_FltDataFim;
         this.AV17StatusDmn = aP4_StatusDmn;
         this.AV19ContadorFMCod = aP5_ContadorFMCod;
         this.AV16Confirmado = aP6_Confirmado;
         executePrivate();
         aP4_StatusDmn=this.AV17StatusDmn;
         aP5_ContadorFMCod=this.AV19ContadorFMCod;
         aP6_Confirmado=this.AV16Confirmado;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContratada_codigo = new GXCombobox();
         dynavServico_codigo = new GXCombobox();
         dynavContadorfm = new GXCombobox();
         cmbavStatus = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV8WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGOEM2( AV8WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSERVICO_CODIGOEM2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTADORFM") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV8WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTADORFMEM2( AV8WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV14FltContratada_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14FltContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14FltContratada_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFLTCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14FltContratada_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV11FltServico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11FltServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11FltServico_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFLTSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11FltServico_Codigo), "ZZZZZ9")));
                  AV12FltDataIni = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12FltDataIni", context.localUtil.Format(AV12FltDataIni, "99/99/99"));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFLTDATAINI", GetSecureSignedToken( "", AV12FltDataIni));
                  AV13FltDataFim = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FltDataFim", context.localUtil.Format(AV13FltDataFim, "99/99/99"));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFLTDATAFIM", GetSecureSignedToken( "", AV13FltDataFim));
                  AV17StatusDmn = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17StatusDmn", AV17StatusDmn);
                  AV19ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContadorFMCod), 6, 0)));
                  AV16Confirmado = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Confirmado", AV16Confirmado);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAEM2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTEM2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423435960");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_aplicardeflator.aspx") + "?" + UrlEncode("" +AV14FltContratada_Codigo) + "," + UrlEncode("" +AV11FltServico_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV12FltDataIni)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV13FltDataFim)) + "," + UrlEncode(StringUtil.RTrim(AV17StatusDmn)) + "," + UrlEncode("" +AV19ContadorFMCod) + "," + UrlEncode(StringUtil.BoolToStr(AV16Confirmado))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vTODAY", context.localUtil.DToC( Gx_date, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vCONTADORFMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19ContadorFMCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSTATUSDMN", StringUtil.RTrim( AV17StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A601ContagemResultado_Servico), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_STATUSDMN", StringUtil.RTrim( A484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAULTCNT", context.localUtil.DToC( A566ContagemResultado_DataUltCnt, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTADORFM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A584ContagemResultado_ContadorFM), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_PERCENTUAL", StringUtil.LTrim( StringUtil.NToC( A558Servico_Percentual, 7, 3, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFLTCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14FltContratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFLTSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11FltServico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFLTDATAINI", context.localUtil.DToC( AV12FltDataIni, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vFLTDATAFIM", context.localUtil.DToC( AV13FltDataFim, 0, "/"));
         GxWebStd.gx_boolean_hidden_field( context, "vCONFIRMADO", AV16Confirmado);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV8WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV8WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vFLTCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14FltContratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFLTSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11FltServico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFLTDATAINI", GetSecureSignedToken( "", AV12FltDataIni));
         GxWebStd.gx_hidden_field( context, "gxhash_vFLTDATAFIM", GetSecureSignedToken( "", AV13FltDataFim));
         GxWebStd.gx_hidden_field( context, "gxhash_vFLTCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14FltContratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFLTSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11FltServico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vFLTDATAINI", GetSecureSignedToken( "", AV12FltDataIni));
         GxWebStd.gx_hidden_field( context, "gxhash_vFLTDATAFIM", GetSecureSignedToken( "", AV13FltDataFim));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEEM2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTEM2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_aplicardeflator.aspx") + "?" + UrlEncode("" +AV14FltContratada_Codigo) + "," + UrlEncode("" +AV11FltServico_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV12FltDataIni)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV13FltDataFim)) + "," + UrlEncode(StringUtil.RTrim(AV17StatusDmn)) + "," + UrlEncode("" +AV19ContadorFMCod) + "," + UrlEncode(StringUtil.BoolToStr(AV16Confirmado)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_AplicarDeflator" ;
      }

      public override String GetPgmdesc( )
      {
         return "Aplicar Deflator" ;
      }

      protected void WBEM0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_EM2( true) ;
         }
         else
         {
            wb_table1_2_EM2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EM2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTEM2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Aplicar Deflator", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPEM0( ) ;
      }

      protected void WSEM2( )
      {
         STARTEM2( ) ;
         EVTEM2( ) ;
      }

      protected void EVTEM2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11EM2 */
                              E11EM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENTER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12EM2 */
                              E12EM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATADA_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13EM2 */
                              E13EM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSERVICO_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14EM2 */
                              E14EM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDATAINI.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15EM2 */
                              E15EM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDATAFIM.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16EM2 */
                              E16EM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSTATUS.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17EM2 */
                              E17EM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTADORFM.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18EM2 */
                              E18EM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19EM2 */
                              E19EM2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12EM2 */
                                    E12EM2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEM2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAEM2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            dynavServico_codigo.Name = "vSERVICO_CODIGO";
            dynavServico_codigo.WebTags = "";
            dynavContadorfm.Name = "vCONTADORFM";
            dynavContadorfm.WebTags = "";
            cmbavStatus.Name = "vSTATUS";
            cmbavStatus.WebTags = "";
            cmbavStatus.addItem("R", "Resolvida", 0);
            cmbavStatus.addItem("C", "Conferida", 0);
            if ( cmbavStatus.ItemCount > 0 )
            {
               AV15Status = cmbavStatus.getValidValue(AV15Status);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Status", AV15Status);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_CODIGOEM2( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_dataEM2( AV8WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_htmlEM2( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_dataEM2( AV8WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV7Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_dataEM2( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00EM2 */
         pr_default.execute(0, new Object[] {AV8WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00EM2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00EM2_A438Contratada_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvSERVICO_CODIGOEM2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICO_CODIGO_dataEM2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICO_CODIGO_htmlEM2( )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICO_CODIGO_dataEM2( ) ;
         gxdynajaxindex = 1;
         dynavServico_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServico_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV5Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvSERVICO_CODIGO_dataEM2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhum");
         /* Using cursor H00EM3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00EM3_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00EM3_A605Servico_Sigla[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvCONTADORFMEM2( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTADORFM_dataEM2( AV8WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTADORFM_htmlEM2( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTADORFM_dataEM2( AV8WWPContext) ;
         gxdynajaxindex = 1;
         dynavContadorfm.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContadorfm.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContadorfm.ItemCount > 0 )
         {
            AV20ContadorFM = (int)(NumberUtil.Val( dynavContadorfm.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContadorFM), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContadorFM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContadorFM), 6, 0)));
         }
      }

      protected void GXDLVvCONTADORFM_dataEM2( wwpbaseobjects.SdtWWPContext AV8WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00EM4 */
         pr_default.execute(2, new Object[] {AV8WWPContext.gxTpr_Userehcontratante, AV8WWPContext.gxTpr_Userehadministradorgam, AV8WWPContext.gxTpr_Contratada_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00EM4_A69ContratadaUsuario_UsuarioCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00EM4_A71ContratadaUsuario_UsuarioPessoaNom[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV7Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)));
         }
         if ( dynavServico_codigo.ItemCount > 0 )
         {
            AV5Servico_Codigo = (int)(NumberUtil.Val( dynavServico_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0)));
         }
         if ( dynavContadorfm.ItemCount > 0 )
         {
            AV20ContadorFM = (int)(NumberUtil.Val( dynavContadorfm.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContadorFM), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContadorFM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContadorFM), 6, 0)));
         }
         if ( cmbavStatus.ItemCount > 0 )
         {
            AV15Status = cmbavStatus.getValidValue(AV15Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Status", AV15Status);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEM2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      protected void RFEM2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E19EM2 */
            E19EM2 ();
            WBEM0( ) ;
         }
      }

      protected void STRUPEM0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         GXVvSERVICO_CODIGO_htmlEM2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11EM2 */
         E11EM2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADA_CODIGO_htmlEM2( AV8WWPContext) ;
         GXVvCONTADORFM_htmlEM2( AV8WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV7Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)));
            dynavServico_codigo.CurrentValue = cgiGet( dynavServico_codigo_Internalname);
            AV5Servico_Codigo = (int)(NumberUtil.Val( cgiGet( dynavServico_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavDataini_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data Ini"}), 1, "vDATAINI");
               GX_FocusControl = edtavDataini_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9DataIni = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DataIni", context.localUtil.Format(AV9DataIni, "99/99/99"));
            }
            else
            {
               AV9DataIni = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDataini_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DataIni", context.localUtil.Format(AV9DataIni, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDatafim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data Fim"}), 1, "vDATAFIM");
               GX_FocusControl = edtavDatafim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV10DataFim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DataFim", context.localUtil.Format(AV10DataFim, "99/99/99"));
            }
            else
            {
               AV10DataFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDatafim_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DataFim", context.localUtil.Format(AV10DataFim, "99/99/99"));
            }
            dynavContadorfm.CurrentValue = cgiGet( dynavContadorfm_Internalname);
            AV20ContadorFM = (int)(NumberUtil.Val( cgiGet( dynavContadorfm_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContadorFM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContadorFM), 6, 0)));
            cmbavStatus.CurrentValue = cgiGet( cmbavStatus_Internalname);
            AV15Status = cgiGet( cmbavStatus_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Status", AV15Status);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".") > 999.999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICO_PERCENTUAL");
               GX_FocusControl = edtavServico_percentual_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6Servico_Percentual = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV6Servico_Percentual, 7, 3)));
            }
            else
            {
               AV6Servico_Percentual = context.localUtil.CToN( cgiGet( edtavServico_percentual_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV6Servico_Percentual, 7, 3)));
            }
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvSERVICO_CODIGO_htmlEM2( ) ;
            GXVvCONTRATADA_CODIGO_htmlEM2( AV8WWPContext) ;
            GXVvCONTADORFM_htmlEM2( AV8WWPContext) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11EM2 */
         E11EM2 ();
         if (returnInSub) return;
      }

      protected void E11EM2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV7Contratada_Codigo = AV14FltContratada_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)));
         AV5Servico_Codigo = AV11FltServico_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0)));
         AV9DataIni = AV12FltDataIni;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DataIni", context.localUtil.Format(AV9DataIni, "99/99/99"));
         AV10DataFim = AV13FltDataFim;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DataFim", context.localUtil.Format(AV10DataFim, "99/99/99"));
         AV15Status = AV17StatusDmn;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Status", AV15Status);
         AV20ContadorFM = AV19ContadorFMCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContadorFM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContadorFM), 6, 0)));
         if ( ! (0==AV5Servico_Codigo) )
         {
            /* Execute user subroutine: 'GETDEFLATOR' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void E12EM2( )
      {
         /* 'Enter' Routine */
         if ( (0==AV7Contratada_Codigo) )
         {
            GX_msglist.addItem("Informe a Contratada Origem das demandas!");
         }
         else if ( (DateTime.MinValue==AV9DataIni) )
         {
            GX_msglist.addItem("Informe a data de inicio!");
         }
         else if ( (DateTime.MinValue==AV10DataFim) )
         {
            GX_msglist.addItem("Informe a data final!");
         }
         else if ( AV9DataIni > Gx_date )
         {
            GX_msglist.addItem("Per�odo incorreto, data inicial maior que a de hoje!");
         }
         else if ( AV10DataFim < AV9DataIni )
         {
            GX_msglist.addItem("Per�odo incorreto, data final menor que data inicial!");
         }
         else
         {
            AV16Confirmado = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Confirmado", AV16Confirmado);
            new prc_aplicardeflator(context ).execute(  AV7Contratada_Codigo,  AV5Servico_Codigo,  AV6Servico_Percentual,  AV9DataIni,  AV10DataFim,  AV15Status, ref  AV20ContadorFM) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV6Servico_Percentual, 7, 3)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DataIni", context.localUtil.Format(AV9DataIni, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DataFim", context.localUtil.Format(AV10DataFim, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Status", AV15Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContadorFM", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20ContadorFM), 6, 0)));
            context.setWebReturnParms(new Object[] {(String)AV17StatusDmn,(int)AV19ContadorFMCod,(bool)AV16Confirmado});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         dynavContadorfm.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContadorFM), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContadorfm_Internalname, "Values", dynavContadorfm.ToJavascriptSource());
      }

      protected void E13EM2( )
      {
         /* Contratada_codigo_Click Routine */
         /* Execute user subroutine: 'COUNT' */
         S122 ();
         if (returnInSub) return;
      }

      protected void E14EM2( )
      {
         /* Servico_codigo_Click Routine */
         if ( (0==AV5Servico_Codigo) )
         {
            AV6Servico_Percentual = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV6Servico_Percentual, 7, 3)));
         }
         else
         {
            /* Execute user subroutine: 'GETDEFLATOR' */
            S112 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'COUNT' */
         S122 ();
         if (returnInSub) return;
      }

      protected void E15EM2( )
      {
         /* Dataini_Isvalid Routine */
         /* Execute user subroutine: 'COUNT' */
         S122 ();
         if (returnInSub) return;
      }

      protected void E16EM2( )
      {
         /* Datafim_Isvalid Routine */
         /* Execute user subroutine: 'COUNT' */
         S122 ();
         if (returnInSub) return;
      }

      protected void E17EM2( )
      {
         /* Status_Click Routine */
         /* Execute user subroutine: 'COUNT' */
         S122 ();
         if (returnInSub) return;
      }

      protected void E18EM2( )
      {
         /* Contadorfm_Click Routine */
         /* Execute user subroutine: 'COUNT' */
         S122 ();
         if (returnInSub) return;
      }

      protected void S112( )
      {
         /* 'GETDEFLATOR' Routine */
         /* Using cursor H00EM5 */
         pr_default.execute(3, new Object[] {AV7Contratada_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A1595Contratada_AreaTrbSrvPdr = H00EM5_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00EM5_n1595Contratada_AreaTrbSrvPdr[0];
            A1013Contrato_PrepostoCod = H00EM5_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = H00EM5_n1013Contrato_PrepostoCod[0];
            A74Contrato_Codigo = H00EM5_A74Contrato_Codigo[0];
            A52Contratada_AreaTrabalhoCod = H00EM5_A52Contratada_AreaTrabalhoCod[0];
            A516Contratada_TipoFabrica = H00EM5_A516Contratada_TipoFabrica[0];
            n516Contratada_TipoFabrica = H00EM5_n516Contratada_TipoFabrica[0];
            A54Usuario_Ativo = H00EM5_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00EM5_n54Usuario_Ativo[0];
            A292Usuario_EhContratante = H00EM5_A292Usuario_EhContratante[0];
            n292Usuario_EhContratante = H00EM5_n292Usuario_EhContratante[0];
            A1093Usuario_EhPreposto = H00EM5_A1093Usuario_EhPreposto[0];
            n1093Usuario_EhPreposto = H00EM5_n1093Usuario_EhPreposto[0];
            A632Servico_Ativo = H00EM5_A632Servico_Ativo[0];
            n632Servico_Ativo = H00EM5_n632Servico_Ativo[0];
            A39Contratada_Codigo = H00EM5_A39Contratada_Codigo[0];
            A54Usuario_Ativo = H00EM5_A54Usuario_Ativo[0];
            n54Usuario_Ativo = H00EM5_n54Usuario_Ativo[0];
            A292Usuario_EhContratante = H00EM5_A292Usuario_EhContratante[0];
            n292Usuario_EhContratante = H00EM5_n292Usuario_EhContratante[0];
            A1093Usuario_EhPreposto = H00EM5_A1093Usuario_EhPreposto[0];
            n1093Usuario_EhPreposto = H00EM5_n1093Usuario_EhPreposto[0];
            A52Contratada_AreaTrabalhoCod = H00EM5_A52Contratada_AreaTrabalhoCod[0];
            A516Contratada_TipoFabrica = H00EM5_A516Contratada_TipoFabrica[0];
            n516Contratada_TipoFabrica = H00EM5_n516Contratada_TipoFabrica[0];
            A1595Contratada_AreaTrbSrvPdr = H00EM5_A1595Contratada_AreaTrbSrvPdr[0];
            n1595Contratada_AreaTrbSrvPdr = H00EM5_n1595Contratada_AreaTrbSrvPdr[0];
            A632Servico_Ativo = H00EM5_A632Servico_Ativo[0];
            n632Servico_Ativo = H00EM5_n632Servico_Ativo[0];
            /* Using cursor H00EM6 */
            pr_default.execute(4, new Object[] {A74Contrato_Codigo, AV5Servico_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A155Servico_Codigo = H00EM6_A155Servico_Codigo[0];
               A558Servico_Percentual = H00EM6_A558Servico_Percentual[0];
               n558Servico_Percentual = H00EM6_n558Servico_Percentual[0];
               AV6Servico_Percentual = A558Servico_Percentual;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Servico_Percentual", StringUtil.LTrim( StringUtil.Str( AV6Servico_Percentual, 7, 3)));
               pr_default.readNext(4);
            }
            pr_default.close(4);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void S122( )
      {
         /* 'COUNT' Routine */
         AV18Count = 0;
         /* Optimized group. */
         /* Using cursor H00EM8 */
         pr_default.execute(5, new Object[] {AV7Contratada_Codigo, AV9DataIni, AV10DataFim, AV20ContadorFM, AV5Servico_Codigo, AV15Status});
         cV18Count = H00EM8_AV18Count[0];
         pr_default.close(5);
         AV18Count = (short)(AV18Count+cV18Count*1);
         /* End optimized group. */
         bttEnter_Caption = "Aplicar em "+StringUtil.Trim( StringUtil.Str( (decimal)(AV18Count), 4, 0))+" demandas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttEnter_Internalname, "Caption", bttEnter_Caption);
      }

      protected void nextLoad( )
      {
      }

      protected void E19EM2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_EM2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(250), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(600), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Contratada Origem:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AplicarDeflator.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATADA_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,9);\"", "", true, "HLP_WP_AplicarDeflator.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV7Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Servi�o:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AplicarDeflator.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServico_codigo, dynavServico_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0)), 1, dynavServico_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSERVICO_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "", true, "HLP_WP_AplicarDeflator.htm");
            dynavServico_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServico_codigo_Internalname, "Values", (String)(dynavServico_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "nas Contagens do per�odo:", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AplicarDeflator.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDataini_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDataini_Internalname, context.localUtil.Format(AV9DataIni, "99/99/99"), context.localUtil.Format( AV9DataIni, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,17);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDataini_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_AplicarDeflator.htm");
            GxWebStd.gx_bitmap( context, edtavDataini_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_AplicarDeflator.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "&nbsp;") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "-", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AplicarDeflator.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDatafim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDatafim_Internalname, context.localUtil.Format(AV10DataFim, "99/99/99"), context.localUtil.Format( AV10DataFim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,19);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDatafim_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_AplicarDeflator.htm");
            GxWebStd.gx_bitmap( context, edtavDatafim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_AplicarDeflator.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock8_Internalname, "do Contador FM:", "", "", lblTextblock8_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AplicarDeflator.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContadorfm, dynavContadorfm_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContadorFM), 6, 0)), 1, dynavContadorfm_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTADORFM.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_WP_AplicarDeflator.htm");
            dynavContadorfm.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20ContadorFM), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContadorfm_Internalname, "Values", (String)(dynavContadorfm.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock7_Internalname, "de Demandas com Status:", "", "", lblTextblock7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AplicarDeflator.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavStatus, cmbavStatus_Internalname, StringUtil.RTrim( AV15Status), 1, cmbavStatus_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSTATUS.CLICK."+"'", "char", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "", true, "HLP_WP_AplicarDeflator.htm");
            cmbavStatus.CurrentValue = StringUtil.RTrim( AV15Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavStatus_Internalname, "Values", (String)(cmbavStatus.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "o Deflator:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AplicarDeflator.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_percentual_Internalname, StringUtil.LTrim( StringUtil.NToC( AV6Servico_Percentual, 7, 3, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV6Servico_Percentual, "ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','3');"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_percentual_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 60, "px", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_AplicarDeflator.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttEnter_Internalname, "", bttEnter_Caption, bttEnter_Jsonclick, 5, "Aplicar em 0 demandas", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ENTER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AplicarDeflator.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCancel_Internalname, "", "Fechar", bttCancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AplicarDeflator.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EM2e( true) ;
         }
         else
         {
            wb_table1_2_EM2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV14FltContratada_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14FltContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14FltContratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFLTCONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV14FltContratada_Codigo), "ZZZZZ9")));
         AV11FltServico_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11FltServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11FltServico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFLTSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11FltServico_Codigo), "ZZZZZ9")));
         AV12FltDataIni = (DateTime)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12FltDataIni", context.localUtil.Format(AV12FltDataIni, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFLTDATAINI", GetSecureSignedToken( "", AV12FltDataIni));
         AV13FltDataFim = (DateTime)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13FltDataFim", context.localUtil.Format(AV13FltDataFim, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vFLTDATAFIM", GetSecureSignedToken( "", AV13FltDataFim));
         AV17StatusDmn = (String)getParm(obj,4);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17StatusDmn", AV17StatusDmn);
         AV19ContadorFMCod = Convert.ToInt32(getParm(obj,5));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19ContadorFMCod), 6, 0)));
         AV16Confirmado = (bool)getParm(obj,6);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Confirmado", AV16Confirmado);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEM2( ) ;
         WSEM2( ) ;
         WEEM2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203242344035");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_aplicardeflator.js", "?20203242344035");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock4_Internalname = "TEXTBLOCK4";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         dynavServico_codigo_Internalname = "vSERVICO_CODIGO";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavDataini_Internalname = "vDATAINI";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         edtavDatafim_Internalname = "vDATAFIM";
         lblTextblock8_Internalname = "TEXTBLOCK8";
         dynavContadorfm_Internalname = "vCONTADORFM";
         lblTextblock7_Internalname = "TEXTBLOCK7";
         cmbavStatus_Internalname = "vSTATUS";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavServico_percentual_Internalname = "vSERVICO_PERCENTUAL";
         bttEnter_Internalname = "ENTER";
         bttCancel_Internalname = "CANCEL";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavServico_percentual_Jsonclick = "";
         cmbavStatus_Jsonclick = "";
         dynavContadorfm_Jsonclick = "";
         edtavDatafim_Jsonclick = "";
         edtavDataini_Jsonclick = "";
         dynavServico_codigo_Jsonclick = "";
         dynavContratada_codigo_Jsonclick = "";
         bttEnter_Caption = "Aplicar em 0 demandas";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Aplicar Deflator";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'ENTER'","{handler:'E12EM2',iparms:[{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9DataIni',fld:'vDATAINI',pic:'',nv:''},{av:'AV10DataFim',fld:'vDATAFIM',pic:'',nv:''},{av:'Gx_date',fld:'vTODAY',pic:'',nv:''},{av:'AV5Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6Servico_Percentual',fld:'vSERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'AV15Status',fld:'vSTATUS',pic:'',nv:''},{av:'AV20ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV19ContadorFMCod',fld:'vCONTADORFMCOD',pic:'ZZZZZ9',nv:0},{av:'AV17StatusDmn',fld:'vSTATUSDMN',pic:'',nv:''}],oparms:[{av:'AV16Confirmado',fld:'vCONFIRMADO',pic:'',nv:false},{av:'AV20ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VCONTRATADA_CODIGO.CLICK","{handler:'E13EM2',iparms:[{av:'A805ContagemResultado_ContratadaOrigemCod',fld:'CONTAGEMRESULTADO_CONTRATADAORIGEMCOD',pic:'ZZZZZ9',nv:0},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV5Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV15Status',fld:'vSTATUS',pic:'',nv:''},{av:'A566ContagemResultado_DataUltCnt',fld:'CONTAGEMRESULTADO_DATAULTCNT',pic:'',nv:''},{av:'AV9DataIni',fld:'vDATAINI',pic:'',nv:''},{av:'AV10DataFim',fld:'vDATAFIM',pic:'',nv:''},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV20ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'ENTER',prop:'Caption'}]}");
         setEventMetadata("VSERVICO_CODIGO.CLICK","{handler:'E14EM2',iparms:[{av:'AV5Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A558Servico_Percentual',fld:'SERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{av:'A805ContagemResultado_ContratadaOrigemCod',fld:'CONTAGEMRESULTADO_CONTRATADAORIGEMCOD',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV15Status',fld:'vSTATUS',pic:'',nv:''},{av:'A566ContagemResultado_DataUltCnt',fld:'CONTAGEMRESULTADO_DATAULTCNT',pic:'',nv:''},{av:'AV9DataIni',fld:'vDATAINI',pic:'',nv:''},{av:'AV10DataFim',fld:'vDATAFIM',pic:'',nv:''},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV20ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV6Servico_Percentual',fld:'vSERVICO_PERCENTUAL',pic:'ZZ9.999',nv:0.0},{ctrl:'ENTER',prop:'Caption'}]}");
         setEventMetadata("VDATAINI.ISVALID","{handler:'E15EM2',iparms:[{av:'A805ContagemResultado_ContratadaOrigemCod',fld:'CONTAGEMRESULTADO_CONTRATADAORIGEMCOD',pic:'ZZZZZ9',nv:0},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV5Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV15Status',fld:'vSTATUS',pic:'',nv:''},{av:'A566ContagemResultado_DataUltCnt',fld:'CONTAGEMRESULTADO_DATAULTCNT',pic:'',nv:''},{av:'AV9DataIni',fld:'vDATAINI',pic:'',nv:''},{av:'AV10DataFim',fld:'vDATAFIM',pic:'',nv:''},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV20ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'ENTER',prop:'Caption'}]}");
         setEventMetadata("VDATAFIM.ISVALID","{handler:'E16EM2',iparms:[{av:'A805ContagemResultado_ContratadaOrigemCod',fld:'CONTAGEMRESULTADO_CONTRATADAORIGEMCOD',pic:'ZZZZZ9',nv:0},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV5Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV15Status',fld:'vSTATUS',pic:'',nv:''},{av:'A566ContagemResultado_DataUltCnt',fld:'CONTAGEMRESULTADO_DATAULTCNT',pic:'',nv:''},{av:'AV9DataIni',fld:'vDATAINI',pic:'',nv:''},{av:'AV10DataFim',fld:'vDATAFIM',pic:'',nv:''},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV20ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'ENTER',prop:'Caption'}]}");
         setEventMetadata("VSTATUS.CLICK","{handler:'E17EM2',iparms:[{av:'A805ContagemResultado_ContratadaOrigemCod',fld:'CONTAGEMRESULTADO_CONTRATADAORIGEMCOD',pic:'ZZZZZ9',nv:0},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV5Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV15Status',fld:'vSTATUS',pic:'',nv:''},{av:'A566ContagemResultado_DataUltCnt',fld:'CONTAGEMRESULTADO_DATAULTCNT',pic:'',nv:''},{av:'AV9DataIni',fld:'vDATAINI',pic:'',nv:''},{av:'AV10DataFim',fld:'vDATAFIM',pic:'',nv:''},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV20ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'ENTER',prop:'Caption'}]}");
         setEventMetadata("VCONTADORFM.CLICK","{handler:'E18EM2',iparms:[{av:'A805ContagemResultado_ContratadaOrigemCod',fld:'CONTAGEMRESULTADO_CONTRATADAORIGEMCOD',pic:'ZZZZZ9',nv:0},{av:'AV7Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A601ContagemResultado_Servico',fld:'CONTAGEMRESULTADO_SERVICO',pic:'ZZZZZ9',nv:0},{av:'AV5Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A484ContagemResultado_StatusDmn',fld:'CONTAGEMRESULTADO_STATUSDMN',pic:'',nv:''},{av:'AV15Status',fld:'vSTATUS',pic:'',nv:''},{av:'A566ContagemResultado_DataUltCnt',fld:'CONTAGEMRESULTADO_DATAULTCNT',pic:'',nv:''},{av:'AV9DataIni',fld:'vDATAINI',pic:'',nv:''},{av:'AV10DataFim',fld:'vDATAFIM',pic:'',nv:''},{av:'A584ContagemResultado_ContadorFM',fld:'CONTAGEMRESULTADO_CONTADORFM',pic:'ZZZZZ9',nv:0},{av:'AV20ContadorFM',fld:'vCONTADORFM',pic:'ZZZZZ9',nv:0}],oparms:[{ctrl:'ENTER',prop:'Caption'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV12FltDataIni = DateTime.MinValue;
         wcpOAV13FltDataFim = DateTime.MinValue;
         wcpOAV17StatusDmn = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         Gx_date = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         A566ContagemResultado_DataUltCnt = DateTime.MinValue;
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Status = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00EM2_A39Contratada_Codigo = new int[1] ;
         H00EM2_A438Contratada_Sigla = new String[] {""} ;
         H00EM2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EM3_A155Servico_Codigo = new int[1] ;
         H00EM3_A605Servico_Sigla = new String[] {""} ;
         H00EM3_A632Servico_Ativo = new bool[] {false} ;
         H00EM3_n632Servico_Ativo = new bool[] {false} ;
         H00EM4_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H00EM4_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00EM4_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00EM4_A71ContratadaUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00EM4_n71ContratadaUsuario_UsuarioPessoaNom = new bool[] {false} ;
         H00EM4_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00EM4_A292Usuario_EhContratante = new bool[] {false} ;
         H00EM4_n292Usuario_EhContratante = new bool[] {false} ;
         H00EM4_A1093Usuario_EhPreposto = new bool[] {false} ;
         H00EM4_n1093Usuario_EhPreposto = new bool[] {false} ;
         H00EM4_A516Contratada_TipoFabrica = new String[] {""} ;
         H00EM4_n516Contratada_TipoFabrica = new bool[] {false} ;
         AV9DataIni = DateTime.MinValue;
         AV10DataFim = DateTime.MinValue;
         H00EM5_A1595Contratada_AreaTrbSrvPdr = new int[1] ;
         H00EM5_n1595Contratada_AreaTrbSrvPdr = new bool[] {false} ;
         H00EM5_A1013Contrato_PrepostoCod = new int[1] ;
         H00EM5_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00EM5_A74Contrato_Codigo = new int[1] ;
         H00EM5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00EM5_A516Contratada_TipoFabrica = new String[] {""} ;
         H00EM5_n516Contratada_TipoFabrica = new bool[] {false} ;
         H00EM5_A54Usuario_Ativo = new bool[] {false} ;
         H00EM5_n54Usuario_Ativo = new bool[] {false} ;
         H00EM5_A292Usuario_EhContratante = new bool[] {false} ;
         H00EM5_n292Usuario_EhContratante = new bool[] {false} ;
         H00EM5_A1093Usuario_EhPreposto = new bool[] {false} ;
         H00EM5_n1093Usuario_EhPreposto = new bool[] {false} ;
         H00EM5_A632Servico_Ativo = new bool[] {false} ;
         H00EM5_n632Servico_Ativo = new bool[] {false} ;
         H00EM5_A39Contratada_Codigo = new int[1] ;
         A516Contratada_TipoFabrica = "";
         H00EM6_A160ContratoServicos_Codigo = new int[1] ;
         H00EM6_A74Contrato_Codigo = new int[1] ;
         H00EM6_A155Servico_Codigo = new int[1] ;
         H00EM6_A558Servico_Percentual = new decimal[1] ;
         H00EM6_n558Servico_Percentual = new bool[] {false} ;
         H00EM8_AV18Count = new short[1] ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock4_Jsonclick = "";
         TempTags = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock8_Jsonclick = "";
         lblTextblock7_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         bttEnter_Jsonclick = "";
         bttCancel_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_aplicardeflator__default(),
            new Object[][] {
                new Object[] {
               H00EM2_A39Contratada_Codigo, H00EM2_A438Contratada_Sigla, H00EM2_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               H00EM3_A155Servico_Codigo, H00EM3_A605Servico_Sigla, H00EM3_A632Servico_Ativo
               }
               , new Object[] {
               H00EM4_A70ContratadaUsuario_UsuarioPessoaCod, H00EM4_n70ContratadaUsuario_UsuarioPessoaCod, H00EM4_A69ContratadaUsuario_UsuarioCod, H00EM4_A71ContratadaUsuario_UsuarioPessoaNom, H00EM4_n71ContratadaUsuario_UsuarioPessoaNom, H00EM4_A66ContratadaUsuario_ContratadaCod, H00EM4_A292Usuario_EhContratante, H00EM4_n292Usuario_EhContratante, H00EM4_A1093Usuario_EhPreposto, H00EM4_n1093Usuario_EhPreposto,
               H00EM4_A516Contratada_TipoFabrica, H00EM4_n516Contratada_TipoFabrica
               }
               , new Object[] {
               H00EM5_A1595Contratada_AreaTrbSrvPdr, H00EM5_n1595Contratada_AreaTrbSrvPdr, H00EM5_A1013Contrato_PrepostoCod, H00EM5_n1013Contrato_PrepostoCod, H00EM5_A74Contrato_Codigo, H00EM5_A52Contratada_AreaTrabalhoCod, H00EM5_A516Contratada_TipoFabrica, H00EM5_A54Usuario_Ativo, H00EM5_n54Usuario_Ativo, H00EM5_A292Usuario_EhContratante,
               H00EM5_n292Usuario_EhContratante, H00EM5_A1093Usuario_EhPreposto, H00EM5_n1093Usuario_EhPreposto, H00EM5_A632Servico_Ativo, H00EM5_n632Servico_Ativo, H00EM5_A39Contratada_Codigo
               }
               , new Object[] {
               H00EM6_A160ContratoServicos_Codigo, H00EM6_A74Contrato_Codigo, H00EM6_A155Servico_Codigo, H00EM6_A558Servico_Percentual, H00EM6_n558Servico_Percentual
               }
               , new Object[] {
               H00EM8_AV18Count
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV18Count ;
      private short cV18Count ;
      private short nGXWrapped ;
      private int AV14FltContratada_Codigo ;
      private int AV11FltServico_Codigo ;
      private int AV19ContadorFMCod ;
      private int wcpOAV14FltContratada_Codigo ;
      private int wcpOAV11FltServico_Codigo ;
      private int wcpOAV19ContadorFMCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A601ContagemResultado_Servico ;
      private int A584ContagemResultado_ContadorFM ;
      private int A39Contratada_Codigo ;
      private int A155Servico_Codigo ;
      private int gxdynajaxindex ;
      private int AV7Contratada_Codigo ;
      private int AV5Servico_Codigo ;
      private int AV20ContadorFM ;
      private int A1595Contratada_AreaTrbSrvPdr ;
      private int A1013Contrato_PrepostoCod ;
      private int A74Contrato_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int idxLst ;
      private decimal A558Servico_Percentual ;
      private decimal AV6Servico_Percentual ;
      private String AV17StatusDmn ;
      private String wcpOAV17StatusDmn ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A484ContagemResultado_StatusDmn ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV15Status ;
      private String dynavContratada_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String dynavServico_codigo_Internalname ;
      private String edtavDataini_Internalname ;
      private String edtavDatafim_Internalname ;
      private String dynavContadorfm_Internalname ;
      private String cmbavStatus_Internalname ;
      private String edtavServico_percentual_Internalname ;
      private String A516Contratada_TipoFabrica ;
      private String bttEnter_Caption ;
      private String bttEnter_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String TempTags ;
      private String dynavContratada_codigo_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String dynavServico_codigo_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtavDataini_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String edtavDatafim_Jsonclick ;
      private String lblTextblock8_Internalname ;
      private String lblTextblock8_Jsonclick ;
      private String dynavContadorfm_Jsonclick ;
      private String lblTextblock7_Internalname ;
      private String lblTextblock7_Jsonclick ;
      private String cmbavStatus_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavServico_percentual_Jsonclick ;
      private String bttEnter_Jsonclick ;
      private String bttCancel_Internalname ;
      private String bttCancel_Jsonclick ;
      private DateTime AV12FltDataIni ;
      private DateTime AV13FltDataFim ;
      private DateTime wcpOAV12FltDataIni ;
      private DateTime wcpOAV13FltDataFim ;
      private DateTime Gx_date ;
      private DateTime A566ContagemResultado_DataUltCnt ;
      private DateTime AV9DataIni ;
      private DateTime AV10DataFim ;
      private bool AV16Confirmado ;
      private bool wcpOAV16Confirmado ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1595Contratada_AreaTrbSrvPdr ;
      private bool n1013Contrato_PrepostoCod ;
      private bool n516Contratada_TipoFabrica ;
      private bool A54Usuario_Ativo ;
      private bool n54Usuario_Ativo ;
      private bool A292Usuario_EhContratante ;
      private bool n292Usuario_EhContratante ;
      private bool A1093Usuario_EhPreposto ;
      private bool n1093Usuario_EhPreposto ;
      private bool A632Servico_Ativo ;
      private bool n632Servico_Ativo ;
      private bool n558Servico_Percentual ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP4_StatusDmn ;
      private int aP5_ContadorFMCod ;
      private bool aP6_Confirmado ;
      private GXCombobox dynavContratada_codigo ;
      private GXCombobox dynavServico_codigo ;
      private GXCombobox dynavContadorfm ;
      private GXCombobox cmbavStatus ;
      private IDataStoreProvider pr_default ;
      private int[] H00EM2_A39Contratada_Codigo ;
      private String[] H00EM2_A438Contratada_Sigla ;
      private int[] H00EM2_A52Contratada_AreaTrabalhoCod ;
      private int[] H00EM3_A155Servico_Codigo ;
      private String[] H00EM3_A605Servico_Sigla ;
      private bool[] H00EM3_A632Servico_Ativo ;
      private bool[] H00EM3_n632Servico_Ativo ;
      private int[] H00EM4_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H00EM4_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H00EM4_A69ContratadaUsuario_UsuarioCod ;
      private String[] H00EM4_A71ContratadaUsuario_UsuarioPessoaNom ;
      private bool[] H00EM4_n71ContratadaUsuario_UsuarioPessoaNom ;
      private int[] H00EM4_A66ContratadaUsuario_ContratadaCod ;
      private bool[] H00EM4_A292Usuario_EhContratante ;
      private bool[] H00EM4_n292Usuario_EhContratante ;
      private bool[] H00EM4_A1093Usuario_EhPreposto ;
      private bool[] H00EM4_n1093Usuario_EhPreposto ;
      private String[] H00EM4_A516Contratada_TipoFabrica ;
      private bool[] H00EM4_n516Contratada_TipoFabrica ;
      private int[] H00EM5_A1595Contratada_AreaTrbSrvPdr ;
      private bool[] H00EM5_n1595Contratada_AreaTrbSrvPdr ;
      private int[] H00EM5_A1013Contrato_PrepostoCod ;
      private bool[] H00EM5_n1013Contrato_PrepostoCod ;
      private int[] H00EM5_A74Contrato_Codigo ;
      private int[] H00EM5_A52Contratada_AreaTrabalhoCod ;
      private String[] H00EM5_A516Contratada_TipoFabrica ;
      private bool[] H00EM5_n516Contratada_TipoFabrica ;
      private bool[] H00EM5_A54Usuario_Ativo ;
      private bool[] H00EM5_n54Usuario_Ativo ;
      private bool[] H00EM5_A292Usuario_EhContratante ;
      private bool[] H00EM5_n292Usuario_EhContratante ;
      private bool[] H00EM5_A1093Usuario_EhPreposto ;
      private bool[] H00EM5_n1093Usuario_EhPreposto ;
      private bool[] H00EM5_A632Servico_Ativo ;
      private bool[] H00EM5_n632Servico_Ativo ;
      private int[] H00EM5_A39Contratada_Codigo ;
      private int[] H00EM6_A160ContratoServicos_Codigo ;
      private int[] H00EM6_A74Contrato_Codigo ;
      private int[] H00EM6_A155Servico_Codigo ;
      private decimal[] H00EM6_A558Servico_Percentual ;
      private bool[] H00EM6_n558Servico_Percentual ;
      private short[] H00EM8_AV18Count ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class wp_aplicardeflator__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EM2 ;
          prmH00EM2 = new Object[] {
          new Object[] {"@AV8WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EM3 ;
          prmH00EM3 = new Object[] {
          } ;
          Object[] prmH00EM4 ;
          prmH00EM4 = new Object[] {
          new Object[] {"@AV8WWPCo_3Userehcontratante",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV8WWPCo_2Userehadministrador",SqlDbType.Bit,4,0} ,
          new Object[] {"@AV8WWPCo_4Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EM5 ;
          prmH00EM5 = new Object[] {
          new Object[] {"@AV7Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EM6 ;
          prmH00EM6 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV5Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00EM8 ;
          prmH00EM8 = new Object[] {
          new Object[] {"@AV7Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9DataIni",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV10DataFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20ContadorFM",SqlDbType.Int,6,0} ,
          new Object[] {"@AV5Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15Status",SqlDbType.Char,1,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EM2", "SELECT [Contratada_Codigo], [Contratada_Sigla], [Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_AreaTrabalhoCod] = @AV8WWPCo_1Areatrabalho_codigo ORDER BY [Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EM2,0,0,true,false )
             ,new CursorDef("H00EM3", "SELECT [Servico_Codigo], [Servico_Sigla], [Servico_Ativo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Ativo] = 1 ORDER BY [Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EM3,0,0,true,false )
             ,new CursorDef("H00EM4", "SELECT T4.[Pessoa_Codigo] AS ContratadaUsuario_UsuarioPesso, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T4.[Pessoa_Nome] AS ContratadaUsuario_UsuarioPessoaNom, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T3.[Usuario_EhContratante], T3.[Usuario_EhPreposto], T2.[Contratada_TipoFabrica] FROM ((([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE ( @AV8WWPCo_3Userehcontratante = 0 and @AV8WWPCo_2Userehadministrador = 0 and T1.[ContratadaUsuario_ContratadaCod] = @AV8WWPCo_4Contratada_codigo) or ( @AV8WWPCo_3Userehcontratante = 1 and ( T3.[Usuario_EhContratante] = 1 or T3.[Usuario_EhPreposto] = 1)) or ( @AV8WWPCo_2Userehadministrador = 1 and T2.[Contratada_TipoFabrica] = 'M') ORDER BY T4.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EM4,0,0,true,false )
             ,new CursorDef("H00EM5", "SELECT T4.[AreaTrabalho_ServicoPadrao] AS Contratada_AreaTrbSrvPdr, T1.[Contrato_PrepostoCod] AS Contrato_PrepostoCod, T1.[Contrato_Codigo], T3.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T3.[Contratada_TipoFabrica], T2.[Usuario_Ativo], T2.[Usuario_EhContratante], T2.[Usuario_EhPreposto], T5.[Servico_Ativo], T1.[Contratada_Codigo] FROM (((([Contrato] T1 WITH (NOLOCK) LEFT JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Contrato_PrepostoCod]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[Contratada_Codigo]) INNER JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[AreaTrabalho_ServicoPadrao]) WHERE T1.[Contratada_Codigo] = @AV7Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EM5,100,0,true,false )
             ,new CursorDef("H00EM6", "SELECT [ContratoServicos_Codigo], [Contrato_Codigo], [Servico_Codigo], [Servico_Percentual] FROM [ContratoServicos] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @Contrato_Codigo) AND ([Servico_Codigo] = @AV5Servico_Codigo) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EM6,100,0,false,false )
             ,new CursorDef("H00EM8", "SELECT COUNT(*) FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo], MIN([ContagemResultado_DataCnt]) AS ContagemResultado_DataUltCnt FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE (T1.[ContagemResultado_ContratadaOrigemCod] = @AV7Contratada_Codigo) AND (COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) >= @AV9DataIni) AND (COALESCE( T3.[ContagemResultado_DataUltCnt], convert( DATETIME, '17530101', 112 )) <= @AV10DataFim) AND ((@AV20ContadorFM = convert(int, 0)) or ( COALESCE( T3.[ContagemResultado_ContadorFM], 0) = @AV20ContadorFM)) AND (T2.[Servico_Codigo] = @AV5Servico_Codigo) AND (T1.[ContagemResultado_StatusDmn] = @AV15Status) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EM8,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((bool[]) buf[9])[0] = rslt.getBool(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((bool[]) buf[13])[0] = rslt.getBool(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (bool)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                return;
       }
    }

 }

}
