/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 0:21:23.55
*/
gx.evt.autoSkip = false;
gx.define('contratoservicosvncgeneral', true, function (CmpContext) {
   this.ServerClass =  "contratoservicosvncgeneral" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.setCmpContext(CmpContext);
   this.ReadonlyForm = true;
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.ajaxSecurityToken =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV12ContratoSrvVnc_CntSrvCod=gx.fn.getIntegerValue("vCONTRATOSRVVNC_CNTSRVCOD",'.') ;
   };
   this.Valid_Contratosrvvnc_codigo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("CONTRATOSRVVNC_CODIGO");
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e13ge2_client=function()
   {
      this.executeServerEvent("'DOUPDATE'", false, null, false, false);
   };
   this.e14ge2_client=function()
   {
      this.executeServerEvent("'DODELETE'", false, null, false, false);
   };
   this.e15ge2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e16ge2_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,8,11,13,15,17,20,22,24,26,29,31,33,35,38,40,42,44,47,49,54,56,58,60,63,65,67,69,72,74,77,79,81,83,86,88,93,99];
   this.GXLastCtrlId =99;
   GXValidFnc[2]={fld:"TABLECONTENT",grid:0};
   GXValidFnc[8]={fld:"TABLEATTRIBUTES",grid:0};
   GXValidFnc[11]={fld:"TEXTBLOCKCONTRATOSRVVNC_SERVICOSIGLA", format:0,grid:0};
   GXValidFnc[13]={lvl:0,type:"char",len:15,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSRVVNC_SERVICOSIGLA",gxz:"Z922ContratoSrvVnc_ServicoSigla",gxold:"O922ContratoSrvVnc_ServicoSigla",gxvar:"A922ContratoSrvVnc_ServicoSigla",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A922ContratoSrvVnc_ServicoSigla=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z922ContratoSrvVnc_ServicoSigla=Value},v2c:function(){gx.fn.setControlValue("CONTRATOSRVVNC_SERVICOSIGLA",gx.O.A922ContratoSrvVnc_ServicoSigla,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A922ContratoSrvVnc_ServicoSigla=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSRVVNC_SERVICOSIGLA")},nac:gx.falseFn};
   this.declareDomainHdlr( 13 , function() {
   });
   GXValidFnc[15]={fld:"TEXTBLOCKCONTRATOSERVICOSVNC_DESCRICAO", format:0,grid:0};
   GXValidFnc[17]={lvl:0,type:"svchar",len:50,dec:0,sign:false,pic:"@!",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSVNC_DESCRICAO",gxz:"Z2108ContratoServicosVnc_Descricao",gxold:"O2108ContratoServicosVnc_Descricao",gxvar:"A2108ContratoServicosVnc_Descricao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A2108ContratoServicosVnc_Descricao=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z2108ContratoServicosVnc_Descricao=Value},v2c:function(){gx.fn.setControlValue("CONTRATOSERVICOSVNC_DESCRICAO",gx.O.A2108ContratoServicosVnc_Descricao,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A2108ContratoServicosVnc_Descricao=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSVNC_DESCRICAO")},nac:gx.falseFn};
   this.declareDomainHdlr( 17 , function() {
   });
   GXValidFnc[20]={fld:"TEXTBLOCKCONTRATOSRVVNC_DOSTATUSDMN", format:0,grid:0};
   GXValidFnc[22]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSRVVNC_DOSTATUSDMN",gxz:"Z1800ContratoSrvVnc_DoStatusDmn",gxold:"O1800ContratoSrvVnc_DoStatusDmn",gxvar:"A1800ContratoSrvVnc_DoStatusDmn",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1800ContratoSrvVnc_DoStatusDmn=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1800ContratoSrvVnc_DoStatusDmn=Value},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSRVVNC_DOSTATUSDMN",gx.O.A1800ContratoSrvVnc_DoStatusDmn);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1800ContratoSrvVnc_DoStatusDmn=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSRVVNC_DOSTATUSDMN")},nac:gx.falseFn};
   this.declareDomainHdlr( 22 , function() {
   });
   GXValidFnc[24]={fld:"TEXTBLOCKCONTRATOSRVVNC_STATUSDMN", format:0,grid:0};
   GXValidFnc[26]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSRVVNC_STATUSDMN",gxz:"Z1084ContratoSrvVnc_StatusDmn",gxold:"O1084ContratoSrvVnc_StatusDmn",gxvar:"A1084ContratoSrvVnc_StatusDmn",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1084ContratoSrvVnc_StatusDmn=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1084ContratoSrvVnc_StatusDmn=Value},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSRVVNC_STATUSDMN",gx.O.A1084ContratoSrvVnc_StatusDmn);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1084ContratoSrvVnc_StatusDmn=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSRVVNC_STATUSDMN")},nac:gx.falseFn};
   this.declareDomainHdlr( 26 , function() {
   });
   GXValidFnc[29]={fld:"TEXTBLOCKCONTRATOSRVVNC_PRESTADORACOD", format:0,grid:0};
   GXValidFnc[31]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSRVVNC_PRESTADORACOD",gxz:"Z1088ContratoSrvVnc_PrestadoraCod",gxold:"O1088ContratoSrvVnc_PrestadoraCod",gxvar:"A1088ContratoSrvVnc_PrestadoraCod",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1088ContratoSrvVnc_PrestadoraCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1088ContratoSrvVnc_PrestadoraCod=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSRVVNC_PRESTADORACOD",gx.O.A1088ContratoSrvVnc_PrestadoraCod);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1088ContratoSrvVnc_PrestadoraCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATOSRVVNC_PRESTADORACOD",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 31 , function() {
   });
   GXValidFnc[33]={fld:"TEXTBLOCKCONTRATOSRVVNC_SRVVNCSTATUS", format:0,grid:0};
   GXValidFnc[35]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSRVVNC_SRVVNCSTATUS",gxz:"Z1663ContratoSrvVnc_SrvVncStatus",gxold:"O1663ContratoSrvVnc_SrvVncStatus",gxvar:"A1663ContratoSrvVnc_SrvVncStatus",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1663ContratoSrvVnc_SrvVncStatus=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1663ContratoSrvVnc_SrvVncStatus=Value},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSRVVNC_SRVVNCSTATUS",gx.O.A1663ContratoSrvVnc_SrvVncStatus);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1663ContratoSrvVnc_SrvVncStatus=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSRVVNC_SRVVNCSTATUS")},nac:gx.falseFn};
   this.declareDomainHdlr( 35 , function() {
   });
   GXValidFnc[38]={fld:"TEXTBLOCKCONTRATOSRVVNC_VINCULARCOM", format:0,grid:0};
   GXValidFnc[40]={lvl:0,type:"char",len:20,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSRVVNC_VINCULARCOM",gxz:"Z1745ContratoSrvVnc_VincularCom",gxold:"O1745ContratoSrvVnc_VincularCom",gxvar:"A1745ContratoSrvVnc_VincularCom",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1745ContratoSrvVnc_VincularCom=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1745ContratoSrvVnc_VincularCom=Value},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSRVVNC_VINCULARCOM",gx.O.A1745ContratoSrvVnc_VincularCom)},c2v:function(){if(this.val()!==undefined)gx.O.A1745ContratoSrvVnc_VincularCom=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSRVVNC_VINCULARCOM")},nac:gx.falseFn};
   GXValidFnc[42]={fld:"TEXTBLOCKCONTRATOSRVVNC_SRVVNCREF", format:0,grid:0};
   GXValidFnc[44]={lvl:0,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSRVVNC_SRVVNCREF",gxz:"Z1821ContratoSrvVnc_SrvVncRef",gxold:"O1821ContratoSrvVnc_SrvVncRef",gxvar:"A1821ContratoSrvVnc_SrvVncRef",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1821ContratoSrvVnc_SrvVncRef=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1821ContratoSrvVnc_SrvVncRef=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSRVVNC_SRVVNCREF",gx.O.A1821ContratoSrvVnc_SrvVncRef)},c2v:function(){if(this.val()!==undefined)gx.O.A1821ContratoSrvVnc_SrvVncRef=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATOSRVVNC_SRVVNCREF",'.')},nac:gx.falseFn};
   GXValidFnc[47]={fld:"TEXTBLOCKCONTRATOSERVICOSVNC_GESTOR", format:0,grid:0};
   GXValidFnc[49]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSVNC_GESTOR",gxz:"Z1438ContratoServicosVnc_Gestor",gxold:"O1438ContratoServicosVnc_Gestor",gxvar:"A1438ContratoServicosVnc_Gestor",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1438ContratoServicosVnc_Gestor=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1438ContratoServicosVnc_Gestor=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSERVICOSVNC_GESTOR",gx.O.A1438ContratoServicosVnc_Gestor);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1438ContratoServicosVnc_Gestor=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATOSERVICOSVNC_GESTOR",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 49 , function() {
   });
   GXValidFnc[54]={fld:"TEXTBLOCKCONTRATOSRVVNC_NOVOSTATUSDMN", format:0,grid:0};
   GXValidFnc[56]={lvl:0,type:"char",len:1,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSRVVNC_NOVOSTATUSDMN",gxz:"Z1743ContratoSrvVnc_NovoStatusDmn",gxold:"O1743ContratoSrvVnc_NovoStatusDmn",gxvar:"A1743ContratoSrvVnc_NovoStatusDmn",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1743ContratoSrvVnc_NovoStatusDmn=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1743ContratoSrvVnc_NovoStatusDmn=Value},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSRVVNC_NOVOSTATUSDMN",gx.O.A1743ContratoSrvVnc_NovoStatusDmn);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1743ContratoSrvVnc_NovoStatusDmn=this.val()},val:function(){return gx.fn.getControlValue("CONTRATOSRVVNC_NOVOSTATUSDMN")},nac:gx.falseFn};
   this.declareDomainHdlr( 56 , function() {
   });
   GXValidFnc[58]={fld:"TEXTBLOCKCONTRATOSRVVNC_NOVORSPDMN", format:0,grid:0};
   GXValidFnc[60]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSRVVNC_NOVORSPDMN",gxz:"Z1801ContratoSrvVnc_NovoRspDmn",gxold:"O1801ContratoSrvVnc_NovoRspDmn",gxvar:"A1801ContratoSrvVnc_NovoRspDmn",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1801ContratoSrvVnc_NovoRspDmn=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1801ContratoSrvVnc_NovoRspDmn=gx.num.intval(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSRVVNC_NOVORSPDMN",gx.O.A1801ContratoSrvVnc_NovoRspDmn);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1801ContratoSrvVnc_NovoRspDmn=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATOSRVVNC_NOVORSPDMN",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 60 , function() {
   });
   GXValidFnc[63]={fld:"TEXTBLOCKCONTRATOSERVICOSVNC_CLONASRVORI", format:0,grid:0};
   GXValidFnc[65]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSVNC_CLONASRVORI",gxz:"Z1145ContratoServicosVnc_ClonaSrvOri",gxold:"O1145ContratoServicosVnc_ClonaSrvOri",gxvar:"A1145ContratoServicosVnc_ClonaSrvOri",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1145ContratoServicosVnc_ClonaSrvOri=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1145ContratoServicosVnc_ClonaSrvOri=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSERVICOSVNC_CLONASRVORI",gx.O.A1145ContratoServicosVnc_ClonaSrvOri);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1145ContratoServicosVnc_ClonaSrvOri=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSVNC_CLONASRVORI")},nac:gx.falseFn};
   this.declareDomainHdlr( 65 , function() {
   });
   GXValidFnc[67]={fld:"TEXTBLOCKCONTRATOSERVICOSVNC_CLONARLINK", format:0,grid:0};
   GXValidFnc[69]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSVNC_CLONARLINK",gxz:"Z1818ContratoServicosVnc_ClonarLink",gxold:"O1818ContratoServicosVnc_ClonarLink",gxvar:"A1818ContratoServicosVnc_ClonarLink",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1818ContratoServicosVnc_ClonarLink=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1818ContratoServicosVnc_ClonarLink=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSERVICOSVNC_CLONARLINK",gx.O.A1818ContratoServicosVnc_ClonarLink);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1818ContratoServicosVnc_ClonarLink=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSVNC_CLONARLINK")},nac:gx.falseFn};
   this.declareDomainHdlr( 69 , function() {
   });
   GXValidFnc[72]={fld:"TEXTBLOCKCONTRATOSERVICOSVNC_NAOCLONAINFO", format:0,grid:0};
   GXValidFnc[74]={fld:"TABLEMERGEDCONTRATOSERVICOSVNC_NAOCLONAINFO",grid:0};
   GXValidFnc[77]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSVNC_NAOCLONAINFO",gxz:"Z1437ContratoServicosVnc_NaoClonaInfo",gxold:"O1437ContratoServicosVnc_NaoClonaInfo",gxvar:"A1437ContratoServicosVnc_NaoClonaInfo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A1437ContratoServicosVnc_NaoClonaInfo=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1437ContratoServicosVnc_NaoClonaInfo=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("CONTRATOSERVICOSVNC_NAOCLONAINFO",gx.O.A1437ContratoServicosVnc_NaoClonaInfo,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1437ContratoServicosVnc_NaoClonaInfo=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSVNC_NAOCLONAINFO")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 77 , function() {
   });
   GXValidFnc[79]={fld:"CONTRATOSERVICOSVNC_NAOCLONAINFO_RIGHTTEXT", format:0,grid:0};
   GXValidFnc[81]={fld:"TEXTBLOCKCONTRATOSRVVNC_SEMCUSTO", format:0,grid:0};
   GXValidFnc[83]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSRVVNC_SEMCUSTO",gxz:"Z1090ContratoSrvVnc_SemCusto",gxold:"O1090ContratoSrvVnc_SemCusto",gxvar:"A1090ContratoSrvVnc_SemCusto",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",v2v:function(Value){if(Value!==undefined)gx.O.A1090ContratoSrvVnc_SemCusto=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1090ContratoSrvVnc_SemCusto=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setComboBoxValue("CONTRATOSRVVNC_SEMCUSTO",gx.O.A1090ContratoSrvVnc_SemCusto);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1090ContratoSrvVnc_SemCusto=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("CONTRATOSRVVNC_SEMCUSTO")},nac:gx.falseFn};
   this.declareDomainHdlr( 83 , function() {
   });
   GXValidFnc[86]={fld:"TEXTBLOCKCONTRATOSERVICOSVNC_ATIVO", format:0,grid:0};
   GXValidFnc[88]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:1,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"CONTRATOSERVICOSVNC_ATIVO",gxz:"Z1453ContratoServicosVnc_Ativo",gxold:"O1453ContratoServicosVnc_Ativo",gxvar:"A1453ContratoServicosVnc_Ativo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.A1453ContratoServicosVnc_Ativo=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1453ContratoServicosVnc_Ativo=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("CONTRATOSERVICOSVNC_ATIVO",gx.O.A1453ContratoServicosVnc_Ativo,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1453ContratoServicosVnc_Ativo=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("CONTRATOSERVICOSVNC_ATIVO")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 88 , function() {
   });
   GXValidFnc[93]={fld:"TABLEACTIONS",grid:0};
   GXValidFnc[99]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Contratosrvvnc_codigo,isvalid:null,rgrid:[],fld:"CONTRATOSRVVNC_CODIGO",gxz:"Z917ContratoSrvVnc_Codigo",gxold:"O917ContratoSrvVnc_Codigo",gxvar:"A917ContratoSrvVnc_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A917ContratoSrvVnc_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z917ContratoSrvVnc_Codigo=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("CONTRATOSRVVNC_CODIGO",gx.O.A917ContratoSrvVnc_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A917ContratoSrvVnc_Codigo=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("CONTRATOSRVVNC_CODIGO",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 99 , function() {
   });
   this.A922ContratoSrvVnc_ServicoSigla = "" ;
   this.Z922ContratoSrvVnc_ServicoSigla = "" ;
   this.O922ContratoSrvVnc_ServicoSigla = "" ;
   this.A2108ContratoServicosVnc_Descricao = "" ;
   this.Z2108ContratoServicosVnc_Descricao = "" ;
   this.O2108ContratoServicosVnc_Descricao = "" ;
   this.A1800ContratoSrvVnc_DoStatusDmn = "" ;
   this.Z1800ContratoSrvVnc_DoStatusDmn = "" ;
   this.O1800ContratoSrvVnc_DoStatusDmn = "" ;
   this.A1084ContratoSrvVnc_StatusDmn = "" ;
   this.Z1084ContratoSrvVnc_StatusDmn = "" ;
   this.O1084ContratoSrvVnc_StatusDmn = "" ;
   this.A1088ContratoSrvVnc_PrestadoraCod = 0 ;
   this.Z1088ContratoSrvVnc_PrestadoraCod = 0 ;
   this.O1088ContratoSrvVnc_PrestadoraCod = 0 ;
   this.A1663ContratoSrvVnc_SrvVncStatus = "" ;
   this.Z1663ContratoSrvVnc_SrvVncStatus = "" ;
   this.O1663ContratoSrvVnc_SrvVncStatus = "" ;
   this.A1745ContratoSrvVnc_VincularCom = "" ;
   this.Z1745ContratoSrvVnc_VincularCom = "" ;
   this.O1745ContratoSrvVnc_VincularCom = "" ;
   this.A1821ContratoSrvVnc_SrvVncRef = 0 ;
   this.Z1821ContratoSrvVnc_SrvVncRef = 0 ;
   this.O1821ContratoSrvVnc_SrvVncRef = 0 ;
   this.A1438ContratoServicosVnc_Gestor = 0 ;
   this.Z1438ContratoServicosVnc_Gestor = 0 ;
   this.O1438ContratoServicosVnc_Gestor = 0 ;
   this.A1743ContratoSrvVnc_NovoStatusDmn = "" ;
   this.Z1743ContratoSrvVnc_NovoStatusDmn = "" ;
   this.O1743ContratoSrvVnc_NovoStatusDmn = "" ;
   this.A1801ContratoSrvVnc_NovoRspDmn = 0 ;
   this.Z1801ContratoSrvVnc_NovoRspDmn = 0 ;
   this.O1801ContratoSrvVnc_NovoRspDmn = 0 ;
   this.A1145ContratoServicosVnc_ClonaSrvOri = false ;
   this.Z1145ContratoServicosVnc_ClonaSrvOri = false ;
   this.O1145ContratoServicosVnc_ClonaSrvOri = false ;
   this.A1818ContratoServicosVnc_ClonarLink = false ;
   this.Z1818ContratoServicosVnc_ClonarLink = false ;
   this.O1818ContratoServicosVnc_ClonarLink = false ;
   this.A1437ContratoServicosVnc_NaoClonaInfo = false ;
   this.Z1437ContratoServicosVnc_NaoClonaInfo = false ;
   this.O1437ContratoServicosVnc_NaoClonaInfo = false ;
   this.A1090ContratoSrvVnc_SemCusto = false ;
   this.Z1090ContratoSrvVnc_SemCusto = false ;
   this.O1090ContratoSrvVnc_SemCusto = false ;
   this.A1453ContratoServicosVnc_Ativo = false ;
   this.Z1453ContratoServicosVnc_Ativo = false ;
   this.O1453ContratoServicosVnc_Ativo = false ;
   this.A917ContratoSrvVnc_Codigo = 0 ;
   this.Z917ContratoSrvVnc_Codigo = 0 ;
   this.O917ContratoSrvVnc_Codigo = 0 ;
   this.A922ContratoSrvVnc_ServicoSigla = "" ;
   this.A2108ContratoServicosVnc_Descricao = "" ;
   this.A1800ContratoSrvVnc_DoStatusDmn = "" ;
   this.A1084ContratoSrvVnc_StatusDmn = "" ;
   this.A1088ContratoSrvVnc_PrestadoraCod = 0 ;
   this.A1663ContratoSrvVnc_SrvVncStatus = "" ;
   this.A1745ContratoSrvVnc_VincularCom = "" ;
   this.A1821ContratoSrvVnc_SrvVncRef = 0 ;
   this.A1438ContratoServicosVnc_Gestor = 0 ;
   this.A1743ContratoSrvVnc_NovoStatusDmn = "" ;
   this.A1801ContratoSrvVnc_NovoRspDmn = 0 ;
   this.A1145ContratoServicosVnc_ClonaSrvOri = false ;
   this.A1818ContratoServicosVnc_ClonarLink = false ;
   this.A1437ContratoServicosVnc_NaoClonaInfo = false ;
   this.A1090ContratoSrvVnc_SemCusto = false ;
   this.A1453ContratoServicosVnc_Ativo = false ;
   this.A917ContratoSrvVnc_Codigo = 0 ;
   this.A915ContratoSrvVnc_CntSrvCod = 0 ;
   this.A921ContratoSrvVnc_ServicoCod = 0 ;
   this.AV12ContratoSrvVnc_CntSrvCod = 0 ;
   this.Events = {"e13ge2_client": ["'DOUPDATE'", true] ,"e14ge2_client": ["'DODELETE'", true] ,"e15ge2_client": ["ENTER", true] ,"e16ge2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.EvtParms["LOAD"] = [[],[{av:'gx.fn.getCtrlProperty("CONTRATOSRVVNC_CODIGO","Visible")',ctrl:'CONTRATOSRVVNC_CODIGO',prop:'Visible'}]];
   this.EvtParms["'DOUPDATE'"] = [[{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],[{av:'AV12ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["'DODELETE'"] = [[{av:'A917ContratoSrvVnc_Codigo',fld:'CONTRATOSRVVNC_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],[{av:'AV12ContratoSrvVnc_CntSrvCod',fld:'vCONTRATOSRVVNC_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]];
   this.setVCMap("AV12ContratoSrvVnc_CntSrvCod", "vCONTRATOSRVVNC_CNTSRVCOD", 0, "int");
   this.InitStandaloneVars( );
});
