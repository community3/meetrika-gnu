/*
               File: GetWWBancoAgenciaFilterData
        Description: Get WWBanco Agencia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:18.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwbancoagenciafilterdata : GXProcedure
   {
      public getwwbancoagenciafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwbancoagenciafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV22DDOName = aP0_DDOName;
         this.AV20SearchTxt = aP1_SearchTxt;
         this.AV21SearchTxtTo = aP2_SearchTxtTo;
         this.AV26OptionsJson = "" ;
         this.AV29OptionsDescJson = "" ;
         this.AV31OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
         return AV31OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwbancoagenciafilterdata objgetwwbancoagenciafilterdata;
         objgetwwbancoagenciafilterdata = new getwwbancoagenciafilterdata();
         objgetwwbancoagenciafilterdata.AV22DDOName = aP0_DDOName;
         objgetwwbancoagenciafilterdata.AV20SearchTxt = aP1_SearchTxt;
         objgetwwbancoagenciafilterdata.AV21SearchTxtTo = aP2_SearchTxtTo;
         objgetwwbancoagenciafilterdata.AV26OptionsJson = "" ;
         objgetwwbancoagenciafilterdata.AV29OptionsDescJson = "" ;
         objgetwwbancoagenciafilterdata.AV31OptionIndexesJson = "" ;
         objgetwwbancoagenciafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwbancoagenciafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwbancoagenciafilterdata);
         aP3_OptionsJson=this.AV26OptionsJson;
         aP4_OptionsDescJson=this.AV29OptionsDescJson;
         aP5_OptionIndexesJson=this.AV31OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwbancoagenciafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV25Options = (IGxCollection)(new GxSimpleCollection());
         AV28OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV30OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_BANCOAGENCIA_AGENCIA") == 0 )
         {
            /* Execute user subroutine: 'LOADBANCOAGENCIA_AGENCIAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_BANCOAGENCIA_NOMEAGENCIA") == 0 )
         {
            /* Execute user subroutine: 'LOADBANCOAGENCIA_NOMEAGENCIAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_BANCO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADBANCO_NOMEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV22DDOName), "DDO_MUNICIPIO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADMUNICIPIO_NOMEOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV26OptionsJson = AV25Options.ToJSonString(false);
         AV29OptionsDescJson = AV28OptionsDesc.ToJSonString(false);
         AV31OptionIndexesJson = AV30OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get("WWBancoAgenciaGridState"), "") == 0 )
         {
            AV35GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWBancoAgenciaGridState"), "");
         }
         else
         {
            AV35GridState.FromXml(AV33Session.Get("WWBancoAgenciaGridState"), "");
         }
         AV57GXV1 = 1;
         while ( AV57GXV1 <= AV35GridState.gxTpr_Filtervalues.Count )
         {
            AV36GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV35GridState.gxTpr_Filtervalues.Item(AV57GXV1));
            if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFBANCOAGENCIA_CODIGO") == 0 )
            {
               AV10TFBancoAgencia_Codigo = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Value, "."));
               AV11TFBancoAgencia_Codigo_To = (int)(NumberUtil.Val( AV36GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFBANCOAGENCIA_AGENCIA") == 0 )
            {
               AV12TFBancoAgencia_Agencia = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFBANCOAGENCIA_AGENCIA_SEL") == 0 )
            {
               AV13TFBancoAgencia_Agencia_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFBANCOAGENCIA_NOMEAGENCIA") == 0 )
            {
               AV14TFBancoAgencia_NomeAgencia = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFBANCOAGENCIA_NOMEAGENCIA_SEL") == 0 )
            {
               AV15TFBancoAgencia_NomeAgencia_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFBANCO_NOME") == 0 )
            {
               AV16TFBanco_Nome = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFBANCO_NOME_SEL") == 0 )
            {
               AV17TFBanco_Nome_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME") == 0 )
            {
               AV18TFMunicipio_Nome = AV36GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME_SEL") == 0 )
            {
               AV19TFMunicipio_Nome_Sel = AV36GridStateFilterValue.gxTpr_Value;
            }
            AV57GXV1 = (int)(AV57GXV1+1);
         }
         if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(1));
            AV38DynamicFiltersSelector1 = AV37GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "BANCOAGENCIA_AGENCIA") == 0 )
            {
               AV39DynamicFiltersOperator1 = AV37GridStateDynamicFilter.gxTpr_Operator;
               AV40BancoAgencia_Agencia1 = AV37GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "BANCO_NOME") == 0 )
            {
               AV39DynamicFiltersOperator1 = AV37GridStateDynamicFilter.gxTpr_Operator;
               AV41Banco_Nome1 = AV37GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38DynamicFiltersSelector1, "MUNICIPIO_NOME") == 0 )
            {
               AV39DynamicFiltersOperator1 = AV37GridStateDynamicFilter.gxTpr_Operator;
               AV42Municipio_Nome1 = AV37GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV43DynamicFiltersEnabled2 = true;
               AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(2));
               AV44DynamicFiltersSelector2 = AV37GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "BANCOAGENCIA_AGENCIA") == 0 )
               {
                  AV45DynamicFiltersOperator2 = AV37GridStateDynamicFilter.gxTpr_Operator;
                  AV46BancoAgencia_Agencia2 = AV37GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "BANCO_NOME") == 0 )
               {
                  AV45DynamicFiltersOperator2 = AV37GridStateDynamicFilter.gxTpr_Operator;
                  AV47Banco_Nome2 = AV37GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector2, "MUNICIPIO_NOME") == 0 )
               {
                  AV45DynamicFiltersOperator2 = AV37GridStateDynamicFilter.gxTpr_Operator;
                  AV48Municipio_Nome2 = AV37GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV35GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV49DynamicFiltersEnabled3 = true;
                  AV37GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV35GridState.gxTpr_Dynamicfilters.Item(3));
                  AV50DynamicFiltersSelector3 = AV37GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "BANCOAGENCIA_AGENCIA") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV37GridStateDynamicFilter.gxTpr_Operator;
                     AV52BancoAgencia_Agencia3 = AV37GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "BANCO_NOME") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV37GridStateDynamicFilter.gxTpr_Operator;
                     AV53Banco_Nome3 = AV37GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "MUNICIPIO_NOME") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV37GridStateDynamicFilter.gxTpr_Operator;
                     AV54Municipio_Nome3 = AV37GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADBANCOAGENCIA_AGENCIAOPTIONS' Routine */
         AV12TFBancoAgencia_Agencia = AV20SearchTxt;
         AV13TFBancoAgencia_Agencia_Sel = "";
         AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 = AV38DynamicFiltersSelector1;
         AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 = AV39DynamicFiltersOperator1;
         AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = AV40BancoAgencia_Agencia1;
         AV62WWBancoAgenciaDS_4_Banco_nome1 = AV41Banco_Nome1;
         AV63WWBancoAgenciaDS_5_Municipio_nome1 = AV42Municipio_Nome1;
         AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = AV46BancoAgencia_Agencia2;
         AV68WWBancoAgenciaDS_10_Banco_nome2 = AV47Banco_Nome2;
         AV69WWBancoAgenciaDS_11_Municipio_nome2 = AV48Municipio_Nome2;
         AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = AV52BancoAgencia_Agencia3;
         AV74WWBancoAgenciaDS_16_Banco_nome3 = AV53Banco_Nome3;
         AV75WWBancoAgenciaDS_17_Municipio_nome3 = AV54Municipio_Nome3;
         AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo = AV10TFBancoAgencia_Codigo;
         AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to = AV11TFBancoAgencia_Codigo_To;
         AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia = AV12TFBancoAgencia_Agencia;
         AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = AV13TFBancoAgencia_Agencia_Sel;
         AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = AV14TFBancoAgencia_NomeAgencia;
         AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = AV15TFBancoAgencia_NomeAgencia_Sel;
         AV82WWBancoAgenciaDS_24_Tfbanco_nome = AV16TFBanco_Nome;
         AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel = AV17TFBanco_Nome_Sel;
         AV84WWBancoAgenciaDS_26_Tfmunicipio_nome = AV18TFMunicipio_Nome;
         AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = AV19TFMunicipio_Nome_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                              AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                              AV62WWBancoAgenciaDS_4_Banco_nome1 ,
                                              AV63WWBancoAgenciaDS_5_Municipio_nome1 ,
                                              AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                              AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                              AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                              AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                              AV68WWBancoAgenciaDS_10_Banco_nome2 ,
                                              AV69WWBancoAgenciaDS_11_Municipio_nome2 ,
                                              AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                              AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                              AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                              AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                              AV74WWBancoAgenciaDS_16_Banco_nome3 ,
                                              AV75WWBancoAgenciaDS_17_Municipio_nome3 ,
                                              AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                              AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                              AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                              AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                              AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                              AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                              AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                              AV82WWBancoAgenciaDS_24_Tfbanco_nome ,
                                              AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                              AV84WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                              A27BancoAgencia_Agencia ,
                                              A20Banco_Nome ,
                                              A26Municipio_Nome ,
                                              A17BancoAgencia_Codigo ,
                                              A28BancoAgencia_NomeAgencia },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
         lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
         lV62WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
         lV62WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
         lV63WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
         lV63WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
         lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
         lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
         lV68WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
         lV68WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
         lV69WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
         lV69WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
         lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
         lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
         lV74WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
         lV74WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
         lV75WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
         lV75WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
         lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia = StringUtil.PadR( StringUtil.RTrim( AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia), 10, "%");
         lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = StringUtil.PadR( StringUtil.RTrim( AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia), 50, "%");
         lV82WWBancoAgenciaDS_24_Tfbanco_nome = StringUtil.PadR( StringUtil.RTrim( AV82WWBancoAgenciaDS_24_Tfbanco_nome), 50, "%");
         lV84WWBancoAgenciaDS_26_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV84WWBancoAgenciaDS_26_Tfmunicipio_nome), 50, "%");
         /* Using cursor P00F02 */
         pr_default.execute(0, new Object[] {lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV62WWBancoAgenciaDS_4_Banco_nome1, lV62WWBancoAgenciaDS_4_Banco_nome1, lV63WWBancoAgenciaDS_5_Municipio_nome1, lV63WWBancoAgenciaDS_5_Municipio_nome1, lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV68WWBancoAgenciaDS_10_Banco_nome2, lV68WWBancoAgenciaDS_10_Banco_nome2, lV69WWBancoAgenciaDS_11_Municipio_nome2, lV69WWBancoAgenciaDS_11_Municipio_nome2, lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV74WWBancoAgenciaDS_16_Banco_nome3, lV74WWBancoAgenciaDS_16_Banco_nome3, lV75WWBancoAgenciaDS_17_Municipio_nome3, lV75WWBancoAgenciaDS_17_Municipio_nome3, AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo, AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to, lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia, AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel, lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia, AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel, lV82WWBancoAgenciaDS_24_Tfbanco_nome, AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel, lV84WWBancoAgenciaDS_26_Tfmunicipio_nome, AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKF02 = false;
            A18Banco_Codigo = P00F02_A18Banco_Codigo[0];
            A25Municipio_Codigo = P00F02_A25Municipio_Codigo[0];
            A27BancoAgencia_Agencia = P00F02_A27BancoAgencia_Agencia[0];
            A28BancoAgencia_NomeAgencia = P00F02_A28BancoAgencia_NomeAgencia[0];
            A17BancoAgencia_Codigo = P00F02_A17BancoAgencia_Codigo[0];
            A26Municipio_Nome = P00F02_A26Municipio_Nome[0];
            A20Banco_Nome = P00F02_A20Banco_Nome[0];
            A20Banco_Nome = P00F02_A20Banco_Nome[0];
            A26Municipio_Nome = P00F02_A26Municipio_Nome[0];
            AV32count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00F02_A27BancoAgencia_Agencia[0], A27BancoAgencia_Agencia) == 0 ) )
            {
               BRKF02 = false;
               A17BancoAgencia_Codigo = P00F02_A17BancoAgencia_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKF02 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A27BancoAgencia_Agencia)) )
            {
               AV24Option = A27BancoAgencia_Agencia;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF02 )
            {
               BRKF02 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADBANCOAGENCIA_NOMEAGENCIAOPTIONS' Routine */
         AV14TFBancoAgencia_NomeAgencia = AV20SearchTxt;
         AV15TFBancoAgencia_NomeAgencia_Sel = "";
         AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 = AV38DynamicFiltersSelector1;
         AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 = AV39DynamicFiltersOperator1;
         AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = AV40BancoAgencia_Agencia1;
         AV62WWBancoAgenciaDS_4_Banco_nome1 = AV41Banco_Nome1;
         AV63WWBancoAgenciaDS_5_Municipio_nome1 = AV42Municipio_Nome1;
         AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = AV46BancoAgencia_Agencia2;
         AV68WWBancoAgenciaDS_10_Banco_nome2 = AV47Banco_Nome2;
         AV69WWBancoAgenciaDS_11_Municipio_nome2 = AV48Municipio_Nome2;
         AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = AV52BancoAgencia_Agencia3;
         AV74WWBancoAgenciaDS_16_Banco_nome3 = AV53Banco_Nome3;
         AV75WWBancoAgenciaDS_17_Municipio_nome3 = AV54Municipio_Nome3;
         AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo = AV10TFBancoAgencia_Codigo;
         AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to = AV11TFBancoAgencia_Codigo_To;
         AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia = AV12TFBancoAgencia_Agencia;
         AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = AV13TFBancoAgencia_Agencia_Sel;
         AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = AV14TFBancoAgencia_NomeAgencia;
         AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = AV15TFBancoAgencia_NomeAgencia_Sel;
         AV82WWBancoAgenciaDS_24_Tfbanco_nome = AV16TFBanco_Nome;
         AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel = AV17TFBanco_Nome_Sel;
         AV84WWBancoAgenciaDS_26_Tfmunicipio_nome = AV18TFMunicipio_Nome;
         AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = AV19TFMunicipio_Nome_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                              AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                              AV62WWBancoAgenciaDS_4_Banco_nome1 ,
                                              AV63WWBancoAgenciaDS_5_Municipio_nome1 ,
                                              AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                              AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                              AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                              AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                              AV68WWBancoAgenciaDS_10_Banco_nome2 ,
                                              AV69WWBancoAgenciaDS_11_Municipio_nome2 ,
                                              AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                              AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                              AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                              AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                              AV74WWBancoAgenciaDS_16_Banco_nome3 ,
                                              AV75WWBancoAgenciaDS_17_Municipio_nome3 ,
                                              AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                              AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                              AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                              AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                              AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                              AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                              AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                              AV82WWBancoAgenciaDS_24_Tfbanco_nome ,
                                              AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                              AV84WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                              A27BancoAgencia_Agencia ,
                                              A20Banco_Nome ,
                                              A26Municipio_Nome ,
                                              A17BancoAgencia_Codigo ,
                                              A28BancoAgencia_NomeAgencia },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
         lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
         lV62WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
         lV62WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
         lV63WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
         lV63WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
         lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
         lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
         lV68WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
         lV68WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
         lV69WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
         lV69WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
         lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
         lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
         lV74WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
         lV74WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
         lV75WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
         lV75WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
         lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia = StringUtil.PadR( StringUtil.RTrim( AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia), 10, "%");
         lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = StringUtil.PadR( StringUtil.RTrim( AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia), 50, "%");
         lV82WWBancoAgenciaDS_24_Tfbanco_nome = StringUtil.PadR( StringUtil.RTrim( AV82WWBancoAgenciaDS_24_Tfbanco_nome), 50, "%");
         lV84WWBancoAgenciaDS_26_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV84WWBancoAgenciaDS_26_Tfmunicipio_nome), 50, "%");
         /* Using cursor P00F03 */
         pr_default.execute(1, new Object[] {lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV62WWBancoAgenciaDS_4_Banco_nome1, lV62WWBancoAgenciaDS_4_Banco_nome1, lV63WWBancoAgenciaDS_5_Municipio_nome1, lV63WWBancoAgenciaDS_5_Municipio_nome1, lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV68WWBancoAgenciaDS_10_Banco_nome2, lV68WWBancoAgenciaDS_10_Banco_nome2, lV69WWBancoAgenciaDS_11_Municipio_nome2, lV69WWBancoAgenciaDS_11_Municipio_nome2, lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV74WWBancoAgenciaDS_16_Banco_nome3, lV74WWBancoAgenciaDS_16_Banco_nome3, lV75WWBancoAgenciaDS_17_Municipio_nome3, lV75WWBancoAgenciaDS_17_Municipio_nome3, AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo, AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to, lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia, AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel, lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia, AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel, lV82WWBancoAgenciaDS_24_Tfbanco_nome, AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel, lV84WWBancoAgenciaDS_26_Tfmunicipio_nome, AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKF04 = false;
            A18Banco_Codigo = P00F03_A18Banco_Codigo[0];
            A25Municipio_Codigo = P00F03_A25Municipio_Codigo[0];
            A28BancoAgencia_NomeAgencia = P00F03_A28BancoAgencia_NomeAgencia[0];
            A17BancoAgencia_Codigo = P00F03_A17BancoAgencia_Codigo[0];
            A26Municipio_Nome = P00F03_A26Municipio_Nome[0];
            A20Banco_Nome = P00F03_A20Banco_Nome[0];
            A27BancoAgencia_Agencia = P00F03_A27BancoAgencia_Agencia[0];
            A20Banco_Nome = P00F03_A20Banco_Nome[0];
            A26Municipio_Nome = P00F03_A26Municipio_Nome[0];
            AV32count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00F03_A28BancoAgencia_NomeAgencia[0], A28BancoAgencia_NomeAgencia) == 0 ) )
            {
               BRKF04 = false;
               A17BancoAgencia_Codigo = P00F03_A17BancoAgencia_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKF04 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A28BancoAgencia_NomeAgencia)) )
            {
               AV24Option = A28BancoAgencia_NomeAgencia;
               AV25Options.Add(AV24Option, 0);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF04 )
            {
               BRKF04 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADBANCO_NOMEOPTIONS' Routine */
         AV16TFBanco_Nome = AV20SearchTxt;
         AV17TFBanco_Nome_Sel = "";
         AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 = AV38DynamicFiltersSelector1;
         AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 = AV39DynamicFiltersOperator1;
         AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = AV40BancoAgencia_Agencia1;
         AV62WWBancoAgenciaDS_4_Banco_nome1 = AV41Banco_Nome1;
         AV63WWBancoAgenciaDS_5_Municipio_nome1 = AV42Municipio_Nome1;
         AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = AV46BancoAgencia_Agencia2;
         AV68WWBancoAgenciaDS_10_Banco_nome2 = AV47Banco_Nome2;
         AV69WWBancoAgenciaDS_11_Municipio_nome2 = AV48Municipio_Nome2;
         AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = AV52BancoAgencia_Agencia3;
         AV74WWBancoAgenciaDS_16_Banco_nome3 = AV53Banco_Nome3;
         AV75WWBancoAgenciaDS_17_Municipio_nome3 = AV54Municipio_Nome3;
         AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo = AV10TFBancoAgencia_Codigo;
         AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to = AV11TFBancoAgencia_Codigo_To;
         AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia = AV12TFBancoAgencia_Agencia;
         AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = AV13TFBancoAgencia_Agencia_Sel;
         AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = AV14TFBancoAgencia_NomeAgencia;
         AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = AV15TFBancoAgencia_NomeAgencia_Sel;
         AV82WWBancoAgenciaDS_24_Tfbanco_nome = AV16TFBanco_Nome;
         AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel = AV17TFBanco_Nome_Sel;
         AV84WWBancoAgenciaDS_26_Tfmunicipio_nome = AV18TFMunicipio_Nome;
         AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = AV19TFMunicipio_Nome_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                              AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                              AV62WWBancoAgenciaDS_4_Banco_nome1 ,
                                              AV63WWBancoAgenciaDS_5_Municipio_nome1 ,
                                              AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                              AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                              AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                              AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                              AV68WWBancoAgenciaDS_10_Banco_nome2 ,
                                              AV69WWBancoAgenciaDS_11_Municipio_nome2 ,
                                              AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                              AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                              AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                              AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                              AV74WWBancoAgenciaDS_16_Banco_nome3 ,
                                              AV75WWBancoAgenciaDS_17_Municipio_nome3 ,
                                              AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                              AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                              AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                              AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                              AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                              AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                              AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                              AV82WWBancoAgenciaDS_24_Tfbanco_nome ,
                                              AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                              AV84WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                              A27BancoAgencia_Agencia ,
                                              A20Banco_Nome ,
                                              A26Municipio_Nome ,
                                              A17BancoAgencia_Codigo ,
                                              A28BancoAgencia_NomeAgencia },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
         lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
         lV62WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
         lV62WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
         lV63WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
         lV63WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
         lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
         lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
         lV68WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
         lV68WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
         lV69WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
         lV69WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
         lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
         lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
         lV74WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
         lV74WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
         lV75WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
         lV75WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
         lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia = StringUtil.PadR( StringUtil.RTrim( AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia), 10, "%");
         lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = StringUtil.PadR( StringUtil.RTrim( AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia), 50, "%");
         lV82WWBancoAgenciaDS_24_Tfbanco_nome = StringUtil.PadR( StringUtil.RTrim( AV82WWBancoAgenciaDS_24_Tfbanco_nome), 50, "%");
         lV84WWBancoAgenciaDS_26_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV84WWBancoAgenciaDS_26_Tfmunicipio_nome), 50, "%");
         /* Using cursor P00F04 */
         pr_default.execute(2, new Object[] {lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV62WWBancoAgenciaDS_4_Banco_nome1, lV62WWBancoAgenciaDS_4_Banco_nome1, lV63WWBancoAgenciaDS_5_Municipio_nome1, lV63WWBancoAgenciaDS_5_Municipio_nome1, lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV68WWBancoAgenciaDS_10_Banco_nome2, lV68WWBancoAgenciaDS_10_Banco_nome2, lV69WWBancoAgenciaDS_11_Municipio_nome2, lV69WWBancoAgenciaDS_11_Municipio_nome2, lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV74WWBancoAgenciaDS_16_Banco_nome3, lV74WWBancoAgenciaDS_16_Banco_nome3, lV75WWBancoAgenciaDS_17_Municipio_nome3, lV75WWBancoAgenciaDS_17_Municipio_nome3, AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo, AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to, lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia, AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel, lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia, AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel, lV82WWBancoAgenciaDS_24_Tfbanco_nome, AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel, lV84WWBancoAgenciaDS_26_Tfmunicipio_nome, AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKF06 = false;
            A25Municipio_Codigo = P00F04_A25Municipio_Codigo[0];
            A18Banco_Codigo = P00F04_A18Banco_Codigo[0];
            A28BancoAgencia_NomeAgencia = P00F04_A28BancoAgencia_NomeAgencia[0];
            A17BancoAgencia_Codigo = P00F04_A17BancoAgencia_Codigo[0];
            A26Municipio_Nome = P00F04_A26Municipio_Nome[0];
            A20Banco_Nome = P00F04_A20Banco_Nome[0];
            A27BancoAgencia_Agencia = P00F04_A27BancoAgencia_Agencia[0];
            A26Municipio_Nome = P00F04_A26Municipio_Nome[0];
            A20Banco_Nome = P00F04_A20Banco_Nome[0];
            AV32count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00F04_A18Banco_Codigo[0] == A18Banco_Codigo ) )
            {
               BRKF06 = false;
               A17BancoAgencia_Codigo = P00F04_A17BancoAgencia_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKF06 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A20Banco_Nome)) )
            {
               AV24Option = A20Banco_Nome;
               AV23InsertIndex = 1;
               while ( ( AV23InsertIndex <= AV25Options.Count ) && ( StringUtil.StrCmp(((String)AV25Options.Item(AV23InsertIndex)), AV24Option) < 0 ) )
               {
                  AV23InsertIndex = (int)(AV23InsertIndex+1);
               }
               AV25Options.Add(AV24Option, AV23InsertIndex);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), AV23InsertIndex);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF06 )
            {
               BRKF06 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADMUNICIPIO_NOMEOPTIONS' Routine */
         AV18TFMunicipio_Nome = AV20SearchTxt;
         AV19TFMunicipio_Nome_Sel = "";
         AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 = AV38DynamicFiltersSelector1;
         AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 = AV39DynamicFiltersOperator1;
         AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = AV40BancoAgencia_Agencia1;
         AV62WWBancoAgenciaDS_4_Banco_nome1 = AV41Banco_Nome1;
         AV63WWBancoAgenciaDS_5_Municipio_nome1 = AV42Municipio_Nome1;
         AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 = AV43DynamicFiltersEnabled2;
         AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 = AV44DynamicFiltersSelector2;
         AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 = AV45DynamicFiltersOperator2;
         AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = AV46BancoAgencia_Agencia2;
         AV68WWBancoAgenciaDS_10_Banco_nome2 = AV47Banco_Nome2;
         AV69WWBancoAgenciaDS_11_Municipio_nome2 = AV48Municipio_Nome2;
         AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = AV52BancoAgencia_Agencia3;
         AV74WWBancoAgenciaDS_16_Banco_nome3 = AV53Banco_Nome3;
         AV75WWBancoAgenciaDS_17_Municipio_nome3 = AV54Municipio_Nome3;
         AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo = AV10TFBancoAgencia_Codigo;
         AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to = AV11TFBancoAgencia_Codigo_To;
         AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia = AV12TFBancoAgencia_Agencia;
         AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = AV13TFBancoAgencia_Agencia_Sel;
         AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = AV14TFBancoAgencia_NomeAgencia;
         AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = AV15TFBancoAgencia_NomeAgencia_Sel;
         AV82WWBancoAgenciaDS_24_Tfbanco_nome = AV16TFBanco_Nome;
         AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel = AV17TFBanco_Nome_Sel;
         AV84WWBancoAgenciaDS_26_Tfmunicipio_nome = AV18TFMunicipio_Nome;
         AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = AV19TFMunicipio_Nome_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                              AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                              AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                              AV62WWBancoAgenciaDS_4_Banco_nome1 ,
                                              AV63WWBancoAgenciaDS_5_Municipio_nome1 ,
                                              AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                              AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                              AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                              AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                              AV68WWBancoAgenciaDS_10_Banco_nome2 ,
                                              AV69WWBancoAgenciaDS_11_Municipio_nome2 ,
                                              AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                              AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                              AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                              AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                              AV74WWBancoAgenciaDS_16_Banco_nome3 ,
                                              AV75WWBancoAgenciaDS_17_Municipio_nome3 ,
                                              AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                              AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                              AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                              AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                              AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                              AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                              AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                              AV82WWBancoAgenciaDS_24_Tfbanco_nome ,
                                              AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                              AV84WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                              A27BancoAgencia_Agencia ,
                                              A20Banco_Nome ,
                                              A26Municipio_Nome ,
                                              A17BancoAgencia_Codigo ,
                                              A28BancoAgencia_NomeAgencia },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING
                                              }
         });
         lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
         lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = StringUtil.PadR( StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1), 10, "%");
         lV62WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
         lV62WWBancoAgenciaDS_4_Banco_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1), 50, "%");
         lV63WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
         lV63WWBancoAgenciaDS_5_Municipio_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1), 50, "%");
         lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
         lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = StringUtil.PadR( StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2), 10, "%");
         lV68WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
         lV68WWBancoAgenciaDS_10_Banco_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2), 50, "%");
         lV69WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
         lV69WWBancoAgenciaDS_11_Municipio_nome2 = StringUtil.PadR( StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2), 50, "%");
         lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
         lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = StringUtil.PadR( StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3), 10, "%");
         lV74WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
         lV74WWBancoAgenciaDS_16_Banco_nome3 = StringUtil.PadR( StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3), 50, "%");
         lV75WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
         lV75WWBancoAgenciaDS_17_Municipio_nome3 = StringUtil.PadR( StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3), 50, "%");
         lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia = StringUtil.PadR( StringUtil.RTrim( AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia), 10, "%");
         lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = StringUtil.PadR( StringUtil.RTrim( AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia), 50, "%");
         lV82WWBancoAgenciaDS_24_Tfbanco_nome = StringUtil.PadR( StringUtil.RTrim( AV82WWBancoAgenciaDS_24_Tfbanco_nome), 50, "%");
         lV84WWBancoAgenciaDS_26_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV84WWBancoAgenciaDS_26_Tfmunicipio_nome), 50, "%");
         /* Using cursor P00F05 */
         pr_default.execute(3, new Object[] {lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1, lV62WWBancoAgenciaDS_4_Banco_nome1, lV62WWBancoAgenciaDS_4_Banco_nome1, lV63WWBancoAgenciaDS_5_Municipio_nome1, lV63WWBancoAgenciaDS_5_Municipio_nome1, lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2, lV68WWBancoAgenciaDS_10_Banco_nome2, lV68WWBancoAgenciaDS_10_Banco_nome2, lV69WWBancoAgenciaDS_11_Municipio_nome2, lV69WWBancoAgenciaDS_11_Municipio_nome2, lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3, lV74WWBancoAgenciaDS_16_Banco_nome3, lV74WWBancoAgenciaDS_16_Banco_nome3, lV75WWBancoAgenciaDS_17_Municipio_nome3, lV75WWBancoAgenciaDS_17_Municipio_nome3, AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo, AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to, lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia, AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel, lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia, AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel, lV82WWBancoAgenciaDS_24_Tfbanco_nome, AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel, lV84WWBancoAgenciaDS_26_Tfmunicipio_nome, AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKF08 = false;
            A18Banco_Codigo = P00F05_A18Banco_Codigo[0];
            A25Municipio_Codigo = P00F05_A25Municipio_Codigo[0];
            A28BancoAgencia_NomeAgencia = P00F05_A28BancoAgencia_NomeAgencia[0];
            A17BancoAgencia_Codigo = P00F05_A17BancoAgencia_Codigo[0];
            A26Municipio_Nome = P00F05_A26Municipio_Nome[0];
            A20Banco_Nome = P00F05_A20Banco_Nome[0];
            A27BancoAgencia_Agencia = P00F05_A27BancoAgencia_Agencia[0];
            A20Banco_Nome = P00F05_A20Banco_Nome[0];
            A26Municipio_Nome = P00F05_A26Municipio_Nome[0];
            AV32count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( P00F05_A25Municipio_Codigo[0] == A25Municipio_Codigo ) )
            {
               BRKF08 = false;
               A17BancoAgencia_Codigo = P00F05_A17BancoAgencia_Codigo[0];
               AV32count = (long)(AV32count+1);
               BRKF08 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A26Municipio_Nome)) )
            {
               AV24Option = A26Municipio_Nome;
               AV23InsertIndex = 1;
               while ( ( AV23InsertIndex <= AV25Options.Count ) && ( StringUtil.StrCmp(((String)AV25Options.Item(AV23InsertIndex)), AV24Option) < 0 ) )
               {
                  AV23InsertIndex = (int)(AV23InsertIndex+1);
               }
               AV25Options.Add(AV24Option, AV23InsertIndex);
               AV30OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV32count), "Z,ZZZ,ZZZ,ZZ9")), AV23InsertIndex);
            }
            if ( AV25Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKF08 )
            {
               BRKF08 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV25Options = new GxSimpleCollection();
         AV28OptionsDesc = new GxSimpleCollection();
         AV30OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV33Session = context.GetSession();
         AV35GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV36GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFBancoAgencia_Agencia = "";
         AV13TFBancoAgencia_Agencia_Sel = "";
         AV14TFBancoAgencia_NomeAgencia = "";
         AV15TFBancoAgencia_NomeAgencia_Sel = "";
         AV16TFBanco_Nome = "";
         AV17TFBanco_Nome_Sel = "";
         AV18TFMunicipio_Nome = "";
         AV19TFMunicipio_Nome_Sel = "";
         AV37GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV38DynamicFiltersSelector1 = "";
         AV40BancoAgencia_Agencia1 = "";
         AV41Banco_Nome1 = "";
         AV42Municipio_Nome1 = "";
         AV44DynamicFiltersSelector2 = "";
         AV46BancoAgencia_Agencia2 = "";
         AV47Banco_Nome2 = "";
         AV48Municipio_Nome2 = "";
         AV50DynamicFiltersSelector3 = "";
         AV52BancoAgencia_Agencia3 = "";
         AV53Banco_Nome3 = "";
         AV54Municipio_Nome3 = "";
         AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 = "";
         AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = "";
         AV62WWBancoAgenciaDS_4_Banco_nome1 = "";
         AV63WWBancoAgenciaDS_5_Municipio_nome1 = "";
         AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 = "";
         AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = "";
         AV68WWBancoAgenciaDS_10_Banco_nome2 = "";
         AV69WWBancoAgenciaDS_11_Municipio_nome2 = "";
         AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 = "";
         AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = "";
         AV74WWBancoAgenciaDS_16_Banco_nome3 = "";
         AV75WWBancoAgenciaDS_17_Municipio_nome3 = "";
         AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia = "";
         AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel = "";
         AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = "";
         AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel = "";
         AV82WWBancoAgenciaDS_24_Tfbanco_nome = "";
         AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel = "";
         AV84WWBancoAgenciaDS_26_Tfmunicipio_nome = "";
         AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel = "";
         scmdbuf = "";
         lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 = "";
         lV62WWBancoAgenciaDS_4_Banco_nome1 = "";
         lV63WWBancoAgenciaDS_5_Municipio_nome1 = "";
         lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 = "";
         lV68WWBancoAgenciaDS_10_Banco_nome2 = "";
         lV69WWBancoAgenciaDS_11_Municipio_nome2 = "";
         lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 = "";
         lV74WWBancoAgenciaDS_16_Banco_nome3 = "";
         lV75WWBancoAgenciaDS_17_Municipio_nome3 = "";
         lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia = "";
         lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia = "";
         lV82WWBancoAgenciaDS_24_Tfbanco_nome = "";
         lV84WWBancoAgenciaDS_26_Tfmunicipio_nome = "";
         A27BancoAgencia_Agencia = "";
         A20Banco_Nome = "";
         A26Municipio_Nome = "";
         A28BancoAgencia_NomeAgencia = "";
         P00F02_A18Banco_Codigo = new int[1] ;
         P00F02_A25Municipio_Codigo = new int[1] ;
         P00F02_A27BancoAgencia_Agencia = new String[] {""} ;
         P00F02_A28BancoAgencia_NomeAgencia = new String[] {""} ;
         P00F02_A17BancoAgencia_Codigo = new int[1] ;
         P00F02_A26Municipio_Nome = new String[] {""} ;
         P00F02_A20Banco_Nome = new String[] {""} ;
         AV24Option = "";
         P00F03_A18Banco_Codigo = new int[1] ;
         P00F03_A25Municipio_Codigo = new int[1] ;
         P00F03_A28BancoAgencia_NomeAgencia = new String[] {""} ;
         P00F03_A17BancoAgencia_Codigo = new int[1] ;
         P00F03_A26Municipio_Nome = new String[] {""} ;
         P00F03_A20Banco_Nome = new String[] {""} ;
         P00F03_A27BancoAgencia_Agencia = new String[] {""} ;
         P00F04_A25Municipio_Codigo = new int[1] ;
         P00F04_A18Banco_Codigo = new int[1] ;
         P00F04_A28BancoAgencia_NomeAgencia = new String[] {""} ;
         P00F04_A17BancoAgencia_Codigo = new int[1] ;
         P00F04_A26Municipio_Nome = new String[] {""} ;
         P00F04_A20Banco_Nome = new String[] {""} ;
         P00F04_A27BancoAgencia_Agencia = new String[] {""} ;
         P00F05_A18Banco_Codigo = new int[1] ;
         P00F05_A25Municipio_Codigo = new int[1] ;
         P00F05_A28BancoAgencia_NomeAgencia = new String[] {""} ;
         P00F05_A17BancoAgencia_Codigo = new int[1] ;
         P00F05_A26Municipio_Nome = new String[] {""} ;
         P00F05_A20Banco_Nome = new String[] {""} ;
         P00F05_A27BancoAgencia_Agencia = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwbancoagenciafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00F02_A18Banco_Codigo, P00F02_A25Municipio_Codigo, P00F02_A27BancoAgencia_Agencia, P00F02_A28BancoAgencia_NomeAgencia, P00F02_A17BancoAgencia_Codigo, P00F02_A26Municipio_Nome, P00F02_A20Banco_Nome
               }
               , new Object[] {
               P00F03_A18Banco_Codigo, P00F03_A25Municipio_Codigo, P00F03_A28BancoAgencia_NomeAgencia, P00F03_A17BancoAgencia_Codigo, P00F03_A26Municipio_Nome, P00F03_A20Banco_Nome, P00F03_A27BancoAgencia_Agencia
               }
               , new Object[] {
               P00F04_A25Municipio_Codigo, P00F04_A18Banco_Codigo, P00F04_A28BancoAgencia_NomeAgencia, P00F04_A17BancoAgencia_Codigo, P00F04_A26Municipio_Nome, P00F04_A20Banco_Nome, P00F04_A27BancoAgencia_Agencia
               }
               , new Object[] {
               P00F05_A18Banco_Codigo, P00F05_A25Municipio_Codigo, P00F05_A28BancoAgencia_NomeAgencia, P00F05_A17BancoAgencia_Codigo, P00F05_A26Municipio_Nome, P00F05_A20Banco_Nome, P00F05_A27BancoAgencia_Agencia
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV39DynamicFiltersOperator1 ;
      private short AV45DynamicFiltersOperator2 ;
      private short AV51DynamicFiltersOperator3 ;
      private short AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ;
      private short AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ;
      private short AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ;
      private int AV57GXV1 ;
      private int AV10TFBancoAgencia_Codigo ;
      private int AV11TFBancoAgencia_Codigo_To ;
      private int AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo ;
      private int AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ;
      private int A17BancoAgencia_Codigo ;
      private int A18Banco_Codigo ;
      private int A25Municipio_Codigo ;
      private int AV23InsertIndex ;
      private long AV32count ;
      private String AV12TFBancoAgencia_Agencia ;
      private String AV13TFBancoAgencia_Agencia_Sel ;
      private String AV14TFBancoAgencia_NomeAgencia ;
      private String AV15TFBancoAgencia_NomeAgencia_Sel ;
      private String AV16TFBanco_Nome ;
      private String AV17TFBanco_Nome_Sel ;
      private String AV18TFMunicipio_Nome ;
      private String AV19TFMunicipio_Nome_Sel ;
      private String AV40BancoAgencia_Agencia1 ;
      private String AV41Banco_Nome1 ;
      private String AV42Municipio_Nome1 ;
      private String AV46BancoAgencia_Agencia2 ;
      private String AV47Banco_Nome2 ;
      private String AV48Municipio_Nome2 ;
      private String AV52BancoAgencia_Agencia3 ;
      private String AV53Banco_Nome3 ;
      private String AV54Municipio_Nome3 ;
      private String AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 ;
      private String AV62WWBancoAgenciaDS_4_Banco_nome1 ;
      private String AV63WWBancoAgenciaDS_5_Municipio_nome1 ;
      private String AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 ;
      private String AV68WWBancoAgenciaDS_10_Banco_nome2 ;
      private String AV69WWBancoAgenciaDS_11_Municipio_nome2 ;
      private String AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 ;
      private String AV74WWBancoAgenciaDS_16_Banco_nome3 ;
      private String AV75WWBancoAgenciaDS_17_Municipio_nome3 ;
      private String AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia ;
      private String AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ;
      private String AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ;
      private String AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ;
      private String AV82WWBancoAgenciaDS_24_Tfbanco_nome ;
      private String AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel ;
      private String AV84WWBancoAgenciaDS_26_Tfmunicipio_nome ;
      private String AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ;
      private String scmdbuf ;
      private String lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 ;
      private String lV62WWBancoAgenciaDS_4_Banco_nome1 ;
      private String lV63WWBancoAgenciaDS_5_Municipio_nome1 ;
      private String lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 ;
      private String lV68WWBancoAgenciaDS_10_Banco_nome2 ;
      private String lV69WWBancoAgenciaDS_11_Municipio_nome2 ;
      private String lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 ;
      private String lV74WWBancoAgenciaDS_16_Banco_nome3 ;
      private String lV75WWBancoAgenciaDS_17_Municipio_nome3 ;
      private String lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia ;
      private String lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ;
      private String lV82WWBancoAgenciaDS_24_Tfbanco_nome ;
      private String lV84WWBancoAgenciaDS_26_Tfmunicipio_nome ;
      private String A27BancoAgencia_Agencia ;
      private String A20Banco_Nome ;
      private String A26Municipio_Nome ;
      private String A28BancoAgencia_NomeAgencia ;
      private bool returnInSub ;
      private bool AV43DynamicFiltersEnabled2 ;
      private bool AV49DynamicFiltersEnabled3 ;
      private bool AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ;
      private bool AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ;
      private bool BRKF02 ;
      private bool BRKF04 ;
      private bool BRKF06 ;
      private bool BRKF08 ;
      private String AV31OptionIndexesJson ;
      private String AV26OptionsJson ;
      private String AV29OptionsDescJson ;
      private String AV22DDOName ;
      private String AV20SearchTxt ;
      private String AV21SearchTxtTo ;
      private String AV38DynamicFiltersSelector1 ;
      private String AV44DynamicFiltersSelector2 ;
      private String AV50DynamicFiltersSelector3 ;
      private String AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 ;
      private String AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 ;
      private String AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 ;
      private String AV24Option ;
      private IGxSession AV33Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00F02_A18Banco_Codigo ;
      private int[] P00F02_A25Municipio_Codigo ;
      private String[] P00F02_A27BancoAgencia_Agencia ;
      private String[] P00F02_A28BancoAgencia_NomeAgencia ;
      private int[] P00F02_A17BancoAgencia_Codigo ;
      private String[] P00F02_A26Municipio_Nome ;
      private String[] P00F02_A20Banco_Nome ;
      private int[] P00F03_A18Banco_Codigo ;
      private int[] P00F03_A25Municipio_Codigo ;
      private String[] P00F03_A28BancoAgencia_NomeAgencia ;
      private int[] P00F03_A17BancoAgencia_Codigo ;
      private String[] P00F03_A26Municipio_Nome ;
      private String[] P00F03_A20Banco_Nome ;
      private String[] P00F03_A27BancoAgencia_Agencia ;
      private int[] P00F04_A25Municipio_Codigo ;
      private int[] P00F04_A18Banco_Codigo ;
      private String[] P00F04_A28BancoAgencia_NomeAgencia ;
      private int[] P00F04_A17BancoAgencia_Codigo ;
      private String[] P00F04_A26Municipio_Nome ;
      private String[] P00F04_A20Banco_Nome ;
      private String[] P00F04_A27BancoAgencia_Agencia ;
      private int[] P00F05_A18Banco_Codigo ;
      private int[] P00F05_A25Municipio_Codigo ;
      private String[] P00F05_A28BancoAgencia_NomeAgencia ;
      private int[] P00F05_A17BancoAgencia_Codigo ;
      private String[] P00F05_A26Municipio_Nome ;
      private String[] P00F05_A20Banco_Nome ;
      private String[] P00F05_A27BancoAgencia_Agencia ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV35GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV36GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV37GridStateDynamicFilter ;
   }

   public class getwwbancoagenciafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00F02( IGxContext context ,
                                             String AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                             short AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                             String AV62WWBancoAgenciaDS_4_Banco_nome1 ,
                                             String AV63WWBancoAgenciaDS_5_Municipio_nome1 ,
                                             bool AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                             String AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                             short AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                             String AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                             String AV68WWBancoAgenciaDS_10_Banco_nome2 ,
                                             String AV69WWBancoAgenciaDS_11_Municipio_nome2 ,
                                             bool AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                             String AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                             short AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                             String AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                             String AV74WWBancoAgenciaDS_16_Banco_nome3 ,
                                             String AV75WWBancoAgenciaDS_17_Municipio_nome3 ,
                                             int AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                             int AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                             String AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                             String AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                             String AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                             String AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                             String AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                             String AV82WWBancoAgenciaDS_24_Tfbanco_nome ,
                                             String AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                             String AV84WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                             String A27BancoAgencia_Agencia ,
                                             String A20Banco_Nome ,
                                             String A26Municipio_Nome ,
                                             int A17BancoAgencia_Codigo ,
                                             String A28BancoAgencia_NomeAgencia )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [28] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Banco_Codigo], T1.[Municipio_Codigo], T1.[BancoAgencia_Agencia], T1.[BancoAgencia_NomeAgencia], T1.[BancoAgencia_Codigo], T3.[Municipio_Nome], T2.[Banco_Nome] FROM (([BancoAgencia] T1 WITH (NOLOCK) INNER JOIN [Banco] T2 WITH (NOLOCK) ON T2.[Banco_Codigo] = T1.[Banco_Codigo]) INNER JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T1.[Municipio_Codigo])";
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (0==AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] >= @AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] >= @AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (0==AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] <= @AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] <= @AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] = @AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] = @AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] like @lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] like @lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] = @AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] = @AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWBancoAgenciaDS_24_Tfbanco_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV82WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV82WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] = @AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] = @AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWBancoAgenciaDS_26_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV84WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV84WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] = @AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[BancoAgencia_Agencia]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00F03( IGxContext context ,
                                             String AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                             short AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                             String AV62WWBancoAgenciaDS_4_Banco_nome1 ,
                                             String AV63WWBancoAgenciaDS_5_Municipio_nome1 ,
                                             bool AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                             String AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                             short AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                             String AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                             String AV68WWBancoAgenciaDS_10_Banco_nome2 ,
                                             String AV69WWBancoAgenciaDS_11_Municipio_nome2 ,
                                             bool AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                             String AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                             short AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                             String AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                             String AV74WWBancoAgenciaDS_16_Banco_nome3 ,
                                             String AV75WWBancoAgenciaDS_17_Municipio_nome3 ,
                                             int AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                             int AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                             String AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                             String AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                             String AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                             String AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                             String AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                             String AV82WWBancoAgenciaDS_24_Tfbanco_nome ,
                                             String AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                             String AV84WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                             String A27BancoAgencia_Agencia ,
                                             String A20Banco_Nome ,
                                             String A26Municipio_Nome ,
                                             int A17BancoAgencia_Codigo ,
                                             String A28BancoAgencia_NomeAgencia )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [28] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Banco_Codigo], T1.[Municipio_Codigo], T1.[BancoAgencia_NomeAgencia], T1.[BancoAgencia_Codigo], T3.[Municipio_Nome], T2.[Banco_Nome], T1.[BancoAgencia_Agencia] FROM (([BancoAgencia] T1 WITH (NOLOCK) INNER JOIN [Banco] T2 WITH (NOLOCK) ON T2.[Banco_Codigo] = T1.[Banco_Codigo]) INNER JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T1.[Municipio_Codigo])";
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (0==AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] >= @AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] >= @AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (0==AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] <= @AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] <= @AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] = @AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] = @AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] like @lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] like @lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] = @AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] = @AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWBancoAgenciaDS_24_Tfbanco_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV82WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV82WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] = @AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] = @AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWBancoAgenciaDS_26_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV84WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV84WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] = @AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[BancoAgencia_NomeAgencia]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00F04( IGxContext context ,
                                             String AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                             short AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                             String AV62WWBancoAgenciaDS_4_Banco_nome1 ,
                                             String AV63WWBancoAgenciaDS_5_Municipio_nome1 ,
                                             bool AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                             String AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                             short AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                             String AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                             String AV68WWBancoAgenciaDS_10_Banco_nome2 ,
                                             String AV69WWBancoAgenciaDS_11_Municipio_nome2 ,
                                             bool AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                             String AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                             short AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                             String AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                             String AV74WWBancoAgenciaDS_16_Banco_nome3 ,
                                             String AV75WWBancoAgenciaDS_17_Municipio_nome3 ,
                                             int AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                             int AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                             String AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                             String AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                             String AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                             String AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                             String AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                             String AV82WWBancoAgenciaDS_24_Tfbanco_nome ,
                                             String AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                             String AV84WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                             String A27BancoAgencia_Agencia ,
                                             String A20Banco_Nome ,
                                             String A26Municipio_Nome ,
                                             int A17BancoAgencia_Codigo ,
                                             String A28BancoAgencia_NomeAgencia )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [28] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Municipio_Codigo], T1.[Banco_Codigo], T1.[BancoAgencia_NomeAgencia], T1.[BancoAgencia_Codigo], T2.[Municipio_Nome], T3.[Banco_Nome], T1.[BancoAgencia_Agencia] FROM (([BancoAgencia] T1 WITH (NOLOCK) INNER JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Municipio_Codigo]) INNER JOIN [Banco] T3 WITH (NOLOCK) ON T3.[Banco_Codigo] = T1.[Banco_Codigo])";
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like '%' + @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like '%' + @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like '%' + @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like '%' + @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like '%' + @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like '%' + @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like '%' + @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like '%' + @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like '%' + @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like '%' + @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like '%' + @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like '%' + @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( ! (0==AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] >= @AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] >= @AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (0==AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] <= @AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] <= @AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] = @AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] = @AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] like @lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] like @lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] = @AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] = @AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWBancoAgenciaDS_24_Tfbanco_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] like @lV82WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] like @lV82WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Banco_Nome] = @AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Banco_Nome] = @AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWBancoAgenciaDS_26_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] like @lV84WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] like @lV84WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Municipio_Nome] = @AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Municipio_Nome] = @AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Banco_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00F05( IGxContext context ,
                                             String AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1 ,
                                             short AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 ,
                                             String AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1 ,
                                             String AV62WWBancoAgenciaDS_4_Banco_nome1 ,
                                             String AV63WWBancoAgenciaDS_5_Municipio_nome1 ,
                                             bool AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 ,
                                             String AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2 ,
                                             short AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 ,
                                             String AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2 ,
                                             String AV68WWBancoAgenciaDS_10_Banco_nome2 ,
                                             String AV69WWBancoAgenciaDS_11_Municipio_nome2 ,
                                             bool AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 ,
                                             String AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3 ,
                                             short AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 ,
                                             String AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3 ,
                                             String AV74WWBancoAgenciaDS_16_Banco_nome3 ,
                                             String AV75WWBancoAgenciaDS_17_Municipio_nome3 ,
                                             int AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo ,
                                             int AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to ,
                                             String AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel ,
                                             String AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia ,
                                             String AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel ,
                                             String AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia ,
                                             String AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel ,
                                             String AV82WWBancoAgenciaDS_24_Tfbanco_nome ,
                                             String AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel ,
                                             String AV84WWBancoAgenciaDS_26_Tfmunicipio_nome ,
                                             String A27BancoAgencia_Agencia ,
                                             String A20Banco_Nome ,
                                             String A26Municipio_Nome ,
                                             int A17BancoAgencia_Codigo ,
                                             String A28BancoAgencia_NomeAgencia )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [28] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[Banco_Codigo], T1.[Municipio_Codigo], T1.[BancoAgencia_NomeAgencia], T1.[BancoAgencia_Codigo], T3.[Municipio_Nome], T2.[Banco_Nome], T1.[BancoAgencia_Agencia] FROM (([BancoAgencia] T1 WITH (NOLOCK) INNER JOIN [Banco] T2 WITH (NOLOCK) ON T2.[Banco_Codigo] = T1.[Banco_Codigo]) INNER JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T1.[Municipio_Codigo])";
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "BANCO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWBancoAgenciaDS_4_Banco_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV62WWBancoAgenciaDS_4_Banco_nome1)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWBancoAgenciaDS_1_Dynamicfiltersselector1, "MUNICIPIO_NOME") == 0 ) && ( AV60WWBancoAgenciaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWBancoAgenciaDS_5_Municipio_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV63WWBancoAgenciaDS_5_Municipio_nome1)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "BANCO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWBancoAgenciaDS_10_Banco_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV68WWBancoAgenciaDS_10_Banco_nome2)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( AV64WWBancoAgenciaDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWBancoAgenciaDS_7_Dynamicfiltersselector2, "MUNICIPIO_NOME") == 0 ) && ( AV66WWBancoAgenciaDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWBancoAgenciaDS_11_Municipio_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV69WWBancoAgenciaDS_11_Municipio_nome2)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCOAGENCIA_AGENCIA") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like '%' + @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like '%' + @lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "BANCO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWBancoAgenciaDS_16_Banco_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like '%' + @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like '%' + @lV74WWBancoAgenciaDS_16_Banco_nome3)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( AV70WWBancoAgenciaDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV71WWBancoAgenciaDS_13_Dynamicfiltersselector3, "MUNICIPIO_NOME") == 0 ) && ( AV72WWBancoAgenciaDS_14_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWBancoAgenciaDS_17_Municipio_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like '%' + @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like '%' + @lV75WWBancoAgenciaDS_17_Municipio_nome3)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( ! (0==AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] >= @AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] >= @AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! (0==AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Codigo] <= @AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Codigo] <= @AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] like @lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] like @lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_Agencia] = @AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_Agencia] = @AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] like @lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] like @lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia)";
            }
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[BancoAgencia_NomeAgencia] = @AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[BancoAgencia_NomeAgencia] = @AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel)";
            }
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV82WWBancoAgenciaDS_24_Tfbanco_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] like @lV82WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] like @lV82WWBancoAgenciaDS_24_Tfbanco_nome)";
            }
         }
         else
         {
            GXv_int7[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Banco_Nome] = @AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Banco_Nome] = @AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel)";
            }
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWBancoAgenciaDS_26_Tfmunicipio_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV84WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] like @lV84WWBancoAgenciaDS_26_Tfmunicipio_nome)";
            }
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Municipio_Nome] = @AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel)";
            }
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Municipio_Codigo]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00F02(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] );
               case 1 :
                     return conditional_P00F03(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] );
               case 2 :
                     return conditional_P00F04(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] );
               case 3 :
                     return conditional_P00F05(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00F02 ;
          prmP00F02 = new Object[] {
          new Object[] {"@lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV62WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV68WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV74WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia",SqlDbType.Char,10,0} ,
          new Object[] {"@AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV82WWBancoAgenciaDS_24_Tfbanco_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWBancoAgenciaDS_26_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00F03 ;
          prmP00F03 = new Object[] {
          new Object[] {"@lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV62WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV68WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV74WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia",SqlDbType.Char,10,0} ,
          new Object[] {"@AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV82WWBancoAgenciaDS_24_Tfbanco_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWBancoAgenciaDS_26_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00F04 ;
          prmP00F04 = new Object[] {
          new Object[] {"@lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV62WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV68WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV74WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia",SqlDbType.Char,10,0} ,
          new Object[] {"@AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV82WWBancoAgenciaDS_24_Tfbanco_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWBancoAgenciaDS_26_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00F05 ;
          prmP00F05 = new Object[] {
          new Object[] {"@lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV61WWBancoAgenciaDS_3_Bancoagencia_agencia1",SqlDbType.Char,10,0} ,
          new Object[] {"@lV62WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWBancoAgenciaDS_4_Banco_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWBancoAgenciaDS_5_Municipio_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV67WWBancoAgenciaDS_9_Bancoagencia_agencia2",SqlDbType.Char,10,0} ,
          new Object[] {"@lV68WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWBancoAgenciaDS_10_Banco_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWBancoAgenciaDS_11_Municipio_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV73WWBancoAgenciaDS_15_Bancoagencia_agencia3",SqlDbType.Char,10,0} ,
          new Object[] {"@lV74WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWBancoAgenciaDS_16_Banco_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWBancoAgenciaDS_17_Municipio_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV76WWBancoAgenciaDS_18_Tfbancoagencia_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV77WWBancoAgenciaDS_19_Tfbancoagencia_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV78WWBancoAgenciaDS_20_Tfbancoagencia_agencia",SqlDbType.Char,10,0} ,
          new Object[] {"@AV79WWBancoAgenciaDS_21_Tfbancoagencia_agencia_sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV80WWBancoAgenciaDS_22_Tfbancoagencia_nomeagencia",SqlDbType.Char,50,0} ,
          new Object[] {"@AV81WWBancoAgenciaDS_23_Tfbancoagencia_nomeagencia_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV82WWBancoAgenciaDS_24_Tfbanco_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV83WWBancoAgenciaDS_25_Tfbanco_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWBancoAgenciaDS_26_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV85WWBancoAgenciaDS_27_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00F02", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F02,100,0,true,false )
             ,new CursorDef("P00F03", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F03,100,0,true,false )
             ,new CursorDef("P00F04", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F04,100,0,true,false )
             ,new CursorDef("P00F05", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00F05,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 10) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 10) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwbancoagenciafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwbancoagenciafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwbancoagenciafilterdata") )
          {
             return  ;
          }
          getwwbancoagenciafilterdata worker = new getwwbancoagenciafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
