/*
               File: type_SdtProposta
        Description: Proposta
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:55:47.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Proposta" )]
   [XmlType(TypeName =  "Proposta" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtProposta : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtProposta( )
      {
         /* Constructor for serialization */
         gxTv_SdtProposta_Proposta_vigencia = (DateTime)(DateTime.MinValue);
         gxTv_SdtProposta_Proposta_status = "";
         gxTv_SdtProposta_Proposta_objetivo = "";
         gxTv_SdtProposta_Proposta_escopo = "";
         gxTv_SdtProposta_Proposta_oportunidade = "";
         gxTv_SdtProposta_Proposta_perspectiva = "";
         gxTv_SdtProposta_Proposta_restricaoimplantacao = (DateTime)(DateTime.MinValue);
         gxTv_SdtProposta_Proposta_contratadarazsocial = "";
         gxTv_SdtProposta_Proposta_prazo = DateTime.MinValue;
         gxTv_SdtProposta_Proposta_osdmn = "";
         gxTv_SdtProposta_Proposta_osdmnfm = "";
         gxTv_SdtProposta_Proposta_osundcntsgl = "";
         gxTv_SdtProposta_Proposta_osprztpdias = "";
         gxTv_SdtProposta_Proposta_osdatacnt = DateTime.MinValue;
         gxTv_SdtProposta_Proposta_oshoracnt = "";
         gxTv_SdtProposta_Proposta_oscntsrvftrm = "";
         gxTv_SdtProposta_Proposta_osinicio = DateTime.MinValue;
         gxTv_SdtProposta_Mode = "";
         gxTv_SdtProposta_Proposta_vigencia_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtProposta_Proposta_status_Z = "";
         gxTv_SdtProposta_Proposta_restricaoimplantacao_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtProposta_Proposta_contratadarazsocial_Z = "";
         gxTv_SdtProposta_Proposta_prazo_Z = DateTime.MinValue;
         gxTv_SdtProposta_Proposta_osdmn_Z = "";
         gxTv_SdtProposta_Proposta_osdmnfm_Z = "";
         gxTv_SdtProposta_Proposta_osundcntsgl_Z = "";
         gxTv_SdtProposta_Proposta_osprztpdias_Z = "";
         gxTv_SdtProposta_Proposta_osdatacnt_Z = DateTime.MinValue;
         gxTv_SdtProposta_Proposta_oshoracnt_Z = "";
         gxTv_SdtProposta_Proposta_oscntsrvftrm_Z = "";
         gxTv_SdtProposta_Proposta_osinicio_Z = DateTime.MinValue;
      }

      public SdtProposta( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1685Proposta_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1685Proposta_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Proposta_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Proposta");
         metadata.Set("BT", "PROPOSTA");
         metadata.Set("PK", "[ \"Proposta_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Proposta_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"AreaTrabalho_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"ContagemResultado_Codigo\" ],\"FKMap\":[ \"Proposta_OSCodigo-ContagemResultado_Codigo\" ] },{ \"FK\":[ \"Projeto_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_vigencia_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_valor_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_status_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Areatrabalho_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_restricaoimplantacao_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_restricaocusto_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_contratadacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_contratadarazsocial_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_prazo_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_prazodias_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_esforco_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_liquido_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_cntsrvcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_valorundcnt_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_oscodigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osservico_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_oscntcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osdmn_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osdmnfm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osundcntcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osundcntsgl_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osprztpdias_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osprzrsp_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_ospfb_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_ospfl_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osdatacnt_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_oshoracnt_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_oscntsrvftrm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osinicio_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_vigencia_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_status_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_escopo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_oportunidade_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_perspectiva_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_restricaoimplantacao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_restricaocusto_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_contratadarazsocial_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_prazodias_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_liquido_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osservico_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_oscntcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osdmn_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osdmnfm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osundcntcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osundcntsgl_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osprztpdias_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osprzrsp_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_ospfb_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_ospfl_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osdatacnt_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_oshoracnt_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_oscntsrvftrm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Proposta_osinicio_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtProposta deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtProposta)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtProposta obj ;
         obj = this;
         obj.gxTpr_Proposta_codigo = deserialized.gxTpr_Proposta_codigo;
         obj.gxTpr_Projeto_codigo = deserialized.gxTpr_Projeto_codigo;
         obj.gxTpr_Proposta_vigencia = deserialized.gxTpr_Proposta_vigencia;
         obj.gxTpr_Proposta_valor = deserialized.gxTpr_Proposta_valor;
         obj.gxTpr_Proposta_status = deserialized.gxTpr_Proposta_status;
         obj.gxTpr_Areatrabalho_codigo = deserialized.gxTpr_Areatrabalho_codigo;
         obj.gxTpr_Proposta_objetivo = deserialized.gxTpr_Proposta_objetivo;
         obj.gxTpr_Proposta_escopo = deserialized.gxTpr_Proposta_escopo;
         obj.gxTpr_Proposta_oportunidade = deserialized.gxTpr_Proposta_oportunidade;
         obj.gxTpr_Proposta_perspectiva = deserialized.gxTpr_Proposta_perspectiva;
         obj.gxTpr_Proposta_restricaoimplantacao = deserialized.gxTpr_Proposta_restricaoimplantacao;
         obj.gxTpr_Proposta_restricaocusto = deserialized.gxTpr_Proposta_restricaocusto;
         obj.gxTpr_Proposta_ativo = deserialized.gxTpr_Proposta_ativo;
         obj.gxTpr_Proposta_contratadacod = deserialized.gxTpr_Proposta_contratadacod;
         obj.gxTpr_Proposta_contratadarazsocial = deserialized.gxTpr_Proposta_contratadarazsocial;
         obj.gxTpr_Proposta_prazo = deserialized.gxTpr_Proposta_prazo;
         obj.gxTpr_Proposta_prazodias = deserialized.gxTpr_Proposta_prazodias;
         obj.gxTpr_Proposta_esforco = deserialized.gxTpr_Proposta_esforco;
         obj.gxTpr_Proposta_liquido = deserialized.gxTpr_Proposta_liquido;
         obj.gxTpr_Proposta_cntsrvcod = deserialized.gxTpr_Proposta_cntsrvcod;
         obj.gxTpr_Proposta_valorundcnt = deserialized.gxTpr_Proposta_valorundcnt;
         obj.gxTpr_Proposta_oscodigo = deserialized.gxTpr_Proposta_oscodigo;
         obj.gxTpr_Proposta_osservico = deserialized.gxTpr_Proposta_osservico;
         obj.gxTpr_Proposta_oscntcod = deserialized.gxTpr_Proposta_oscntcod;
         obj.gxTpr_Proposta_osdmn = deserialized.gxTpr_Proposta_osdmn;
         obj.gxTpr_Proposta_osdmnfm = deserialized.gxTpr_Proposta_osdmnfm;
         obj.gxTpr_Proposta_osundcntcod = deserialized.gxTpr_Proposta_osundcntcod;
         obj.gxTpr_Proposta_osundcntsgl = deserialized.gxTpr_Proposta_osundcntsgl;
         obj.gxTpr_Proposta_osprztpdias = deserialized.gxTpr_Proposta_osprztpdias;
         obj.gxTpr_Proposta_osprzrsp = deserialized.gxTpr_Proposta_osprzrsp;
         obj.gxTpr_Proposta_ospfb = deserialized.gxTpr_Proposta_ospfb;
         obj.gxTpr_Proposta_ospfl = deserialized.gxTpr_Proposta_ospfl;
         obj.gxTpr_Proposta_osdatacnt = deserialized.gxTpr_Proposta_osdatacnt;
         obj.gxTpr_Proposta_oshoracnt = deserialized.gxTpr_Proposta_oshoracnt;
         obj.gxTpr_Proposta_oscntsrvftrm = deserialized.gxTpr_Proposta_oscntsrvftrm;
         obj.gxTpr_Proposta_osinicio = deserialized.gxTpr_Proposta_osinicio;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Proposta_codigo_Z = deserialized.gxTpr_Proposta_codigo_Z;
         obj.gxTpr_Projeto_codigo_Z = deserialized.gxTpr_Projeto_codigo_Z;
         obj.gxTpr_Proposta_vigencia_Z = deserialized.gxTpr_Proposta_vigencia_Z;
         obj.gxTpr_Proposta_valor_Z = deserialized.gxTpr_Proposta_valor_Z;
         obj.gxTpr_Proposta_status_Z = deserialized.gxTpr_Proposta_status_Z;
         obj.gxTpr_Areatrabalho_codigo_Z = deserialized.gxTpr_Areatrabalho_codigo_Z;
         obj.gxTpr_Proposta_restricaoimplantacao_Z = deserialized.gxTpr_Proposta_restricaoimplantacao_Z;
         obj.gxTpr_Proposta_restricaocusto_Z = deserialized.gxTpr_Proposta_restricaocusto_Z;
         obj.gxTpr_Proposta_ativo_Z = deserialized.gxTpr_Proposta_ativo_Z;
         obj.gxTpr_Proposta_contratadacod_Z = deserialized.gxTpr_Proposta_contratadacod_Z;
         obj.gxTpr_Proposta_contratadarazsocial_Z = deserialized.gxTpr_Proposta_contratadarazsocial_Z;
         obj.gxTpr_Proposta_prazo_Z = deserialized.gxTpr_Proposta_prazo_Z;
         obj.gxTpr_Proposta_prazodias_Z = deserialized.gxTpr_Proposta_prazodias_Z;
         obj.gxTpr_Proposta_esforco_Z = deserialized.gxTpr_Proposta_esforco_Z;
         obj.gxTpr_Proposta_liquido_Z = deserialized.gxTpr_Proposta_liquido_Z;
         obj.gxTpr_Proposta_cntsrvcod_Z = deserialized.gxTpr_Proposta_cntsrvcod_Z;
         obj.gxTpr_Proposta_valorundcnt_Z = deserialized.gxTpr_Proposta_valorundcnt_Z;
         obj.gxTpr_Proposta_oscodigo_Z = deserialized.gxTpr_Proposta_oscodigo_Z;
         obj.gxTpr_Proposta_osservico_Z = deserialized.gxTpr_Proposta_osservico_Z;
         obj.gxTpr_Proposta_oscntcod_Z = deserialized.gxTpr_Proposta_oscntcod_Z;
         obj.gxTpr_Proposta_osdmn_Z = deserialized.gxTpr_Proposta_osdmn_Z;
         obj.gxTpr_Proposta_osdmnfm_Z = deserialized.gxTpr_Proposta_osdmnfm_Z;
         obj.gxTpr_Proposta_osundcntcod_Z = deserialized.gxTpr_Proposta_osundcntcod_Z;
         obj.gxTpr_Proposta_osundcntsgl_Z = deserialized.gxTpr_Proposta_osundcntsgl_Z;
         obj.gxTpr_Proposta_osprztpdias_Z = deserialized.gxTpr_Proposta_osprztpdias_Z;
         obj.gxTpr_Proposta_osprzrsp_Z = deserialized.gxTpr_Proposta_osprzrsp_Z;
         obj.gxTpr_Proposta_ospfb_Z = deserialized.gxTpr_Proposta_ospfb_Z;
         obj.gxTpr_Proposta_ospfl_Z = deserialized.gxTpr_Proposta_ospfl_Z;
         obj.gxTpr_Proposta_osdatacnt_Z = deserialized.gxTpr_Proposta_osdatacnt_Z;
         obj.gxTpr_Proposta_oshoracnt_Z = deserialized.gxTpr_Proposta_oshoracnt_Z;
         obj.gxTpr_Proposta_oscntsrvftrm_Z = deserialized.gxTpr_Proposta_oscntsrvftrm_Z;
         obj.gxTpr_Proposta_osinicio_Z = deserialized.gxTpr_Proposta_osinicio_Z;
         obj.gxTpr_Proposta_codigo_N = deserialized.gxTpr_Proposta_codigo_N;
         obj.gxTpr_Projeto_codigo_N = deserialized.gxTpr_Projeto_codigo_N;
         obj.gxTpr_Proposta_vigencia_N = deserialized.gxTpr_Proposta_vigencia_N;
         obj.gxTpr_Proposta_status_N = deserialized.gxTpr_Proposta_status_N;
         obj.gxTpr_Proposta_escopo_N = deserialized.gxTpr_Proposta_escopo_N;
         obj.gxTpr_Proposta_oportunidade_N = deserialized.gxTpr_Proposta_oportunidade_N;
         obj.gxTpr_Proposta_perspectiva_N = deserialized.gxTpr_Proposta_perspectiva_N;
         obj.gxTpr_Proposta_restricaoimplantacao_N = deserialized.gxTpr_Proposta_restricaoimplantacao_N;
         obj.gxTpr_Proposta_restricaocusto_N = deserialized.gxTpr_Proposta_restricaocusto_N;
         obj.gxTpr_Proposta_contratadarazsocial_N = deserialized.gxTpr_Proposta_contratadarazsocial_N;
         obj.gxTpr_Proposta_prazodias_N = deserialized.gxTpr_Proposta_prazodias_N;
         obj.gxTpr_Proposta_liquido_N = deserialized.gxTpr_Proposta_liquido_N;
         obj.gxTpr_Proposta_osservico_N = deserialized.gxTpr_Proposta_osservico_N;
         obj.gxTpr_Proposta_oscntcod_N = deserialized.gxTpr_Proposta_oscntcod_N;
         obj.gxTpr_Proposta_osdmn_N = deserialized.gxTpr_Proposta_osdmn_N;
         obj.gxTpr_Proposta_osdmnfm_N = deserialized.gxTpr_Proposta_osdmnfm_N;
         obj.gxTpr_Proposta_osundcntcod_N = deserialized.gxTpr_Proposta_osundcntcod_N;
         obj.gxTpr_Proposta_osundcntsgl_N = deserialized.gxTpr_Proposta_osundcntsgl_N;
         obj.gxTpr_Proposta_osprztpdias_N = deserialized.gxTpr_Proposta_osprztpdias_N;
         obj.gxTpr_Proposta_osprzrsp_N = deserialized.gxTpr_Proposta_osprzrsp_N;
         obj.gxTpr_Proposta_ospfb_N = deserialized.gxTpr_Proposta_ospfb_N;
         obj.gxTpr_Proposta_ospfl_N = deserialized.gxTpr_Proposta_ospfl_N;
         obj.gxTpr_Proposta_osdatacnt_N = deserialized.gxTpr_Proposta_osdatacnt_N;
         obj.gxTpr_Proposta_oshoracnt_N = deserialized.gxTpr_Proposta_oshoracnt_N;
         obj.gxTpr_Proposta_oscntsrvftrm_N = deserialized.gxTpr_Proposta_oscntsrvftrm_N;
         obj.gxTpr_Proposta_osinicio_N = deserialized.gxTpr_Proposta_osinicio_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Codigo") )
               {
                  gxTv_SdtProposta_Proposta_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Codigo") )
               {
                  gxTv_SdtProposta_Projeto_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Vigencia") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProposta_Proposta_vigencia = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtProposta_Proposta_vigencia = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Valor") )
               {
                  gxTv_SdtProposta_Proposta_valor = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Status") )
               {
                  gxTv_SdtProposta_Proposta_status = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Codigo") )
               {
                  gxTv_SdtProposta_Areatrabalho_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Objetivo") )
               {
                  gxTv_SdtProposta_Proposta_objetivo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Escopo") )
               {
                  gxTv_SdtProposta_Proposta_escopo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Oportunidade") )
               {
                  gxTv_SdtProposta_Proposta_oportunidade = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Perspectiva") )
               {
                  gxTv_SdtProposta_Proposta_perspectiva = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_RestricaoImplantacao") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProposta_Proposta_restricaoimplantacao = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtProposta_Proposta_restricaoimplantacao = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_RestricaoCusto") )
               {
                  gxTv_SdtProposta_Proposta_restricaocusto = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Ativo") )
               {
                  gxTv_SdtProposta_Proposta_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_ContratadaCod") )
               {
                  gxTv_SdtProposta_Proposta_contratadacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_ContratadaRazSocial") )
               {
                  gxTv_SdtProposta_Proposta_contratadarazsocial = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Prazo") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProposta_Proposta_prazo = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProposta_Proposta_prazo = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_PrazoDias") )
               {
                  gxTv_SdtProposta_Proposta_prazodias = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Esforco") )
               {
                  gxTv_SdtProposta_Proposta_esforco = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Liquido") )
               {
                  gxTv_SdtProposta_Proposta_liquido = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_CntSrvCod") )
               {
                  gxTv_SdtProposta_Proposta_cntsrvcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_ValorUndCnt") )
               {
                  gxTv_SdtProposta_Proposta_valorundcnt = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSCodigo") )
               {
                  gxTv_SdtProposta_Proposta_oscodigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSServico") )
               {
                  gxTv_SdtProposta_Proposta_osservico = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSCntCod") )
               {
                  gxTv_SdtProposta_Proposta_oscntcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSDmn") )
               {
                  gxTv_SdtProposta_Proposta_osdmn = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSDmnFM") )
               {
                  gxTv_SdtProposta_Proposta_osdmnfm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSUndCntCod") )
               {
                  gxTv_SdtProposta_Proposta_osundcntcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSUndCntSgl") )
               {
                  gxTv_SdtProposta_Proposta_osundcntsgl = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPrzTpDias") )
               {
                  gxTv_SdtProposta_Proposta_osprztpdias = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPrzRsp") )
               {
                  gxTv_SdtProposta_Proposta_osprzrsp = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPFB") )
               {
                  gxTv_SdtProposta_Proposta_ospfb = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPFL") )
               {
                  gxTv_SdtProposta_Proposta_ospfl = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSDataCnt") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProposta_Proposta_osdatacnt = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProposta_Proposta_osdatacnt = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSHoraCnt") )
               {
                  gxTv_SdtProposta_Proposta_oshoracnt = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSCntSrvFtrm") )
               {
                  gxTv_SdtProposta_Proposta_oscntsrvftrm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSInicio") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProposta_Proposta_osinicio = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProposta_Proposta_osinicio = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtProposta_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtProposta_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Codigo_Z") )
               {
                  gxTv_SdtProposta_Proposta_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Codigo_Z") )
               {
                  gxTv_SdtProposta_Projeto_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Vigencia_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProposta_Proposta_vigencia_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtProposta_Proposta_vigencia_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Valor_Z") )
               {
                  gxTv_SdtProposta_Proposta_valor_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Status_Z") )
               {
                  gxTv_SdtProposta_Proposta_status_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Codigo_Z") )
               {
                  gxTv_SdtProposta_Areatrabalho_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_RestricaoImplantacao_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProposta_Proposta_restricaoimplantacao_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtProposta_Proposta_restricaoimplantacao_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_RestricaoCusto_Z") )
               {
                  gxTv_SdtProposta_Proposta_restricaocusto_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Ativo_Z") )
               {
                  gxTv_SdtProposta_Proposta_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_ContratadaCod_Z") )
               {
                  gxTv_SdtProposta_Proposta_contratadacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_ContratadaRazSocial_Z") )
               {
                  gxTv_SdtProposta_Proposta_contratadarazsocial_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Prazo_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProposta_Proposta_prazo_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProposta_Proposta_prazo_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_PrazoDias_Z") )
               {
                  gxTv_SdtProposta_Proposta_prazodias_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Esforco_Z") )
               {
                  gxTv_SdtProposta_Proposta_esforco_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Liquido_Z") )
               {
                  gxTv_SdtProposta_Proposta_liquido_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_CntSrvCod_Z") )
               {
                  gxTv_SdtProposta_Proposta_cntsrvcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_ValorUndCnt_Z") )
               {
                  gxTv_SdtProposta_Proposta_valorundcnt_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSCodigo_Z") )
               {
                  gxTv_SdtProposta_Proposta_oscodigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSServico_Z") )
               {
                  gxTv_SdtProposta_Proposta_osservico_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSCntCod_Z") )
               {
                  gxTv_SdtProposta_Proposta_oscntcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSDmn_Z") )
               {
                  gxTv_SdtProposta_Proposta_osdmn_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSDmnFM_Z") )
               {
                  gxTv_SdtProposta_Proposta_osdmnfm_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSUndCntCod_Z") )
               {
                  gxTv_SdtProposta_Proposta_osundcntcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSUndCntSgl_Z") )
               {
                  gxTv_SdtProposta_Proposta_osundcntsgl_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPrzTpDias_Z") )
               {
                  gxTv_SdtProposta_Proposta_osprztpdias_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPrzRsp_Z") )
               {
                  gxTv_SdtProposta_Proposta_osprzrsp_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPFB_Z") )
               {
                  gxTv_SdtProposta_Proposta_ospfb_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPFL_Z") )
               {
                  gxTv_SdtProposta_Proposta_ospfl_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSDataCnt_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProposta_Proposta_osdatacnt_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProposta_Proposta_osdatacnt_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSHoraCnt_Z") )
               {
                  gxTv_SdtProposta_Proposta_oshoracnt_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSCntSrvFtrm_Z") )
               {
                  gxTv_SdtProposta_Proposta_oscntsrvftrm_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSInicio_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProposta_Proposta_osinicio_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProposta_Proposta_osinicio_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Codigo_N") )
               {
                  gxTv_SdtProposta_Proposta_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Codigo_N") )
               {
                  gxTv_SdtProposta_Projeto_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Vigencia_N") )
               {
                  gxTv_SdtProposta_Proposta_vigencia_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Status_N") )
               {
                  gxTv_SdtProposta_Proposta_status_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Escopo_N") )
               {
                  gxTv_SdtProposta_Proposta_escopo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Oportunidade_N") )
               {
                  gxTv_SdtProposta_Proposta_oportunidade_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Perspectiva_N") )
               {
                  gxTv_SdtProposta_Proposta_perspectiva_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_RestricaoImplantacao_N") )
               {
                  gxTv_SdtProposta_Proposta_restricaoimplantacao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_RestricaoCusto_N") )
               {
                  gxTv_SdtProposta_Proposta_restricaocusto_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_ContratadaRazSocial_N") )
               {
                  gxTv_SdtProposta_Proposta_contratadarazsocial_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_PrazoDias_N") )
               {
                  gxTv_SdtProposta_Proposta_prazodias_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_Liquido_N") )
               {
                  gxTv_SdtProposta_Proposta_liquido_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSServico_N") )
               {
                  gxTv_SdtProposta_Proposta_osservico_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSCntCod_N") )
               {
                  gxTv_SdtProposta_Proposta_oscntcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSDmn_N") )
               {
                  gxTv_SdtProposta_Proposta_osdmn_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSDmnFM_N") )
               {
                  gxTv_SdtProposta_Proposta_osdmnfm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSUndCntCod_N") )
               {
                  gxTv_SdtProposta_Proposta_osundcntcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSUndCntSgl_N") )
               {
                  gxTv_SdtProposta_Proposta_osundcntsgl_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPrzTpDias_N") )
               {
                  gxTv_SdtProposta_Proposta_osprztpdias_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPrzRsp_N") )
               {
                  gxTv_SdtProposta_Proposta_osprzrsp_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPFB_N") )
               {
                  gxTv_SdtProposta_Proposta_ospfb_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSPFL_N") )
               {
                  gxTv_SdtProposta_Proposta_ospfl_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSDataCnt_N") )
               {
                  gxTv_SdtProposta_Proposta_osdatacnt_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSHoraCnt_N") )
               {
                  gxTv_SdtProposta_Proposta_oshoracnt_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSCntSrvFtrm_N") )
               {
                  gxTv_SdtProposta_Proposta_oscntsrvftrm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Proposta_OSInicio_N") )
               {
                  gxTv_SdtProposta_Proposta_osinicio_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Proposta";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Proposta_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_codigo), 9, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Projeto_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtProposta_Proposta_vigencia) )
         {
            oWriter.WriteStartElement("Proposta_Vigencia");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_vigencia)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_vigencia)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_vigencia)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtProposta_Proposta_vigencia)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtProposta_Proposta_vigencia)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtProposta_Proposta_vigencia)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Proposta_Vigencia", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("Proposta_Valor", StringUtil.Trim( StringUtil.Str( gxTv_SdtProposta_Proposta_valor, 15, 4)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_Status", StringUtil.RTrim( gxTv_SdtProposta_Proposta_status));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("AreaTrabalho_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Areatrabalho_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_Objetivo", StringUtil.RTrim( gxTv_SdtProposta_Proposta_objetivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_Escopo", StringUtil.RTrim( gxTv_SdtProposta_Proposta_escopo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_Oportunidade", StringUtil.RTrim( gxTv_SdtProposta_Proposta_oportunidade));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_Perspectiva", StringUtil.RTrim( gxTv_SdtProposta_Proposta_perspectiva));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtProposta_Proposta_restricaoimplantacao) )
         {
            oWriter.WriteStartElement("Proposta_RestricaoImplantacao");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_restricaoimplantacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_restricaoimplantacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_restricaoimplantacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtProposta_Proposta_restricaoimplantacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtProposta_Proposta_restricaoimplantacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtProposta_Proposta_restricaoimplantacao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Proposta_RestricaoImplantacao", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("Proposta_RestricaoCusto", StringUtil.Trim( StringUtil.Str( gxTv_SdtProposta_Proposta_restricaocusto, 15, 4)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtProposta_Proposta_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_ContratadaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_contratadacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_ContratadaRazSocial", StringUtil.RTrim( gxTv_SdtProposta_Proposta_contratadarazsocial));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtProposta_Proposta_prazo) )
         {
            oWriter.WriteStartElement("Proposta_Prazo");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_prazo)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Proposta_Prazo", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("Proposta_PrazoDias", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_prazodias), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_Esforco", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_esforco), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_Liquido", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_liquido), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_CntSrvCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_cntsrvcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_ValorUndCnt", StringUtil.Trim( StringUtil.Str( gxTv_SdtProposta_Proposta_valorundcnt, 10, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSCodigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_oscodigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSServico", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osservico), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSCntCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_oscntcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSDmn", StringUtil.RTrim( gxTv_SdtProposta_Proposta_osdmn));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSDmnFM", StringUtil.RTrim( gxTv_SdtProposta_Proposta_osdmnfm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSUndCntCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osundcntcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSUndCntSgl", StringUtil.RTrim( gxTv_SdtProposta_Proposta_osundcntsgl));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSPrzTpDias", StringUtil.RTrim( gxTv_SdtProposta_Proposta_osprztpdias));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSPrzRsp", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osprzrsp), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSPFB", StringUtil.Trim( StringUtil.Str( gxTv_SdtProposta_Proposta_ospfb, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSPFL", StringUtil.Trim( StringUtil.Str( gxTv_SdtProposta_Proposta_ospfl, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtProposta_Proposta_osdatacnt) )
         {
            oWriter.WriteStartElement("Proposta_OSDataCnt");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_osdatacnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_osdatacnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_osdatacnt)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Proposta_OSDataCnt", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("Proposta_OSHoraCnt", StringUtil.RTrim( gxTv_SdtProposta_Proposta_oshoracnt));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Proposta_OSCntSrvFtrm", StringUtil.RTrim( gxTv_SdtProposta_Proposta_oscntsrvftrm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtProposta_Proposta_osinicio) )
         {
            oWriter.WriteStartElement("Proposta_OSInicio");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_osinicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_osinicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_osinicio)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Proposta_OSInicio", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtProposta_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_codigo_Z), 9, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Projeto_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtProposta_Proposta_vigencia_Z) )
            {
               oWriter.WriteStartElement("Proposta_Vigencia_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_vigencia_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_vigencia_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_vigencia_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtProposta_Proposta_vigencia_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtProposta_Proposta_vigencia_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtProposta_Proposta_vigencia_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Proposta_Vigencia_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("Proposta_Valor_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProposta_Proposta_valor_Z, 15, 4)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_Status_Z", StringUtil.RTrim( gxTv_SdtProposta_Proposta_status_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("AreaTrabalho_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Areatrabalho_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtProposta_Proposta_restricaoimplantacao_Z) )
            {
               oWriter.WriteStartElement("Proposta_RestricaoImplantacao_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_restricaoimplantacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_restricaoimplantacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_restricaoimplantacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtProposta_Proposta_restricaoimplantacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtProposta_Proposta_restricaoimplantacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtProposta_Proposta_restricaoimplantacao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Proposta_RestricaoImplantacao_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("Proposta_RestricaoCusto_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProposta_Proposta_restricaocusto_Z, 15, 4)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtProposta_Proposta_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_ContratadaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_contratadacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_ContratadaRazSocial_Z", StringUtil.RTrim( gxTv_SdtProposta_Proposta_contratadarazsocial_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtProposta_Proposta_prazo_Z) )
            {
               oWriter.WriteStartElement("Proposta_Prazo_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_prazo_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_prazo_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_prazo_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Proposta_Prazo_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("Proposta_PrazoDias_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_prazodias_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_Esforco_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_esforco_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_Liquido_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_liquido_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_CntSrvCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_cntsrvcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_ValorUndCnt_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProposta_Proposta_valorundcnt_Z, 10, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSCodigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_oscodigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSServico_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osservico_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSCntCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_oscntcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSDmn_Z", StringUtil.RTrim( gxTv_SdtProposta_Proposta_osdmn_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSDmnFM_Z", StringUtil.RTrim( gxTv_SdtProposta_Proposta_osdmnfm_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSUndCntCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osundcntcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSUndCntSgl_Z", StringUtil.RTrim( gxTv_SdtProposta_Proposta_osundcntsgl_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSPrzTpDias_Z", StringUtil.RTrim( gxTv_SdtProposta_Proposta_osprztpdias_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSPrzRsp_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osprzrsp_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSPFB_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProposta_Proposta_ospfb_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSPFL_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProposta_Proposta_ospfl_Z, 14, 5)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtProposta_Proposta_osdatacnt_Z) )
            {
               oWriter.WriteStartElement("Proposta_OSDataCnt_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_osdatacnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_osdatacnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_osdatacnt_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Proposta_OSDataCnt_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("Proposta_OSHoraCnt_Z", StringUtil.RTrim( gxTv_SdtProposta_Proposta_oshoracnt_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSCntSrvFtrm_Z", StringUtil.RTrim( gxTv_SdtProposta_Proposta_oscntsrvftrm_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtProposta_Proposta_osinicio_Z) )
            {
               oWriter.WriteStartElement("Proposta_OSInicio_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_osinicio_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_osinicio_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_osinicio_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Proposta_OSInicio_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("Proposta_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Projeto_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_Vigencia_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_vigencia_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_Status_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_status_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_Escopo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_escopo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_Oportunidade_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_oportunidade_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_Perspectiva_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_perspectiva_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_RestricaoImplantacao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_restricaoimplantacao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_RestricaoCusto_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_restricaocusto_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_ContratadaRazSocial_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_contratadarazsocial_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_PrazoDias_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_prazodias_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_Liquido_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_liquido_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSServico_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osservico_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSCntCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_oscntcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSDmn_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osdmn_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSDmnFM_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osdmnfm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSUndCntCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osundcntcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSUndCntSgl_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osundcntsgl_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSPrzTpDias_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osprztpdias_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSPrzRsp_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osprzrsp_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSPFB_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_ospfb_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSPFL_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_ospfl_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSDataCnt_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osdatacnt_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSHoraCnt_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_oshoracnt_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSCntSrvFtrm_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_oscntsrvftrm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Proposta_OSInicio_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProposta_Proposta_osinicio_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Proposta_Codigo", gxTv_SdtProposta_Proposta_codigo, false);
         AddObjectProperty("Projeto_Codigo", gxTv_SdtProposta_Projeto_codigo, false);
         datetime_STZ = gxTv_SdtProposta_Proposta_vigencia;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Proposta_Vigencia", sDateCnv, false);
         AddObjectProperty("Proposta_Valor", gxTv_SdtProposta_Proposta_valor, false);
         AddObjectProperty("Proposta_Status", gxTv_SdtProposta_Proposta_status, false);
         AddObjectProperty("AreaTrabalho_Codigo", gxTv_SdtProposta_Areatrabalho_codigo, false);
         AddObjectProperty("Proposta_Objetivo", gxTv_SdtProposta_Proposta_objetivo, false);
         AddObjectProperty("Proposta_Escopo", gxTv_SdtProposta_Proposta_escopo, false);
         AddObjectProperty("Proposta_Oportunidade", gxTv_SdtProposta_Proposta_oportunidade, false);
         AddObjectProperty("Proposta_Perspectiva", gxTv_SdtProposta_Proposta_perspectiva, false);
         datetime_STZ = gxTv_SdtProposta_Proposta_restricaoimplantacao;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Proposta_RestricaoImplantacao", sDateCnv, false);
         AddObjectProperty("Proposta_RestricaoCusto", gxTv_SdtProposta_Proposta_restricaocusto, false);
         AddObjectProperty("Proposta_Ativo", gxTv_SdtProposta_Proposta_ativo, false);
         AddObjectProperty("Proposta_ContratadaCod", gxTv_SdtProposta_Proposta_contratadacod, false);
         AddObjectProperty("Proposta_ContratadaRazSocial", gxTv_SdtProposta_Proposta_contratadarazsocial, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_prazo)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_prazo)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_prazo)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Proposta_Prazo", sDateCnv, false);
         AddObjectProperty("Proposta_PrazoDias", gxTv_SdtProposta_Proposta_prazodias, false);
         AddObjectProperty("Proposta_Esforco", gxTv_SdtProposta_Proposta_esforco, false);
         AddObjectProperty("Proposta_Liquido", gxTv_SdtProposta_Proposta_liquido, false);
         AddObjectProperty("Proposta_CntSrvCod", gxTv_SdtProposta_Proposta_cntsrvcod, false);
         AddObjectProperty("Proposta_ValorUndCnt", gxTv_SdtProposta_Proposta_valorundcnt, false);
         AddObjectProperty("Proposta_OSCodigo", gxTv_SdtProposta_Proposta_oscodigo, false);
         AddObjectProperty("Proposta_OSServico", gxTv_SdtProposta_Proposta_osservico, false);
         AddObjectProperty("Proposta_OSCntCod", gxTv_SdtProposta_Proposta_oscntcod, false);
         AddObjectProperty("Proposta_OSDmn", gxTv_SdtProposta_Proposta_osdmn, false);
         AddObjectProperty("Proposta_OSDmnFM", gxTv_SdtProposta_Proposta_osdmnfm, false);
         AddObjectProperty("Proposta_OSUndCntCod", gxTv_SdtProposta_Proposta_osundcntcod, false);
         AddObjectProperty("Proposta_OSUndCntSgl", gxTv_SdtProposta_Proposta_osundcntsgl, false);
         AddObjectProperty("Proposta_OSPrzTpDias", gxTv_SdtProposta_Proposta_osprztpdias, false);
         AddObjectProperty("Proposta_OSPrzRsp", gxTv_SdtProposta_Proposta_osprzrsp, false);
         AddObjectProperty("Proposta_OSPFB", gxTv_SdtProposta_Proposta_ospfb, false);
         AddObjectProperty("Proposta_OSPFL", gxTv_SdtProposta_Proposta_ospfl, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_osdatacnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_osdatacnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_osdatacnt)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Proposta_OSDataCnt", sDateCnv, false);
         AddObjectProperty("Proposta_OSHoraCnt", gxTv_SdtProposta_Proposta_oshoracnt, false);
         AddObjectProperty("Proposta_OSCntSrvFtrm", gxTv_SdtProposta_Proposta_oscntsrvftrm, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_osinicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_osinicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_osinicio)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Proposta_OSInicio", sDateCnv, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtProposta_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtProposta_Initialized, false);
            AddObjectProperty("Proposta_Codigo_Z", gxTv_SdtProposta_Proposta_codigo_Z, false);
            AddObjectProperty("Projeto_Codigo_Z", gxTv_SdtProposta_Projeto_codigo_Z, false);
            datetime_STZ = gxTv_SdtProposta_Proposta_vigencia_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Proposta_Vigencia_Z", sDateCnv, false);
            AddObjectProperty("Proposta_Valor_Z", gxTv_SdtProposta_Proposta_valor_Z, false);
            AddObjectProperty("Proposta_Status_Z", gxTv_SdtProposta_Proposta_status_Z, false);
            AddObjectProperty("AreaTrabalho_Codigo_Z", gxTv_SdtProposta_Areatrabalho_codigo_Z, false);
            datetime_STZ = gxTv_SdtProposta_Proposta_restricaoimplantacao_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Proposta_RestricaoImplantacao_Z", sDateCnv, false);
            AddObjectProperty("Proposta_RestricaoCusto_Z", gxTv_SdtProposta_Proposta_restricaocusto_Z, false);
            AddObjectProperty("Proposta_Ativo_Z", gxTv_SdtProposta_Proposta_ativo_Z, false);
            AddObjectProperty("Proposta_ContratadaCod_Z", gxTv_SdtProposta_Proposta_contratadacod_Z, false);
            AddObjectProperty("Proposta_ContratadaRazSocial_Z", gxTv_SdtProposta_Proposta_contratadarazsocial_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_prazo_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_prazo_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_prazo_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Proposta_Prazo_Z", sDateCnv, false);
            AddObjectProperty("Proposta_PrazoDias_Z", gxTv_SdtProposta_Proposta_prazodias_Z, false);
            AddObjectProperty("Proposta_Esforco_Z", gxTv_SdtProposta_Proposta_esforco_Z, false);
            AddObjectProperty("Proposta_Liquido_Z", gxTv_SdtProposta_Proposta_liquido_Z, false);
            AddObjectProperty("Proposta_CntSrvCod_Z", gxTv_SdtProposta_Proposta_cntsrvcod_Z, false);
            AddObjectProperty("Proposta_ValorUndCnt_Z", gxTv_SdtProposta_Proposta_valorundcnt_Z, false);
            AddObjectProperty("Proposta_OSCodigo_Z", gxTv_SdtProposta_Proposta_oscodigo_Z, false);
            AddObjectProperty("Proposta_OSServico_Z", gxTv_SdtProposta_Proposta_osservico_Z, false);
            AddObjectProperty("Proposta_OSCntCod_Z", gxTv_SdtProposta_Proposta_oscntcod_Z, false);
            AddObjectProperty("Proposta_OSDmn_Z", gxTv_SdtProposta_Proposta_osdmn_Z, false);
            AddObjectProperty("Proposta_OSDmnFM_Z", gxTv_SdtProposta_Proposta_osdmnfm_Z, false);
            AddObjectProperty("Proposta_OSUndCntCod_Z", gxTv_SdtProposta_Proposta_osundcntcod_Z, false);
            AddObjectProperty("Proposta_OSUndCntSgl_Z", gxTv_SdtProposta_Proposta_osundcntsgl_Z, false);
            AddObjectProperty("Proposta_OSPrzTpDias_Z", gxTv_SdtProposta_Proposta_osprztpdias_Z, false);
            AddObjectProperty("Proposta_OSPrzRsp_Z", gxTv_SdtProposta_Proposta_osprzrsp_Z, false);
            AddObjectProperty("Proposta_OSPFB_Z", gxTv_SdtProposta_Proposta_ospfb_Z, false);
            AddObjectProperty("Proposta_OSPFL_Z", gxTv_SdtProposta_Proposta_ospfl_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_osdatacnt_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_osdatacnt_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_osdatacnt_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Proposta_OSDataCnt_Z", sDateCnv, false);
            AddObjectProperty("Proposta_OSHoraCnt_Z", gxTv_SdtProposta_Proposta_oshoracnt_Z, false);
            AddObjectProperty("Proposta_OSCntSrvFtrm_Z", gxTv_SdtProposta_Proposta_oscntsrvftrm_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProposta_Proposta_osinicio_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProposta_Proposta_osinicio_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProposta_Proposta_osinicio_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Proposta_OSInicio_Z", sDateCnv, false);
            AddObjectProperty("Proposta_Codigo_N", gxTv_SdtProposta_Proposta_codigo_N, false);
            AddObjectProperty("Projeto_Codigo_N", gxTv_SdtProposta_Projeto_codigo_N, false);
            AddObjectProperty("Proposta_Vigencia_N", gxTv_SdtProposta_Proposta_vigencia_N, false);
            AddObjectProperty("Proposta_Status_N", gxTv_SdtProposta_Proposta_status_N, false);
            AddObjectProperty("Proposta_Escopo_N", gxTv_SdtProposta_Proposta_escopo_N, false);
            AddObjectProperty("Proposta_Oportunidade_N", gxTv_SdtProposta_Proposta_oportunidade_N, false);
            AddObjectProperty("Proposta_Perspectiva_N", gxTv_SdtProposta_Proposta_perspectiva_N, false);
            AddObjectProperty("Proposta_RestricaoImplantacao_N", gxTv_SdtProposta_Proposta_restricaoimplantacao_N, false);
            AddObjectProperty("Proposta_RestricaoCusto_N", gxTv_SdtProposta_Proposta_restricaocusto_N, false);
            AddObjectProperty("Proposta_ContratadaRazSocial_N", gxTv_SdtProposta_Proposta_contratadarazsocial_N, false);
            AddObjectProperty("Proposta_PrazoDias_N", gxTv_SdtProposta_Proposta_prazodias_N, false);
            AddObjectProperty("Proposta_Liquido_N", gxTv_SdtProposta_Proposta_liquido_N, false);
            AddObjectProperty("Proposta_OSServico_N", gxTv_SdtProposta_Proposta_osservico_N, false);
            AddObjectProperty("Proposta_OSCntCod_N", gxTv_SdtProposta_Proposta_oscntcod_N, false);
            AddObjectProperty("Proposta_OSDmn_N", gxTv_SdtProposta_Proposta_osdmn_N, false);
            AddObjectProperty("Proposta_OSDmnFM_N", gxTv_SdtProposta_Proposta_osdmnfm_N, false);
            AddObjectProperty("Proposta_OSUndCntCod_N", gxTv_SdtProposta_Proposta_osundcntcod_N, false);
            AddObjectProperty("Proposta_OSUndCntSgl_N", gxTv_SdtProposta_Proposta_osundcntsgl_N, false);
            AddObjectProperty("Proposta_OSPrzTpDias_N", gxTv_SdtProposta_Proposta_osprztpdias_N, false);
            AddObjectProperty("Proposta_OSPrzRsp_N", gxTv_SdtProposta_Proposta_osprzrsp_N, false);
            AddObjectProperty("Proposta_OSPFB_N", gxTv_SdtProposta_Proposta_ospfb_N, false);
            AddObjectProperty("Proposta_OSPFL_N", gxTv_SdtProposta_Proposta_ospfl_N, false);
            AddObjectProperty("Proposta_OSDataCnt_N", gxTv_SdtProposta_Proposta_osdatacnt_N, false);
            AddObjectProperty("Proposta_OSHoraCnt_N", gxTv_SdtProposta_Proposta_oshoracnt_N, false);
            AddObjectProperty("Proposta_OSCntSrvFtrm_N", gxTv_SdtProposta_Proposta_oscntsrvftrm_N, false);
            AddObjectProperty("Proposta_OSInicio_N", gxTv_SdtProposta_Proposta_osinicio_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Proposta_Codigo" )]
      [  XmlElement( ElementName = "Proposta_Codigo"   )]
      public int gxTpr_Proposta_codigo
      {
         get {
            return gxTv_SdtProposta_Proposta_codigo ;
         }

         set {
            if ( gxTv_SdtProposta_Proposta_codigo != value )
            {
               gxTv_SdtProposta_Mode = "INS";
               this.gxTv_SdtProposta_Proposta_codigo_Z_SetNull( );
               this.gxTv_SdtProposta_Projeto_codigo_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_vigencia_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_valor_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_status_Z_SetNull( );
               this.gxTv_SdtProposta_Areatrabalho_codigo_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_restricaoimplantacao_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_restricaocusto_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_ativo_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_contratadacod_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_contratadarazsocial_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_prazo_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_prazodias_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_esforco_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_liquido_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_cntsrvcod_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_valorundcnt_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_oscodigo_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_osservico_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_oscntcod_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_osdmn_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_osdmnfm_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_osundcntcod_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_osundcntsgl_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_osprztpdias_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_osprzrsp_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_ospfb_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_ospfl_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_osdatacnt_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_oshoracnt_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_oscntsrvftrm_Z_SetNull( );
               this.gxTv_SdtProposta_Proposta_osinicio_Z_SetNull( );
            }
            gxTv_SdtProposta_Proposta_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_Codigo" )]
      [  XmlElement( ElementName = "Projeto_Codigo"   )]
      public int gxTpr_Projeto_codigo
      {
         get {
            return gxTv_SdtProposta_Projeto_codigo ;
         }

         set {
            gxTv_SdtProposta_Projeto_codigo_N = 0;
            gxTv_SdtProposta_Projeto_codigo = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Projeto_codigo_SetNull( )
      {
         gxTv_SdtProposta_Projeto_codigo_N = 1;
         gxTv_SdtProposta_Projeto_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Projeto_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Vigencia" )]
      [  XmlElement( ElementName = "Proposta_Vigencia"  , IsNullable=true )]
      public string gxTpr_Proposta_vigencia_Nullable
      {
         get {
            if ( gxTv_SdtProposta_Proposta_vigencia == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtProposta_Proposta_vigencia).value ;
         }

         set {
            gxTv_SdtProposta_Proposta_vigencia_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtProposta_Proposta_vigencia = DateTime.MinValue;
            else
               gxTv_SdtProposta_Proposta_vigencia = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Proposta_vigencia
      {
         get {
            return gxTv_SdtProposta_Proposta_vigencia ;
         }

         set {
            gxTv_SdtProposta_Proposta_vigencia_N = 0;
            gxTv_SdtProposta_Proposta_vigencia = (DateTime)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_vigencia_SetNull( )
      {
         gxTv_SdtProposta_Proposta_vigencia_N = 1;
         gxTv_SdtProposta_Proposta_vigencia = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_vigencia_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Valor" )]
      [  XmlElement( ElementName = "Proposta_Valor"   )]
      public double gxTpr_Proposta_valor_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProposta_Proposta_valor) ;
         }

         set {
            gxTv_SdtProposta_Proposta_valor = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Proposta_valor
      {
         get {
            return gxTv_SdtProposta_Proposta_valor ;
         }

         set {
            gxTv_SdtProposta_Proposta_valor = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "Proposta_Status" )]
      [  XmlElement( ElementName = "Proposta_Status"   )]
      public String gxTpr_Proposta_status
      {
         get {
            return gxTv_SdtProposta_Proposta_status ;
         }

         set {
            gxTv_SdtProposta_Proposta_status_N = 0;
            gxTv_SdtProposta_Proposta_status = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_status_SetNull( )
      {
         gxTv_SdtProposta_Proposta_status_N = 1;
         gxTv_SdtProposta_Proposta_status = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_status_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Codigo" )]
      [  XmlElement( ElementName = "AreaTrabalho_Codigo"   )]
      public int gxTpr_Areatrabalho_codigo
      {
         get {
            return gxTv_SdtProposta_Areatrabalho_codigo ;
         }

         set {
            gxTv_SdtProposta_Areatrabalho_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Proposta_Objetivo" )]
      [  XmlElement( ElementName = "Proposta_Objetivo"   )]
      public String gxTpr_Proposta_objetivo
      {
         get {
            return gxTv_SdtProposta_Proposta_objetivo ;
         }

         set {
            gxTv_SdtProposta_Proposta_objetivo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Proposta_Escopo" )]
      [  XmlElement( ElementName = "Proposta_Escopo"   )]
      public String gxTpr_Proposta_escopo
      {
         get {
            return gxTv_SdtProposta_Proposta_escopo ;
         }

         set {
            gxTv_SdtProposta_Proposta_escopo_N = 0;
            gxTv_SdtProposta_Proposta_escopo = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_escopo_SetNull( )
      {
         gxTv_SdtProposta_Proposta_escopo_N = 1;
         gxTv_SdtProposta_Proposta_escopo = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_escopo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Oportunidade" )]
      [  XmlElement( ElementName = "Proposta_Oportunidade"   )]
      public String gxTpr_Proposta_oportunidade
      {
         get {
            return gxTv_SdtProposta_Proposta_oportunidade ;
         }

         set {
            gxTv_SdtProposta_Proposta_oportunidade_N = 0;
            gxTv_SdtProposta_Proposta_oportunidade = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oportunidade_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oportunidade_N = 1;
         gxTv_SdtProposta_Proposta_oportunidade = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oportunidade_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Perspectiva" )]
      [  XmlElement( ElementName = "Proposta_Perspectiva"   )]
      public String gxTpr_Proposta_perspectiva
      {
         get {
            return gxTv_SdtProposta_Proposta_perspectiva ;
         }

         set {
            gxTv_SdtProposta_Proposta_perspectiva_N = 0;
            gxTv_SdtProposta_Proposta_perspectiva = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_perspectiva_SetNull( )
      {
         gxTv_SdtProposta_Proposta_perspectiva_N = 1;
         gxTv_SdtProposta_Proposta_perspectiva = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_perspectiva_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_RestricaoImplantacao" )]
      [  XmlElement( ElementName = "Proposta_RestricaoImplantacao"  , IsNullable=true )]
      public string gxTpr_Proposta_restricaoimplantacao_Nullable
      {
         get {
            if ( gxTv_SdtProposta_Proposta_restricaoimplantacao == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtProposta_Proposta_restricaoimplantacao).value ;
         }

         set {
            gxTv_SdtProposta_Proposta_restricaoimplantacao_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtProposta_Proposta_restricaoimplantacao = DateTime.MinValue;
            else
               gxTv_SdtProposta_Proposta_restricaoimplantacao = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Proposta_restricaoimplantacao
      {
         get {
            return gxTv_SdtProposta_Proposta_restricaoimplantacao ;
         }

         set {
            gxTv_SdtProposta_Proposta_restricaoimplantacao_N = 0;
            gxTv_SdtProposta_Proposta_restricaoimplantacao = (DateTime)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_restricaoimplantacao_SetNull( )
      {
         gxTv_SdtProposta_Proposta_restricaoimplantacao_N = 1;
         gxTv_SdtProposta_Proposta_restricaoimplantacao = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_restricaoimplantacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_RestricaoCusto" )]
      [  XmlElement( ElementName = "Proposta_RestricaoCusto"   )]
      public double gxTpr_Proposta_restricaocusto_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProposta_Proposta_restricaocusto) ;
         }

         set {
            gxTv_SdtProposta_Proposta_restricaocusto_N = 0;
            gxTv_SdtProposta_Proposta_restricaocusto = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Proposta_restricaocusto
      {
         get {
            return gxTv_SdtProposta_Proposta_restricaocusto ;
         }

         set {
            gxTv_SdtProposta_Proposta_restricaocusto_N = 0;
            gxTv_SdtProposta_Proposta_restricaocusto = (decimal)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_restricaocusto_SetNull( )
      {
         gxTv_SdtProposta_Proposta_restricaocusto_N = 1;
         gxTv_SdtProposta_Proposta_restricaocusto = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_restricaocusto_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Ativo" )]
      [  XmlElement( ElementName = "Proposta_Ativo"   )]
      public bool gxTpr_Proposta_ativo
      {
         get {
            return gxTv_SdtProposta_Proposta_ativo ;
         }

         set {
            gxTv_SdtProposta_Proposta_ativo = value;
         }

      }

      [  SoapElement( ElementName = "Proposta_ContratadaCod" )]
      [  XmlElement( ElementName = "Proposta_ContratadaCod"   )]
      public int gxTpr_Proposta_contratadacod
      {
         get {
            return gxTv_SdtProposta_Proposta_contratadacod ;
         }

         set {
            gxTv_SdtProposta_Proposta_contratadacod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Proposta_ContratadaRazSocial" )]
      [  XmlElement( ElementName = "Proposta_ContratadaRazSocial"   )]
      public String gxTpr_Proposta_contratadarazsocial
      {
         get {
            return gxTv_SdtProposta_Proposta_contratadarazsocial ;
         }

         set {
            gxTv_SdtProposta_Proposta_contratadarazsocial_N = 0;
            gxTv_SdtProposta_Proposta_contratadarazsocial = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_contratadarazsocial_SetNull( )
      {
         gxTv_SdtProposta_Proposta_contratadarazsocial_N = 1;
         gxTv_SdtProposta_Proposta_contratadarazsocial = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_contratadarazsocial_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Prazo" )]
      [  XmlElement( ElementName = "Proposta_Prazo"  , IsNullable=true )]
      public string gxTpr_Proposta_prazo_Nullable
      {
         get {
            if ( gxTv_SdtProposta_Proposta_prazo == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProposta_Proposta_prazo).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProposta_Proposta_prazo = DateTime.MinValue;
            else
               gxTv_SdtProposta_Proposta_prazo = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Proposta_prazo
      {
         get {
            return gxTv_SdtProposta_Proposta_prazo ;
         }

         set {
            gxTv_SdtProposta_Proposta_prazo = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "Proposta_PrazoDias" )]
      [  XmlElement( ElementName = "Proposta_PrazoDias"   )]
      public short gxTpr_Proposta_prazodias
      {
         get {
            return gxTv_SdtProposta_Proposta_prazodias ;
         }

         set {
            gxTv_SdtProposta_Proposta_prazodias_N = 0;
            gxTv_SdtProposta_Proposta_prazodias = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_prazodias_SetNull( )
      {
         gxTv_SdtProposta_Proposta_prazodias_N = 1;
         gxTv_SdtProposta_Proposta_prazodias = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_prazodias_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Esforco" )]
      [  XmlElement( ElementName = "Proposta_Esforco"   )]
      public int gxTpr_Proposta_esforco
      {
         get {
            return gxTv_SdtProposta_Proposta_esforco ;
         }

         set {
            gxTv_SdtProposta_Proposta_esforco = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Proposta_Liquido" )]
      [  XmlElement( ElementName = "Proposta_Liquido"   )]
      public int gxTpr_Proposta_liquido
      {
         get {
            return gxTv_SdtProposta_Proposta_liquido ;
         }

         set {
            gxTv_SdtProposta_Proposta_liquido_N = 0;
            gxTv_SdtProposta_Proposta_liquido = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_liquido_SetNull( )
      {
         gxTv_SdtProposta_Proposta_liquido_N = 1;
         gxTv_SdtProposta_Proposta_liquido = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_liquido_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_CntSrvCod" )]
      [  XmlElement( ElementName = "Proposta_CntSrvCod"   )]
      public int gxTpr_Proposta_cntsrvcod
      {
         get {
            return gxTv_SdtProposta_Proposta_cntsrvcod ;
         }

         set {
            gxTv_SdtProposta_Proposta_cntsrvcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Proposta_ValorUndCnt" )]
      [  XmlElement( ElementName = "Proposta_ValorUndCnt"   )]
      public double gxTpr_Proposta_valorundcnt_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProposta_Proposta_valorundcnt) ;
         }

         set {
            gxTv_SdtProposta_Proposta_valorundcnt = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Proposta_valorundcnt
      {
         get {
            return gxTv_SdtProposta_Proposta_valorundcnt ;
         }

         set {
            gxTv_SdtProposta_Proposta_valorundcnt = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "Proposta_OSCodigo" )]
      [  XmlElement( ElementName = "Proposta_OSCodigo"   )]
      public int gxTpr_Proposta_oscodigo
      {
         get {
            return gxTv_SdtProposta_Proposta_oscodigo ;
         }

         set {
            gxTv_SdtProposta_Proposta_oscodigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Proposta_OSServico" )]
      [  XmlElement( ElementName = "Proposta_OSServico"   )]
      public int gxTpr_Proposta_osservico
      {
         get {
            return gxTv_SdtProposta_Proposta_osservico ;
         }

         set {
            gxTv_SdtProposta_Proposta_osservico_N = 0;
            gxTv_SdtProposta_Proposta_osservico = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osservico_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osservico_N = 1;
         gxTv_SdtProposta_Proposta_osservico = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osservico_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSCntCod" )]
      [  XmlElement( ElementName = "Proposta_OSCntCod"   )]
      public int gxTpr_Proposta_oscntcod
      {
         get {
            return gxTv_SdtProposta_Proposta_oscntcod ;
         }

         set {
            gxTv_SdtProposta_Proposta_oscntcod_N = 0;
            gxTv_SdtProposta_Proposta_oscntcod = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oscntcod_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oscntcod_N = 1;
         gxTv_SdtProposta_Proposta_oscntcod = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oscntcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSDmn" )]
      [  XmlElement( ElementName = "Proposta_OSDmn"   )]
      public String gxTpr_Proposta_osdmn
      {
         get {
            return gxTv_SdtProposta_Proposta_osdmn ;
         }

         set {
            gxTv_SdtProposta_Proposta_osdmn_N = 0;
            gxTv_SdtProposta_Proposta_osdmn = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osdmn_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osdmn_N = 1;
         gxTv_SdtProposta_Proposta_osdmn = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osdmn_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSDmnFM" )]
      [  XmlElement( ElementName = "Proposta_OSDmnFM"   )]
      public String gxTpr_Proposta_osdmnfm
      {
         get {
            return gxTv_SdtProposta_Proposta_osdmnfm ;
         }

         set {
            gxTv_SdtProposta_Proposta_osdmnfm_N = 0;
            gxTv_SdtProposta_Proposta_osdmnfm = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osdmnfm_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osdmnfm_N = 1;
         gxTv_SdtProposta_Proposta_osdmnfm = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osdmnfm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSUndCntCod" )]
      [  XmlElement( ElementName = "Proposta_OSUndCntCod"   )]
      public int gxTpr_Proposta_osundcntcod
      {
         get {
            return gxTv_SdtProposta_Proposta_osundcntcod ;
         }

         set {
            gxTv_SdtProposta_Proposta_osundcntcod_N = 0;
            gxTv_SdtProposta_Proposta_osundcntcod = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osundcntcod_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osundcntcod_N = 1;
         gxTv_SdtProposta_Proposta_osundcntcod = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osundcntcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSUndCntSgl" )]
      [  XmlElement( ElementName = "Proposta_OSUndCntSgl"   )]
      public String gxTpr_Proposta_osundcntsgl
      {
         get {
            return gxTv_SdtProposta_Proposta_osundcntsgl ;
         }

         set {
            gxTv_SdtProposta_Proposta_osundcntsgl_N = 0;
            gxTv_SdtProposta_Proposta_osundcntsgl = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osundcntsgl_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osundcntsgl_N = 1;
         gxTv_SdtProposta_Proposta_osundcntsgl = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osundcntsgl_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPrzTpDias" )]
      [  XmlElement( ElementName = "Proposta_OSPrzTpDias"   )]
      public String gxTpr_Proposta_osprztpdias
      {
         get {
            return gxTv_SdtProposta_Proposta_osprztpdias ;
         }

         set {
            gxTv_SdtProposta_Proposta_osprztpdias_N = 0;
            gxTv_SdtProposta_Proposta_osprztpdias = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osprztpdias_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osprztpdias_N = 1;
         gxTv_SdtProposta_Proposta_osprztpdias = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osprztpdias_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPrzRsp" )]
      [  XmlElement( ElementName = "Proposta_OSPrzRsp"   )]
      public short gxTpr_Proposta_osprzrsp
      {
         get {
            return gxTv_SdtProposta_Proposta_osprzrsp ;
         }

         set {
            gxTv_SdtProposta_Proposta_osprzrsp_N = 0;
            gxTv_SdtProposta_Proposta_osprzrsp = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osprzrsp_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osprzrsp_N = 1;
         gxTv_SdtProposta_Proposta_osprzrsp = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osprzrsp_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPFB" )]
      [  XmlElement( ElementName = "Proposta_OSPFB"   )]
      public double gxTpr_Proposta_ospfb_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProposta_Proposta_ospfb) ;
         }

         set {
            gxTv_SdtProposta_Proposta_ospfb_N = 0;
            gxTv_SdtProposta_Proposta_ospfb = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Proposta_ospfb
      {
         get {
            return gxTv_SdtProposta_Proposta_ospfb ;
         }

         set {
            gxTv_SdtProposta_Proposta_ospfb_N = 0;
            gxTv_SdtProposta_Proposta_ospfb = (decimal)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_ospfb_SetNull( )
      {
         gxTv_SdtProposta_Proposta_ospfb_N = 1;
         gxTv_SdtProposta_Proposta_ospfb = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_ospfb_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPFL" )]
      [  XmlElement( ElementName = "Proposta_OSPFL"   )]
      public double gxTpr_Proposta_ospfl_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProposta_Proposta_ospfl) ;
         }

         set {
            gxTv_SdtProposta_Proposta_ospfl_N = 0;
            gxTv_SdtProposta_Proposta_ospfl = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Proposta_ospfl
      {
         get {
            return gxTv_SdtProposta_Proposta_ospfl ;
         }

         set {
            gxTv_SdtProposta_Proposta_ospfl_N = 0;
            gxTv_SdtProposta_Proposta_ospfl = (decimal)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_ospfl_SetNull( )
      {
         gxTv_SdtProposta_Proposta_ospfl_N = 1;
         gxTv_SdtProposta_Proposta_ospfl = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_ospfl_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSDataCnt" )]
      [  XmlElement( ElementName = "Proposta_OSDataCnt"  , IsNullable=true )]
      public string gxTpr_Proposta_osdatacnt_Nullable
      {
         get {
            if ( gxTv_SdtProposta_Proposta_osdatacnt == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProposta_Proposta_osdatacnt).value ;
         }

         set {
            gxTv_SdtProposta_Proposta_osdatacnt_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProposta_Proposta_osdatacnt = DateTime.MinValue;
            else
               gxTv_SdtProposta_Proposta_osdatacnt = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Proposta_osdatacnt
      {
         get {
            return gxTv_SdtProposta_Proposta_osdatacnt ;
         }

         set {
            gxTv_SdtProposta_Proposta_osdatacnt_N = 0;
            gxTv_SdtProposta_Proposta_osdatacnt = (DateTime)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osdatacnt_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osdatacnt_N = 1;
         gxTv_SdtProposta_Proposta_osdatacnt = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osdatacnt_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSHoraCnt" )]
      [  XmlElement( ElementName = "Proposta_OSHoraCnt"   )]
      public String gxTpr_Proposta_oshoracnt
      {
         get {
            return gxTv_SdtProposta_Proposta_oshoracnt ;
         }

         set {
            gxTv_SdtProposta_Proposta_oshoracnt_N = 0;
            gxTv_SdtProposta_Proposta_oshoracnt = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oshoracnt_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oshoracnt_N = 1;
         gxTv_SdtProposta_Proposta_oshoracnt = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oshoracnt_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSCntSrvFtrm" )]
      [  XmlElement( ElementName = "Proposta_OSCntSrvFtrm"   )]
      public String gxTpr_Proposta_oscntsrvftrm
      {
         get {
            return gxTv_SdtProposta_Proposta_oscntsrvftrm ;
         }

         set {
            gxTv_SdtProposta_Proposta_oscntsrvftrm_N = 0;
            gxTv_SdtProposta_Proposta_oscntsrvftrm = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oscntsrvftrm_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oscntsrvftrm_N = 1;
         gxTv_SdtProposta_Proposta_oscntsrvftrm = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oscntsrvftrm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSInicio" )]
      [  XmlElement( ElementName = "Proposta_OSInicio"  , IsNullable=true )]
      public string gxTpr_Proposta_osinicio_Nullable
      {
         get {
            if ( gxTv_SdtProposta_Proposta_osinicio == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProposta_Proposta_osinicio).value ;
         }

         set {
            gxTv_SdtProposta_Proposta_osinicio_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProposta_Proposta_osinicio = DateTime.MinValue;
            else
               gxTv_SdtProposta_Proposta_osinicio = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Proposta_osinicio
      {
         get {
            return gxTv_SdtProposta_Proposta_osinicio ;
         }

         set {
            gxTv_SdtProposta_Proposta_osinicio_N = 0;
            gxTv_SdtProposta_Proposta_osinicio = (DateTime)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osinicio_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osinicio_N = 1;
         gxTv_SdtProposta_Proposta_osinicio = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osinicio_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtProposta_Mode ;
         }

         set {
            gxTv_SdtProposta_Mode = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Mode_SetNull( )
      {
         gxTv_SdtProposta_Mode = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtProposta_Initialized ;
         }

         set {
            gxTv_SdtProposta_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Initialized_SetNull( )
      {
         gxTv_SdtProposta_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Codigo_Z" )]
      [  XmlElement( ElementName = "Proposta_Codigo_Z"   )]
      public int gxTpr_Proposta_codigo_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_codigo_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_codigo_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Codigo_Z" )]
      [  XmlElement( ElementName = "Projeto_Codigo_Z"   )]
      public int gxTpr_Projeto_codigo_Z
      {
         get {
            return gxTv_SdtProposta_Projeto_codigo_Z ;
         }

         set {
            gxTv_SdtProposta_Projeto_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Projeto_codigo_Z_SetNull( )
      {
         gxTv_SdtProposta_Projeto_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Projeto_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Vigencia_Z" )]
      [  XmlElement( ElementName = "Proposta_Vigencia_Z"  , IsNullable=true )]
      public string gxTpr_Proposta_vigencia_Z_Nullable
      {
         get {
            if ( gxTv_SdtProposta_Proposta_vigencia_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtProposta_Proposta_vigencia_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtProposta_Proposta_vigencia_Z = DateTime.MinValue;
            else
               gxTv_SdtProposta_Proposta_vigencia_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Proposta_vigencia_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_vigencia_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_vigencia_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_vigencia_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_vigencia_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_vigencia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Valor_Z" )]
      [  XmlElement( ElementName = "Proposta_Valor_Z"   )]
      public double gxTpr_Proposta_valor_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProposta_Proposta_valor_Z) ;
         }

         set {
            gxTv_SdtProposta_Proposta_valor_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Proposta_valor_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_valor_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_valor_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_valor_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_valor_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_valor_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Status_Z" )]
      [  XmlElement( ElementName = "Proposta_Status_Z"   )]
      public String gxTpr_Proposta_status_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_status_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_status_Z = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_status_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_status_Z = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_status_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Codigo_Z" )]
      [  XmlElement( ElementName = "AreaTrabalho_Codigo_Z"   )]
      public int gxTpr_Areatrabalho_codigo_Z
      {
         get {
            return gxTv_SdtProposta_Areatrabalho_codigo_Z ;
         }

         set {
            gxTv_SdtProposta_Areatrabalho_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Areatrabalho_codigo_Z_SetNull( )
      {
         gxTv_SdtProposta_Areatrabalho_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Areatrabalho_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_RestricaoImplantacao_Z" )]
      [  XmlElement( ElementName = "Proposta_RestricaoImplantacao_Z"  , IsNullable=true )]
      public string gxTpr_Proposta_restricaoimplantacao_Z_Nullable
      {
         get {
            if ( gxTv_SdtProposta_Proposta_restricaoimplantacao_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtProposta_Proposta_restricaoimplantacao_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtProposta_Proposta_restricaoimplantacao_Z = DateTime.MinValue;
            else
               gxTv_SdtProposta_Proposta_restricaoimplantacao_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Proposta_restricaoimplantacao_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_restricaoimplantacao_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_restricaoimplantacao_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_restricaoimplantacao_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_restricaoimplantacao_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_restricaoimplantacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_RestricaoCusto_Z" )]
      [  XmlElement( ElementName = "Proposta_RestricaoCusto_Z"   )]
      public double gxTpr_Proposta_restricaocusto_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProposta_Proposta_restricaocusto_Z) ;
         }

         set {
            gxTv_SdtProposta_Proposta_restricaocusto_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Proposta_restricaocusto_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_restricaocusto_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_restricaocusto_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_restricaocusto_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_restricaocusto_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_restricaocusto_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Ativo_Z" )]
      [  XmlElement( ElementName = "Proposta_Ativo_Z"   )]
      public bool gxTpr_Proposta_ativo_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_ativo_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_ativo_Z = value;
         }

      }

      public void gxTv_SdtProposta_Proposta_ativo_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_ContratadaCod_Z" )]
      [  XmlElement( ElementName = "Proposta_ContratadaCod_Z"   )]
      public int gxTpr_Proposta_contratadacod_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_contratadacod_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_contratadacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_contratadacod_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_contratadacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_contratadacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_ContratadaRazSocial_Z" )]
      [  XmlElement( ElementName = "Proposta_ContratadaRazSocial_Z"   )]
      public String gxTpr_Proposta_contratadarazsocial_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_contratadarazsocial_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_contratadarazsocial_Z = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_contratadarazsocial_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_contratadarazsocial_Z = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_contratadarazsocial_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Prazo_Z" )]
      [  XmlElement( ElementName = "Proposta_Prazo_Z"  , IsNullable=true )]
      public string gxTpr_Proposta_prazo_Z_Nullable
      {
         get {
            if ( gxTv_SdtProposta_Proposta_prazo_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProposta_Proposta_prazo_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProposta_Proposta_prazo_Z = DateTime.MinValue;
            else
               gxTv_SdtProposta_Proposta_prazo_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Proposta_prazo_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_prazo_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_prazo_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_prazo_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_prazo_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_prazo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_PrazoDias_Z" )]
      [  XmlElement( ElementName = "Proposta_PrazoDias_Z"   )]
      public short gxTpr_Proposta_prazodias_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_prazodias_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_prazodias_Z = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_prazodias_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_prazodias_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_prazodias_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Esforco_Z" )]
      [  XmlElement( ElementName = "Proposta_Esforco_Z"   )]
      public int gxTpr_Proposta_esforco_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_esforco_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_esforco_Z = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_esforco_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_esforco_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_esforco_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Liquido_Z" )]
      [  XmlElement( ElementName = "Proposta_Liquido_Z"   )]
      public int gxTpr_Proposta_liquido_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_liquido_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_liquido_Z = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_liquido_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_liquido_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_liquido_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_CntSrvCod_Z" )]
      [  XmlElement( ElementName = "Proposta_CntSrvCod_Z"   )]
      public int gxTpr_Proposta_cntsrvcod_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_cntsrvcod_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_cntsrvcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_cntsrvcod_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_cntsrvcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_cntsrvcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_ValorUndCnt_Z" )]
      [  XmlElement( ElementName = "Proposta_ValorUndCnt_Z"   )]
      public double gxTpr_Proposta_valorundcnt_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProposta_Proposta_valorundcnt_Z) ;
         }

         set {
            gxTv_SdtProposta_Proposta_valorundcnt_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Proposta_valorundcnt_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_valorundcnt_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_valorundcnt_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_valorundcnt_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_valorundcnt_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_valorundcnt_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSCodigo_Z" )]
      [  XmlElement( ElementName = "Proposta_OSCodigo_Z"   )]
      public int gxTpr_Proposta_oscodigo_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_oscodigo_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_oscodigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oscodigo_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oscodigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oscodigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSServico_Z" )]
      [  XmlElement( ElementName = "Proposta_OSServico_Z"   )]
      public int gxTpr_Proposta_osservico_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_osservico_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_osservico_Z = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osservico_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osservico_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osservico_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSCntCod_Z" )]
      [  XmlElement( ElementName = "Proposta_OSCntCod_Z"   )]
      public int gxTpr_Proposta_oscntcod_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_oscntcod_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_oscntcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oscntcod_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oscntcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oscntcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSDmn_Z" )]
      [  XmlElement( ElementName = "Proposta_OSDmn_Z"   )]
      public String gxTpr_Proposta_osdmn_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_osdmn_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_osdmn_Z = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osdmn_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osdmn_Z = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osdmn_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSDmnFM_Z" )]
      [  XmlElement( ElementName = "Proposta_OSDmnFM_Z"   )]
      public String gxTpr_Proposta_osdmnfm_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_osdmnfm_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_osdmnfm_Z = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osdmnfm_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osdmnfm_Z = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osdmnfm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSUndCntCod_Z" )]
      [  XmlElement( ElementName = "Proposta_OSUndCntCod_Z"   )]
      public int gxTpr_Proposta_osundcntcod_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_osundcntcod_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_osundcntcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osundcntcod_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osundcntcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osundcntcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSUndCntSgl_Z" )]
      [  XmlElement( ElementName = "Proposta_OSUndCntSgl_Z"   )]
      public String gxTpr_Proposta_osundcntsgl_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_osundcntsgl_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_osundcntsgl_Z = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osundcntsgl_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osundcntsgl_Z = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osundcntsgl_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPrzTpDias_Z" )]
      [  XmlElement( ElementName = "Proposta_OSPrzTpDias_Z"   )]
      public String gxTpr_Proposta_osprztpdias_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_osprztpdias_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_osprztpdias_Z = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osprztpdias_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osprztpdias_Z = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osprztpdias_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPrzRsp_Z" )]
      [  XmlElement( ElementName = "Proposta_OSPrzRsp_Z"   )]
      public short gxTpr_Proposta_osprzrsp_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_osprzrsp_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_osprzrsp_Z = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osprzrsp_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osprzrsp_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osprzrsp_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPFB_Z" )]
      [  XmlElement( ElementName = "Proposta_OSPFB_Z"   )]
      public double gxTpr_Proposta_ospfb_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProposta_Proposta_ospfb_Z) ;
         }

         set {
            gxTv_SdtProposta_Proposta_ospfb_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Proposta_ospfb_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_ospfb_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_ospfb_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_ospfb_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_ospfb_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_ospfb_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPFL_Z" )]
      [  XmlElement( ElementName = "Proposta_OSPFL_Z"   )]
      public double gxTpr_Proposta_ospfl_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProposta_Proposta_ospfl_Z) ;
         }

         set {
            gxTv_SdtProposta_Proposta_ospfl_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Proposta_ospfl_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_ospfl_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_ospfl_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_ospfl_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_ospfl_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_ospfl_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSDataCnt_Z" )]
      [  XmlElement( ElementName = "Proposta_OSDataCnt_Z"  , IsNullable=true )]
      public string gxTpr_Proposta_osdatacnt_Z_Nullable
      {
         get {
            if ( gxTv_SdtProposta_Proposta_osdatacnt_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProposta_Proposta_osdatacnt_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProposta_Proposta_osdatacnt_Z = DateTime.MinValue;
            else
               gxTv_SdtProposta_Proposta_osdatacnt_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Proposta_osdatacnt_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_osdatacnt_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_osdatacnt_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osdatacnt_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osdatacnt_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osdatacnt_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSHoraCnt_Z" )]
      [  XmlElement( ElementName = "Proposta_OSHoraCnt_Z"   )]
      public String gxTpr_Proposta_oshoracnt_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_oshoracnt_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_oshoracnt_Z = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oshoracnt_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oshoracnt_Z = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oshoracnt_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSCntSrvFtrm_Z" )]
      [  XmlElement( ElementName = "Proposta_OSCntSrvFtrm_Z"   )]
      public String gxTpr_Proposta_oscntsrvftrm_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_oscntsrvftrm_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_oscntsrvftrm_Z = (String)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oscntsrvftrm_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oscntsrvftrm_Z = "";
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oscntsrvftrm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSInicio_Z" )]
      [  XmlElement( ElementName = "Proposta_OSInicio_Z"  , IsNullable=true )]
      public string gxTpr_Proposta_osinicio_Z_Nullable
      {
         get {
            if ( gxTv_SdtProposta_Proposta_osinicio_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProposta_Proposta_osinicio_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProposta_Proposta_osinicio_Z = DateTime.MinValue;
            else
               gxTv_SdtProposta_Proposta_osinicio_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Proposta_osinicio_Z
      {
         get {
            return gxTv_SdtProposta_Proposta_osinicio_Z ;
         }

         set {
            gxTv_SdtProposta_Proposta_osinicio_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osinicio_Z_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osinicio_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osinicio_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Codigo_N" )]
      [  XmlElement( ElementName = "Proposta_Codigo_N"   )]
      public short gxTpr_Proposta_codigo_N
      {
         get {
            return gxTv_SdtProposta_Proposta_codigo_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_codigo_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Codigo_N" )]
      [  XmlElement( ElementName = "Projeto_Codigo_N"   )]
      public short gxTpr_Projeto_codigo_N
      {
         get {
            return gxTv_SdtProposta_Projeto_codigo_N ;
         }

         set {
            gxTv_SdtProposta_Projeto_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Projeto_codigo_N_SetNull( )
      {
         gxTv_SdtProposta_Projeto_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Projeto_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Vigencia_N" )]
      [  XmlElement( ElementName = "Proposta_Vigencia_N"   )]
      public short gxTpr_Proposta_vigencia_N
      {
         get {
            return gxTv_SdtProposta_Proposta_vigencia_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_vigencia_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_vigencia_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_vigencia_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_vigencia_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Status_N" )]
      [  XmlElement( ElementName = "Proposta_Status_N"   )]
      public short gxTpr_Proposta_status_N
      {
         get {
            return gxTv_SdtProposta_Proposta_status_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_status_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_status_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_status_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_status_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Escopo_N" )]
      [  XmlElement( ElementName = "Proposta_Escopo_N"   )]
      public short gxTpr_Proposta_escopo_N
      {
         get {
            return gxTv_SdtProposta_Proposta_escopo_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_escopo_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_escopo_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_escopo_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_escopo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Oportunidade_N" )]
      [  XmlElement( ElementName = "Proposta_Oportunidade_N"   )]
      public short gxTpr_Proposta_oportunidade_N
      {
         get {
            return gxTv_SdtProposta_Proposta_oportunidade_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_oportunidade_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oportunidade_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oportunidade_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oportunidade_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Perspectiva_N" )]
      [  XmlElement( ElementName = "Proposta_Perspectiva_N"   )]
      public short gxTpr_Proposta_perspectiva_N
      {
         get {
            return gxTv_SdtProposta_Proposta_perspectiva_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_perspectiva_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_perspectiva_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_perspectiva_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_perspectiva_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_RestricaoImplantacao_N" )]
      [  XmlElement( ElementName = "Proposta_RestricaoImplantacao_N"   )]
      public short gxTpr_Proposta_restricaoimplantacao_N
      {
         get {
            return gxTv_SdtProposta_Proposta_restricaoimplantacao_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_restricaoimplantacao_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_restricaoimplantacao_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_restricaoimplantacao_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_restricaoimplantacao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_RestricaoCusto_N" )]
      [  XmlElement( ElementName = "Proposta_RestricaoCusto_N"   )]
      public short gxTpr_Proposta_restricaocusto_N
      {
         get {
            return gxTv_SdtProposta_Proposta_restricaocusto_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_restricaocusto_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_restricaocusto_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_restricaocusto_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_restricaocusto_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_ContratadaRazSocial_N" )]
      [  XmlElement( ElementName = "Proposta_ContratadaRazSocial_N"   )]
      public short gxTpr_Proposta_contratadarazsocial_N
      {
         get {
            return gxTv_SdtProposta_Proposta_contratadarazsocial_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_contratadarazsocial_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_contratadarazsocial_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_contratadarazsocial_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_contratadarazsocial_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_PrazoDias_N" )]
      [  XmlElement( ElementName = "Proposta_PrazoDias_N"   )]
      public short gxTpr_Proposta_prazodias_N
      {
         get {
            return gxTv_SdtProposta_Proposta_prazodias_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_prazodias_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_prazodias_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_prazodias_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_prazodias_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_Liquido_N" )]
      [  XmlElement( ElementName = "Proposta_Liquido_N"   )]
      public short gxTpr_Proposta_liquido_N
      {
         get {
            return gxTv_SdtProposta_Proposta_liquido_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_liquido_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_liquido_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_liquido_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_liquido_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSServico_N" )]
      [  XmlElement( ElementName = "Proposta_OSServico_N"   )]
      public short gxTpr_Proposta_osservico_N
      {
         get {
            return gxTv_SdtProposta_Proposta_osservico_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_osservico_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osservico_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osservico_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osservico_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSCntCod_N" )]
      [  XmlElement( ElementName = "Proposta_OSCntCod_N"   )]
      public short gxTpr_Proposta_oscntcod_N
      {
         get {
            return gxTv_SdtProposta_Proposta_oscntcod_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_oscntcod_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oscntcod_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oscntcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oscntcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSDmn_N" )]
      [  XmlElement( ElementName = "Proposta_OSDmn_N"   )]
      public short gxTpr_Proposta_osdmn_N
      {
         get {
            return gxTv_SdtProposta_Proposta_osdmn_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_osdmn_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osdmn_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osdmn_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osdmn_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSDmnFM_N" )]
      [  XmlElement( ElementName = "Proposta_OSDmnFM_N"   )]
      public short gxTpr_Proposta_osdmnfm_N
      {
         get {
            return gxTv_SdtProposta_Proposta_osdmnfm_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_osdmnfm_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osdmnfm_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osdmnfm_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osdmnfm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSUndCntCod_N" )]
      [  XmlElement( ElementName = "Proposta_OSUndCntCod_N"   )]
      public short gxTpr_Proposta_osundcntcod_N
      {
         get {
            return gxTv_SdtProposta_Proposta_osundcntcod_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_osundcntcod_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osundcntcod_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osundcntcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osundcntcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSUndCntSgl_N" )]
      [  XmlElement( ElementName = "Proposta_OSUndCntSgl_N"   )]
      public short gxTpr_Proposta_osundcntsgl_N
      {
         get {
            return gxTv_SdtProposta_Proposta_osundcntsgl_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_osundcntsgl_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osundcntsgl_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osundcntsgl_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osundcntsgl_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPrzTpDias_N" )]
      [  XmlElement( ElementName = "Proposta_OSPrzTpDias_N"   )]
      public short gxTpr_Proposta_osprztpdias_N
      {
         get {
            return gxTv_SdtProposta_Proposta_osprztpdias_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_osprztpdias_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osprztpdias_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osprztpdias_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osprztpdias_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPrzRsp_N" )]
      [  XmlElement( ElementName = "Proposta_OSPrzRsp_N"   )]
      public short gxTpr_Proposta_osprzrsp_N
      {
         get {
            return gxTv_SdtProposta_Proposta_osprzrsp_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_osprzrsp_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osprzrsp_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osprzrsp_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osprzrsp_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPFB_N" )]
      [  XmlElement( ElementName = "Proposta_OSPFB_N"   )]
      public short gxTpr_Proposta_ospfb_N
      {
         get {
            return gxTv_SdtProposta_Proposta_ospfb_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_ospfb_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_ospfb_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_ospfb_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_ospfb_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSPFL_N" )]
      [  XmlElement( ElementName = "Proposta_OSPFL_N"   )]
      public short gxTpr_Proposta_ospfl_N
      {
         get {
            return gxTv_SdtProposta_Proposta_ospfl_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_ospfl_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_ospfl_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_ospfl_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_ospfl_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSDataCnt_N" )]
      [  XmlElement( ElementName = "Proposta_OSDataCnt_N"   )]
      public short gxTpr_Proposta_osdatacnt_N
      {
         get {
            return gxTv_SdtProposta_Proposta_osdatacnt_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_osdatacnt_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osdatacnt_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osdatacnt_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osdatacnt_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSHoraCnt_N" )]
      [  XmlElement( ElementName = "Proposta_OSHoraCnt_N"   )]
      public short gxTpr_Proposta_oshoracnt_N
      {
         get {
            return gxTv_SdtProposta_Proposta_oshoracnt_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_oshoracnt_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oshoracnt_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oshoracnt_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oshoracnt_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSCntSrvFtrm_N" )]
      [  XmlElement( ElementName = "Proposta_OSCntSrvFtrm_N"   )]
      public short gxTpr_Proposta_oscntsrvftrm_N
      {
         get {
            return gxTv_SdtProposta_Proposta_oscntsrvftrm_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_oscntsrvftrm_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_oscntsrvftrm_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_oscntsrvftrm_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_oscntsrvftrm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Proposta_OSInicio_N" )]
      [  XmlElement( ElementName = "Proposta_OSInicio_N"   )]
      public short gxTpr_Proposta_osinicio_N
      {
         get {
            return gxTv_SdtProposta_Proposta_osinicio_N ;
         }

         set {
            gxTv_SdtProposta_Proposta_osinicio_N = (short)(value);
         }

      }

      public void gxTv_SdtProposta_Proposta_osinicio_N_SetNull( )
      {
         gxTv_SdtProposta_Proposta_osinicio_N = 0;
         return  ;
      }

      public bool gxTv_SdtProposta_Proposta_osinicio_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtProposta_Proposta_vigencia = (DateTime)(DateTime.MinValue);
         gxTv_SdtProposta_Proposta_status = "";
         gxTv_SdtProposta_Proposta_objetivo = "";
         gxTv_SdtProposta_Proposta_escopo = "";
         gxTv_SdtProposta_Proposta_oportunidade = "";
         gxTv_SdtProposta_Proposta_perspectiva = "";
         gxTv_SdtProposta_Proposta_restricaoimplantacao = (DateTime)(DateTime.MinValue);
         gxTv_SdtProposta_Proposta_contratadarazsocial = "";
         gxTv_SdtProposta_Proposta_prazo = DateTime.MinValue;
         gxTv_SdtProposta_Proposta_osdmn = "";
         gxTv_SdtProposta_Proposta_osdmnfm = "";
         gxTv_SdtProposta_Proposta_osundcntsgl = "";
         gxTv_SdtProposta_Proposta_osprztpdias = "";
         gxTv_SdtProposta_Proposta_osdatacnt = DateTime.MinValue;
         gxTv_SdtProposta_Proposta_oshoracnt = "";
         gxTv_SdtProposta_Proposta_oscntsrvftrm = "B";
         gxTv_SdtProposta_Proposta_osinicio = DateTime.MinValue;
         gxTv_SdtProposta_Mode = "";
         gxTv_SdtProposta_Proposta_vigencia_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtProposta_Proposta_status_Z = "";
         gxTv_SdtProposta_Proposta_restricaoimplantacao_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtProposta_Proposta_contratadarazsocial_Z = "";
         gxTv_SdtProposta_Proposta_prazo_Z = DateTime.MinValue;
         gxTv_SdtProposta_Proposta_osdmn_Z = "";
         gxTv_SdtProposta_Proposta_osdmnfm_Z = "";
         gxTv_SdtProposta_Proposta_osundcntsgl_Z = "";
         gxTv_SdtProposta_Proposta_osprztpdias_Z = "";
         gxTv_SdtProposta_Proposta_osdatacnt_Z = DateTime.MinValue;
         gxTv_SdtProposta_Proposta_oshoracnt_Z = "";
         gxTv_SdtProposta_Proposta_oscntsrvftrm_Z = "";
         gxTv_SdtProposta_Proposta_osinicio_Z = DateTime.MinValue;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "proposta", "GeneXus.Programs.proposta_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtProposta_Proposta_prazodias ;
      private short gxTv_SdtProposta_Proposta_osprzrsp ;
      private short gxTv_SdtProposta_Initialized ;
      private short gxTv_SdtProposta_Proposta_prazodias_Z ;
      private short gxTv_SdtProposta_Proposta_osprzrsp_Z ;
      private short gxTv_SdtProposta_Proposta_codigo_N ;
      private short gxTv_SdtProposta_Projeto_codigo_N ;
      private short gxTv_SdtProposta_Proposta_vigencia_N ;
      private short gxTv_SdtProposta_Proposta_status_N ;
      private short gxTv_SdtProposta_Proposta_escopo_N ;
      private short gxTv_SdtProposta_Proposta_oportunidade_N ;
      private short gxTv_SdtProposta_Proposta_perspectiva_N ;
      private short gxTv_SdtProposta_Proposta_restricaoimplantacao_N ;
      private short gxTv_SdtProposta_Proposta_restricaocusto_N ;
      private short gxTv_SdtProposta_Proposta_contratadarazsocial_N ;
      private short gxTv_SdtProposta_Proposta_prazodias_N ;
      private short gxTv_SdtProposta_Proposta_liquido_N ;
      private short gxTv_SdtProposta_Proposta_osservico_N ;
      private short gxTv_SdtProposta_Proposta_oscntcod_N ;
      private short gxTv_SdtProposta_Proposta_osdmn_N ;
      private short gxTv_SdtProposta_Proposta_osdmnfm_N ;
      private short gxTv_SdtProposta_Proposta_osundcntcod_N ;
      private short gxTv_SdtProposta_Proposta_osundcntsgl_N ;
      private short gxTv_SdtProposta_Proposta_osprztpdias_N ;
      private short gxTv_SdtProposta_Proposta_osprzrsp_N ;
      private short gxTv_SdtProposta_Proposta_ospfb_N ;
      private short gxTv_SdtProposta_Proposta_ospfl_N ;
      private short gxTv_SdtProposta_Proposta_osdatacnt_N ;
      private short gxTv_SdtProposta_Proposta_oshoracnt_N ;
      private short gxTv_SdtProposta_Proposta_oscntsrvftrm_N ;
      private short gxTv_SdtProposta_Proposta_osinicio_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtProposta_Proposta_codigo ;
      private int gxTv_SdtProposta_Projeto_codigo ;
      private int gxTv_SdtProposta_Areatrabalho_codigo ;
      private int gxTv_SdtProposta_Proposta_contratadacod ;
      private int gxTv_SdtProposta_Proposta_esforco ;
      private int gxTv_SdtProposta_Proposta_liquido ;
      private int gxTv_SdtProposta_Proposta_cntsrvcod ;
      private int gxTv_SdtProposta_Proposta_oscodigo ;
      private int gxTv_SdtProposta_Proposta_osservico ;
      private int gxTv_SdtProposta_Proposta_oscntcod ;
      private int gxTv_SdtProposta_Proposta_osundcntcod ;
      private int gxTv_SdtProposta_Proposta_codigo_Z ;
      private int gxTv_SdtProposta_Projeto_codigo_Z ;
      private int gxTv_SdtProposta_Areatrabalho_codigo_Z ;
      private int gxTv_SdtProposta_Proposta_contratadacod_Z ;
      private int gxTv_SdtProposta_Proposta_esforco_Z ;
      private int gxTv_SdtProposta_Proposta_liquido_Z ;
      private int gxTv_SdtProposta_Proposta_cntsrvcod_Z ;
      private int gxTv_SdtProposta_Proposta_oscodigo_Z ;
      private int gxTv_SdtProposta_Proposta_osservico_Z ;
      private int gxTv_SdtProposta_Proposta_oscntcod_Z ;
      private int gxTv_SdtProposta_Proposta_osundcntcod_Z ;
      private decimal gxTv_SdtProposta_Proposta_valor ;
      private decimal gxTv_SdtProposta_Proposta_restricaocusto ;
      private decimal gxTv_SdtProposta_Proposta_valorundcnt ;
      private decimal gxTv_SdtProposta_Proposta_ospfb ;
      private decimal gxTv_SdtProposta_Proposta_ospfl ;
      private decimal gxTv_SdtProposta_Proposta_valor_Z ;
      private decimal gxTv_SdtProposta_Proposta_restricaocusto_Z ;
      private decimal gxTv_SdtProposta_Proposta_valorundcnt_Z ;
      private decimal gxTv_SdtProposta_Proposta_ospfb_Z ;
      private decimal gxTv_SdtProposta_Proposta_ospfl_Z ;
      private String gxTv_SdtProposta_Proposta_status ;
      private String gxTv_SdtProposta_Proposta_osundcntsgl ;
      private String gxTv_SdtProposta_Proposta_osprztpdias ;
      private String gxTv_SdtProposta_Proposta_oshoracnt ;
      private String gxTv_SdtProposta_Mode ;
      private String gxTv_SdtProposta_Proposta_status_Z ;
      private String gxTv_SdtProposta_Proposta_osundcntsgl_Z ;
      private String gxTv_SdtProposta_Proposta_osprztpdias_Z ;
      private String gxTv_SdtProposta_Proposta_oshoracnt_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtProposta_Proposta_vigencia ;
      private DateTime gxTv_SdtProposta_Proposta_restricaoimplantacao ;
      private DateTime gxTv_SdtProposta_Proposta_vigencia_Z ;
      private DateTime gxTv_SdtProposta_Proposta_restricaoimplantacao_Z ;
      private DateTime datetime_STZ ;
      private DateTime gxTv_SdtProposta_Proposta_prazo ;
      private DateTime gxTv_SdtProposta_Proposta_osdatacnt ;
      private DateTime gxTv_SdtProposta_Proposta_osinicio ;
      private DateTime gxTv_SdtProposta_Proposta_prazo_Z ;
      private DateTime gxTv_SdtProposta_Proposta_osdatacnt_Z ;
      private DateTime gxTv_SdtProposta_Proposta_osinicio_Z ;
      private bool gxTv_SdtProposta_Proposta_ativo ;
      private bool gxTv_SdtProposta_Proposta_ativo_Z ;
      private String gxTv_SdtProposta_Proposta_objetivo ;
      private String gxTv_SdtProposta_Proposta_escopo ;
      private String gxTv_SdtProposta_Proposta_oportunidade ;
      private String gxTv_SdtProposta_Proposta_perspectiva ;
      private String gxTv_SdtProposta_Proposta_contratadarazsocial ;
      private String gxTv_SdtProposta_Proposta_osdmn ;
      private String gxTv_SdtProposta_Proposta_osdmnfm ;
      private String gxTv_SdtProposta_Proposta_oscntsrvftrm ;
      private String gxTv_SdtProposta_Proposta_contratadarazsocial_Z ;
      private String gxTv_SdtProposta_Proposta_osdmn_Z ;
      private String gxTv_SdtProposta_Proposta_osdmnfm_Z ;
      private String gxTv_SdtProposta_Proposta_oscntsrvftrm_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Proposta", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtProposta_RESTInterface : GxGenericCollectionItem<SdtProposta>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtProposta_RESTInterface( ) : base()
      {
      }

      public SdtProposta_RESTInterface( SdtProposta psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Proposta_Codigo" , Order = 0 )]
      [GxSeudo()]
      public String gxTpr_Proposta_codigo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Proposta_codigo), 9, 0)) ;
         }

         set {
            sdt.gxTpr_Proposta_codigo = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Projeto_Codigo" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_codigo
      {
         get {
            return sdt.gxTpr_Projeto_codigo ;
         }

         set {
            sdt.gxTpr_Projeto_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Proposta_Vigencia" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Proposta_vigencia
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Proposta_vigencia) ;
         }

         set {
            sdt.gxTpr_Proposta_vigencia = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "Proposta_Valor" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Proposta_valor
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Proposta_valor, 15, 4)) ;
         }

         set {
            sdt.gxTpr_Proposta_valor = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Proposta_Status" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Proposta_status
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Proposta_status) ;
         }

         set {
            sdt.gxTpr_Proposta_status = (String)(value);
         }

      }

      [DataMember( Name = "AreaTrabalho_Codigo" , Order = 5 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Areatrabalho_codigo
      {
         get {
            return sdt.gxTpr_Areatrabalho_codigo ;
         }

         set {
            sdt.gxTpr_Areatrabalho_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Proposta_Objetivo" , Order = 6 )]
      public String gxTpr_Proposta_objetivo
      {
         get {
            return sdt.gxTpr_Proposta_objetivo ;
         }

         set {
            sdt.gxTpr_Proposta_objetivo = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_Escopo" , Order = 7 )]
      public String gxTpr_Proposta_escopo
      {
         get {
            return sdt.gxTpr_Proposta_escopo ;
         }

         set {
            sdt.gxTpr_Proposta_escopo = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_Oportunidade" , Order = 8 )]
      public String gxTpr_Proposta_oportunidade
      {
         get {
            return sdt.gxTpr_Proposta_oportunidade ;
         }

         set {
            sdt.gxTpr_Proposta_oportunidade = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_Perspectiva" , Order = 9 )]
      public String gxTpr_Proposta_perspectiva
      {
         get {
            return sdt.gxTpr_Proposta_perspectiva ;
         }

         set {
            sdt.gxTpr_Proposta_perspectiva = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_RestricaoImplantacao" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Proposta_restricaoimplantacao
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Proposta_restricaoimplantacao) ;
         }

         set {
            sdt.gxTpr_Proposta_restricaoimplantacao = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "Proposta_RestricaoCusto" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Proposta_restricaocusto
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Proposta_restricaocusto, 15, 4)) ;
         }

         set {
            sdt.gxTpr_Proposta_restricaocusto = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Proposta_Ativo" , Order = 12 )]
      [GxSeudo()]
      public bool gxTpr_Proposta_ativo
      {
         get {
            return sdt.gxTpr_Proposta_ativo ;
         }

         set {
            sdt.gxTpr_Proposta_ativo = value;
         }

      }

      [DataMember( Name = "Proposta_ContratadaCod" , Order = 13 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Proposta_contratadacod
      {
         get {
            return sdt.gxTpr_Proposta_contratadacod ;
         }

         set {
            sdt.gxTpr_Proposta_contratadacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Proposta_ContratadaRazSocial" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Proposta_contratadarazsocial
      {
         get {
            return sdt.gxTpr_Proposta_contratadarazsocial ;
         }

         set {
            sdt.gxTpr_Proposta_contratadarazsocial = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_Prazo" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Proposta_prazo
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Proposta_prazo) ;
         }

         set {
            sdt.gxTpr_Proposta_prazo = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Proposta_PrazoDias" , Order = 16 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Proposta_prazodias
      {
         get {
            return sdt.gxTpr_Proposta_prazodias ;
         }

         set {
            sdt.gxTpr_Proposta_prazodias = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Proposta_Esforco" , Order = 17 )]
      [GxSeudo()]
      public String gxTpr_Proposta_esforco
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Proposta_esforco), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Proposta_esforco = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Proposta_Liquido" , Order = 18 )]
      [GxSeudo()]
      public String gxTpr_Proposta_liquido
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Proposta_liquido), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Proposta_liquido = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "Proposta_CntSrvCod" , Order = 19 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Proposta_cntsrvcod
      {
         get {
            return sdt.gxTpr_Proposta_cntsrvcod ;
         }

         set {
            sdt.gxTpr_Proposta_cntsrvcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Proposta_ValorUndCnt" , Order = 20 )]
      [GxSeudo()]
      public String gxTpr_Proposta_valorundcnt
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Proposta_valorundcnt, 10, 2)) ;
         }

         set {
            sdt.gxTpr_Proposta_valorundcnt = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Proposta_OSCodigo" , Order = 21 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Proposta_oscodigo
      {
         get {
            return sdt.gxTpr_Proposta_oscodigo ;
         }

         set {
            sdt.gxTpr_Proposta_oscodigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Proposta_OSServico" , Order = 22 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Proposta_osservico
      {
         get {
            return sdt.gxTpr_Proposta_osservico ;
         }

         set {
            sdt.gxTpr_Proposta_osservico = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Proposta_OSCntCod" , Order = 23 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Proposta_oscntcod
      {
         get {
            return sdt.gxTpr_Proposta_oscntcod ;
         }

         set {
            sdt.gxTpr_Proposta_oscntcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Proposta_OSDmn" , Order = 24 )]
      [GxSeudo()]
      public String gxTpr_Proposta_osdmn
      {
         get {
            return sdt.gxTpr_Proposta_osdmn ;
         }

         set {
            sdt.gxTpr_Proposta_osdmn = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_OSDmnFM" , Order = 25 )]
      [GxSeudo()]
      public String gxTpr_Proposta_osdmnfm
      {
         get {
            return sdt.gxTpr_Proposta_osdmnfm ;
         }

         set {
            sdt.gxTpr_Proposta_osdmnfm = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_OSUndCntCod" , Order = 26 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Proposta_osundcntcod
      {
         get {
            return sdt.gxTpr_Proposta_osundcntcod ;
         }

         set {
            sdt.gxTpr_Proposta_osundcntcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Proposta_OSUndCntSgl" , Order = 27 )]
      [GxSeudo()]
      public String gxTpr_Proposta_osundcntsgl
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Proposta_osundcntsgl) ;
         }

         set {
            sdt.gxTpr_Proposta_osundcntsgl = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_OSPrzTpDias" , Order = 28 )]
      [GxSeudo()]
      public String gxTpr_Proposta_osprztpdias
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Proposta_osprztpdias) ;
         }

         set {
            sdt.gxTpr_Proposta_osprztpdias = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_OSPrzRsp" , Order = 29 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Proposta_osprzrsp
      {
         get {
            return sdt.gxTpr_Proposta_osprzrsp ;
         }

         set {
            sdt.gxTpr_Proposta_osprzrsp = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Proposta_OSPFB" , Order = 30 )]
      [GxSeudo()]
      public String gxTpr_Proposta_ospfb
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Proposta_ospfb, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Proposta_ospfb = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Proposta_OSPFL" , Order = 31 )]
      [GxSeudo()]
      public String gxTpr_Proposta_ospfl
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Proposta_ospfl, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Proposta_ospfl = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Proposta_OSDataCnt" , Order = 32 )]
      [GxSeudo()]
      public String gxTpr_Proposta_osdatacnt
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Proposta_osdatacnt) ;
         }

         set {
            sdt.gxTpr_Proposta_osdatacnt = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Proposta_OSHoraCnt" , Order = 33 )]
      [GxSeudo()]
      public String gxTpr_Proposta_oshoracnt
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Proposta_oshoracnt) ;
         }

         set {
            sdt.gxTpr_Proposta_oshoracnt = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_OSCntSrvFtrm" , Order = 34 )]
      [GxSeudo()]
      public String gxTpr_Proposta_oscntsrvftrm
      {
         get {
            return sdt.gxTpr_Proposta_oscntsrvftrm ;
         }

         set {
            sdt.gxTpr_Proposta_oscntsrvftrm = (String)(value);
         }

      }

      [DataMember( Name = "Proposta_OSInicio" , Order = 35 )]
      [GxSeudo()]
      public String gxTpr_Proposta_osinicio
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Proposta_osinicio) ;
         }

         set {
            sdt.gxTpr_Proposta_osinicio = DateTimeUtil.CToD2( (String)(value));
         }

      }

      public SdtProposta sdt
      {
         get {
            return (SdtProposta)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtProposta() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 96 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
