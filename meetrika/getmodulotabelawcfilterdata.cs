/*
               File: GetModuloTabelaWCFilterData
        Description: Get Modulo Tabela WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:48.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getmodulotabelawcfilterdata : GXProcedure
   {
      public getmodulotabelawcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getmodulotabelawcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV19DDOName = aP0_DDOName;
         this.AV17SearchTxt = aP1_SearchTxt;
         this.AV18SearchTxtTo = aP2_SearchTxtTo;
         this.AV23OptionsJson = "" ;
         this.AV26OptionsDescJson = "" ;
         this.AV28OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
         return AV28OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getmodulotabelawcfilterdata objgetmodulotabelawcfilterdata;
         objgetmodulotabelawcfilterdata = new getmodulotabelawcfilterdata();
         objgetmodulotabelawcfilterdata.AV19DDOName = aP0_DDOName;
         objgetmodulotabelawcfilterdata.AV17SearchTxt = aP1_SearchTxt;
         objgetmodulotabelawcfilterdata.AV18SearchTxtTo = aP2_SearchTxtTo;
         objgetmodulotabelawcfilterdata.AV23OptionsJson = "" ;
         objgetmodulotabelawcfilterdata.AV26OptionsDescJson = "" ;
         objgetmodulotabelawcfilterdata.AV28OptionIndexesJson = "" ;
         objgetmodulotabelawcfilterdata.context.SetSubmitInitialConfig(context);
         objgetmodulotabelawcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetmodulotabelawcfilterdata);
         aP3_OptionsJson=this.AV23OptionsJson;
         aP4_OptionsDescJson=this.AV26OptionsDescJson;
         aP5_OptionIndexesJson=this.AV28OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getmodulotabelawcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV22Options = (IGxCollection)(new GxSimpleCollection());
         AV25OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV27OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_TABELA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTABELA_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_TABELA_SISTEMADES") == 0 )
         {
            /* Execute user subroutine: 'LOADTABELA_SISTEMADESOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV19DDOName), "DDO_TABELA_PAINOM") == 0 )
         {
            /* Execute user subroutine: 'LOADTABELA_PAINOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV23OptionsJson = AV22Options.ToJSonString(false);
         AV26OptionsDescJson = AV25OptionsDesc.ToJSonString(false);
         AV28OptionIndexesJson = AV27OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get("ModuloTabelaWCGridState"), "") == 0 )
         {
            AV32GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ModuloTabelaWCGridState"), "");
         }
         else
         {
            AV32GridState.FromXml(AV30Session.Get("ModuloTabelaWCGridState"), "");
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV32GridState.gxTpr_Filtervalues.Count )
         {
            AV33GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV32GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFTABELA_NOME") == 0 )
            {
               AV10TFTabela_Nome = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFTABELA_NOME_SEL") == 0 )
            {
               AV11TFTabela_Nome_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFTABELA_SISTEMADES") == 0 )
            {
               AV12TFTabela_SistemaDes = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFTABELA_SISTEMADES_SEL") == 0 )
            {
               AV13TFTabela_SistemaDes_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFTABELA_PAINOM") == 0 )
            {
               AV14TFTabela_PaiNom = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFTABELA_PAINOM_SEL") == 0 )
            {
               AV15TFTabela_PaiNom_Sel = AV33GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "TFTABELA_ATIVO_SEL") == 0 )
            {
               AV16TFTabela_Ativo_Sel = (short)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV33GridStateFilterValue.gxTpr_Name, "PARM_&TABELA_MODULOCOD") == 0 )
            {
               AV38Tabela_ModuloCod = (int)(NumberUtil.Val( AV33GridStateFilterValue.gxTpr_Value, "."));
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
         if ( AV32GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV34GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV32GridState.gxTpr_Dynamicfilters.Item(1));
            AV35DynamicFiltersSelector1 = AV34GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "TABELA_NOME") == 0 )
            {
               AV36DynamicFiltersOperator1 = AV34GridStateDynamicFilter.gxTpr_Operator;
               AV37Tabela_Nome1 = AV34GridStateDynamicFilter.gxTpr_Value;
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADTABELA_NOMEOPTIONS' Routine */
         AV10TFTabela_Nome = AV17SearchTxt;
         AV11TFTabela_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36DynamicFiltersOperator1 ,
                                              AV37Tabela_Nome1 ,
                                              AV11TFTabela_Nome_Sel ,
                                              AV10TFTabela_Nome ,
                                              AV13TFTabela_SistemaDes_Sel ,
                                              AV12TFTabela_SistemaDes ,
                                              AV15TFTabela_PaiNom_Sel ,
                                              AV14TFTabela_PaiNom ,
                                              AV16TFTabela_Ativo_Sel ,
                                              A173Tabela_Nome ,
                                              A191Tabela_SistemaDes ,
                                              A182Tabela_PaiNom ,
                                              A174Tabela_Ativo ,
                                              AV38Tabela_ModuloCod ,
                                              A188Tabela_ModuloCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV37Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Tabela_Nome1), 50, "%");
         lV37Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Tabela_Nome1), 50, "%");
         lV10TFTabela_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFTabela_Nome), 50, "%");
         lV12TFTabela_SistemaDes = StringUtil.Concat( StringUtil.RTrim( AV12TFTabela_SistemaDes), "%", "");
         lV14TFTabela_PaiNom = StringUtil.PadR( StringUtil.RTrim( AV14TFTabela_PaiNom), 50, "%");
         /* Using cursor P00US2 */
         pr_default.execute(0, new Object[] {AV38Tabela_ModuloCod, lV37Tabela_Nome1, lV37Tabela_Nome1, lV10TFTabela_Nome, AV11TFTabela_Nome_Sel, lV12TFTabela_SistemaDes, AV13TFTabela_SistemaDes_Sel, lV14TFTabela_PaiNom, AV15TFTabela_PaiNom_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKUS2 = false;
            A190Tabela_SistemaCod = P00US2_A190Tabela_SistemaCod[0];
            A181Tabela_PaiCod = P00US2_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = P00US2_n181Tabela_PaiCod[0];
            A188Tabela_ModuloCod = P00US2_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = P00US2_n188Tabela_ModuloCod[0];
            A173Tabela_Nome = P00US2_A173Tabela_Nome[0];
            A174Tabela_Ativo = P00US2_A174Tabela_Ativo[0];
            A182Tabela_PaiNom = P00US2_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00US2_n182Tabela_PaiNom[0];
            A191Tabela_SistemaDes = P00US2_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00US2_n191Tabela_SistemaDes[0];
            A172Tabela_Codigo = P00US2_A172Tabela_Codigo[0];
            A191Tabela_SistemaDes = P00US2_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00US2_n191Tabela_SistemaDes[0];
            A182Tabela_PaiNom = P00US2_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00US2_n182Tabela_PaiNom[0];
            AV29count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00US2_A188Tabela_ModuloCod[0] == A188Tabela_ModuloCod ) && ( StringUtil.StrCmp(P00US2_A173Tabela_Nome[0], A173Tabela_Nome) == 0 ) )
            {
               BRKUS2 = false;
               A172Tabela_Codigo = P00US2_A172Tabela_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKUS2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A173Tabela_Nome)) )
            {
               AV21Option = A173Tabela_Nome;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUS2 )
            {
               BRKUS2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADTABELA_SISTEMADESOPTIONS' Routine */
         AV12TFTabela_SistemaDes = AV17SearchTxt;
         AV13TFTabela_SistemaDes_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36DynamicFiltersOperator1 ,
                                              AV37Tabela_Nome1 ,
                                              AV11TFTabela_Nome_Sel ,
                                              AV10TFTabela_Nome ,
                                              AV13TFTabela_SistemaDes_Sel ,
                                              AV12TFTabela_SistemaDes ,
                                              AV15TFTabela_PaiNom_Sel ,
                                              AV14TFTabela_PaiNom ,
                                              AV16TFTabela_Ativo_Sel ,
                                              A173Tabela_Nome ,
                                              A191Tabela_SistemaDes ,
                                              A182Tabela_PaiNom ,
                                              A174Tabela_Ativo ,
                                              AV38Tabela_ModuloCod ,
                                              A188Tabela_ModuloCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV37Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Tabela_Nome1), 50, "%");
         lV37Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Tabela_Nome1), 50, "%");
         lV10TFTabela_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFTabela_Nome), 50, "%");
         lV12TFTabela_SistemaDes = StringUtil.Concat( StringUtil.RTrim( AV12TFTabela_SistemaDes), "%", "");
         lV14TFTabela_PaiNom = StringUtil.PadR( StringUtil.RTrim( AV14TFTabela_PaiNom), 50, "%");
         /* Using cursor P00US3 */
         pr_default.execute(1, new Object[] {AV38Tabela_ModuloCod, lV37Tabela_Nome1, lV37Tabela_Nome1, lV10TFTabela_Nome, AV11TFTabela_Nome_Sel, lV12TFTabela_SistemaDes, AV13TFTabela_SistemaDes_Sel, lV14TFTabela_PaiNom, AV15TFTabela_PaiNom_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKUS4 = false;
            A181Tabela_PaiCod = P00US3_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = P00US3_n181Tabela_PaiCod[0];
            A188Tabela_ModuloCod = P00US3_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = P00US3_n188Tabela_ModuloCod[0];
            A190Tabela_SistemaCod = P00US3_A190Tabela_SistemaCod[0];
            A174Tabela_Ativo = P00US3_A174Tabela_Ativo[0];
            A182Tabela_PaiNom = P00US3_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00US3_n182Tabela_PaiNom[0];
            A191Tabela_SistemaDes = P00US3_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00US3_n191Tabela_SistemaDes[0];
            A173Tabela_Nome = P00US3_A173Tabela_Nome[0];
            A172Tabela_Codigo = P00US3_A172Tabela_Codigo[0];
            A182Tabela_PaiNom = P00US3_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00US3_n182Tabela_PaiNom[0];
            A191Tabela_SistemaDes = P00US3_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00US3_n191Tabela_SistemaDes[0];
            AV29count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00US3_A188Tabela_ModuloCod[0] == A188Tabela_ModuloCod ) && ( P00US3_A190Tabela_SistemaCod[0] == A190Tabela_SistemaCod ) )
            {
               BRKUS4 = false;
               A172Tabela_Codigo = P00US3_A172Tabela_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKUS4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A191Tabela_SistemaDes)) )
            {
               AV21Option = A191Tabela_SistemaDes;
               AV20InsertIndex = 1;
               while ( ( AV20InsertIndex <= AV22Options.Count ) && ( StringUtil.StrCmp(((String)AV22Options.Item(AV20InsertIndex)), AV21Option) < 0 ) )
               {
                  AV20InsertIndex = (int)(AV20InsertIndex+1);
               }
               AV22Options.Add(AV21Option, AV20InsertIndex);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), AV20InsertIndex);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUS4 )
            {
               BRKUS4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADTABELA_PAINOMOPTIONS' Routine */
         AV14TFTabela_PaiNom = AV17SearchTxt;
         AV15TFTabela_PaiNom_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36DynamicFiltersOperator1 ,
                                              AV37Tabela_Nome1 ,
                                              AV11TFTabela_Nome_Sel ,
                                              AV10TFTabela_Nome ,
                                              AV13TFTabela_SistemaDes_Sel ,
                                              AV12TFTabela_SistemaDes ,
                                              AV15TFTabela_PaiNom_Sel ,
                                              AV14TFTabela_PaiNom ,
                                              AV16TFTabela_Ativo_Sel ,
                                              A173Tabela_Nome ,
                                              A191Tabela_SistemaDes ,
                                              A182Tabela_PaiNom ,
                                              A174Tabela_Ativo ,
                                              AV38Tabela_ModuloCod ,
                                              A188Tabela_ModuloCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV37Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Tabela_Nome1), 50, "%");
         lV37Tabela_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV37Tabela_Nome1), 50, "%");
         lV10TFTabela_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFTabela_Nome), 50, "%");
         lV12TFTabela_SistemaDes = StringUtil.Concat( StringUtil.RTrim( AV12TFTabela_SistemaDes), "%", "");
         lV14TFTabela_PaiNom = StringUtil.PadR( StringUtil.RTrim( AV14TFTabela_PaiNom), 50, "%");
         /* Using cursor P00US4 */
         pr_default.execute(2, new Object[] {AV38Tabela_ModuloCod, lV37Tabela_Nome1, lV37Tabela_Nome1, lV10TFTabela_Nome, AV11TFTabela_Nome_Sel, lV12TFTabela_SistemaDes, AV13TFTabela_SistemaDes_Sel, lV14TFTabela_PaiNom, AV15TFTabela_PaiNom_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKUS6 = false;
            A190Tabela_SistemaCod = P00US4_A190Tabela_SistemaCod[0];
            A181Tabela_PaiCod = P00US4_A181Tabela_PaiCod[0];
            n181Tabela_PaiCod = P00US4_n181Tabela_PaiCod[0];
            A188Tabela_ModuloCod = P00US4_A188Tabela_ModuloCod[0];
            n188Tabela_ModuloCod = P00US4_n188Tabela_ModuloCod[0];
            A182Tabela_PaiNom = P00US4_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00US4_n182Tabela_PaiNom[0];
            A174Tabela_Ativo = P00US4_A174Tabela_Ativo[0];
            A191Tabela_SistemaDes = P00US4_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00US4_n191Tabela_SistemaDes[0];
            A173Tabela_Nome = P00US4_A173Tabela_Nome[0];
            A172Tabela_Codigo = P00US4_A172Tabela_Codigo[0];
            A191Tabela_SistemaDes = P00US4_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = P00US4_n191Tabela_SistemaDes[0];
            A182Tabela_PaiNom = P00US4_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = P00US4_n182Tabela_PaiNom[0];
            AV29count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00US4_A188Tabela_ModuloCod[0] == A188Tabela_ModuloCod ) && ( StringUtil.StrCmp(P00US4_A182Tabela_PaiNom[0], A182Tabela_PaiNom) == 0 ) )
            {
               BRKUS6 = false;
               A181Tabela_PaiCod = P00US4_A181Tabela_PaiCod[0];
               n181Tabela_PaiCod = P00US4_n181Tabela_PaiCod[0];
               A172Tabela_Codigo = P00US4_A172Tabela_Codigo[0];
               AV29count = (long)(AV29count+1);
               BRKUS6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A182Tabela_PaiNom)) )
            {
               AV21Option = A182Tabela_PaiNom;
               AV22Options.Add(AV21Option, 0);
               AV27OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV29count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV22Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKUS6 )
            {
               BRKUS6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV22Options = new GxSimpleCollection();
         AV25OptionsDesc = new GxSimpleCollection();
         AV27OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV30Session = context.GetSession();
         AV32GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV33GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFTabela_Nome = "";
         AV11TFTabela_Nome_Sel = "";
         AV12TFTabela_SistemaDes = "";
         AV13TFTabela_SistemaDes_Sel = "";
         AV14TFTabela_PaiNom = "";
         AV15TFTabela_PaiNom_Sel = "";
         AV34GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV35DynamicFiltersSelector1 = "";
         AV37Tabela_Nome1 = "";
         scmdbuf = "";
         lV37Tabela_Nome1 = "";
         lV10TFTabela_Nome = "";
         lV12TFTabela_SistemaDes = "";
         lV14TFTabela_PaiNom = "";
         A173Tabela_Nome = "";
         A191Tabela_SistemaDes = "";
         A182Tabela_PaiNom = "";
         P00US2_A190Tabela_SistemaCod = new int[1] ;
         P00US2_A181Tabela_PaiCod = new int[1] ;
         P00US2_n181Tabela_PaiCod = new bool[] {false} ;
         P00US2_A188Tabela_ModuloCod = new int[1] ;
         P00US2_n188Tabela_ModuloCod = new bool[] {false} ;
         P00US2_A173Tabela_Nome = new String[] {""} ;
         P00US2_A174Tabela_Ativo = new bool[] {false} ;
         P00US2_A182Tabela_PaiNom = new String[] {""} ;
         P00US2_n182Tabela_PaiNom = new bool[] {false} ;
         P00US2_A191Tabela_SistemaDes = new String[] {""} ;
         P00US2_n191Tabela_SistemaDes = new bool[] {false} ;
         P00US2_A172Tabela_Codigo = new int[1] ;
         AV21Option = "";
         P00US3_A181Tabela_PaiCod = new int[1] ;
         P00US3_n181Tabela_PaiCod = new bool[] {false} ;
         P00US3_A188Tabela_ModuloCod = new int[1] ;
         P00US3_n188Tabela_ModuloCod = new bool[] {false} ;
         P00US3_A190Tabela_SistemaCod = new int[1] ;
         P00US3_A174Tabela_Ativo = new bool[] {false} ;
         P00US3_A182Tabela_PaiNom = new String[] {""} ;
         P00US3_n182Tabela_PaiNom = new bool[] {false} ;
         P00US3_A191Tabela_SistemaDes = new String[] {""} ;
         P00US3_n191Tabela_SistemaDes = new bool[] {false} ;
         P00US3_A173Tabela_Nome = new String[] {""} ;
         P00US3_A172Tabela_Codigo = new int[1] ;
         P00US4_A190Tabela_SistemaCod = new int[1] ;
         P00US4_A181Tabela_PaiCod = new int[1] ;
         P00US4_n181Tabela_PaiCod = new bool[] {false} ;
         P00US4_A188Tabela_ModuloCod = new int[1] ;
         P00US4_n188Tabela_ModuloCod = new bool[] {false} ;
         P00US4_A182Tabela_PaiNom = new String[] {""} ;
         P00US4_n182Tabela_PaiNom = new bool[] {false} ;
         P00US4_A174Tabela_Ativo = new bool[] {false} ;
         P00US4_A191Tabela_SistemaDes = new String[] {""} ;
         P00US4_n191Tabela_SistemaDes = new bool[] {false} ;
         P00US4_A173Tabela_Nome = new String[] {""} ;
         P00US4_A172Tabela_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getmodulotabelawcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00US2_A190Tabela_SistemaCod, P00US2_A181Tabela_PaiCod, P00US2_n181Tabela_PaiCod, P00US2_A188Tabela_ModuloCod, P00US2_n188Tabela_ModuloCod, P00US2_A173Tabela_Nome, P00US2_A174Tabela_Ativo, P00US2_A182Tabela_PaiNom, P00US2_n182Tabela_PaiNom, P00US2_A191Tabela_SistemaDes,
               P00US2_n191Tabela_SistemaDes, P00US2_A172Tabela_Codigo
               }
               , new Object[] {
               P00US3_A181Tabela_PaiCod, P00US3_n181Tabela_PaiCod, P00US3_A188Tabela_ModuloCod, P00US3_n188Tabela_ModuloCod, P00US3_A190Tabela_SistemaCod, P00US3_A174Tabela_Ativo, P00US3_A182Tabela_PaiNom, P00US3_n182Tabela_PaiNom, P00US3_A191Tabela_SistemaDes, P00US3_n191Tabela_SistemaDes,
               P00US3_A173Tabela_Nome, P00US3_A172Tabela_Codigo
               }
               , new Object[] {
               P00US4_A190Tabela_SistemaCod, P00US4_A181Tabela_PaiCod, P00US4_n181Tabela_PaiCod, P00US4_A188Tabela_ModuloCod, P00US4_n188Tabela_ModuloCod, P00US4_A182Tabela_PaiNom, P00US4_n182Tabela_PaiNom, P00US4_A174Tabela_Ativo, P00US4_A191Tabela_SistemaDes, P00US4_n191Tabela_SistemaDes,
               P00US4_A173Tabela_Nome, P00US4_A172Tabela_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFTabela_Ativo_Sel ;
      private short AV36DynamicFiltersOperator1 ;
      private int AV41GXV1 ;
      private int AV38Tabela_ModuloCod ;
      private int A188Tabela_ModuloCod ;
      private int A190Tabela_SistemaCod ;
      private int A181Tabela_PaiCod ;
      private int A172Tabela_Codigo ;
      private int AV20InsertIndex ;
      private long AV29count ;
      private String AV10TFTabela_Nome ;
      private String AV11TFTabela_Nome_Sel ;
      private String AV14TFTabela_PaiNom ;
      private String AV15TFTabela_PaiNom_Sel ;
      private String AV37Tabela_Nome1 ;
      private String scmdbuf ;
      private String lV37Tabela_Nome1 ;
      private String lV10TFTabela_Nome ;
      private String lV14TFTabela_PaiNom ;
      private String A173Tabela_Nome ;
      private String A182Tabela_PaiNom ;
      private bool returnInSub ;
      private bool A174Tabela_Ativo ;
      private bool BRKUS2 ;
      private bool n181Tabela_PaiCod ;
      private bool n188Tabela_ModuloCod ;
      private bool n182Tabela_PaiNom ;
      private bool n191Tabela_SistemaDes ;
      private bool BRKUS4 ;
      private bool BRKUS6 ;
      private String AV28OptionIndexesJson ;
      private String AV23OptionsJson ;
      private String AV26OptionsDescJson ;
      private String AV19DDOName ;
      private String AV17SearchTxt ;
      private String AV18SearchTxtTo ;
      private String AV12TFTabela_SistemaDes ;
      private String AV13TFTabela_SistemaDes_Sel ;
      private String AV35DynamicFiltersSelector1 ;
      private String lV12TFTabela_SistemaDes ;
      private String A191Tabela_SistemaDes ;
      private String AV21Option ;
      private IGxSession AV30Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00US2_A190Tabela_SistemaCod ;
      private int[] P00US2_A181Tabela_PaiCod ;
      private bool[] P00US2_n181Tabela_PaiCod ;
      private int[] P00US2_A188Tabela_ModuloCod ;
      private bool[] P00US2_n188Tabela_ModuloCod ;
      private String[] P00US2_A173Tabela_Nome ;
      private bool[] P00US2_A174Tabela_Ativo ;
      private String[] P00US2_A182Tabela_PaiNom ;
      private bool[] P00US2_n182Tabela_PaiNom ;
      private String[] P00US2_A191Tabela_SistemaDes ;
      private bool[] P00US2_n191Tabela_SistemaDes ;
      private int[] P00US2_A172Tabela_Codigo ;
      private int[] P00US3_A181Tabela_PaiCod ;
      private bool[] P00US3_n181Tabela_PaiCod ;
      private int[] P00US3_A188Tabela_ModuloCod ;
      private bool[] P00US3_n188Tabela_ModuloCod ;
      private int[] P00US3_A190Tabela_SistemaCod ;
      private bool[] P00US3_A174Tabela_Ativo ;
      private String[] P00US3_A182Tabela_PaiNom ;
      private bool[] P00US3_n182Tabela_PaiNom ;
      private String[] P00US3_A191Tabela_SistemaDes ;
      private bool[] P00US3_n191Tabela_SistemaDes ;
      private String[] P00US3_A173Tabela_Nome ;
      private int[] P00US3_A172Tabela_Codigo ;
      private int[] P00US4_A190Tabela_SistemaCod ;
      private int[] P00US4_A181Tabela_PaiCod ;
      private bool[] P00US4_n181Tabela_PaiCod ;
      private int[] P00US4_A188Tabela_ModuloCod ;
      private bool[] P00US4_n188Tabela_ModuloCod ;
      private String[] P00US4_A182Tabela_PaiNom ;
      private bool[] P00US4_n182Tabela_PaiNom ;
      private bool[] P00US4_A174Tabela_Ativo ;
      private String[] P00US4_A191Tabela_SistemaDes ;
      private bool[] P00US4_n191Tabela_SistemaDes ;
      private String[] P00US4_A173Tabela_Nome ;
      private int[] P00US4_A172Tabela_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV32GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV33GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV34GridStateDynamicFilter ;
   }

   public class getmodulotabelawcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00US2( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             short AV36DynamicFiltersOperator1 ,
                                             String AV37Tabela_Nome1 ,
                                             String AV11TFTabela_Nome_Sel ,
                                             String AV10TFTabela_Nome ,
                                             String AV13TFTabela_SistemaDes_Sel ,
                                             String AV12TFTabela_SistemaDes ,
                                             String AV15TFTabela_PaiNom_Sel ,
                                             String AV14TFTabela_PaiNom ,
                                             short AV16TFTabela_Ativo_Sel ,
                                             String A173Tabela_Nome ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             bool A174Tabela_Ativo ,
                                             int AV38Tabela_ModuloCod ,
                                             int A188Tabela_ModuloCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [9] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_PaiCod] AS Tabela_PaiCod, T1.[Tabela_ModuloCod], T1.[Tabela_Nome], T1.[Tabela_Ativo], T3.[Tabela_Nome] AS Tabela_PaiNom, T2.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_Codigo] FROM (([Tabela] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Tabela_SistemaCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T1.[Tabela_PaiCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Tabela_ModuloCod] = @AV38Tabela_ModuloCod)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV36DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV37Tabela_Nome1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV36DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV37Tabela_Nome1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTabela_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFTabela_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV10TFTabela_Nome)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTabela_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV11TFTabela_Nome_Sel)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFTabela_SistemaDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFTabela_SistemaDes)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV12TFTabela_SistemaDes)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFTabela_SistemaDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] = @AV13TFTabela_SistemaDes_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFTabela_PaiNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFTabela_PaiNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV14TFTabela_PaiNom)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFTabela_PaiNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV15TFTabela_PaiNom_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV16TFTabela_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
         }
         if ( AV16TFTabela_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Tabela_ModuloCod], T1.[Tabela_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00US3( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             short AV36DynamicFiltersOperator1 ,
                                             String AV37Tabela_Nome1 ,
                                             String AV11TFTabela_Nome_Sel ,
                                             String AV10TFTabela_Nome ,
                                             String AV13TFTabela_SistemaDes_Sel ,
                                             String AV12TFTabela_SistemaDes ,
                                             String AV15TFTabela_PaiNom_Sel ,
                                             String AV14TFTabela_PaiNom ,
                                             short AV16TFTabela_Ativo_Sel ,
                                             String A173Tabela_Nome ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             bool A174Tabela_Ativo ,
                                             int AV38Tabela_ModuloCod ,
                                             int A188Tabela_ModuloCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [9] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Tabela_PaiCod] AS Tabela_PaiCod, T1.[Tabela_ModuloCod], T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_Ativo], T2.[Tabela_Nome] AS Tabela_PaiNom, T3.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_Nome], T1.[Tabela_Codigo] FROM (([Tabela] T1 WITH (NOLOCK) LEFT JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Tabela_PaiCod]) INNER JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T1.[Tabela_SistemaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Tabela_ModuloCod] = @AV38Tabela_ModuloCod)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV36DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV37Tabela_Nome1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV36DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV37Tabela_Nome1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTabela_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFTabela_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV10TFTabela_Nome)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTabela_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV11TFTabela_Nome_Sel)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFTabela_SistemaDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFTabela_SistemaDes)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] like @lV12TFTabela_SistemaDes)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFTabela_SistemaDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Sistema_Nome] = @AV13TFTabela_SistemaDes_Sel)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFTabela_PaiNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFTabela_PaiNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Tabela_Nome] like @lV14TFTabela_PaiNom)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFTabela_PaiNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Tabela_Nome] = @AV15TFTabela_PaiNom_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV16TFTabela_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
         }
         if ( AV16TFTabela_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Tabela_ModuloCod], T1.[Tabela_SistemaCod]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00US4( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             short AV36DynamicFiltersOperator1 ,
                                             String AV37Tabela_Nome1 ,
                                             String AV11TFTabela_Nome_Sel ,
                                             String AV10TFTabela_Nome ,
                                             String AV13TFTabela_SistemaDes_Sel ,
                                             String AV12TFTabela_SistemaDes ,
                                             String AV15TFTabela_PaiNom_Sel ,
                                             String AV14TFTabela_PaiNom ,
                                             short AV16TFTabela_Ativo_Sel ,
                                             String A173Tabela_Nome ,
                                             String A191Tabela_SistemaDes ,
                                             String A182Tabela_PaiNom ,
                                             bool A174Tabela_Ativo ,
                                             int AV38Tabela_ModuloCod ,
                                             int A188Tabela_ModuloCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [9] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Tabela_SistemaCod] AS Tabela_SistemaCod, T1.[Tabela_PaiCod] AS Tabela_PaiCod, T1.[Tabela_ModuloCod], T3.[Tabela_Nome] AS Tabela_PaiNom, T1.[Tabela_Ativo], T2.[Sistema_Nome] AS Tabela_SistemaDes, T1.[Tabela_Nome], T1.[Tabela_Codigo] FROM (([Tabela] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[Tabela_SistemaCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T1.[Tabela_PaiCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Tabela_ModuloCod] = @AV38Tabela_ModuloCod)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV36DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV37Tabela_Nome1)";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "TABELA_NOME") == 0 ) && ( AV36DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Tabela_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like '%' + @lV37Tabela_Nome1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTabela_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFTabela_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] like @lV10TFTabela_Nome)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFTabela_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Nome] = @AV11TFTabela_Nome_Sel)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFTabela_SistemaDes_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFTabela_SistemaDes)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV12TFTabela_SistemaDes)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFTabela_SistemaDes_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Sistema_Nome] = @AV13TFTabela_SistemaDes_Sel)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFTabela_PaiNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFTabela_PaiNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV14TFTabela_PaiNom)";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFTabela_PaiNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV15TFTabela_PaiNom_Sel)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV16TFTabela_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 1)";
         }
         if ( AV16TFTabela_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Tabela_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Tabela_ModuloCod], T3.[Tabela_Nome]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00US2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
               case 1 :
                     return conditional_P00US3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
               case 2 :
                     return conditional_P00US4(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (bool)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00US2 ;
          prmP00US2 = new Object[] {
          new Object[] {"@AV38Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV37Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFTabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFTabela_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFTabela_SistemaDes",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV13TFTabela_SistemaDes_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV14TFTabela_PaiNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFTabela_PaiNom_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00US3 ;
          prmP00US3 = new Object[] {
          new Object[] {"@AV38Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV37Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFTabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFTabela_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFTabela_SistemaDes",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV13TFTabela_SistemaDes_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV14TFTabela_PaiNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFTabela_PaiNom_Sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00US4 ;
          prmP00US4 = new Object[] {
          new Object[] {"@AV38Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV37Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37Tabela_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFTabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFTabela_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFTabela_SistemaDes",SqlDbType.VarChar,100,0} ,
          new Object[] {"@AV13TFTabela_SistemaDes_Sel",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV14TFTabela_PaiNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFTabela_PaiNom_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00US2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00US2,100,0,true,false )
             ,new CursorDef("P00US3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00US3,100,0,true,false )
             ,new CursorDef("P00US4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00US4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.getBool(4) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((bool[]) buf[7])[0] = rslt.getBool(5) ;
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getmodulotabelawcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getmodulotabelawcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getmodulotabelawcfilterdata") )
          {
             return  ;
          }
          getmodulotabelawcfilterdata worker = new getmodulotabelawcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
