/*
               File: PRC_SolicitacaoPadrao
        Description: Solicitacao Padrao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:48.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_solicitacaopadrao : GXProcedure
   {
      public prc_solicitacaopadrao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_solicitacaopadrao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratante_Codigo ,
                           out int aP1_Contratante_ServicoSSPadrao )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV8Contratante_ServicoSSPadrao = 0 ;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_Contratante_ServicoSSPadrao=this.AV8Contratante_ServicoSSPadrao;
      }

      public int executeUdp( ref int aP0_Contratante_Codigo )
      {
         this.A29Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV8Contratante_ServicoSSPadrao = 0 ;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_Contratante_ServicoSSPadrao=this.AV8Contratante_ServicoSSPadrao;
         return AV8Contratante_ServicoSSPadrao ;
      }

      public void executeSubmit( ref int aP0_Contratante_Codigo ,
                                 out int aP1_Contratante_ServicoSSPadrao )
      {
         prc_solicitacaopadrao objprc_solicitacaopadrao;
         objprc_solicitacaopadrao = new prc_solicitacaopadrao();
         objprc_solicitacaopadrao.A29Contratante_Codigo = aP0_Contratante_Codigo;
         objprc_solicitacaopadrao.AV8Contratante_ServicoSSPadrao = 0 ;
         objprc_solicitacaopadrao.context.SetSubmitInitialConfig(context);
         objprc_solicitacaopadrao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_solicitacaopadrao);
         aP0_Contratante_Codigo=this.A29Contratante_Codigo;
         aP1_Contratante_ServicoSSPadrao=this.AV8Contratante_ServicoSSPadrao;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_solicitacaopadrao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00CD2 */
         pr_default.execute(0, new Object[] {A29Contratante_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1727Contratante_ServicoSSPadrao = P00CD2_A1727Contratante_ServicoSSPadrao[0];
            n1727Contratante_ServicoSSPadrao = P00CD2_n1727Contratante_ServicoSSPadrao[0];
            AV8Contratante_ServicoSSPadrao = A1727Contratante_ServicoSSPadrao;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CD2_A29Contratante_Codigo = new int[1] ;
         P00CD2_A1727Contratante_ServicoSSPadrao = new int[1] ;
         P00CD2_n1727Contratante_ServicoSSPadrao = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_solicitacaopadrao__default(),
            new Object[][] {
                new Object[] {
               P00CD2_A29Contratante_Codigo, P00CD2_A1727Contratante_ServicoSSPadrao, P00CD2_n1727Contratante_ServicoSSPadrao
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A29Contratante_Codigo ;
      private int AV8Contratante_ServicoSSPadrao ;
      private int A1727Contratante_ServicoSSPadrao ;
      private String scmdbuf ;
      private bool n1727Contratante_ServicoSSPadrao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratante_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00CD2_A29Contratante_Codigo ;
      private int[] P00CD2_A1727Contratante_ServicoSSPadrao ;
      private bool[] P00CD2_n1727Contratante_ServicoSSPadrao ;
      private int aP1_Contratante_ServicoSSPadrao ;
   }

   public class prc_solicitacaopadrao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CD2 ;
          prmP00CD2 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CD2", "SELECT TOP 1 [Contratante_Codigo], [Contratante_ServicoSSPadrao] FROM [Contratante] WITH (NOLOCK) WHERE [Contratante_Codigo] = @Contratante_Codigo ORDER BY [Contratante_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CD2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
