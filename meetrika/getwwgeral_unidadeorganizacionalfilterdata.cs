/*
               File: GetWWGeral_UnidadeOrganizacionalFilterData
        Description: Get WWGeral_Unidade Organizacional Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:15.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwgeral_unidadeorganizacionalfilterdata : GXProcedure
   {
      public getwwgeral_unidadeorganizacionalfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwgeral_unidadeorganizacionalfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV21DDOName = aP0_DDOName;
         this.AV19SearchTxt = aP1_SearchTxt;
         this.AV20SearchTxtTo = aP2_SearchTxtTo;
         this.AV25OptionsJson = "" ;
         this.AV28OptionsDescJson = "" ;
         this.AV30OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV21DDOName = aP0_DDOName;
         this.AV19SearchTxt = aP1_SearchTxt;
         this.AV20SearchTxtTo = aP2_SearchTxtTo;
         this.AV25OptionsJson = "" ;
         this.AV28OptionsDescJson = "" ;
         this.AV30OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
         return AV30OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwgeral_unidadeorganizacionalfilterdata objgetwwgeral_unidadeorganizacionalfilterdata;
         objgetwwgeral_unidadeorganizacionalfilterdata = new getwwgeral_unidadeorganizacionalfilterdata();
         objgetwwgeral_unidadeorganizacionalfilterdata.AV21DDOName = aP0_DDOName;
         objgetwwgeral_unidadeorganizacionalfilterdata.AV19SearchTxt = aP1_SearchTxt;
         objgetwwgeral_unidadeorganizacionalfilterdata.AV20SearchTxtTo = aP2_SearchTxtTo;
         objgetwwgeral_unidadeorganizacionalfilterdata.AV25OptionsJson = "" ;
         objgetwwgeral_unidadeorganizacionalfilterdata.AV28OptionsDescJson = "" ;
         objgetwwgeral_unidadeorganizacionalfilterdata.AV30OptionIndexesJson = "" ;
         objgetwwgeral_unidadeorganizacionalfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwgeral_unidadeorganizacionalfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwgeral_unidadeorganizacionalfilterdata);
         aP3_OptionsJson=this.AV25OptionsJson;
         aP4_OptionsDescJson=this.AV28OptionsDescJson;
         aP5_OptionIndexesJson=this.AV30OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwgeral_unidadeorganizacionalfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV24Options = (IGxCollection)(new GxSimpleCollection());
         AV27OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV29OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUNIDADEORGANIZACIONAL_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_TPUO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTPUO_NOMEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_UNIDADEORGANIZACINAL_ARVORE") == 0 )
         {
            /* Execute user subroutine: 'LOADUNIDADEORGANIZACINAL_ARVOREOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV21DDOName), "DDO_ESTADO_UF") == 0 )
         {
            /* Execute user subroutine: 'LOADESTADO_UFOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV25OptionsJson = AV24Options.ToJSonString(false);
         AV28OptionsDescJson = AV27OptionsDesc.ToJSonString(false);
         AV30OptionIndexesJson = AV29OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV32Session.Get("WWGeral_UnidadeOrganizacionalGridState"), "") == 0 )
         {
            AV34GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWGeral_UnidadeOrganizacionalGridState"), "");
         }
         else
         {
            AV34GridState.FromXml(AV32Session.Get("WWGeral_UnidadeOrganizacionalGridState"), "");
         }
         AV50GXV1 = 1;
         while ( AV50GXV1 <= AV34GridState.gxTpr_Filtervalues.Count )
         {
            AV35GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV34GridState.gxTpr_Filtervalues.Item(AV50GXV1));
            if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV10TFUnidadeOrganizacional_Nome = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME_SEL") == 0 )
            {
               AV11TFUnidadeOrganizacional_Nome_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFTPUO_NOME") == 0 )
            {
               AV12TFTpUo_Nome = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFTPUO_NOME_SEL") == 0 )
            {
               AV13TFTpUo_Nome_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACINAL_ARVORE") == 0 )
            {
               AV14TFUnidadeOrganizacinal_Arvore = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACINAL_ARVORE_SEL") == 0 )
            {
               AV15TFUnidadeOrganizacinal_Arvore_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFESTADO_UF") == 0 )
            {
               AV16TFEstado_UF = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFESTADO_UF_SEL") == 0 )
            {
               AV17TFEstado_UF_Sel = AV35GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_ATIVO_SEL") == 0 )
            {
               AV18TFUnidadeOrganizacional_Ativo_Sel = (short)(NumberUtil.Val( AV35GridStateFilterValue.gxTpr_Value, "."));
            }
            AV50GXV1 = (int)(AV50GXV1+1);
         }
         if ( AV34GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV36GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV34GridState.gxTpr_Dynamicfilters.Item(1));
            AV37DynamicFiltersSelector1 = AV36GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV38UnidadeOrganizacional_Nome1 = AV36GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
            {
               AV39UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( AV36GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV34GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV40DynamicFiltersEnabled2 = true;
               AV36GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV34GridState.gxTpr_Dynamicfilters.Item(2));
               AV41DynamicFiltersSelector2 = AV36GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
               {
                  AV42UnidadeOrganizacional_Nome2 = AV36GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
               {
                  AV43UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( AV36GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV34GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV44DynamicFiltersEnabled3 = true;
                  AV36GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV34GridState.gxTpr_Dynamicfilters.Item(3));
                  AV45DynamicFiltersSelector3 = AV36GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
                  {
                     AV46UnidadeOrganizacional_Nome3 = AV36GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
                  {
                     AV47UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( AV36GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUNIDADEORGANIZACIONAL_NOMEOPTIONS' Routine */
         AV10TFUnidadeOrganizacional_Nome = AV19SearchTxt;
         AV11TFUnidadeOrganizacional_Nome_Sel = "";
         AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = AV37DynamicFiltersSelector1;
         AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = AV38UnidadeOrganizacional_Nome1;
         AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 = AV39UnidadeOrganizacional_Vinculada1;
         AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = AV42UnidadeOrganizacional_Nome2;
         AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 = AV43UnidadeOrganizacional_Vinculada2;
         AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 = AV44DynamicFiltersEnabled3;
         AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = AV45DynamicFiltersSelector3;
         AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = AV46UnidadeOrganizacional_Nome3;
         AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 = AV47UnidadeOrganizacional_Vinculada3;
         AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = AV10TFUnidadeOrganizacional_Nome;
         AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = AV11TFUnidadeOrganizacional_Nome_Sel;
         AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = AV12TFTpUo_Nome;
         AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = AV13TFTpUo_Nome_Sel;
         AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = AV14TFUnidadeOrganizacinal_Arvore;
         AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = AV15TFUnidadeOrganizacinal_Arvore_Sel;
         AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = AV16TFEstado_UF;
         AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = AV17TFEstado_UF_Sel;
         AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel = AV18TFUnidadeOrganizacional_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ,
                                              AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ,
                                              AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ,
                                              AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ,
                                              AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ,
                                              AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ,
                                              AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ,
                                              AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ,
                                              AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ,
                                              AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ,
                                              AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ,
                                              AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ,
                                              AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ,
                                              AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ,
                                              AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ,
                                              AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ,
                                              AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ,
                                              AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              A610TpUo_Nome ,
                                              A23Estado_UF ,
                                              A629UnidadeOrganizacional_Ativo ,
                                              AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ,
                                              AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ,
                                              A1174UnidadeOrganizacinal_Arvore },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = StringUtil.PadR( StringUtil.RTrim( AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1), 50, "%");
         lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = StringUtil.PadR( StringUtil.RTrim( AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2), 50, "%");
         lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = StringUtil.PadR( StringUtil.RTrim( AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3), 50, "%");
         lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = StringUtil.PadR( StringUtil.RTrim( AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome), 50, "%");
         lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = StringUtil.PadR( StringUtil.RTrim( AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome), 50, "%");
         lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf), 2, "%");
         /* Using cursor P00NN2 */
         pr_default.execute(0, new Object[] {lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1, AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1, lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2, AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2, lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3, AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3, lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome, AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel, lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome, AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel, lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf, AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKNN2 = false;
            A609TpUo_Codigo = P00NN2_A609TpUo_Codigo[0];
            A612UnidadeOrganizacional_Nome = P00NN2_A612UnidadeOrganizacional_Nome[0];
            A629UnidadeOrganizacional_Ativo = P00NN2_A629UnidadeOrganizacional_Ativo[0];
            A23Estado_UF = P00NN2_A23Estado_UF[0];
            A610TpUo_Nome = P00NN2_A610TpUo_Nome[0];
            A613UnidadeOrganizacional_Vinculada = P00NN2_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P00NN2_n613UnidadeOrganizacional_Vinculada[0];
            A611UnidadeOrganizacional_Codigo = P00NN2_A611UnidadeOrganizacional_Codigo[0];
            A610TpUo_Nome = P00NN2_A610TpUo_Nome[0];
            if ( ! P00NN2_n613UnidadeOrganizacional_Vinculada[0] )
            {
               GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
               new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
               A1174UnidadeOrganizacinal_Arvore = GXt_char1;
            }
            else
            {
               A1174UnidadeOrganizacinal_Arvore = "";
            }
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore)) ) ) || ( StringUtil.Like( A1174UnidadeOrganizacinal_Arvore , StringUtil.PadR( AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore , 1000 , "%"),  ' ' ) ) )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel)) || ( ( StringUtil.StrCmp(A1174UnidadeOrganizacinal_Arvore, AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel) == 0 ) ) )
               {
                  AV31count = 0;
                  while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00NN2_A612UnidadeOrganizacional_Nome[0], A612UnidadeOrganizacional_Nome) == 0 ) )
                  {
                     BRKNN2 = false;
                     A611UnidadeOrganizacional_Codigo = P00NN2_A611UnidadeOrganizacional_Codigo[0];
                     AV31count = (long)(AV31count+1);
                     BRKNN2 = true;
                     pr_default.readNext(0);
                  }
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A612UnidadeOrganizacional_Nome)) )
                  {
                     AV23Option = A612UnidadeOrganizacional_Nome;
                     AV24Options.Add(AV23Option, 0);
                     AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                  }
                  if ( AV24Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            if ( ! BRKNN2 )
            {
               BRKNN2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADTPUO_NOMEOPTIONS' Routine */
         AV12TFTpUo_Nome = AV19SearchTxt;
         AV13TFTpUo_Nome_Sel = "";
         AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = AV37DynamicFiltersSelector1;
         AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = AV38UnidadeOrganizacional_Nome1;
         AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 = AV39UnidadeOrganizacional_Vinculada1;
         AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = AV42UnidadeOrganizacional_Nome2;
         AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 = AV43UnidadeOrganizacional_Vinculada2;
         AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 = AV44DynamicFiltersEnabled3;
         AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = AV45DynamicFiltersSelector3;
         AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = AV46UnidadeOrganizacional_Nome3;
         AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 = AV47UnidadeOrganizacional_Vinculada3;
         AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = AV10TFUnidadeOrganizacional_Nome;
         AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = AV11TFUnidadeOrganizacional_Nome_Sel;
         AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = AV12TFTpUo_Nome;
         AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = AV13TFTpUo_Nome_Sel;
         AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = AV14TFUnidadeOrganizacinal_Arvore;
         AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = AV15TFUnidadeOrganizacinal_Arvore_Sel;
         AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = AV16TFEstado_UF;
         AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = AV17TFEstado_UF_Sel;
         AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel = AV18TFUnidadeOrganizacional_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ,
                                              AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ,
                                              AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ,
                                              AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ,
                                              AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ,
                                              AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ,
                                              AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ,
                                              AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ,
                                              AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ,
                                              AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ,
                                              AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ,
                                              AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ,
                                              AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ,
                                              AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ,
                                              AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ,
                                              AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ,
                                              AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ,
                                              AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              A610TpUo_Nome ,
                                              A23Estado_UF ,
                                              A629UnidadeOrganizacional_Ativo ,
                                              AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ,
                                              AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ,
                                              A1174UnidadeOrganizacinal_Arvore },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = StringUtil.PadR( StringUtil.RTrim( AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1), 50, "%");
         lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = StringUtil.PadR( StringUtil.RTrim( AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2), 50, "%");
         lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = StringUtil.PadR( StringUtil.RTrim( AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3), 50, "%");
         lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = StringUtil.PadR( StringUtil.RTrim( AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome), 50, "%");
         lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = StringUtil.PadR( StringUtil.RTrim( AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome), 50, "%");
         lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf), 2, "%");
         /* Using cursor P00NN3 */
         pr_default.execute(1, new Object[] {lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1, AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1, lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2, AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2, lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3, AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3, lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome, AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel, lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome, AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel, lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf, AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKNN4 = false;
            A609TpUo_Codigo = P00NN3_A609TpUo_Codigo[0];
            A629UnidadeOrganizacional_Ativo = P00NN3_A629UnidadeOrganizacional_Ativo[0];
            A23Estado_UF = P00NN3_A23Estado_UF[0];
            A610TpUo_Nome = P00NN3_A610TpUo_Nome[0];
            A612UnidadeOrganizacional_Nome = P00NN3_A612UnidadeOrganizacional_Nome[0];
            A613UnidadeOrganizacional_Vinculada = P00NN3_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P00NN3_n613UnidadeOrganizacional_Vinculada[0];
            A611UnidadeOrganizacional_Codigo = P00NN3_A611UnidadeOrganizacional_Codigo[0];
            A610TpUo_Nome = P00NN3_A610TpUo_Nome[0];
            if ( ! P00NN3_n613UnidadeOrganizacional_Vinculada[0] )
            {
               GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
               new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
               A1174UnidadeOrganizacinal_Arvore = GXt_char1;
            }
            else
            {
               A1174UnidadeOrganizacinal_Arvore = "";
            }
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore)) ) ) || ( StringUtil.Like( A1174UnidadeOrganizacinal_Arvore , StringUtil.PadR( AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore , 1000 , "%"),  ' ' ) ) )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel)) || ( ( StringUtil.StrCmp(A1174UnidadeOrganizacinal_Arvore, AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel) == 0 ) ) )
               {
                  AV31count = 0;
                  while ( (pr_default.getStatus(1) != 101) && ( P00NN3_A609TpUo_Codigo[0] == A609TpUo_Codigo ) )
                  {
                     BRKNN4 = false;
                     A611UnidadeOrganizacional_Codigo = P00NN3_A611UnidadeOrganizacional_Codigo[0];
                     AV31count = (long)(AV31count+1);
                     BRKNN4 = true;
                     pr_default.readNext(1);
                  }
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A610TpUo_Nome)) )
                  {
                     AV23Option = A610TpUo_Nome;
                     AV22InsertIndex = 1;
                     while ( ( AV22InsertIndex <= AV24Options.Count ) && ( StringUtil.StrCmp(((String)AV24Options.Item(AV22InsertIndex)), AV23Option) < 0 ) )
                     {
                        AV22InsertIndex = (int)(AV22InsertIndex+1);
                     }
                     AV24Options.Add(AV23Option, AV22InsertIndex);
                     AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), AV22InsertIndex);
                  }
                  if ( AV24Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            if ( ! BRKNN4 )
            {
               BRKNN4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADUNIDADEORGANIZACINAL_ARVOREOPTIONS' Routine */
         AV14TFUnidadeOrganizacinal_Arvore = AV19SearchTxt;
         AV15TFUnidadeOrganizacinal_Arvore_Sel = "";
         AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = AV37DynamicFiltersSelector1;
         AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = AV38UnidadeOrganizacional_Nome1;
         AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 = AV39UnidadeOrganizacional_Vinculada1;
         AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = AV42UnidadeOrganizacional_Nome2;
         AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 = AV43UnidadeOrganizacional_Vinculada2;
         AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 = AV44DynamicFiltersEnabled3;
         AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = AV45DynamicFiltersSelector3;
         AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = AV46UnidadeOrganizacional_Nome3;
         AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 = AV47UnidadeOrganizacional_Vinculada3;
         AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = AV10TFUnidadeOrganizacional_Nome;
         AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = AV11TFUnidadeOrganizacional_Nome_Sel;
         AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = AV12TFTpUo_Nome;
         AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = AV13TFTpUo_Nome_Sel;
         AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = AV14TFUnidadeOrganizacinal_Arvore;
         AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = AV15TFUnidadeOrganizacinal_Arvore_Sel;
         AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = AV16TFEstado_UF;
         AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = AV17TFEstado_UF_Sel;
         AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel = AV18TFUnidadeOrganizacional_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ,
                                              AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ,
                                              AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ,
                                              AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ,
                                              AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ,
                                              AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ,
                                              AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ,
                                              AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ,
                                              AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ,
                                              AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ,
                                              AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ,
                                              AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ,
                                              AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ,
                                              AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ,
                                              AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ,
                                              AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ,
                                              AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ,
                                              AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              A610TpUo_Nome ,
                                              A23Estado_UF ,
                                              A629UnidadeOrganizacional_Ativo ,
                                              AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ,
                                              AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ,
                                              A1174UnidadeOrganizacinal_Arvore },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = StringUtil.PadR( StringUtil.RTrim( AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1), 50, "%");
         lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = StringUtil.PadR( StringUtil.RTrim( AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2), 50, "%");
         lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = StringUtil.PadR( StringUtil.RTrim( AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3), 50, "%");
         lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = StringUtil.PadR( StringUtil.RTrim( AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome), 50, "%");
         lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = StringUtil.PadR( StringUtil.RTrim( AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome), 50, "%");
         lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf), 2, "%");
         /* Using cursor P00NN4 */
         pr_default.execute(2, new Object[] {lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1, AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1, lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2, AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2, lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3, AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3, lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome, AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel, lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome, AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel, lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf, AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A609TpUo_Codigo = P00NN4_A609TpUo_Codigo[0];
            A629UnidadeOrganizacional_Ativo = P00NN4_A629UnidadeOrganizacional_Ativo[0];
            A23Estado_UF = P00NN4_A23Estado_UF[0];
            A610TpUo_Nome = P00NN4_A610TpUo_Nome[0];
            A612UnidadeOrganizacional_Nome = P00NN4_A612UnidadeOrganizacional_Nome[0];
            A613UnidadeOrganizacional_Vinculada = P00NN4_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P00NN4_n613UnidadeOrganizacional_Vinculada[0];
            A611UnidadeOrganizacional_Codigo = P00NN4_A611UnidadeOrganizacional_Codigo[0];
            A610TpUo_Nome = P00NN4_A610TpUo_Nome[0];
            if ( ! P00NN4_n613UnidadeOrganizacional_Vinculada[0] )
            {
               GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
               new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
               A1174UnidadeOrganizacinal_Arvore = GXt_char1;
            }
            else
            {
               A1174UnidadeOrganizacinal_Arvore = "";
            }
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore)) ) ) || ( StringUtil.Like( A1174UnidadeOrganizacinal_Arvore , StringUtil.PadR( AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore , 1000 , "%"),  ' ' ) ) )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel)) || ( ( StringUtil.StrCmp(A1174UnidadeOrganizacinal_Arvore, AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel) == 0 ) ) )
               {
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1174UnidadeOrganizacinal_Arvore)) )
                  {
                     AV23Option = A1174UnidadeOrganizacinal_Arvore;
                     AV22InsertIndex = 1;
                     while ( ( AV22InsertIndex <= AV24Options.Count ) && ( StringUtil.StrCmp(((String)AV24Options.Item(AV22InsertIndex)), AV23Option) < 0 ) )
                     {
                        AV22InsertIndex = (int)(AV22InsertIndex+1);
                     }
                     if ( ( AV22InsertIndex <= AV24Options.Count ) && ( StringUtil.StrCmp(((String)AV24Options.Item(AV22InsertIndex)), AV23Option) == 0 ) )
                     {
                        AV31count = (long)(NumberUtil.Val( ((String)AV29OptionIndexes.Item(AV22InsertIndex)), "."));
                        AV31count = (long)(AV31count+1);
                        AV29OptionIndexes.RemoveItem(AV22InsertIndex);
                        AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), AV22InsertIndex);
                     }
                     else
                     {
                        AV24Options.Add(AV23Option, AV22InsertIndex);
                        AV29OptionIndexes.Add("1", AV22InsertIndex);
                     }
                  }
                  if ( AV24Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADESTADO_UFOPTIONS' Routine */
         AV16TFEstado_UF = AV19SearchTxt;
         AV17TFEstado_UF_Sel = "";
         AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = AV37DynamicFiltersSelector1;
         AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = AV38UnidadeOrganizacional_Nome1;
         AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 = AV39UnidadeOrganizacional_Vinculada1;
         AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = AV42UnidadeOrganizacional_Nome2;
         AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 = AV43UnidadeOrganizacional_Vinculada2;
         AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 = AV44DynamicFiltersEnabled3;
         AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = AV45DynamicFiltersSelector3;
         AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = AV46UnidadeOrganizacional_Nome3;
         AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 = AV47UnidadeOrganizacional_Vinculada3;
         AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = AV10TFUnidadeOrganizacional_Nome;
         AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = AV11TFUnidadeOrganizacional_Nome_Sel;
         AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = AV12TFTpUo_Nome;
         AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = AV13TFTpUo_Nome_Sel;
         AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = AV14TFUnidadeOrganizacinal_Arvore;
         AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = AV15TFUnidadeOrganizacinal_Arvore_Sel;
         AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = AV16TFEstado_UF;
         AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = AV17TFEstado_UF_Sel;
         AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel = AV18TFUnidadeOrganizacional_Ativo_Sel;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ,
                                              AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ,
                                              AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ,
                                              AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ,
                                              AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ,
                                              AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ,
                                              AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ,
                                              AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ,
                                              AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ,
                                              AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ,
                                              AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ,
                                              AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ,
                                              AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ,
                                              AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ,
                                              AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ,
                                              AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ,
                                              AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ,
                                              AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              A610TpUo_Nome ,
                                              A23Estado_UF ,
                                              A629UnidadeOrganizacional_Ativo ,
                                              AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ,
                                              AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ,
                                              A1174UnidadeOrganizacinal_Arvore },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = StringUtil.PadR( StringUtil.RTrim( AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1), 50, "%");
         lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = StringUtil.PadR( StringUtil.RTrim( AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2), 50, "%");
         lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = StringUtil.PadR( StringUtil.RTrim( AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3), 50, "%");
         lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = StringUtil.PadR( StringUtil.RTrim( AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome), 50, "%");
         lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = StringUtil.PadR( StringUtil.RTrim( AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome), 50, "%");
         lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf), 2, "%");
         /* Using cursor P00NN5 */
         pr_default.execute(3, new Object[] {lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1, AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1, lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2, AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2, lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3, AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3, lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome, AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel, lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome, AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel, lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf, AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKNN7 = false;
            A609TpUo_Codigo = P00NN5_A609TpUo_Codigo[0];
            A23Estado_UF = P00NN5_A23Estado_UF[0];
            A629UnidadeOrganizacional_Ativo = P00NN5_A629UnidadeOrganizacional_Ativo[0];
            A610TpUo_Nome = P00NN5_A610TpUo_Nome[0];
            A612UnidadeOrganizacional_Nome = P00NN5_A612UnidadeOrganizacional_Nome[0];
            A613UnidadeOrganizacional_Vinculada = P00NN5_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P00NN5_n613UnidadeOrganizacional_Vinculada[0];
            A611UnidadeOrganizacional_Codigo = P00NN5_A611UnidadeOrganizacional_Codigo[0];
            A610TpUo_Nome = P00NN5_A610TpUo_Nome[0];
            if ( ! P00NN5_n613UnidadeOrganizacional_Vinculada[0] )
            {
               GXt_char1 = A1174UnidadeOrganizacinal_Arvore;
               new prc_uoarvoredependencias(context ).execute(  A613UnidadeOrganizacional_Vinculada, out  GXt_char1) ;
               A1174UnidadeOrganizacinal_Arvore = GXt_char1;
            }
            else
            {
               A1174UnidadeOrganizacinal_Arvore = "";
            }
            if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore)) ) ) || ( StringUtil.Like( A1174UnidadeOrganizacinal_Arvore , StringUtil.PadR( AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore , 1000 , "%"),  ' ' ) ) )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel)) || ( ( StringUtil.StrCmp(A1174UnidadeOrganizacinal_Arvore, AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel) == 0 ) ) )
               {
                  AV31count = 0;
                  while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00NN5_A23Estado_UF[0], A23Estado_UF) == 0 ) )
                  {
                     BRKNN7 = false;
                     A611UnidadeOrganizacional_Codigo = P00NN5_A611UnidadeOrganizacional_Codigo[0];
                     AV31count = (long)(AV31count+1);
                     BRKNN7 = true;
                     pr_default.readNext(3);
                  }
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) )
                  {
                     AV23Option = A23Estado_UF;
                     AV24Options.Add(AV23Option, 0);
                     AV29OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV31count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                  }
                  if ( AV24Options.Count == 50 )
                  {
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                  }
               }
            }
            if ( ! BRKNN7 )
            {
               BRKNN7 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV24Options = new GxSimpleCollection();
         AV27OptionsDesc = new GxSimpleCollection();
         AV29OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV32Session = context.GetSession();
         AV34GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV35GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFUnidadeOrganizacional_Nome = "";
         AV11TFUnidadeOrganizacional_Nome_Sel = "";
         AV12TFTpUo_Nome = "";
         AV13TFTpUo_Nome_Sel = "";
         AV14TFUnidadeOrganizacinal_Arvore = "";
         AV15TFUnidadeOrganizacinal_Arvore_Sel = "";
         AV16TFEstado_UF = "";
         AV17TFEstado_UF_Sel = "";
         AV36GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV37DynamicFiltersSelector1 = "";
         AV38UnidadeOrganizacional_Nome1 = "";
         AV41DynamicFiltersSelector2 = "";
         AV42UnidadeOrganizacional_Nome2 = "";
         AV45DynamicFiltersSelector3 = "";
         AV46UnidadeOrganizacional_Nome3 = "";
         AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 = "";
         AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = "";
         AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 = "";
         AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = "";
         AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 = "";
         AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = "";
         AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = "";
         AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel = "";
         AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = "";
         AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel = "";
         AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore = "";
         AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel = "";
         AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = "";
         AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel = "";
         scmdbuf = "";
         lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 = "";
         lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 = "";
         lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 = "";
         lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome = "";
         lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome = "";
         lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf = "";
         A612UnidadeOrganizacional_Nome = "";
         A610TpUo_Nome = "";
         A23Estado_UF = "";
         A1174UnidadeOrganizacinal_Arvore = "";
         P00NN2_A609TpUo_Codigo = new int[1] ;
         P00NN2_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P00NN2_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P00NN2_A23Estado_UF = new String[] {""} ;
         P00NN2_A610TpUo_Nome = new String[] {""} ;
         P00NN2_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P00NN2_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P00NN2_A611UnidadeOrganizacional_Codigo = new int[1] ;
         AV23Option = "";
         P00NN3_A609TpUo_Codigo = new int[1] ;
         P00NN3_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P00NN3_A23Estado_UF = new String[] {""} ;
         P00NN3_A610TpUo_Nome = new String[] {""} ;
         P00NN3_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P00NN3_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P00NN3_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P00NN3_A611UnidadeOrganizacional_Codigo = new int[1] ;
         P00NN4_A609TpUo_Codigo = new int[1] ;
         P00NN4_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P00NN4_A23Estado_UF = new String[] {""} ;
         P00NN4_A610TpUo_Nome = new String[] {""} ;
         P00NN4_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P00NN4_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P00NN4_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P00NN4_A611UnidadeOrganizacional_Codigo = new int[1] ;
         P00NN5_A609TpUo_Codigo = new int[1] ;
         P00NN5_A23Estado_UF = new String[] {""} ;
         P00NN5_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P00NN5_A610TpUo_Nome = new String[] {""} ;
         P00NN5_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P00NN5_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P00NN5_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P00NN5_A611UnidadeOrganizacional_Codigo = new int[1] ;
         GXt_char1 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwgeral_unidadeorganizacionalfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00NN2_A609TpUo_Codigo, P00NN2_A612UnidadeOrganizacional_Nome, P00NN2_A629UnidadeOrganizacional_Ativo, P00NN2_A23Estado_UF, P00NN2_A610TpUo_Nome, P00NN2_A613UnidadeOrganizacional_Vinculada, P00NN2_n613UnidadeOrganizacional_Vinculada, P00NN2_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               P00NN3_A609TpUo_Codigo, P00NN3_A629UnidadeOrganizacional_Ativo, P00NN3_A23Estado_UF, P00NN3_A610TpUo_Nome, P00NN3_A612UnidadeOrganizacional_Nome, P00NN3_A613UnidadeOrganizacional_Vinculada, P00NN3_n613UnidadeOrganizacional_Vinculada, P00NN3_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               P00NN4_A609TpUo_Codigo, P00NN4_A629UnidadeOrganizacional_Ativo, P00NN4_A23Estado_UF, P00NN4_A610TpUo_Nome, P00NN4_A612UnidadeOrganizacional_Nome, P00NN4_A613UnidadeOrganizacional_Vinculada, P00NN4_n613UnidadeOrganizacional_Vinculada, P00NN4_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               P00NN5_A609TpUo_Codigo, P00NN5_A23Estado_UF, P00NN5_A629UnidadeOrganizacional_Ativo, P00NN5_A610TpUo_Nome, P00NN5_A612UnidadeOrganizacional_Nome, P00NN5_A613UnidadeOrganizacional_Vinculada, P00NN5_n613UnidadeOrganizacional_Vinculada, P00NN5_A611UnidadeOrganizacional_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFUnidadeOrganizacional_Ativo_Sel ;
      private short AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ;
      private int AV50GXV1 ;
      private int AV39UnidadeOrganizacional_Vinculada1 ;
      private int AV43UnidadeOrganizacional_Vinculada2 ;
      private int AV47UnidadeOrganizacional_Vinculada3 ;
      private int AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ;
      private int AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ;
      private int AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ;
      private int A613UnidadeOrganizacional_Vinculada ;
      private int A609TpUo_Codigo ;
      private int A611UnidadeOrganizacional_Codigo ;
      private int AV22InsertIndex ;
      private long AV31count ;
      private String AV10TFUnidadeOrganizacional_Nome ;
      private String AV11TFUnidadeOrganizacional_Nome_Sel ;
      private String AV12TFTpUo_Nome ;
      private String AV13TFTpUo_Nome_Sel ;
      private String AV14TFUnidadeOrganizacinal_Arvore ;
      private String AV15TFUnidadeOrganizacinal_Arvore_Sel ;
      private String AV16TFEstado_UF ;
      private String AV17TFEstado_UF_Sel ;
      private String AV38UnidadeOrganizacional_Nome1 ;
      private String AV42UnidadeOrganizacional_Nome2 ;
      private String AV46UnidadeOrganizacional_Nome3 ;
      private String AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ;
      private String AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ;
      private String AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ;
      private String AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ;
      private String AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ;
      private String AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ;
      private String AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ;
      private String AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ;
      private String AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ;
      private String AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ;
      private String AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ;
      private String scmdbuf ;
      private String lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ;
      private String lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ;
      private String lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ;
      private String lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ;
      private String lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ;
      private String lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ;
      private String A612UnidadeOrganizacional_Nome ;
      private String A610TpUo_Nome ;
      private String A23Estado_UF ;
      private String A1174UnidadeOrganizacinal_Arvore ;
      private String GXt_char1 ;
      private bool returnInSub ;
      private bool AV40DynamicFiltersEnabled2 ;
      private bool AV44DynamicFiltersEnabled3 ;
      private bool AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ;
      private bool AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ;
      private bool A629UnidadeOrganizacional_Ativo ;
      private bool BRKNN2 ;
      private bool n613UnidadeOrganizacional_Vinculada ;
      private bool BRKNN4 ;
      private bool BRKNN7 ;
      private String AV30OptionIndexesJson ;
      private String AV25OptionsJson ;
      private String AV28OptionsDescJson ;
      private String AV21DDOName ;
      private String AV19SearchTxt ;
      private String AV20SearchTxtTo ;
      private String AV37DynamicFiltersSelector1 ;
      private String AV41DynamicFiltersSelector2 ;
      private String AV45DynamicFiltersSelector3 ;
      private String AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ;
      private String AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ;
      private String AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ;
      private String AV23Option ;
      private IGxSession AV32Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00NN2_A609TpUo_Codigo ;
      private String[] P00NN2_A612UnidadeOrganizacional_Nome ;
      private bool[] P00NN2_A629UnidadeOrganizacional_Ativo ;
      private String[] P00NN2_A23Estado_UF ;
      private String[] P00NN2_A610TpUo_Nome ;
      private int[] P00NN2_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NN2_n613UnidadeOrganizacional_Vinculada ;
      private int[] P00NN2_A611UnidadeOrganizacional_Codigo ;
      private int[] P00NN3_A609TpUo_Codigo ;
      private bool[] P00NN3_A629UnidadeOrganizacional_Ativo ;
      private String[] P00NN3_A23Estado_UF ;
      private String[] P00NN3_A610TpUo_Nome ;
      private String[] P00NN3_A612UnidadeOrganizacional_Nome ;
      private int[] P00NN3_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NN3_n613UnidadeOrganizacional_Vinculada ;
      private int[] P00NN3_A611UnidadeOrganizacional_Codigo ;
      private int[] P00NN4_A609TpUo_Codigo ;
      private bool[] P00NN4_A629UnidadeOrganizacional_Ativo ;
      private String[] P00NN4_A23Estado_UF ;
      private String[] P00NN4_A610TpUo_Nome ;
      private String[] P00NN4_A612UnidadeOrganizacional_Nome ;
      private int[] P00NN4_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NN4_n613UnidadeOrganizacional_Vinculada ;
      private int[] P00NN4_A611UnidadeOrganizacional_Codigo ;
      private int[] P00NN5_A609TpUo_Codigo ;
      private String[] P00NN5_A23Estado_UF ;
      private bool[] P00NN5_A629UnidadeOrganizacional_Ativo ;
      private String[] P00NN5_A610TpUo_Nome ;
      private String[] P00NN5_A612UnidadeOrganizacional_Nome ;
      private int[] P00NN5_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NN5_n613UnidadeOrganizacional_Vinculada ;
      private int[] P00NN5_A611UnidadeOrganizacional_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV34GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV35GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV36GridStateDynamicFilter ;
   }

   public class getwwgeral_unidadeorganizacionalfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00NN2( IGxContext context ,
                                             String AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ,
                                             String AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ,
                                             int AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ,
                                             bool AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ,
                                             String AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ,
                                             String AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ,
                                             int AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ,
                                             bool AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ,
                                             String AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ,
                                             String AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ,
                                             int AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ,
                                             String AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ,
                                             String AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ,
                                             String AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ,
                                             String AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ,
                                             String AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ,
                                             String AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ,
                                             short AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             String A610TpUo_Nome ,
                                             String A23Estado_UF ,
                                             bool A629UnidadeOrganizacional_Ativo ,
                                             String AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ,
                                             String AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ,
                                             String A1174UnidadeOrganizacinal_Arvore )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[TpUo_Codigo], T1.[UnidadeOrganizacional_Nome], T1.[UnidadeOrganizacional_Ativo], T1.[Estado_UF], T2.[TpUo_Nome], T1.[UnidadeOrganizacional_Vinculada], T1.[UnidadeOrganizacional_Codigo] FROM ([Geral_UnidadeOrganizacional] T1 WITH (NOLOCK) INNER JOIN [Geral_tp_uo] T2 WITH (NOLOCK) ON T2.[TpUo_Codigo] = T1.[TpUo_Codigo])";
         if ( ( StringUtil.StrCmp(AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 + '%')";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like @lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like @lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] = @AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] = @AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TpUo_Nome] like @lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TpUo_Nome] like @lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TpUo_Nome] = @AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TpUo_Nome] = @AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Estado_UF] like @lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Estado_UF] like @lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Estado_UF] = @AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Estado_UF] = @AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Ativo] = 1)";
            }
         }
         if ( AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[UnidadeOrganizacional_Nome]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00NN3( IGxContext context ,
                                             String AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ,
                                             String AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ,
                                             int AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ,
                                             bool AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ,
                                             String AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ,
                                             String AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ,
                                             int AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ,
                                             bool AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ,
                                             String AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ,
                                             String AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ,
                                             int AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ,
                                             String AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ,
                                             String AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ,
                                             String AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ,
                                             String AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ,
                                             String AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ,
                                             String AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ,
                                             short AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             String A610TpUo_Nome ,
                                             String A23Estado_UF ,
                                             bool A629UnidadeOrganizacional_Ativo ,
                                             String AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ,
                                             String AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ,
                                             String A1174UnidadeOrganizacinal_Arvore )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [12] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[TpUo_Codigo], T1.[UnidadeOrganizacional_Ativo], T1.[Estado_UF], T2.[TpUo_Nome], T1.[UnidadeOrganizacional_Nome], T1.[UnidadeOrganizacional_Vinculada], T1.[UnidadeOrganizacional_Codigo] FROM ([Geral_UnidadeOrganizacional] T1 WITH (NOLOCK) INNER JOIN [Geral_tp_uo] T2 WITH (NOLOCK) ON T2.[TpUo_Codigo] = T1.[TpUo_Codigo])";
         if ( ( StringUtil.StrCmp(AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like @lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like @lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] = @AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] = @AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TpUo_Nome] like @lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TpUo_Nome] like @lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TpUo_Nome] = @AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TpUo_Nome] = @AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Estado_UF] like @lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Estado_UF] like @lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Estado_UF] = @AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Estado_UF] = @AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Ativo] = 1)";
            }
         }
         if ( AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[TpUo_Codigo]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00NN4( IGxContext context ,
                                             String AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ,
                                             String AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ,
                                             int AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ,
                                             bool AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ,
                                             String AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ,
                                             String AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ,
                                             int AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ,
                                             bool AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ,
                                             String AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ,
                                             String AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ,
                                             int AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ,
                                             String AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ,
                                             String AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ,
                                             String AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ,
                                             String AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ,
                                             String AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ,
                                             String AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ,
                                             short AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             String A610TpUo_Nome ,
                                             String A23Estado_UF ,
                                             bool A629UnidadeOrganizacional_Ativo ,
                                             String AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ,
                                             String AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ,
                                             String A1174UnidadeOrganizacinal_Arvore )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [12] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[TpUo_Codigo], T1.[UnidadeOrganizacional_Ativo], T1.[Estado_UF], T2.[TpUo_Nome], T1.[UnidadeOrganizacional_Nome], T1.[UnidadeOrganizacional_Vinculada], T1.[UnidadeOrganizacional_Codigo] FROM ([Geral_UnidadeOrganizacional] T1 WITH (NOLOCK) INNER JOIN [Geral_tp_uo] T2 WITH (NOLOCK) ON T2.[TpUo_Codigo] = T1.[TpUo_Codigo])";
         if ( ( StringUtil.StrCmp(AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 + '%')";
            }
         }
         else
         {
            GXv_int6[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1)";
            }
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 + '%')";
            }
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2)";
            }
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 + '%')";
            }
         }
         else
         {
            GXv_int6[4] = 1;
         }
         if ( AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3)";
            }
         }
         else
         {
            GXv_int6[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like @lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like @lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)";
            }
         }
         else
         {
            GXv_int6[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] = @AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] = @AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)";
            }
         }
         else
         {
            GXv_int6[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TpUo_Nome] like @lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TpUo_Nome] like @lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)";
            }
         }
         else
         {
            GXv_int6[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TpUo_Nome] = @AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TpUo_Nome] = @AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)";
            }
         }
         else
         {
            GXv_int6[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Estado_UF] like @lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Estado_UF] like @lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)";
            }
         }
         else
         {
            GXv_int6[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Estado_UF] = @AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Estado_UF] = @AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)";
            }
         }
         else
         {
            GXv_int6[11] = 1;
         }
         if ( AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Ativo] = 1)";
            }
         }
         if ( AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[UnidadeOrganizacional_Codigo]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      protected Object[] conditional_P00NN5( IGxContext context ,
                                             String AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1 ,
                                             String AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 ,
                                             int AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1 ,
                                             bool AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 ,
                                             String AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2 ,
                                             String AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 ,
                                             int AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2 ,
                                             bool AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 ,
                                             String AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3 ,
                                             String AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 ,
                                             int AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3 ,
                                             String AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel ,
                                             String AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome ,
                                             String AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel ,
                                             String AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome ,
                                             String AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel ,
                                             String AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf ,
                                             short AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             String A610TpUo_Nome ,
                                             String A23Estado_UF ,
                                             bool A629UnidadeOrganizacional_Ativo ,
                                             String AV68WWGeral_UnidadeOrganizacionalDS_17_Tfunidadeorganizacinal_arvore_sel ,
                                             String AV67WWGeral_UnidadeOrganizacionalDS_16_Tfunidadeorganizacinal_arvore ,
                                             String A1174UnidadeOrganizacinal_Arvore )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int8 ;
         GXv_int8 = new short [12] ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         scmdbuf = "SELECT T1.[TpUo_Codigo], T1.[Estado_UF], T1.[UnidadeOrganizacional_Ativo], T2.[TpUo_Nome], T1.[UnidadeOrganizacional_Nome], T1.[UnidadeOrganizacional_Vinculada], T1.[UnidadeOrganizacional_Codigo] FROM ([Geral_UnidadeOrganizacional] T1 WITH (NOLOCK) INNER JOIN [Geral_tp_uo] T2 WITH (NOLOCK) ON T2.[TpUo_Codigo] = T1.[TpUo_Codigo])";
         if ( ( StringUtil.StrCmp(AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1 + '%')";
            }
         }
         else
         {
            GXv_int8[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWGeral_UnidadeOrganizacionalDS_1_Dynamicfiltersselector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1)";
            }
         }
         else
         {
            GXv_int8[1] = 1;
         }
         if ( AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2 + '%')";
            }
         }
         else
         {
            GXv_int8[2] = 1;
         }
         if ( AV55WWGeral_UnidadeOrganizacionalDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWGeral_UnidadeOrganizacionalDS_5_Dynamicfiltersselector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2)";
            }
         }
         else
         {
            GXv_int8[3] = 1;
         }
         if ( AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like '%' + @lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like '%' + @lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3 + '%')";
            }
         }
         else
         {
            GXv_int8[4] = 1;
         }
         if ( AV59WWGeral_UnidadeOrganizacionalDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWGeral_UnidadeOrganizacionalDS_9_Dynamicfiltersselector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Vinculada] = @AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Vinculada] = @AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3)";
            }
         }
         else
         {
            GXv_int8[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] like @lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] like @lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome)";
            }
         }
         else
         {
            GXv_int8[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Nome] = @AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Nome] = @AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel)";
            }
         }
         else
         {
            GXv_int8[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TpUo_Nome] like @lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TpUo_Nome] like @lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome)";
            }
         }
         else
         {
            GXv_int8[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TpUo_Nome] = @AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TpUo_Nome] = @AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel)";
            }
         }
         else
         {
            GXv_int8[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Estado_UF] like @lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Estado_UF] like @lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf)";
            }
         }
         else
         {
            GXv_int8[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Estado_UF] = @AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Estado_UF] = @AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel)";
            }
         }
         else
         {
            GXv_int8[11] = 1;
         }
         if ( AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Ativo] = 1)";
            }
         }
         if ( AV71WWGeral_UnidadeOrganizacionalDS_20_Tfunidadeorganizacional_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UnidadeOrganizacional_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UnidadeOrganizacional_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Estado_UF]";
         GXv_Object9[0] = scmdbuf;
         GXv_Object9[1] = GXv_int8;
         return GXv_Object9 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00NN2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
               case 1 :
                     return conditional_P00NN3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
               case 2 :
                     return conditional_P00NN4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
               case 3 :
                     return conditional_P00NN5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (int)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (short)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (bool)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00NN2 ;
          prmP00NN2 = new Object[] {
          new Object[] {"@lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel",SqlDbType.Char,2,0}
          } ;
          Object[] prmP00NN3 ;
          prmP00NN3 = new Object[] {
          new Object[] {"@lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel",SqlDbType.Char,2,0}
          } ;
          Object[] prmP00NN4 ;
          prmP00NN4 = new Object[] {
          new Object[] {"@lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel",SqlDbType.Char,2,0}
          } ;
          Object[] prmP00NN5 ;
          prmP00NN5 = new Object[] {
          new Object[] {"@lV53WWGeral_UnidadeOrganizacionalDS_2_Unidadeorganizacional_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV54WWGeral_UnidadeOrganizacionalDS_3_Unidadeorganizacional_vinculada1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV57WWGeral_UnidadeOrganizacionalDS_6_Unidadeorganizacional_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58WWGeral_UnidadeOrganizacionalDS_7_Unidadeorganizacional_vinculada2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWGeral_UnidadeOrganizacionalDS_10_Unidadeorganizacional_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV62WWGeral_UnidadeOrganizacionalDS_11_Unidadeorganizacional_vinculada3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV63WWGeral_UnidadeOrganizacionalDS_12_Tfunidadeorganizacional_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV64WWGeral_UnidadeOrganizacionalDS_13_Tfunidadeorganizacional_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV65WWGeral_UnidadeOrganizacionalDS_14_Tftpuo_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV66WWGeral_UnidadeOrganizacionalDS_15_Tftpuo_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV69WWGeral_UnidadeOrganizacionalDS_18_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV70WWGeral_UnidadeOrganizacionalDS_19_Tfestado_uf_sel",SqlDbType.Char,2,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00NN2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NN2,100,0,true,false )
             ,new CursorDef("P00NN3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NN3,100,0,true,false )
             ,new CursorDef("P00NN4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NN4,100,0,true,false )
             ,new CursorDef("P00NN5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NN5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 2) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((int[]) buf[7])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwgeral_unidadeorganizacionalfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwgeral_unidadeorganizacionalfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwgeral_unidadeorganizacionalfilterdata") )
          {
             return  ;
          }
          getwwgeral_unidadeorganizacionalfilterdata worker = new getwwgeral_unidadeorganizacionalfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
