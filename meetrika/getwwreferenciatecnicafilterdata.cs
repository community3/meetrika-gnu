/*
               File: GetWWReferenciaTecnicaFilterData
        Description: Get WWReferencia Tecnica Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:11.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwreferenciatecnicafilterdata : GXProcedure
   {
      public getwwreferenciatecnicafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwreferenciatecnicafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV24DDOName = aP0_DDOName;
         this.AV22SearchTxt = aP1_SearchTxt;
         this.AV23SearchTxtTo = aP2_SearchTxtTo;
         this.AV28OptionsJson = "" ;
         this.AV31OptionsDescJson = "" ;
         this.AV33OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
         return AV33OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwreferenciatecnicafilterdata objgetwwreferenciatecnicafilterdata;
         objgetwwreferenciatecnicafilterdata = new getwwreferenciatecnicafilterdata();
         objgetwwreferenciatecnicafilterdata.AV24DDOName = aP0_DDOName;
         objgetwwreferenciatecnicafilterdata.AV22SearchTxt = aP1_SearchTxt;
         objgetwwreferenciatecnicafilterdata.AV23SearchTxtTo = aP2_SearchTxtTo;
         objgetwwreferenciatecnicafilterdata.AV28OptionsJson = "" ;
         objgetwwreferenciatecnicafilterdata.AV31OptionsDescJson = "" ;
         objgetwwreferenciatecnicafilterdata.AV33OptionIndexesJson = "" ;
         objgetwwreferenciatecnicafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwreferenciatecnicafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwreferenciatecnicafilterdata);
         aP3_OptionsJson=this.AV28OptionsJson;
         aP4_OptionsDescJson=this.AV31OptionsDescJson;
         aP5_OptionIndexesJson=this.AV33OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwreferenciatecnicafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV27Options = (IGxCollection)(new GxSimpleCollection());
         AV30OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV32OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_REFERENCIATECNICA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADREFERENCIATECNICA_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_REFERENCIATECNICA_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADREFERENCIATECNICA_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV24DDOName), "DDO_GUIA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADGUIA_NOMEOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV28OptionsJson = AV27Options.ToJSonString(false);
         AV31OptionsDescJson = AV30OptionsDesc.ToJSonString(false);
         AV33OptionIndexesJson = AV32OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV35Session.Get("WWReferenciaTecnicaGridState"), "") == 0 )
         {
            AV37GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWReferenciaTecnicaGridState"), "");
         }
         else
         {
            AV37GridState.FromXml(AV35Session.Get("WWReferenciaTecnicaGridState"), "");
         }
         AV58GXV1 = 1;
         while ( AV58GXV1 <= AV37GridState.gxTpr_Filtervalues.Count )
         {
            AV38GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV37GridState.gxTpr_Filtervalues.Item(AV58GXV1));
            if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_CODIGO") == 0 )
            {
               AV10TFReferenciaTecnica_Codigo = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, "."));
               AV11TFReferenciaTecnica_Codigo_To = (int)(NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_NOME") == 0 )
            {
               AV12TFReferenciaTecnica_Nome = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_NOME_SEL") == 0 )
            {
               AV13TFReferenciaTecnica_Nome_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_DESCRICAO") == 0 )
            {
               AV14TFReferenciaTecnica_Descricao = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_DESCRICAO_SEL") == 0 )
            {
               AV15TFReferenciaTecnica_Descricao_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_UNIDADE_SEL") == 0 )
            {
               AV16TFReferenciaTecnica_Unidade_SelsJson = AV38GridStateFilterValue.gxTpr_Value;
               AV17TFReferenciaTecnica_Unidade_Sels.FromJSonString(AV16TFReferenciaTecnica_Unidade_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFREFERENCIATECNICA_VALOR") == 0 )
            {
               AV18TFReferenciaTecnica_Valor = NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Value, ".");
               AV19TFReferenciaTecnica_Valor_To = NumberUtil.Val( AV38GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFGUIA_NOME") == 0 )
            {
               AV20TFGuia_Nome = AV38GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV38GridStateFilterValue.gxTpr_Name, "TFGUIA_NOME_SEL") == 0 )
            {
               AV21TFGuia_Nome_Sel = AV38GridStateFilterValue.gxTpr_Value;
            }
            AV58GXV1 = (int)(AV58GXV1+1);
         }
         if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(1));
            AV40DynamicFiltersSelector1 = AV39GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "REFERENCIATECNICA_NOME") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV39GridStateDynamicFilter.gxTpr_Operator;
               AV42ReferenciaTecnica_Nome1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector1, "GUIA_NOME") == 0 )
            {
               AV41DynamicFiltersOperator1 = AV39GridStateDynamicFilter.gxTpr_Operator;
               AV43Guia_Nome1 = AV39GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV44DynamicFiltersEnabled2 = true;
               AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(2));
               AV45DynamicFiltersSelector2 = AV39GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "REFERENCIATECNICA_NOME") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV39GridStateDynamicFilter.gxTpr_Operator;
                  AV47ReferenciaTecnica_Nome2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector2, "GUIA_NOME") == 0 )
               {
                  AV46DynamicFiltersOperator2 = AV39GridStateDynamicFilter.gxTpr_Operator;
                  AV48Guia_Nome2 = AV39GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV37GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV49DynamicFiltersEnabled3 = true;
                  AV39GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV37GridState.gxTpr_Dynamicfilters.Item(3));
                  AV50DynamicFiltersSelector3 = AV39GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "REFERENCIATECNICA_NOME") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV39GridStateDynamicFilter.gxTpr_Operator;
                     AV52ReferenciaTecnica_Nome3 = AV39GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "GUIA_NOME") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV39GridStateDynamicFilter.gxTpr_Operator;
                     AV53Guia_Nome3 = AV39GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADREFERENCIATECNICA_NOMEOPTIONS' Routine */
         AV12TFReferenciaTecnica_Nome = AV22SearchTxt;
         AV13TFReferenciaTecnica_Nome_Sel = "";
         AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 = AV42ReferenciaTecnica_Nome1;
         AV63WWReferenciaTecnicaDS_4_Guia_nome1 = AV43Guia_Nome1;
         AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 = AV44DynamicFiltersEnabled2;
         AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2 = AV45DynamicFiltersSelector2;
         AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 = AV46DynamicFiltersOperator2;
         AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 = AV47ReferenciaTecnica_Nome2;
         AV68WWReferenciaTecnicaDS_9_Guia_nome2 = AV48Guia_Nome2;
         AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 = AV52ReferenciaTecnica_Nome3;
         AV73WWReferenciaTecnicaDS_14_Guia_nome3 = AV53Guia_Nome3;
         AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo = AV10TFReferenciaTecnica_Codigo;
         AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to = AV11TFReferenciaTecnica_Codigo_To;
         AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome = AV12TFReferenciaTecnica_Nome;
         AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel = AV13TFReferenciaTecnica_Nome_Sel;
         AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao = AV14TFReferenciaTecnica_Descricao;
         AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel = AV15TFReferenciaTecnica_Descricao_Sel;
         AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels = AV17TFReferenciaTecnica_Unidade_Sels;
         AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor = AV18TFReferenciaTecnica_Valor;
         AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to = AV19TFReferenciaTecnica_Valor_To;
         AV83WWReferenciaTecnicaDS_24_Tfguia_nome = AV20TFGuia_Nome;
         AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel = AV21TFGuia_Nome_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A114ReferenciaTecnica_Unidade ,
                                              AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels ,
                                              AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1 ,
                                              AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 ,
                                              AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 ,
                                              AV63WWReferenciaTecnicaDS_4_Guia_nome1 ,
                                              AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 ,
                                              AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2 ,
                                              AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 ,
                                              AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 ,
                                              AV68WWReferenciaTecnicaDS_9_Guia_nome2 ,
                                              AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 ,
                                              AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3 ,
                                              AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 ,
                                              AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 ,
                                              AV73WWReferenciaTecnicaDS_14_Guia_nome3 ,
                                              AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo ,
                                              AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to ,
                                              AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel ,
                                              AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome ,
                                              AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel ,
                                              AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao ,
                                              AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels.Count ,
                                              AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor ,
                                              AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to ,
                                              AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel ,
                                              AV83WWReferenciaTecnicaDS_24_Tfguia_nome ,
                                              A98ReferenciaTecnica_Nome ,
                                              A94Guia_Nome ,
                                              A97ReferenciaTecnica_Codigo ,
                                              A99ReferenciaTecnica_Descricao ,
                                              A100ReferenciaTecnica_Valor },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.DECIMAL
                                              }
         });
         lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1), 50, "%");
         lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1), 50, "%");
         lV63WWReferenciaTecnicaDS_4_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1), 50, "%");
         lV63WWReferenciaTecnicaDS_4_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1), 50, "%");
         lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2), 50, "%");
         lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2), 50, "%");
         lV68WWReferenciaTecnicaDS_9_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2), 50, "%");
         lV68WWReferenciaTecnicaDS_9_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2), 50, "%");
         lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3), 50, "%");
         lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3), 50, "%");
         lV73WWReferenciaTecnicaDS_14_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3), 50, "%");
         lV73WWReferenciaTecnicaDS_14_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3), 50, "%");
         lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome = StringUtil.PadR( StringUtil.RTrim( AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome), 50, "%");
         lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao = StringUtil.Concat( StringUtil.RTrim( AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao), "%", "");
         lV83WWReferenciaTecnicaDS_24_Tfguia_nome = StringUtil.PadR( StringUtil.RTrim( AV83WWReferenciaTecnicaDS_24_Tfguia_nome), 50, "%");
         /* Using cursor P00SN2 */
         pr_default.execute(0, new Object[] {lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1, lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1, lV63WWReferenciaTecnicaDS_4_Guia_nome1, lV63WWReferenciaTecnicaDS_4_Guia_nome1, lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2, lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2, lV68WWReferenciaTecnicaDS_9_Guia_nome2, lV68WWReferenciaTecnicaDS_9_Guia_nome2, lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3, lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3, lV73WWReferenciaTecnicaDS_14_Guia_nome3, lV73WWReferenciaTecnicaDS_14_Guia_nome3, AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo, AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to, lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome, AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel, lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao, AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel, AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor, AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to, lV83WWReferenciaTecnicaDS_24_Tfguia_nome, AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKSN2 = false;
            A93Guia_Codigo = P00SN2_A93Guia_Codigo[0];
            A98ReferenciaTecnica_Nome = P00SN2_A98ReferenciaTecnica_Nome[0];
            A100ReferenciaTecnica_Valor = P00SN2_A100ReferenciaTecnica_Valor[0];
            A114ReferenciaTecnica_Unidade = P00SN2_A114ReferenciaTecnica_Unidade[0];
            A99ReferenciaTecnica_Descricao = P00SN2_A99ReferenciaTecnica_Descricao[0];
            A97ReferenciaTecnica_Codigo = P00SN2_A97ReferenciaTecnica_Codigo[0];
            A94Guia_Nome = P00SN2_A94Guia_Nome[0];
            A94Guia_Nome = P00SN2_A94Guia_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00SN2_A98ReferenciaTecnica_Nome[0], A98ReferenciaTecnica_Nome) == 0 ) )
            {
               BRKSN2 = false;
               A97ReferenciaTecnica_Codigo = P00SN2_A97ReferenciaTecnica_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKSN2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A98ReferenciaTecnica_Nome)) )
            {
               AV26Option = A98ReferenciaTecnica_Nome;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSN2 )
            {
               BRKSN2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADREFERENCIATECNICA_DESCRICAOOPTIONS' Routine */
         AV14TFReferenciaTecnica_Descricao = AV22SearchTxt;
         AV15TFReferenciaTecnica_Descricao_Sel = "";
         AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 = AV42ReferenciaTecnica_Nome1;
         AV63WWReferenciaTecnicaDS_4_Guia_nome1 = AV43Guia_Nome1;
         AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 = AV44DynamicFiltersEnabled2;
         AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2 = AV45DynamicFiltersSelector2;
         AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 = AV46DynamicFiltersOperator2;
         AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 = AV47ReferenciaTecnica_Nome2;
         AV68WWReferenciaTecnicaDS_9_Guia_nome2 = AV48Guia_Nome2;
         AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 = AV52ReferenciaTecnica_Nome3;
         AV73WWReferenciaTecnicaDS_14_Guia_nome3 = AV53Guia_Nome3;
         AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo = AV10TFReferenciaTecnica_Codigo;
         AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to = AV11TFReferenciaTecnica_Codigo_To;
         AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome = AV12TFReferenciaTecnica_Nome;
         AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel = AV13TFReferenciaTecnica_Nome_Sel;
         AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao = AV14TFReferenciaTecnica_Descricao;
         AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel = AV15TFReferenciaTecnica_Descricao_Sel;
         AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels = AV17TFReferenciaTecnica_Unidade_Sels;
         AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor = AV18TFReferenciaTecnica_Valor;
         AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to = AV19TFReferenciaTecnica_Valor_To;
         AV83WWReferenciaTecnicaDS_24_Tfguia_nome = AV20TFGuia_Nome;
         AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel = AV21TFGuia_Nome_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A114ReferenciaTecnica_Unidade ,
                                              AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels ,
                                              AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1 ,
                                              AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 ,
                                              AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 ,
                                              AV63WWReferenciaTecnicaDS_4_Guia_nome1 ,
                                              AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 ,
                                              AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2 ,
                                              AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 ,
                                              AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 ,
                                              AV68WWReferenciaTecnicaDS_9_Guia_nome2 ,
                                              AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 ,
                                              AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3 ,
                                              AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 ,
                                              AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 ,
                                              AV73WWReferenciaTecnicaDS_14_Guia_nome3 ,
                                              AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo ,
                                              AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to ,
                                              AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel ,
                                              AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome ,
                                              AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel ,
                                              AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao ,
                                              AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels.Count ,
                                              AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor ,
                                              AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to ,
                                              AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel ,
                                              AV83WWReferenciaTecnicaDS_24_Tfguia_nome ,
                                              A98ReferenciaTecnica_Nome ,
                                              A94Guia_Nome ,
                                              A97ReferenciaTecnica_Codigo ,
                                              A99ReferenciaTecnica_Descricao ,
                                              A100ReferenciaTecnica_Valor },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.DECIMAL
                                              }
         });
         lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1), 50, "%");
         lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1), 50, "%");
         lV63WWReferenciaTecnicaDS_4_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1), 50, "%");
         lV63WWReferenciaTecnicaDS_4_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1), 50, "%");
         lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2), 50, "%");
         lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2), 50, "%");
         lV68WWReferenciaTecnicaDS_9_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2), 50, "%");
         lV68WWReferenciaTecnicaDS_9_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2), 50, "%");
         lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3), 50, "%");
         lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3), 50, "%");
         lV73WWReferenciaTecnicaDS_14_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3), 50, "%");
         lV73WWReferenciaTecnicaDS_14_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3), 50, "%");
         lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome = StringUtil.PadR( StringUtil.RTrim( AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome), 50, "%");
         lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao = StringUtil.Concat( StringUtil.RTrim( AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao), "%", "");
         lV83WWReferenciaTecnicaDS_24_Tfguia_nome = StringUtil.PadR( StringUtil.RTrim( AV83WWReferenciaTecnicaDS_24_Tfguia_nome), 50, "%");
         /* Using cursor P00SN3 */
         pr_default.execute(1, new Object[] {lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1, lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1, lV63WWReferenciaTecnicaDS_4_Guia_nome1, lV63WWReferenciaTecnicaDS_4_Guia_nome1, lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2, lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2, lV68WWReferenciaTecnicaDS_9_Guia_nome2, lV68WWReferenciaTecnicaDS_9_Guia_nome2, lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3, lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3, lV73WWReferenciaTecnicaDS_14_Guia_nome3, lV73WWReferenciaTecnicaDS_14_Guia_nome3, AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo, AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to, lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome, AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel, lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao, AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel, AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor, AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to, lV83WWReferenciaTecnicaDS_24_Tfguia_nome, AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKSN4 = false;
            A93Guia_Codigo = P00SN3_A93Guia_Codigo[0];
            A99ReferenciaTecnica_Descricao = P00SN3_A99ReferenciaTecnica_Descricao[0];
            A100ReferenciaTecnica_Valor = P00SN3_A100ReferenciaTecnica_Valor[0];
            A114ReferenciaTecnica_Unidade = P00SN3_A114ReferenciaTecnica_Unidade[0];
            A97ReferenciaTecnica_Codigo = P00SN3_A97ReferenciaTecnica_Codigo[0];
            A94Guia_Nome = P00SN3_A94Guia_Nome[0];
            A98ReferenciaTecnica_Nome = P00SN3_A98ReferenciaTecnica_Nome[0];
            A94Guia_Nome = P00SN3_A94Guia_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00SN3_A99ReferenciaTecnica_Descricao[0], A99ReferenciaTecnica_Descricao) == 0 ) )
            {
               BRKSN4 = false;
               A97ReferenciaTecnica_Codigo = P00SN3_A97ReferenciaTecnica_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKSN4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A99ReferenciaTecnica_Descricao)) )
            {
               AV26Option = A99ReferenciaTecnica_Descricao;
               AV27Options.Add(AV26Option, 0);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSN4 )
            {
               BRKSN4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADGUIA_NOMEOPTIONS' Routine */
         AV20TFGuia_Nome = AV22SearchTxt;
         AV21TFGuia_Nome_Sel = "";
         AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1 = AV40DynamicFiltersSelector1;
         AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 = AV41DynamicFiltersOperator1;
         AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 = AV42ReferenciaTecnica_Nome1;
         AV63WWReferenciaTecnicaDS_4_Guia_nome1 = AV43Guia_Nome1;
         AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 = AV44DynamicFiltersEnabled2;
         AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2 = AV45DynamicFiltersSelector2;
         AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 = AV46DynamicFiltersOperator2;
         AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 = AV47ReferenciaTecnica_Nome2;
         AV68WWReferenciaTecnicaDS_9_Guia_nome2 = AV48Guia_Nome2;
         AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 = AV49DynamicFiltersEnabled3;
         AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3 = AV50DynamicFiltersSelector3;
         AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 = AV51DynamicFiltersOperator3;
         AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 = AV52ReferenciaTecnica_Nome3;
         AV73WWReferenciaTecnicaDS_14_Guia_nome3 = AV53Guia_Nome3;
         AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo = AV10TFReferenciaTecnica_Codigo;
         AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to = AV11TFReferenciaTecnica_Codigo_To;
         AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome = AV12TFReferenciaTecnica_Nome;
         AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel = AV13TFReferenciaTecnica_Nome_Sel;
         AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao = AV14TFReferenciaTecnica_Descricao;
         AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel = AV15TFReferenciaTecnica_Descricao_Sel;
         AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels = AV17TFReferenciaTecnica_Unidade_Sels;
         AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor = AV18TFReferenciaTecnica_Valor;
         AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to = AV19TFReferenciaTecnica_Valor_To;
         AV83WWReferenciaTecnicaDS_24_Tfguia_nome = AV20TFGuia_Nome;
         AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel = AV21TFGuia_Nome_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A114ReferenciaTecnica_Unidade ,
                                              AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels ,
                                              AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1 ,
                                              AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 ,
                                              AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 ,
                                              AV63WWReferenciaTecnicaDS_4_Guia_nome1 ,
                                              AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 ,
                                              AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2 ,
                                              AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 ,
                                              AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 ,
                                              AV68WWReferenciaTecnicaDS_9_Guia_nome2 ,
                                              AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 ,
                                              AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3 ,
                                              AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 ,
                                              AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 ,
                                              AV73WWReferenciaTecnicaDS_14_Guia_nome3 ,
                                              AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo ,
                                              AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to ,
                                              AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel ,
                                              AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome ,
                                              AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel ,
                                              AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao ,
                                              AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels.Count ,
                                              AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor ,
                                              AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to ,
                                              AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel ,
                                              AV83WWReferenciaTecnicaDS_24_Tfguia_nome ,
                                              A98ReferenciaTecnica_Nome ,
                                              A94Guia_Nome ,
                                              A97ReferenciaTecnica_Codigo ,
                                              A99ReferenciaTecnica_Descricao ,
                                              A100ReferenciaTecnica_Valor },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.DECIMAL
                                              }
         });
         lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1), 50, "%");
         lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 = StringUtil.PadR( StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1), 50, "%");
         lV63WWReferenciaTecnicaDS_4_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1), 50, "%");
         lV63WWReferenciaTecnicaDS_4_Guia_nome1 = StringUtil.PadR( StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1), 50, "%");
         lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2), 50, "%");
         lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 = StringUtil.PadR( StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2), 50, "%");
         lV68WWReferenciaTecnicaDS_9_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2), 50, "%");
         lV68WWReferenciaTecnicaDS_9_Guia_nome2 = StringUtil.PadR( StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2), 50, "%");
         lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3), 50, "%");
         lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 = StringUtil.PadR( StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3), 50, "%");
         lV73WWReferenciaTecnicaDS_14_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3), 50, "%");
         lV73WWReferenciaTecnicaDS_14_Guia_nome3 = StringUtil.PadR( StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3), 50, "%");
         lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome = StringUtil.PadR( StringUtil.RTrim( AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome), 50, "%");
         lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao = StringUtil.Concat( StringUtil.RTrim( AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao), "%", "");
         lV83WWReferenciaTecnicaDS_24_Tfguia_nome = StringUtil.PadR( StringUtil.RTrim( AV83WWReferenciaTecnicaDS_24_Tfguia_nome), 50, "%");
         /* Using cursor P00SN4 */
         pr_default.execute(2, new Object[] {lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1, lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1, lV63WWReferenciaTecnicaDS_4_Guia_nome1, lV63WWReferenciaTecnicaDS_4_Guia_nome1, lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2, lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2, lV68WWReferenciaTecnicaDS_9_Guia_nome2, lV68WWReferenciaTecnicaDS_9_Guia_nome2, lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3, lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3, lV73WWReferenciaTecnicaDS_14_Guia_nome3, lV73WWReferenciaTecnicaDS_14_Guia_nome3, AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo, AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to, lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome, AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel, lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao, AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel, AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor, AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to, lV83WWReferenciaTecnicaDS_24_Tfguia_nome, AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKSN6 = false;
            A93Guia_Codigo = P00SN4_A93Guia_Codigo[0];
            A100ReferenciaTecnica_Valor = P00SN4_A100ReferenciaTecnica_Valor[0];
            A114ReferenciaTecnica_Unidade = P00SN4_A114ReferenciaTecnica_Unidade[0];
            A99ReferenciaTecnica_Descricao = P00SN4_A99ReferenciaTecnica_Descricao[0];
            A97ReferenciaTecnica_Codigo = P00SN4_A97ReferenciaTecnica_Codigo[0];
            A94Guia_Nome = P00SN4_A94Guia_Nome[0];
            A98ReferenciaTecnica_Nome = P00SN4_A98ReferenciaTecnica_Nome[0];
            A94Guia_Nome = P00SN4_A94Guia_Nome[0];
            AV34count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00SN4_A93Guia_Codigo[0] == A93Guia_Codigo ) )
            {
               BRKSN6 = false;
               A97ReferenciaTecnica_Codigo = P00SN4_A97ReferenciaTecnica_Codigo[0];
               AV34count = (long)(AV34count+1);
               BRKSN6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A94Guia_Nome)) )
            {
               AV26Option = A94Guia_Nome;
               AV25InsertIndex = 1;
               while ( ( AV25InsertIndex <= AV27Options.Count ) && ( StringUtil.StrCmp(((String)AV27Options.Item(AV25InsertIndex)), AV26Option) < 0 ) )
               {
                  AV25InsertIndex = (int)(AV25InsertIndex+1);
               }
               AV27Options.Add(AV26Option, AV25InsertIndex);
               AV32OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV34count), "Z,ZZZ,ZZZ,ZZ9")), AV25InsertIndex);
            }
            if ( AV27Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSN6 )
            {
               BRKSN6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV27Options = new GxSimpleCollection();
         AV30OptionsDesc = new GxSimpleCollection();
         AV32OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV35Session = context.GetSession();
         AV37GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV38GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFReferenciaTecnica_Nome = "";
         AV13TFReferenciaTecnica_Nome_Sel = "";
         AV14TFReferenciaTecnica_Descricao = "";
         AV15TFReferenciaTecnica_Descricao_Sel = "";
         AV16TFReferenciaTecnica_Unidade_SelsJson = "";
         AV17TFReferenciaTecnica_Unidade_Sels = new GxSimpleCollection();
         AV20TFGuia_Nome = "";
         AV21TFGuia_Nome_Sel = "";
         AV39GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV40DynamicFiltersSelector1 = "";
         AV42ReferenciaTecnica_Nome1 = "";
         AV43Guia_Nome1 = "";
         AV45DynamicFiltersSelector2 = "";
         AV47ReferenciaTecnica_Nome2 = "";
         AV48Guia_Nome2 = "";
         AV50DynamicFiltersSelector3 = "";
         AV52ReferenciaTecnica_Nome3 = "";
         AV53Guia_Nome3 = "";
         AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1 = "";
         AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 = "";
         AV63WWReferenciaTecnicaDS_4_Guia_nome1 = "";
         AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2 = "";
         AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 = "";
         AV68WWReferenciaTecnicaDS_9_Guia_nome2 = "";
         AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3 = "";
         AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 = "";
         AV73WWReferenciaTecnicaDS_14_Guia_nome3 = "";
         AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome = "";
         AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel = "";
         AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao = "";
         AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel = "";
         AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels = new GxSimpleCollection();
         AV83WWReferenciaTecnicaDS_24_Tfguia_nome = "";
         AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel = "";
         scmdbuf = "";
         lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 = "";
         lV63WWReferenciaTecnicaDS_4_Guia_nome1 = "";
         lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 = "";
         lV68WWReferenciaTecnicaDS_9_Guia_nome2 = "";
         lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 = "";
         lV73WWReferenciaTecnicaDS_14_Guia_nome3 = "";
         lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome = "";
         lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao = "";
         lV83WWReferenciaTecnicaDS_24_Tfguia_nome = "";
         A98ReferenciaTecnica_Nome = "";
         A94Guia_Nome = "";
         A99ReferenciaTecnica_Descricao = "";
         P00SN2_A93Guia_Codigo = new int[1] ;
         P00SN2_A98ReferenciaTecnica_Nome = new String[] {""} ;
         P00SN2_A100ReferenciaTecnica_Valor = new decimal[1] ;
         P00SN2_A114ReferenciaTecnica_Unidade = new short[1] ;
         P00SN2_A99ReferenciaTecnica_Descricao = new String[] {""} ;
         P00SN2_A97ReferenciaTecnica_Codigo = new int[1] ;
         P00SN2_A94Guia_Nome = new String[] {""} ;
         AV26Option = "";
         P00SN3_A93Guia_Codigo = new int[1] ;
         P00SN3_A99ReferenciaTecnica_Descricao = new String[] {""} ;
         P00SN3_A100ReferenciaTecnica_Valor = new decimal[1] ;
         P00SN3_A114ReferenciaTecnica_Unidade = new short[1] ;
         P00SN3_A97ReferenciaTecnica_Codigo = new int[1] ;
         P00SN3_A94Guia_Nome = new String[] {""} ;
         P00SN3_A98ReferenciaTecnica_Nome = new String[] {""} ;
         P00SN4_A93Guia_Codigo = new int[1] ;
         P00SN4_A100ReferenciaTecnica_Valor = new decimal[1] ;
         P00SN4_A114ReferenciaTecnica_Unidade = new short[1] ;
         P00SN4_A99ReferenciaTecnica_Descricao = new String[] {""} ;
         P00SN4_A97ReferenciaTecnica_Codigo = new int[1] ;
         P00SN4_A94Guia_Nome = new String[] {""} ;
         P00SN4_A98ReferenciaTecnica_Nome = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwreferenciatecnicafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00SN2_A93Guia_Codigo, P00SN2_A98ReferenciaTecnica_Nome, P00SN2_A100ReferenciaTecnica_Valor, P00SN2_A114ReferenciaTecnica_Unidade, P00SN2_A99ReferenciaTecnica_Descricao, P00SN2_A97ReferenciaTecnica_Codigo, P00SN2_A94Guia_Nome
               }
               , new Object[] {
               P00SN3_A93Guia_Codigo, P00SN3_A99ReferenciaTecnica_Descricao, P00SN3_A100ReferenciaTecnica_Valor, P00SN3_A114ReferenciaTecnica_Unidade, P00SN3_A97ReferenciaTecnica_Codigo, P00SN3_A94Guia_Nome, P00SN3_A98ReferenciaTecnica_Nome
               }
               , new Object[] {
               P00SN4_A93Guia_Codigo, P00SN4_A100ReferenciaTecnica_Valor, P00SN4_A114ReferenciaTecnica_Unidade, P00SN4_A99ReferenciaTecnica_Descricao, P00SN4_A97ReferenciaTecnica_Codigo, P00SN4_A94Guia_Nome, P00SN4_A98ReferenciaTecnica_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV41DynamicFiltersOperator1 ;
      private short AV46DynamicFiltersOperator2 ;
      private short AV51DynamicFiltersOperator3 ;
      private short AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 ;
      private short AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 ;
      private short AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 ;
      private short A114ReferenciaTecnica_Unidade ;
      private int AV58GXV1 ;
      private int AV10TFReferenciaTecnica_Codigo ;
      private int AV11TFReferenciaTecnica_Codigo_To ;
      private int AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo ;
      private int AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to ;
      private int AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels_Count ;
      private int A97ReferenciaTecnica_Codigo ;
      private int A93Guia_Codigo ;
      private int AV25InsertIndex ;
      private long AV34count ;
      private decimal AV18TFReferenciaTecnica_Valor ;
      private decimal AV19TFReferenciaTecnica_Valor_To ;
      private decimal AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor ;
      private decimal AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to ;
      private decimal A100ReferenciaTecnica_Valor ;
      private String AV12TFReferenciaTecnica_Nome ;
      private String AV13TFReferenciaTecnica_Nome_Sel ;
      private String AV20TFGuia_Nome ;
      private String AV21TFGuia_Nome_Sel ;
      private String AV42ReferenciaTecnica_Nome1 ;
      private String AV43Guia_Nome1 ;
      private String AV47ReferenciaTecnica_Nome2 ;
      private String AV48Guia_Nome2 ;
      private String AV52ReferenciaTecnica_Nome3 ;
      private String AV53Guia_Nome3 ;
      private String AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 ;
      private String AV63WWReferenciaTecnicaDS_4_Guia_nome1 ;
      private String AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 ;
      private String AV68WWReferenciaTecnicaDS_9_Guia_nome2 ;
      private String AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 ;
      private String AV73WWReferenciaTecnicaDS_14_Guia_nome3 ;
      private String AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome ;
      private String AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel ;
      private String AV83WWReferenciaTecnicaDS_24_Tfguia_nome ;
      private String AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel ;
      private String scmdbuf ;
      private String lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 ;
      private String lV63WWReferenciaTecnicaDS_4_Guia_nome1 ;
      private String lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 ;
      private String lV68WWReferenciaTecnicaDS_9_Guia_nome2 ;
      private String lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 ;
      private String lV73WWReferenciaTecnicaDS_14_Guia_nome3 ;
      private String lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome ;
      private String lV83WWReferenciaTecnicaDS_24_Tfguia_nome ;
      private String A98ReferenciaTecnica_Nome ;
      private String A94Guia_Nome ;
      private bool returnInSub ;
      private bool AV44DynamicFiltersEnabled2 ;
      private bool AV49DynamicFiltersEnabled3 ;
      private bool AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 ;
      private bool AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 ;
      private bool BRKSN2 ;
      private bool BRKSN4 ;
      private bool BRKSN6 ;
      private String AV33OptionIndexesJson ;
      private String AV28OptionsJson ;
      private String AV31OptionsDescJson ;
      private String AV16TFReferenciaTecnica_Unidade_SelsJson ;
      private String A99ReferenciaTecnica_Descricao ;
      private String AV24DDOName ;
      private String AV22SearchTxt ;
      private String AV23SearchTxtTo ;
      private String AV14TFReferenciaTecnica_Descricao ;
      private String AV15TFReferenciaTecnica_Descricao_Sel ;
      private String AV40DynamicFiltersSelector1 ;
      private String AV45DynamicFiltersSelector2 ;
      private String AV50DynamicFiltersSelector3 ;
      private String AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1 ;
      private String AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2 ;
      private String AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3 ;
      private String AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao ;
      private String AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel ;
      private String lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao ;
      private String AV26Option ;
      private IGxSession AV35Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00SN2_A93Guia_Codigo ;
      private String[] P00SN2_A98ReferenciaTecnica_Nome ;
      private decimal[] P00SN2_A100ReferenciaTecnica_Valor ;
      private short[] P00SN2_A114ReferenciaTecnica_Unidade ;
      private String[] P00SN2_A99ReferenciaTecnica_Descricao ;
      private int[] P00SN2_A97ReferenciaTecnica_Codigo ;
      private String[] P00SN2_A94Guia_Nome ;
      private int[] P00SN3_A93Guia_Codigo ;
      private String[] P00SN3_A99ReferenciaTecnica_Descricao ;
      private decimal[] P00SN3_A100ReferenciaTecnica_Valor ;
      private short[] P00SN3_A114ReferenciaTecnica_Unidade ;
      private int[] P00SN3_A97ReferenciaTecnica_Codigo ;
      private String[] P00SN3_A94Guia_Nome ;
      private String[] P00SN3_A98ReferenciaTecnica_Nome ;
      private int[] P00SN4_A93Guia_Codigo ;
      private decimal[] P00SN4_A100ReferenciaTecnica_Valor ;
      private short[] P00SN4_A114ReferenciaTecnica_Unidade ;
      private String[] P00SN4_A99ReferenciaTecnica_Descricao ;
      private int[] P00SN4_A97ReferenciaTecnica_Codigo ;
      private String[] P00SN4_A94Guia_Nome ;
      private String[] P00SN4_A98ReferenciaTecnica_Nome ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17TFReferenciaTecnica_Unidade_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV37GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV38GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV39GridStateDynamicFilter ;
   }

   public class getwwreferenciatecnicafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00SN2( IGxContext context ,
                                             short A114ReferenciaTecnica_Unidade ,
                                             IGxCollection AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels ,
                                             String AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1 ,
                                             short AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 ,
                                             String AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 ,
                                             String AV63WWReferenciaTecnicaDS_4_Guia_nome1 ,
                                             bool AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 ,
                                             String AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2 ,
                                             short AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 ,
                                             String AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 ,
                                             String AV68WWReferenciaTecnicaDS_9_Guia_nome2 ,
                                             bool AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 ,
                                             String AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3 ,
                                             short AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 ,
                                             String AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 ,
                                             String AV73WWReferenciaTecnicaDS_14_Guia_nome3 ,
                                             int AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo ,
                                             int AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to ,
                                             String AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel ,
                                             String AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome ,
                                             String AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel ,
                                             String AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao ,
                                             int AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels_Count ,
                                             decimal AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor ,
                                             decimal AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to ,
                                             String AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel ,
                                             String AV83WWReferenciaTecnicaDS_24_Tfguia_nome ,
                                             String A98ReferenciaTecnica_Nome ,
                                             String A94Guia_Nome ,
                                             int A97ReferenciaTecnica_Codigo ,
                                             String A99ReferenciaTecnica_Descricao ,
                                             decimal A100ReferenciaTecnica_Valor )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [22] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Guia_Codigo], T1.[ReferenciaTecnica_Nome], T1.[ReferenciaTecnica_Valor], T1.[ReferenciaTecnica_Unidade], T1.[ReferenciaTecnica_Descricao], T1.[ReferenciaTecnica_Codigo], T2.[Guia_Nome] FROM ([ReferenciaTecnica] T1 WITH (NOLOCK) INNER JOIN [Guia] T2 WITH (NOLOCK) ON T2.[Guia_Codigo] = T1.[Guia_Codigo])";
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] >= @AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] >= @AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] <= @AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] <= @AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] = @AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] = @AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] like @lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] like @lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] = @AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] = @AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
         }
         if ( ! (Convert.ToDecimal(0)==AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] >= @AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] >= @AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] <= @AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] <= @AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWReferenciaTecnicaDS_24_Tfguia_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV83WWReferenciaTecnicaDS_24_Tfguia_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV83WWReferenciaTecnicaDS_24_Tfguia_nome)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] = @AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] = @AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ReferenciaTecnica_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00SN3( IGxContext context ,
                                             short A114ReferenciaTecnica_Unidade ,
                                             IGxCollection AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels ,
                                             String AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1 ,
                                             short AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 ,
                                             String AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 ,
                                             String AV63WWReferenciaTecnicaDS_4_Guia_nome1 ,
                                             bool AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 ,
                                             String AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2 ,
                                             short AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 ,
                                             String AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 ,
                                             String AV68WWReferenciaTecnicaDS_9_Guia_nome2 ,
                                             bool AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 ,
                                             String AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3 ,
                                             short AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 ,
                                             String AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 ,
                                             String AV73WWReferenciaTecnicaDS_14_Guia_nome3 ,
                                             int AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo ,
                                             int AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to ,
                                             String AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel ,
                                             String AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome ,
                                             String AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel ,
                                             String AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao ,
                                             int AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels_Count ,
                                             decimal AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor ,
                                             decimal AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to ,
                                             String AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel ,
                                             String AV83WWReferenciaTecnicaDS_24_Tfguia_nome ,
                                             String A98ReferenciaTecnica_Nome ,
                                             String A94Guia_Nome ,
                                             int A97ReferenciaTecnica_Codigo ,
                                             String A99ReferenciaTecnica_Descricao ,
                                             decimal A100ReferenciaTecnica_Valor )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [22] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Guia_Codigo], T1.[ReferenciaTecnica_Descricao], T1.[ReferenciaTecnica_Valor], T1.[ReferenciaTecnica_Unidade], T1.[ReferenciaTecnica_Codigo], T2.[Guia_Nome], T1.[ReferenciaTecnica_Nome] FROM ([ReferenciaTecnica] T1 WITH (NOLOCK) INNER JOIN [Guia] T2 WITH (NOLOCK) ON T2.[Guia_Codigo] = T1.[Guia_Codigo])";
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] >= @AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] >= @AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] <= @AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] <= @AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] = @AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] = @AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] like @lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] like @lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] = @AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] = @AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
         }
         if ( ! (Convert.ToDecimal(0)==AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] >= @AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] >= @AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] <= @AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] <= @AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWReferenciaTecnicaDS_24_Tfguia_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV83WWReferenciaTecnicaDS_24_Tfguia_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV83WWReferenciaTecnicaDS_24_Tfguia_nome)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] = @AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] = @AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ReferenciaTecnica_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00SN4( IGxContext context ,
                                             short A114ReferenciaTecnica_Unidade ,
                                             IGxCollection AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels ,
                                             String AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1 ,
                                             short AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 ,
                                             String AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1 ,
                                             String AV63WWReferenciaTecnicaDS_4_Guia_nome1 ,
                                             bool AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 ,
                                             String AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2 ,
                                             short AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 ,
                                             String AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2 ,
                                             String AV68WWReferenciaTecnicaDS_9_Guia_nome2 ,
                                             bool AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 ,
                                             String AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3 ,
                                             short AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 ,
                                             String AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3 ,
                                             String AV73WWReferenciaTecnicaDS_14_Guia_nome3 ,
                                             int AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo ,
                                             int AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to ,
                                             String AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel ,
                                             String AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome ,
                                             String AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel ,
                                             String AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao ,
                                             int AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels_Count ,
                                             decimal AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor ,
                                             decimal AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to ,
                                             String AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel ,
                                             String AV83WWReferenciaTecnicaDS_24_Tfguia_nome ,
                                             String A98ReferenciaTecnica_Nome ,
                                             String A94Guia_Nome ,
                                             int A97ReferenciaTecnica_Codigo ,
                                             String A99ReferenciaTecnica_Descricao ,
                                             decimal A100ReferenciaTecnica_Valor )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [22] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Guia_Codigo], T1.[ReferenciaTecnica_Valor], T1.[ReferenciaTecnica_Unidade], T1.[ReferenciaTecnica_Descricao], T1.[ReferenciaTecnica_Codigo], T2.[Guia_Nome], T1.[ReferenciaTecnica_Nome] FROM ([ReferenciaTecnica] T1 WITH (NOLOCK) INNER JOIN [Guia] T2 WITH (NOLOCK) ON T2.[Guia_Codigo] = T1.[Guia_Codigo])";
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "REFERENCIATECNICA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV60WWReferenciaTecnicaDS_1_Dynamicfiltersselector1, "GUIA_NOME") == 0 ) && ( AV61WWReferenciaTecnicaDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWReferenciaTecnicaDS_4_Guia_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV63WWReferenciaTecnicaDS_4_Guia_nome1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "REFERENCIATECNICA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV64WWReferenciaTecnicaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV65WWReferenciaTecnicaDS_6_Dynamicfiltersselector2, "GUIA_NOME") == 0 ) && ( AV66WWReferenciaTecnicaDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWReferenciaTecnicaDS_9_Guia_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV68WWReferenciaTecnicaDS_9_Guia_nome2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "REFERENCIATECNICA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like '%' + @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like '%' + @lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV69WWReferenciaTecnicaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV70WWReferenciaTecnicaDS_11_Dynamicfiltersselector3, "GUIA_NOME") == 0 ) && ( AV71WWReferenciaTecnicaDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWReferenciaTecnicaDS_14_Guia_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like '%' + @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like '%' + @lV73WWReferenciaTecnicaDS_14_Guia_nome3)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] >= @AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] >= @AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (0==AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Codigo] <= @AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Codigo] <= @AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] like @lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] like @lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Nome] = @AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Nome] = @AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] like @lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] like @lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Descricao] = @AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Descricao] = @AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV80WWReferenciaTecnicaDS_21_Tfreferenciatecnica_unidade_sels, "T1.[ReferenciaTecnica_Unidade] IN (", ")") + ")";
            }
         }
         if ( ! (Convert.ToDecimal(0)==AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] >= @AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] >= @AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ReferenciaTecnica_Valor] <= @AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ReferenciaTecnica_Valor] <= @AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWReferenciaTecnicaDS_24_Tfguia_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] like @lV83WWReferenciaTecnicaDS_24_Tfguia_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] like @lV83WWReferenciaTecnicaDS_24_Tfguia_nome)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Guia_Nome] = @AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Guia_Nome] = @AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Guia_Codigo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00SN2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (decimal)dynConstraints[31] );
               case 1 :
                     return conditional_P00SN3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (decimal)dynConstraints[31] );
               case 2 :
                     return conditional_P00SN4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (decimal)dynConstraints[31] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SN2 ;
          prmP00SN2 = new Object[] {
          new Object[] {"@lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWReferenciaTecnicaDS_4_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWReferenciaTecnicaDS_4_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWReferenciaTecnicaDS_9_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWReferenciaTecnicaDS_9_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWReferenciaTecnicaDS_14_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWReferenciaTecnicaDS_14_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV83WWReferenciaTecnicaDS_24_Tfguia_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00SN3 ;
          prmP00SN3 = new Object[] {
          new Object[] {"@lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWReferenciaTecnicaDS_4_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWReferenciaTecnicaDS_4_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWReferenciaTecnicaDS_9_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWReferenciaTecnicaDS_9_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWReferenciaTecnicaDS_14_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWReferenciaTecnicaDS_14_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV83WWReferenciaTecnicaDS_24_Tfguia_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel",SqlDbType.Char,50,0}
          } ;
          Object[] prmP00SN4 ;
          prmP00SN4 = new Object[] {
          new Object[] {"@lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWReferenciaTecnicaDS_3_Referenciatecnica_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWReferenciaTecnicaDS_4_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV63WWReferenciaTecnicaDS_4_Guia_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWReferenciaTecnicaDS_8_Referenciatecnica_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWReferenciaTecnicaDS_9_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV68WWReferenciaTecnicaDS_9_Guia_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV72WWReferenciaTecnicaDS_13_Referenciatecnica_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWReferenciaTecnicaDS_14_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWReferenciaTecnicaDS_14_Guia_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV74WWReferenciaTecnicaDS_15_Tfreferenciatecnica_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV75WWReferenciaTecnicaDS_16_Tfreferenciatecnica_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV76WWReferenciaTecnicaDS_17_Tfreferenciatecnica_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV77WWReferenciaTecnicaDS_18_Tfreferenciatecnica_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV78WWReferenciaTecnicaDS_19_Tfreferenciatecnica_descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV79WWReferenciaTecnicaDS_20_Tfreferenciatecnica_descricao_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV81WWReferenciaTecnicaDS_22_Tfreferenciatecnica_valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV82WWReferenciaTecnicaDS_23_Tfreferenciatecnica_valor_to",SqlDbType.Decimal,18,5} ,
          new Object[] {"@lV83WWReferenciaTecnicaDS_24_Tfguia_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV84WWReferenciaTecnicaDS_25_Tfguia_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SN2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SN2,100,0,true,false )
             ,new CursorDef("P00SN3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SN3,100,0,true,false )
             ,new CursorDef("P00SN4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SN4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwreferenciatecnicafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwreferenciatecnicafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwreferenciatecnicafilterdata") )
          {
             return  ;
          }
          getwwreferenciatecnicafilterdata worker = new getwwreferenciatecnicafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
