/*
               File: type_SdtGAMAuthenticationGAMRemote
        Description: GAMAuthenticationGAMRemote
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:36:43.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMAuthenticationGAMRemote : GxUserType, IGxExternalObject
   {
      public SdtGAMAuthenticationGAMRemote( )
      {
         initialize();
      }

      public SdtGAMAuthenticationGAMRemote( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMAuthenticationGAMRemote_externalReference == null )
         {
            GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
         }
         returntostring = "";
         returntostring = (String)(GAMAuthenticationGAMRemote_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Clientid
      {
         get {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            return GAMAuthenticationGAMRemote_externalReference.ClientId ;
         }

         set {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            GAMAuthenticationGAMRemote_externalReference.ClientId = value;
         }

      }

      public String gxTpr_Clientsecret
      {
         get {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            return GAMAuthenticationGAMRemote_externalReference.ClientSecret ;
         }

         set {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            GAMAuthenticationGAMRemote_externalReference.ClientSecret = value;
         }

      }

      public String gxTpr_Siteurl
      {
         get {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            return GAMAuthenticationGAMRemote_externalReference.SiteURL ;
         }

         set {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            GAMAuthenticationGAMRemote_externalReference.SiteURL = value;
         }

      }

      public String gxTpr_Additionalscope
      {
         get {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            return GAMAuthenticationGAMRemote_externalReference.AdditionalScope ;
         }

         set {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            GAMAuthenticationGAMRemote_externalReference.AdditionalScope = value;
         }

      }

      public String gxTpr_Remoteserverurl
      {
         get {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            return GAMAuthenticationGAMRemote_externalReference.RemoteServerURL ;
         }

         set {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            GAMAuthenticationGAMRemote_externalReference.RemoteServerURL = value;
         }

      }

      public String gxTpr_Remoteserverkey
      {
         get {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            return GAMAuthenticationGAMRemote_externalReference.RemoteServerKey ;
         }

         set {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            GAMAuthenticationGAMRemote_externalReference.RemoteServerKey = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMAuthenticationGAMRemote_externalReference == null )
            {
               GAMAuthenticationGAMRemote_externalReference = new Artech.Security.GAMAuthenticationGAMRemote(context);
            }
            return GAMAuthenticationGAMRemote_externalReference ;
         }

         set {
            GAMAuthenticationGAMRemote_externalReference = (Artech.Security.GAMAuthenticationGAMRemote)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMAuthenticationGAMRemote GAMAuthenticationGAMRemote_externalReference=null ;
   }

}
