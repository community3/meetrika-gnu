/*
               File: AtributoRegraDeNegocio
        Description: Atributo Regra De Negocio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:38:49.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class atributoregradenegocio : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public atributoregradenegocio( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public atributoregradenegocio( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_FuncaoAPF_Codigo ,
                           ref int aP1_FuncaoAPFAtributos_AtributosCod )
      {
         this.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.A364FuncaoAPFAtributos_AtributosCod = aP1_FuncaoAPFAtributos_AtributosCod;
         executePrivate();
         aP0_FuncaoAPF_Codigo=this.A165FuncaoAPF_Codigo;
         aP1_FuncaoAPFAtributos_AtributosCod=this.A364FuncaoAPFAtributos_AtributosCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  A364FuncaoAPFAtributos_AtributosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA9U2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START9U2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117384940");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("atributoregradenegocio.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A364FuncaoAPFAtributos_AtributosCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMESSAGES", AV6Messages);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMESSAGES", AV6Messages);
         }
         GxWebStd.gx_hidden_field( context, "FUNCOESAPFATRIBUTOS_CODE", A383FuncoesAPFAtributos_Code);
         GxWebStd.gx_hidden_field( context, "FUNCOESAPFATRIBUTOS_NOME", StringUtil.RTrim( A384FuncoesAPFAtributos_Nome));
         GxWebStd.gx_hidden_field( context, "FUNCOESAPFATRIBUTOS_DESCRICAO", A385FuncoesAPFAtributos_Descricao);
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE9U2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT9U2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("atributoregradenegocio.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +A364FuncaoAPFAtributos_AtributosCod) ;
      }

      public override String GetPgmname( )
      {
         return "AtributoRegraDeNegocio" ;
      }

      public override String GetPgmdesc( )
      {
         return "Atributo Regra De Negocio" ;
      }

      protected void WB9U0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_9U2( true) ;
         }
         else
         {
            wb_table1_2_9U2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9U2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START9U2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Atributo Regra De Negocio", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP9U0( ) ;
      }

      protected void WS9U2( )
      {
         START9U2( ) ;
         EVT9U2( ) ;
      }

      protected void EVT9U2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E119U2 */
                              E119U2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E129U2 */
                                    E129U2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E139U2 */
                              E139U2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E149U2 */
                              E149U2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9U2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA9U2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavFuncoesapfatributos_code_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9U2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF9U2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E139U2 */
         E139U2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H009U2 */
            pr_default.execute(0, new Object[] {A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A383FuncoesAPFAtributos_Code = H009U2_A383FuncoesAPFAtributos_Code[0];
               n383FuncoesAPFAtributos_Code = H009U2_n383FuncoesAPFAtributos_Code[0];
               A384FuncoesAPFAtributos_Nome = H009U2_A384FuncoesAPFAtributos_Nome[0];
               n384FuncoesAPFAtributos_Nome = H009U2_n384FuncoesAPFAtributos_Nome[0];
               A385FuncoesAPFAtributos_Descricao = H009U2_A385FuncoesAPFAtributos_Descricao[0];
               n385FuncoesAPFAtributos_Descricao = H009U2_n385FuncoesAPFAtributos_Descricao[0];
               /* Execute user event: E149U2 */
               E149U2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB9U0( ) ;
         }
      }

      protected void STRUP9U0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Using cursor H009U3 */
         pr_default.execute(1, new Object[] {A364FuncaoAPFAtributos_AtributosCod});
         A365FuncaoAPFAtributos_AtributosNom = H009U3_A365FuncaoAPFAtributos_AtributosNom[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
         n365FuncaoAPFAtributos_AtributosNom = H009U3_n365FuncaoAPFAtributos_AtributosNom[0];
         pr_default.close(1);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E119U2 */
         E119U2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A365FuncaoAPFAtributos_AtributosNom = StringUtil.Upper( cgiGet( edtFuncaoAPFAtributos_AtributosNom_Internalname));
            n365FuncaoAPFAtributos_AtributosNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A365FuncaoAPFAtributos_AtributosNom", A365FuncaoAPFAtributos_AtributosNom);
            AV10FuncoesAPFAtributos_Code = cgiGet( edtavFuncoesapfatributos_code_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10FuncoesAPFAtributos_Code", AV10FuncoesAPFAtributos_Code);
            AV9FuncoesAPFAtributos_Nome = StringUtil.Upper( cgiGet( edtavFuncoesapfatributos_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9FuncoesAPFAtributos_Nome", AV9FuncoesAPFAtributos_Nome);
            AV8FuncoesAPFAtributos_Descricao = StringUtil.Upper( cgiGet( edtavFuncoesapfatributos_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncoesAPFAtributos_Descricao", AV8FuncoesAPFAtributos_Descricao);
            /* Read saved values. */
            Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
            Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
            Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
            Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
            Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
            Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
            Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
            Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
            Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
            Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E119U2 */
         E119U2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E119U2( )
      {
         /* Start Routine */
         edtFuncaoAPFAtributos_AtributosNom_Link = formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("DSP")) + "," + UrlEncode("" +A364FuncaoAPFAtributos_AtributosCod) + "," + UrlEncode("" +0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoAPFAtributos_AtributosNom_Internalname, "Link", edtFuncaoAPFAtributos_AtributosNom_Link);
      }

      public void GXEnter( )
      {
         /* Execute user event: E129U2 */
         E129U2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E129U2( )
      {
         /* Enter Routine */
         AV11FuncoesAPFAtributos.Load(A165FuncaoAPF_Codigo, A364FuncaoAPFAtributos_AtributosCod);
         AV11FuncoesAPFAtributos.gxTpr_Funcoesapfatributos_code = AV10FuncoesAPFAtributos_Code;
         AV11FuncoesAPFAtributos.gxTpr_Funcoesapfatributos_nome = AV9FuncoesAPFAtributos_Nome;
         AV11FuncoesAPFAtributos.gxTpr_Funcoesapfatributos_descricao = AV8FuncoesAPFAtributos_Descricao;
         AV11FuncoesAPFAtributos.Save();
         if ( AV11FuncoesAPFAtributos.Success() )
         {
            context.CommitDataStores( "AtributoRegraDeNegocio");
         }
         else
         {
            context.RollbackDataStores( "AtributoRegraDeNegocio");
            AV6Messages = AV11FuncoesAPFAtributos.GetMessages();
            /* Execute user subroutine: 'SHOW MESSAGES' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         context.setWebReturnParms(new Object[] {(int)A165FuncaoAPF_Codigo,(int)A364FuncaoAPFAtributos_AtributosCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6Messages", AV6Messages);
      }

      protected void S112( )
      {
         /* 'SHOW MESSAGES' Routine */
         AV14GXV1 = 1;
         while ( AV14GXV1 <= AV6Messages.Count )
         {
            AV5Message = ((SdtMessages_Message)AV6Messages.Item(AV14GXV1));
            GX_msglist.addItem(AV5Message.gxTpr_Description);
            AV14GXV1 = (int)(AV14GXV1+1);
         }
      }

      protected void E139U2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7Context) ;
         AV10FuncoesAPFAtributos_Code = A383FuncoesAPFAtributos_Code;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10FuncoesAPFAtributos_Code", AV10FuncoesAPFAtributos_Code);
         AV9FuncoesAPFAtributos_Nome = A384FuncoesAPFAtributos_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9FuncoesAPFAtributos_Nome", AV9FuncoesAPFAtributos_Nome);
         AV8FuncoesAPFAtributos_Descricao = A385FuncoesAPFAtributos_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8FuncoesAPFAtributos_Descricao", AV8FuncoesAPFAtributos_Descricao);
      }

      protected void nextLoad( )
      {
      }

      protected void E149U2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_9U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_9U2( true) ;
         }
         else
         {
            wb_table2_5_9U2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_9U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_36_9U2( true) ;
         }
         else
         {
            wb_table3_36_9U2( false) ;
         }
         return  ;
      }

      protected void wb_table3_36_9U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9U2e( true) ;
         }
         else
         {
            wb_table1_2_9U2e( false) ;
         }
      }

      protected void wb_table3_36_9U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_AtributoRegraDeNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_AtributoRegraDeNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_9U2e( true) ;
         }
         else
         {
            wb_table3_36_9U2e( false) ;
         }
      }

      protected void wb_table2_5_9U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_9U2( true) ;
         }
         else
         {
            wb_table4_13_9U2( false) ;
         }
         return  ;
      }

      protected void wb_table4_13_9U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_9U2e( true) ;
         }
         else
         {
            wb_table2_5_9U2e( false) ;
         }
      }

      protected void wb_table4_13_9U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapfatributos_atributosnom_Internalname, "Atributo", "", "", lblTextblockfuncaoapfatributos_atributosnom_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AtributoRegraDeNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPFAtributos_AtributosNom_Internalname, StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom), StringUtil.RTrim( context.localUtil.Format( A365FuncaoAPFAtributos_AtributosNom, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", edtFuncaoAPFAtributos_AtributosNom_Link, "", "", "", edtFuncaoAPFAtributos_AtributosNom_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_AtributoRegraDeNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncoesapfatributos_code_Internalname, "C�digo", "", "", lblTextblockfuncoesapfatributos_code_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AtributoRegraDeNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncoesapfatributos_code_Internalname, AV10FuncoesAPFAtributos_Code, StringUtil.RTrim( context.localUtil.Format( AV10FuncoesAPFAtributos_Code, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncoesapfatributos_code_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AtributoRegraDeNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncoesapfatributos_nome_Internalname, "Nome", "", "", lblTextblockfuncoesapfatributos_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AtributoRegraDeNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncoesapfatributos_nome_Internalname, StringUtil.RTrim( AV9FuncoesAPFAtributos_Nome), StringUtil.RTrim( context.localUtil.Format( AV9FuncoesAPFAtributos_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncoesapfatributos_nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AtributoRegraDeNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncoesapfatributos_descricao_Internalname, "Descri��o", "", "", lblTextblockfuncoesapfatributos_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AtributoRegraDeNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncoesapfatributos_descricao_Internalname, AV8FuncoesAPFAtributos_Descricao, StringUtil.RTrim( context.localUtil.Format( AV8FuncoesAPFAtributos_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncoesapfatributos_descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AtributoRegraDeNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_9U2e( true) ;
         }
         else
         {
            wb_table4_13_9U2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A165FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
         A364FuncaoAPFAtributos_AtributosCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A364FuncaoAPFAtributos_AtributosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9U2( ) ;
         WS9U2( ) ;
         WE9U2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117384984");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("atributoregradenegocio.js", "?20203117384984");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockfuncaoapfatributos_atributosnom_Internalname = "TEXTBLOCKFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         edtFuncaoAPFAtributos_AtributosNom_Internalname = "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         lblTextblockfuncoesapfatributos_code_Internalname = "TEXTBLOCKFUNCOESAPFATRIBUTOS_CODE";
         edtavFuncoesapfatributos_code_Internalname = "vFUNCOESAPFATRIBUTOS_CODE";
         lblTextblockfuncoesapfatributos_nome_Internalname = "TEXTBLOCKFUNCOESAPFATRIBUTOS_NOME";
         edtavFuncoesapfatributos_nome_Internalname = "vFUNCOESAPFATRIBUTOS_NOME";
         lblTextblockfuncoesapfatributos_descricao_Internalname = "TEXTBLOCKFUNCOESAPFATRIBUTOS_DESCRICAO";
         edtavFuncoesapfatributos_descricao_Internalname = "vFUNCOESAPFATRIBUTOS_DESCRICAO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavFuncoesapfatributos_descricao_Jsonclick = "";
         edtavFuncoesapfatributos_nome_Jsonclick = "";
         edtavFuncoesapfatributos_code_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosNom_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosNom_Link = "";
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Regra de Neg�cio";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Atributo Regra De Negocio";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'A383FuncoesAPFAtributos_Code',fld:'FUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'A384FuncoesAPFAtributos_Nome',fld:'FUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'A385FuncoesAPFAtributos_Descricao',fld:'FUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''}],oparms:[{av:'AV10FuncoesAPFAtributos_Code',fld:'vFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV9FuncoesAPFAtributos_Nome',fld:'vFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV8FuncoesAPFAtributos_Descricao',fld:'vFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''}]}");
         setEventMetadata("ENTER","{handler:'E129U2',iparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV10FuncoesAPFAtributos_Code',fld:'vFUNCOESAPFATRIBUTOS_CODE',pic:'',nv:''},{av:'AV9FuncoesAPFAtributos_Nome',fld:'vFUNCOESAPFATRIBUTOS_NOME',pic:'@!',nv:''},{av:'AV8FuncoesAPFAtributos_Descricao',fld:'vFUNCOESAPFATRIBUTOS_DESCRICAO',pic:'@!',nv:''},{av:'AV6Messages',fld:'vMESSAGES',pic:'',nv:null}],oparms:[{av:'AV6Messages',fld:'vMESSAGES',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV6Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         A383FuncoesAPFAtributos_Code = "";
         A384FuncoesAPFAtributos_Nome = "";
         A385FuncoesAPFAtributos_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H009U2_A165FuncaoAPF_Codigo = new int[1] ;
         H009U2_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         H009U2_A383FuncoesAPFAtributos_Code = new String[] {""} ;
         H009U2_n383FuncoesAPFAtributos_Code = new bool[] {false} ;
         H009U2_A384FuncoesAPFAtributos_Nome = new String[] {""} ;
         H009U2_n384FuncoesAPFAtributos_Nome = new bool[] {false} ;
         H009U2_A385FuncoesAPFAtributos_Descricao = new String[] {""} ;
         H009U2_n385FuncoesAPFAtributos_Descricao = new bool[] {false} ;
         H009U2_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         H009U2_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         H009U3_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         H009U3_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         A365FuncaoAPFAtributos_AtributosNom = "";
         AV10FuncoesAPFAtributos_Code = "";
         AV9FuncoesAPFAtributos_Nome = "";
         AV8FuncoesAPFAtributos_Descricao = "";
         AV11FuncoesAPFAtributos = new SdtFuncoesAPFAtributos(context);
         AV5Message = new SdtMessages_Message(context);
         AV7Context = new wwpbaseobjects.SdtWWPContext(context);
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtnenter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblTextblockfuncaoapfatributos_atributosnom_Jsonclick = "";
         lblTextblockfuncoesapfatributos_code_Jsonclick = "";
         lblTextblockfuncoesapfatributos_nome_Jsonclick = "";
         lblTextblockfuncoesapfatributos_descricao_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.atributoregradenegocio__default(),
            new Object[][] {
                new Object[] {
               H009U2_A165FuncaoAPF_Codigo, H009U2_A364FuncaoAPFAtributos_AtributosCod, H009U2_A383FuncoesAPFAtributos_Code, H009U2_n383FuncoesAPFAtributos_Code, H009U2_A384FuncoesAPFAtributos_Nome, H009U2_n384FuncoesAPFAtributos_Nome, H009U2_A385FuncoesAPFAtributos_Descricao, H009U2_n385FuncoesAPFAtributos_Descricao, H009U2_A365FuncaoAPFAtributos_AtributosNom, H009U2_n365FuncaoAPFAtributos_AtributosNom
               }
               , new Object[] {
               H009U3_A365FuncaoAPFAtributos_AtributosNom, H009U3_n365FuncaoAPFAtributos_AtributosNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int wcpOA165FuncaoAPF_Codigo ;
      private int wcpOA364FuncaoAPFAtributos_AtributosCod ;
      private int AV14GXV1 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A384FuncoesAPFAtributos_Nome ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavFuncoesapfatributos_code_Internalname ;
      private String scmdbuf ;
      private String A365FuncaoAPFAtributos_AtributosNom ;
      private String edtFuncaoAPFAtributos_AtributosNom_Internalname ;
      private String AV9FuncoesAPFAtributos_Nome ;
      private String edtavFuncoesapfatributos_nome_Internalname ;
      private String edtavFuncoesapfatributos_descricao_Internalname ;
      private String edtFuncaoAPFAtributos_AtributosNom_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnenter_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockfuncaoapfatributos_atributosnom_Internalname ;
      private String lblTextblockfuncaoapfatributos_atributosnom_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtributosNom_Jsonclick ;
      private String lblTextblockfuncoesapfatributos_code_Internalname ;
      private String lblTextblockfuncoesapfatributos_code_Jsonclick ;
      private String edtavFuncoesapfatributos_code_Jsonclick ;
      private String lblTextblockfuncoesapfatributos_nome_Internalname ;
      private String lblTextblockfuncoesapfatributos_nome_Jsonclick ;
      private String edtavFuncoesapfatributos_nome_Jsonclick ;
      private String lblTextblockfuncoesapfatributos_descricao_Internalname ;
      private String lblTextblockfuncoesapfatributos_descricao_Jsonclick ;
      private String edtavFuncoesapfatributos_descricao_Jsonclick ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n383FuncoesAPFAtributos_Code ;
      private bool n384FuncoesAPFAtributos_Nome ;
      private bool n385FuncoesAPFAtributos_Descricao ;
      private bool n365FuncaoAPFAtributos_AtributosNom ;
      private bool returnInSub ;
      private String A383FuncoesAPFAtributos_Code ;
      private String A385FuncoesAPFAtributos_Descricao ;
      private String AV10FuncoesAPFAtributos_Code ;
      private String AV8FuncoesAPFAtributos_Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_FuncaoAPF_Codigo ;
      private int aP1_FuncaoAPFAtributos_AtributosCod ;
      private IDataStoreProvider pr_default ;
      private int[] H009U2_A165FuncaoAPF_Codigo ;
      private int[] H009U2_A364FuncaoAPFAtributos_AtributosCod ;
      private String[] H009U2_A383FuncoesAPFAtributos_Code ;
      private bool[] H009U2_n383FuncoesAPFAtributos_Code ;
      private String[] H009U2_A384FuncoesAPFAtributos_Nome ;
      private bool[] H009U2_n384FuncoesAPFAtributos_Nome ;
      private String[] H009U2_A385FuncoesAPFAtributos_Descricao ;
      private bool[] H009U2_n385FuncoesAPFAtributos_Descricao ;
      private String[] H009U2_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] H009U2_n365FuncaoAPFAtributos_AtributosNom ;
      private String[] H009U3_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] H009U3_n365FuncaoAPFAtributos_AtributosNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV6Messages ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV7Context ;
      private SdtFuncoesAPFAtributos AV11FuncoesAPFAtributos ;
      private SdtMessages_Message AV5Message ;
   }

   public class atributoregradenegocio__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009U2 ;
          prmH009U2 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009U3 ;
          prmH009U3 = new Object[] {
          new Object[] {"@FuncaoAPFAtributos_AtributosCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009U2", "SELECT T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T1.[FuncoesAPFAtributos_Code], T1.[FuncoesAPFAtributos_Nome], T1.[FuncoesAPFAtributos_Descricao], T2.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom FROM ([FuncoesAPFAtributos] T1 WITH (NOLOCK) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) WHERE T1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo and T1.[FuncaoAPFAtributos_AtributosCod] = @FuncaoAPFAtributos_AtributosCod ORDER BY T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFAtributos_AtributosCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009U2,1,0,true,true )
             ,new CursorDef("H009U3", "SELECT [Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_Codigo] = @FuncaoAPFAtributos_AtributosCod ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009U3,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 50) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
