/*
               File: WP_NovoUsuario
        Description: Novo Usuario - GAM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:38:41.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_novousuario : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_novousuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_novousuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratante_Codigo ,
                           int aP1_Contratada_Codigo )
      {
         this.AV46Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV44Contratada_Codigo = aP1_Contratada_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavGender = new GXCombobox();
         chkavDisponivel = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV46Contratante_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Contratante_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV46Contratante_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV44Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Contratada_Codigo), 6, 0)));
               }
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA8F2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START8F2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311739354");
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_novousuario.aspx") + "?" + UrlEncode("" +AV46Contratante_Codigo) + "," + UrlEncode("" +AV44Contratada_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44Contratada_Codigo), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE8F2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT8F2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_novousuario.aspx") + "?" + UrlEncode("" +AV46Contratante_Codigo) + "," + UrlEncode("" +AV44Contratada_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_NovoUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Novo Usuario - GAM" ;
      }

      protected void WB8F0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_8F2( true) ;
         }
         else
         {
            wb_table1_2_8F2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_8F2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START8F2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Novo Usuario - GAM", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP8F0( ) ;
      }

      protected void WS8F2( )
      {
         START8F2( ) ;
         EVT8F2( ) ;
      }

      protected void EVT8F2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E118F2 */
                              E118F2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VPESSOA_DOCTO.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E128F2 */
                              E128F2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E138F2 */
                                    E138F2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VFIRSTNAME.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E148F2 */
                              E148F2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VLASTNAME.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E158F2 */
                              E158F2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VNAME.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E168F2 */
                              E168F2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CONFIRMAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E178F2 */
                              E178F2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E188F2 */
                              E188F2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE8F2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA8F2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavGender.Name = "vGENDER";
            cmbavGender.WebTags = "";
            cmbavGender.addItem("N", "Not Specified", 0);
            cmbavGender.addItem("F", "Female", 0);
            cmbavGender.addItem("M", "Male", 0);
            if ( cmbavGender.ItemCount > 0 )
            {
               AV11Gender = cmbavGender.getValidValue(AV11Gender);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Gender", AV11Gender);
            }
            chkavDisponivel.Name = "vDISPONIVEL";
            chkavDisponivel.WebTags = "";
            chkavDisponivel.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDisponivel_Internalname, "TitleCaption", chkavDisponivel.Caption);
            chkavDisponivel.CheckedValue = "false";
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavPessoa_docto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavGender.ItemCount > 0 )
         {
            AV11Gender = cmbavGender.getValidValue(AV11Gender);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Gender", AV11Gender);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8F2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         chkavDisponivel.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDisponivel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDisponivel.Enabled), 5, 0)));
      }

      protected void RF8F2( )
      {
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E188F2 */
            E188F2 ();
            WB8F0( ) ;
         }
      }

      protected void STRUP8F0( )
      {
         /* Before Start, stand alone formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         chkavDisponivel.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDisponivel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDisponivel.Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E118F2 */
         E118F2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV35ReqCPF = cgiGet( imgavReqcpf_Internalname);
            AV22Pessoa_Docto = cgiGet( edtavPessoa_docto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pessoa_Docto", AV22Pessoa_Docto);
            AV26ReqFirstName = cgiGet( imgavReqfirstname_Internalname);
            AV10FirstName = cgiGet( edtavFirstname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10FirstName", AV10FirstName);
            AV29ReqLastName = cgiGet( imgavReqlastname_Internalname);
            AV13LastName = cgiGet( edtavLastname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13LastName", AV13LastName);
            AV24ReqBirthday = cgiGet( imgavReqbirthday_Internalname);
            if ( context.localUtil.VCDate( cgiGet( edtavBirthday_Internalname), 2) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Birthday"}), 1, "vBIRTHDAY");
               GX_FocusControl = edtavBirthday_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6Birthday = DateTime.MinValue;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Birthday", context.localUtil.Format(AV6Birthday, "99/99/9999"));
            }
            else
            {
               AV6Birthday = context.localUtil.CToD( cgiGet( edtavBirthday_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Birthday", context.localUtil.Format(AV6Birthday, "99/99/9999"));
            }
            AV27ReqGender = cgiGet( imgavReqgender_Internalname);
            cmbavGender.CurrentValue = cgiGet( cmbavGender_Internalname);
            AV11Gender = cgiGet( cmbavGender_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Gender", AV11Gender);
            AV25ReqEmail = cgiGet( imgavReqemail_Internalname);
            AV7EMail = cgiGet( edtavEmail_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7EMail", AV7EMail);
            AV70Pessoa_Telefone = cgiGet( edtavPessoa_telefone_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70Pessoa_Telefone", AV70Pessoa_Telefone);
            AV30ReqName = cgiGet( imgavReqname_Internalname);
            AV17Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Name", AV17Name);
            AV31ReqPassword = cgiGet( imgavReqpassword_Internalname);
            AV18Password = cgiGet( edtavPassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Password", AV18Password);
            AV31ReqPassword = cgiGet( imgavReqpassword_Internalname);
            AV19PasswordConf = cgiGet( edtavPasswordconf_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19PasswordConf", AV19PasswordConf);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPerfil_gamid_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPerfil_gamid_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPERFIL_GAMID");
               GX_FocusControl = edtavPerfil_gamid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40Perfil_GamID = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Perfil_GamID", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Perfil_GamID), 12, 0)));
            }
            else
            {
               AV40Perfil_GamID = (long)(context.localUtil.CToN( cgiGet( edtavPerfil_gamid_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Perfil_GamID", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Perfil_GamID), 12, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPerfil_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPerfil_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPERFIL_CODIGO");
               GX_FocusControl = edtavPerfil_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38Perfil_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Perfil_Codigo), 6, 0)));
            }
            else
            {
               AV38Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavPerfil_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Perfil_Codigo), 6, 0)));
            }
            AV28ReqIcon = cgiGet( imgavReqicon_Internalname);
            AV48Disponivel = StringUtil.StrToBool( cgiGet( chkavDisponivel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Disponivel", AV48Disponivel);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E118F2 */
         E118F2 ();
         if (returnInSub) return;
      }

      protected void E118F2( )
      {
         /* Start Routine */
         Form.Meta.addItem("Versao", "1.2 - Data: 06/02/2020 21:00", 0) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV51WWPContext) ;
         AV39AreaTrabalho_Codigo = AV51WWPContext.gxTpr_Areatrabalho_codigo;
         new prc_retornacodigoperfilusuarioareatrabalho(context ).execute(  AV39AreaTrabalho_Codigo, out  AV38Perfil_Codigo, out  AV40Perfil_GamID) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38Perfil_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Perfil_GamID", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40Perfil_GamID), 12, 0)));
         edtavPerfil_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_codigo_Visible), 5, 0)));
         edtavPerfil_gamid_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_gamid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_gamid_Visible), 5, 0)));
         AV23Repository = new SdtGAMRepository(context).get();
         AV30ReqName = context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30ReqName)) ? AV74Reqname_GXI : context.convertURL( context.PathToRelativeUrl( AV30ReqName))));
         AV74Reqname_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV30ReqName)) ? AV74Reqname_GXI : context.convertURL( context.PathToRelativeUrl( AV30ReqName))));
         if ( AV23Repository.gxTpr_Requiredemail )
         {
            AV25ReqEmail = context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25ReqEmail)) ? AV75Reqemail_GXI : context.convertURL( context.PathToRelativeUrl( AV25ReqEmail))));
            AV75Reqemail_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV25ReqEmail)) ? AV75Reqemail_GXI : context.convertURL( context.PathToRelativeUrl( AV25ReqEmail))));
         }
         else
         {
            imgavReqemail_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqemail_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqemail_Visible), 5, 0)));
         }
         AV26ReqFirstName = context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqfirstname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqFirstName)) ? AV76Reqfirstname_GXI : context.convertURL( context.PathToRelativeUrl( AV26ReqFirstName))));
         AV76Reqfirstname_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqfirstname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqFirstName)) ? AV76Reqfirstname_GXI : context.convertURL( context.PathToRelativeUrl( AV26ReqFirstName))));
         AV29ReqLastName = context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqlastname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29ReqLastName)) ? AV77Reqlastname_GXI : context.convertURL( context.PathToRelativeUrl( AV29ReqLastName))));
         AV77Reqlastname_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqlastname_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29ReqLastName)) ? AV77Reqlastname_GXI : context.convertURL( context.PathToRelativeUrl( AV29ReqLastName))));
         if ( AV23Repository.gxTpr_Requiredbirthday )
         {
            AV24ReqBirthday = context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV24ReqBirthday)) ? AV78Reqbirthday_GXI : context.convertURL( context.PathToRelativeUrl( AV24ReqBirthday))));
            AV78Reqbirthday_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV24ReqBirthday)) ? AV78Reqbirthday_GXI : context.convertURL( context.PathToRelativeUrl( AV24ReqBirthday))));
         }
         else
         {
            imgavReqbirthday_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqbirthday_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqbirthday_Visible), 5, 0)));
         }
         if ( AV23Repository.gxTpr_Requiredgender )
         {
            AV27ReqGender = context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqGender)) ? AV79Reqgender_GXI : context.convertURL( context.PathToRelativeUrl( AV27ReqGender))));
            AV79Reqgender_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqGender)) ? AV79Reqgender_GXI : context.convertURL( context.PathToRelativeUrl( AV27ReqGender))));
         }
         else
         {
            imgavReqgender_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqgender_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqgender_Visible), 5, 0)));
         }
         if ( AV23Repository.gxTpr_Requiredpassword )
         {
            AV31ReqPassword = context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqpassword_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqPassword)) ? AV80Reqpassword_GXI : context.convertURL( context.PathToRelativeUrl( AV31ReqPassword))));
            AV80Reqpassword_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqpassword_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqPassword)) ? AV80Reqpassword_GXI : context.convertURL( context.PathToRelativeUrl( AV31ReqPassword))));
         }
         else
         {
            imgavReqpassword_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqpassword_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavReqpassword_Visible), 5, 0)));
         }
         AV28ReqIcon = context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqicon_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqIcon)) ? AV81Reqicon_GXI : context.convertURL( context.PathToRelativeUrl( AV28ReqIcon))));
         AV81Reqicon_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqicon_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqIcon)) ? AV81Reqicon_GXI : context.convertURL( context.PathToRelativeUrl( AV28ReqIcon))));
         lblTbinficonreq_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbinficonreq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbinficonreq_Visible), 5, 0)));
         AV35ReqCPF = context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqcpf_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35ReqCPF)) ? AV82Reqcpf_GXI : context.convertURL( context.PathToRelativeUrl( AV35ReqCPF))));
         AV82Reqcpf_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "fd3a5f9a-446b-463b-8a30-7ca3ac4d36a3", "", context.GetTheme( )));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavReqcpf_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35ReqCPF)) ? AV82Reqcpf_GXI : context.convertURL( context.PathToRelativeUrl( AV35ReqCPF))));
         if ( (0==AV44Contratada_Codigo) )
         {
            lblTbformtitle_Caption = "Novo Usu�rio do Contratante ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbformtitle_Internalname, "Caption", lblTbformtitle_Caption);
         }
         else
         {
            lblTbformtitle_Caption = "Novo Usu�rio da Contratada";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbformtitle_Internalname, "Caption", lblTbformtitle_Caption);
         }
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         chkavDisponivel.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDisponivel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDisponivel.Visible), 5, 0)));
         AV49UsuarioLogado.load( new SdtGAMUser(context).getid());
      }

      protected void E128F2( )
      {
         /* Pessoa_docto_Isvalid Routine */
         AV22Pessoa_Docto = StringUtil.StringReplace( AV22Pessoa_Docto, ".", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pessoa_Docto", AV22Pessoa_Docto);
         AV22Pessoa_Docto = StringUtil.StringReplace( AV22Pessoa_Docto, "-", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pessoa_Docto", AV22Pessoa_Docto);
         AV22Pessoa_Docto = StringUtil.StringReplace( AV22Pessoa_Docto, ",", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pessoa_Docto", AV22Pessoa_Docto);
         AV22Pessoa_Docto = StringUtil.StringReplace( AV22Pessoa_Docto, "/", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pessoa_Docto", AV22Pessoa_Docto);
         AV22Pessoa_Docto = StringUtil.StringReplace( AV22Pessoa_Docto, "\\", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pessoa_Docto", AV22Pessoa_Docto);
         AV22Pessoa_Docto = StringUtil.StringReplace( AV22Pessoa_Docto, " ", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Pessoa_Docto", AV22Pessoa_Docto);
         if ( new prc_validadocumento(context).executeUdp(  AV22Pessoa_Docto,  "F") )
         {
            GX_msglist.addItem("N�mero de Documento inv�lido!");
            GX_FocusControl = edtavPessoa_docto_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         AV21Pessoa_Codigo = 0;
         AV7EMail = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7EMail", AV7EMail);
         AV10FirstName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10FirstName", AV10FirstName);
         AV13LastName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13LastName", AV13LastName);
         AV6Birthday = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Birthday", context.localUtil.Format(AV6Birthday, "99/99/9999"));
         AV11Gender = "N";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Gender", AV11Gender);
         AV48Disponivel = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Disponivel", AV48Disponivel);
         AV83GXLvl83 = 0;
         /* Using cursor H008F2 */
         pr_default.execute(0, new Object[] {AV22Pessoa_Docto});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A34Pessoa_Codigo = H008F2_A34Pessoa_Codigo[0];
            A36Pessoa_TipoPessoa = H008F2_A36Pessoa_TipoPessoa[0];
            A37Pessoa_Docto = H008F2_A37Pessoa_Docto[0];
            AV83GXLvl83 = 1;
            if ( (0==AV44Contratada_Codigo) )
            {
               /* Using cursor H008F3 */
               pr_default.execute(1, new Object[] {A34Pessoa_Codigo, AV51WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A66ContratadaUsuario_ContratadaCod = H008F3_A66ContratadaUsuario_ContratadaCod[0];
                  A69ContratadaUsuario_UsuarioCod = H008F3_A69ContratadaUsuario_UsuarioCod[0];
                  A516Contratada_TipoFabrica = H008F3_A516Contratada_TipoFabrica[0];
                  n516Contratada_TipoFabrica = H008F3_n516Contratada_TipoFabrica[0];
                  A1228ContratadaUsuario_AreaTrabalhoCod = H008F3_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                  n1228ContratadaUsuario_AreaTrabalhoCod = H008F3_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                  A70ContratadaUsuario_UsuarioPessoaCod = H008F3_A70ContratadaUsuario_UsuarioPessoaCod[0];
                  n70ContratadaUsuario_UsuarioPessoaCod = H008F3_n70ContratadaUsuario_UsuarioPessoaCod[0];
                  A516Contratada_TipoFabrica = H008F3_A516Contratada_TipoFabrica[0];
                  n516Contratada_TipoFabrica = H008F3_n516Contratada_TipoFabrica[0];
                  A1228ContratadaUsuario_AreaTrabalhoCod = H008F3_A1228ContratadaUsuario_AreaTrabalhoCod[0];
                  n1228ContratadaUsuario_AreaTrabalhoCod = H008F3_n1228ContratadaUsuario_AreaTrabalhoCod[0];
                  A70ContratadaUsuario_UsuarioPessoaCod = H008F3_A70ContratadaUsuario_UsuarioPessoaCod[0];
                  n70ContratadaUsuario_UsuarioPessoaCod = H008F3_n70ContratadaUsuario_UsuarioPessoaCod[0];
                  GX_msglist.addItem("Usu�rio n�o dispon�vel para Pessoas envolvidas em alguma Contratada!");
                  AV48Disponivel = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Disponivel", AV48Disponivel);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
            }
            else
            {
               /* Using cursor H008F4 */
               pr_default.execute(2, new Object[] {AV44Contratada_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A52Contratada_AreaTrabalhoCod = H008F4_A52Contratada_AreaTrabalhoCod[0];
                  A29Contratante_Codigo = H008F4_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = H008F4_n29Contratante_Codigo[0];
                  A516Contratada_TipoFabrica = H008F4_A516Contratada_TipoFabrica[0];
                  n516Contratada_TipoFabrica = H008F4_n516Contratada_TipoFabrica[0];
                  A39Contratada_Codigo = H008F4_A39Contratada_Codigo[0];
                  A29Contratante_Codigo = H008F4_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = H008F4_n29Contratante_Codigo[0];
                  /* Using cursor H008F6 */
                  pr_default.execute(3, new Object[] {n29Contratante_Codigo, A29Contratante_Codigo, A34Pessoa_Codigo, AV51WWPContext.gxTpr_Areatrabalho_codigo});
                  while ( (pr_default.getStatus(3) != 101) )
                  {
                     A60ContratanteUsuario_UsuarioCod = H008F6_A60ContratanteUsuario_UsuarioCod[0];
                     A63ContratanteUsuario_ContratanteCod = H008F6_A63ContratanteUsuario_ContratanteCod[0];
                     A61ContratanteUsuario_UsuarioPessoaCod = H008F6_A61ContratanteUsuario_UsuarioPessoaCod[0];
                     n61ContratanteUsuario_UsuarioPessoaCod = H008F6_n61ContratanteUsuario_UsuarioPessoaCod[0];
                     A1020ContratanteUsuario_AreaTrabalhoCod = H008F6_A1020ContratanteUsuario_AreaTrabalhoCod[0];
                     n1020ContratanteUsuario_AreaTrabalhoCod = H008F6_n1020ContratanteUsuario_AreaTrabalhoCod[0];
                     A61ContratanteUsuario_UsuarioPessoaCod = H008F6_A61ContratanteUsuario_UsuarioPessoaCod[0];
                     n61ContratanteUsuario_UsuarioPessoaCod = H008F6_n61ContratanteUsuario_UsuarioPessoaCod[0];
                     A1020ContratanteUsuario_AreaTrabalhoCod = H008F6_A1020ContratanteUsuario_AreaTrabalhoCod[0];
                     n1020ContratanteUsuario_AreaTrabalhoCod = H008F6_n1020ContratanteUsuario_AreaTrabalhoCod[0];
                     GX_msglist.addItem("Usu�rio n�o dispon�vel para Pessoas envolvidas na Contratante!");
                     AV48Disponivel = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48Disponivel", AV48Disponivel);
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(3);
                  }
                  pr_default.close(3);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
            }
            if ( AV48Disponivel )
            {
               AV50WebSession.Set("Pessoa_Codigo", StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0));
               AV20Pessoa.Load(A34Pessoa_Codigo);
               /* Using cursor H008F7 */
               pr_default.execute(4, new Object[] {A34Pessoa_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A57Usuario_PessoaCod = H008F7_A57Usuario_PessoaCod[0];
                  A341Usuario_UserGamGuid = H008F7_A341Usuario_UserGamGuid[0];
                  AV32User.load( A341Usuario_UserGamGuid);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A341Usuario_UserGamGuid", A341Usuario_UserGamGuid);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(4);
               }
               pr_default.close(4);
               AV7EMail = AV32User.gxTpr_Email;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7EMail", AV7EMail);
               AV10FirstName = AV32User.gxTpr_Firstname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10FirstName", AV10FirstName);
               AV13LastName = AV32User.gxTpr_Lastname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13LastName", AV13LastName);
               AV6Birthday = AV32User.gxTpr_Birthday;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Birthday", context.localUtil.Format(AV6Birthday, "99/99/9999"));
               AV11Gender = AV32User.gxTpr_Gender;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Gender", AV11Gender);
               edtavEmail_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_Enabled), 5, 0)));
               edtavFirstname_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFirstname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFirstname_Enabled), 5, 0)));
               edtavLastname_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLastname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLastname_Enabled), 5, 0)));
               edtavBirthday_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBirthday_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBirthday_Enabled), 5, 0)));
               cmbavGender.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGender_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavGender.Enabled), 5, 0)));
               /* Execute user subroutine: 'MONTAUSERNAME' */
               S113 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  returnInSub = true;
                  if (true) return;
               }
               edtavEmail_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_Enabled), 5, 0)));
               edtavFirstname_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFirstname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFirstname_Enabled), 5, 0)));
               edtavLastname_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLastname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLastname_Enabled), 5, 0)));
               edtavBirthday_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBirthday_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBirthday_Enabled), 5, 0)));
               cmbavGender.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGender_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavGender.Enabled), 5, 0)));
               edtavFirstname_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFirstname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFirstname_Enabled), 5, 0)));
               edtavLastname_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLastname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLastname_Enabled), 5, 0)));
               edtavBirthday_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBirthday_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBirthday_Enabled), 5, 0)));
            }
            else
            {
               GX_FocusControl = edtavPessoa_docto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               context.DoAjaxSetFocus(GX_FocusControl);
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( AV83GXLvl83 == 0 )
         {
            edtavEmail_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmail_Enabled), 5, 0)));
            edtavFirstname_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFirstname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFirstname_Enabled), 5, 0)));
            edtavLastname_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLastname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLastname_Enabled), 5, 0)));
            edtavBirthday_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBirthday_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavBirthday_Enabled), 5, 0)));
            cmbavGender.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGender_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavGender.Enabled), 5, 0)));
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E138F2 */
         E138F2 ();
         if (returnInSub) return;
      }

      protected void E138F2( )
      {
         /* Enter Routine */
         GX_FocusControl = edtavFirstname_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         context.DoAjaxSetFocus(GX_FocusControl);
      }

      protected void E148F2( )
      {
         /* Firstname_Isvalid Routine */
         /* Execute user subroutine: 'MONTAUSERNAME' */
         S113 ();
         if (returnInSub) return;
      }

      protected void E158F2( )
      {
         /* Lastname_Isvalid Routine */
         /* Execute user subroutine: 'MONTAUSERNAME' */
         S113 ();
         if (returnInSub) return;
      }

      protected void E168F2( )
      {
         /* Name_Isvalid Routine */
         GXt_char1 = AV17Name;
         new prc_padronizastring(context ).execute(  AV17Name, out  GXt_char1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Name", AV17Name);
         AV17Name = StringUtil.Lower( GXt_char1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Name", AV17Name);
      }

      protected void S113( )
      {
         /* 'MONTAUSERNAME' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13LastName)) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV10FirstName)) )
         {
            AV52i = (short)(StringUtil.StringSearch( AV10FirstName, " ", 1)-1);
            if ( AV52i <= 0 )
            {
               AV52i = (short)(StringUtil.Len( AV10FirstName));
            }
            AV53d = (short)(StringUtil.StringSearchRev( AV13LastName, " ", -1)+1);
            AV17Name = StringUtil.Substring( AV10FirstName, 1, AV52i) + "." + StringUtil.Substring( AV13LastName, AV53d, StringUtil.Len( AV13LastName));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Name", AV17Name);
            GXt_char1 = AV17Name;
            new prc_padronizastring(context ).execute(  AV17Name, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Name", AV17Name);
            AV17Name = StringUtil.Lower( GXt_char1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Name", AV17Name);
            AV56pattern = "[^0-9a-zA-Z\\.]+";
            AV17Name = GxRegex.Replace(AV17Name,AV56pattern,"");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Name", AV17Name);
         }
      }

      protected void E178F2( )
      {
         /* 'Confirmar' Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV22Pessoa_Docto)) )
         {
            GX_msglist.addItem("CPF da pessoa � obrigat�rio!");
         }
         else if ( ! AV48Disponivel )
         {
            GX_msglist.addItem("A Pessoa esta envolvida em outra entidade desta �rea de Trabalho!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10FirstName)) || String.IsNullOrEmpty(StringUtil.RTrim( AV13LastName)) )
         {
            GX_msglist.addItem("O Nome e Sobrenome da pessoa s�o obrigat�rios!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV7EMail)) )
         {
            GX_msglist.addItem("E-Mail � obrigat�rio!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17Name)) )
         {
            GX_msglist.addItem("O Nome de Usu�rio � obrigat�rio!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV18Password)) )
         {
            GX_msglist.addItem("A Senha � obrigat�ria!");
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19PasswordConf)) )
         {
            GX_msglist.addItem("A confirma��o da Senha � obrigat�ria!");
         }
         else if ( StringUtil.StrCmp(AV18Password, AV19PasswordConf) != 0 )
         {
            GX_msglist.addItem("A senha e a confirma��o de senha n�o batem!");
         }
         else
         {
            GXt_char1 = AV17Name;
            new prc_padronizastring(context ).execute(  AV17Name, out  GXt_char1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Name", AV17Name);
            AV17Name = StringUtil.Lower( GXt_char1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Name", AV17Name);
            AV32User = new SdtGAMUser(context);
            AV32User.gxTpr_Name = AV17Name;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32User", AV32User);
            AV32User.gxTpr_Email = AV7EMail;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32User", AV32User);
            AV32User.gxTpr_Firstname = AV10FirstName;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32User", AV32User);
            AV32User.gxTpr_Lastname = AV13LastName;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32User", AV32User);
            AV32User.gxTpr_Birthday = AV6Birthday;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32User", AV32User);
            AV32User.gxTpr_Gender = AV11Gender;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32User", AV32User);
            AV32User.gxTpr_Password = AV18Password;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32User", AV32User);
            AV32User.gxTpr_Mustchangepassword = true;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32User", AV32User);
            AV32User.save();
            if ( AV32User.success() )
            {
               if ( AV40Perfil_GamID > 0 )
               {
                  AV12isOK = AV32User.addrolebyid(AV40Perfil_GamID, out  AV9Errors);
                  if ( ! AV12isOK )
                  {
                     AV9Errors = AV32User.geterrors();
                     /* Execute user subroutine: 'DISPLAYMESSAGES' */
                     S122 ();
                     if (returnInSub) return;
                  }
               }
               AV21Pessoa_Codigo = (int)(NumberUtil.Val( AV50WebSession.Get("Pessoa_Codigo"), "."));
               if ( (0==AV21Pessoa_Codigo) )
               {
                  AV20Pessoa = new SdtPessoa(context);
                  AV20Pessoa.gxTpr_Pessoa_tipopessoa = "F";
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20Pessoa", AV20Pessoa);
                  AV20Pessoa.gxTpr_Pessoa_docto = AV22Pessoa_Docto;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20Pessoa", AV20Pessoa);
                  AV20Pessoa.gxTpr_Pessoa_nome = StringUtil.Upper( AV10FirstName)+" "+StringUtil.Upper( AV13LastName);
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20Pessoa", AV20Pessoa);
                  AV20Pessoa.gxTpr_Pessoa_ativo = true;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20Pessoa", AV20Pessoa);
                  AV20Pessoa.gxTv_SdtPessoa_Pessoa_municipiocod_SetNull();
                  AV20Pessoa.gxTpr_Pessoa_telefone = AV70Pessoa_Telefone;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20Pessoa", AV20Pessoa);
                  AV20Pessoa.Save();
               }
               if ( AV20Pessoa.Success() )
               {
                  if ( (0==AV21Pessoa_Codigo) )
                  {
                     AV21Pessoa_Codigo = AV20Pessoa.gxTpr_Pessoa_codigo;
                  }
                  AV34Usuario = new SdtUsuario(context);
                  AV34Usuario.gxTpr_Usuario_pessoacod = AV21Pessoa_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Usuario", AV34Usuario);
                  AV34Usuario.gxTpr_Usuario_ativo = true;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Usuario", AV34Usuario);
                  AV34Usuario.gxTpr_Usuario_usergamguid = AV32User.gxTpr_Guid;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Usuario", AV34Usuario);
                  AV34Usuario.gxTpr_Usuario_ehcontratada = (0==AV46Contratante_Codigo);
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Usuario", AV34Usuario);
                  AV34Usuario.gxTpr_Usuario_ehcontratante = (0==AV44Contratada_Codigo);
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Usuario", AV34Usuario);
                  AV34Usuario.gxTpr_Usuario_nome = AV17Name;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Usuario", AV34Usuario);
                  AV34Usuario.gxTv_SdtUsuario_Usuario_cargocod_SetNull();
                  AV34Usuario.gxTpr_Usuario_email = AV7EMail;
                  context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Usuario", AV34Usuario);
                  AV34Usuario.Save();
                  if ( AV34Usuario.Success() )
                  {
                     AV37Usuario_Codigo = AV34Usuario.gxTpr_Usuario_codigo;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37Usuario_Codigo), 6, 0)));
                     if ( (0==AV46Contratante_Codigo) )
                     {
                        AV45ContratadaUsuario = new SdtContratadaUsuario(context);
                        AV45ContratadaUsuario.gxTpr_Contratadausuario_contratadacod = AV44Contratada_Codigo;
                        context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45ContratadaUsuario", AV45ContratadaUsuario);
                        AV45ContratadaUsuario.gxTpr_Contratadausuario_usuariocod = AV37Usuario_Codigo;
                        context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45ContratadaUsuario", AV45ContratadaUsuario);
                        AV45ContratadaUsuario.Save();
                     }
                     else
                     {
                        AV47ContratanteUsuario = new SdtContratanteUsuario(context);
                        AV47ContratanteUsuario.gxTpr_Contratanteusuario_contratantecod = AV46Contratante_Codigo;
                        context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47ContratanteUsuario", AV47ContratanteUsuario);
                        AV47ContratanteUsuario.gxTpr_Contratanteusuario_usuariocod = AV37Usuario_Codigo;
                        context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV47ContratanteUsuario", AV47ContratanteUsuario);
                        AV47ContratanteUsuario.Save();
                     }
                     if ( ! AV45ContratadaUsuario.Fail() && ! AV47ContratanteUsuario.Fail() )
                     {
                        if ( AV38Perfil_Codigo > 0 )
                        {
                           AV36UsuarioPerfil = new SdtUsuarioPerfil(context);
                           AV36UsuarioPerfil.gxTpr_Usuario_codigo = AV37Usuario_Codigo;
                           context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36UsuarioPerfil", AV36UsuarioPerfil);
                           AV36UsuarioPerfil.gxTpr_Perfil_codigo = AV38Perfil_Codigo;
                           context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV36UsuarioPerfil", AV36UsuarioPerfil);
                           AV36UsuarioPerfil.Save();
                        }
                        if ( AV36UsuarioPerfil.Success() || (0==AV38Perfil_Codigo) )
                        {
                           context.CommitDataStores( "WP_NovoUsuario");
                           if ( StringUtil.StrCmp(AV23Repository.gxTpr_Useractivationmethod, "A") == 0 )
                           {
                              AV5AdditionalParameter.gxTpr_Rememberusertype = AV33UserRememberMe;
                              context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
                              AV5AdditionalParameter.gxTpr_Isbatch = false;
                              context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
                              AV49UsuarioLogado.load( new SdtGAMUser(context).getid());
                              AV14LoginOK = new SdtGAMRepository(context).login("admin", "eficacia@2016", AV5AdditionalParameter, out  AV9Errors);
                              if ( ! AV14LoginOK )
                              {
                                 /* Execute user subroutine: 'DISPLAYMESSAGES' */
                                 S122 ();
                                 if (returnInSub) return;
                              }
                              else
                              {
                                 /* Execute user subroutine: 'ENVIONOTIFICACAO' */
                                 S132 ();
                                 if (returnInSub) return;
                                 /* Execute user subroutine: 'VOLTAR' */
                                 S142 ();
                                 if (returnInSub) return;
                              }
                           }
                           else
                           {
                              new gamcheckuseractivationmethod(context ).execute(  AV32User.gxTpr_Guid, out  AV16Messages) ;
                              context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV32User", AV32User);
                              AV88GXV1 = 1;
                              while ( AV88GXV1 <= AV16Messages.Count )
                              {
                                 AV15Message = ((SdtMessages_Message)AV16Messages.Item(AV88GXV1));
                                 GX_msglist.addItem(AV15Message.gxTpr_Description);
                                 AV88GXV1 = (int)(AV88GXV1+1);
                              }
                              /* Execute user subroutine: 'ENVIONOTIFICACAO' */
                              S132 ();
                              if (returnInSub) return;
                              /* Execute user subroutine: 'VOLTAR' */
                              S142 ();
                              if (returnInSub) return;
                           }
                        }
                        else
                        {
                           AV16Messages = AV36UsuarioPerfil.GetMessages();
                           /* Execute user subroutine: 'DISPLAYMESSAGESBC' */
                           S152 ();
                           if (returnInSub) return;
                        }
                     }
                     else
                     {
                        if ( AV45ContratadaUsuario.Fail() )
                        {
                           AV16Messages = AV45ContratadaUsuario.GetMessages();
                           /* Execute user subroutine: 'DISPLAYMESSAGESBC' */
                           S152 ();
                           if (returnInSub) return;
                        }
                        else
                        {
                           AV16Messages = AV47ContratanteUsuario.GetMessages();
                           /* Execute user subroutine: 'DISPLAYMESSAGESBC' */
                           S152 ();
                           if (returnInSub) return;
                        }
                     }
                  }
                  else
                  {
                     AV16Messages = AV34Usuario.GetMessages();
                     /* Execute user subroutine: 'DISPLAYMESSAGESBC' */
                     S152 ();
                     if (returnInSub) return;
                  }
               }
               else
               {
                  AV16Messages = AV20Pessoa.GetMessages();
                  /* Execute user subroutine: 'DISPLAYMESSAGESBC' */
                  S152 ();
                  if (returnInSub) return;
               }
            }
            else
            {
               AV9Errors = AV32User.geterrors();
               /* Execute user subroutine: 'DISPLAYMESSAGES' */
               S122 ();
               if (returnInSub) return;
            }
         }
      }

      protected void S122( )
      {
         /* 'DISPLAYMESSAGES' Routine */
         context.RollbackDataStores( "WP_NovoUsuario");
         AV89GXV2 = 1;
         while ( AV89GXV2 <= AV9Errors.Count )
         {
            AV8Error = ((SdtGAMError)AV9Errors.Item(AV89GXV2));
            GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV8Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            AV89GXV2 = (int)(AV89GXV2+1);
         }
      }

      protected void S152( )
      {
         /* 'DISPLAYMESSAGESBC' Routine */
         context.RollbackDataStores( "WP_NovoUsuario");
         AV90GXV3 = 1;
         while ( AV90GXV3 <= AV16Messages.Count )
         {
            AV15Message = ((SdtMessages_Message)AV16Messages.Item(AV90GXV3));
            GX_msglist.addItem(StringUtil.Format( "%1 (BC %2)", AV15Message.gxTpr_Description, AV15Message.gxTpr_Id, "", "", "", "", "", "", ""));
            AV90GXV3 = (int)(AV90GXV3+1);
         }
      }

      protected void S142( )
      {
         /* 'VOLTAR' Routine */
         AV50WebSession.Remove("Pessoa_Codigo");
         if ( (0==AV44Contratada_Codigo) )
         {
            lblTbjava_Caption = "<script language=\"javascript\">"+StringUtil.NewLine( )+"window.history.replaceState({},\"\",\"/meetrika/wwcontratante.aspx\");"+StringUtil.NewLine( )+"</script>";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            /* Execute user subroutine: 'PERFIS' */
            S162 ();
            if (returnInSub) return;
         }
         else
         {
            lblTbjava_Caption = "<script language=\"javascript\">"+StringUtil.NewLine( )+"window.history.replaceState({},\"\",\"/meetrika/wwcontratada.aspx\");"+StringUtil.NewLine( )+"</script>";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
            /* Execute user subroutine: 'PERFIS' */
            S162 ();
            if (returnInSub) return;
         }
      }

      protected void S162( )
      {
         /* 'PERFIS' Routine */
         context.PopUp(formatLink("wp_associationusuariousuarioperfil.aspx") + "?" + UrlEncode("" +AV34Usuario.gxTpr_Usuario_codigo), new Object[] {});
         /* Execute user subroutine: 'SERVICOS' */
         S172 ();
         if (returnInSub) return;
      }

      protected void S172( )
      {
         /* 'SERVICOS' Routine */
         if ( AV44Contratada_Codigo > 0 )
         {
            context.PopUp(formatLink("wp_associationusuarioservicos.aspx") + "?" + UrlEncode("" +AV34Usuario.gxTpr_Usuario_codigo) + "," + UrlEncode("" +AV44Contratada_Codigo), new Object[] {"AV44Contratada_Codigo"});
         }
         /* Execute user subroutine: 'SISTEMAS' */
         S182 ();
         if (returnInSub) return;
      }

      protected void S182( )
      {
         /* 'SISTEMAS' Routine */
         if ( AV34Usuario.gxTpr_Usuario_ehcontratante )
         {
            if ( AV46Contratante_Codigo > 0 )
            {
               context.PopUp(formatLink("wp_associarcontratanteusuario_sistema.aspx") + "?" + UrlEncode("" +AV46Contratante_Codigo) + "," + UrlEncode("" +AV34Usuario.gxTpr_Usuario_codigo), new Object[] {});
            }
            else
            {
               /* Using cursor H008F8 */
               pr_default.execute(5, new Object[] {AV51WWPContext.gxTpr_Areatrabalho_codigo});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A5AreaTrabalho_Codigo = H008F8_A5AreaTrabalho_Codigo[0];
                  A29Contratante_Codigo = H008F8_A29Contratante_Codigo[0];
                  n29Contratante_Codigo = H008F8_n29Contratante_Codigo[0];
                  context.PopUp(formatLink("wp_associarcontratanteusuario_sistema.aspx") + "?" + UrlEncode("" +A29Contratante_Codigo) + "," + UrlEncode("" +AV34Usuario.gxTpr_Usuario_codigo), new Object[] {});
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(5);
            }
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S132( )
      {
         /* 'ENVIONOTIFICACAO' Routine */
         /* Execute user subroutine: 'BUSCA.URL.SISTEMA' */
         S192 ();
         if (returnInSub) return;
         AV59Usuarios.Add(AV37Usuario_Codigo, 0);
         AV64EmailText = StringUtil.NewLine( ) + "Prezado(a)," + StringUtil.NewLine( );
         AV64EmailText = AV64EmailText + "Seu login foi criado." + StringUtil.NewLine( ) + StringUtil.NewLine( );
         AV64EmailText = AV64EmailText + "Link de acesso ao Sistema     " + StringUtil.Trim( AV71ParametrosSistema_URLApp);
         AV64EmailText = AV64EmailText + "Login  ��������������������   " + AV17Name + StringUtil.NewLine( );
         AV64EmailText = AV64EmailText + "Senha inicial��������������   " + AV18Password + StringUtil.NewLine( );
         AV64EmailText = AV64EmailText + StringUtil.NewLine( ) + StringUtil.Trim( AV51WWPContext.gxTpr_Areatrabalho_descricao) + ", usu�rio " + StringUtil.Trim( AV51WWPContext.gxTpr_Username) + StringUtil.NewLine( );
         AV64EmailText = AV64EmailText + StringUtil.NewLine( ) + StringUtil.NewLine( ) + "Esta mensagem pode conter informa��o confidencial e/ou privilegiada. Se voc� n�o for o destinat�rio ou a pessoa autorizada a receber esta mensagem, n�o pode usar, copiar ou divulgar as informa��es nela contidas ou tomar qualquer a��o baseada nessas informa��es. Se voc� recebeu esta mensagem por engano, por favor avise imediatamente o remetente, respondendo o e-mail e em seguida apague-o.";
         AV65Demandante = AV51WWPContext.gxTpr_Areatrabalho_descricao;
         AV66NomeSistema = AV51WWPContext.gxTpr_Parametrossistema_nomesistema;
         AV67Subject = "Login criado (No reply)";
         new prc_enviaremail(context ).execute(  AV51WWPContext.gxTpr_Areatrabalho_codigo,  AV59Usuarios,  AV67Subject,  AV64EmailText,  AV69Attachments, ref  AV68Resultado) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51WWPContext", AV51WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68Resultado", AV68Resultado);
      }

      protected void S202( )
      {
         /* 'NOTIFICAR NOVO USUARIO' Routine */
         AV58SDT_ID_Valor_Item.gxTpr_Id = "#Usuario_Nome#";
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58SDT_ID_Valor_Item", AV58SDT_ID_Valor_Item);
         AV58SDT_ID_Valor_Item.gxTpr_Valor = StringUtil.Trim( AV10FirstName);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58SDT_ID_Valor_Item", AV58SDT_ID_Valor_Item);
         AV57SDT_ID_Valor.Add(AV58SDT_ID_Valor_Item, 0);
         AV58SDT_ID_Valor_Item.gxTpr_Id = "#Usuario_Login#";
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58SDT_ID_Valor_Item", AV58SDT_ID_Valor_Item);
         AV58SDT_ID_Valor_Item.gxTpr_Valor = StringUtil.Trim( AV17Name);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58SDT_ID_Valor_Item", AV58SDT_ID_Valor_Item);
         AV57SDT_ID_Valor.Add(AV58SDT_ID_Valor_Item, 0);
         AV58SDT_ID_Valor_Item.gxTpr_Id = "#Usuario_Senha#";
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58SDT_ID_Valor_Item", AV58SDT_ID_Valor_Item);
         AV58SDT_ID_Valor_Item.gxTpr_Valor = StringUtil.Trim( AV18Password);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58SDT_ID_Valor_Item", AV58SDT_ID_Valor_Item);
         AV57SDT_ID_Valor.Add(AV58SDT_ID_Valor_Item, 0);
         AV58SDT_ID_Valor_Item.gxTpr_Id = "#Usuario_DataInclusao#";
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58SDT_ID_Valor_Item", AV58SDT_ID_Valor_Item);
         AV58SDT_ID_Valor_Item.gxTpr_Valor = context.localUtil.Format( Gx_date, "99/99/99");
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58SDT_ID_Valor_Item", AV58SDT_ID_Valor_Item);
         AV57SDT_ID_Valor.Add(AV58SDT_ID_Valor_Item, 0);
         AV58SDT_ID_Valor_Item.gxTpr_Id = "#Sistema_Link_Acesso#";
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58SDT_ID_Valor_Item", AV58SDT_ID_Valor_Item);
         AV58SDT_ID_Valor_Item.gxTpr_Valor = "link?";
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58SDT_ID_Valor_Item", AV58SDT_ID_Valor_Item);
         AV57SDT_ID_Valor.Add(AV58SDT_ID_Valor_Item, 0);
         AV55Email_Instancia_Guid = (Guid)(Guid.NewGuid( ));
         GXt_guid2 = (Guid)(AV55Email_Instancia_Guid);
         new prc_emailinstanciabykey(context ).execute(  "Novo_Usuario",  AV57SDT_ID_Valor,  AV51WWPContext.gxTpr_Areatrabalho_codigo, ref  GXt_guid2) ;
         AV55Email_Instancia_Guid = (Guid)((Guid)(GXt_guid2));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV51WWPContext", AV51WWPContext);
         new prc_emailinstanciadestinatario_new_by_email(context ).execute(  AV55Email_Instancia_Guid,  AV7EMail) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7EMail", AV7EMail);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWPContext.gxTpr_Usuario_email)) )
         {
            new prc_emailinstanciadestinatario_new_by_email(context ).execute(  AV55Email_Instancia_Guid,  AV7EMail) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7EMail", AV7EMail);
         }
         new prc_emailinstancia_send(context).executeSubmit(  AV55Email_Instancia_Guid) ;
      }

      protected void S192( )
      {
         /* 'BUSCA.URL.SISTEMA' Routine */
         AV71ParametrosSistema_URLApp = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ParametrosSistema_URLApp", AV71ParametrosSistema_URLApp);
         /* Using cursor H008F9 */
         pr_default.execute(6);
         while ( (pr_default.getStatus(6) != 101) )
         {
            A1679ParametrosSistema_URLApp = H008F9_A1679ParametrosSistema_URLApp[0];
            n1679ParametrosSistema_URLApp = H008F9_n1679ParametrosSistema_URLApp[0];
            A330ParametrosSistema_Codigo = H008F9_A330ParametrosSistema_Codigo[0];
            AV71ParametrosSistema_URLApp = A1679ParametrosSistema_URLApp;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ParametrosSistema_URLApp", AV71ParametrosSistema_URLApp);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void nextLoad( )
      {
      }

      protected void E188F2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_8F2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            wb_table2_8_8F2( true) ;
         }
         else
         {
            wb_table2_8_8F2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_8F2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_8F2e( true) ;
         }
         else
         {
            wb_table1_2_8F2e( false) ;
         }
      }

      protected void wb_table2_8_8F2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr bordercolor=\"#000000\"  class='Table'>") ;
            context.WriteHtmlText( "<td bgcolor=\"#F5F5F5\" bordercolor=\"#000000\" colspan=\"6\"  style=\""+CSSHelper.Prettify( "height:40px;width:10px")+"\" class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbformtitle_Internalname, lblTbformtitle_Caption, "", "", lblTbformtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Arial'; font-size:12.0pt; font-weight:normal; font-style:normal; color:#000000; background-color:#F5F5F5;", "Title", 0, "", 1, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqcpf_Internalname, AV35ReqCPF);
            ClassString = "Image";
            StyleString = "";
            AV35ReqCPF_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35ReqCPF))&&String.IsNullOrEmpty(StringUtil.RTrim( AV82Reqcpf_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35ReqCPF)));
            GxWebStd.gx_bitmap( context, imgavReqcpf_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV35ReqCPF)) ? AV82Reqcpf_GXI : context.PathToRelativeUrl( AV35ReqCPF)), "", "", "", context.GetTheme( ), 1, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV35ReqCPF_IsBlob, false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgender2_Internalname, "CPF", "", "", lblTbgender2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_docto_Internalname, AV22Pessoa_Docto, StringUtil.RTrim( context.localUtil.Format( AV22Pessoa_Docto, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_docto_Jsonclick, 0, "DescriptionAttribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\" class='Table'>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqfirstname_Internalname, AV26ReqFirstName);
            ClassString = "Image";
            StyleString = "";
            AV26ReqFirstName_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqFirstName))&&String.IsNullOrEmpty(StringUtil.RTrim( AV76Reqfirstname_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqFirstName)));
            GxWebStd.gx_bitmap( context, imgavReqfirstname_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV26ReqFirstName)) ? AV76Reqfirstname_GXI : context.PathToRelativeUrl( AV26ReqFirstName)), "", "", "", context.GetTheme( ), 1, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV26ReqFirstName_IsBlob, false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbfirstname_Internalname, "Nome", "", "", lblTbfirstname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFirstname_Internalname, StringUtil.RTrim( AV10FirstName), StringUtil.RTrim( context.localUtil.Format( AV10FirstName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFirstname_Jsonclick, 0, "DescriptionAttribute", "", "", "", 1, edtavFirstname_Enabled, 1, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqlastname_Internalname, AV29ReqLastName);
            ClassString = "Image";
            StyleString = "";
            AV29ReqLastName_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29ReqLastName))&&String.IsNullOrEmpty(StringUtil.RTrim( AV77Reqlastname_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29ReqLastName)));
            GxWebStd.gx_bitmap( context, imgavReqlastname_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV29ReqLastName)) ? AV77Reqlastname_GXI : context.PathToRelativeUrl( AV29ReqLastName)), "", "", "", context.GetTheme( ), 1, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV29ReqLastName_IsBlob, false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTblastname_Internalname, "Sobrenome", "", "", lblTblastname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLastname_Internalname, StringUtil.RTrim( AV13LastName), StringUtil.RTrim( context.localUtil.Format( AV13LastName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLastname_Jsonclick, 0, "DescriptionAttribute", "", "", "", 1, edtavLastname_Enabled, 1, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqbirthday_Internalname, AV24ReqBirthday);
            ClassString = "Image";
            StyleString = "";
            AV24ReqBirthday_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV24ReqBirthday))&&String.IsNullOrEmpty(StringUtil.RTrim( AV78Reqbirthday_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV24ReqBirthday)));
            GxWebStd.gx_bitmap( context, imgavReqbirthday_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV24ReqBirthday)) ? AV78Reqbirthday_GXI : context.PathToRelativeUrl( AV24ReqBirthday)), "", "", "", context.GetTheme( ), imgavReqbirthday_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV24ReqBirthday_IsBlob, false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbbirthday_Internalname, "Nascimento", "", "", lblTbbirthday_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavBirthday_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavBirthday_Internalname, context.localUtil.Format(AV6Birthday, "99/99/9999"), context.localUtil.Format( AV6Birthday, "99/99/9999"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavBirthday_Jsonclick, 0, "Attribute", "", "", "", 1, edtavBirthday_Enabled, 1, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, 0, true, "GAMDate", "right", false, "HLP_WP_NovoUsuario.htm");
            GxWebStd.gx_bitmap( context, edtavBirthday_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavBirthday_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqgender_Internalname, AV27ReqGender);
            ClassString = "Image";
            StyleString = "";
            AV27ReqGender_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqGender))&&String.IsNullOrEmpty(StringUtil.RTrim( AV79Reqgender_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqGender)));
            GxWebStd.gx_bitmap( context, imgavReqgender_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV27ReqGender)) ? AV79Reqgender_GXI : context.PathToRelativeUrl( AV27ReqGender)), "", "", "", context.GetTheme( ), imgavReqgender_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV27ReqGender_IsBlob, false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbgender_Internalname, "Sexo", "", "", lblTbgender_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGender, cmbavGender_Internalname, StringUtil.RTrim( AV11Gender), 1, cmbavGender_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavGender.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_WP_NovoUsuario.htm");
            cmbavGender.CurrentValue = StringUtil.RTrim( AV11Gender);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGender_Internalname, "Values", (String)(cmbavGender.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqemail_Internalname, AV25ReqEmail);
            ClassString = "Image";
            StyleString = "";
            AV25ReqEmail_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV25ReqEmail))&&String.IsNullOrEmpty(StringUtil.RTrim( AV75Reqemail_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV25ReqEmail)));
            GxWebStd.gx_bitmap( context, imgavReqemail_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV25ReqEmail)) ? AV75Reqemail_GXI : context.PathToRelativeUrl( AV25ReqEmail)), "", "", "", context.GetTheme( ), imgavReqemail_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV25ReqEmail_IsBlob, false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbemail_Internalname, "E-mail", "", "", lblTbemail_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  style=\""+CSSHelper.Prettify( "height:9px")+"\" class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEmail_Internalname, AV7EMail, StringUtil.RTrim( context.localUtil.Format( AV7EMail, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEmail_Jsonclick, 0, "DescriptionAttribute", "", "", "", 1, edtavEmail_Enabled, 1, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMEMail", "left", true, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:14px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbtelefone_Internalname, "Telefone", "", "", lblTbtelefone_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPessoa_telefone_Internalname, StringUtil.RTrim( AV70Pessoa_Telefone), StringUtil.RTrim( context.localUtil.Format( AV70Pessoa_Telefone, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPessoa_telefone_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:38px")+"\" class='Table'>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqname_Internalname, AV30ReqName);
            ClassString = "Image";
            StyleString = "";
            AV30ReqName_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV30ReqName))&&String.IsNullOrEmpty(StringUtil.RTrim( AV74Reqname_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV30ReqName)));
            GxWebStd.gx_bitmap( context, imgavReqname_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV30ReqName)) ? AV74Reqname_GXI : context.PathToRelativeUrl( AV30ReqName)), "", "", "", context.GetTheme( ), 1, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV30ReqName_IsBlob, false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname2_Internalname, "Usu�rio", "", "", lblTbname2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, AV17Name, StringUtil.RTrim( context.localUtil.Format( AV17Name, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "DescriptionAttribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMUserIdentification", "left", true, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:8px")+"\" class='Table'>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqpassword_Internalname, AV31ReqPassword);
            ClassString = "Image";
            StyleString = "";
            AV31ReqPassword_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqPassword))&&String.IsNullOrEmpty(StringUtil.RTrim( AV80Reqpassword_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqPassword)));
            GxWebStd.gx_bitmap( context, imgavReqpassword_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqPassword)) ? AV80Reqpassword_GXI : context.PathToRelativeUrl( AV31ReqPassword)), "", "", "", context.GetTheme( ), imgavReqpassword_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV31ReqPassword_IsBlob, false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpwd_Internalname, "Senha", "", "", lblTbpwd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPassword_Internalname, StringUtil.RTrim( AV18Password), StringUtil.RTrim( context.localUtil.Format( AV18Password, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPassword_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "<br/>") ;
            context.WriteHtmlText( "- Ser� alterada pelo usu�rio no seu primeiro Login -") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqpassword_Internalname, AV31ReqPassword);
            ClassString = "Image";
            StyleString = "";
            AV31ReqPassword_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqPassword))&&String.IsNullOrEmpty(StringUtil.RTrim( AV80Reqpassword_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqPassword)));
            GxWebStd.gx_bitmap( context, imgavReqpassword_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV31ReqPassword)) ? AV80Reqpassword_GXI : context.PathToRelativeUrl( AV31ReqPassword)), "", "", "", context.GetTheme( ), imgavReqpassword_Visible, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV31ReqPassword_IsBlob, false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpwdconf_Internalname, "Confirma��o de Senha", "", "", lblTbpwdconf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPasswordconf_Internalname, StringUtil.RTrim( AV19PasswordConf), StringUtil.RTrim( context.localUtil.Format( AV19PasswordConf, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPasswordconf_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:24px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPerfil_gamid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40Perfil_GamID), 12, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40Perfil_GamID), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPerfil_gamid_Jsonclick, 0, "Attribute", "", "", "", edtavPerfil_gamid_Visible, 1, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPerfil_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38Perfil_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV38Perfil_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPerfil_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavPerfil_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:bottom")+"\" class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"6\"  style=\""+CSSHelper.Prettify( "vertical-align:bottom")+"\" class='Table'>") ;
            wb_table3_104_8F2( true) ;
         }
         else
         {
            wb_table3_104_8F2( false) ;
         }
         return  ;
      }

      protected void wb_table3_104_8F2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Static Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavReqicon_Internalname, AV28ReqIcon);
            ClassString = "Image";
            StyleString = "";
            AV28ReqIcon_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqIcon))&&String.IsNullOrEmpty(StringUtil.RTrim( AV81Reqicon_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqIcon)));
            GxWebStd.gx_bitmap( context, imgavReqicon_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV28ReqIcon)) ? AV81Reqicon_GXI : context.PathToRelativeUrl( AV28ReqIcon)), "", "", "", context.GetTheme( ), 1, 0, "", "", 0, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 1, AV28ReqIcon_IsBlob, false, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbinficonreq_Internalname, "Informa��o Obrigat�ria", "", "", lblTbinficonreq_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:8.0pt; font-weight:normal; font-style:normal;", "Label", 0, "", lblTbinficonreq_Visible, 1, 0, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td colspan=\"6\"  class='Table'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDisponivel_Internalname, StringUtil.BoolToStr( AV48Disponivel), "", "", chkavDisponivel.Visible, chkavDisponivel.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(119, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_8F2e( true) ;
         }
         else
         {
            wb_table2_8_8F2e( false) ;
         }
      }

      protected void wb_table3_104_8F2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblbuttons_Internalname, tblTblbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "vertical-align:bottom")+"\" class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", "Confirmar", bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CONFIRMAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:2px")+"\" class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Fechar", bttBtnclose_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_NovoUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_104_8F2e( true) ;
         }
         else
         {
            wb_table3_104_8F2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV46Contratante_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV46Contratante_Codigo), "ZZZZZ9")));
         AV44Contratada_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44Contratada_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8F2( ) ;
         WS8F2( ) ;
         WE8F2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117391574");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_novousuario.js", "?20203117391576");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbformtitle_Internalname = "TBFORMTITLE";
         imgavReqcpf_Internalname = "vREQCPF";
         lblTbgender2_Internalname = "TBGENDER2";
         edtavPessoa_docto_Internalname = "vPESSOA_DOCTO";
         imgavReqfirstname_Internalname = "vREQFIRSTNAME";
         lblTbfirstname_Internalname = "TBFIRSTNAME";
         edtavFirstname_Internalname = "vFIRSTNAME";
         imgavReqlastname_Internalname = "vREQLASTNAME";
         lblTblastname_Internalname = "TBLASTNAME";
         edtavLastname_Internalname = "vLASTNAME";
         imgavReqbirthday_Internalname = "vREQBIRTHDAY";
         lblTbbirthday_Internalname = "TBBIRTHDAY";
         edtavBirthday_Internalname = "vBIRTHDAY";
         imgavReqgender_Internalname = "vREQGENDER";
         lblTbgender_Internalname = "TBGENDER";
         cmbavGender_Internalname = "vGENDER";
         imgavReqemail_Internalname = "vREQEMAIL";
         lblTbemail_Internalname = "TBEMAIL";
         edtavEmail_Internalname = "vEMAIL";
         lblTbtelefone_Internalname = "TBTELEFONE";
         edtavPessoa_telefone_Internalname = "vPESSOA_TELEFONE";
         imgavReqname_Internalname = "vREQNAME";
         lblTbname2_Internalname = "TBNAME2";
         edtavName_Internalname = "vNAME";
         imgavReqpassword_Internalname = "vREQPASSWORD";
         lblTbpwd_Internalname = "TBPWD";
         edtavPassword_Internalname = "vPASSWORD";
         imgavReqpassword_Internalname = "vREQPASSWORD";
         lblTbpwdconf_Internalname = "TBPWDCONF";
         edtavPasswordconf_Internalname = "vPASSWORDCONF";
         edtavPerfil_gamid_Internalname = "vPERFIL_GAMID";
         edtavPerfil_codigo_Internalname = "vPERFIL_CODIGO";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTblbuttons_Internalname = "TBLBUTTONS";
         imgavReqicon_Internalname = "vREQICON";
         lblTbinficonreq_Internalname = "TBINFICONREQ";
         chkavDisponivel_Internalname = "vDISPONIVEL";
         lblTbjava_Internalname = "TBJAVA";
         tblTable1_Internalname = "TABLE1";
         tblTable2_Internalname = "TABLE2";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         lblTbjava_Visible = 1;
         chkavDisponivel.Enabled = 1;
         lblTbinficonreq_Visible = 1;
         edtavPerfil_codigo_Jsonclick = "";
         edtavPerfil_gamid_Jsonclick = "";
         edtavPasswordconf_Jsonclick = "";
         edtavPassword_Jsonclick = "";
         edtavName_Jsonclick = "";
         edtavPessoa_telefone_Jsonclick = "";
         edtavEmail_Jsonclick = "";
         cmbavGender_Jsonclick = "";
         edtavBirthday_Jsonclick = "";
         edtavLastname_Jsonclick = "";
         edtavFirstname_Jsonclick = "";
         edtavPessoa_docto_Jsonclick = "";
         lblTbjava_Caption = "Java";
         cmbavGender.Enabled = 1;
         edtavBirthday_Enabled = 1;
         edtavLastname_Enabled = 1;
         edtavFirstname_Enabled = 1;
         edtavEmail_Enabled = 1;
         chkavDisponivel.Visible = 1;
         lblTbformtitle_Caption = "Novo Usu�rio";
         imgavReqpassword_Visible = 1;
         imgavReqgender_Visible = 1;
         imgavReqbirthday_Visible = 1;
         imgavReqemail_Visible = 1;
         edtavPerfil_gamid_Visible = 1;
         edtavPerfil_codigo_Visible = 1;
         chkavDisponivel.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Novo Usuario - GAM";
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV11Gender = "";
         Gx_date = DateTime.MinValue;
         AV35ReqCPF = "";
         AV22Pessoa_Docto = "";
         AV26ReqFirstName = "";
         AV10FirstName = "";
         AV29ReqLastName = "";
         AV13LastName = "";
         AV24ReqBirthday = "";
         AV6Birthday = DateTime.MinValue;
         AV27ReqGender = "";
         AV25ReqEmail = "";
         AV7EMail = "";
         AV70Pessoa_Telefone = "";
         AV30ReqName = "";
         AV17Name = "";
         AV31ReqPassword = "";
         AV18Password = "";
         AV19PasswordConf = "";
         AV28ReqIcon = "";
         AV51WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV23Repository = new SdtGAMRepository(context);
         AV74Reqname_GXI = "";
         AV75Reqemail_GXI = "";
         AV76Reqfirstname_GXI = "";
         AV77Reqlastname_GXI = "";
         AV78Reqbirthday_GXI = "";
         AV79Reqgender_GXI = "";
         AV80Reqpassword_GXI = "";
         AV81Reqicon_GXI = "";
         AV82Reqcpf_GXI = "";
         AV49UsuarioLogado = new SdtGAMUser(context);
         scmdbuf = "";
         H008F2_A34Pessoa_Codigo = new int[1] ;
         H008F2_A36Pessoa_TipoPessoa = new String[] {""} ;
         H008F2_A37Pessoa_Docto = new String[] {""} ;
         A36Pessoa_TipoPessoa = "";
         A37Pessoa_Docto = "";
         H008F3_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H008F3_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H008F3_A516Contratada_TipoFabrica = new String[] {""} ;
         H008F3_n516Contratada_TipoFabrica = new bool[] {false} ;
         H008F3_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H008F3_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H008F3_A70ContratadaUsuario_UsuarioPessoaCod = new int[1] ;
         H008F3_n70ContratadaUsuario_UsuarioPessoaCod = new bool[] {false} ;
         A516Contratada_TipoFabrica = "";
         H008F4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H008F4_A29Contratante_Codigo = new int[1] ;
         H008F4_n29Contratante_Codigo = new bool[] {false} ;
         H008F4_A516Contratada_TipoFabrica = new String[] {""} ;
         H008F4_n516Contratada_TipoFabrica = new bool[] {false} ;
         H008F4_A39Contratada_Codigo = new int[1] ;
         H008F6_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H008F6_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H008F6_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H008F6_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H008F6_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         H008F6_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         AV50WebSession = context.GetSession();
         AV20Pessoa = new SdtPessoa(context);
         H008F7_A1Usuario_Codigo = new int[1] ;
         H008F7_A57Usuario_PessoaCod = new int[1] ;
         H008F7_A341Usuario_UserGamGuid = new String[] {""} ;
         A341Usuario_UserGamGuid = "";
         AV32User = new SdtGAMUser(context);
         AV56pattern = "";
         GXt_char1 = "";
         AV9Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV34Usuario = new SdtUsuario(context);
         AV45ContratadaUsuario = new SdtContratadaUsuario(context);
         AV47ContratanteUsuario = new SdtContratanteUsuario(context);
         AV36UsuarioPerfil = new SdtUsuarioPerfil(context);
         AV5AdditionalParameter = new SdtGAMLoginAdditionalParameters(context);
         AV16Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV15Message = new SdtMessages_Message(context);
         AV8Error = new SdtGAMError(context);
         H008F8_A5AreaTrabalho_Codigo = new int[1] ;
         H008F8_A29Contratante_Codigo = new int[1] ;
         H008F8_n29Contratante_Codigo = new bool[] {false} ;
         AV59Usuarios = new GxSimpleCollection();
         AV64EmailText = "";
         AV71ParametrosSistema_URLApp = "";
         AV65Demandante = "";
         AV66NomeSistema = "";
         AV67Subject = "";
         AV69Attachments = new GxSimpleCollection();
         AV68Resultado = "";
         AV58SDT_ID_Valor_Item = new SdtSDT_ID_Valor(context);
         AV57SDT_ID_Valor = new GxObjectCollection( context, "SDT_ID_Valor", "GxEv3Up14_Meetrika", "SdtSDT_ID_Valor", "GeneXus.Programs");
         AV55Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         GXt_guid2 = (Guid)(System.Guid.Empty);
         H008F9_A1679ParametrosSistema_URLApp = new String[] {""} ;
         H008F9_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         H008F9_A330ParametrosSistema_Codigo = new int[1] ;
         A1679ParametrosSistema_URLApp = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbformtitle_Jsonclick = "";
         lblTbgender2_Jsonclick = "";
         TempTags = "";
         lblTbfirstname_Jsonclick = "";
         lblTblastname_Jsonclick = "";
         lblTbbirthday_Jsonclick = "";
         lblTbgender_Jsonclick = "";
         lblTbemail_Jsonclick = "";
         lblTbtelefone_Jsonclick = "";
         lblTbname2_Jsonclick = "";
         lblTbpwd_Jsonclick = "";
         lblTbpwdconf_Jsonclick = "";
         lblTbinficonreq_Jsonclick = "";
         lblTbjava_Jsonclick = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_novousuario__default(),
            new Object[][] {
                new Object[] {
               H008F2_A34Pessoa_Codigo, H008F2_A36Pessoa_TipoPessoa, H008F2_A37Pessoa_Docto
               }
               , new Object[] {
               H008F3_A66ContratadaUsuario_ContratadaCod, H008F3_A69ContratadaUsuario_UsuarioCod, H008F3_A516Contratada_TipoFabrica, H008F3_n516Contratada_TipoFabrica, H008F3_A1228ContratadaUsuario_AreaTrabalhoCod, H008F3_n1228ContratadaUsuario_AreaTrabalhoCod, H008F3_A70ContratadaUsuario_UsuarioPessoaCod, H008F3_n70ContratadaUsuario_UsuarioPessoaCod
               }
               , new Object[] {
               H008F4_A52Contratada_AreaTrabalhoCod, H008F4_A29Contratante_Codigo, H008F4_n29Contratante_Codigo, H008F4_A516Contratada_TipoFabrica, H008F4_A39Contratada_Codigo
               }
               , new Object[] {
               H008F6_A60ContratanteUsuario_UsuarioCod, H008F6_A63ContratanteUsuario_ContratanteCod, H008F6_A61ContratanteUsuario_UsuarioPessoaCod, H008F6_n61ContratanteUsuario_UsuarioPessoaCod, H008F6_A1020ContratanteUsuario_AreaTrabalhoCod, H008F6_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               H008F7_A1Usuario_Codigo, H008F7_A57Usuario_PessoaCod, H008F7_A341Usuario_UserGamGuid
               }
               , new Object[] {
               H008F8_A5AreaTrabalho_Codigo, H008F8_A29Contratante_Codigo, H008F8_n29Contratante_Codigo
               }
               , new Object[] {
               H008F9_A1679ParametrosSistema_URLApp, H008F9_n1679ParametrosSistema_URLApp, H008F9_A330ParametrosSistema_Codigo
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
         chkavDisponivel.Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV83GXLvl83 ;
      private short AV52i ;
      private short AV53d ;
      private short AV33UserRememberMe ;
      private short nGXWrapped ;
      private int AV46Contratante_Codigo ;
      private int AV44Contratada_Codigo ;
      private int wcpOAV46Contratante_Codigo ;
      private int wcpOAV44Contratada_Codigo ;
      private int AV38Perfil_Codigo ;
      private int AV39AreaTrabalho_Codigo ;
      private int edtavPerfil_codigo_Visible ;
      private int edtavPerfil_gamid_Visible ;
      private int imgavReqemail_Visible ;
      private int imgavReqbirthday_Visible ;
      private int imgavReqgender_Visible ;
      private int imgavReqpassword_Visible ;
      private int lblTbinficonreq_Visible ;
      private int lblTbjava_Visible ;
      private int AV21Pessoa_Codigo ;
      private int A34Pessoa_Codigo ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A70ContratadaUsuario_UsuarioPessoaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A29Contratante_Codigo ;
      private int A39Contratada_Codigo ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private int A57Usuario_PessoaCod ;
      private int edtavEmail_Enabled ;
      private int edtavFirstname_Enabled ;
      private int edtavLastname_Enabled ;
      private int edtavBirthday_Enabled ;
      private int AV37Usuario_Codigo ;
      private int AV88GXV1 ;
      private int AV89GXV2 ;
      private int AV90GXV3 ;
      private int A5AreaTrabalho_Codigo ;
      private int A330ParametrosSistema_Codigo ;
      private int idxLst ;
      private long AV40Perfil_GamID ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV11Gender ;
      private String chkavDisponivel_Internalname ;
      private String edtavPessoa_docto_Internalname ;
      private String imgavReqcpf_Internalname ;
      private String imgavReqfirstname_Internalname ;
      private String AV10FirstName ;
      private String edtavFirstname_Internalname ;
      private String imgavReqlastname_Internalname ;
      private String AV13LastName ;
      private String edtavLastname_Internalname ;
      private String imgavReqbirthday_Internalname ;
      private String edtavBirthday_Internalname ;
      private String imgavReqgender_Internalname ;
      private String cmbavGender_Internalname ;
      private String imgavReqemail_Internalname ;
      private String edtavEmail_Internalname ;
      private String AV70Pessoa_Telefone ;
      private String edtavPessoa_telefone_Internalname ;
      private String imgavReqname_Internalname ;
      private String edtavName_Internalname ;
      private String imgavReqpassword_Internalname ;
      private String AV18Password ;
      private String edtavPassword_Internalname ;
      private String AV19PasswordConf ;
      private String edtavPasswordconf_Internalname ;
      private String edtavPerfil_gamid_Internalname ;
      private String edtavPerfil_codigo_Internalname ;
      private String imgavReqicon_Internalname ;
      private String lblTbinficonreq_Internalname ;
      private String lblTbformtitle_Caption ;
      private String lblTbformtitle_Internalname ;
      private String lblTbjava_Internalname ;
      private String scmdbuf ;
      private String A36Pessoa_TipoPessoa ;
      private String A516Contratada_TipoFabrica ;
      private String A341Usuario_UserGamGuid ;
      private String GXt_char1 ;
      private String lblTbjava_Caption ;
      private String AV64EmailText ;
      private String AV65Demandante ;
      private String AV66NomeSistema ;
      private String AV67Subject ;
      private String AV68Resultado ;
      private String sStyleString ;
      private String tblTable2_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTable1_Internalname ;
      private String lblTbformtitle_Jsonclick ;
      private String lblTbgender2_Internalname ;
      private String lblTbgender2_Jsonclick ;
      private String TempTags ;
      private String edtavPessoa_docto_Jsonclick ;
      private String lblTbfirstname_Internalname ;
      private String lblTbfirstname_Jsonclick ;
      private String edtavFirstname_Jsonclick ;
      private String lblTblastname_Internalname ;
      private String lblTblastname_Jsonclick ;
      private String edtavLastname_Jsonclick ;
      private String lblTbbirthday_Internalname ;
      private String lblTbbirthday_Jsonclick ;
      private String edtavBirthday_Jsonclick ;
      private String lblTbgender_Internalname ;
      private String lblTbgender_Jsonclick ;
      private String cmbavGender_Jsonclick ;
      private String lblTbemail_Internalname ;
      private String lblTbemail_Jsonclick ;
      private String edtavEmail_Jsonclick ;
      private String lblTbtelefone_Internalname ;
      private String lblTbtelefone_Jsonclick ;
      private String edtavPessoa_telefone_Jsonclick ;
      private String lblTbname2_Internalname ;
      private String lblTbname2_Jsonclick ;
      private String edtavName_Jsonclick ;
      private String lblTbpwd_Internalname ;
      private String lblTbpwd_Jsonclick ;
      private String edtavPassword_Jsonclick ;
      private String lblTbpwdconf_Internalname ;
      private String lblTbpwdconf_Jsonclick ;
      private String edtavPasswordconf_Jsonclick ;
      private String edtavPerfil_gamid_Jsonclick ;
      private String edtavPerfil_codigo_Jsonclick ;
      private String lblTbinficonreq_Jsonclick ;
      private String lblTbjava_Jsonclick ;
      private String tblTblbuttons_Internalname ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private DateTime Gx_date ;
      private DateTime AV6Birthday ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV48Disponivel ;
      private bool returnInSub ;
      private bool n516Contratada_TipoFabrica ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n70ContratadaUsuario_UsuarioPessoaCod ;
      private bool n29Contratante_Codigo ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool AV12isOK ;
      private bool AV14LoginOK ;
      private bool n1679ParametrosSistema_URLApp ;
      private bool AV35ReqCPF_IsBlob ;
      private bool AV26ReqFirstName_IsBlob ;
      private bool AV29ReqLastName_IsBlob ;
      private bool AV24ReqBirthday_IsBlob ;
      private bool AV27ReqGender_IsBlob ;
      private bool AV25ReqEmail_IsBlob ;
      private bool AV30ReqName_IsBlob ;
      private bool AV31ReqPassword_IsBlob ;
      private bool AV28ReqIcon_IsBlob ;
      private String AV22Pessoa_Docto ;
      private String AV7EMail ;
      private String AV17Name ;
      private String AV74Reqname_GXI ;
      private String AV75Reqemail_GXI ;
      private String AV76Reqfirstname_GXI ;
      private String AV77Reqlastname_GXI ;
      private String AV78Reqbirthday_GXI ;
      private String AV79Reqgender_GXI ;
      private String AV80Reqpassword_GXI ;
      private String AV81Reqicon_GXI ;
      private String AV82Reqcpf_GXI ;
      private String A37Pessoa_Docto ;
      private String AV56pattern ;
      private String AV71ParametrosSistema_URLApp ;
      private String A1679ParametrosSistema_URLApp ;
      private String AV35ReqCPF ;
      private String AV26ReqFirstName ;
      private String AV29ReqLastName ;
      private String AV24ReqBirthday ;
      private String AV27ReqGender ;
      private String AV25ReqEmail ;
      private String AV30ReqName ;
      private String AV31ReqPassword ;
      private String AV28ReqIcon ;
      private Guid AV55Email_Instancia_Guid ;
      private Guid GXt_guid2 ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavGender ;
      private GXCheckbox chkavDisponivel ;
      private IDataStoreProvider pr_default ;
      private int[] H008F2_A34Pessoa_Codigo ;
      private String[] H008F2_A36Pessoa_TipoPessoa ;
      private String[] H008F2_A37Pessoa_Docto ;
      private int[] H008F3_A66ContratadaUsuario_ContratadaCod ;
      private int[] H008F3_A69ContratadaUsuario_UsuarioCod ;
      private String[] H008F3_A516Contratada_TipoFabrica ;
      private bool[] H008F3_n516Contratada_TipoFabrica ;
      private int[] H008F3_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H008F3_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H008F3_A70ContratadaUsuario_UsuarioPessoaCod ;
      private bool[] H008F3_n70ContratadaUsuario_UsuarioPessoaCod ;
      private int[] H008F4_A52Contratada_AreaTrabalhoCod ;
      private int[] H008F4_A29Contratante_Codigo ;
      private bool[] H008F4_n29Contratante_Codigo ;
      private String[] H008F4_A516Contratada_TipoFabrica ;
      private bool[] H008F4_n516Contratada_TipoFabrica ;
      private int[] H008F4_A39Contratada_Codigo ;
      private int[] H008F6_A60ContratanteUsuario_UsuarioCod ;
      private int[] H008F6_A63ContratanteUsuario_ContratanteCod ;
      private int[] H008F6_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H008F6_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H008F6_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] H008F6_n1020ContratanteUsuario_AreaTrabalhoCod ;
      private int[] H008F7_A1Usuario_Codigo ;
      private int[] H008F7_A57Usuario_PessoaCod ;
      private String[] H008F7_A341Usuario_UserGamGuid ;
      private int[] H008F8_A5AreaTrabalho_Codigo ;
      private int[] H008F8_A29Contratante_Codigo ;
      private bool[] H008F8_n29Contratante_Codigo ;
      private String[] H008F9_A1679ParametrosSistema_URLApp ;
      private bool[] H008F9_n1679ParametrosSistema_URLApp ;
      private int[] H008F9_A330ParametrosSistema_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV50WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV59Usuarios ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV69Attachments ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV16Messages ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV9Errors ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ID_Valor ))]
      private IGxCollection AV57SDT_ID_Valor ;
      private GXWebForm Form ;
      private SdtGAMLoginAdditionalParameters AV5AdditionalParameter ;
      private SdtContratadaUsuario AV45ContratadaUsuario ;
      private SdtMessages_Message AV15Message ;
      private SdtContratanteUsuario AV47ContratanteUsuario ;
      private SdtGAMError AV8Error ;
      private SdtGAMUser AV49UsuarioLogado ;
      private SdtGAMUser AV32User ;
      private SdtPessoa AV20Pessoa ;
      private SdtGAMRepository AV23Repository ;
      private SdtSDT_ID_Valor AV58SDT_ID_Valor_Item ;
      private SdtUsuario AV34Usuario ;
      private SdtUsuarioPerfil AV36UsuarioPerfil ;
      private wwpbaseobjects.SdtWWPContext AV51WWPContext ;
   }

   public class wp_novousuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH008F2 ;
          prmH008F2 = new Object[] {
          new Object[] {"@AV22Pessoa_Docto",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmH008F3 ;
          prmH008F3 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV51WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008F4 ;
          prmH008F4 = new Object[] {
          new Object[] {"@AV44Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008F6 ;
          prmH008F6 = new Object[] {
          new Object[] {"@Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV51WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008F7 ;
          prmH008F7 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008F8 ;
          prmH008F8 = new Object[] {
          new Object[] {"@AV51WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH008F9 ;
          prmH008F9 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H008F2", "SELECT [Pessoa_Codigo], [Pessoa_TipoPessoa], [Pessoa_Docto] FROM [Pessoa] WITH (NOLOCK) WHERE ([Pessoa_Docto] = @AV22Pessoa_Docto) AND ([Pessoa_TipoPessoa] = 'F') ORDER BY [Pessoa_Docto] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008F2,1,0,true,true )
             ,new CursorDef("H008F3", "SELECT TOP 1 T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T1.[ContratadaUsuario_UsuarioCod] AS ContratadaUsuario_UsuarioCod, T2.[Contratada_TipoFabrica], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T3.[Usuario_PessoaCod] AS ContratadaUsuario_UsuarioPessoaCod FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[ContratadaUsuario_UsuarioCod]) WHERE (T3.[Usuario_PessoaCod] = @Pessoa_Codigo) AND (Not T2.[Contratada_TipoFabrica] = 'I') AND (T2.[Contratada_AreaTrabalhoCod] = @AV51WWPC_1Areatrabalho_codigo) ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008F3,1,0,false,true )
             ,new CursorDef("H008F4", "SELECT TOP 1 T1.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T2.[Contratante_Codigo], T1.[Contratada_TipoFabrica], T1.[Contratada_Codigo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[Contratada_AreaTrabalhoCod]) WHERE (T1.[Contratada_Codigo] = @AV44Contratada_Codigo) AND (Not T1.[Contratada_TipoFabrica] = 'I') ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008F4,1,0,true,true )
             ,new CursorDef("H008F6", "SELECT TOP 1 T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T1.[ContratanteUsuario_ContratanteCod], T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPess, COALESCE( T3.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN (SELECT MIN(T4.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T5.[ContratanteUsuario_ContratanteCod], T5.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod FROM [AreaTrabalho] T4 WITH (NOLOCK),  [ContratanteUsuario] T5 WITH (NOLOCK) WHERE T4.[Contratante_Codigo] = T5.[ContratanteUsuario_ContratanteCod] GROUP BY T5.[ContratanteUsuario_ContratanteCod], T5.[ContratanteUsuario_UsuarioCod] ) T3 ON T3.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T3.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod]) WHERE (T1.[ContratanteUsuario_ContratanteCod] = @Contratante_Codigo) AND (T2.[Usuario_PessoaCod] = @Pessoa_Codigo) AND (COALESCE( T3.[ContratanteUsuario_AreaTrabalhoCod], 0) = @AV51WWPC_1Areatrabalho_codigo) ORDER BY T1.[ContratanteUsuario_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008F6,1,0,false,true )
             ,new CursorDef("H008F7", "SELECT TOP 1 [Usuario_Codigo], [Usuario_PessoaCod], [Usuario_UserGamGuid] FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_PessoaCod] = @Pessoa_Codigo ORDER BY [Usuario_PessoaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008F7,1,0,true,true )
             ,new CursorDef("H008F8", "SELECT TOP 1 [AreaTrabalho_Codigo], [Contratante_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV51WWPC_1Areatrabalho_codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008F8,1,0,false,true )
             ,new CursorDef("H008F9", "SELECT TOP 1 [ParametrosSistema_URLApp], [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008F9,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 40) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
