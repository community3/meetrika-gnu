/*
               File: GetSaldoContratoNotaEmpenhoWCFilterData
        Description: Get Saldo Contrato Nota Empenho WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:5.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getsaldocontratonotaempenhowcfilterdata : GXProcedure
   {
      public getsaldocontratonotaempenhowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getsaldocontratonotaempenhowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV27DDOName = aP0_DDOName;
         this.AV25SearchTxt = aP1_SearchTxt;
         this.AV26SearchTxtTo = aP2_SearchTxtTo;
         this.AV31OptionsJson = "" ;
         this.AV34OptionsDescJson = "" ;
         this.AV36OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV31OptionsJson;
         aP4_OptionsDescJson=this.AV34OptionsDescJson;
         aP5_OptionIndexesJson=this.AV36OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV27DDOName = aP0_DDOName;
         this.AV25SearchTxt = aP1_SearchTxt;
         this.AV26SearchTxtTo = aP2_SearchTxtTo;
         this.AV31OptionsJson = "" ;
         this.AV34OptionsDescJson = "" ;
         this.AV36OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV31OptionsJson;
         aP4_OptionsDescJson=this.AV34OptionsDescJson;
         aP5_OptionIndexesJson=this.AV36OptionIndexesJson;
         return AV36OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getsaldocontratonotaempenhowcfilterdata objgetsaldocontratonotaempenhowcfilterdata;
         objgetsaldocontratonotaempenhowcfilterdata = new getsaldocontratonotaempenhowcfilterdata();
         objgetsaldocontratonotaempenhowcfilterdata.AV27DDOName = aP0_DDOName;
         objgetsaldocontratonotaempenhowcfilterdata.AV25SearchTxt = aP1_SearchTxt;
         objgetsaldocontratonotaempenhowcfilterdata.AV26SearchTxtTo = aP2_SearchTxtTo;
         objgetsaldocontratonotaempenhowcfilterdata.AV31OptionsJson = "" ;
         objgetsaldocontratonotaempenhowcfilterdata.AV34OptionsDescJson = "" ;
         objgetsaldocontratonotaempenhowcfilterdata.AV36OptionIndexesJson = "" ;
         objgetsaldocontratonotaempenhowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetsaldocontratonotaempenhowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetsaldocontratonotaempenhowcfilterdata);
         aP3_OptionsJson=this.AV31OptionsJson;
         aP4_OptionsDescJson=this.AV34OptionsDescJson;
         aP5_OptionIndexesJson=this.AV36OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getsaldocontratonotaempenhowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV30Options = (IGxCollection)(new GxSimpleCollection());
         AV33OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV35OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV27DDOName), "DDO_NOTAEMPENHO_ITENTIFICADOR") == 0 )
         {
            /* Execute user subroutine: 'LOADNOTAEMPENHO_ITENTIFICADOROPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV31OptionsJson = AV30Options.ToJSonString(false);
         AV34OptionsDescJson = AV33OptionsDesc.ToJSonString(false);
         AV36OptionIndexesJson = AV35OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV38Session.Get("SaldoContratoNotaEmpenhoWCGridState"), "") == 0 )
         {
            AV40GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "SaldoContratoNotaEmpenhoWCGridState"), "");
         }
         else
         {
            AV40GridState.FromXml(AV38Session.Get("SaldoContratoNotaEmpenhoWCGridState"), "");
         }
         AV57GXV1 = 1;
         while ( AV57GXV1 <= AV40GridState.gxTpr_Filtervalues.Count )
         {
            AV41GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV40GridState.gxTpr_Filtervalues.Item(AV57GXV1));
            if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_CODIGO") == 0 )
            {
               AV10TFNotaEmpenho_Codigo = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
               AV11TFNotaEmpenho_Codigo_To = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ITENTIFICADOR") == 0 )
            {
               AV12TFNotaEmpenho_Itentificador = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ITENTIFICADOR_SEL") == 0 )
            {
               AV13TFNotaEmpenho_Itentificador_Sel = AV41GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_DEMISSAO") == 0 )
            {
               AV14TFNotaEmpenho_DEmissao = context.localUtil.CToT( AV41GridStateFilterValue.gxTpr_Value, 2);
               AV15TFNotaEmpenho_DEmissao_To = context.localUtil.CToT( AV41GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_VALOR") == 0 )
            {
               AV16TFNotaEmpenho_Valor = NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, ".");
               AV17TFNotaEmpenho_Valor_To = NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_QTD") == 0 )
            {
               AV18TFNotaEmpenho_Qtd = NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, ".");
               AV19TFNotaEmpenho_Qtd_To = NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_SALDOANT") == 0 )
            {
               AV20TFNotaEmpenho_SaldoAnt = NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, ".");
               AV21TFNotaEmpenho_SaldoAnt_To = NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_SALDOPOS") == 0 )
            {
               AV22TFNotaEmpenho_SaldoPos = NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, ".");
               AV23TFNotaEmpenho_SaldoPos_To = NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ATIVO_SEL") == 0 )
            {
               AV24TFNotaEmpenho_Ativo_Sel = (short)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV41GridStateFilterValue.gxTpr_Name, "PARM_&SALDOCONTRATO_CODIGO") == 0 )
            {
               AV54SaldoContrato_Codigo = (int)(NumberUtil.Val( AV41GridStateFilterValue.gxTpr_Value, "."));
            }
            AV57GXV1 = (int)(AV57GXV1+1);
         }
         if ( AV40GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV42GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV40GridState.gxTpr_Dynamicfilters.Item(1));
            AV43DynamicFiltersSelector1 = AV42GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV43DynamicFiltersSelector1, "NOTAEMPENHO_ITENTIFICADOR") == 0 )
            {
               AV44DynamicFiltersOperator1 = AV42GridStateDynamicFilter.gxTpr_Operator;
               AV45NotaEmpenho_Itentificador1 = AV42GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV40GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV46DynamicFiltersEnabled2 = true;
               AV42GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV40GridState.gxTpr_Dynamicfilters.Item(2));
               AV47DynamicFiltersSelector2 = AV42GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "NOTAEMPENHO_ITENTIFICADOR") == 0 )
               {
                  AV48DynamicFiltersOperator2 = AV42GridStateDynamicFilter.gxTpr_Operator;
                  AV49NotaEmpenho_Itentificador2 = AV42GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV40GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV50DynamicFiltersEnabled3 = true;
                  AV42GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV40GridState.gxTpr_Dynamicfilters.Item(3));
                  AV51DynamicFiltersSelector3 = AV42GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "NOTAEMPENHO_ITENTIFICADOR") == 0 )
                  {
                     AV52DynamicFiltersOperator3 = AV42GridStateDynamicFilter.gxTpr_Operator;
                     AV53NotaEmpenho_Itentificador3 = AV42GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADNOTAEMPENHO_ITENTIFICADOROPTIONS' Routine */
         AV12TFNotaEmpenho_Itentificador = AV25SearchTxt;
         AV13TFNotaEmpenho_Itentificador_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV43DynamicFiltersSelector1 ,
                                              AV44DynamicFiltersOperator1 ,
                                              AV45NotaEmpenho_Itentificador1 ,
                                              AV46DynamicFiltersEnabled2 ,
                                              AV47DynamicFiltersSelector2 ,
                                              AV48DynamicFiltersOperator2 ,
                                              AV49NotaEmpenho_Itentificador2 ,
                                              AV50DynamicFiltersEnabled3 ,
                                              AV51DynamicFiltersSelector3 ,
                                              AV52DynamicFiltersOperator3 ,
                                              AV53NotaEmpenho_Itentificador3 ,
                                              AV10TFNotaEmpenho_Codigo ,
                                              AV11TFNotaEmpenho_Codigo_To ,
                                              AV13TFNotaEmpenho_Itentificador_Sel ,
                                              AV12TFNotaEmpenho_Itentificador ,
                                              AV14TFNotaEmpenho_DEmissao ,
                                              AV15TFNotaEmpenho_DEmissao_To ,
                                              AV16TFNotaEmpenho_Valor ,
                                              AV17TFNotaEmpenho_Valor_To ,
                                              AV18TFNotaEmpenho_Qtd ,
                                              AV19TFNotaEmpenho_Qtd_To ,
                                              AV20TFNotaEmpenho_SaldoAnt ,
                                              AV21TFNotaEmpenho_SaldoAnt_To ,
                                              AV22TFNotaEmpenho_SaldoPos ,
                                              AV23TFNotaEmpenho_SaldoPos_To ,
                                              AV24TFNotaEmpenho_Ativo_Sel ,
                                              A1564NotaEmpenho_Itentificador ,
                                              A1560NotaEmpenho_Codigo ,
                                              A1565NotaEmpenho_DEmissao ,
                                              A1566NotaEmpenho_Valor ,
                                              A1567NotaEmpenho_Qtd ,
                                              A1568NotaEmpenho_SaldoAnt ,
                                              A1569NotaEmpenho_SaldoPos ,
                                              A1570NotaEmpenho_Ativo ,
                                              AV54SaldoContrato_Codigo ,
                                              A1561SaldoContrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV45NotaEmpenho_Itentificador1 = StringUtil.PadR( StringUtil.RTrim( AV45NotaEmpenho_Itentificador1), 15, "%");
         lV45NotaEmpenho_Itentificador1 = StringUtil.PadR( StringUtil.RTrim( AV45NotaEmpenho_Itentificador1), 15, "%");
         lV49NotaEmpenho_Itentificador2 = StringUtil.PadR( StringUtil.RTrim( AV49NotaEmpenho_Itentificador2), 15, "%");
         lV49NotaEmpenho_Itentificador2 = StringUtil.PadR( StringUtil.RTrim( AV49NotaEmpenho_Itentificador2), 15, "%");
         lV53NotaEmpenho_Itentificador3 = StringUtil.PadR( StringUtil.RTrim( AV53NotaEmpenho_Itentificador3), 15, "%");
         lV53NotaEmpenho_Itentificador3 = StringUtil.PadR( StringUtil.RTrim( AV53NotaEmpenho_Itentificador3), 15, "%");
         lV12TFNotaEmpenho_Itentificador = StringUtil.PadR( StringUtil.RTrim( AV12TFNotaEmpenho_Itentificador), 15, "%");
         /* Using cursor P00SF2 */
         pr_default.execute(0, new Object[] {AV54SaldoContrato_Codigo, lV45NotaEmpenho_Itentificador1, lV45NotaEmpenho_Itentificador1, lV49NotaEmpenho_Itentificador2, lV49NotaEmpenho_Itentificador2, lV53NotaEmpenho_Itentificador3, lV53NotaEmpenho_Itentificador3, AV10TFNotaEmpenho_Codigo, AV11TFNotaEmpenho_Codigo_To, lV12TFNotaEmpenho_Itentificador, AV13TFNotaEmpenho_Itentificador_Sel, AV14TFNotaEmpenho_DEmissao, AV15TFNotaEmpenho_DEmissao_To, AV16TFNotaEmpenho_Valor, AV17TFNotaEmpenho_Valor_To, AV18TFNotaEmpenho_Qtd, AV19TFNotaEmpenho_Qtd_To, AV20TFNotaEmpenho_SaldoAnt, AV21TFNotaEmpenho_SaldoAnt_To, AV22TFNotaEmpenho_SaldoPos, AV23TFNotaEmpenho_SaldoPos_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKSF2 = false;
            A1561SaldoContrato_Codigo = P00SF2_A1561SaldoContrato_Codigo[0];
            A1564NotaEmpenho_Itentificador = P00SF2_A1564NotaEmpenho_Itentificador[0];
            n1564NotaEmpenho_Itentificador = P00SF2_n1564NotaEmpenho_Itentificador[0];
            A1570NotaEmpenho_Ativo = P00SF2_A1570NotaEmpenho_Ativo[0];
            n1570NotaEmpenho_Ativo = P00SF2_n1570NotaEmpenho_Ativo[0];
            A1569NotaEmpenho_SaldoPos = P00SF2_A1569NotaEmpenho_SaldoPos[0];
            n1569NotaEmpenho_SaldoPos = P00SF2_n1569NotaEmpenho_SaldoPos[0];
            A1568NotaEmpenho_SaldoAnt = P00SF2_A1568NotaEmpenho_SaldoAnt[0];
            n1568NotaEmpenho_SaldoAnt = P00SF2_n1568NotaEmpenho_SaldoAnt[0];
            A1567NotaEmpenho_Qtd = P00SF2_A1567NotaEmpenho_Qtd[0];
            n1567NotaEmpenho_Qtd = P00SF2_n1567NotaEmpenho_Qtd[0];
            A1566NotaEmpenho_Valor = P00SF2_A1566NotaEmpenho_Valor[0];
            n1566NotaEmpenho_Valor = P00SF2_n1566NotaEmpenho_Valor[0];
            A1565NotaEmpenho_DEmissao = P00SF2_A1565NotaEmpenho_DEmissao[0];
            n1565NotaEmpenho_DEmissao = P00SF2_n1565NotaEmpenho_DEmissao[0];
            A1560NotaEmpenho_Codigo = P00SF2_A1560NotaEmpenho_Codigo[0];
            AV37count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00SF2_A1561SaldoContrato_Codigo[0] == A1561SaldoContrato_Codigo ) && ( StringUtil.StrCmp(P00SF2_A1564NotaEmpenho_Itentificador[0], A1564NotaEmpenho_Itentificador) == 0 ) )
            {
               BRKSF2 = false;
               A1560NotaEmpenho_Codigo = P00SF2_A1560NotaEmpenho_Codigo[0];
               AV37count = (long)(AV37count+1);
               BRKSF2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1564NotaEmpenho_Itentificador)) )
            {
               AV29Option = A1564NotaEmpenho_Itentificador;
               AV30Options.Add(AV29Option, 0);
               AV35OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV37count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV30Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSF2 )
            {
               BRKSF2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV30Options = new GxSimpleCollection();
         AV33OptionsDesc = new GxSimpleCollection();
         AV35OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV38Session = context.GetSession();
         AV40GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV41GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFNotaEmpenho_Itentificador = "";
         AV13TFNotaEmpenho_Itentificador_Sel = "";
         AV14TFNotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         AV15TFNotaEmpenho_DEmissao_To = (DateTime)(DateTime.MinValue);
         AV42GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV43DynamicFiltersSelector1 = "";
         AV45NotaEmpenho_Itentificador1 = "";
         AV47DynamicFiltersSelector2 = "";
         AV49NotaEmpenho_Itentificador2 = "";
         AV51DynamicFiltersSelector3 = "";
         AV53NotaEmpenho_Itentificador3 = "";
         scmdbuf = "";
         lV45NotaEmpenho_Itentificador1 = "";
         lV49NotaEmpenho_Itentificador2 = "";
         lV53NotaEmpenho_Itentificador3 = "";
         lV12TFNotaEmpenho_Itentificador = "";
         A1564NotaEmpenho_Itentificador = "";
         A1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         P00SF2_A1561SaldoContrato_Codigo = new int[1] ;
         P00SF2_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         P00SF2_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         P00SF2_A1570NotaEmpenho_Ativo = new bool[] {false} ;
         P00SF2_n1570NotaEmpenho_Ativo = new bool[] {false} ;
         P00SF2_A1569NotaEmpenho_SaldoPos = new decimal[1] ;
         P00SF2_n1569NotaEmpenho_SaldoPos = new bool[] {false} ;
         P00SF2_A1568NotaEmpenho_SaldoAnt = new decimal[1] ;
         P00SF2_n1568NotaEmpenho_SaldoAnt = new bool[] {false} ;
         P00SF2_A1567NotaEmpenho_Qtd = new decimal[1] ;
         P00SF2_n1567NotaEmpenho_Qtd = new bool[] {false} ;
         P00SF2_A1566NotaEmpenho_Valor = new decimal[1] ;
         P00SF2_n1566NotaEmpenho_Valor = new bool[] {false} ;
         P00SF2_A1565NotaEmpenho_DEmissao = new DateTime[] {DateTime.MinValue} ;
         P00SF2_n1565NotaEmpenho_DEmissao = new bool[] {false} ;
         P00SF2_A1560NotaEmpenho_Codigo = new int[1] ;
         AV29Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getsaldocontratonotaempenhowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00SF2_A1561SaldoContrato_Codigo, P00SF2_A1564NotaEmpenho_Itentificador, P00SF2_n1564NotaEmpenho_Itentificador, P00SF2_A1570NotaEmpenho_Ativo, P00SF2_n1570NotaEmpenho_Ativo, P00SF2_A1569NotaEmpenho_SaldoPos, P00SF2_n1569NotaEmpenho_SaldoPos, P00SF2_A1568NotaEmpenho_SaldoAnt, P00SF2_n1568NotaEmpenho_SaldoAnt, P00SF2_A1567NotaEmpenho_Qtd,
               P00SF2_n1567NotaEmpenho_Qtd, P00SF2_A1566NotaEmpenho_Valor, P00SF2_n1566NotaEmpenho_Valor, P00SF2_A1565NotaEmpenho_DEmissao, P00SF2_n1565NotaEmpenho_DEmissao, P00SF2_A1560NotaEmpenho_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV24TFNotaEmpenho_Ativo_Sel ;
      private short AV44DynamicFiltersOperator1 ;
      private short AV48DynamicFiltersOperator2 ;
      private short AV52DynamicFiltersOperator3 ;
      private int AV57GXV1 ;
      private int AV10TFNotaEmpenho_Codigo ;
      private int AV11TFNotaEmpenho_Codigo_To ;
      private int AV54SaldoContrato_Codigo ;
      private int A1560NotaEmpenho_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private long AV37count ;
      private decimal AV16TFNotaEmpenho_Valor ;
      private decimal AV17TFNotaEmpenho_Valor_To ;
      private decimal AV18TFNotaEmpenho_Qtd ;
      private decimal AV19TFNotaEmpenho_Qtd_To ;
      private decimal AV20TFNotaEmpenho_SaldoAnt ;
      private decimal AV21TFNotaEmpenho_SaldoAnt_To ;
      private decimal AV22TFNotaEmpenho_SaldoPos ;
      private decimal AV23TFNotaEmpenho_SaldoPos_To ;
      private decimal A1566NotaEmpenho_Valor ;
      private decimal A1567NotaEmpenho_Qtd ;
      private decimal A1568NotaEmpenho_SaldoAnt ;
      private decimal A1569NotaEmpenho_SaldoPos ;
      private String AV12TFNotaEmpenho_Itentificador ;
      private String AV13TFNotaEmpenho_Itentificador_Sel ;
      private String AV45NotaEmpenho_Itentificador1 ;
      private String AV49NotaEmpenho_Itentificador2 ;
      private String AV53NotaEmpenho_Itentificador3 ;
      private String scmdbuf ;
      private String lV45NotaEmpenho_Itentificador1 ;
      private String lV49NotaEmpenho_Itentificador2 ;
      private String lV53NotaEmpenho_Itentificador3 ;
      private String lV12TFNotaEmpenho_Itentificador ;
      private String A1564NotaEmpenho_Itentificador ;
      private DateTime AV14TFNotaEmpenho_DEmissao ;
      private DateTime AV15TFNotaEmpenho_DEmissao_To ;
      private DateTime A1565NotaEmpenho_DEmissao ;
      private bool returnInSub ;
      private bool AV46DynamicFiltersEnabled2 ;
      private bool AV50DynamicFiltersEnabled3 ;
      private bool A1570NotaEmpenho_Ativo ;
      private bool BRKSF2 ;
      private bool n1564NotaEmpenho_Itentificador ;
      private bool n1570NotaEmpenho_Ativo ;
      private bool n1569NotaEmpenho_SaldoPos ;
      private bool n1568NotaEmpenho_SaldoAnt ;
      private bool n1567NotaEmpenho_Qtd ;
      private bool n1566NotaEmpenho_Valor ;
      private bool n1565NotaEmpenho_DEmissao ;
      private String AV36OptionIndexesJson ;
      private String AV31OptionsJson ;
      private String AV34OptionsDescJson ;
      private String AV27DDOName ;
      private String AV25SearchTxt ;
      private String AV26SearchTxtTo ;
      private String AV43DynamicFiltersSelector1 ;
      private String AV47DynamicFiltersSelector2 ;
      private String AV51DynamicFiltersSelector3 ;
      private String AV29Option ;
      private IGxSession AV38Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00SF2_A1561SaldoContrato_Codigo ;
      private String[] P00SF2_A1564NotaEmpenho_Itentificador ;
      private bool[] P00SF2_n1564NotaEmpenho_Itentificador ;
      private bool[] P00SF2_A1570NotaEmpenho_Ativo ;
      private bool[] P00SF2_n1570NotaEmpenho_Ativo ;
      private decimal[] P00SF2_A1569NotaEmpenho_SaldoPos ;
      private bool[] P00SF2_n1569NotaEmpenho_SaldoPos ;
      private decimal[] P00SF2_A1568NotaEmpenho_SaldoAnt ;
      private bool[] P00SF2_n1568NotaEmpenho_SaldoAnt ;
      private decimal[] P00SF2_A1567NotaEmpenho_Qtd ;
      private bool[] P00SF2_n1567NotaEmpenho_Qtd ;
      private decimal[] P00SF2_A1566NotaEmpenho_Valor ;
      private bool[] P00SF2_n1566NotaEmpenho_Valor ;
      private DateTime[] P00SF2_A1565NotaEmpenho_DEmissao ;
      private bool[] P00SF2_n1565NotaEmpenho_DEmissao ;
      private int[] P00SF2_A1560NotaEmpenho_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV30Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV40GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV41GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV42GridStateDynamicFilter ;
   }

   public class getsaldocontratonotaempenhowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00SF2( IGxContext context ,
                                             String AV43DynamicFiltersSelector1 ,
                                             short AV44DynamicFiltersOperator1 ,
                                             String AV45NotaEmpenho_Itentificador1 ,
                                             bool AV46DynamicFiltersEnabled2 ,
                                             String AV47DynamicFiltersSelector2 ,
                                             short AV48DynamicFiltersOperator2 ,
                                             String AV49NotaEmpenho_Itentificador2 ,
                                             bool AV50DynamicFiltersEnabled3 ,
                                             String AV51DynamicFiltersSelector3 ,
                                             short AV52DynamicFiltersOperator3 ,
                                             String AV53NotaEmpenho_Itentificador3 ,
                                             int AV10TFNotaEmpenho_Codigo ,
                                             int AV11TFNotaEmpenho_Codigo_To ,
                                             String AV13TFNotaEmpenho_Itentificador_Sel ,
                                             String AV12TFNotaEmpenho_Itentificador ,
                                             DateTime AV14TFNotaEmpenho_DEmissao ,
                                             DateTime AV15TFNotaEmpenho_DEmissao_To ,
                                             decimal AV16TFNotaEmpenho_Valor ,
                                             decimal AV17TFNotaEmpenho_Valor_To ,
                                             decimal AV18TFNotaEmpenho_Qtd ,
                                             decimal AV19TFNotaEmpenho_Qtd_To ,
                                             decimal AV20TFNotaEmpenho_SaldoAnt ,
                                             decimal AV21TFNotaEmpenho_SaldoAnt_To ,
                                             decimal AV22TFNotaEmpenho_SaldoPos ,
                                             decimal AV23TFNotaEmpenho_SaldoPos_To ,
                                             short AV24TFNotaEmpenho_Ativo_Sel ,
                                             String A1564NotaEmpenho_Itentificador ,
                                             int A1560NotaEmpenho_Codigo ,
                                             DateTime A1565NotaEmpenho_DEmissao ,
                                             decimal A1566NotaEmpenho_Valor ,
                                             decimal A1567NotaEmpenho_Qtd ,
                                             decimal A1568NotaEmpenho_SaldoAnt ,
                                             decimal A1569NotaEmpenho_SaldoPos ,
                                             bool A1570NotaEmpenho_Ativo ,
                                             int AV54SaldoContrato_Codigo ,
                                             int A1561SaldoContrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [21] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [SaldoContrato_Codigo], [NotaEmpenho_Itentificador], [NotaEmpenho_Ativo], [NotaEmpenho_SaldoPos], [NotaEmpenho_SaldoAnt], [NotaEmpenho_Qtd], [NotaEmpenho_Valor], [NotaEmpenho_DEmissao], [NotaEmpenho_Codigo] FROM [NotaEmpenho] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([SaldoContrato_Codigo] = @AV54SaldoContrato_Codigo)";
         if ( ( StringUtil.StrCmp(AV43DynamicFiltersSelector1, "NOTAEMPENHO_ITENTIFICADOR") == 0 ) && ( AV44DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45NotaEmpenho_Itentificador1)) ) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] like @lV45NotaEmpenho_Itentificador1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV43DynamicFiltersSelector1, "NOTAEMPENHO_ITENTIFICADOR") == 0 ) && ( AV44DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45NotaEmpenho_Itentificador1)) ) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] like '%' + @lV45NotaEmpenho_Itentificador1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "NOTAEMPENHO_ITENTIFICADOR") == 0 ) && ( AV48DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49NotaEmpenho_Itentificador2)) ) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] like @lV49NotaEmpenho_Itentificador2)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV46DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV47DynamicFiltersSelector2, "NOTAEMPENHO_ITENTIFICADOR") == 0 ) && ( AV48DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49NotaEmpenho_Itentificador2)) ) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] like '%' + @lV49NotaEmpenho_Itentificador2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "NOTAEMPENHO_ITENTIFICADOR") == 0 ) && ( AV52DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53NotaEmpenho_Itentificador3)) ) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] like @lV53NotaEmpenho_Itentificador3)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV50DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV51DynamicFiltersSelector3, "NOTAEMPENHO_ITENTIFICADOR") == 0 ) && ( AV52DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53NotaEmpenho_Itentificador3)) ) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] like '%' + @lV53NotaEmpenho_Itentificador3)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV10TFNotaEmpenho_Codigo) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Codigo] >= @AV10TFNotaEmpenho_Codigo)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV11TFNotaEmpenho_Codigo_To) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Codigo] <= @AV11TFNotaEmpenho_Codigo_To)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFNotaEmpenho_Itentificador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFNotaEmpenho_Itentificador)) ) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] like @lV12TFNotaEmpenho_Itentificador)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFNotaEmpenho_Itentificador_Sel)) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] = @AV13TFNotaEmpenho_Itentificador_Sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (DateTime.MinValue==AV14TFNotaEmpenho_DEmissao) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_DEmissao] >= @AV14TFNotaEmpenho_DEmissao)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (DateTime.MinValue==AV15TFNotaEmpenho_DEmissao_To) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_DEmissao] <= @AV15TFNotaEmpenho_DEmissao_To)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV16TFNotaEmpenho_Valor) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Valor] >= @AV16TFNotaEmpenho_Valor)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV17TFNotaEmpenho_Valor_To) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Valor] <= @AV17TFNotaEmpenho_Valor_To)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFNotaEmpenho_Qtd) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Qtd] >= @AV18TFNotaEmpenho_Qtd)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFNotaEmpenho_Qtd_To) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Qtd] <= @AV19TFNotaEmpenho_Qtd_To)";
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV20TFNotaEmpenho_SaldoAnt) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_SaldoAnt] >= @AV20TFNotaEmpenho_SaldoAnt)";
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFNotaEmpenho_SaldoAnt_To) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_SaldoAnt] <= @AV21TFNotaEmpenho_SaldoAnt_To)";
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFNotaEmpenho_SaldoPos) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_SaldoPos] >= @AV22TFNotaEmpenho_SaldoPos)";
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFNotaEmpenho_SaldoPos_To) )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_SaldoPos] <= @AV23TFNotaEmpenho_SaldoPos_To)";
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( AV24TFNotaEmpenho_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Ativo] = 1)";
         }
         if ( AV24TFNotaEmpenho_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and ([NotaEmpenho_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [SaldoContrato_Codigo], [NotaEmpenho_Itentificador]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00SF2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (DateTime)dynConstraints[15] , (DateTime)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (short)dynConstraints[25] , (String)dynConstraints[26] , (int)dynConstraints[27] , (DateTime)dynConstraints[28] , (decimal)dynConstraints[29] , (decimal)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (bool)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SF2 ;
          prmP00SF2 = new Object[] {
          new Object[] {"@AV54SaldoContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV45NotaEmpenho_Itentificador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV45NotaEmpenho_Itentificador1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV49NotaEmpenho_Itentificador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV49NotaEmpenho_Itentificador2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV53NotaEmpenho_Itentificador3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV53NotaEmpenho_Itentificador3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV10TFNotaEmpenho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFNotaEmpenho_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFNotaEmpenho_Itentificador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFNotaEmpenho_Itentificador_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV14TFNotaEmpenho_DEmissao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV15TFNotaEmpenho_DEmissao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV16TFNotaEmpenho_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17TFNotaEmpenho_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV18TFNotaEmpenho_Qtd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV19TFNotaEmpenho_Qtd_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV20TFNotaEmpenho_SaldoAnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21TFNotaEmpenho_SaldoAnt_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV22TFNotaEmpenho_SaldoPos",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV23TFNotaEmpenho_SaldoPos_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SF2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SF2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[41]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getsaldocontratonotaempenhowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getsaldocontratonotaempenhowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getsaldocontratonotaempenhowcfilterdata") )
          {
             return  ;
          }
          getsaldocontratonotaempenhowcfilterdata worker = new getsaldocontratonotaempenhowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
