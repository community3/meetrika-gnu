/*
               File: MenuPerfil_BC
        Description: Menu Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:35.82
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class menuperfil_bc : GXHttpHandler, IGxSilentTrn
   {
      public menuperfil_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public menuperfil_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1C49( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1C49( ) ;
         standaloneModal( ) ;
         AddRow1C49( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z277Menu_Codigo = A277Menu_Codigo;
               Z3Perfil_Codigo = A3Perfil_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_1C0( )
      {
         BeforeValidate1C49( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1C49( ) ;
            }
            else
            {
               CheckExtendedTable1C49( ) ;
               if ( AnyError == 0 )
               {
                  ZM1C49( 2) ;
                  ZM1C49( 3) ;
               }
               CloseExtendedTableCursors1C49( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM1C49( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z278Menu_Nome = A278Menu_Nome;
            Z280Menu_Tipo = A280Menu_Tipo;
            Z284Menu_Ativo = A284Menu_Ativo;
            Z283Menu_Ordem = A283Menu_Ordem;
            Z281Menu_Link = A281Menu_Link;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z4Perfil_Nome = A4Perfil_Nome;
         }
         if ( GX_JID == -1 )
         {
            Z277Menu_Codigo = A277Menu_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
            Z278Menu_Nome = A278Menu_Nome;
            Z280Menu_Tipo = A280Menu_Tipo;
            Z284Menu_Ativo = A284Menu_Ativo;
            Z283Menu_Ordem = A283Menu_Ordem;
            Z282Menu_Imagem = A282Menu_Imagem;
            Z40000Menu_Imagem_GXI = A40000Menu_Imagem_GXI;
            Z281Menu_Link = A281Menu_Link;
            Z4Perfil_Nome = A4Perfil_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load1C49( )
      {
         /* Using cursor BC001C6 */
         pr_default.execute(4, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound49 = 1;
            A278Menu_Nome = BC001C6_A278Menu_Nome[0];
            A280Menu_Tipo = BC001C6_A280Menu_Tipo[0];
            A284Menu_Ativo = BC001C6_A284Menu_Ativo[0];
            A283Menu_Ordem = BC001C6_A283Menu_Ordem[0];
            A40000Menu_Imagem_GXI = BC001C6_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = BC001C6_n40000Menu_Imagem_GXI[0];
            A281Menu_Link = BC001C6_A281Menu_Link[0];
            n281Menu_Link = BC001C6_n281Menu_Link[0];
            A4Perfil_Nome = BC001C6_A4Perfil_Nome[0];
            A282Menu_Imagem = BC001C6_A282Menu_Imagem[0];
            n282Menu_Imagem = BC001C6_n282Menu_Imagem[0];
            ZM1C49( -1) ;
         }
         pr_default.close(4);
         OnLoadActions1C49( ) ;
      }

      protected void OnLoadActions1C49( )
      {
      }

      protected void CheckExtendedTable1C49( )
      {
         standaloneModal( ) ;
         /* Using cursor BC001C4 */
         pr_default.execute(2, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Menu'.", "ForeignKeyNotFound", 1, "MENU_CODIGO");
            AnyError = 1;
         }
         A278Menu_Nome = BC001C4_A278Menu_Nome[0];
         A280Menu_Tipo = BC001C4_A280Menu_Tipo[0];
         A284Menu_Ativo = BC001C4_A284Menu_Ativo[0];
         A283Menu_Ordem = BC001C4_A283Menu_Ordem[0];
         A40000Menu_Imagem_GXI = BC001C4_A40000Menu_Imagem_GXI[0];
         n40000Menu_Imagem_GXI = BC001C4_n40000Menu_Imagem_GXI[0];
         A281Menu_Link = BC001C4_A281Menu_Link[0];
         n281Menu_Link = BC001C4_n281Menu_Link[0];
         A282Menu_Imagem = BC001C4_A282Menu_Imagem[0];
         n282Menu_Imagem = BC001C4_n282Menu_Imagem[0];
         pr_default.close(2);
         /* Using cursor BC001C5 */
         pr_default.execute(3, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
            AnyError = 1;
         }
         A4Perfil_Nome = BC001C5_A4Perfil_Nome[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1C49( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1C49( )
      {
         /* Using cursor BC001C7 */
         pr_default.execute(5, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound49 = 1;
         }
         else
         {
            RcdFound49 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC001C3 */
         pr_default.execute(1, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1C49( 1) ;
            RcdFound49 = 1;
            A277Menu_Codigo = BC001C3_A277Menu_Codigo[0];
            A3Perfil_Codigo = BC001C3_A3Perfil_Codigo[0];
            Z277Menu_Codigo = A277Menu_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
            sMode49 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1C49( ) ;
            if ( AnyError == 1 )
            {
               RcdFound49 = 0;
               InitializeNonKey1C49( ) ;
            }
            Gx_mode = sMode49;
         }
         else
         {
            RcdFound49 = 0;
            InitializeNonKey1C49( ) ;
            sMode49 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode49;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1C49( ) ;
         if ( RcdFound49 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_1C0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1C49( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC001C2 */
            pr_default.execute(0, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"MenuPerfil"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"MenuPerfil"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1C49( )
      {
         BeforeValidate1C49( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1C49( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1C49( 0) ;
            CheckOptimisticConcurrency1C49( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1C49( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1C49( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001C8 */
                     pr_default.execute(6, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("MenuPerfil") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1C49( ) ;
            }
            EndLevel1C49( ) ;
         }
         CloseExtendedTableCursors1C49( ) ;
      }

      protected void Update1C49( )
      {
         BeforeValidate1C49( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1C49( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1C49( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1C49( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1C49( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [MenuPerfil] */
                     DeferredUpdate1C49( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1C49( ) ;
         }
         CloseExtendedTableCursors1C49( ) ;
      }

      protected void DeferredUpdate1C49( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1C49( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1C49( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1C49( ) ;
            AfterConfirm1C49( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1C49( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001C9 */
                  pr_default.execute(7, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("MenuPerfil") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode49 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1C49( ) ;
         Gx_mode = sMode49;
      }

      protected void OnDeleteControls1C49( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC001C10 */
            pr_default.execute(8, new Object[] {A277Menu_Codigo});
            A278Menu_Nome = BC001C10_A278Menu_Nome[0];
            A280Menu_Tipo = BC001C10_A280Menu_Tipo[0];
            A284Menu_Ativo = BC001C10_A284Menu_Ativo[0];
            A283Menu_Ordem = BC001C10_A283Menu_Ordem[0];
            A40000Menu_Imagem_GXI = BC001C10_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = BC001C10_n40000Menu_Imagem_GXI[0];
            A281Menu_Link = BC001C10_A281Menu_Link[0];
            n281Menu_Link = BC001C10_n281Menu_Link[0];
            A282Menu_Imagem = BC001C10_A282Menu_Imagem[0];
            n282Menu_Imagem = BC001C10_n282Menu_Imagem[0];
            pr_default.close(8);
            /* Using cursor BC001C11 */
            pr_default.execute(9, new Object[] {A3Perfil_Codigo});
            A4Perfil_Nome = BC001C11_A4Perfil_Nome[0];
            pr_default.close(9);
         }
      }

      protected void EndLevel1C49( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1C49( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1C49( )
      {
         /* Using cursor BC001C12 */
         pr_default.execute(10, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
         RcdFound49 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound49 = 1;
            A278Menu_Nome = BC001C12_A278Menu_Nome[0];
            A280Menu_Tipo = BC001C12_A280Menu_Tipo[0];
            A284Menu_Ativo = BC001C12_A284Menu_Ativo[0];
            A283Menu_Ordem = BC001C12_A283Menu_Ordem[0];
            A40000Menu_Imagem_GXI = BC001C12_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = BC001C12_n40000Menu_Imagem_GXI[0];
            A281Menu_Link = BC001C12_A281Menu_Link[0];
            n281Menu_Link = BC001C12_n281Menu_Link[0];
            A4Perfil_Nome = BC001C12_A4Perfil_Nome[0];
            A277Menu_Codigo = BC001C12_A277Menu_Codigo[0];
            A3Perfil_Codigo = BC001C12_A3Perfil_Codigo[0];
            A282Menu_Imagem = BC001C12_A282Menu_Imagem[0];
            n282Menu_Imagem = BC001C12_n282Menu_Imagem[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1C49( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound49 = 0;
         ScanKeyLoad1C49( ) ;
      }

      protected void ScanKeyLoad1C49( )
      {
         sMode49 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound49 = 1;
            A278Menu_Nome = BC001C12_A278Menu_Nome[0];
            A280Menu_Tipo = BC001C12_A280Menu_Tipo[0];
            A284Menu_Ativo = BC001C12_A284Menu_Ativo[0];
            A283Menu_Ordem = BC001C12_A283Menu_Ordem[0];
            A40000Menu_Imagem_GXI = BC001C12_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = BC001C12_n40000Menu_Imagem_GXI[0];
            A281Menu_Link = BC001C12_A281Menu_Link[0];
            n281Menu_Link = BC001C12_n281Menu_Link[0];
            A4Perfil_Nome = BC001C12_A4Perfil_Nome[0];
            A277Menu_Codigo = BC001C12_A277Menu_Codigo[0];
            A3Perfil_Codigo = BC001C12_A3Perfil_Codigo[0];
            A282Menu_Imagem = BC001C12_A282Menu_Imagem[0];
            n282Menu_Imagem = BC001C12_n282Menu_Imagem[0];
         }
         Gx_mode = sMode49;
      }

      protected void ScanKeyEnd1C49( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm1C49( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1C49( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1C49( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1C49( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1C49( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1C49( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1C49( )
      {
      }

      protected void AddRow1C49( )
      {
         VarsToRow49( bcMenuPerfil) ;
      }

      protected void ReadRow1C49( )
      {
         RowToVars49( bcMenuPerfil, 1) ;
      }

      protected void InitializeNonKey1C49( )
      {
         A278Menu_Nome = "";
         A280Menu_Tipo = 0;
         A284Menu_Ativo = false;
         A283Menu_Ordem = 0;
         A282Menu_Imagem = "";
         n282Menu_Imagem = false;
         A40000Menu_Imagem_GXI = "";
         n40000Menu_Imagem_GXI = false;
         A281Menu_Link = "";
         n281Menu_Link = false;
         A4Perfil_Nome = "";
      }

      protected void InitAll1C49( )
      {
         A277Menu_Codigo = 0;
         A3Perfil_Codigo = 0;
         InitializeNonKey1C49( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow49( SdtMenuPerfil obj49 )
      {
         obj49.gxTpr_Mode = Gx_mode;
         obj49.gxTpr_Menu_nome = A278Menu_Nome;
         obj49.gxTpr_Menu_tipo = A280Menu_Tipo;
         obj49.gxTpr_Menu_ativo = A284Menu_Ativo;
         obj49.gxTpr_Menu_ordem = A283Menu_Ordem;
         obj49.gxTpr_Menu_imagem = A282Menu_Imagem;
         obj49.gxTpr_Menu_imagem_gxi = A40000Menu_Imagem_GXI;
         obj49.gxTpr_Menu_link = A281Menu_Link;
         obj49.gxTpr_Perfil_nome = A4Perfil_Nome;
         obj49.gxTpr_Menu_codigo = A277Menu_Codigo;
         obj49.gxTpr_Perfil_codigo = A3Perfil_Codigo;
         obj49.gxTpr_Menu_codigo_Z = Z277Menu_Codigo;
         obj49.gxTpr_Menu_nome_Z = Z278Menu_Nome;
         obj49.gxTpr_Menu_tipo_Z = Z280Menu_Tipo;
         obj49.gxTpr_Menu_ativo_Z = Z284Menu_Ativo;
         obj49.gxTpr_Menu_ordem_Z = Z283Menu_Ordem;
         obj49.gxTpr_Menu_link_Z = Z281Menu_Link;
         obj49.gxTpr_Perfil_codigo_Z = Z3Perfil_Codigo;
         obj49.gxTpr_Perfil_nome_Z = Z4Perfil_Nome;
         obj49.gxTpr_Menu_imagem_gxi_Z = Z40000Menu_Imagem_GXI;
         obj49.gxTpr_Menu_imagem_N = (short)(Convert.ToInt16(n282Menu_Imagem));
         obj49.gxTpr_Menu_link_N = (short)(Convert.ToInt16(n281Menu_Link));
         obj49.gxTpr_Menu_imagem_gxi_N = (short)(Convert.ToInt16(n40000Menu_Imagem_GXI));
         obj49.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow49( SdtMenuPerfil obj49 )
      {
         obj49.gxTpr_Menu_codigo = A277Menu_Codigo;
         obj49.gxTpr_Perfil_codigo = A3Perfil_Codigo;
         return  ;
      }

      public void RowToVars49( SdtMenuPerfil obj49 ,
                               int forceLoad )
      {
         Gx_mode = obj49.gxTpr_Mode;
         A278Menu_Nome = obj49.gxTpr_Menu_nome;
         A280Menu_Tipo = obj49.gxTpr_Menu_tipo;
         A284Menu_Ativo = obj49.gxTpr_Menu_ativo;
         A283Menu_Ordem = obj49.gxTpr_Menu_ordem;
         A282Menu_Imagem = obj49.gxTpr_Menu_imagem;
         n282Menu_Imagem = false;
         A40000Menu_Imagem_GXI = obj49.gxTpr_Menu_imagem_gxi;
         n40000Menu_Imagem_GXI = false;
         A281Menu_Link = obj49.gxTpr_Menu_link;
         n281Menu_Link = false;
         A4Perfil_Nome = obj49.gxTpr_Perfil_nome;
         A277Menu_Codigo = obj49.gxTpr_Menu_codigo;
         A3Perfil_Codigo = obj49.gxTpr_Perfil_codigo;
         Z277Menu_Codigo = obj49.gxTpr_Menu_codigo_Z;
         Z278Menu_Nome = obj49.gxTpr_Menu_nome_Z;
         Z280Menu_Tipo = obj49.gxTpr_Menu_tipo_Z;
         Z284Menu_Ativo = obj49.gxTpr_Menu_ativo_Z;
         Z283Menu_Ordem = obj49.gxTpr_Menu_ordem_Z;
         Z281Menu_Link = obj49.gxTpr_Menu_link_Z;
         Z3Perfil_Codigo = obj49.gxTpr_Perfil_codigo_Z;
         Z4Perfil_Nome = obj49.gxTpr_Perfil_nome_Z;
         Z40000Menu_Imagem_GXI = obj49.gxTpr_Menu_imagem_gxi_Z;
         n282Menu_Imagem = (bool)(Convert.ToBoolean(obj49.gxTpr_Menu_imagem_N));
         n281Menu_Link = (bool)(Convert.ToBoolean(obj49.gxTpr_Menu_link_N));
         n40000Menu_Imagem_GXI = (bool)(Convert.ToBoolean(obj49.gxTpr_Menu_imagem_gxi_N));
         Gx_mode = obj49.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A277Menu_Codigo = (int)getParm(obj,0);
         A3Perfil_Codigo = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1C49( ) ;
         ScanKeyStart1C49( ) ;
         if ( RcdFound49 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001C10 */
            pr_default.execute(8, new Object[] {A277Menu_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Menu'.", "ForeignKeyNotFound", 1, "MENU_CODIGO");
               AnyError = 1;
            }
            A278Menu_Nome = BC001C10_A278Menu_Nome[0];
            A280Menu_Tipo = BC001C10_A280Menu_Tipo[0];
            A284Menu_Ativo = BC001C10_A284Menu_Ativo[0];
            A283Menu_Ordem = BC001C10_A283Menu_Ordem[0];
            A40000Menu_Imagem_GXI = BC001C10_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = BC001C10_n40000Menu_Imagem_GXI[0];
            A281Menu_Link = BC001C10_A281Menu_Link[0];
            n281Menu_Link = BC001C10_n281Menu_Link[0];
            A282Menu_Imagem = BC001C10_A282Menu_Imagem[0];
            n282Menu_Imagem = BC001C10_n282Menu_Imagem[0];
            pr_default.close(8);
            /* Using cursor BC001C11 */
            pr_default.execute(9, new Object[] {A3Perfil_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
               AnyError = 1;
            }
            A4Perfil_Nome = BC001C11_A4Perfil_Nome[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z277Menu_Codigo = A277Menu_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
         }
         ZM1C49( -1) ;
         OnLoadActions1C49( ) ;
         AddRow1C49( ) ;
         ScanKeyEnd1C49( ) ;
         if ( RcdFound49 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars49( bcMenuPerfil, 0) ;
         ScanKeyStart1C49( ) ;
         if ( RcdFound49 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC001C10 */
            pr_default.execute(8, new Object[] {A277Menu_Codigo});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Menu'.", "ForeignKeyNotFound", 1, "MENU_CODIGO");
               AnyError = 1;
            }
            A278Menu_Nome = BC001C10_A278Menu_Nome[0];
            A280Menu_Tipo = BC001C10_A280Menu_Tipo[0];
            A284Menu_Ativo = BC001C10_A284Menu_Ativo[0];
            A283Menu_Ordem = BC001C10_A283Menu_Ordem[0];
            A40000Menu_Imagem_GXI = BC001C10_A40000Menu_Imagem_GXI[0];
            n40000Menu_Imagem_GXI = BC001C10_n40000Menu_Imagem_GXI[0];
            A281Menu_Link = BC001C10_A281Menu_Link[0];
            n281Menu_Link = BC001C10_n281Menu_Link[0];
            A282Menu_Imagem = BC001C10_A282Menu_Imagem[0];
            n282Menu_Imagem = BC001C10_n282Menu_Imagem[0];
            pr_default.close(8);
            /* Using cursor BC001C11 */
            pr_default.execute(9, new Object[] {A3Perfil_Codigo});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
               AnyError = 1;
            }
            A4Perfil_Nome = BC001C11_A4Perfil_Nome[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z277Menu_Codigo = A277Menu_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
         }
         ZM1C49( -1) ;
         OnLoadActions1C49( ) ;
         AddRow1C49( ) ;
         ScanKeyEnd1C49( ) ;
         if ( RcdFound49 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars49( bcMenuPerfil, 0) ;
         nKeyPressed = 1;
         GetKey1C49( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1C49( ) ;
         }
         else
         {
            if ( RcdFound49 == 1 )
            {
               if ( ( A277Menu_Codigo != Z277Menu_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
               {
                  A277Menu_Codigo = Z277Menu_Codigo;
                  A3Perfil_Codigo = Z3Perfil_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1C49( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A277Menu_Codigo != Z277Menu_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1C49( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1C49( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow49( bcMenuPerfil) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars49( bcMenuPerfil, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1C49( ) ;
         if ( RcdFound49 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A277Menu_Codigo != Z277Menu_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
            {
               A277Menu_Codigo = Z277Menu_Codigo;
               A3Perfil_Codigo = Z3Perfil_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A277Menu_Codigo != Z277Menu_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
         context.RollbackDataStores( "MenuPerfil_BC");
         VarsToRow49( bcMenuPerfil) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcMenuPerfil.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcMenuPerfil.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcMenuPerfil )
         {
            bcMenuPerfil = (SdtMenuPerfil)(sdt);
            if ( StringUtil.StrCmp(bcMenuPerfil.gxTpr_Mode, "") == 0 )
            {
               bcMenuPerfil.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow49( bcMenuPerfil) ;
            }
            else
            {
               RowToVars49( bcMenuPerfil, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcMenuPerfil.gxTpr_Mode, "") == 0 )
            {
               bcMenuPerfil.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars49( bcMenuPerfil, 1) ;
         return  ;
      }

      public SdtMenuPerfil MenuPerfil_BC
      {
         get {
            return bcMenuPerfil ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(8);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z278Menu_Nome = "";
         A278Menu_Nome = "";
         Z281Menu_Link = "";
         A281Menu_Link = "";
         Z4Perfil_Nome = "";
         A4Perfil_Nome = "";
         Z282Menu_Imagem = "";
         A282Menu_Imagem = "";
         Z40000Menu_Imagem_GXI = "";
         A40000Menu_Imagem_GXI = "";
         BC001C6_A278Menu_Nome = new String[] {""} ;
         BC001C6_A280Menu_Tipo = new short[1] ;
         BC001C6_A284Menu_Ativo = new bool[] {false} ;
         BC001C6_A283Menu_Ordem = new short[1] ;
         BC001C6_A40000Menu_Imagem_GXI = new String[] {""} ;
         BC001C6_n40000Menu_Imagem_GXI = new bool[] {false} ;
         BC001C6_A281Menu_Link = new String[] {""} ;
         BC001C6_n281Menu_Link = new bool[] {false} ;
         BC001C6_A4Perfil_Nome = new String[] {""} ;
         BC001C6_A277Menu_Codigo = new int[1] ;
         BC001C6_A3Perfil_Codigo = new int[1] ;
         BC001C6_A282Menu_Imagem = new String[] {""} ;
         BC001C6_n282Menu_Imagem = new bool[] {false} ;
         BC001C4_A278Menu_Nome = new String[] {""} ;
         BC001C4_A280Menu_Tipo = new short[1] ;
         BC001C4_A284Menu_Ativo = new bool[] {false} ;
         BC001C4_A283Menu_Ordem = new short[1] ;
         BC001C4_A40000Menu_Imagem_GXI = new String[] {""} ;
         BC001C4_n40000Menu_Imagem_GXI = new bool[] {false} ;
         BC001C4_A281Menu_Link = new String[] {""} ;
         BC001C4_n281Menu_Link = new bool[] {false} ;
         BC001C4_A282Menu_Imagem = new String[] {""} ;
         BC001C4_n282Menu_Imagem = new bool[] {false} ;
         BC001C5_A4Perfil_Nome = new String[] {""} ;
         BC001C7_A277Menu_Codigo = new int[1] ;
         BC001C7_A3Perfil_Codigo = new int[1] ;
         BC001C3_A277Menu_Codigo = new int[1] ;
         BC001C3_A3Perfil_Codigo = new int[1] ;
         sMode49 = "";
         BC001C2_A277Menu_Codigo = new int[1] ;
         BC001C2_A3Perfil_Codigo = new int[1] ;
         BC001C10_A278Menu_Nome = new String[] {""} ;
         BC001C10_A280Menu_Tipo = new short[1] ;
         BC001C10_A284Menu_Ativo = new bool[] {false} ;
         BC001C10_A283Menu_Ordem = new short[1] ;
         BC001C10_A40000Menu_Imagem_GXI = new String[] {""} ;
         BC001C10_n40000Menu_Imagem_GXI = new bool[] {false} ;
         BC001C10_A281Menu_Link = new String[] {""} ;
         BC001C10_n281Menu_Link = new bool[] {false} ;
         BC001C10_A282Menu_Imagem = new String[] {""} ;
         BC001C10_n282Menu_Imagem = new bool[] {false} ;
         BC001C11_A4Perfil_Nome = new String[] {""} ;
         BC001C12_A278Menu_Nome = new String[] {""} ;
         BC001C12_A280Menu_Tipo = new short[1] ;
         BC001C12_A284Menu_Ativo = new bool[] {false} ;
         BC001C12_A283Menu_Ordem = new short[1] ;
         BC001C12_A40000Menu_Imagem_GXI = new String[] {""} ;
         BC001C12_n40000Menu_Imagem_GXI = new bool[] {false} ;
         BC001C12_A281Menu_Link = new String[] {""} ;
         BC001C12_n281Menu_Link = new bool[] {false} ;
         BC001C12_A4Perfil_Nome = new String[] {""} ;
         BC001C12_A277Menu_Codigo = new int[1] ;
         BC001C12_A3Perfil_Codigo = new int[1] ;
         BC001C12_A282Menu_Imagem = new String[] {""} ;
         BC001C12_n282Menu_Imagem = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.menuperfil_bc__default(),
            new Object[][] {
                new Object[] {
               BC001C2_A277Menu_Codigo, BC001C2_A3Perfil_Codigo
               }
               , new Object[] {
               BC001C3_A277Menu_Codigo, BC001C3_A3Perfil_Codigo
               }
               , new Object[] {
               BC001C4_A278Menu_Nome, BC001C4_A280Menu_Tipo, BC001C4_A284Menu_Ativo, BC001C4_A283Menu_Ordem, BC001C4_A40000Menu_Imagem_GXI, BC001C4_n40000Menu_Imagem_GXI, BC001C4_A281Menu_Link, BC001C4_n281Menu_Link, BC001C4_A282Menu_Imagem, BC001C4_n282Menu_Imagem
               }
               , new Object[] {
               BC001C5_A4Perfil_Nome
               }
               , new Object[] {
               BC001C6_A278Menu_Nome, BC001C6_A280Menu_Tipo, BC001C6_A284Menu_Ativo, BC001C6_A283Menu_Ordem, BC001C6_A40000Menu_Imagem_GXI, BC001C6_n40000Menu_Imagem_GXI, BC001C6_A281Menu_Link, BC001C6_n281Menu_Link, BC001C6_A4Perfil_Nome, BC001C6_A277Menu_Codigo,
               BC001C6_A3Perfil_Codigo, BC001C6_A282Menu_Imagem, BC001C6_n282Menu_Imagem
               }
               , new Object[] {
               BC001C7_A277Menu_Codigo, BC001C7_A3Perfil_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001C10_A278Menu_Nome, BC001C10_A280Menu_Tipo, BC001C10_A284Menu_Ativo, BC001C10_A283Menu_Ordem, BC001C10_A40000Menu_Imagem_GXI, BC001C10_n40000Menu_Imagem_GXI, BC001C10_A281Menu_Link, BC001C10_n281Menu_Link, BC001C10_A282Menu_Imagem, BC001C10_n282Menu_Imagem
               }
               , new Object[] {
               BC001C11_A4Perfil_Nome
               }
               , new Object[] {
               BC001C12_A278Menu_Nome, BC001C12_A280Menu_Tipo, BC001C12_A284Menu_Ativo, BC001C12_A283Menu_Ordem, BC001C12_A40000Menu_Imagem_GXI, BC001C12_n40000Menu_Imagem_GXI, BC001C12_A281Menu_Link, BC001C12_n281Menu_Link, BC001C12_A4Perfil_Nome, BC001C12_A277Menu_Codigo,
               BC001C12_A3Perfil_Codigo, BC001C12_A282Menu_Imagem, BC001C12_n282Menu_Imagem
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z280Menu_Tipo ;
      private short A280Menu_Tipo ;
      private short Z283Menu_Ordem ;
      private short A283Menu_Ordem ;
      private short RcdFound49 ;
      private int trnEnded ;
      private int Z277Menu_Codigo ;
      private int A277Menu_Codigo ;
      private int Z3Perfil_Codigo ;
      private int A3Perfil_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z278Menu_Nome ;
      private String A278Menu_Nome ;
      private String Z4Perfil_Nome ;
      private String A4Perfil_Nome ;
      private String sMode49 ;
      private bool Z284Menu_Ativo ;
      private bool A284Menu_Ativo ;
      private bool n40000Menu_Imagem_GXI ;
      private bool n281Menu_Link ;
      private bool n282Menu_Imagem ;
      private String Z281Menu_Link ;
      private String A281Menu_Link ;
      private String Z40000Menu_Imagem_GXI ;
      private String A40000Menu_Imagem_GXI ;
      private String Z282Menu_Imagem ;
      private String A282Menu_Imagem ;
      private SdtMenuPerfil bcMenuPerfil ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC001C6_A278Menu_Nome ;
      private short[] BC001C6_A280Menu_Tipo ;
      private bool[] BC001C6_A284Menu_Ativo ;
      private short[] BC001C6_A283Menu_Ordem ;
      private String[] BC001C6_A40000Menu_Imagem_GXI ;
      private bool[] BC001C6_n40000Menu_Imagem_GXI ;
      private String[] BC001C6_A281Menu_Link ;
      private bool[] BC001C6_n281Menu_Link ;
      private String[] BC001C6_A4Perfil_Nome ;
      private int[] BC001C6_A277Menu_Codigo ;
      private int[] BC001C6_A3Perfil_Codigo ;
      private String[] BC001C6_A282Menu_Imagem ;
      private bool[] BC001C6_n282Menu_Imagem ;
      private String[] BC001C4_A278Menu_Nome ;
      private short[] BC001C4_A280Menu_Tipo ;
      private bool[] BC001C4_A284Menu_Ativo ;
      private short[] BC001C4_A283Menu_Ordem ;
      private String[] BC001C4_A40000Menu_Imagem_GXI ;
      private bool[] BC001C4_n40000Menu_Imagem_GXI ;
      private String[] BC001C4_A281Menu_Link ;
      private bool[] BC001C4_n281Menu_Link ;
      private String[] BC001C4_A282Menu_Imagem ;
      private bool[] BC001C4_n282Menu_Imagem ;
      private String[] BC001C5_A4Perfil_Nome ;
      private int[] BC001C7_A277Menu_Codigo ;
      private int[] BC001C7_A3Perfil_Codigo ;
      private int[] BC001C3_A277Menu_Codigo ;
      private int[] BC001C3_A3Perfil_Codigo ;
      private int[] BC001C2_A277Menu_Codigo ;
      private int[] BC001C2_A3Perfil_Codigo ;
      private String[] BC001C10_A278Menu_Nome ;
      private short[] BC001C10_A280Menu_Tipo ;
      private bool[] BC001C10_A284Menu_Ativo ;
      private short[] BC001C10_A283Menu_Ordem ;
      private String[] BC001C10_A40000Menu_Imagem_GXI ;
      private bool[] BC001C10_n40000Menu_Imagem_GXI ;
      private String[] BC001C10_A281Menu_Link ;
      private bool[] BC001C10_n281Menu_Link ;
      private String[] BC001C10_A282Menu_Imagem ;
      private bool[] BC001C10_n282Menu_Imagem ;
      private String[] BC001C11_A4Perfil_Nome ;
      private String[] BC001C12_A278Menu_Nome ;
      private short[] BC001C12_A280Menu_Tipo ;
      private bool[] BC001C12_A284Menu_Ativo ;
      private short[] BC001C12_A283Menu_Ordem ;
      private String[] BC001C12_A40000Menu_Imagem_GXI ;
      private bool[] BC001C12_n40000Menu_Imagem_GXI ;
      private String[] BC001C12_A281Menu_Link ;
      private bool[] BC001C12_n281Menu_Link ;
      private String[] BC001C12_A4Perfil_Nome ;
      private int[] BC001C12_A277Menu_Codigo ;
      private int[] BC001C12_A3Perfil_Codigo ;
      private String[] BC001C12_A282Menu_Imagem ;
      private bool[] BC001C12_n282Menu_Imagem ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class menuperfil_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC001C6 ;
          prmBC001C6 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001C4 ;
          prmBC001C4 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001C5 ;
          prmBC001C5 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001C7 ;
          prmBC001C7 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001C3 ;
          prmBC001C3 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001C2 ;
          prmBC001C2 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001C8 ;
          prmBC001C8 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001C9 ;
          prmBC001C9 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001C12 ;
          prmBC001C12 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001C10 ;
          prmBC001C10 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001C11 ;
          prmBC001C11 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC001C2", "SELECT [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (UPDLOCK) WHERE [Menu_Codigo] = @Menu_Codigo AND [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001C2,1,0,true,false )
             ,new CursorDef("BC001C3", "SELECT [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo AND [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001C3,1,0,true,false )
             ,new CursorDef("BC001C4", "SELECT [Menu_Nome], [Menu_Tipo], [Menu_Ativo], [Menu_Ordem], [Menu_Imagem_GXI], [Menu_Link], [Menu_Imagem] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001C4,1,0,true,false )
             ,new CursorDef("BC001C5", "SELECT [Perfil_Nome] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001C5,1,0,true,false )
             ,new CursorDef("BC001C6", "SELECT T2.[Menu_Nome], T2.[Menu_Tipo], T2.[Menu_Ativo], T2.[Menu_Ordem], T2.[Menu_Imagem_GXI], T2.[Menu_Link], T3.[Perfil_Nome], TM1.[Menu_Codigo], TM1.[Perfil_Codigo], T2.[Menu_Imagem] FROM (([MenuPerfil] TM1 WITH (NOLOCK) INNER JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = TM1.[Menu_Codigo]) INNER JOIN [Perfil] T3 WITH (NOLOCK) ON T3.[Perfil_Codigo] = TM1.[Perfil_Codigo]) WHERE TM1.[Menu_Codigo] = @Menu_Codigo and TM1.[Perfil_Codigo] = @Perfil_Codigo ORDER BY TM1.[Menu_Codigo], TM1.[Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001C6,100,0,true,false )
             ,new CursorDef("BC001C7", "SELECT [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo AND [Perfil_Codigo] = @Perfil_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001C7,1,0,true,false )
             ,new CursorDef("BC001C8", "INSERT INTO [MenuPerfil]([Menu_Codigo], [Perfil_Codigo]) VALUES(@Menu_Codigo, @Perfil_Codigo)", GxErrorMask.GX_NOMASK,prmBC001C8)
             ,new CursorDef("BC001C9", "DELETE FROM [MenuPerfil]  WHERE [Menu_Codigo] = @Menu_Codigo AND [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK,prmBC001C9)
             ,new CursorDef("BC001C10", "SELECT [Menu_Nome], [Menu_Tipo], [Menu_Ativo], [Menu_Ordem], [Menu_Imagem_GXI], [Menu_Link], [Menu_Imagem] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001C10,1,0,true,false )
             ,new CursorDef("BC001C11", "SELECT [Perfil_Nome] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001C11,1,0,true,false )
             ,new CursorDef("BC001C12", "SELECT T2.[Menu_Nome], T2.[Menu_Tipo], T2.[Menu_Ativo], T2.[Menu_Ordem], T2.[Menu_Imagem_GXI], T2.[Menu_Link], T3.[Perfil_Nome], TM1.[Menu_Codigo], TM1.[Perfil_Codigo], T2.[Menu_Imagem] FROM (([MenuPerfil] TM1 WITH (NOLOCK) INNER JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = TM1.[Menu_Codigo]) INNER JOIN [Perfil] T3 WITH (NOLOCK) ON T3.[Perfil_Codigo] = TM1.[Perfil_Codigo]) WHERE TM1.[Menu_Codigo] = @Menu_Codigo and TM1.[Perfil_Codigo] = @Perfil_Codigo ORDER BY TM1.[Menu_Codigo], TM1.[Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001C12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(5)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((String[]) buf[11])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(5)) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(10);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(5)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((String[]) buf[11])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(5)) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
