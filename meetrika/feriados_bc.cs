/*
               File: Feriados_BC
        Description: Feriados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:27:24.35
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class feriados_bc : GXHttpHandler, IGxSilentTrn
   {
      public feriados_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public feriados_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow39137( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey39137( ) ;
         standaloneModal( ) ;
         AddRow39137( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11392 */
            E11392 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1175Feriado_Data = A1175Feriado_Data;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_390( )
      {
         BeforeValidate39137( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls39137( ) ;
            }
            else
            {
               CheckExtendedTable39137( ) ;
               if ( AnyError == 0 )
               {
               }
               CloseExtendedTableCursors39137( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12392( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
      }

      protected void E11392( )
      {
         /* After Trn Routine */
      }

      protected void ZM39137( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z1176Feriado_Nome = A1176Feriado_Nome;
            Z1195Feriado_Fixo = A1195Feriado_Fixo;
         }
         if ( GX_JID == -2 )
         {
            Z1175Feriado_Data = A1175Feriado_Data;
            Z1176Feriado_Nome = A1176Feriado_Nome;
            Z1195Feriado_Fixo = A1195Feriado_Fixo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load39137( )
      {
         /* Using cursor BC00394 */
         pr_default.execute(2, new Object[] {A1175Feriado_Data});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound137 = 1;
            A1176Feriado_Nome = BC00394_A1176Feriado_Nome[0];
            A1195Feriado_Fixo = BC00394_A1195Feriado_Fixo[0];
            n1195Feriado_Fixo = BC00394_n1195Feriado_Fixo[0];
            ZM39137( -2) ;
         }
         pr_default.close(2);
         OnLoadActions39137( ) ;
      }

      protected void OnLoadActions39137( )
      {
      }

      protected void CheckExtendedTable39137( )
      {
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1175Feriado_Data) || ( A1175Feriado_Data >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors39137( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey39137( )
      {
         /* Using cursor BC00395 */
         pr_default.execute(3, new Object[] {A1175Feriado_Data});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound137 = 1;
         }
         else
         {
            RcdFound137 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00393 */
         pr_default.execute(1, new Object[] {A1175Feriado_Data});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM39137( 2) ;
            RcdFound137 = 1;
            A1175Feriado_Data = BC00393_A1175Feriado_Data[0];
            A1176Feriado_Nome = BC00393_A1176Feriado_Nome[0];
            A1195Feriado_Fixo = BC00393_A1195Feriado_Fixo[0];
            n1195Feriado_Fixo = BC00393_n1195Feriado_Fixo[0];
            Z1175Feriado_Data = A1175Feriado_Data;
            sMode137 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load39137( ) ;
            if ( AnyError == 1 )
            {
               RcdFound137 = 0;
               InitializeNonKey39137( ) ;
            }
            Gx_mode = sMode137;
         }
         else
         {
            RcdFound137 = 0;
            InitializeNonKey39137( ) ;
            sMode137 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode137;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey39137( ) ;
         if ( RcdFound137 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_390( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency39137( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00392 */
            pr_default.execute(0, new Object[] {A1175Feriado_Data});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Feriados"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1176Feriado_Nome, BC00392_A1176Feriado_Nome[0]) != 0 ) || ( Z1195Feriado_Fixo != BC00392_A1195Feriado_Fixo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Feriados"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert39137( )
      {
         BeforeValidate39137( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable39137( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM39137( 0) ;
            CheckOptimisticConcurrency39137( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm39137( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert39137( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00396 */
                     pr_default.execute(4, new Object[] {A1175Feriado_Data, A1176Feriado_Nome, n1195Feriado_Fixo, A1195Feriado_Fixo});
                     pr_default.close(4);
                     dsDefault.SmartCacheProvider.SetUpdated("Feriados") ;
                     if ( (pr_default.getStatus(4) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load39137( ) ;
            }
            EndLevel39137( ) ;
         }
         CloseExtendedTableCursors39137( ) ;
      }

      protected void Update39137( )
      {
         BeforeValidate39137( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable39137( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency39137( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm39137( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate39137( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00397 */
                     pr_default.execute(5, new Object[] {A1176Feriado_Nome, n1195Feriado_Fixo, A1195Feriado_Fixo, A1175Feriado_Data});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("Feriados") ;
                     if ( (pr_default.getStatus(5) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Feriados"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate39137( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel39137( ) ;
         }
         CloseExtendedTableCursors39137( ) ;
      }

      protected void DeferredUpdate39137( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate39137( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency39137( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls39137( ) ;
            AfterConfirm39137( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete39137( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC00398 */
                  pr_default.execute(6, new Object[] {A1175Feriado_Data});
                  pr_default.close(6);
                  dsDefault.SmartCacheProvider.SetUpdated("Feriados") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode137 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel39137( ) ;
         Gx_mode = sMode137;
      }

      protected void OnDeleteControls39137( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel39137( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete39137( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart39137( )
      {
         /* Scan By routine */
         /* Using cursor BC00399 */
         pr_default.execute(7, new Object[] {A1175Feriado_Data});
         RcdFound137 = 0;
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound137 = 1;
            A1175Feriado_Data = BC00399_A1175Feriado_Data[0];
            A1176Feriado_Nome = BC00399_A1176Feriado_Nome[0];
            A1195Feriado_Fixo = BC00399_A1195Feriado_Fixo[0];
            n1195Feriado_Fixo = BC00399_n1195Feriado_Fixo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext39137( )
      {
         /* Scan next routine */
         pr_default.readNext(7);
         RcdFound137 = 0;
         ScanKeyLoad39137( ) ;
      }

      protected void ScanKeyLoad39137( )
      {
         sMode137 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound137 = 1;
            A1175Feriado_Data = BC00399_A1175Feriado_Data[0];
            A1176Feriado_Nome = BC00399_A1176Feriado_Nome[0];
            A1195Feriado_Fixo = BC00399_A1195Feriado_Fixo[0];
            n1195Feriado_Fixo = BC00399_n1195Feriado_Fixo[0];
         }
         Gx_mode = sMode137;
      }

      protected void ScanKeyEnd39137( )
      {
         pr_default.close(7);
      }

      protected void AfterConfirm39137( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert39137( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate39137( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete39137( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete39137( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate39137( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes39137( )
      {
      }

      protected void AddRow39137( )
      {
         VarsToRow137( bcFeriados) ;
      }

      protected void ReadRow39137( )
      {
         RowToVars137( bcFeriados, 1) ;
      }

      protected void InitializeNonKey39137( )
      {
         A1176Feriado_Nome = "";
         A1195Feriado_Fixo = false;
         n1195Feriado_Fixo = false;
         Z1176Feriado_Nome = "";
         Z1195Feriado_Fixo = false;
      }

      protected void InitAll39137( )
      {
         A1175Feriado_Data = DateTime.MinValue;
         InitializeNonKey39137( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow137( SdtFeriados obj137 )
      {
         obj137.gxTpr_Mode = Gx_mode;
         obj137.gxTpr_Feriado_nome = A1176Feriado_Nome;
         obj137.gxTpr_Feriado_fixo = A1195Feriado_Fixo;
         obj137.gxTpr_Feriado_data = A1175Feriado_Data;
         obj137.gxTpr_Feriado_data_Z = Z1175Feriado_Data;
         obj137.gxTpr_Feriado_nome_Z = Z1176Feriado_Nome;
         obj137.gxTpr_Feriado_fixo_Z = Z1195Feriado_Fixo;
         obj137.gxTpr_Feriado_fixo_N = (short)(Convert.ToInt16(n1195Feriado_Fixo));
         obj137.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow137( SdtFeriados obj137 )
      {
         obj137.gxTpr_Feriado_data = A1175Feriado_Data;
         return  ;
      }

      public void RowToVars137( SdtFeriados obj137 ,
                                int forceLoad )
      {
         Gx_mode = obj137.gxTpr_Mode;
         A1176Feriado_Nome = obj137.gxTpr_Feriado_nome;
         A1195Feriado_Fixo = obj137.gxTpr_Feriado_fixo;
         n1195Feriado_Fixo = false;
         A1175Feriado_Data = obj137.gxTpr_Feriado_data;
         Z1175Feriado_Data = obj137.gxTpr_Feriado_data_Z;
         Z1176Feriado_Nome = obj137.gxTpr_Feriado_nome_Z;
         Z1195Feriado_Fixo = obj137.gxTpr_Feriado_fixo_Z;
         n1195Feriado_Fixo = (bool)(Convert.ToBoolean(obj137.gxTpr_Feriado_fixo_N));
         Gx_mode = obj137.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1175Feriado_Data = (DateTime)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey39137( ) ;
         ScanKeyStart39137( ) ;
         if ( RcdFound137 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1175Feriado_Data = A1175Feriado_Data;
         }
         ZM39137( -2) ;
         OnLoadActions39137( ) ;
         AddRow39137( ) ;
         ScanKeyEnd39137( ) ;
         if ( RcdFound137 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars137( bcFeriados, 0) ;
         ScanKeyStart39137( ) ;
         if ( RcdFound137 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1175Feriado_Data = A1175Feriado_Data;
         }
         ZM39137( -2) ;
         OnLoadActions39137( ) ;
         AddRow39137( ) ;
         ScanKeyEnd39137( ) ;
         if ( RcdFound137 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars137( bcFeriados, 0) ;
         nKeyPressed = 1;
         GetKey39137( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert39137( ) ;
         }
         else
         {
            if ( RcdFound137 == 1 )
            {
               if ( A1175Feriado_Data != Z1175Feriado_Data )
               {
                  A1175Feriado_Data = Z1175Feriado_Data;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update39137( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1175Feriado_Data != Z1175Feriado_Data )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert39137( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert39137( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow137( bcFeriados) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars137( bcFeriados, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey39137( ) ;
         if ( RcdFound137 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1175Feriado_Data != Z1175Feriado_Data )
            {
               A1175Feriado_Data = Z1175Feriado_Data;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1175Feriado_Data != Z1175Feriado_Data )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         context.RollbackDataStores( "Feriados_BC");
         VarsToRow137( bcFeriados) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcFeriados.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcFeriados.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcFeriados )
         {
            bcFeriados = (SdtFeriados)(sdt);
            if ( StringUtil.StrCmp(bcFeriados.gxTpr_Mode, "") == 0 )
            {
               bcFeriados.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow137( bcFeriados) ;
            }
            else
            {
               RowToVars137( bcFeriados, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcFeriados.gxTpr_Mode, "") == 0 )
            {
               bcFeriados.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars137( bcFeriados, 1) ;
         return  ;
      }

      public SdtFeriados Feriados_BC
      {
         get {
            return bcFeriados ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1175Feriado_Data = DateTime.MinValue;
         A1175Feriado_Data = DateTime.MinValue;
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         Z1176Feriado_Nome = "";
         A1176Feriado_Nome = "";
         BC00394_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         BC00394_A1176Feriado_Nome = new String[] {""} ;
         BC00394_A1195Feriado_Fixo = new bool[] {false} ;
         BC00394_n1195Feriado_Fixo = new bool[] {false} ;
         BC00395_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         BC00393_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         BC00393_A1176Feriado_Nome = new String[] {""} ;
         BC00393_A1195Feriado_Fixo = new bool[] {false} ;
         BC00393_n1195Feriado_Fixo = new bool[] {false} ;
         sMode137 = "";
         BC00392_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         BC00392_A1176Feriado_Nome = new String[] {""} ;
         BC00392_A1195Feriado_Fixo = new bool[] {false} ;
         BC00392_n1195Feriado_Fixo = new bool[] {false} ;
         BC00399_A1175Feriado_Data = new DateTime[] {DateTime.MinValue} ;
         BC00399_A1176Feriado_Nome = new String[] {""} ;
         BC00399_A1195Feriado_Fixo = new bool[] {false} ;
         BC00399_n1195Feriado_Fixo = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.feriados_bc__default(),
            new Object[][] {
                new Object[] {
               BC00392_A1175Feriado_Data, BC00392_A1176Feriado_Nome, BC00392_A1195Feriado_Fixo, BC00392_n1195Feriado_Fixo
               }
               , new Object[] {
               BC00393_A1175Feriado_Data, BC00393_A1176Feriado_Nome, BC00393_A1195Feriado_Fixo, BC00393_n1195Feriado_Fixo
               }
               , new Object[] {
               BC00394_A1175Feriado_Data, BC00394_A1176Feriado_Nome, BC00394_A1195Feriado_Fixo, BC00394_n1195Feriado_Fixo
               }
               , new Object[] {
               BC00395_A1175Feriado_Data
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC00399_A1175Feriado_Data, BC00399_A1176Feriado_Nome, BC00399_A1195Feriado_Fixo, BC00399_n1195Feriado_Fixo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12392 */
         E12392 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound137 ;
      private int trnEnded ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z1176Feriado_Nome ;
      private String A1176Feriado_Nome ;
      private String sMode137 ;
      private DateTime Z1175Feriado_Data ;
      private DateTime A1175Feriado_Data ;
      private bool Z1195Feriado_Fixo ;
      private bool A1195Feriado_Fixo ;
      private bool n1195Feriado_Fixo ;
      private IGxSession AV10WebSession ;
      private SdtFeriados bcFeriados ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private DateTime[] BC00394_A1175Feriado_Data ;
      private String[] BC00394_A1176Feriado_Nome ;
      private bool[] BC00394_A1195Feriado_Fixo ;
      private bool[] BC00394_n1195Feriado_Fixo ;
      private DateTime[] BC00395_A1175Feriado_Data ;
      private DateTime[] BC00393_A1175Feriado_Data ;
      private String[] BC00393_A1176Feriado_Nome ;
      private bool[] BC00393_A1195Feriado_Fixo ;
      private bool[] BC00393_n1195Feriado_Fixo ;
      private DateTime[] BC00392_A1175Feriado_Data ;
      private String[] BC00392_A1176Feriado_Nome ;
      private bool[] BC00392_A1195Feriado_Fixo ;
      private bool[] BC00392_n1195Feriado_Fixo ;
      private DateTime[] BC00399_A1175Feriado_Data ;
      private String[] BC00399_A1176Feriado_Nome ;
      private bool[] BC00399_A1195Feriado_Fixo ;
      private bool[] BC00399_n1195Feriado_Fixo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class feriados_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00394 ;
          prmBC00394 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmBC00395 ;
          prmBC00395 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmBC00393 ;
          prmBC00393 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmBC00392 ;
          prmBC00392 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmBC00396 ;
          prmBC00396 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Feriado_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Feriado_Fixo",SqlDbType.Bit,4,0}
          } ;
          Object[] prmBC00397 ;
          prmBC00397 = new Object[] {
          new Object[] {"@Feriado_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Feriado_Fixo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmBC00398 ;
          prmBC00398 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          Object[] prmBC00399 ;
          prmBC00399 = new Object[] {
          new Object[] {"@Feriado_Data",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00392", "SELECT [Feriado_Data], [Feriado_Nome], [Feriado_Fixo] FROM [Feriados] WITH (UPDLOCK) WHERE [Feriado_Data] = @Feriado_Data ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00392,1,0,true,false )
             ,new CursorDef("BC00393", "SELECT [Feriado_Data], [Feriado_Nome], [Feriado_Fixo] FROM [Feriados] WITH (NOLOCK) WHERE [Feriado_Data] = @Feriado_Data ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00393,1,0,true,false )
             ,new CursorDef("BC00394", "SELECT TM1.[Feriado_Data], TM1.[Feriado_Nome], TM1.[Feriado_Fixo] FROM [Feriados] TM1 WITH (NOLOCK) WHERE TM1.[Feriado_Data] = @Feriado_Data ORDER BY TM1.[Feriado_Data]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00394,100,0,true,false )
             ,new CursorDef("BC00395", "SELECT [Feriado_Data] FROM [Feriados] WITH (NOLOCK) WHERE [Feriado_Data] = @Feriado_Data  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00395,1,0,true,false )
             ,new CursorDef("BC00396", "INSERT INTO [Feriados]([Feriado_Data], [Feriado_Nome], [Feriado_Fixo]) VALUES(@Feriado_Data, @Feriado_Nome, @Feriado_Fixo)", GxErrorMask.GX_NOMASK,prmBC00396)
             ,new CursorDef("BC00397", "UPDATE [Feriados] SET [Feriado_Nome]=@Feriado_Nome, [Feriado_Fixo]=@Feriado_Fixo  WHERE [Feriado_Data] = @Feriado_Data", GxErrorMask.GX_NOMASK,prmBC00397)
             ,new CursorDef("BC00398", "DELETE FROM [Feriados]  WHERE [Feriado_Data] = @Feriado_Data", GxErrorMask.GX_NOMASK,prmBC00398)
             ,new CursorDef("BC00399", "SELECT TM1.[Feriado_Data], TM1.[Feriado_Nome], TM1.[Feriado_Fixo] FROM [Feriados] TM1 WITH (NOLOCK) WHERE TM1.[Feriado_Data] = @Feriado_Data ORDER BY TM1.[Feriado_Data]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00399,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 3 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                return;
             case 7 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(3, (bool)parms[3]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[2]);
                }
                stmt.SetParameter(3, (DateTime)parms[3]);
                return;
             case 6 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                return;
       }
    }

 }

}
