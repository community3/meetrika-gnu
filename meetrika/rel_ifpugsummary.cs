/*
               File: REL_IFPUGSummary
        Description: Stub for REL_IFPUGSummary
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:18:1.75
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_ifpugsummary : GXProcedure
   {
      public rel_ifpugsummary( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_ifpugsummary( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_User_Nome ,
                           int aP1_Contratada_Codigo ,
                           int aP2_Area ,
                           int aP3_Colaborador ,
                           int aP4_Servico ,
                           short aP5_Ano ,
                           long aP6_Mes )
      {
         this.AV2User_Nome = aP0_User_Nome;
         this.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV4Area = aP2_Area;
         this.AV5Colaborador = aP3_Colaborador;
         this.AV6Servico = aP4_Servico;
         this.AV7Ano = aP5_Ano;
         this.AV8Mes = aP6_Mes;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_User_Nome ,
                                 int aP1_Contratada_Codigo ,
                                 int aP2_Area ,
                                 int aP3_Colaborador ,
                                 int aP4_Servico ,
                                 short aP5_Ano ,
                                 long aP6_Mes )
      {
         rel_ifpugsummary objrel_ifpugsummary;
         objrel_ifpugsummary = new rel_ifpugsummary();
         objrel_ifpugsummary.AV2User_Nome = aP0_User_Nome;
         objrel_ifpugsummary.AV3Contratada_Codigo = aP1_Contratada_Codigo;
         objrel_ifpugsummary.AV4Area = aP2_Area;
         objrel_ifpugsummary.AV5Colaborador = aP3_Colaborador;
         objrel_ifpugsummary.AV6Servico = aP4_Servico;
         objrel_ifpugsummary.AV7Ano = aP5_Ano;
         objrel_ifpugsummary.AV8Mes = aP6_Mes;
         objrel_ifpugsummary.context.SetSubmitInitialConfig(context);
         objrel_ifpugsummary.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_ifpugsummary);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_ifpugsummary)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2User_Nome,(int)AV3Contratada_Codigo,(int)AV4Area,(int)AV5Colaborador,(int)AV6Servico,(short)AV7Ano,(long)AV8Mes} ;
         ClassLoader.Execute("arel_ifpugsummary","GeneXus.Programs.arel_ifpugsummary", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 7 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV7Ano ;
      private int AV3Contratada_Codigo ;
      private int AV4Area ;
      private int AV5Colaborador ;
      private int AV6Servico ;
      private long AV8Mes ;
      private String AV2User_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
