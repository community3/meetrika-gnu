/*
               File: MenuPerfil
        Description: Menu Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:35.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class menuperfil : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
         {
            A277Menu_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_2( A277Menu_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A3Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A3Perfil_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbMenu_Tipo.Name = "MENU_TIPO";
         cmbMenu_Tipo.WebTags = "";
         cmbMenu_Tipo.addItem("1", "Superior", 0);
         cmbMenu_Tipo.addItem("2", "Acesso R�pido", 0);
         if ( cmbMenu_Tipo.ItemCount > 0 )
         {
            A280Menu_Tipo = (short)(NumberUtil.Val( cmbMenu_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
         }
         chkMenu_Ativo.Name = "MENU_ATIVO";
         chkMenu_Ativo.WebTags = "";
         chkMenu_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkMenu_Ativo_Internalname, "TitleCaption", chkMenu_Ativo.Caption);
         chkMenu_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Menu Perfil", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtMenu_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public menuperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public menuperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbMenu_Tipo = new GXCombobox();
         chkMenu_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbMenu_Tipo.ItemCount > 0 )
         {
            A280Menu_Tipo = (short)(NumberUtil.Val( cmbMenu_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1C49( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1C49e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1C49( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1C49( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1C49e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Menu Perfil", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_MenuPerfil.htm");
            wb_table3_28_1C49( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_1C49e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1C49e( true) ;
         }
         else
         {
            wb_table1_2_1C49e( false) ;
         }
      }

      protected void wb_table3_28_1C49( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_1C49( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_1C49e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_MenuPerfil.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_MenuPerfil.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_1C49e( true) ;
         }
         else
         {
            wb_table3_28_1C49e( false) ;
         }
      }

      protected void wb_table4_34_1C49( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_codigo_Internalname, "C�digo", "", "", lblTextblockmenu_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtMenu_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A277Menu_Codigo), 6, 0, ",", "")), ((edtMenu_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A277Menu_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A277Menu_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtMenu_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_nome_Internalname, "Nome", "", "", lblTextblockmenu_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_Nome_Internalname, StringUtil.RTrim( A278Menu_Nome), StringUtil.RTrim( context.localUtil.Format( A278Menu_Nome, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Nome_Jsonclick, 0, "Attribute", "", "", "", 1, edtMenu_Nome_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "NomeMenu30", "left", true, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_tipo_Internalname, "Tipo(Superior ou Acesso R�pido)", "", "", lblTextblockmenu_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbMenu_Tipo, cmbMenu_Tipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)), 1, cmbMenu_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbMenu_Tipo.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", "", true, "HLP_MenuPerfil.htm");
            cmbMenu_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbMenu_Tipo_Internalname, "Values", (String)(cmbMenu_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_ativo_Internalname, "Ativo", "", "", lblTextblockmenu_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Check box */
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkMenu_Ativo_Internalname, StringUtil.BoolToStr( A284Menu_Ativo), "", "", 1, chkMenu_Ativo.Enabled, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_ordem_Internalname, "Ordem do Menu", "", "", lblTextblockmenu_ordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_Ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A283Menu_Ordem), 3, 0, ",", "")), ((edtMenu_Ordem_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A283Menu_Ordem), "ZZ9")) : context.localUtil.Format( (decimal)(A283Menu_Ordem), "ZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Ordem_Jsonclick, 0, "Attribute", "", "", "", 1, edtMenu_Ordem_Enabled, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Ordem", "right", false, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_imagem_Internalname, "Imagem dos �cones", "", "", lblTextblockmenu_imagem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Static Bitmap Variable */
            ClassString = "ImageAttribute";
            StyleString = "";
            A282Menu_Imagem_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000Menu_Imagem_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)));
            GxWebStd.gx_bitmap( context, imgMenu_Imagem_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.PathToRelativeUrl( A282Menu_Imagem)), "", "", "", context.GetTheme( ), 1, imgMenu_Imagem_Enabled, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", 0, A282Menu_Imagem_IsBlob, true, "HLP_MenuPerfil.htm");
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "URL", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.PathToRelativeUrl( A282Menu_Imagem)));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "IsBlob", StringUtil.BoolToStr( A282Menu_Imagem_IsBlob));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmenu_link_Internalname, "Link do Menu", "", "", lblTextblockmenu_link_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtMenu_Link_Internalname, A281Menu_Link, StringUtil.RTrim( context.localUtil.Format( A281Menu_Link, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtMenu_Link_Jsonclick, 0, "Attribute", "", "", "", 1, edtMenu_Link_Enabled, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "LinkMenu", "left", true, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockperfil_codigo_Internalname, "C�digo", "", "", lblTextblockperfil_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPerfil_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ",", "")), ((edtPerfil_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPerfil_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtPerfil_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockperfil_nome_Internalname, "Nome", "", "", lblTextblockperfil_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPerfil_Nome_Internalname, StringUtil.RTrim( A4Perfil_Nome), StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPerfil_Nome_Jsonclick, 0, "Attribute", "", "", "", 1, edtPerfil_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_MenuPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_1C49e( true) ;
         }
         else
         {
            wb_table4_34_1C49e( false) ;
         }
      }

      protected void wb_table2_5_1C49( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_MenuPerfil.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1C49e( true) ;
         }
         else
         {
            wb_table2_5_1C49e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtMenu_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtMenu_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "MENU_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtMenu_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A277Menu_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
               }
               else
               {
                  A277Menu_Codigo = (int)(context.localUtil.CToN( cgiGet( edtMenu_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
               }
               A278Menu_Nome = cgiGet( edtMenu_Nome_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
               cmbMenu_Tipo.CurrentValue = cgiGet( cmbMenu_Tipo_Internalname);
               A280Menu_Tipo = (short)(NumberUtil.Val( cgiGet( cmbMenu_Tipo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
               A284Menu_Ativo = StringUtil.StrToBool( cgiGet( chkMenu_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
               A283Menu_Ordem = (short)(context.localUtil.CToN( cgiGet( edtMenu_Ordem_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
               A282Menu_Imagem = cgiGet( imgMenu_Imagem_Internalname);
               n282Menu_Imagem = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A282Menu_Imagem", A282Menu_Imagem);
               A281Menu_Link = cgiGet( edtMenu_Link_Internalname);
               n281Menu_Link = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
               if ( ( ( context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "PERFIL_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtPerfil_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A3Perfil_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               }
               else
               {
                  A3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               }
               A4Perfil_Nome = StringUtil.Upper( cgiGet( edtPerfil_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
               /* Read saved values. */
               Z277Menu_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z277Menu_Codigo"), ",", "."));
               Z3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z3Perfil_Codigo"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               A40000Menu_Imagem_GXI = cgiGet( "MENU_IMAGEM_GXI");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               getMultimediaValue(imgMenu_Imagem_Internalname, ref  A282Menu_Imagem, ref  A40000Menu_Imagem_GXI);
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A277Menu_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
                  A3Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1C49( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes1C49( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption1C0( )
      {
      }

      protected void ZM1C49( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -1 )
         {
            Z277Menu_Codigo = A277Menu_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
            Z278Menu_Nome = A278Menu_Nome;
            Z280Menu_Tipo = A280Menu_Tipo;
            Z284Menu_Ativo = A284Menu_Ativo;
            Z283Menu_Ordem = A283Menu_Ordem;
            Z282Menu_Imagem = A282Menu_Imagem;
            Z40000Menu_Imagem_GXI = A40000Menu_Imagem_GXI;
            Z281Menu_Link = A281Menu_Link;
            Z4Perfil_Nome = A4Perfil_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load1C49( )
      {
         /* Using cursor T001C6 */
         pr_default.execute(4, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound49 = 1;
            A278Menu_Nome = T001C6_A278Menu_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
            A280Menu_Tipo = T001C6_A280Menu_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
            A284Menu_Ativo = T001C6_A284Menu_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
            A283Menu_Ordem = T001C6_A283Menu_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
            A40000Menu_Imagem_GXI = T001C6_A40000Menu_Imagem_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
            n40000Menu_Imagem_GXI = T001C6_n40000Menu_Imagem_GXI[0];
            A281Menu_Link = T001C6_A281Menu_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
            n281Menu_Link = T001C6_n281Menu_Link[0];
            A4Perfil_Nome = T001C6_A4Perfil_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
            A282Menu_Imagem = T001C6_A282Menu_Imagem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A282Menu_Imagem", A282Menu_Imagem);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
            n282Menu_Imagem = T001C6_n282Menu_Imagem[0];
            ZM1C49( -1) ;
         }
         pr_default.close(4);
         OnLoadActions1C49( ) ;
      }

      protected void OnLoadActions1C49( )
      {
      }

      protected void CheckExtendedTable1C49( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T001C4 */
         pr_default.execute(2, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Menu'.", "ForeignKeyNotFound", 1, "MENU_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtMenu_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A278Menu_Nome = T001C4_A278Menu_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
         A280Menu_Tipo = T001C4_A280Menu_Tipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
         A284Menu_Ativo = T001C4_A284Menu_Ativo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
         A283Menu_Ordem = T001C4_A283Menu_Ordem[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
         A40000Menu_Imagem_GXI = T001C4_A40000Menu_Imagem_GXI[0];
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
         n40000Menu_Imagem_GXI = T001C4_n40000Menu_Imagem_GXI[0];
         A281Menu_Link = T001C4_A281Menu_Link[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
         n281Menu_Link = T001C4_n281Menu_Link[0];
         A282Menu_Imagem = T001C4_A282Menu_Imagem[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A282Menu_Imagem", A282Menu_Imagem);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
         n282Menu_Imagem = T001C4_n282Menu_Imagem[0];
         pr_default.close(2);
         /* Using cursor T001C5 */
         pr_default.execute(3, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtPerfil_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A4Perfil_Nome = T001C5_A4Perfil_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1C49( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_2( int A277Menu_Codigo )
      {
         /* Using cursor T001C7 */
         pr_default.execute(5, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Menu'.", "ForeignKeyNotFound", 1, "MENU_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtMenu_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A278Menu_Nome = T001C7_A278Menu_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
         A280Menu_Tipo = T001C7_A280Menu_Tipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
         A284Menu_Ativo = T001C7_A284Menu_Ativo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
         A283Menu_Ordem = T001C7_A283Menu_Ordem[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
         A40000Menu_Imagem_GXI = T001C7_A40000Menu_Imagem_GXI[0];
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
         n40000Menu_Imagem_GXI = T001C7_n40000Menu_Imagem_GXI[0];
         A281Menu_Link = T001C7_A281Menu_Link[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
         n281Menu_Link = T001C7_n281Menu_Link[0];
         A282Menu_Imagem = T001C7_A282Menu_Imagem[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A282Menu_Imagem", A282Menu_Imagem);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
         n282Menu_Imagem = T001C7_n282Menu_Imagem[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A278Menu_Nome))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A280Menu_Tipo), 2, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.BoolToStr( A284Menu_Ativo))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A283Menu_Ordem), 3, 0, ".", "")))+"\""+","+"\""+GXUtil.EncodeJSConstant( A282Menu_Imagem)+"\""+","+"\""+GXUtil.EncodeJSConstant( A40000Menu_Imagem_GXI)+"\""+","+"\""+GXUtil.EncodeJSConstant( A281Menu_Link)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_3( int A3Perfil_Codigo )
      {
         /* Using cursor T001C8 */
         pr_default.execute(6, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtPerfil_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A4Perfil_Nome = T001C8_A4Perfil_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A4Perfil_Nome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey1C49( )
      {
         /* Using cursor T001C9 */
         pr_default.execute(7, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound49 = 1;
         }
         else
         {
            RcdFound49 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001C3 */
         pr_default.execute(1, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1C49( 1) ;
            RcdFound49 = 1;
            A277Menu_Codigo = T001C3_A277Menu_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
            A3Perfil_Codigo = T001C3_A3Perfil_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
            Z277Menu_Codigo = A277Menu_Codigo;
            Z3Perfil_Codigo = A3Perfil_Codigo;
            sMode49 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load1C49( ) ;
            if ( AnyError == 1 )
            {
               RcdFound49 = 0;
               InitializeNonKey1C49( ) ;
            }
            Gx_mode = sMode49;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound49 = 0;
            InitializeNonKey1C49( ) ;
            sMode49 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode49;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1C49( ) ;
         if ( RcdFound49 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound49 = 0;
         /* Using cursor T001C10 */
         pr_default.execute(8, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T001C10_A277Menu_Codigo[0] < A277Menu_Codigo ) || ( T001C10_A277Menu_Codigo[0] == A277Menu_Codigo ) && ( T001C10_A3Perfil_Codigo[0] < A3Perfil_Codigo ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T001C10_A277Menu_Codigo[0] > A277Menu_Codigo ) || ( T001C10_A277Menu_Codigo[0] == A277Menu_Codigo ) && ( T001C10_A3Perfil_Codigo[0] > A3Perfil_Codigo ) ) )
            {
               A277Menu_Codigo = T001C10_A277Menu_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
               A3Perfil_Codigo = T001C10_A3Perfil_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               RcdFound49 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound49 = 0;
         /* Using cursor T001C11 */
         pr_default.execute(9, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T001C11_A277Menu_Codigo[0] > A277Menu_Codigo ) || ( T001C11_A277Menu_Codigo[0] == A277Menu_Codigo ) && ( T001C11_A3Perfil_Codigo[0] > A3Perfil_Codigo ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T001C11_A277Menu_Codigo[0] < A277Menu_Codigo ) || ( T001C11_A277Menu_Codigo[0] == A277Menu_Codigo ) && ( T001C11_A3Perfil_Codigo[0] < A3Perfil_Codigo ) ) )
            {
               A277Menu_Codigo = T001C11_A277Menu_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
               A3Perfil_Codigo = T001C11_A3Perfil_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
               RcdFound49 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1C49( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtMenu_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1C49( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound49 == 1 )
            {
               if ( ( A277Menu_Codigo != Z277Menu_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
               {
                  A277Menu_Codigo = Z277Menu_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
                  A3Perfil_Codigo = Z3Perfil_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "MENU_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtMenu_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtMenu_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update1C49( ) ;
                  GX_FocusControl = edtMenu_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A277Menu_Codigo != Z277Menu_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtMenu_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1C49( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "MENU_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtMenu_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtMenu_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1C49( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A277Menu_Codigo != Z277Menu_Codigo ) || ( A3Perfil_Codigo != Z3Perfil_Codigo ) )
         {
            A277Menu_Codigo = Z277Menu_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
            A3Perfil_Codigo = Z3Perfil_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "MENU_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtMenu_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtMenu_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound49 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "MENU_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtMenu_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1C49( ) ;
         if ( RcdFound49 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd1C49( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound49 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound49 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1C49( ) ;
         if ( RcdFound49 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound49 != 0 )
            {
               ScanNext1C49( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         ScanEnd1C49( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency1C49( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001C2 */
            pr_default.execute(0, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"MenuPerfil"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"MenuPerfil"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1C49( )
      {
         BeforeValidate1C49( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1C49( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1C49( 0) ;
            CheckOptimisticConcurrency1C49( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1C49( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1C49( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001C12 */
                     pr_default.execute(10, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("MenuPerfil") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1C0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1C49( ) ;
            }
            EndLevel1C49( ) ;
         }
         CloseExtendedTableCursors1C49( ) ;
      }

      protected void Update1C49( )
      {
         BeforeValidate1C49( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1C49( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1C49( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1C49( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1C49( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [MenuPerfil] */
                     DeferredUpdate1C49( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption1C0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1C49( ) ;
         }
         CloseExtendedTableCursors1C49( ) ;
      }

      protected void DeferredUpdate1C49( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate1C49( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1C49( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1C49( ) ;
            AfterConfirm1C49( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1C49( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001C13 */
                  pr_default.execute(11, new Object[] {A277Menu_Codigo, A3Perfil_Codigo});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("MenuPerfil") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound49 == 0 )
                        {
                           InitAll1C49( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption1C0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode49 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel1C49( ) ;
         Gx_mode = sMode49;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls1C49( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001C14 */
            pr_default.execute(12, new Object[] {A277Menu_Codigo});
            A278Menu_Nome = T001C14_A278Menu_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
            A280Menu_Tipo = T001C14_A280Menu_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
            A284Menu_Ativo = T001C14_A284Menu_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
            A283Menu_Ordem = T001C14_A283Menu_Ordem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
            A40000Menu_Imagem_GXI = T001C14_A40000Menu_Imagem_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
            n40000Menu_Imagem_GXI = T001C14_n40000Menu_Imagem_GXI[0];
            A281Menu_Link = T001C14_A281Menu_Link[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
            n281Menu_Link = T001C14_n281Menu_Link[0];
            A282Menu_Imagem = T001C14_A282Menu_Imagem[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A282Menu_Imagem", A282Menu_Imagem);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
            n282Menu_Imagem = T001C14_n282Menu_Imagem[0];
            pr_default.close(12);
            /* Using cursor T001C15 */
            pr_default.execute(13, new Object[] {A3Perfil_Codigo});
            A4Perfil_Nome = T001C15_A4Perfil_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
            pr_default.close(13);
         }
      }

      protected void EndLevel1C49( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1C49( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            pr_default.close(13);
            context.CommitDataStores( "MenuPerfil");
            if ( AnyError == 0 )
            {
               ConfirmValues1C0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            pr_default.close(13);
            context.RollbackDataStores( "MenuPerfil");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1C49( )
      {
         /* Using cursor T001C16 */
         pr_default.execute(14);
         RcdFound49 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound49 = 1;
            A277Menu_Codigo = T001C16_A277Menu_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
            A3Perfil_Codigo = T001C16_A3Perfil_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1C49( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound49 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound49 = 1;
            A277Menu_Codigo = T001C16_A277Menu_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
            A3Perfil_Codigo = T001C16_A3Perfil_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1C49( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm1C49( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1C49( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1C49( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1C49( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1C49( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1C49( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1C49( )
      {
         edtMenu_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Codigo_Enabled), 5, 0)));
         edtMenu_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Nome_Enabled), 5, 0)));
         cmbMenu_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbMenu_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbMenu_Tipo.Enabled), 5, 0)));
         chkMenu_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkMenu_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkMenu_Ativo.Enabled), 5, 0)));
         edtMenu_Ordem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Ordem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Ordem_Enabled), 5, 0)));
         imgMenu_Imagem_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgMenu_Imagem_Enabled), 5, 0)));
         edtMenu_Link_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMenu_Link_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtMenu_Link_Enabled), 5, 0)));
         edtPerfil_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Codigo_Enabled), 5, 0)));
         edtPerfil_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPerfil_Nome_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1C0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311719376");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("menuperfil.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z277Menu_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z277Menu_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z3Perfil_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z3Perfil_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "MENU_IMAGEM_GXI", A40000Menu_Imagem_GXI);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXCCtlgxBlob = "MENU_IMAGEM" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A282Menu_Imagem);
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("menuperfil.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "MenuPerfil" ;
      }

      public override String GetPgmdesc( )
      {
         return "Menu Perfil" ;
      }

      protected void InitializeNonKey1C49( )
      {
         A278Menu_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
         A280Menu_Tipo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
         A284Menu_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
         A283Menu_Ordem = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
         A282Menu_Imagem = "";
         n282Menu_Imagem = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A282Menu_Imagem", A282Menu_Imagem);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
         A40000Menu_Imagem_GXI = "";
         n40000Menu_Imagem_GXI = false;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
         A281Menu_Link = "";
         n281Menu_Link = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
         A4Perfil_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
      }

      protected void InitAll1C49( )
      {
         A277Menu_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A277Menu_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A277Menu_Codigo), 6, 0)));
         A3Perfil_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3Perfil_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A3Perfil_Codigo), 6, 0)));
         InitializeNonKey1C49( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117193714");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("menuperfil.js", "?20203117193714");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockmenu_codigo_Internalname = "TEXTBLOCKMENU_CODIGO";
         edtMenu_Codigo_Internalname = "MENU_CODIGO";
         lblTextblockmenu_nome_Internalname = "TEXTBLOCKMENU_NOME";
         edtMenu_Nome_Internalname = "MENU_NOME";
         lblTextblockmenu_tipo_Internalname = "TEXTBLOCKMENU_TIPO";
         cmbMenu_Tipo_Internalname = "MENU_TIPO";
         lblTextblockmenu_ativo_Internalname = "TEXTBLOCKMENU_ATIVO";
         chkMenu_Ativo_Internalname = "MENU_ATIVO";
         lblTextblockmenu_ordem_Internalname = "TEXTBLOCKMENU_ORDEM";
         edtMenu_Ordem_Internalname = "MENU_ORDEM";
         lblTextblockmenu_imagem_Internalname = "TEXTBLOCKMENU_IMAGEM";
         imgMenu_Imagem_Internalname = "MENU_IMAGEM";
         lblTextblockmenu_link_Internalname = "TEXTBLOCKMENU_LINK";
         edtMenu_Link_Internalname = "MENU_LINK";
         lblTextblockperfil_codigo_Internalname = "TEXTBLOCKPERFIL_CODIGO";
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO";
         lblTextblockperfil_nome_Internalname = "TEXTBLOCKPERFIL_NOME";
         edtPerfil_Nome_Internalname = "PERFIL_NOME";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Menu Perfil";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtPerfil_Nome_Jsonclick = "";
         edtPerfil_Nome_Enabled = 0;
         edtPerfil_Codigo_Jsonclick = "";
         edtPerfil_Codigo_Enabled = 1;
         edtMenu_Link_Jsonclick = "";
         edtMenu_Link_Enabled = 0;
         imgMenu_Imagem_Enabled = 0;
         edtMenu_Ordem_Jsonclick = "";
         edtMenu_Ordem_Enabled = 0;
         chkMenu_Ativo.Enabled = 0;
         cmbMenu_Tipo_Jsonclick = "";
         cmbMenu_Tipo.Enabled = 0;
         edtMenu_Nome_Jsonclick = "";
         edtMenu_Nome_Enabled = 0;
         edtMenu_Codigo_Jsonclick = "";
         edtMenu_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         chkMenu_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T001C14 */
         pr_default.execute(12, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Menu'.", "ForeignKeyNotFound", 1, "MENU_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtMenu_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A278Menu_Nome = T001C14_A278Menu_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A278Menu_Nome", A278Menu_Nome);
         A280Menu_Tipo = T001C14_A280Menu_Tipo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A280Menu_Tipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0)));
         A284Menu_Ativo = T001C14_A284Menu_Ativo[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A284Menu_Ativo", A284Menu_Ativo);
         A283Menu_Ordem = T001C14_A283Menu_Ordem[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A283Menu_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A283Menu_Ordem), 3, 0)));
         A40000Menu_Imagem_GXI = T001C14_A40000Menu_Imagem_GXI[0];
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
         n40000Menu_Imagem_GXI = T001C14_n40000Menu_Imagem_GXI[0];
         A281Menu_Link = T001C14_A281Menu_Link[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A281Menu_Link", A281Menu_Link);
         n281Menu_Link = T001C14_n281Menu_Link[0];
         A282Menu_Imagem = T001C14_A282Menu_Imagem[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A282Menu_Imagem", A282Menu_Imagem);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgMenu_Imagem_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A282Menu_Imagem)) ? A40000Menu_Imagem_GXI : context.convertURL( context.PathToRelativeUrl( A282Menu_Imagem))));
         n282Menu_Imagem = T001C14_n282Menu_Imagem[0];
         pr_default.close(12);
         /* Using cursor T001C15 */
         pr_default.execute(13, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtPerfil_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A4Perfil_Nome = T001C15_A4Perfil_Nome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4Perfil_Nome", A4Perfil_Nome);
         pr_default.close(13);
         if ( AnyError == 0 )
         {
            GX_FocusControl = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Menu_codigo( int GX_Parm1 ,
                                     String GX_Parm2 ,
                                     GXCombobox cmbGX_Parm3 ,
                                     bool GX_Parm4 ,
                                     short GX_Parm5 ,
                                     String GX_Parm6 ,
                                     String GX_Parm7 ,
                                     String GX_Parm8 )
      {
         A277Menu_Codigo = GX_Parm1;
         A278Menu_Nome = GX_Parm2;
         cmbMenu_Tipo = cmbGX_Parm3;
         A280Menu_Tipo = (short)(NumberUtil.Val( cmbMenu_Tipo.CurrentValue, "."));
         cmbMenu_Tipo.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0));
         A284Menu_Ativo = GX_Parm4;
         A283Menu_Ordem = GX_Parm5;
         A282Menu_Imagem = GX_Parm6;
         n282Menu_Imagem = false;
         A40000Menu_Imagem_GXI = GX_Parm7;
         n40000Menu_Imagem_GXI = false;
         A281Menu_Link = GX_Parm8;
         n281Menu_Link = false;
         /* Using cursor T001C14 */
         pr_default.execute(12, new Object[] {A277Menu_Codigo});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Menu'.", "ForeignKeyNotFound", 1, "MENU_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtMenu_Codigo_Internalname;
         }
         A278Menu_Nome = T001C14_A278Menu_Nome[0];
         A280Menu_Tipo = T001C14_A280Menu_Tipo[0];
         cmbMenu_Tipo.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0));
         A284Menu_Ativo = T001C14_A284Menu_Ativo[0];
         A283Menu_Ordem = T001C14_A283Menu_Ordem[0];
         A40000Menu_Imagem_GXI = T001C14_A40000Menu_Imagem_GXI[0];
         n40000Menu_Imagem_GXI = T001C14_n40000Menu_Imagem_GXI[0];
         A281Menu_Link = T001C14_A281Menu_Link[0];
         n281Menu_Link = T001C14_n281Menu_Link[0];
         A282Menu_Imagem = T001C14_A282Menu_Imagem[0];
         n282Menu_Imagem = T001C14_n282Menu_Imagem[0];
         pr_default.close(12);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A278Menu_Nome = "";
            A280Menu_Tipo = 0;
            cmbMenu_Tipo.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0));
            A284Menu_Ativo = false;
            A283Menu_Ordem = 0;
            A282Menu_Imagem = "";
            n282Menu_Imagem = false;
            A40000Menu_Imagem_GXI = "";
            n40000Menu_Imagem_GXI = false;
            A281Menu_Link = "";
            n281Menu_Link = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A278Menu_Nome));
         cmbMenu_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0));
         isValidOutput.Add(cmbMenu_Tipo);
         isValidOutput.Add(A284Menu_Ativo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A283Menu_Ordem), 3, 0, ".", "")));
         isValidOutput.Add(context.PathToRelativeUrl( A282Menu_Imagem));
         isValidOutput.Add(A282Menu_Imagem);
         isValidOutput.Add(A40000Menu_Imagem_GXI);
         isValidOutput.Add(A281Menu_Link);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Perfil_codigo( GXCombobox cmbGX_Parm1 ,
                                       int GX_Parm2 ,
                                       int GX_Parm3 ,
                                       String GX_Parm4 )
      {
         cmbMenu_Tipo = cmbGX_Parm1;
         A280Menu_Tipo = (short)(NumberUtil.Val( cmbMenu_Tipo.CurrentValue, "."));
         cmbMenu_Tipo.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0));
         A277Menu_Codigo = GX_Parm2;
         A3Perfil_Codigo = GX_Parm3;
         A4Perfil_Nome = GX_Parm4;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         /* Using cursor T001C15 */
         pr_default.execute(13, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Perfil'.", "ForeignKeyNotFound", 1, "PERFIL_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtPerfil_Codigo_Internalname;
         }
         A4Perfil_Nome = T001C15_A4Perfil_Nome[0];
         pr_default.close(13);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A278Menu_Nome = "";
            A280Menu_Tipo = 0;
            cmbMenu_Tipo.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0));
            A284Menu_Ativo = false;
            A283Menu_Ordem = 0;
            A282Menu_Imagem = "";
            n282Menu_Imagem = false;
            A40000Menu_Imagem_GXI = "";
            n40000Menu_Imagem_GXI = false;
            A281Menu_Link = "";
            n281Menu_Link = false;
            A4Perfil_Nome = "";
         }
         isValidOutput.Add(StringUtil.RTrim( A278Menu_Nome));
         cmbMenu_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A280Menu_Tipo), 2, 0));
         isValidOutput.Add(cmbMenu_Tipo);
         isValidOutput.Add(A284Menu_Ativo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A283Menu_Ordem), 3, 0, ".", "")));
         isValidOutput.Add(context.PathToRelativeUrl( A282Menu_Imagem));
         isValidOutput.Add(A282Menu_Imagem);
         isValidOutput.Add(A40000Menu_Imagem_GXI);
         isValidOutput.Add(A281Menu_Link);
         isValidOutput.Add(StringUtil.RTrim( A4Perfil_Nome));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z277Menu_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z3Perfil_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z278Menu_Nome));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z280Menu_Tipo), 2, 0, ".", "")));
         isValidOutput.Add(Z284Menu_Ativo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z283Menu_Ordem), 3, 0, ".", "")));
         isValidOutput.Add(context.PathToRelativeUrl( Z282Menu_Imagem));
         isValidOutput.Add(Z40000Menu_Imagem_GXI);
         isValidOutput.Add(Z281Menu_Link);
         isValidOutput.Add(StringUtil.RTrim( Z4Perfil_Nome));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockmenu_codigo_Jsonclick = "";
         lblTextblockmenu_nome_Jsonclick = "";
         A278Menu_Nome = "";
         lblTextblockmenu_tipo_Jsonclick = "";
         lblTextblockmenu_ativo_Jsonclick = "";
         lblTextblockmenu_ordem_Jsonclick = "";
         lblTextblockmenu_imagem_Jsonclick = "";
         A282Menu_Imagem = "";
         A40000Menu_Imagem_GXI = "";
         lblTextblockmenu_link_Jsonclick = "";
         A281Menu_Link = "";
         lblTextblockperfil_codigo_Jsonclick = "";
         lblTextblockperfil_nome_Jsonclick = "";
         A4Perfil_Nome = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z278Menu_Nome = "";
         Z282Menu_Imagem = "";
         Z40000Menu_Imagem_GXI = "";
         Z281Menu_Link = "";
         Z4Perfil_Nome = "";
         T001C6_A278Menu_Nome = new String[] {""} ;
         T001C6_A280Menu_Tipo = new short[1] ;
         T001C6_A284Menu_Ativo = new bool[] {false} ;
         T001C6_A283Menu_Ordem = new short[1] ;
         T001C6_A40000Menu_Imagem_GXI = new String[] {""} ;
         T001C6_n40000Menu_Imagem_GXI = new bool[] {false} ;
         T001C6_A281Menu_Link = new String[] {""} ;
         T001C6_n281Menu_Link = new bool[] {false} ;
         T001C6_A4Perfil_Nome = new String[] {""} ;
         T001C6_A277Menu_Codigo = new int[1] ;
         T001C6_A3Perfil_Codigo = new int[1] ;
         T001C6_A282Menu_Imagem = new String[] {""} ;
         T001C6_n282Menu_Imagem = new bool[] {false} ;
         T001C4_A278Menu_Nome = new String[] {""} ;
         T001C4_A280Menu_Tipo = new short[1] ;
         T001C4_A284Menu_Ativo = new bool[] {false} ;
         T001C4_A283Menu_Ordem = new short[1] ;
         T001C4_A40000Menu_Imagem_GXI = new String[] {""} ;
         T001C4_n40000Menu_Imagem_GXI = new bool[] {false} ;
         T001C4_A281Menu_Link = new String[] {""} ;
         T001C4_n281Menu_Link = new bool[] {false} ;
         T001C4_A282Menu_Imagem = new String[] {""} ;
         T001C4_n282Menu_Imagem = new bool[] {false} ;
         T001C5_A4Perfil_Nome = new String[] {""} ;
         T001C7_A278Menu_Nome = new String[] {""} ;
         T001C7_A280Menu_Tipo = new short[1] ;
         T001C7_A284Menu_Ativo = new bool[] {false} ;
         T001C7_A283Menu_Ordem = new short[1] ;
         T001C7_A40000Menu_Imagem_GXI = new String[] {""} ;
         T001C7_n40000Menu_Imagem_GXI = new bool[] {false} ;
         T001C7_A281Menu_Link = new String[] {""} ;
         T001C7_n281Menu_Link = new bool[] {false} ;
         T001C7_A282Menu_Imagem = new String[] {""} ;
         T001C7_n282Menu_Imagem = new bool[] {false} ;
         T001C8_A4Perfil_Nome = new String[] {""} ;
         T001C9_A277Menu_Codigo = new int[1] ;
         T001C9_A3Perfil_Codigo = new int[1] ;
         T001C3_A277Menu_Codigo = new int[1] ;
         T001C3_A3Perfil_Codigo = new int[1] ;
         sMode49 = "";
         T001C10_A277Menu_Codigo = new int[1] ;
         T001C10_A3Perfil_Codigo = new int[1] ;
         T001C11_A277Menu_Codigo = new int[1] ;
         T001C11_A3Perfil_Codigo = new int[1] ;
         T001C2_A277Menu_Codigo = new int[1] ;
         T001C2_A3Perfil_Codigo = new int[1] ;
         T001C14_A278Menu_Nome = new String[] {""} ;
         T001C14_A280Menu_Tipo = new short[1] ;
         T001C14_A284Menu_Ativo = new bool[] {false} ;
         T001C14_A283Menu_Ordem = new short[1] ;
         T001C14_A40000Menu_Imagem_GXI = new String[] {""} ;
         T001C14_n40000Menu_Imagem_GXI = new bool[] {false} ;
         T001C14_A281Menu_Link = new String[] {""} ;
         T001C14_n281Menu_Link = new bool[] {false} ;
         T001C14_A282Menu_Imagem = new String[] {""} ;
         T001C14_n282Menu_Imagem = new bool[] {false} ;
         T001C15_A4Perfil_Nome = new String[] {""} ;
         T001C16_A277Menu_Codigo = new int[1] ;
         T001C16_A3Perfil_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXCCtlgxBlob = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.menuperfil__default(),
            new Object[][] {
                new Object[] {
               T001C2_A277Menu_Codigo, T001C2_A3Perfil_Codigo
               }
               , new Object[] {
               T001C3_A277Menu_Codigo, T001C3_A3Perfil_Codigo
               }
               , new Object[] {
               T001C4_A278Menu_Nome, T001C4_A280Menu_Tipo, T001C4_A284Menu_Ativo, T001C4_A283Menu_Ordem, T001C4_A40000Menu_Imagem_GXI, T001C4_n40000Menu_Imagem_GXI, T001C4_A281Menu_Link, T001C4_n281Menu_Link, T001C4_A282Menu_Imagem, T001C4_n282Menu_Imagem
               }
               , new Object[] {
               T001C5_A4Perfil_Nome
               }
               , new Object[] {
               T001C6_A278Menu_Nome, T001C6_A280Menu_Tipo, T001C6_A284Menu_Ativo, T001C6_A283Menu_Ordem, T001C6_A40000Menu_Imagem_GXI, T001C6_n40000Menu_Imagem_GXI, T001C6_A281Menu_Link, T001C6_n281Menu_Link, T001C6_A4Perfil_Nome, T001C6_A277Menu_Codigo,
               T001C6_A3Perfil_Codigo, T001C6_A282Menu_Imagem, T001C6_n282Menu_Imagem
               }
               , new Object[] {
               T001C7_A278Menu_Nome, T001C7_A280Menu_Tipo, T001C7_A284Menu_Ativo, T001C7_A283Menu_Ordem, T001C7_A40000Menu_Imagem_GXI, T001C7_n40000Menu_Imagem_GXI, T001C7_A281Menu_Link, T001C7_n281Menu_Link, T001C7_A282Menu_Imagem, T001C7_n282Menu_Imagem
               }
               , new Object[] {
               T001C8_A4Perfil_Nome
               }
               , new Object[] {
               T001C9_A277Menu_Codigo, T001C9_A3Perfil_Codigo
               }
               , new Object[] {
               T001C10_A277Menu_Codigo, T001C10_A3Perfil_Codigo
               }
               , new Object[] {
               T001C11_A277Menu_Codigo, T001C11_A3Perfil_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001C14_A278Menu_Nome, T001C14_A280Menu_Tipo, T001C14_A284Menu_Ativo, T001C14_A283Menu_Ordem, T001C14_A40000Menu_Imagem_GXI, T001C14_n40000Menu_Imagem_GXI, T001C14_A281Menu_Link, T001C14_n281Menu_Link, T001C14_A282Menu_Imagem, T001C14_n282Menu_Imagem
               }
               , new Object[] {
               T001C15_A4Perfil_Nome
               }
               , new Object[] {
               T001C16_A277Menu_Codigo, T001C16_A3Perfil_Codigo
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short A280Menu_Tipo ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A283Menu_Ordem ;
      private short GX_JID ;
      private short Z280Menu_Tipo ;
      private short Z283Menu_Ordem ;
      private short RcdFound49 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z277Menu_Codigo ;
      private int Z3Perfil_Codigo ;
      private int A277Menu_Codigo ;
      private int A3Perfil_Codigo ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtMenu_Codigo_Enabled ;
      private int edtMenu_Nome_Enabled ;
      private int edtMenu_Ordem_Enabled ;
      private int imgMenu_Imagem_Enabled ;
      private int edtMenu_Link_Enabled ;
      private int edtPerfil_Codigo_Enabled ;
      private int edtPerfil_Nome_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String chkMenu_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtMenu_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockmenu_codigo_Internalname ;
      private String lblTextblockmenu_codigo_Jsonclick ;
      private String edtMenu_Codigo_Jsonclick ;
      private String lblTextblockmenu_nome_Internalname ;
      private String lblTextblockmenu_nome_Jsonclick ;
      private String edtMenu_Nome_Internalname ;
      private String A278Menu_Nome ;
      private String edtMenu_Nome_Jsonclick ;
      private String lblTextblockmenu_tipo_Internalname ;
      private String lblTextblockmenu_tipo_Jsonclick ;
      private String cmbMenu_Tipo_Internalname ;
      private String cmbMenu_Tipo_Jsonclick ;
      private String lblTextblockmenu_ativo_Internalname ;
      private String lblTextblockmenu_ativo_Jsonclick ;
      private String lblTextblockmenu_ordem_Internalname ;
      private String lblTextblockmenu_ordem_Jsonclick ;
      private String edtMenu_Ordem_Internalname ;
      private String edtMenu_Ordem_Jsonclick ;
      private String lblTextblockmenu_imagem_Internalname ;
      private String lblTextblockmenu_imagem_Jsonclick ;
      private String imgMenu_Imagem_Internalname ;
      private String lblTextblockmenu_link_Internalname ;
      private String lblTextblockmenu_link_Jsonclick ;
      private String edtMenu_Link_Internalname ;
      private String edtMenu_Link_Jsonclick ;
      private String lblTextblockperfil_codigo_Internalname ;
      private String lblTextblockperfil_codigo_Jsonclick ;
      private String edtPerfil_Codigo_Internalname ;
      private String edtPerfil_Codigo_Jsonclick ;
      private String lblTextblockperfil_nome_Internalname ;
      private String lblTextblockperfil_nome_Jsonclick ;
      private String edtPerfil_Nome_Internalname ;
      private String A4Perfil_Nome ;
      private String edtPerfil_Nome_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z278Menu_Nome ;
      private String Z4Perfil_Nome ;
      private String sMode49 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A284Menu_Ativo ;
      private bool A282Menu_Imagem_IsBlob ;
      private bool n282Menu_Imagem ;
      private bool n281Menu_Link ;
      private bool Z284Menu_Ativo ;
      private bool n40000Menu_Imagem_GXI ;
      private String A40000Menu_Imagem_GXI ;
      private String A281Menu_Link ;
      private String Z40000Menu_Imagem_GXI ;
      private String Z281Menu_Link ;
      private String A282Menu_Imagem ;
      private String Z282Menu_Imagem ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbMenu_Tipo ;
      private GXCheckbox chkMenu_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T001C6_A278Menu_Nome ;
      private short[] T001C6_A280Menu_Tipo ;
      private bool[] T001C6_A284Menu_Ativo ;
      private short[] T001C6_A283Menu_Ordem ;
      private String[] T001C6_A40000Menu_Imagem_GXI ;
      private bool[] T001C6_n40000Menu_Imagem_GXI ;
      private String[] T001C6_A281Menu_Link ;
      private bool[] T001C6_n281Menu_Link ;
      private String[] T001C6_A4Perfil_Nome ;
      private int[] T001C6_A277Menu_Codigo ;
      private int[] T001C6_A3Perfil_Codigo ;
      private String[] T001C6_A282Menu_Imagem ;
      private bool[] T001C6_n282Menu_Imagem ;
      private String[] T001C4_A278Menu_Nome ;
      private short[] T001C4_A280Menu_Tipo ;
      private bool[] T001C4_A284Menu_Ativo ;
      private short[] T001C4_A283Menu_Ordem ;
      private String[] T001C4_A40000Menu_Imagem_GXI ;
      private bool[] T001C4_n40000Menu_Imagem_GXI ;
      private String[] T001C4_A281Menu_Link ;
      private bool[] T001C4_n281Menu_Link ;
      private String[] T001C4_A282Menu_Imagem ;
      private bool[] T001C4_n282Menu_Imagem ;
      private String[] T001C5_A4Perfil_Nome ;
      private String[] T001C7_A278Menu_Nome ;
      private short[] T001C7_A280Menu_Tipo ;
      private bool[] T001C7_A284Menu_Ativo ;
      private short[] T001C7_A283Menu_Ordem ;
      private String[] T001C7_A40000Menu_Imagem_GXI ;
      private bool[] T001C7_n40000Menu_Imagem_GXI ;
      private String[] T001C7_A281Menu_Link ;
      private bool[] T001C7_n281Menu_Link ;
      private String[] T001C7_A282Menu_Imagem ;
      private bool[] T001C7_n282Menu_Imagem ;
      private String[] T001C8_A4Perfil_Nome ;
      private int[] T001C9_A277Menu_Codigo ;
      private int[] T001C9_A3Perfil_Codigo ;
      private int[] T001C3_A277Menu_Codigo ;
      private int[] T001C3_A3Perfil_Codigo ;
      private int[] T001C10_A277Menu_Codigo ;
      private int[] T001C10_A3Perfil_Codigo ;
      private int[] T001C11_A277Menu_Codigo ;
      private int[] T001C11_A3Perfil_Codigo ;
      private int[] T001C2_A277Menu_Codigo ;
      private int[] T001C2_A3Perfil_Codigo ;
      private String[] T001C14_A278Menu_Nome ;
      private short[] T001C14_A280Menu_Tipo ;
      private bool[] T001C14_A284Menu_Ativo ;
      private short[] T001C14_A283Menu_Ordem ;
      private String[] T001C14_A40000Menu_Imagem_GXI ;
      private bool[] T001C14_n40000Menu_Imagem_GXI ;
      private String[] T001C14_A281Menu_Link ;
      private bool[] T001C14_n281Menu_Link ;
      private String[] T001C14_A282Menu_Imagem ;
      private bool[] T001C14_n282Menu_Imagem ;
      private String[] T001C15_A4Perfil_Nome ;
      private int[] T001C16_A277Menu_Codigo ;
      private int[] T001C16_A3Perfil_Codigo ;
      private GXWebForm Form ;
   }

   public class menuperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001C6 ;
          prmT001C6 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C4 ;
          prmT001C4 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C5 ;
          prmT001C5 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C7 ;
          prmT001C7 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C8 ;
          prmT001C8 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C9 ;
          prmT001C9 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C3 ;
          prmT001C3 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C10 ;
          prmT001C10 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C11 ;
          prmT001C11 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C2 ;
          prmT001C2 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C12 ;
          prmT001C12 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C13 ;
          prmT001C13 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C16 ;
          prmT001C16 = new Object[] {
          } ;
          Object[] prmT001C14 ;
          prmT001C14 = new Object[] {
          new Object[] {"@Menu_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001C15 ;
          prmT001C15 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001C2", "SELECT [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (UPDLOCK) WHERE [Menu_Codigo] = @Menu_Codigo AND [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001C2,1,0,true,false )
             ,new CursorDef("T001C3", "SELECT [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo AND [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001C3,1,0,true,false )
             ,new CursorDef("T001C4", "SELECT [Menu_Nome], [Menu_Tipo], [Menu_Ativo], [Menu_Ordem], [Menu_Imagem_GXI], [Menu_Link], [Menu_Imagem] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001C4,1,0,true,false )
             ,new CursorDef("T001C5", "SELECT [Perfil_Nome] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001C5,1,0,true,false )
             ,new CursorDef("T001C6", "SELECT T2.[Menu_Nome], T2.[Menu_Tipo], T2.[Menu_Ativo], T2.[Menu_Ordem], T2.[Menu_Imagem_GXI], T2.[Menu_Link], T3.[Perfil_Nome], TM1.[Menu_Codigo], TM1.[Perfil_Codigo], T2.[Menu_Imagem] FROM (([MenuPerfil] TM1 WITH (NOLOCK) INNER JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = TM1.[Menu_Codigo]) INNER JOIN [Perfil] T3 WITH (NOLOCK) ON T3.[Perfil_Codigo] = TM1.[Perfil_Codigo]) WHERE TM1.[Menu_Codigo] = @Menu_Codigo and TM1.[Perfil_Codigo] = @Perfil_Codigo ORDER BY TM1.[Menu_Codigo], TM1.[Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001C6,100,0,true,false )
             ,new CursorDef("T001C7", "SELECT [Menu_Nome], [Menu_Tipo], [Menu_Ativo], [Menu_Ordem], [Menu_Imagem_GXI], [Menu_Link], [Menu_Imagem] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001C7,1,0,true,false )
             ,new CursorDef("T001C8", "SELECT [Perfil_Nome] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001C8,1,0,true,false )
             ,new CursorDef("T001C9", "SELECT [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo AND [Perfil_Codigo] = @Perfil_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001C9,1,0,true,false )
             ,new CursorDef("T001C10", "SELECT TOP 1 [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE ( [Menu_Codigo] > @Menu_Codigo or [Menu_Codigo] = @Menu_Codigo and [Perfil_Codigo] > @Perfil_Codigo) ORDER BY [Menu_Codigo], [Perfil_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001C10,1,0,true,true )
             ,new CursorDef("T001C11", "SELECT TOP 1 [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE ( [Menu_Codigo] < @Menu_Codigo or [Menu_Codigo] = @Menu_Codigo and [Perfil_Codigo] < @Perfil_Codigo) ORDER BY [Menu_Codigo] DESC, [Perfil_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001C11,1,0,true,true )
             ,new CursorDef("T001C12", "INSERT INTO [MenuPerfil]([Menu_Codigo], [Perfil_Codigo]) VALUES(@Menu_Codigo, @Perfil_Codigo)", GxErrorMask.GX_NOMASK,prmT001C12)
             ,new CursorDef("T001C13", "DELETE FROM [MenuPerfil]  WHERE [Menu_Codigo] = @Menu_Codigo AND [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK,prmT001C13)
             ,new CursorDef("T001C14", "SELECT [Menu_Nome], [Menu_Tipo], [Menu_Ativo], [Menu_Ordem], [Menu_Imagem_GXI], [Menu_Link], [Menu_Imagem] FROM [Menu] WITH (NOLOCK) WHERE [Menu_Codigo] = @Menu_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001C14,1,0,true,false )
             ,new CursorDef("T001C15", "SELECT [Perfil_Nome] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001C15,1,0,true,false )
             ,new CursorDef("T001C16", "SELECT [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) ORDER BY [Menu_Codigo], [Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001C16,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(5)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 50) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((int[]) buf[10])[0] = rslt.getInt(9) ;
                ((String[]) buf[11])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(5)) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(10);
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(5)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(5)) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
