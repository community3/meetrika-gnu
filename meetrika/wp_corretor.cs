/*
               File: WP_Corretor
        Description: Corretor
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:24:35.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_corretor : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_corretor( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_corretor( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_LogResponsavel_DemandaCod )
      {
         this.AV11LogResponsavel_DemandaCod = aP0_LogResponsavel_DemandaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavNaoconsiderarprazoinicial = new GXCheckbox();
         cmbavEmissor = new GXCombobox();
         cmbLogResponsavel_Acao = new GXCombobox();
         cmbavDestino = new GXCombobox();
         cmbLogResponsavel_Status = new GXCombobox();
         cmbLogResponsavel_NovoStatus = new GXCombobox();
         chkavSelected = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_9 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_9_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_9_idx = GetNextPar( );
               edtLogResponsavel_DemandaCod_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_DemandaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_DemandaCod_Visible), 5, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV11LogResponsavel_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11LogResponsavel_DemandaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOGRESPONSAVEL_DEMANDACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11LogResponsavel_DemandaCod), "ZZZZZ9")));
               edtLogResponsavel_DemandaCod_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_DemandaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_DemandaCod_Visible), 5, 0)));
               AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace", AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace);
               AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace", AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace);
               AV32ddo_LogResponsavel_StatusTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ddo_LogResponsavel_StatusTitleControlIdToReplace", AV32ddo_LogResponsavel_StatusTitleControlIdToReplace);
               AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace", AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace);
               AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace", AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace);
               AV75Pgmname = GetNextPar( );
               AV20TFLogResponsavel_DataHora = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFLogResponsavel_DataHora", context.localUtil.TToC( AV20TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
               AV21TFLogResponsavel_DataHora_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV21TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV27TFLogResponsavel_Acao_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV31TFLogResponsavel_Status_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV35TFLogResponsavel_NovoStatus_Sels);
               AV38TFLogResponsavel_Prazo = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLogResponsavel_Prazo", context.localUtil.TToC( AV38TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
               AV39TFLogResponsavel_Prazo_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV39TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
               A54Usuario_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A54Usuario_Ativo", A54Usuario_Ativo);
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)));
               A58Usuario_PessoaNom = GetNextPar( );
               n58Usuario_PessoaNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A58Usuario_PessoaNom", A58Usuario_PessoaNom);
               A896LogResponsavel_Owner = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
               A891LogResponsavel_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n891LogResponsavel_UsuarioCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A891LogResponsavel_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0)));
               A493ContagemResultado_DemandaFM = GetNextPar( );
               n493ContagemResultado_DemandaFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV18WWPContext);
               AV70Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70Codigo), 6, 0)));
               A892LogResponsavel_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n892LogResponsavel_DemandaCod = false;
               A1553ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1553ContagemResultado_CntSrvCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
               A1611ContagemResultado_PrzTpDias = GetNextPar( );
               n1611ContagemResultado_PrzTpDias = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1611ContagemResultado_PrzTpDias", A1611ContagemResultado_PrzTpDias);
               A798ContagemResultado_PFBFSImp = NumberUtil.Val( GetNextPar( ), ".");
               n798ContagemResultado_PFBFSImp = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A798ContagemResultado_PFBFSImp", StringUtil.LTrim( StringUtil.Str( A798ContagemResultado_PFBFSImp, 14, 5)));
               A1234LogResponsavel_NovoStatus = GetNextPar( );
               n1234LogResponsavel_NovoStatus = false;
               A1237ContagemResultado_PrazoMaisDias = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n1237ContagemResultado_PrazoMaisDias = false;
               A1227ContagemResultado_PrazoInicialDias = (short)(NumberUtil.Val( GetNextPar( ), "."));
               n1227ContagemResultado_PrazoInicialDias = false;
               A471ContagemResultado_DataDmn = context.localUtil.ParseDateParm( GetNextPar( ));
               n471ContagemResultado_DataDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               AV62NovaData = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62NovaData", context.localUtil.Format(AV62NovaData, "99/99/99"));
               AV61UltimoPrazo = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61UltimoPrazo", context.localUtil.TToC( AV61UltimoPrazo, 8, 5, 0, 3, "/", ":", " "));
               A893LogResponsavel_DataHora = context.localUtil.ParseDTimeParm( GetNextPar( ));
               A1149LogResponsavel_OwnerEhContratante = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
               A894LogResponsavel_Acao = GetNextPar( );
               AV51PrazoSugerido = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
               A1177LogResponsavel_Prazo = context.localUtil.ParseDTimeParm( GetNextPar( ));
               n1177LogResponsavel_Prazo = false;
               AV67Selecionadas = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Selecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Selecionadas), 4, 0)));
               AV52Selected = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV52Selected);
               A1228ContratadaUsuario_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n1228ContratadaUsuario_AreaTrabalhoCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1228ContratadaUsuario_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0)));
               AV5Destino = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Destino), 6, 0)));
               A69ContratadaUsuario_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A69ContratadaUsuario_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0)));
               AV6Emissor = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0)));
               A66ContratadaUsuario_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A66ContratadaUsuario_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0)));
               A438Contratada_Sigla = GetNextPar( );
               n438Contratada_Sigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A438Contratada_Sigla", A438Contratada_Sigla);
               AV71PrazoInicial = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PrazoInicial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71PrazoInicial), 4, 0)));
               AV72NaoConsiderarPrazoInicial = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72NaoConsiderarPrazoInicial", AV72NaoConsiderarPrazoInicial);
               AV58ContratoServicos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58ContratoServicos_Codigo), 6, 0)));
               AV60Unidades = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Unidades", StringUtil.LTrim( StringUtil.Str( AV60Unidades, 14, 5)));
               AV59Complexidade = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV63TipoDias = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TipoDias", AV63TipoDias);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV11LogResponsavel_DemandaCod, AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV32ddo_LogResponsavel_StatusTitleControlIdToReplace, AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV75Pgmname, AV20TFLogResponsavel_DataHora, AV21TFLogResponsavel_DataHora_To, AV27TFLogResponsavel_Acao_Sels, AV31TFLogResponsavel_Status_Sels, AV35TFLogResponsavel_NovoStatus_Sels, AV38TFLogResponsavel_Prazo, AV39TFLogResponsavel_Prazo_To, A54Usuario_Ativo, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV18WWPContext, AV70Codigo, A892LogResponsavel_DemandaCod, A1553ContagemResultado_CntSrvCod, A1611ContagemResultado_PrzTpDias, A798ContagemResultado_PFBFSImp, A1234LogResponsavel_NovoStatus, A1237ContagemResultado_PrazoMaisDias, A1227ContagemResultado_PrazoInicialDias, A471ContagemResultado_DataDmn, AV62NovaData, AV61UltimoPrazo, A893LogResponsavel_DataHora, A1149LogResponsavel_OwnerEhContratante, A894LogResponsavel_Acao, AV51PrazoSugerido, A1177LogResponsavel_Prazo, AV67Selecionadas, AV52Selected, A1228ContratadaUsuario_AreaTrabalhoCod, AV5Destino, A69ContratadaUsuario_UsuarioCod, AV6Emissor, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, AV71PrazoInicial, AV72NaoConsiderarPrazoInicial, AV58ContratoServicos_Codigo, AV60Unidades, AV59Complexidade, AV63TipoDias) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV11LogResponsavel_DemandaCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11LogResponsavel_DemandaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOGRESPONSAVEL_DEMANDACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11LogResponsavel_DemandaCod), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAR62( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTR62( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221243541");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_corretor.aspx") + "?" + UrlEncode("" +AV11LogResponsavel_DemandaCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_9", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_9), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV47DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV47DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA", AV19LogResponsavel_DataHoraTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA", AV19LogResponsavel_DataHoraTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOGRESPONSAVEL_ACAOTITLEFILTERDATA", AV25LogResponsavel_AcaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOGRESPONSAVEL_ACAOTITLEFILTERDATA", AV25LogResponsavel_AcaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOGRESPONSAVEL_STATUSTITLEFILTERDATA", AV29LogResponsavel_StatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOGRESPONSAVEL_STATUSTITLEFILTERDATA", AV29LogResponsavel_StatusTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA", AV33LogResponsavel_NovoStatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA", AV33LogResponsavel_NovoStatusTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA", AV37LogResponsavel_PrazoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA", AV37LogResponsavel_PrazoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vLOGRESPONSAVEL_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11LogResponsavel_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV75Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFLOGRESPONSAVEL_ACAO_SELS", AV27TFLogResponsavel_Acao_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFLOGRESPONSAVEL_ACAO_SELS", AV27TFLogResponsavel_Acao_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFLOGRESPONSAVEL_STATUS_SELS", AV31TFLogResponsavel_Status_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFLOGRESPONSAVEL_STATUS_SELS", AV31TFLogResponsavel_Status_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFLOGRESPONSAVEL_NOVOSTATUS_SELS", AV35TFLogResponsavel_NovoStatus_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFLOGRESPONSAVEL_NOVOSTATUS_SELS", AV35TFLogResponsavel_NovoStatus_Sels);
         }
         GxWebStd.gx_boolean_hidden_field( context, "USUARIO_ATIVO", A54Usuario_Ativo);
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "USUARIO_PESSOANOM", StringUtil.RTrim( A58Usuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A891LogResponsavel_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DEMANDAFM", A493ContagemResultado_DemandaFM);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV18WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV18WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV70Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PRZTPDIAS", StringUtil.RTrim( A1611ContagemResultado_PrzTpDias));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSIMP", StringUtil.LTrim( StringUtil.NToC( A798ContagemResultado_PFBFSImp, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATADMN", context.localUtil.DToC( A471ContagemResultado_DataDmn, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vNOVADATA", context.localUtil.DToC( AV62NovaData, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vULTIMOPRAZO", context.localUtil.TToC( AV61UltimoPrazo, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vSELECIONADAS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67Selecionadas), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_SIGLA", StringUtil.RTrim( A438Contratada_Sigla));
         GxWebStd.gx_hidden_field( context, "vPRAZOINICIAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71PrazoInicial), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUNIDADES", StringUtil.LTrim( StringUtil.NToC( AV60Unidades, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCOMPLEXIDADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59Complexidade), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTIPODIAS", StringUtil.RTrim( AV63TipoDias));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A896LogResponsavel_Owner), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "LOGRESPONSAVEL_OWNEREHCONTRATANTE", A1149LogResponsavel_OwnerEhContratante);
         GxWebStd.gx_hidden_field( context, "gxhash_vLOGRESPONSAVEL_DEMANDACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11LogResponsavel_DemandaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOGRESPONSAVEL_DEMANDACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11LogResponsavel_DemandaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Caption", StringUtil.RTrim( Ddo_logresponsavel_datahora_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Tooltip", StringUtil.RTrim( Ddo_logresponsavel_datahora_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Cls", StringUtil.RTrim( Ddo_logresponsavel_datahora_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Filteredtext_set", StringUtil.RTrim( Ddo_logresponsavel_datahora_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Filteredtextto_set", StringUtil.RTrim( Ddo_logresponsavel_datahora_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Dropdownoptionstype", StringUtil.RTrim( Ddo_logresponsavel_datahora_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_logresponsavel_datahora_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Includesortasc", StringUtil.BoolToStr( Ddo_logresponsavel_datahora_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Includesortdsc", StringUtil.BoolToStr( Ddo_logresponsavel_datahora_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Includefilter", StringUtil.BoolToStr( Ddo_logresponsavel_datahora_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Filtertype", StringUtil.RTrim( Ddo_logresponsavel_datahora_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Filterisrange", StringUtil.BoolToStr( Ddo_logresponsavel_datahora_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Includedatalist", StringUtil.BoolToStr( Ddo_logresponsavel_datahora_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Cleanfilter", StringUtil.RTrim( Ddo_logresponsavel_datahora_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Rangefilterfrom", StringUtil.RTrim( Ddo_logresponsavel_datahora_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Rangefilterto", StringUtil.RTrim( Ddo_logresponsavel_datahora_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Searchbuttontext", StringUtil.RTrim( Ddo_logresponsavel_datahora_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Caption", StringUtil.RTrim( Ddo_logresponsavel_acao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Tooltip", StringUtil.RTrim( Ddo_logresponsavel_acao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Cls", StringUtil.RTrim( Ddo_logresponsavel_acao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Selectedvalue_set", StringUtil.RTrim( Ddo_logresponsavel_acao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_logresponsavel_acao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_logresponsavel_acao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Includesortasc", StringUtil.BoolToStr( Ddo_logresponsavel_acao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Includesortdsc", StringUtil.BoolToStr( Ddo_logresponsavel_acao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Includefilter", StringUtil.BoolToStr( Ddo_logresponsavel_acao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Includedatalist", StringUtil.BoolToStr( Ddo_logresponsavel_acao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Datalisttype", StringUtil.RTrim( Ddo_logresponsavel_acao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_logresponsavel_acao_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_logresponsavel_acao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Cleanfilter", StringUtil.RTrim( Ddo_logresponsavel_acao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Searchbuttontext", StringUtil.RTrim( Ddo_logresponsavel_acao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Caption", StringUtil.RTrim( Ddo_logresponsavel_status_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Tooltip", StringUtil.RTrim( Ddo_logresponsavel_status_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Cls", StringUtil.RTrim( Ddo_logresponsavel_status_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_logresponsavel_status_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_logresponsavel_status_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_logresponsavel_status_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Includesortasc", StringUtil.BoolToStr( Ddo_logresponsavel_status_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_logresponsavel_status_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Includefilter", StringUtil.BoolToStr( Ddo_logresponsavel_status_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Includedatalist", StringUtil.BoolToStr( Ddo_logresponsavel_status_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Datalisttype", StringUtil.RTrim( Ddo_logresponsavel_status_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_logresponsavel_status_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Datalistfixedvalues", StringUtil.RTrim( Ddo_logresponsavel_status_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Cleanfilter", StringUtil.RTrim( Ddo_logresponsavel_status_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Searchbuttontext", StringUtil.RTrim( Ddo_logresponsavel_status_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Caption", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Tooltip", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Cls", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Includesortasc", StringUtil.BoolToStr( Ddo_logresponsavel_novostatus_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_logresponsavel_novostatus_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Includefilter", StringUtil.BoolToStr( Ddo_logresponsavel_novostatus_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Includedatalist", StringUtil.BoolToStr( Ddo_logresponsavel_novostatus_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Datalisttype", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_logresponsavel_novostatus_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Datalistfixedvalues", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Cleanfilter", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Searchbuttontext", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Caption", StringUtil.RTrim( Ddo_logresponsavel_prazo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Tooltip", StringUtil.RTrim( Ddo_logresponsavel_prazo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Cls", StringUtil.RTrim( Ddo_logresponsavel_prazo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Filteredtext_set", StringUtil.RTrim( Ddo_logresponsavel_prazo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Filteredtextto_set", StringUtil.RTrim( Ddo_logresponsavel_prazo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Dropdownoptionstype", StringUtil.RTrim( Ddo_logresponsavel_prazo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_logresponsavel_prazo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Includesortasc", StringUtil.BoolToStr( Ddo_logresponsavel_prazo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Includesortdsc", StringUtil.BoolToStr( Ddo_logresponsavel_prazo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Includefilter", StringUtil.BoolToStr( Ddo_logresponsavel_prazo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Filtertype", StringUtil.RTrim( Ddo_logresponsavel_prazo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Filterisrange", StringUtil.BoolToStr( Ddo_logresponsavel_prazo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Includedatalist", StringUtil.BoolToStr( Ddo_logresponsavel_prazo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Cleanfilter", StringUtil.RTrim( Ddo_logresponsavel_prazo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Rangefilterfrom", StringUtil.RTrim( Ddo_logresponsavel_prazo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Rangefilterto", StringUtil.RTrim( Ddo_logresponsavel_prazo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Searchbuttontext", StringUtil.RTrim( Ddo_logresponsavel_prazo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_DEMANDACOD_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLogResponsavel_DemandaCod_Visible), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Activeeventkey", StringUtil.RTrim( Ddo_logresponsavel_datahora_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Filteredtext_get", StringUtil.RTrim( Ddo_logresponsavel_datahora_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_DATAHORA_Filteredtextto_get", StringUtil.RTrim( Ddo_logresponsavel_datahora_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Activeeventkey", StringUtil.RTrim( Ddo_logresponsavel_acao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_ACAO_Selectedvalue_get", StringUtil.RTrim( Ddo_logresponsavel_acao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Activeeventkey", StringUtil.RTrim( Ddo_logresponsavel_status_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_STATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_logresponsavel_status_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Activeeventkey", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_NOVOSTATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_logresponsavel_novostatus_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Activeeventkey", StringUtil.RTrim( Ddo_logresponsavel_prazo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Filteredtext_get", StringUtil.RTrim( Ddo_logresponsavel_prazo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOGRESPONSAVEL_PRAZO_Filteredtextto_get", StringUtil.RTrim( Ddo_logresponsavel_prazo_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WER62( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTR62( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_corretor.aspx") + "?" + UrlEncode("" +AV11LogResponsavel_DemandaCod) ;
      }

      public override String GetPgmname( )
      {
         return "WP_Corretor" ;
      }

      public override String GetPgmdesc( )
      {
         return "Corretor" ;
      }

      protected void WBR60( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_R62( true) ;
         }
         else
         {
            wb_table1_2_R62( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_R62e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            ROClassString = "Attribute";
            GxWebStd.gx_single_line_edit( context, edtLogResponsavel_DemandaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A892LogResponsavel_DemandaCod), "ZZZZZ9"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLogResponsavel_DemandaCod_Jsonclick, 0, "Attribute", "", ROClassString, "", edtLogResponsavel_DemandaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_WP_Corretor.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflogresponsavel_datahora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflogresponsavel_datahora_Internalname, context.localUtil.TToC( AV20TFLogResponsavel_DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV20TFLogResponsavel_DataHora, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflogresponsavel_datahora_Jsonclick, 0, "Attribute", "", "", "", edtavTflogresponsavel_datahora_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_Corretor.htm");
            GxWebStd.gx_bitmap( context, edtavTflogresponsavel_datahora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflogresponsavel_datahora_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Corretor.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflogresponsavel_datahora_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflogresponsavel_datahora_to_Internalname, context.localUtil.TToC( AV21TFLogResponsavel_DataHora_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV21TFLogResponsavel_DataHora_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflogresponsavel_datahora_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflogresponsavel_datahora_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_Corretor.htm");
            GxWebStd.gx_bitmap( context, edtavTflogresponsavel_datahora_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflogresponsavel_datahora_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Corretor.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_logresponsavel_datahoraauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_logresponsavel_datahoraauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_logresponsavel_datahoraauxdate_Internalname, context.localUtil.Format(AV22DDO_LogResponsavel_DataHoraAuxDate, "99/99/99"), context.localUtil.Format( AV22DDO_LogResponsavel_DataHoraAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_logresponsavel_datahoraauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Corretor.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_logresponsavel_datahoraauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Corretor.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_logresponsavel_datahoraauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_logresponsavel_datahoraauxdateto_Internalname, context.localUtil.Format(AV23DDO_LogResponsavel_DataHoraAuxDateTo, "99/99/99"), context.localUtil.Format( AV23DDO_LogResponsavel_DataHoraAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_logresponsavel_datahoraauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Corretor.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_logresponsavel_datahoraauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Corretor.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflogresponsavel_prazo_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflogresponsavel_prazo_Internalname, context.localUtil.TToC( AV38TFLogResponsavel_Prazo, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV38TFLogResponsavel_Prazo, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflogresponsavel_prazo_Jsonclick, 0, "Attribute", "", "", "", edtavTflogresponsavel_prazo_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Corretor.htm");
            GxWebStd.gx_bitmap( context, edtavTflogresponsavel_prazo_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflogresponsavel_prazo_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Corretor.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflogresponsavel_prazo_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflogresponsavel_prazo_to_Internalname, context.localUtil.TToC( AV39TFLogResponsavel_Prazo_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV39TFLogResponsavel_Prazo_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflogresponsavel_prazo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflogresponsavel_prazo_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Corretor.htm");
            GxWebStd.gx_bitmap( context, edtavTflogresponsavel_prazo_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflogresponsavel_prazo_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Corretor.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_logresponsavel_prazoauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_logresponsavel_prazoauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_logresponsavel_prazoauxdate_Internalname, context.localUtil.Format(AV40DDO_LogResponsavel_PrazoAuxDate, "99/99/99"), context.localUtil.Format( AV40DDO_LogResponsavel_PrazoAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_logresponsavel_prazoauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Corretor.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_logresponsavel_prazoauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Corretor.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_9_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_logresponsavel_prazoauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_logresponsavel_prazoauxdateto_Internalname, context.localUtil.Format(AV41DDO_LogResponsavel_PrazoAuxDateTo, "99/99/99"), context.localUtil.Format( AV41DDO_LogResponsavel_PrazoAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_logresponsavel_prazoauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Corretor.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_logresponsavel_prazoauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_Corretor.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOGRESPONSAVEL_DATAHORAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Internalname, AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WP_Corretor.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOGRESPONSAVEL_ACAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Internalname, AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", 0, edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WP_Corretor.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOGRESPONSAVEL_STATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Internalname, AV32ddo_LogResponsavel_StatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", 0, edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WP_Corretor.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOGRESPONSAVEL_NOVOSTATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Internalname, AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", 0, edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WP_Corretor.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOGRESPONSAVEL_PRAZOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Internalname, AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", 0, edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WP_Corretor.htm");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_Corretor.htm");
         }
         wbLoad = true;
      }

      protected void STARTR62( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Corretor", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPR60( ) ;
      }

      protected void WSR62( )
      {
         STARTR62( ) ;
         EVTR62( ) ;
      }

      protected void EVTR62( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOGRESPONSAVEL_DATAHORA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11R62 */
                              E11R62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOGRESPONSAVEL_ACAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12R62 */
                              E12R62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOGRESPONSAVEL_STATUS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13R62 */
                              E13R62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOGRESPONSAVEL_NOVOSTATUS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14R62 */
                              E14R62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_LOGRESPONSAVEL_PRAZO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15R62 */
                              E15R62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'FECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16R62 */
                              E16R62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CORREGIR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17R62 */
                              E17R62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VNAOCONSIDERARPRAZOINICIAL.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18R62 */
                              E18R62 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VSELECTED.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VSELECTED.CLICK") == 0 ) )
                           {
                              nGXsfl_9_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
                              SubsflControlProps_92( ) ;
                              A892LogResponsavel_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtLogResponsavel_DemandaCod_Internalname), ",", "."));
                              n892LogResponsavel_DemandaCod = false;
                              A1797LogResponsavel_Codigo = (long)(context.localUtil.CToN( cgiGet( edtLogResponsavel_Codigo_Internalname), ",", "."));
                              A893LogResponsavel_DataHora = context.localUtil.CToT( cgiGet( edtLogResponsavel_DataHora_Internalname), 0);
                              cmbavEmissor.Name = cmbavEmissor_Internalname;
                              cmbavEmissor.CurrentValue = cgiGet( cmbavEmissor_Internalname);
                              AV6Emissor = (int)(NumberUtil.Val( cgiGet( cmbavEmissor_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0)));
                              cmbLogResponsavel_Acao.Name = cmbLogResponsavel_Acao_Internalname;
                              cmbLogResponsavel_Acao.CurrentValue = cgiGet( cmbLogResponsavel_Acao_Internalname);
                              A894LogResponsavel_Acao = cgiGet( cmbLogResponsavel_Acao_Internalname);
                              cmbavDestino.Name = cmbavDestino_Internalname;
                              cmbavDestino.CurrentValue = cgiGet( cmbavDestino_Internalname);
                              AV5Destino = (int)(NumberUtil.Val( cgiGet( cmbavDestino_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Destino), 6, 0)));
                              cmbLogResponsavel_Status.Name = cmbLogResponsavel_Status_Internalname;
                              cmbLogResponsavel_Status.CurrentValue = cgiGet( cmbLogResponsavel_Status_Internalname);
                              A1130LogResponsavel_Status = cgiGet( cmbLogResponsavel_Status_Internalname);
                              n1130LogResponsavel_Status = false;
                              cmbLogResponsavel_NovoStatus.Name = cmbLogResponsavel_NovoStatus_Internalname;
                              cmbLogResponsavel_NovoStatus.CurrentValue = cgiGet( cmbLogResponsavel_NovoStatus_Internalname);
                              A1234LogResponsavel_NovoStatus = cgiGet( cmbLogResponsavel_NovoStatus_Internalname);
                              n1234LogResponsavel_NovoStatus = false;
                              A1177LogResponsavel_Prazo = context.localUtil.CToT( cgiGet( edtLogResponsavel_Prazo_Internalname), 0);
                              n1177LogResponsavel_Prazo = false;
                              A1227ContagemResultado_PrazoInicialDias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultado_PrazoInicialDias_Internalname), ",", "."));
                              n1227ContagemResultado_PrazoInicialDias = false;
                              A1237ContagemResultado_PrazoMaisDias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultado_PrazoMaisDias_Internalname), ",", "."));
                              n1237ContagemResultado_PrazoMaisDias = false;
                              AV52Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV52Selected);
                              if ( context.localUtil.VCDateTime( cgiGet( edtavPrazosugerido_Internalname), 0, 0) == 0 )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Prazo Sugerido"}), 1, "vPRAZOSUGERIDO");
                                 GX_FocusControl = edtavPrazosugerido_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV51PrazoSugerido = (DateTime)(DateTime.MinValue);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
                              }
                              else
                              {
                                 AV51PrazoSugerido = context.localUtil.CToT( cgiGet( edtavPrazosugerido_Internalname), 0);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavDias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavDias_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vDIAS");
                                 GX_FocusControl = edtavDias_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV54Dias = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Dias), 4, 0)));
                              }
                              else
                              {
                                 AV54Dias = (short)(context.localUtil.CToN( cgiGet( edtavDias_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Dias), 4, 0)));
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E19R62 */
                                    E19R62 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E20R62 */
                                    E20R62 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E21R62 */
                                    E21R62 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VSELECTED.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E22R62 */
                                    E22R62 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WER62( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAR62( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            chkavNaoconsiderarprazoinicial.Name = "vNAOCONSIDERARPRAZOINICIAL";
            chkavNaoconsiderarprazoinicial.WebTags = "";
            chkavNaoconsiderarprazoinicial.Caption = "N�o considerar Prazo Inicial (BD)";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavNaoconsiderarprazoinicial_Internalname, "TitleCaption", chkavNaoconsiderarprazoinicial.Caption);
            chkavNaoconsiderarprazoinicial.CheckedValue = "false";
            GXCCtl = "vEMISSOR_" + sGXsfl_9_idx;
            cmbavEmissor.Name = GXCCtl;
            cmbavEmissor.WebTags = "";
            if ( cmbavEmissor.ItemCount > 0 )
            {
               AV6Emissor = (int)(NumberUtil.Val( cmbavEmissor.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0)));
            }
            GXCCtl = "LOGRESPONSAVEL_ACAO_" + sGXsfl_9_idx;
            cmbLogResponsavel_Acao.Name = GXCCtl;
            cmbLogResponsavel_Acao.WebTags = "";
            cmbLogResponsavel_Acao.addItem("A", "Atribui", 0);
            cmbLogResponsavel_Acao.addItem("B", "StandBy", 0);
            cmbLogResponsavel_Acao.addItem("C", "Captura", 0);
            cmbLogResponsavel_Acao.addItem("R", "Rejeita", 0);
            cmbLogResponsavel_Acao.addItem("L", "Libera", 0);
            cmbLogResponsavel_Acao.addItem("E", "Encaminha", 0);
            cmbLogResponsavel_Acao.addItem("I", "Importa", 0);
            cmbLogResponsavel_Acao.addItem("S", "Solicita", 0);
            cmbLogResponsavel_Acao.addItem("D", "Diverg�ncia", 0);
            cmbLogResponsavel_Acao.addItem("V", "Resolvida", 0);
            cmbLogResponsavel_Acao.addItem("H", "Homologa", 0);
            cmbLogResponsavel_Acao.addItem("Q", "Liquida", 0);
            cmbLogResponsavel_Acao.addItem("P", "Fatura", 0);
            cmbLogResponsavel_Acao.addItem("O", "Aceite", 0);
            cmbLogResponsavel_Acao.addItem("N", "N�o Acata", 0);
            cmbLogResponsavel_Acao.addItem("M", "Autom�tica", 0);
            cmbLogResponsavel_Acao.addItem("F", "Cumprido", 0);
            cmbLogResponsavel_Acao.addItem("T", "Acata", 0);
            cmbLogResponsavel_Acao.addItem("X", "Cancela", 0);
            cmbLogResponsavel_Acao.addItem("NO", "Nota", 0);
            cmbLogResponsavel_Acao.addItem("U", "Altera", 0);
            cmbLogResponsavel_Acao.addItem("UN", "Rascunho", 0);
            cmbLogResponsavel_Acao.addItem("EA", "Em An�lise", 0);
            cmbLogResponsavel_Acao.addItem("RN", "Reuni�o", 0);
            cmbLogResponsavel_Acao.addItem("BK", "Desfaz", 0);
            cmbLogResponsavel_Acao.addItem("RE", "Reinicio", 0);
            cmbLogResponsavel_Acao.addItem("PR", "Prioridade", 0);
            if ( cmbLogResponsavel_Acao.ItemCount > 0 )
            {
               A894LogResponsavel_Acao = cmbLogResponsavel_Acao.getValidValue(A894LogResponsavel_Acao);
            }
            GXCCtl = "vDESTINO_" + sGXsfl_9_idx;
            cmbavDestino.Name = GXCCtl;
            cmbavDestino.WebTags = "";
            if ( cmbavDestino.ItemCount > 0 )
            {
               AV5Destino = (int)(NumberUtil.Val( cmbavDestino.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Destino), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Destino), 6, 0)));
            }
            GXCCtl = "LOGRESPONSAVEL_STATUS_" + sGXsfl_9_idx;
            cmbLogResponsavel_Status.Name = GXCCtl;
            cmbLogResponsavel_Status.WebTags = "";
            cmbLogResponsavel_Status.addItem("B", "Stand by", 0);
            cmbLogResponsavel_Status.addItem("S", "Solicitada", 0);
            cmbLogResponsavel_Status.addItem("E", "Em An�lise", 0);
            cmbLogResponsavel_Status.addItem("A", "Em execu��o", 0);
            cmbLogResponsavel_Status.addItem("R", "Resolvida", 0);
            cmbLogResponsavel_Status.addItem("C", "Conferida", 0);
            cmbLogResponsavel_Status.addItem("D", "Rejeitada", 0);
            cmbLogResponsavel_Status.addItem("H", "Homologada", 0);
            cmbLogResponsavel_Status.addItem("O", "Aceite", 0);
            cmbLogResponsavel_Status.addItem("P", "A Pagar", 0);
            cmbLogResponsavel_Status.addItem("L", "Liquidada", 0);
            cmbLogResponsavel_Status.addItem("X", "Cancelada", 0);
            cmbLogResponsavel_Status.addItem("N", "N�o Faturada", 0);
            cmbLogResponsavel_Status.addItem("J", "Planejamento", 0);
            cmbLogResponsavel_Status.addItem("I", "An�lise Planejamento", 0);
            cmbLogResponsavel_Status.addItem("T", "Validacao T�cnica", 0);
            cmbLogResponsavel_Status.addItem("Q", "Validacao Qualidade", 0);
            cmbLogResponsavel_Status.addItem("G", "Em Homologa��o", 0);
            cmbLogResponsavel_Status.addItem("M", "Valida��o Mensura��o", 0);
            cmbLogResponsavel_Status.addItem("U", "Rascunho", 0);
            if ( cmbLogResponsavel_Status.ItemCount > 0 )
            {
               A1130LogResponsavel_Status = cmbLogResponsavel_Status.getValidValue(A1130LogResponsavel_Status);
               n1130LogResponsavel_Status = false;
            }
            GXCCtl = "LOGRESPONSAVEL_NOVOSTATUS_" + sGXsfl_9_idx;
            cmbLogResponsavel_NovoStatus.Name = GXCCtl;
            cmbLogResponsavel_NovoStatus.WebTags = "";
            cmbLogResponsavel_NovoStatus.addItem("B", "Stand by", 0);
            cmbLogResponsavel_NovoStatus.addItem("S", "Solicitada", 0);
            cmbLogResponsavel_NovoStatus.addItem("E", "Em An�lise", 0);
            cmbLogResponsavel_NovoStatus.addItem("A", "Em execu��o", 0);
            cmbLogResponsavel_NovoStatus.addItem("R", "Resolvida", 0);
            cmbLogResponsavel_NovoStatus.addItem("C", "Conferida", 0);
            cmbLogResponsavel_NovoStatus.addItem("D", "Rejeitada", 0);
            cmbLogResponsavel_NovoStatus.addItem("H", "Homologada", 0);
            cmbLogResponsavel_NovoStatus.addItem("O", "Aceite", 0);
            cmbLogResponsavel_NovoStatus.addItem("P", "A Pagar", 0);
            cmbLogResponsavel_NovoStatus.addItem("L", "Liquidada", 0);
            cmbLogResponsavel_NovoStatus.addItem("X", "Cancelada", 0);
            cmbLogResponsavel_NovoStatus.addItem("N", "N�o Faturada", 0);
            cmbLogResponsavel_NovoStatus.addItem("J", "Planejamento", 0);
            cmbLogResponsavel_NovoStatus.addItem("I", "An�lise Planejamento", 0);
            cmbLogResponsavel_NovoStatus.addItem("T", "Validacao T�cnica", 0);
            cmbLogResponsavel_NovoStatus.addItem("Q", "Validacao Qualidade", 0);
            cmbLogResponsavel_NovoStatus.addItem("G", "Em Homologa��o", 0);
            cmbLogResponsavel_NovoStatus.addItem("M", "Valida��o Mensura��o", 0);
            cmbLogResponsavel_NovoStatus.addItem("U", "Rascunho", 0);
            if ( cmbLogResponsavel_NovoStatus.ItemCount > 0 )
            {
               A1234LogResponsavel_NovoStatus = cmbLogResponsavel_NovoStatus.getValidValue(A1234LogResponsavel_NovoStatus);
               n1234LogResponsavel_NovoStatus = false;
            }
            GXCCtl = "vSELECTED_" + sGXsfl_9_idx;
            chkavSelected.Name = GXCCtl;
            chkavSelected.WebTags = "";
            chkavSelected.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelected_Internalname, "TitleCaption", chkavSelected.Caption);
            chkavSelected.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = chkavNaoconsiderarprazoinicial_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_92( ) ;
         while ( nGXsfl_9_idx <= nRC_GXsfl_9 )
         {
            sendrow_92( ) ;
            nGXsfl_9_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_9_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_9_idx+1));
            sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
            SubsflControlProps_92( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int AV11LogResponsavel_DemandaCod ,
                                       String AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace ,
                                       String AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace ,
                                       String AV32ddo_LogResponsavel_StatusTitleControlIdToReplace ,
                                       String AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace ,
                                       String AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace ,
                                       String AV75Pgmname ,
                                       DateTime AV20TFLogResponsavel_DataHora ,
                                       DateTime AV21TFLogResponsavel_DataHora_To ,
                                       IGxCollection AV27TFLogResponsavel_Acao_Sels ,
                                       IGxCollection AV31TFLogResponsavel_Status_Sels ,
                                       IGxCollection AV35TFLogResponsavel_NovoStatus_Sels ,
                                       DateTime AV38TFLogResponsavel_Prazo ,
                                       DateTime AV39TFLogResponsavel_Prazo_To ,
                                       bool A54Usuario_Ativo ,
                                       int A1Usuario_Codigo ,
                                       String A58Usuario_PessoaNom ,
                                       int A896LogResponsavel_Owner ,
                                       int A891LogResponsavel_UsuarioCod ,
                                       String A493ContagemResultado_DemandaFM ,
                                       wwpbaseobjects.SdtWWPContext AV18WWPContext ,
                                       int AV70Codigo ,
                                       int A892LogResponsavel_DemandaCod ,
                                       int A1553ContagemResultado_CntSrvCod ,
                                       String A1611ContagemResultado_PrzTpDias ,
                                       decimal A798ContagemResultado_PFBFSImp ,
                                       String A1234LogResponsavel_NovoStatus ,
                                       short A1237ContagemResultado_PrazoMaisDias ,
                                       short A1227ContagemResultado_PrazoInicialDias ,
                                       DateTime A471ContagemResultado_DataDmn ,
                                       DateTime AV62NovaData ,
                                       DateTime AV61UltimoPrazo ,
                                       DateTime A893LogResponsavel_DataHora ,
                                       bool A1149LogResponsavel_OwnerEhContratante ,
                                       String A894LogResponsavel_Acao ,
                                       DateTime AV51PrazoSugerido ,
                                       DateTime A1177LogResponsavel_Prazo ,
                                       short AV67Selecionadas ,
                                       bool AV52Selected ,
                                       int A1228ContratadaUsuario_AreaTrabalhoCod ,
                                       int AV5Destino ,
                                       int A69ContratadaUsuario_UsuarioCod ,
                                       int AV6Emissor ,
                                       int A66ContratadaUsuario_ContratadaCod ,
                                       String A438Contratada_Sigla ,
                                       short AV71PrazoInicial ,
                                       bool AV72NaoConsiderarPrazoInicial ,
                                       int AV58ContratoServicos_Codigo ,
                                       decimal AV60Unidades ,
                                       short AV59Complexidade ,
                                       String AV63TipoDias )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFR62( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_DEMANDACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A892LogResponsavel_DemandaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_DATAHORA", GetSecureSignedToken( "", context.localUtil.Format( A893LogResponsavel_DataHora, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_DATAHORA", context.localUtil.TToC( A893LogResponsavel_DataHora, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_ACAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A894LogResponsavel_Acao, ""))));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_ACAO", StringUtil.RTrim( A894LogResponsavel_Acao));
         GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_STATUS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1130LogResponsavel_Status, ""))));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_STATUS", StringUtil.RTrim( A1130LogResponsavel_Status));
         GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_NOVOSTATUS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1234LogResponsavel_NovoStatus, ""))));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_NOVOSTATUS", StringUtil.RTrim( A1234LogResponsavel_NovoStatus));
         GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_PRAZO", GetSecureSignedToken( "", context.localUtil.Format( A1177LogResponsavel_Prazo, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "LOGRESPONSAVEL_PRAZO", context.localUtil.TToC( A1177LogResponsavel_Prazo, 10, 8, 0, 3, "/", ":", " "));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFR62( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV75Pgmname = "WP_Corretor";
         context.Gx_err = 0;
         cmbavEmissor.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEmissor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavEmissor.Enabled), 5, 0)));
         cmbavDestino.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDestino_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDestino.Enabled), 5, 0)));
         edtavPrazosugerido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrazosugerido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazosugerido_Enabled), 5, 0)));
         edtavDias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDias_Enabled), 5, 0)));
      }

      protected void RFR62( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 9;
         /* Execute user event: E20R62 */
         E20R62 ();
         nGXsfl_9_idx = 1;
         sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
         SubsflControlProps_92( ) ;
         nGXsfl_9_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_92( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A892LogResponsavel_DemandaCod ,
                                                 AV68Codigos ,
                                                 AV68Codigos.Count ,
                                                 AV11LogResponsavel_DemandaCod },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            /* Using cursor H00R63 */
            pr_default.execute(0, new Object[] {AV11LogResponsavel_DemandaCod, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_9_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A891LogResponsavel_UsuarioCod = H00R63_A891LogResponsavel_UsuarioCod[0];
               n891LogResponsavel_UsuarioCod = H00R63_n891LogResponsavel_UsuarioCod[0];
               A493ContagemResultado_DemandaFM = H00R63_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00R63_n493ContagemResultado_DemandaFM[0];
               A1553ContagemResultado_CntSrvCod = H00R63_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00R63_n1553ContagemResultado_CntSrvCod[0];
               A1611ContagemResultado_PrzTpDias = H00R63_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = H00R63_n1611ContagemResultado_PrzTpDias[0];
               A798ContagemResultado_PFBFSImp = H00R63_A798ContagemResultado_PFBFSImp[0];
               n798ContagemResultado_PFBFSImp = H00R63_n798ContagemResultado_PFBFSImp[0];
               A471ContagemResultado_DataDmn = H00R63_A471ContagemResultado_DataDmn[0];
               n471ContagemResultado_DataDmn = H00R63_n471ContagemResultado_DataDmn[0];
               A1237ContagemResultado_PrazoMaisDias = H00R63_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = H00R63_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = H00R63_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = H00R63_n1227ContagemResultado_PrazoInicialDias[0];
               A1177LogResponsavel_Prazo = H00R63_A1177LogResponsavel_Prazo[0];
               n1177LogResponsavel_Prazo = H00R63_n1177LogResponsavel_Prazo[0];
               A1234LogResponsavel_NovoStatus = H00R63_A1234LogResponsavel_NovoStatus[0];
               n1234LogResponsavel_NovoStatus = H00R63_n1234LogResponsavel_NovoStatus[0];
               A1130LogResponsavel_Status = H00R63_A1130LogResponsavel_Status[0];
               n1130LogResponsavel_Status = H00R63_n1130LogResponsavel_Status[0];
               A894LogResponsavel_Acao = H00R63_A894LogResponsavel_Acao[0];
               A893LogResponsavel_DataHora = H00R63_A893LogResponsavel_DataHora[0];
               A1797LogResponsavel_Codigo = H00R63_A1797LogResponsavel_Codigo[0];
               A40000ContratadaUsuario_UsuarioCod = H00R63_A40000ContratadaUsuario_UsuarioCod[0];
               n40000ContratadaUsuario_UsuarioCod = H00R63_n40000ContratadaUsuario_UsuarioCod[0];
               A896LogResponsavel_Owner = H00R63_A896LogResponsavel_Owner[0];
               A892LogResponsavel_DemandaCod = H00R63_A892LogResponsavel_DemandaCod[0];
               n892LogResponsavel_DemandaCod = H00R63_n892LogResponsavel_DemandaCod[0];
               A40000ContratadaUsuario_UsuarioCod = H00R63_A40000ContratadaUsuario_UsuarioCod[0];
               n40000ContratadaUsuario_UsuarioCod = H00R63_n40000ContratadaUsuario_UsuarioCod[0];
               A493ContagemResultado_DemandaFM = H00R63_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00R63_n493ContagemResultado_DemandaFM[0];
               A1553ContagemResultado_CntSrvCod = H00R63_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00R63_n1553ContagemResultado_CntSrvCod[0];
               A798ContagemResultado_PFBFSImp = H00R63_A798ContagemResultado_PFBFSImp[0];
               n798ContagemResultado_PFBFSImp = H00R63_n798ContagemResultado_PFBFSImp[0];
               A471ContagemResultado_DataDmn = H00R63_A471ContagemResultado_DataDmn[0];
               n471ContagemResultado_DataDmn = H00R63_n471ContagemResultado_DataDmn[0];
               A1237ContagemResultado_PrazoMaisDias = H00R63_A1237ContagemResultado_PrazoMaisDias[0];
               n1237ContagemResultado_PrazoMaisDias = H00R63_n1237ContagemResultado_PrazoMaisDias[0];
               A1227ContagemResultado_PrazoInicialDias = H00R63_A1227ContagemResultado_PrazoInicialDias[0];
               n1227ContagemResultado_PrazoInicialDias = H00R63_n1227ContagemResultado_PrazoInicialDias[0];
               A1611ContagemResultado_PrzTpDias = H00R63_A1611ContagemResultado_PrzTpDias[0];
               n1611ContagemResultado_PrzTpDias = H00R63_n1611ContagemResultado_PrzTpDias[0];
               GXt_boolean1 = A1149LogResponsavel_OwnerEhContratante;
               new prc_usuarioehcontratante(context ).execute( ref  A892LogResponsavel_DemandaCod,  A896LogResponsavel_Owner, out  GXt_boolean1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A896LogResponsavel_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A896LogResponsavel_Owner), 6, 0)));
               A1149LogResponsavel_OwnerEhContratante = GXt_boolean1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1149LogResponsavel_OwnerEhContratante", A1149LogResponsavel_OwnerEhContratante);
               /* Execute user event: E21R62 */
               E21R62 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 9;
            WBR60( ) ;
         }
         nGXsfl_9_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A892LogResponsavel_DemandaCod ,
                                              AV68Codigos ,
                                              AV68Codigos.Count ,
                                              AV11LogResponsavel_DemandaCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor H00R65 */
         pr_default.execute(1, new Object[] {AV11LogResponsavel_DemandaCod});
         GRID_nRecordCount = H00R65_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV11LogResponsavel_DemandaCod, AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV32ddo_LogResponsavel_StatusTitleControlIdToReplace, AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV75Pgmname, AV20TFLogResponsavel_DataHora, AV21TFLogResponsavel_DataHora_To, AV27TFLogResponsavel_Acao_Sels, AV31TFLogResponsavel_Status_Sels, AV35TFLogResponsavel_NovoStatus_Sels, AV38TFLogResponsavel_Prazo, AV39TFLogResponsavel_Prazo_To, A54Usuario_Ativo, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV18WWPContext, AV70Codigo, A892LogResponsavel_DemandaCod, A1553ContagemResultado_CntSrvCod, A1611ContagemResultado_PrzTpDias, A798ContagemResultado_PFBFSImp, A1234LogResponsavel_NovoStatus, A1237ContagemResultado_PrazoMaisDias, A1227ContagemResultado_PrazoInicialDias, A471ContagemResultado_DataDmn, AV62NovaData, AV61UltimoPrazo, A893LogResponsavel_DataHora, A1149LogResponsavel_OwnerEhContratante, A894LogResponsavel_Acao, AV51PrazoSugerido, A1177LogResponsavel_Prazo, AV67Selecionadas, AV52Selected, A1228ContratadaUsuario_AreaTrabalhoCod, AV5Destino, A69ContratadaUsuario_UsuarioCod, AV6Emissor, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, AV71PrazoInicial, AV72NaoConsiderarPrazoInicial, AV58ContratoServicos_Codigo, AV60Unidades, AV59Complexidade, AV63TipoDias) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV11LogResponsavel_DemandaCod, AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV32ddo_LogResponsavel_StatusTitleControlIdToReplace, AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV75Pgmname, AV20TFLogResponsavel_DataHora, AV21TFLogResponsavel_DataHora_To, AV27TFLogResponsavel_Acao_Sels, AV31TFLogResponsavel_Status_Sels, AV35TFLogResponsavel_NovoStatus_Sels, AV38TFLogResponsavel_Prazo, AV39TFLogResponsavel_Prazo_To, A54Usuario_Ativo, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV18WWPContext, AV70Codigo, A892LogResponsavel_DemandaCod, A1553ContagemResultado_CntSrvCod, A1611ContagemResultado_PrzTpDias, A798ContagemResultado_PFBFSImp, A1234LogResponsavel_NovoStatus, A1237ContagemResultado_PrazoMaisDias, A1227ContagemResultado_PrazoInicialDias, A471ContagemResultado_DataDmn, AV62NovaData, AV61UltimoPrazo, A893LogResponsavel_DataHora, A1149LogResponsavel_OwnerEhContratante, A894LogResponsavel_Acao, AV51PrazoSugerido, A1177LogResponsavel_Prazo, AV67Selecionadas, AV52Selected, A1228ContratadaUsuario_AreaTrabalhoCod, AV5Destino, A69ContratadaUsuario_UsuarioCod, AV6Emissor, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, AV71PrazoInicial, AV72NaoConsiderarPrazoInicial, AV58ContratoServicos_Codigo, AV60Unidades, AV59Complexidade, AV63TipoDias) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV11LogResponsavel_DemandaCod, AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV32ddo_LogResponsavel_StatusTitleControlIdToReplace, AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV75Pgmname, AV20TFLogResponsavel_DataHora, AV21TFLogResponsavel_DataHora_To, AV27TFLogResponsavel_Acao_Sels, AV31TFLogResponsavel_Status_Sels, AV35TFLogResponsavel_NovoStatus_Sels, AV38TFLogResponsavel_Prazo, AV39TFLogResponsavel_Prazo_To, A54Usuario_Ativo, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV18WWPContext, AV70Codigo, A892LogResponsavel_DemandaCod, A1553ContagemResultado_CntSrvCod, A1611ContagemResultado_PrzTpDias, A798ContagemResultado_PFBFSImp, A1234LogResponsavel_NovoStatus, A1237ContagemResultado_PrazoMaisDias, A1227ContagemResultado_PrazoInicialDias, A471ContagemResultado_DataDmn, AV62NovaData, AV61UltimoPrazo, A893LogResponsavel_DataHora, A1149LogResponsavel_OwnerEhContratante, A894LogResponsavel_Acao, AV51PrazoSugerido, A1177LogResponsavel_Prazo, AV67Selecionadas, AV52Selected, A1228ContratadaUsuario_AreaTrabalhoCod, AV5Destino, A69ContratadaUsuario_UsuarioCod, AV6Emissor, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, AV71PrazoInicial, AV72NaoConsiderarPrazoInicial, AV58ContratoServicos_Codigo, AV60Unidades, AV59Complexidade, AV63TipoDias) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV11LogResponsavel_DemandaCod, AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV32ddo_LogResponsavel_StatusTitleControlIdToReplace, AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV75Pgmname, AV20TFLogResponsavel_DataHora, AV21TFLogResponsavel_DataHora_To, AV27TFLogResponsavel_Acao_Sels, AV31TFLogResponsavel_Status_Sels, AV35TFLogResponsavel_NovoStatus_Sels, AV38TFLogResponsavel_Prazo, AV39TFLogResponsavel_Prazo_To, A54Usuario_Ativo, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV18WWPContext, AV70Codigo, A892LogResponsavel_DemandaCod, A1553ContagemResultado_CntSrvCod, A1611ContagemResultado_PrzTpDias, A798ContagemResultado_PFBFSImp, A1234LogResponsavel_NovoStatus, A1237ContagemResultado_PrazoMaisDias, A1227ContagemResultado_PrazoInicialDias, A471ContagemResultado_DataDmn, AV62NovaData, AV61UltimoPrazo, A893LogResponsavel_DataHora, A1149LogResponsavel_OwnerEhContratante, A894LogResponsavel_Acao, AV51PrazoSugerido, A1177LogResponsavel_Prazo, AV67Selecionadas, AV52Selected, A1228ContratadaUsuario_AreaTrabalhoCod, AV5Destino, A69ContratadaUsuario_UsuarioCod, AV6Emissor, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, AV71PrazoInicial, AV72NaoConsiderarPrazoInicial, AV58ContratoServicos_Codigo, AV60Unidades, AV59Complexidade, AV63TipoDias) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV11LogResponsavel_DemandaCod, AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV32ddo_LogResponsavel_StatusTitleControlIdToReplace, AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV75Pgmname, AV20TFLogResponsavel_DataHora, AV21TFLogResponsavel_DataHora_To, AV27TFLogResponsavel_Acao_Sels, AV31TFLogResponsavel_Status_Sels, AV35TFLogResponsavel_NovoStatus_Sels, AV38TFLogResponsavel_Prazo, AV39TFLogResponsavel_Prazo_To, A54Usuario_Ativo, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV18WWPContext, AV70Codigo, A892LogResponsavel_DemandaCod, A1553ContagemResultado_CntSrvCod, A1611ContagemResultado_PrzTpDias, A798ContagemResultado_PFBFSImp, A1234LogResponsavel_NovoStatus, A1237ContagemResultado_PrazoMaisDias, A1227ContagemResultado_PrazoInicialDias, A471ContagemResultado_DataDmn, AV62NovaData, AV61UltimoPrazo, A893LogResponsavel_DataHora, A1149LogResponsavel_OwnerEhContratante, A894LogResponsavel_Acao, AV51PrazoSugerido, A1177LogResponsavel_Prazo, AV67Selecionadas, AV52Selected, A1228ContratadaUsuario_AreaTrabalhoCod, AV5Destino, A69ContratadaUsuario_UsuarioCod, AV6Emissor, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, AV71PrazoInicial, AV72NaoConsiderarPrazoInicial, AV58ContratoServicos_Codigo, AV60Unidades, AV59Complexidade, AV63TipoDias) ;
         }
         return (int)(0) ;
      }

      protected void STRUPR60( )
      {
         /* Before Start, stand alone formulas. */
         AV75Pgmname = "WP_Corretor";
         context.Gx_err = 0;
         cmbavEmissor.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEmissor_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavEmissor.Enabled), 5, 0)));
         cmbavDestino.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDestino_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDestino.Enabled), 5, 0)));
         edtavPrazosugerido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrazosugerido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazosugerido_Enabled), 5, 0)));
         edtavDias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDias_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E19R62 */
         E19R62 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV47DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA"), AV19LogResponsavel_DataHoraTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOGRESPONSAVEL_ACAOTITLEFILTERDATA"), AV25LogResponsavel_AcaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOGRESPONSAVEL_STATUSTITLEFILTERDATA"), AV29LogResponsavel_StatusTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA"), AV33LogResponsavel_NovoStatusTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA"), AV37LogResponsavel_PrazoTitleFilterData);
            /* Read variables values. */
            AV72NaoConsiderarPrazoInicial = StringUtil.StrToBool( cgiGet( chkavNaoconsiderarprazoinicial_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72NaoConsiderarPrazoInicial", AV72NaoConsiderarPrazoInicial);
            A892LogResponsavel_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtLogResponsavel_DemandaCod_Internalname), ",", "."));
            n892LogResponsavel_DemandaCod = false;
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflogresponsavel_datahora_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLog Responsavel_Data Hora"}), 1, "vTFLOGRESPONSAVEL_DATAHORA");
               GX_FocusControl = edtavTflogresponsavel_datahora_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20TFLogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFLogResponsavel_DataHora", context.localUtil.TToC( AV20TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV20TFLogResponsavel_DataHora = context.localUtil.CToT( cgiGet( edtavTflogresponsavel_datahora_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFLogResponsavel_DataHora", context.localUtil.TToC( AV20TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflogresponsavel_datahora_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLog Responsavel_Data Hora_To"}), 1, "vTFLOGRESPONSAVEL_DATAHORA_TO");
               GX_FocusControl = edtavTflogresponsavel_datahora_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21TFLogResponsavel_DataHora_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV21TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV21TFLogResponsavel_DataHora_To = context.localUtil.CToT( cgiGet( edtavTflogresponsavel_datahora_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV21TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_logresponsavel_datahoraauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Log Responsavel_Data Hora Aux Date"}), 1, "vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE");
               GX_FocusControl = edtavDdo_logresponsavel_datahoraauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22DDO_LogResponsavel_DataHoraAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DDO_LogResponsavel_DataHoraAuxDate", context.localUtil.Format(AV22DDO_LogResponsavel_DataHoraAuxDate, "99/99/99"));
            }
            else
            {
               AV22DDO_LogResponsavel_DataHoraAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_logresponsavel_datahoraauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DDO_LogResponsavel_DataHoraAuxDate", context.localUtil.Format(AV22DDO_LogResponsavel_DataHoraAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_logresponsavel_datahoraauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Log Responsavel_Data Hora Aux Date To"}), 1, "vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO");
               GX_FocusControl = edtavDdo_logresponsavel_datahoraauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23DDO_LogResponsavel_DataHoraAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DDO_LogResponsavel_DataHoraAuxDateTo", context.localUtil.Format(AV23DDO_LogResponsavel_DataHoraAuxDateTo, "99/99/99"));
            }
            else
            {
               AV23DDO_LogResponsavel_DataHoraAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_logresponsavel_datahoraauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DDO_LogResponsavel_DataHoraAuxDateTo", context.localUtil.Format(AV23DDO_LogResponsavel_DataHoraAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflogresponsavel_prazo_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLog Responsavel_Prazo"}), 1, "vTFLOGRESPONSAVEL_PRAZO");
               GX_FocusControl = edtavTflogresponsavel_prazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38TFLogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLogResponsavel_Prazo", context.localUtil.TToC( AV38TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV38TFLogResponsavel_Prazo = context.localUtil.CToT( cgiGet( edtavTflogresponsavel_prazo_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLogResponsavel_Prazo", context.localUtil.TToC( AV38TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflogresponsavel_prazo_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLog Responsavel_Prazo_To"}), 1, "vTFLOGRESPONSAVEL_PRAZO_TO");
               GX_FocusControl = edtavTflogresponsavel_prazo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFLogResponsavel_Prazo_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV39TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV39TFLogResponsavel_Prazo_To = context.localUtil.CToT( cgiGet( edtavTflogresponsavel_prazo_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV39TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_logresponsavel_prazoauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Log Responsavel_Prazo Aux Date"}), 1, "vDDO_LOGRESPONSAVEL_PRAZOAUXDATE");
               GX_FocusControl = edtavDdo_logresponsavel_prazoauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40DDO_LogResponsavel_PrazoAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DDO_LogResponsavel_PrazoAuxDate", context.localUtil.Format(AV40DDO_LogResponsavel_PrazoAuxDate, "99/99/99"));
            }
            else
            {
               AV40DDO_LogResponsavel_PrazoAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_logresponsavel_prazoauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DDO_LogResponsavel_PrazoAuxDate", context.localUtil.Format(AV40DDO_LogResponsavel_PrazoAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_logresponsavel_prazoauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Log Responsavel_Prazo Aux Date To"}), 1, "vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO");
               GX_FocusControl = edtavDdo_logresponsavel_prazoauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41DDO_LogResponsavel_PrazoAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DDO_LogResponsavel_PrazoAuxDateTo", context.localUtil.Format(AV41DDO_LogResponsavel_PrazoAuxDateTo, "99/99/99"));
            }
            else
            {
               AV41DDO_LogResponsavel_PrazoAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_logresponsavel_prazoauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DDO_LogResponsavel_PrazoAuxDateTo", context.localUtil.Format(AV41DDO_LogResponsavel_PrazoAuxDateTo, "99/99/99"));
            }
            AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace = cgiGet( edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace", AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace);
            AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace = cgiGet( edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace", AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace);
            AV32ddo_LogResponsavel_StatusTitleControlIdToReplace = cgiGet( edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ddo_LogResponsavel_StatusTitleControlIdToReplace", AV32ddo_LogResponsavel_StatusTitleControlIdToReplace);
            AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = cgiGet( edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace", AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace);
            AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace = cgiGet( edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace", AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_9 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_9"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_logresponsavel_datahora_Caption = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Caption");
            Ddo_logresponsavel_datahora_Tooltip = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Tooltip");
            Ddo_logresponsavel_datahora_Cls = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Cls");
            Ddo_logresponsavel_datahora_Filteredtext_set = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Filteredtext_set");
            Ddo_logresponsavel_datahora_Filteredtextto_set = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Filteredtextto_set");
            Ddo_logresponsavel_datahora_Dropdownoptionstype = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Dropdownoptionstype");
            Ddo_logresponsavel_datahora_Titlecontrolidtoreplace = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Titlecontrolidtoreplace");
            Ddo_logresponsavel_datahora_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Includesortasc"));
            Ddo_logresponsavel_datahora_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Includesortdsc"));
            Ddo_logresponsavel_datahora_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Includefilter"));
            Ddo_logresponsavel_datahora_Filtertype = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Filtertype");
            Ddo_logresponsavel_datahora_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Filterisrange"));
            Ddo_logresponsavel_datahora_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Includedatalist"));
            Ddo_logresponsavel_datahora_Cleanfilter = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Cleanfilter");
            Ddo_logresponsavel_datahora_Rangefilterfrom = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Rangefilterfrom");
            Ddo_logresponsavel_datahora_Rangefilterto = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Rangefilterto");
            Ddo_logresponsavel_datahora_Searchbuttontext = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Searchbuttontext");
            Ddo_logresponsavel_acao_Caption = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Caption");
            Ddo_logresponsavel_acao_Tooltip = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Tooltip");
            Ddo_logresponsavel_acao_Cls = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Cls");
            Ddo_logresponsavel_acao_Selectedvalue_set = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Selectedvalue_set");
            Ddo_logresponsavel_acao_Dropdownoptionstype = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Dropdownoptionstype");
            Ddo_logresponsavel_acao_Titlecontrolidtoreplace = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Titlecontrolidtoreplace");
            Ddo_logresponsavel_acao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Includesortasc"));
            Ddo_logresponsavel_acao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Includesortdsc"));
            Ddo_logresponsavel_acao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Includefilter"));
            Ddo_logresponsavel_acao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Includedatalist"));
            Ddo_logresponsavel_acao_Datalisttype = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Datalisttype");
            Ddo_logresponsavel_acao_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Allowmultipleselection"));
            Ddo_logresponsavel_acao_Datalistfixedvalues = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Datalistfixedvalues");
            Ddo_logresponsavel_acao_Cleanfilter = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Cleanfilter");
            Ddo_logresponsavel_acao_Searchbuttontext = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Searchbuttontext");
            Ddo_logresponsavel_status_Caption = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Caption");
            Ddo_logresponsavel_status_Tooltip = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Tooltip");
            Ddo_logresponsavel_status_Cls = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Cls");
            Ddo_logresponsavel_status_Selectedvalue_set = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Selectedvalue_set");
            Ddo_logresponsavel_status_Dropdownoptionstype = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Dropdownoptionstype");
            Ddo_logresponsavel_status_Titlecontrolidtoreplace = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Titlecontrolidtoreplace");
            Ddo_logresponsavel_status_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Includesortasc"));
            Ddo_logresponsavel_status_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Includesortdsc"));
            Ddo_logresponsavel_status_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Includefilter"));
            Ddo_logresponsavel_status_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Includedatalist"));
            Ddo_logresponsavel_status_Datalisttype = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Datalisttype");
            Ddo_logresponsavel_status_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Allowmultipleselection"));
            Ddo_logresponsavel_status_Datalistfixedvalues = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Datalistfixedvalues");
            Ddo_logresponsavel_status_Cleanfilter = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Cleanfilter");
            Ddo_logresponsavel_status_Searchbuttontext = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Searchbuttontext");
            Ddo_logresponsavel_novostatus_Caption = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Caption");
            Ddo_logresponsavel_novostatus_Tooltip = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Tooltip");
            Ddo_logresponsavel_novostatus_Cls = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Cls");
            Ddo_logresponsavel_novostatus_Selectedvalue_set = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Selectedvalue_set");
            Ddo_logresponsavel_novostatus_Dropdownoptionstype = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Dropdownoptionstype");
            Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Titlecontrolidtoreplace");
            Ddo_logresponsavel_novostatus_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Includesortasc"));
            Ddo_logresponsavel_novostatus_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Includesortdsc"));
            Ddo_logresponsavel_novostatus_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Includefilter"));
            Ddo_logresponsavel_novostatus_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Includedatalist"));
            Ddo_logresponsavel_novostatus_Datalisttype = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Datalisttype");
            Ddo_logresponsavel_novostatus_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Allowmultipleselection"));
            Ddo_logresponsavel_novostatus_Datalistfixedvalues = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Datalistfixedvalues");
            Ddo_logresponsavel_novostatus_Cleanfilter = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Cleanfilter");
            Ddo_logresponsavel_novostatus_Searchbuttontext = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Searchbuttontext");
            Ddo_logresponsavel_prazo_Caption = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Caption");
            Ddo_logresponsavel_prazo_Tooltip = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Tooltip");
            Ddo_logresponsavel_prazo_Cls = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Cls");
            Ddo_logresponsavel_prazo_Filteredtext_set = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Filteredtext_set");
            Ddo_logresponsavel_prazo_Filteredtextto_set = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Filteredtextto_set");
            Ddo_logresponsavel_prazo_Dropdownoptionstype = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Dropdownoptionstype");
            Ddo_logresponsavel_prazo_Titlecontrolidtoreplace = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Titlecontrolidtoreplace");
            Ddo_logresponsavel_prazo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Includesortasc"));
            Ddo_logresponsavel_prazo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Includesortdsc"));
            Ddo_logresponsavel_prazo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Includefilter"));
            Ddo_logresponsavel_prazo_Filtertype = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Filtertype");
            Ddo_logresponsavel_prazo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Filterisrange"));
            Ddo_logresponsavel_prazo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Includedatalist"));
            Ddo_logresponsavel_prazo_Cleanfilter = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Cleanfilter");
            Ddo_logresponsavel_prazo_Rangefilterfrom = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Rangefilterfrom");
            Ddo_logresponsavel_prazo_Rangefilterto = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Rangefilterto");
            Ddo_logresponsavel_prazo_Searchbuttontext = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Searchbuttontext");
            Ddo_logresponsavel_datahora_Activeeventkey = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Activeeventkey");
            Ddo_logresponsavel_datahora_Filteredtext_get = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Filteredtext_get");
            Ddo_logresponsavel_datahora_Filteredtextto_get = cgiGet( "DDO_LOGRESPONSAVEL_DATAHORA_Filteredtextto_get");
            Ddo_logresponsavel_acao_Activeeventkey = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Activeeventkey");
            Ddo_logresponsavel_acao_Selectedvalue_get = cgiGet( "DDO_LOGRESPONSAVEL_ACAO_Selectedvalue_get");
            Ddo_logresponsavel_status_Activeeventkey = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Activeeventkey");
            Ddo_logresponsavel_status_Selectedvalue_get = cgiGet( "DDO_LOGRESPONSAVEL_STATUS_Selectedvalue_get");
            Ddo_logresponsavel_novostatus_Activeeventkey = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Activeeventkey");
            Ddo_logresponsavel_novostatus_Selectedvalue_get = cgiGet( "DDO_LOGRESPONSAVEL_NOVOSTATUS_Selectedvalue_get");
            Ddo_logresponsavel_prazo_Activeeventkey = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Activeeventkey");
            Ddo_logresponsavel_prazo_Filteredtext_get = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Filteredtext_get");
            Ddo_logresponsavel_prazo_Filteredtextto_get = cgiGet( "DDO_LOGRESPONSAVEL_PRAZO_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E19R62 */
         E19R62 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19R62( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTflogresponsavel_datahora_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflogresponsavel_datahora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflogresponsavel_datahora_Visible), 5, 0)));
         edtavTflogresponsavel_datahora_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflogresponsavel_datahora_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflogresponsavel_datahora_to_Visible), 5, 0)));
         edtavTflogresponsavel_prazo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflogresponsavel_prazo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflogresponsavel_prazo_Visible), 5, 0)));
         edtavTflogresponsavel_prazo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflogresponsavel_prazo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflogresponsavel_prazo_to_Visible), 5, 0)));
         Ddo_logresponsavel_datahora_Titlecontrolidtoreplace = subGrid_Internalname+"_LogResponsavel_DataHora";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_datahora_Internalname, "TitleControlIdToReplace", Ddo_logresponsavel_datahora_Titlecontrolidtoreplace);
         AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace = Ddo_logresponsavel_datahora_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace", AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace);
         edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_logresponsavel_acao_Titlecontrolidtoreplace = subGrid_Internalname+"_LogResponsavel_Acao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_acao_Internalname, "TitleControlIdToReplace", Ddo_logresponsavel_acao_Titlecontrolidtoreplace);
         AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace = Ddo_logresponsavel_acao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace", AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace);
         edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_logresponsavel_status_Titlecontrolidtoreplace = subGrid_Internalname+"_LogResponsavel_Status";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_status_Internalname, "TitleControlIdToReplace", Ddo_logresponsavel_status_Titlecontrolidtoreplace);
         AV32ddo_LogResponsavel_StatusTitleControlIdToReplace = Ddo_logresponsavel_status_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32ddo_LogResponsavel_StatusTitleControlIdToReplace", AV32ddo_LogResponsavel_StatusTitleControlIdToReplace);
         edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace = subGrid_Internalname+"_LogResponsavel_NovoStatus";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_novostatus_Internalname, "TitleControlIdToReplace", Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace);
         AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace", AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace);
         edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_logresponsavel_prazo_Titlecontrolidtoreplace = subGrid_Internalname+"_LogResponsavel_Prazo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_prazo_Internalname, "TitleControlIdToReplace", Ddo_logresponsavel_prazo_Titlecontrolidtoreplace);
         AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace = Ddo_logresponsavel_prazo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace", AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace);
         edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Visible), 5, 0)));
         edtLogResponsavel_DemandaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_DemandaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLogResponsavel_DemandaCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = AV47DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2) ;
         AV47DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2;
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void E20R62( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV19LogResponsavel_DataHoraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV25LogResponsavel_AcaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV29LogResponsavel_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV33LogResponsavel_NovoStatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37LogResponsavel_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV18WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtLogResponsavel_DataHora_Titleformat = 2;
         edtLogResponsavel_DataHora_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_DataHora_Internalname, "Title", edtLogResponsavel_DataHora_Title);
         cmbLogResponsavel_Acao_Titleformat = 2;
         cmbLogResponsavel_Acao.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "A��o", AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_Acao_Internalname, "Title", cmbLogResponsavel_Acao.Title.Text);
         cmbLogResponsavel_Status_Titleformat = 2;
         cmbLogResponsavel_Status.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status anterior", AV32ddo_LogResponsavel_StatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_Status_Internalname, "Title", cmbLogResponsavel_Status.Title.Text);
         cmbLogResponsavel_NovoStatus_Titleformat = 2;
         cmbLogResponsavel_NovoStatus.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Novo status", AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_NovoStatus_Internalname, "Title", cmbLogResponsavel_NovoStatus.Title.Text);
         edtLogResponsavel_Prazo_Titleformat = 2;
         edtLogResponsavel_Prazo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Prazo", AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLogResponsavel_Prazo_Internalname, "Title", edtLogResponsavel_Prazo_Title);
         AV68Codigos.FromXml(AV69WebSession.Get("Codigos"), "Collection");
         AV69WebSession.Remove("Codigos");
         AV50String = "BK S ";
         AV67Selecionadas = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Selecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Selecionadas), 4, 0)));
         AV70Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70Codigo), 6, 0)));
         bttCorregir_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCorregir_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttCorregir_Visible), 5, 0)));
         /* Execute user subroutine: 'PRECARREGARCOMBOSDEUSUARIOS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV19LogResponsavel_DataHoraTitleFilterData", AV19LogResponsavel_DataHoraTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25LogResponsavel_AcaoTitleFilterData", AV25LogResponsavel_AcaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV29LogResponsavel_StatusTitleFilterData", AV29LogResponsavel_StatusTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV33LogResponsavel_NovoStatusTitleFilterData", AV33LogResponsavel_NovoStatusTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37LogResponsavel_PrazoTitleFilterData", AV37LogResponsavel_PrazoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV18WWPContext", AV18WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68Codigos", AV68Codigos);
         cmbavEmissor.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEmissor_Internalname, "Values", cmbavEmissor.ToJavascriptSource());
         cmbavDestino.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5Destino), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDestino_Internalname, "Values", cmbavDestino.ToJavascriptSource());
      }

      protected void E11R62( )
      {
         /* Ddo_logresponsavel_datahora_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_logresponsavel_datahora_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV20TFLogResponsavel_DataHora = context.localUtil.CToT( Ddo_logresponsavel_datahora_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFLogResponsavel_DataHora", context.localUtil.TToC( AV20TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
            AV21TFLogResponsavel_DataHora_To = context.localUtil.CToT( Ddo_logresponsavel_datahora_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV21TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV21TFLogResponsavel_DataHora_To) )
            {
               AV21TFLogResponsavel_DataHora_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV21TFLogResponsavel_DataHora_To)), (short)(DateTimeUtil.Month( AV21TFLogResponsavel_DataHora_To)), (short)(DateTimeUtil.Day( AV21TFLogResponsavel_DataHora_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV21TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      protected void E12R62( )
      {
         /* Ddo_logresponsavel_acao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_logresponsavel_acao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV26TFLogResponsavel_Acao_SelsJson = Ddo_logresponsavel_acao_Selectedvalue_get;
            AV27TFLogResponsavel_Acao_Sels.FromJSonString(AV26TFLogResponsavel_Acao_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV27TFLogResponsavel_Acao_Sels", AV27TFLogResponsavel_Acao_Sels);
      }

      protected void E13R62( )
      {
         /* Ddo_logresponsavel_status_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_logresponsavel_status_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV30TFLogResponsavel_Status_SelsJson = Ddo_logresponsavel_status_Selectedvalue_get;
            AV31TFLogResponsavel_Status_Sels.FromJSonString(AV30TFLogResponsavel_Status_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV31TFLogResponsavel_Status_Sels", AV31TFLogResponsavel_Status_Sels);
      }

      protected void E14R62( )
      {
         /* Ddo_logresponsavel_novostatus_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_logresponsavel_novostatus_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV34TFLogResponsavel_NovoStatus_SelsJson = Ddo_logresponsavel_novostatus_Selectedvalue_get;
            AV35TFLogResponsavel_NovoStatus_Sels.FromJSonString(AV34TFLogResponsavel_NovoStatus_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV35TFLogResponsavel_NovoStatus_Sels", AV35TFLogResponsavel_NovoStatus_Sels);
      }

      protected void E15R62( )
      {
         /* Ddo_logresponsavel_prazo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_logresponsavel_prazo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV38TFLogResponsavel_Prazo = context.localUtil.CToT( Ddo_logresponsavel_prazo_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLogResponsavel_Prazo", context.localUtil.TToC( AV38TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
            AV39TFLogResponsavel_Prazo_To = context.localUtil.CToT( Ddo_logresponsavel_prazo_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV39TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV39TFLogResponsavel_Prazo_To) )
            {
               AV39TFLogResponsavel_Prazo_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV39TFLogResponsavel_Prazo_To)), (short)(DateTimeUtil.Month( AV39TFLogResponsavel_Prazo_To)), (short)(DateTimeUtil.Day( AV39TFLogResponsavel_Prazo_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV39TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
      }

      private void E21R62( )
      {
         /* Grid_Load Routine */
         AV6Emissor = A896LogResponsavel_Owner;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0)));
         AV5Destino = A891LogResponsavel_UsuarioCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Destino), 6, 0)));
         AV48OS = A493ContagemResultado_DemandaFM;
         if ( AV18WWPContext.gxTpr_Userehcontratada )
         {
            /* Execute user subroutine: 'CARREGAUSUARIOS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtLogResponsavel_DemandaCod_Visible = 0;
         if ( AV70Codigo != A892LogResponsavel_DemandaCod )
         {
            AV70Codigo = A892LogResponsavel_DemandaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV70Codigo), 6, 0)));
            AV52Selected = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV52Selected);
            AV51PrazoSugerido = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            AV62NovaData = DateTime.MinValue;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62NovaData", context.localUtil.Format(AV62NovaData, "99/99/99"));
            AV61UltimoPrazo = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61UltimoPrazo", context.localUtil.TToC( AV61UltimoPrazo, 8, 5, 0, 3, "/", ":", " "));
            AV58ContratoServicos_Codigo = A1553ContagemResultado_CntSrvCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58ContratoServicos_Codigo), 6, 0)));
            AV63TipoDias = A1611ContagemResultado_PrzTpDias;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TipoDias", AV63TipoDias);
            AV60Unidades = A798ContagemResultado_PFBFSImp;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Unidades", StringUtil.LTrim( StringUtil.Str( AV60Unidades, 14, 5)));
         }
         AV71PrazoInicial = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PrazoInicial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71PrazoInicial), 4, 0)));
         if ( ( StringUtil.StrCmp(A894LogResponsavel_Acao, "S") == 0 ) || ( StringUtil.StrCmp(A894LogResponsavel_Acao, "I") == 0 ) )
         {
            edtLogResponsavel_DemandaCod_Visible = 1;
            AV51PrazoSugerido = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            AV71PrazoInicial = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PrazoInicial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71PrazoInicial), 4, 0)));
            /* Execute user subroutine: 'PRAZOENTREGA' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( ( ( StringUtil.StrCmp(A894LogResponsavel_Acao, "A") == 0 ) || ( StringUtil.StrCmp(A894LogResponsavel_Acao, "C") == 0 ) ) && ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "E") == 0 ) )
         {
            AV51PrazoSugerido = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'PRAZOANALISE' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( ( ( StringUtil.StrCmp(A894LogResponsavel_Acao, "A") == 0 ) || ( StringUtil.StrCmp(A894LogResponsavel_Acao, "C") == 0 ) ) && ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "A") == 0 ) )
         {
            AV51PrazoSugerido = AV61UltimoPrazo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( StringUtil.StrCmp(A894LogResponsavel_Acao, "EA") == 0 )
         {
            if ( AV62NovaData > A471ContagemResultado_DataDmn )
            {
               AV51PrazoSugerido = DateTimeUtil.ResetTime( AV62NovaData ) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV51PrazoSugerido = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            }
            /* Execute user subroutine: 'PRAZOANALISE' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV61UltimoPrazo = AV51PrazoSugerido;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61UltimoPrazo", context.localUtil.TToC( AV61UltimoPrazo, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( ( StringUtil.StrCmp(A894LogResponsavel_Acao, "R") == 0 ) && A1149LogResponsavel_OwnerEhContratante )
         {
            AV62NovaData = DateTimeUtil.ResetTime(A893LogResponsavel_DataHora);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62NovaData", context.localUtil.Format(AV62NovaData, "99/99/99"));
            AV51PrazoSugerido = A893LogResponsavel_DataHora;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            /* Execute user subroutine: 'PRAZOCORRECAO' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV61UltimoPrazo = AV51PrazoSugerido;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61UltimoPrazo", context.localUtil.TToC( AV61UltimoPrazo, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( ( StringUtil.StrCmp(A894LogResponsavel_Acao, "E") == 0 ) && ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "E") == 0 ) )
         {
            AV51PrazoSugerido = A893LogResponsavel_DataHora;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            AV62NovaData = DateTimeUtil.ResetTime(A893LogResponsavel_DataHora);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62NovaData", context.localUtil.Format(AV62NovaData, "99/99/99"));
            /* Execute user subroutine: 'PRAZOANALISE' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV61UltimoPrazo = AV51PrazoSugerido;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61UltimoPrazo", context.localUtil.TToC( AV61UltimoPrazo, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( ( StringUtil.StrCmp(A894LogResponsavel_Acao, "E") == 0 ) && ( StringUtil.StrCmp(A1234LogResponsavel_NovoStatus, "A") == 0 ) )
         {
            AV51PrazoSugerido = A893LogResponsavel_DataHora;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            AV62NovaData = DateTimeUtil.ResetTime(A893LogResponsavel_DataHora);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62NovaData", context.localUtil.Format(AV62NovaData, "99/99/99"));
            AV71PrazoInicial = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PrazoInicial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71PrazoInicial), 4, 0)));
            /* Execute user subroutine: 'PRAZOENTREGA' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV61UltimoPrazo = AV51PrazoSugerido;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61UltimoPrazo", context.localUtil.TToC( AV61UltimoPrazo, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( StringUtil.StrCmp(A894LogResponsavel_Acao, "F") == 0 )
         {
            if ( AV62NovaData > A471ContagemResultado_DataDmn )
            {
               AV51PrazoSugerido = DateTimeUtil.ResetTime( AV62NovaData ) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV51PrazoSugerido = DateTimeUtil.ResetTime( A471ContagemResultado_DataDmn ) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            }
            AV71PrazoInicial = (short)(A1227ContagemResultado_PrazoInicialDias+A1237ContagemResultado_PrazoMaisDias);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71PrazoInicial", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71PrazoInicial), 4, 0)));
            /* Execute user subroutine: 'PRAZOENTREGA' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(A894LogResponsavel_Acao, "V") == 0 )
         {
            AV51PrazoSugerido = AV61UltimoPrazo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( ( StringUtil.StrCmp(A894LogResponsavel_Acao, "T") == 0 ) && ( A40000ContratadaUsuario_UsuarioCod > 0 ) )
         {
            AV51PrazoSugerido = AV61UltimoPrazo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
         }
         else if ( ( StringUtil.StrCmp(A894LogResponsavel_Acao, "N") == 0 ) && A1149LogResponsavel_OwnerEhContratante )
         {
            AV51PrazoSugerido = A893LogResponsavel_DataHora;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            AV62NovaData = DateTimeUtil.ResetTime(A893LogResponsavel_DataHora);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62NovaData", context.localUtil.Format(AV62NovaData, "99/99/99"));
            /* Execute user subroutine: 'PRAZOCORRECAO' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( ( StringUtil.StrCmp(A894LogResponsavel_Acao, "H") == 0 ) || ( StringUtil.StrCmp(A894LogResponsavel_Acao, "P") == 0 ) || ( StringUtil.StrCmp(A894LogResponsavel_Acao, "Q") == 0 ) )
         {
            AV51PrazoSugerido = (DateTime)(DateTime.MinValue);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( ! (DateTime.MinValue==AV51PrazoSugerido) )
         {
            /* Execute user subroutine: 'ADDHORAS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( A1177LogResponsavel_Prazo != AV51PrazoSugerido )
            {
               AV52Selected = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV52Selected);
               AV67Selecionadas = (short)(AV67Selecionadas+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Selecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Selecionadas), 4, 0)));
               bttCorregir_Caption = "Corregir "+StringUtil.Trim( StringUtil.Str( (decimal)(AV67Selecionadas), 4, 0))+" prazo(s)";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCorregir_Internalname, "Caption", bttCorregir_Caption);
               bttCorregir_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCorregir_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttCorregir_Visible), 5, 0)));
            }
            else
            {
               AV52Selected = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV52Selected);
            }
         }
         if ( ! AV52Selected && ( A1177LogResponsavel_Prazo > AV61UltimoPrazo ) )
         {
            AV61UltimoPrazo = A1177LogResponsavel_Prazo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61UltimoPrazo", context.localUtil.TToC( AV61UltimoPrazo, 8, 5, 0, 3, "/", ":", " "));
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 9;
         }
         sendrow_92( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_9_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(9, GridRow);
         }
         cmbavEmissor.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0));
         cmbavDestino.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5Destino), 6, 0));
      }

      protected void E22R62( )
      {
         /* Selected_Click Routine */
         if ( AV52Selected )
         {
            AV67Selecionadas = (short)(AV67Selecionadas+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Selecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Selecionadas), 4, 0)));
         }
         else
         {
            AV67Selecionadas = (short)(AV67Selecionadas-1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67Selecionadas", StringUtil.LTrim( StringUtil.Str( (decimal)(AV67Selecionadas), 4, 0)));
         }
         bttCorregir_Caption = "Corregir "+StringUtil.Trim( StringUtil.Str( (decimal)(AV67Selecionadas), 4, 0))+" prazo(s)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCorregir_Internalname, "Caption", bttCorregir_Caption);
         bttCorregir_Visible = (((AV67Selecionadas>0)) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttCorregir_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttCorregir_Visible), 5, 0)));
      }

      protected void E18R62( )
      {
         /* Naoconsiderarprazoinicial_Click Routine */
         gxgrGrid_refresh( subGrid_Rows, AV11LogResponsavel_DemandaCod, AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV32ddo_LogResponsavel_StatusTitleControlIdToReplace, AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV75Pgmname, AV20TFLogResponsavel_DataHora, AV21TFLogResponsavel_DataHora_To, AV27TFLogResponsavel_Acao_Sels, AV31TFLogResponsavel_Status_Sels, AV35TFLogResponsavel_NovoStatus_Sels, AV38TFLogResponsavel_Prazo, AV39TFLogResponsavel_Prazo_To, A54Usuario_Ativo, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV18WWPContext, AV70Codigo, A892LogResponsavel_DemandaCod, A1553ContagemResultado_CntSrvCod, A1611ContagemResultado_PrzTpDias, A798ContagemResultado_PFBFSImp, A1234LogResponsavel_NovoStatus, A1237ContagemResultado_PrazoMaisDias, A1227ContagemResultado_PrazoInicialDias, A471ContagemResultado_DataDmn, AV62NovaData, AV61UltimoPrazo, A893LogResponsavel_DataHora, A1149LogResponsavel_OwnerEhContratante, A894LogResponsavel_Acao, AV51PrazoSugerido, A1177LogResponsavel_Prazo, AV67Selecionadas, AV52Selected, A1228ContratadaUsuario_AreaTrabalhoCod, AV5Destino, A69ContratadaUsuario_UsuarioCod, AV6Emissor, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, AV71PrazoInicial, AV72NaoConsiderarPrazoInicial, AV58ContratoServicos_Codigo, AV60Unidades, AV59Complexidade, AV63TipoDias) ;
      }

      protected void S162( )
      {
         /* 'PRAZOENTREGA' Routine */
         if ( (0==AV71PrazoInicial) || AV72NaoConsiderarPrazoInicial )
         {
            GXt_int3 = AV54Dias;
            new prc_diasparaentrega(context ).execute( ref  AV58ContratoServicos_Codigo,  AV60Unidades,  AV59Complexidade, out  GXt_int3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58ContratoServicos_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Unidades", StringUtil.LTrim( StringUtil.Str( AV60Unidades, 14, 5)));
            AV54Dias = GXt_int3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Dias), 4, 0)));
         }
         else
         {
            AV54Dias = AV71PrazoInicial;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Dias), 4, 0)));
         }
         GXt_dtime4 = AV51PrazoSugerido;
         new prc_adddiasuteis(context ).execute(  AV51PrazoSugerido,  AV54Dias,  A1611ContagemResultado_PrzTpDias, out  GXt_dtime4) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Dias), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1611ContagemResultado_PrzTpDias", A1611ContagemResultado_PrzTpDias);
         AV51PrazoSugerido = GXt_dtime4;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void S172( )
      {
         /* 'PRAZOANALISE' Routine */
         GXt_int3 = AV54Dias;
         new prc_diasparaanalise(context ).execute( ref  AV58ContratoServicos_Codigo,  AV60Unidades,  0, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58ContratoServicos_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60Unidades", StringUtil.LTrim( StringUtil.Str( AV60Unidades, 14, 5)));
         AV54Dias = (short)(Convert.ToInt16((GXt_int3>0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Dias), 4, 0)));
         if ( AV54Dias > 0 )
         {
            GXt_dtime4 = AV51PrazoSugerido;
            new prc_adddiasuteis(context ).execute(  AV51PrazoSugerido,  AV54Dias,  AV63TipoDias, out  GXt_dtime4) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Dias), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TipoDias", AV63TipoDias);
            AV51PrazoSugerido = GXt_dtime4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
         }
         else
         {
            /* Execute user subroutine: 'PRAZOENTREGA' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S182( )
      {
         /* 'PRAZOCORRECAO' Routine */
         GXt_int3 = AV54Dias;
         new prc_diasparacorrecao(context ).execute( ref  AV58ContratoServicos_Codigo,  A892LogResponsavel_DemandaCod, out  GXt_int3) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58ContratoServicos_Codigo), 6, 0)));
         AV54Dias = GXt_int3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Dias), 4, 0)));
         GXt_dtime4 = AV51PrazoSugerido;
         new prc_adddiasuteis(context ).execute(  AV51PrazoSugerido,  AV54Dias,  AV63TipoDias, out  GXt_dtime4) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Dias), 4, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TipoDias", AV63TipoDias);
         AV51PrazoSugerido = GXt_dtime4;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV75Pgmname+"GridState"), "") == 0 )
         {
            AV7GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV75Pgmname+"GridState"), "");
         }
         else
         {
            AV7GridState.FromXml(AV15Session.Get(AV75Pgmname+"GridState"), "");
         }
         AV76GXV1 = 1;
         while ( AV76GXV1 <= AV7GridState.gxTpr_Filtervalues.Count )
         {
            AV8GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV7GridState.gxTpr_Filtervalues.Item(AV76GXV1));
            if ( StringUtil.StrCmp(AV8GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_DATAHORA") == 0 )
            {
               AV20TFLogResponsavel_DataHora = context.localUtil.CToT( AV8GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20TFLogResponsavel_DataHora", context.localUtil.TToC( AV20TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " "));
               AV21TFLogResponsavel_DataHora_To = context.localUtil.CToT( AV8GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFLogResponsavel_DataHora_To", context.localUtil.TToC( AV21TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV20TFLogResponsavel_DataHora) )
               {
                  AV22DDO_LogResponsavel_DataHoraAuxDate = DateTimeUtil.ResetTime(AV20TFLogResponsavel_DataHora);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DDO_LogResponsavel_DataHoraAuxDate", context.localUtil.Format(AV22DDO_LogResponsavel_DataHoraAuxDate, "99/99/99"));
                  Ddo_logresponsavel_datahora_Filteredtext_set = context.localUtil.DToC( AV22DDO_LogResponsavel_DataHoraAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_datahora_Internalname, "FilteredText_set", Ddo_logresponsavel_datahora_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV21TFLogResponsavel_DataHora_To) )
               {
                  AV23DDO_LogResponsavel_DataHoraAuxDateTo = DateTimeUtil.ResetTime(AV21TFLogResponsavel_DataHora_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DDO_LogResponsavel_DataHoraAuxDateTo", context.localUtil.Format(AV23DDO_LogResponsavel_DataHoraAuxDateTo, "99/99/99"));
                  Ddo_logresponsavel_datahora_Filteredtextto_set = context.localUtil.DToC( AV23DDO_LogResponsavel_DataHoraAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_datahora_Internalname, "FilteredTextTo_set", Ddo_logresponsavel_datahora_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV8GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_ACAO_SEL") == 0 )
            {
               AV26TFLogResponsavel_Acao_SelsJson = AV8GridStateFilterValue.gxTpr_Value;
               AV27TFLogResponsavel_Acao_Sels.FromJSonString(AV26TFLogResponsavel_Acao_SelsJson);
               if ( ! ( AV27TFLogResponsavel_Acao_Sels.Count == 0 ) )
               {
                  Ddo_logresponsavel_acao_Selectedvalue_set = AV26TFLogResponsavel_Acao_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_acao_Internalname, "SelectedValue_set", Ddo_logresponsavel_acao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV8GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_STATUS_SEL") == 0 )
            {
               AV30TFLogResponsavel_Status_SelsJson = AV8GridStateFilterValue.gxTpr_Value;
               AV31TFLogResponsavel_Status_Sels.FromJSonString(AV30TFLogResponsavel_Status_SelsJson);
               if ( ! ( AV31TFLogResponsavel_Status_Sels.Count == 0 ) )
               {
                  Ddo_logresponsavel_status_Selectedvalue_set = AV30TFLogResponsavel_Status_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_status_Internalname, "SelectedValue_set", Ddo_logresponsavel_status_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV8GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_NOVOSTATUS_SEL") == 0 )
            {
               AV34TFLogResponsavel_NovoStatus_SelsJson = AV8GridStateFilterValue.gxTpr_Value;
               AV35TFLogResponsavel_NovoStatus_Sels.FromJSonString(AV34TFLogResponsavel_NovoStatus_SelsJson);
               if ( ! ( AV35TFLogResponsavel_NovoStatus_Sels.Count == 0 ) )
               {
                  Ddo_logresponsavel_novostatus_Selectedvalue_set = AV34TFLogResponsavel_NovoStatus_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_novostatus_Internalname, "SelectedValue_set", Ddo_logresponsavel_novostatus_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV8GridStateFilterValue.gxTpr_Name, "TFLOGRESPONSAVEL_PRAZO") == 0 )
            {
               AV38TFLogResponsavel_Prazo = context.localUtil.CToT( AV8GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38TFLogResponsavel_Prazo", context.localUtil.TToC( AV38TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " "));
               AV39TFLogResponsavel_Prazo_To = context.localUtil.CToT( AV8GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLogResponsavel_Prazo_To", context.localUtil.TToC( AV39TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " "));
               if ( ! (DateTime.MinValue==AV38TFLogResponsavel_Prazo) )
               {
                  AV40DDO_LogResponsavel_PrazoAuxDate = DateTimeUtil.ResetTime(AV38TFLogResponsavel_Prazo);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40DDO_LogResponsavel_PrazoAuxDate", context.localUtil.Format(AV40DDO_LogResponsavel_PrazoAuxDate, "99/99/99"));
                  Ddo_logresponsavel_prazo_Filteredtext_set = context.localUtil.DToC( AV40DDO_LogResponsavel_PrazoAuxDate, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_prazo_Internalname, "FilteredText_set", Ddo_logresponsavel_prazo_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV39TFLogResponsavel_Prazo_To) )
               {
                  AV41DDO_LogResponsavel_PrazoAuxDateTo = DateTimeUtil.ResetTime(AV39TFLogResponsavel_Prazo_To);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DDO_LogResponsavel_PrazoAuxDateTo", context.localUtil.Format(AV41DDO_LogResponsavel_PrazoAuxDateTo, "99/99/99"));
                  Ddo_logresponsavel_prazo_Filteredtextto_set = context.localUtil.DToC( AV41DDO_LogResponsavel_PrazoAuxDateTo, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_logresponsavel_prazo_Internalname, "FilteredTextTo_set", Ddo_logresponsavel_prazo_Filteredtextto_set);
               }
            }
            AV76GXV1 = (int)(AV76GXV1+1);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV7GridState.FromXml(AV15Session.Get(AV75Pgmname+"GridState"), "");
         AV7GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (DateTime.MinValue==AV20TFLogResponsavel_DataHora) && (DateTime.MinValue==AV21TFLogResponsavel_DataHora_To) ) )
         {
            AV8GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV8GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_DATAHORA";
            AV8GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV20TFLogResponsavel_DataHora, 8, 5, 0, 3, "/", ":", " ");
            AV8GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV21TFLogResponsavel_DataHora_To, 8, 5, 0, 3, "/", ":", " ");
            AV7GridState.gxTpr_Filtervalues.Add(AV8GridStateFilterValue, 0);
         }
         if ( ! ( AV27TFLogResponsavel_Acao_Sels.Count == 0 ) )
         {
            AV8GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV8GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_ACAO_SEL";
            AV8GridStateFilterValue.gxTpr_Value = AV27TFLogResponsavel_Acao_Sels.ToJSonString(false);
            AV7GridState.gxTpr_Filtervalues.Add(AV8GridStateFilterValue, 0);
         }
         if ( ! ( AV31TFLogResponsavel_Status_Sels.Count == 0 ) )
         {
            AV8GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV8GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_STATUS_SEL";
            AV8GridStateFilterValue.gxTpr_Value = AV31TFLogResponsavel_Status_Sels.ToJSonString(false);
            AV7GridState.gxTpr_Filtervalues.Add(AV8GridStateFilterValue, 0);
         }
         if ( ! ( AV35TFLogResponsavel_NovoStatus_Sels.Count == 0 ) )
         {
            AV8GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV8GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_NOVOSTATUS_SEL";
            AV8GridStateFilterValue.gxTpr_Value = AV35TFLogResponsavel_NovoStatus_Sels.ToJSonString(false);
            AV7GridState.gxTpr_Filtervalues.Add(AV8GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV38TFLogResponsavel_Prazo) && (DateTime.MinValue==AV39TFLogResponsavel_Prazo_To) ) )
         {
            AV8GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV8GridStateFilterValue.gxTpr_Name = "TFLOGRESPONSAVEL_PRAZO";
            AV8GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV38TFLogResponsavel_Prazo, 8, 5, 0, 3, "/", ":", " ");
            AV8GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV39TFLogResponsavel_Prazo_To, 8, 5, 0, 3, "/", ":", " ");
            AV7GridState.gxTpr_Filtervalues.Add(AV8GridStateFilterValue, 0);
         }
         if ( ! (0==AV11LogResponsavel_DemandaCod) )
         {
            AV8GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV8GridStateFilterValue.gxTpr_Name = "PARM_&LOGRESPONSAVEL_DEMANDACOD";
            AV8GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV11LogResponsavel_DemandaCod), 6, 0);
            AV7GridState.gxTpr_Filtervalues.Add(AV8GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV75Pgmname+"GridState",  AV7GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV16TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV16TrnContext.gxTpr_Callerobject = AV75Pgmname;
         AV16TrnContext.gxTpr_Callerondelete = true;
         AV16TrnContext.gxTpr_Callerurl = AV9HTTPRequest.ScriptName+"?"+AV9HTTPRequest.QueryString;
         AV16TrnContext.gxTpr_Transactionname = "LogResponsavel";
         AV17TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV17TrnContextAtt.gxTpr_Attributename = "LogResponsavel_DemandaCod";
         AV17TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV11LogResponsavel_DemandaCod), 6, 0);
         AV16TrnContext.gxTpr_Attributes.Add(AV17TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV16TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void S152( )
      {
         /* 'CARREGAUSUARIOS' Routine */
         /* Using cursor H00R66 */
         pr_default.execute(2, new Object[] {AV5Destino, AV6Emissor, AV18WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A69ContratadaUsuario_UsuarioCod = H00R66_A69ContratadaUsuario_UsuarioCod[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = H00R66_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = H00R66_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A66ContratadaUsuario_ContratadaCod = H00R66_A66ContratadaUsuario_ContratadaCod[0];
            A438Contratada_Sigla = H00R66_A438Contratada_Sigla[0];
            n438Contratada_Sigla = H00R66_n438Contratada_Sigla[0];
            A1228ContratadaUsuario_AreaTrabalhoCod = H00R66_A1228ContratadaUsuario_AreaTrabalhoCod[0];
            n1228ContratadaUsuario_AreaTrabalhoCod = H00R66_n1228ContratadaUsuario_AreaTrabalhoCod[0];
            A438Contratada_Sigla = H00R66_A438Contratada_Sigla[0];
            n438Contratada_Sigla = H00R66_n438Contratada_Sigla[0];
            if ( A69ContratadaUsuario_UsuarioCod == AV6Emissor )
            {
               if ( ! ( A66ContratadaUsuario_ContratadaCod == AV18WWPContext.gxTpr_Contratada_codigo ) )
               {
                  cmbavEmissor.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod+900000), 6, 0)), A438Contratada_Sigla, 0);
                  AV6Emissor = (int)(A66ContratadaUsuario_ContratadaCod+900000);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0)));
               }
            }
            if ( A69ContratadaUsuario_UsuarioCod == AV5Destino )
            {
               if ( ! ( A66ContratadaUsuario_ContratadaCod == AV18WWPContext.gxTpr_Contratada_codigo ) )
               {
                  cmbavDestino.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A66ContratadaUsuario_ContratadaCod+900000), 6, 0)), A438Contratada_Sigla, 0);
                  AV5Destino = (int)(A66ContratadaUsuario_ContratadaCod+900000);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Destino), 6, 0)));
               }
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void S142( )
      {
         /* 'PRECARREGARCOMBOSDEUSUARIOS' Routine */
         cmbavEmissor.addItem("0", "(Nenhum)", 0);
         cmbavDestino.addItem("0", "(Nenhum)", 0);
         /* Using cursor H00R67 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            A57Usuario_PessoaCod = H00R67_A57Usuario_PessoaCod[0];
            A54Usuario_Ativo = H00R67_A54Usuario_Ativo[0];
            A58Usuario_PessoaNom = H00R67_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00R67_n58Usuario_PessoaNom[0];
            A1Usuario_Codigo = H00R67_A1Usuario_Codigo[0];
            A58Usuario_PessoaNom = H00R67_A58Usuario_PessoaNom[0];
            n58Usuario_PessoaNom = H00R67_n58Usuario_PessoaNom[0];
            cmbavEmissor.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)), A58Usuario_PessoaNom, 0);
            cmbavDestino.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1Usuario_Codigo), 6, 0)), A58Usuario_PessoaNom, 0);
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void S192( )
      {
         /* 'ADDHORAS' Routine */
         new prc_gethorasentrega(context ).execute(  AV18WWPContext.gxTpr_Areatrabalho_codigo,  0,  AV58ContratoServicos_Codigo, out  AV56Hours, out  AV57Minutes) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV58ContratoServicos_Codigo), 6, 0)));
         AV51PrazoSugerido = DateTimeUtil.ResetTime( DateTimeUtil.ResetTime( AV51PrazoSugerido) ) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
         AV51PrazoSugerido = DateTimeUtil.TAdd( AV51PrazoSugerido, 3600*(AV56Hours));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
         AV51PrazoSugerido = DateTimeUtil.TAdd( AV51PrazoSugerido, 60*(AV57Minutes));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void E16R62( )
      {
         /* 'Fechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E17R62( )
      {
         /* 'Corregir' Routine */
         /* Start For Each Line */
         nRC_GXsfl_9 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_9"), ",", "."));
         nGXsfl_9_fel_idx = 0;
         while ( nGXsfl_9_fel_idx < nRC_GXsfl_9 )
         {
            nGXsfl_9_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_9_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_9_fel_idx+1));
            sGXsfl_9_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_92( ) ;
            A892LogResponsavel_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtLogResponsavel_DemandaCod_Internalname), ",", "."));
            n892LogResponsavel_DemandaCod = false;
            A1797LogResponsavel_Codigo = (long)(context.localUtil.CToN( cgiGet( edtLogResponsavel_Codigo_Internalname), ",", "."));
            A893LogResponsavel_DataHora = context.localUtil.CToT( cgiGet( edtLogResponsavel_DataHora_Internalname), 0);
            cmbavEmissor.Name = cmbavEmissor_Internalname;
            cmbavEmissor.CurrentValue = cgiGet( cmbavEmissor_Internalname);
            AV6Emissor = (int)(NumberUtil.Val( cgiGet( cmbavEmissor_Internalname), "."));
            cmbLogResponsavel_Acao.Name = cmbLogResponsavel_Acao_Internalname;
            cmbLogResponsavel_Acao.CurrentValue = cgiGet( cmbLogResponsavel_Acao_Internalname);
            A894LogResponsavel_Acao = cgiGet( cmbLogResponsavel_Acao_Internalname);
            cmbavDestino.Name = cmbavDestino_Internalname;
            cmbavDestino.CurrentValue = cgiGet( cmbavDestino_Internalname);
            AV5Destino = (int)(NumberUtil.Val( cgiGet( cmbavDestino_Internalname), "."));
            cmbLogResponsavel_Status.Name = cmbLogResponsavel_Status_Internalname;
            cmbLogResponsavel_Status.CurrentValue = cgiGet( cmbLogResponsavel_Status_Internalname);
            A1130LogResponsavel_Status = cgiGet( cmbLogResponsavel_Status_Internalname);
            n1130LogResponsavel_Status = false;
            cmbLogResponsavel_NovoStatus.Name = cmbLogResponsavel_NovoStatus_Internalname;
            cmbLogResponsavel_NovoStatus.CurrentValue = cgiGet( cmbLogResponsavel_NovoStatus_Internalname);
            A1234LogResponsavel_NovoStatus = cgiGet( cmbLogResponsavel_NovoStatus_Internalname);
            n1234LogResponsavel_NovoStatus = false;
            A1177LogResponsavel_Prazo = context.localUtil.CToT( cgiGet( edtLogResponsavel_Prazo_Internalname), 0);
            n1177LogResponsavel_Prazo = false;
            A1227ContagemResultado_PrazoInicialDias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultado_PrazoInicialDias_Internalname), ",", "."));
            n1227ContagemResultado_PrazoInicialDias = false;
            A1237ContagemResultado_PrazoMaisDias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultado_PrazoMaisDias_Internalname), ",", "."));
            n1237ContagemResultado_PrazoMaisDias = false;
            AV52Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
            if ( context.localUtil.VCDateTime( cgiGet( edtavPrazosugerido_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Prazo Sugerido"}), 1, "vPRAZOSUGERIDO");
               GX_FocusControl = edtavPrazosugerido_Internalname;
               wbErr = true;
               AV51PrazoSugerido = (DateTime)(DateTime.MinValue);
            }
            else
            {
               AV51PrazoSugerido = context.localUtil.CToT( cgiGet( edtavPrazosugerido_Internalname), 0);
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavDias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavDias_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vDIAS");
               GX_FocusControl = edtavDias_Internalname;
               wbErr = true;
               AV54Dias = 0;
            }
            else
            {
               AV54Dias = (short)(context.localUtil.CToN( cgiGet( edtavDias_Internalname), ",", "."));
            }
            if ( AV52Selected )
            {
               if ( ( StringUtil.StrCmp(A894LogResponsavel_Acao, "S") == 0 ) || ( StringUtil.StrCmp(A894LogResponsavel_Acao, "I") == 0 ) )
               {
                  new prc_saveprazolog(context ).execute( ref  A1797LogResponsavel_Codigo,  AV51PrazoSugerido,  AV54Dias) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDias_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV54Dias), 4, 0)));
               }
               else
               {
                  new prc_saveprazolog(context ).execute( ref  A1797LogResponsavel_Codigo,  AV51PrazoSugerido,  0) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavPrazosugerido_Internalname, context.localUtil.TToC( AV51PrazoSugerido, 8, 5, 0, 3, "/", ":", " "));
               }
            }
            /* End For Each Line */
         }
         if ( nGXsfl_9_fel_idx == 0 )
         {
            nGXsfl_9_idx = 1;
            sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
            SubsflControlProps_92( ) ;
         }
         nGXsfl_9_fel_idx = 1;
         context.CommitDataStores( "WP_Corretor");
         gxgrGrid_refresh( subGrid_Rows, AV11LogResponsavel_DemandaCod, AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace, AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace, AV32ddo_LogResponsavel_StatusTitleControlIdToReplace, AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace, AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace, AV75Pgmname, AV20TFLogResponsavel_DataHora, AV21TFLogResponsavel_DataHora_To, AV27TFLogResponsavel_Acao_Sels, AV31TFLogResponsavel_Status_Sels, AV35TFLogResponsavel_NovoStatus_Sels, AV38TFLogResponsavel_Prazo, AV39TFLogResponsavel_Prazo_To, A54Usuario_Ativo, A1Usuario_Codigo, A58Usuario_PessoaNom, A896LogResponsavel_Owner, A891LogResponsavel_UsuarioCod, A493ContagemResultado_DemandaFM, AV18WWPContext, AV70Codigo, A892LogResponsavel_DemandaCod, A1553ContagemResultado_CntSrvCod, A1611ContagemResultado_PrzTpDias, A798ContagemResultado_PFBFSImp, A1234LogResponsavel_NovoStatus, A1237ContagemResultado_PrazoMaisDias, A1227ContagemResultado_PrazoInicialDias, A471ContagemResultado_DataDmn, AV62NovaData, AV61UltimoPrazo, A893LogResponsavel_DataHora, A1149LogResponsavel_OwnerEhContratante, A894LogResponsavel_Acao, AV51PrazoSugerido, A1177LogResponsavel_Prazo, AV67Selecionadas, AV52Selected, A1228ContratadaUsuario_AreaTrabalhoCod, AV5Destino, A69ContratadaUsuario_UsuarioCod, AV6Emissor, A66ContratadaUsuario_ContratadaCod, A438Contratada_Sigla, AV71PrazoInicial, AV72NaoConsiderarPrazoInicial, AV58ContratoServicos_Codigo, AV60Unidades, AV59Complexidade, AV63TipoDias) ;
      }

      protected void wb_table1_2_R62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 5,'',false,'" + sGXsfl_9_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavNaoconsiderarprazoinicial_Internalname, StringUtil.BoolToStr( AV72NaoConsiderarPrazoInicial), "", "", 1, 1, "true", "N�o considerar Prazo Inicial (BD)", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(5, this, 'true', 'false');gx.ajax.executeCliEvent('e18r62_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,5);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\">") ;
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"9\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtLogResponsavel_DemandaCod_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Demanda") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Log Responsavel_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLogResponsavel_DataHora_Titleformat == 0 )
               {
                  context.SendWebValue( edtLogResponsavel_DataHora_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLogResponsavel_DataHora_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Emissor") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbLogResponsavel_Acao_Titleformat == 0 )
               {
                  context.SendWebValue( cmbLogResponsavel_Acao.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbLogResponsavel_Acao.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Destino") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbLogResponsavel_Status_Titleformat == 0 )
               {
                  context.SendWebValue( cmbLogResponsavel_Status.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbLogResponsavel_Status.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbLogResponsavel_NovoStatus_Titleformat == 0 )
               {
                  context.SendWebValue( cmbLogResponsavel_NovoStatus.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbLogResponsavel_NovoStatus.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLogResponsavel_Prazo_Titleformat == 0 )
               {
                  context.SendWebValue( edtLogResponsavel_Prazo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLogResponsavel_Prazo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Dias") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Extra") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Prazo Sugerido") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Dias") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLogResponsavel_DemandaCod_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1797LogResponsavel_Codigo), 10, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A893LogResponsavel_DataHora, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLogResponsavel_DataHora_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLogResponsavel_DataHora_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6Emissor), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavEmissor.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A894LogResponsavel_Acao));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbLogResponsavel_Acao.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbLogResponsavel_Acao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5Destino), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavDestino.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1130LogResponsavel_Status));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbLogResponsavel_Status.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbLogResponsavel_Status_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1234LogResponsavel_NovoStatus));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbLogResponsavel_NovoStatus.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbLogResponsavel_NovoStatus_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1177LogResponsavel_Prazo, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLogResponsavel_Prazo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLogResponsavel_Prazo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1237ContagemResultado_PrazoMaisDias), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV52Selected));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( AV51PrazoSugerido, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavPrazosugerido_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54Dias), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDias_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 9 )
         {
            wbEnd = 0;
            nRC_GXsfl_9 = (short)(nGXsfl_9_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCorregir_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(9), 1, 0)+","+"null"+");", bttCorregir_Caption, bttCorregir_Jsonclick, 5, "Corregir", "", StyleString, ClassString, bttCorregir_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CORREGIR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Corretor.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(9), 1, 0)+","+"null"+");", "Fechar", bttButton1_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'FECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Corretor.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_R62e( true) ;
         }
         else
         {
            wb_table1_2_R62e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV11LogResponsavel_DemandaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11LogResponsavel_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11LogResponsavel_DemandaCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOGRESPONSAVEL_DEMANDACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11LogResponsavel_DemandaCod), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAR62( ) ;
         WSR62( ) ;
         WER62( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203122124405");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_corretor.js", "?20203122124406");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_92( )
      {
         edtLogResponsavel_DemandaCod_Internalname = "LOGRESPONSAVEL_DEMANDACOD_"+sGXsfl_9_idx;
         edtLogResponsavel_Codigo_Internalname = "LOGRESPONSAVEL_CODIGO_"+sGXsfl_9_idx;
         edtLogResponsavel_DataHora_Internalname = "LOGRESPONSAVEL_DATAHORA_"+sGXsfl_9_idx;
         cmbavEmissor_Internalname = "vEMISSOR_"+sGXsfl_9_idx;
         cmbLogResponsavel_Acao_Internalname = "LOGRESPONSAVEL_ACAO_"+sGXsfl_9_idx;
         cmbavDestino_Internalname = "vDESTINO_"+sGXsfl_9_idx;
         cmbLogResponsavel_Status_Internalname = "LOGRESPONSAVEL_STATUS_"+sGXsfl_9_idx;
         cmbLogResponsavel_NovoStatus_Internalname = "LOGRESPONSAVEL_NOVOSTATUS_"+sGXsfl_9_idx;
         edtLogResponsavel_Prazo_Internalname = "LOGRESPONSAVEL_PRAZO_"+sGXsfl_9_idx;
         edtContagemResultado_PrazoInicialDias_Internalname = "CONTAGEMRESULTADO_PRAZOINICIALDIAS_"+sGXsfl_9_idx;
         edtContagemResultado_PrazoMaisDias_Internalname = "CONTAGEMRESULTADO_PRAZOMAISDIAS_"+sGXsfl_9_idx;
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_9_idx;
         edtavPrazosugerido_Internalname = "vPRAZOSUGERIDO_"+sGXsfl_9_idx;
         edtavDias_Internalname = "vDIAS_"+sGXsfl_9_idx;
      }

      protected void SubsflControlProps_fel_92( )
      {
         edtLogResponsavel_DemandaCod_Internalname = "LOGRESPONSAVEL_DEMANDACOD_"+sGXsfl_9_fel_idx;
         edtLogResponsavel_Codigo_Internalname = "LOGRESPONSAVEL_CODIGO_"+sGXsfl_9_fel_idx;
         edtLogResponsavel_DataHora_Internalname = "LOGRESPONSAVEL_DATAHORA_"+sGXsfl_9_fel_idx;
         cmbavEmissor_Internalname = "vEMISSOR_"+sGXsfl_9_fel_idx;
         cmbLogResponsavel_Acao_Internalname = "LOGRESPONSAVEL_ACAO_"+sGXsfl_9_fel_idx;
         cmbavDestino_Internalname = "vDESTINO_"+sGXsfl_9_fel_idx;
         cmbLogResponsavel_Status_Internalname = "LOGRESPONSAVEL_STATUS_"+sGXsfl_9_fel_idx;
         cmbLogResponsavel_NovoStatus_Internalname = "LOGRESPONSAVEL_NOVOSTATUS_"+sGXsfl_9_fel_idx;
         edtLogResponsavel_Prazo_Internalname = "LOGRESPONSAVEL_PRAZO_"+sGXsfl_9_fel_idx;
         edtContagemResultado_PrazoInicialDias_Internalname = "CONTAGEMRESULTADO_PRAZOINICIALDIAS_"+sGXsfl_9_fel_idx;
         edtContagemResultado_PrazoMaisDias_Internalname = "CONTAGEMRESULTADO_PRAZOMAISDIAS_"+sGXsfl_9_fel_idx;
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_9_fel_idx;
         edtavPrazosugerido_Internalname = "vPRAZOSUGERIDO_"+sGXsfl_9_fel_idx;
         edtavDias_Internalname = "vDIAS_"+sGXsfl_9_fel_idx;
      }

      protected void sendrow_92( )
      {
         SubsflControlProps_92( ) ;
         WBR60( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_9_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_9_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_9_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((edtLogResponsavel_DemandaCod_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLogResponsavel_DemandaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A892LogResponsavel_DemandaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A892LogResponsavel_DemandaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLogResponsavel_DemandaCod_Jsonclick,(short)0,(String)"Attribute","font-family:'Microsoft Sans Serif'; font-size:10.0pt; font-weight:bold; font-style:normal; text-decoration:underline;",(String)ROClassString,(String)"",(int)edtLogResponsavel_DemandaCod_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)9,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLogResponsavel_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1797LogResponsavel_Codigo), 10, 0, ",", "")),context.localUtil.Format( (decimal)(A1797LogResponsavel_Codigo), "ZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLogResponsavel_Codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)9,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo10",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLogResponsavel_DataHora_Internalname,context.localUtil.TToC( A893LogResponsavel_DataHora, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A893LogResponsavel_DataHora, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLogResponsavel_DataHora_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)9,(short)1,(short)0,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            TempTags = " " + ((cmbavEmissor.Enabled!=0)&&(cmbavEmissor.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 13,'',false,'"+sGXsfl_9_idx+"',9)\"" : " ");
            if ( ( nGXsfl_9_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vEMISSOR_" + sGXsfl_9_idx;
               cmbavEmissor.Name = GXCCtl;
               cmbavEmissor.WebTags = "";
               if ( cmbavEmissor.ItemCount > 0 )
               {
                  AV6Emissor = (int)(NumberUtil.Val( cmbavEmissor.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0))), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavEmissor_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0)));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavEmissor,(String)cmbavEmissor_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0)),(short)1,(String)cmbavEmissor_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavEmissor.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",TempTags+((cmbavEmissor.Enabled!=0)&&(cmbavEmissor.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavEmissor.Enabled!=0)&&(cmbavEmissor.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,13);\"" : " "),(String)"",(bool)true});
            cmbavEmissor.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6Emissor), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEmissor_Internalname, "Values", (String)(cmbavEmissor.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_9_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "LOGRESPONSAVEL_ACAO_" + sGXsfl_9_idx;
               cmbLogResponsavel_Acao.Name = GXCCtl;
               cmbLogResponsavel_Acao.WebTags = "";
               cmbLogResponsavel_Acao.addItem("A", "Atribui", 0);
               cmbLogResponsavel_Acao.addItem("B", "StandBy", 0);
               cmbLogResponsavel_Acao.addItem("C", "Captura", 0);
               cmbLogResponsavel_Acao.addItem("R", "Rejeita", 0);
               cmbLogResponsavel_Acao.addItem("L", "Libera", 0);
               cmbLogResponsavel_Acao.addItem("E", "Encaminha", 0);
               cmbLogResponsavel_Acao.addItem("I", "Importa", 0);
               cmbLogResponsavel_Acao.addItem("S", "Solicita", 0);
               cmbLogResponsavel_Acao.addItem("D", "Diverg�ncia", 0);
               cmbLogResponsavel_Acao.addItem("V", "Resolvida", 0);
               cmbLogResponsavel_Acao.addItem("H", "Homologa", 0);
               cmbLogResponsavel_Acao.addItem("Q", "Liquida", 0);
               cmbLogResponsavel_Acao.addItem("P", "Fatura", 0);
               cmbLogResponsavel_Acao.addItem("O", "Aceite", 0);
               cmbLogResponsavel_Acao.addItem("N", "N�o Acata", 0);
               cmbLogResponsavel_Acao.addItem("M", "Autom�tica", 0);
               cmbLogResponsavel_Acao.addItem("F", "Cumprido", 0);
               cmbLogResponsavel_Acao.addItem("T", "Acata", 0);
               cmbLogResponsavel_Acao.addItem("X", "Cancela", 0);
               cmbLogResponsavel_Acao.addItem("NO", "Nota", 0);
               cmbLogResponsavel_Acao.addItem("U", "Altera", 0);
               cmbLogResponsavel_Acao.addItem("UN", "Rascunho", 0);
               cmbLogResponsavel_Acao.addItem("EA", "Em An�lise", 0);
               cmbLogResponsavel_Acao.addItem("RN", "Reuni�o", 0);
               cmbLogResponsavel_Acao.addItem("BK", "Desfaz", 0);
               cmbLogResponsavel_Acao.addItem("RE", "Reinicio", 0);
               cmbLogResponsavel_Acao.addItem("PR", "Prioridade", 0);
               if ( cmbLogResponsavel_Acao.ItemCount > 0 )
               {
                  A894LogResponsavel_Acao = cmbLogResponsavel_Acao.getValidValue(A894LogResponsavel_Acao);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbLogResponsavel_Acao,(String)cmbLogResponsavel_Acao_Internalname,StringUtil.RTrim( A894LogResponsavel_Acao),(short)1,(String)cmbLogResponsavel_Acao_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbLogResponsavel_Acao.CurrentValue = StringUtil.RTrim( A894LogResponsavel_Acao);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_Acao_Internalname, "Values", (String)(cmbLogResponsavel_Acao.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            TempTags = " " + ((cmbavDestino.Enabled!=0)&&(cmbavDestino.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 15,'',false,'"+sGXsfl_9_idx+"',9)\"" : " ");
            if ( ( nGXsfl_9_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vDESTINO_" + sGXsfl_9_idx;
               cmbavDestino.Name = GXCCtl;
               cmbavDestino.WebTags = "";
               if ( cmbavDestino.ItemCount > 0 )
               {
                  AV5Destino = (int)(NumberUtil.Val( cmbavDestino.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Destino), 6, 0))), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavDestino_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Destino), 6, 0)));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavDestino,(String)cmbavDestino_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV5Destino), 6, 0)),(short)1,(String)cmbavDestino_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavDestino.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",TempTags+((cmbavDestino.Enabled!=0)&&(cmbavDestino.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavDestino.Enabled!=0)&&(cmbavDestino.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,15);\"" : " "),(String)"",(bool)true});
            cmbavDestino.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5Destino), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDestino_Internalname, "Values", (String)(cmbavDestino.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_9_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "LOGRESPONSAVEL_STATUS_" + sGXsfl_9_idx;
               cmbLogResponsavel_Status.Name = GXCCtl;
               cmbLogResponsavel_Status.WebTags = "";
               cmbLogResponsavel_Status.addItem("B", "Stand by", 0);
               cmbLogResponsavel_Status.addItem("S", "Solicitada", 0);
               cmbLogResponsavel_Status.addItem("E", "Em An�lise", 0);
               cmbLogResponsavel_Status.addItem("A", "Em execu��o", 0);
               cmbLogResponsavel_Status.addItem("R", "Resolvida", 0);
               cmbLogResponsavel_Status.addItem("C", "Conferida", 0);
               cmbLogResponsavel_Status.addItem("D", "Rejeitada", 0);
               cmbLogResponsavel_Status.addItem("H", "Homologada", 0);
               cmbLogResponsavel_Status.addItem("O", "Aceite", 0);
               cmbLogResponsavel_Status.addItem("P", "A Pagar", 0);
               cmbLogResponsavel_Status.addItem("L", "Liquidada", 0);
               cmbLogResponsavel_Status.addItem("X", "Cancelada", 0);
               cmbLogResponsavel_Status.addItem("N", "N�o Faturada", 0);
               cmbLogResponsavel_Status.addItem("J", "Planejamento", 0);
               cmbLogResponsavel_Status.addItem("I", "An�lise Planejamento", 0);
               cmbLogResponsavel_Status.addItem("T", "Validacao T�cnica", 0);
               cmbLogResponsavel_Status.addItem("Q", "Validacao Qualidade", 0);
               cmbLogResponsavel_Status.addItem("G", "Em Homologa��o", 0);
               cmbLogResponsavel_Status.addItem("M", "Valida��o Mensura��o", 0);
               cmbLogResponsavel_Status.addItem("U", "Rascunho", 0);
               if ( cmbLogResponsavel_Status.ItemCount > 0 )
               {
                  A1130LogResponsavel_Status = cmbLogResponsavel_Status.getValidValue(A1130LogResponsavel_Status);
                  n1130LogResponsavel_Status = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbLogResponsavel_Status,(String)cmbLogResponsavel_Status_Internalname,StringUtil.RTrim( A1130LogResponsavel_Status),(short)1,(String)cmbLogResponsavel_Status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbLogResponsavel_Status.CurrentValue = StringUtil.RTrim( A1130LogResponsavel_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_Status_Internalname, "Values", (String)(cmbLogResponsavel_Status.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_9_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "LOGRESPONSAVEL_NOVOSTATUS_" + sGXsfl_9_idx;
               cmbLogResponsavel_NovoStatus.Name = GXCCtl;
               cmbLogResponsavel_NovoStatus.WebTags = "";
               cmbLogResponsavel_NovoStatus.addItem("B", "Stand by", 0);
               cmbLogResponsavel_NovoStatus.addItem("S", "Solicitada", 0);
               cmbLogResponsavel_NovoStatus.addItem("E", "Em An�lise", 0);
               cmbLogResponsavel_NovoStatus.addItem("A", "Em execu��o", 0);
               cmbLogResponsavel_NovoStatus.addItem("R", "Resolvida", 0);
               cmbLogResponsavel_NovoStatus.addItem("C", "Conferida", 0);
               cmbLogResponsavel_NovoStatus.addItem("D", "Rejeitada", 0);
               cmbLogResponsavel_NovoStatus.addItem("H", "Homologada", 0);
               cmbLogResponsavel_NovoStatus.addItem("O", "Aceite", 0);
               cmbLogResponsavel_NovoStatus.addItem("P", "A Pagar", 0);
               cmbLogResponsavel_NovoStatus.addItem("L", "Liquidada", 0);
               cmbLogResponsavel_NovoStatus.addItem("X", "Cancelada", 0);
               cmbLogResponsavel_NovoStatus.addItem("N", "N�o Faturada", 0);
               cmbLogResponsavel_NovoStatus.addItem("J", "Planejamento", 0);
               cmbLogResponsavel_NovoStatus.addItem("I", "An�lise Planejamento", 0);
               cmbLogResponsavel_NovoStatus.addItem("T", "Validacao T�cnica", 0);
               cmbLogResponsavel_NovoStatus.addItem("Q", "Validacao Qualidade", 0);
               cmbLogResponsavel_NovoStatus.addItem("G", "Em Homologa��o", 0);
               cmbLogResponsavel_NovoStatus.addItem("M", "Valida��o Mensura��o", 0);
               cmbLogResponsavel_NovoStatus.addItem("U", "Rascunho", 0);
               if ( cmbLogResponsavel_NovoStatus.ItemCount > 0 )
               {
                  A1234LogResponsavel_NovoStatus = cmbLogResponsavel_NovoStatus.getValidValue(A1234LogResponsavel_NovoStatus);
                  n1234LogResponsavel_NovoStatus = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbLogResponsavel_NovoStatus,(String)cmbLogResponsavel_NovoStatus_Internalname,StringUtil.RTrim( A1234LogResponsavel_NovoStatus),(short)1,(String)cmbLogResponsavel_NovoStatus_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbLogResponsavel_NovoStatus.CurrentValue = StringUtil.RTrim( A1234LogResponsavel_NovoStatus);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbLogResponsavel_NovoStatus_Internalname, "Values", (String)(cmbLogResponsavel_NovoStatus.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLogResponsavel_Prazo_Internalname,context.localUtil.TToC( A1177LogResponsavel_Prazo, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1177LogResponsavel_Prazo, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLogResponsavel_Prazo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)9,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PrazoInicialDias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1227ContagemResultado_PrazoInicialDias), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PrazoInicialDias_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)9,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContagemResultado_PrazoMaisDias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1237ContagemResultado_PrazoMaisDias), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1237ContagemResultado_PrazoMaisDias), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContagemResultado_PrazoMaisDias_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)9,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 21,'',false,'"+sGXsfl_9_idx+"',9)\"" : " ");
            ClassString = "Attribute";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavSelected_Internalname,StringUtil.BoolToStr( AV52Selected),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(21, this, 'true', 'false');gx.ajax.executeCliEvent('e22r62_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,21);\"" : " ")});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavPrazosugerido_Enabled!=0)&&(edtavPrazosugerido_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 22,'',false,'"+sGXsfl_9_idx+"',9)\"" : " ");
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavPrazosugerido_Internalname,context.localUtil.TToC( AV51PrazoSugerido, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( AV51PrazoSugerido, "99/99/99 99:99"),TempTags+((edtavPrazosugerido_Enabled!=0)&&(edtavPrazosugerido_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavPrazosugerido_Enabled!=0)&&(edtavPrazosugerido_Visible!=0) ? " onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,22);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavPrazosugerido_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavPrazosugerido_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)9,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavDias_Enabled!=0)&&(edtavDias_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 23,'',false,'"+sGXsfl_9_idx+"',9)\"" : " ");
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavDias_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54Dias), 4, 0, ",", "")),((edtavDias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV54Dias), "ZZZ9")) : context.localUtil.Format( (decimal)(AV54Dias), "ZZZ9")),TempTags+((edtavDias_Enabled!=0)&&(edtavDias_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavDias_Enabled!=0)&&(edtavDias_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavDias_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavDias_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)9,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_DEMANDACOD"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sGXsfl_9_idx, context.localUtil.Format( (decimal)(A892LogResponsavel_DemandaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_DATAHORA"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sGXsfl_9_idx, context.localUtil.Format( A893LogResponsavel_DataHora, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_ACAO"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sGXsfl_9_idx, StringUtil.RTrim( context.localUtil.Format( A894LogResponsavel_Acao, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_STATUS"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sGXsfl_9_idx, StringUtil.RTrim( context.localUtil.Format( A1130LogResponsavel_Status, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_NOVOSTATUS"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sGXsfl_9_idx, StringUtil.RTrim( context.localUtil.Format( A1234LogResponsavel_NovoStatus, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_LOGRESPONSAVEL_PRAZO"+"_"+sGXsfl_9_idx, GetSecureSignedToken( sGXsfl_9_idx, context.localUtil.Format( A1177LogResponsavel_Prazo, "99/99/99 99:99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_9_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_9_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_9_idx+1));
            sGXsfl_9_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_9_idx), 4, 0)), 4, "0");
            SubsflControlProps_92( ) ;
         }
         /* End function sendrow_92 */
      }

      protected void init_default_properties( )
      {
         chkavNaoconsiderarprazoinicial_Internalname = "vNAOCONSIDERARPRAZOINICIAL";
         edtLogResponsavel_DemandaCod_Internalname = "LOGRESPONSAVEL_DEMANDACOD";
         edtLogResponsavel_Codigo_Internalname = "LOGRESPONSAVEL_CODIGO";
         edtLogResponsavel_DataHora_Internalname = "LOGRESPONSAVEL_DATAHORA";
         cmbavEmissor_Internalname = "vEMISSOR";
         cmbLogResponsavel_Acao_Internalname = "LOGRESPONSAVEL_ACAO";
         cmbavDestino_Internalname = "vDESTINO";
         cmbLogResponsavel_Status_Internalname = "LOGRESPONSAVEL_STATUS";
         cmbLogResponsavel_NovoStatus_Internalname = "LOGRESPONSAVEL_NOVOSTATUS";
         edtLogResponsavel_Prazo_Internalname = "LOGRESPONSAVEL_PRAZO";
         edtContagemResultado_PrazoInicialDias_Internalname = "CONTAGEMRESULTADO_PRAZOINICIALDIAS";
         edtContagemResultado_PrazoMaisDias_Internalname = "CONTAGEMRESULTADO_PRAZOMAISDIAS";
         chkavSelected_Internalname = "vSELECTED";
         edtavPrazosugerido_Internalname = "vPRAZOSUGERIDO";
         edtavDias_Internalname = "vDIAS";
         bttCorregir_Internalname = "CORREGIR";
         bttButton1_Internalname = "BUTTON1";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         edtLogResponsavel_DemandaCod_Internalname = "LOGRESPONSAVEL_DEMANDACOD";
         edtavTflogresponsavel_datahora_Internalname = "vTFLOGRESPONSAVEL_DATAHORA";
         edtavTflogresponsavel_datahora_to_Internalname = "vTFLOGRESPONSAVEL_DATAHORA_TO";
         edtavDdo_logresponsavel_datahoraauxdate_Internalname = "vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE";
         edtavDdo_logresponsavel_datahoraauxdateto_Internalname = "vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO";
         divDdo_logresponsavel_datahoraauxdates_Internalname = "DDO_LOGRESPONSAVEL_DATAHORAAUXDATES";
         edtavTflogresponsavel_prazo_Internalname = "vTFLOGRESPONSAVEL_PRAZO";
         edtavTflogresponsavel_prazo_to_Internalname = "vTFLOGRESPONSAVEL_PRAZO_TO";
         edtavDdo_logresponsavel_prazoauxdate_Internalname = "vDDO_LOGRESPONSAVEL_PRAZOAUXDATE";
         edtavDdo_logresponsavel_prazoauxdateto_Internalname = "vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO";
         divDdo_logresponsavel_prazoauxdates_Internalname = "DDO_LOGRESPONSAVEL_PRAZOAUXDATES";
         Ddo_logresponsavel_datahora_Internalname = "DDO_LOGRESPONSAVEL_DATAHORA";
         edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Internalname = "vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE";
         Ddo_logresponsavel_acao_Internalname = "DDO_LOGRESPONSAVEL_ACAO";
         edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Internalname = "vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE";
         Ddo_logresponsavel_status_Internalname = "DDO_LOGRESPONSAVEL_STATUS";
         edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Internalname = "vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE";
         Ddo_logresponsavel_novostatus_Internalname = "DDO_LOGRESPONSAVEL_NOVOSTATUS";
         edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Internalname = "vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE";
         Ddo_logresponsavel_prazo_Internalname = "DDO_LOGRESPONSAVEL_PRAZO";
         edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Internalname = "vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavDias_Jsonclick = "";
         edtavDias_Visible = -1;
         edtavPrazosugerido_Jsonclick = "";
         edtavPrazosugerido_Visible = -1;
         chkavSelected.Visible = -1;
         chkavSelected.Enabled = 1;
         edtContagemResultado_PrazoMaisDias_Jsonclick = "";
         edtContagemResultado_PrazoInicialDias_Jsonclick = "";
         edtLogResponsavel_Prazo_Jsonclick = "";
         cmbLogResponsavel_NovoStatus_Jsonclick = "";
         cmbLogResponsavel_Status_Jsonclick = "";
         cmbavDestino_Jsonclick = "";
         cmbavDestino.Visible = -1;
         cmbLogResponsavel_Acao_Jsonclick = "";
         cmbavEmissor_Jsonclick = "";
         cmbavEmissor.Visible = -1;
         edtLogResponsavel_DataHora_Jsonclick = "";
         edtLogResponsavel_Codigo_Jsonclick = "";
         bttCorregir_Visible = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDias_Enabled = 1;
         edtavPrazosugerido_Enabled = 1;
         cmbavDestino.Enabled = 1;
         cmbavEmissor.Enabled = 1;
         edtLogResponsavel_Prazo_Titleformat = 0;
         cmbLogResponsavel_NovoStatus_Titleformat = 0;
         cmbLogResponsavel_Status_Titleformat = 0;
         cmbLogResponsavel_Acao_Titleformat = 0;
         edtLogResponsavel_DataHora_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         bttCorregir_Caption = "Corregir";
         edtLogResponsavel_Prazo_Title = "Prazo";
         cmbLogResponsavel_NovoStatus.Title.Text = "Novo status";
         cmbLogResponsavel_Status.Title.Text = "Status anterior";
         cmbLogResponsavel_Acao.Title.Text = "A��o";
         edtLogResponsavel_DataHora_Title = "Data";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavSelected.Caption = "";
         chkavNaoconsiderarprazoinicial.Caption = "";
         lblTbjava_Caption = "Script";
         lblTbjava_Visible = 1;
         edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Visible = 1;
         edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Visible = 1;
         edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Visible = 1;
         edtavDdo_logresponsavel_prazoauxdateto_Jsonclick = "";
         edtavDdo_logresponsavel_prazoauxdate_Jsonclick = "";
         edtavTflogresponsavel_prazo_to_Jsonclick = "";
         edtavTflogresponsavel_prazo_to_Visible = 1;
         edtavTflogresponsavel_prazo_Jsonclick = "";
         edtavTflogresponsavel_prazo_Visible = 1;
         edtavDdo_logresponsavel_datahoraauxdateto_Jsonclick = "";
         edtavDdo_logresponsavel_datahoraauxdate_Jsonclick = "";
         edtavTflogresponsavel_datahora_to_Jsonclick = "";
         edtavTflogresponsavel_datahora_to_Visible = 1;
         edtavTflogresponsavel_datahora_Jsonclick = "";
         edtavTflogresponsavel_datahora_Visible = 1;
         edtLogResponsavel_DemandaCod_Jsonclick = "";
         Ddo_logresponsavel_prazo_Searchbuttontext = "Pesquisar";
         Ddo_logresponsavel_prazo_Rangefilterto = "At�";
         Ddo_logresponsavel_prazo_Rangefilterfrom = "Desde";
         Ddo_logresponsavel_prazo_Cleanfilter = "Limpar pesquisa";
         Ddo_logresponsavel_prazo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_logresponsavel_prazo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_logresponsavel_prazo_Filtertype = "Date";
         Ddo_logresponsavel_prazo_Includefilter = Convert.ToBoolean( -1);
         Ddo_logresponsavel_prazo_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_prazo_Includesortasc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_prazo_Titlecontrolidtoreplace = "";
         Ddo_logresponsavel_prazo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_logresponsavel_prazo_Cls = "ColumnSettings";
         Ddo_logresponsavel_prazo_Tooltip = "Op��es";
         Ddo_logresponsavel_prazo_Caption = "";
         Ddo_logresponsavel_novostatus_Searchbuttontext = "Filtrar Selecionados";
         Ddo_logresponsavel_novostatus_Cleanfilter = "Limpar pesquisa";
         Ddo_logresponsavel_novostatus_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Rejeitada,H:Homologada,O:Aceite,P:A_Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_logresponsavel_novostatus_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_logresponsavel_novostatus_Datalisttype = "FixedValues";
         Ddo_logresponsavel_novostatus_Includedatalist = Convert.ToBoolean( -1);
         Ddo_logresponsavel_novostatus_Includefilter = Convert.ToBoolean( 0);
         Ddo_logresponsavel_novostatus_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_novostatus_Includesortasc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace = "";
         Ddo_logresponsavel_novostatus_Dropdownoptionstype = "GridTitleSettings";
         Ddo_logresponsavel_novostatus_Cls = "ColumnSettings";
         Ddo_logresponsavel_novostatus_Tooltip = "Op��es";
         Ddo_logresponsavel_novostatus_Caption = "";
         Ddo_logresponsavel_status_Searchbuttontext = "Filtrar Selecionados";
         Ddo_logresponsavel_status_Cleanfilter = "Limpar pesquisa";
         Ddo_logresponsavel_status_Datalistfixedvalues = "B:Stand by,S:Solicitada,E:Em An�lise,A:Em execu��o,R:Resolvida,C:Conferida,D:Rejeitada,H:Homologada,O:Aceite,P:A_Pagar,L:Liquidada,X:Cancelada,N:N�o Faturada,J:Planejamento,I:An�lise Planejamento,T:Validacao T�cnica,Q:Validacao Qualidade,G:Em Homologa��o,M:Valida��o Mensura��o,U:Rascunho";
         Ddo_logresponsavel_status_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_logresponsavel_status_Datalisttype = "FixedValues";
         Ddo_logresponsavel_status_Includedatalist = Convert.ToBoolean( -1);
         Ddo_logresponsavel_status_Includefilter = Convert.ToBoolean( 0);
         Ddo_logresponsavel_status_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_status_Includesortasc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_status_Titlecontrolidtoreplace = "";
         Ddo_logresponsavel_status_Dropdownoptionstype = "GridTitleSettings";
         Ddo_logresponsavel_status_Cls = "ColumnSettings";
         Ddo_logresponsavel_status_Tooltip = "Op��es";
         Ddo_logresponsavel_status_Caption = "";
         Ddo_logresponsavel_acao_Searchbuttontext = "Filtrar Selecionados";
         Ddo_logresponsavel_acao_Cleanfilter = "Limpar pesquisa";
         Ddo_logresponsavel_acao_Datalistfixedvalues = "A:Atribui,B:StandBy,C:Captura,R:Rejeita,L:Libera,E:Encaminha,I:Importa,S:Solicita,D:Diverg�ncia,V:Resolvida,H:Homologa,Q:Liquida,P:Fatura,O:Aceite,N:N�o Acata,M:Autom�tica,F:Cumprido,T:Acata,X:Cancela,NO:Nota,U:Altera,UN:Rascunho,EA:Em An�lise,RN:Reuni�o,BK:Desfaz,RE:Reinicio,PR:Prioridade";
         Ddo_logresponsavel_acao_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_logresponsavel_acao_Datalisttype = "FixedValues";
         Ddo_logresponsavel_acao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_logresponsavel_acao_Includefilter = Convert.ToBoolean( 0);
         Ddo_logresponsavel_acao_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_acao_Includesortasc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_acao_Titlecontrolidtoreplace = "";
         Ddo_logresponsavel_acao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_logresponsavel_acao_Cls = "ColumnSettings";
         Ddo_logresponsavel_acao_Tooltip = "Op��es";
         Ddo_logresponsavel_acao_Caption = "";
         Ddo_logresponsavel_datahora_Searchbuttontext = "Pesquisar";
         Ddo_logresponsavel_datahora_Rangefilterto = "At�";
         Ddo_logresponsavel_datahora_Rangefilterfrom = "Desde";
         Ddo_logresponsavel_datahora_Cleanfilter = "Limpar pesquisa";
         Ddo_logresponsavel_datahora_Includedatalist = Convert.ToBoolean( 0);
         Ddo_logresponsavel_datahora_Filterisrange = Convert.ToBoolean( -1);
         Ddo_logresponsavel_datahora_Filtertype = "Date";
         Ddo_logresponsavel_datahora_Includefilter = Convert.ToBoolean( -1);
         Ddo_logresponsavel_datahora_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_datahora_Includesortasc = Convert.ToBoolean( 0);
         Ddo_logresponsavel_datahora_Titlecontrolidtoreplace = "";
         Ddo_logresponsavel_datahora_Dropdownoptionstype = "GridTitleSettings";
         Ddo_logresponsavel_datahora_Cls = "ColumnSettings";
         Ddo_logresponsavel_datahora_Tooltip = "Op��es";
         Ddo_logresponsavel_datahora_Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Corretor";
         subGrid_Rows = 0;
         edtLogResponsavel_DemandaCod_Visible = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV19LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV25LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV29LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV33LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV37LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLogResponsavel_DataHora_Titleformat',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat'},{av:'edtLogResponsavel_DataHora_Title',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{av:'cmbLogResponsavel_Acao'},{av:'cmbLogResponsavel_Status'},{av:'cmbLogResponsavel_NovoStatus'},{av:'edtLogResponsavel_Prazo_Titleformat',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat'},{av:'edtLogResponsavel_Prazo_Title',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'AV68Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("DDO_LOGRESPONSAVEL_DATAHORA.ONOPTIONCLICKED","{handler:'E11R62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'Ddo_logresponsavel_datahora_Activeeventkey',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'ActiveEventKey'},{av:'Ddo_logresponsavel_datahora_Filteredtext_get',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'FilteredText_get'},{av:'Ddo_logresponsavel_datahora_Filteredtextto_get',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("DDO_LOGRESPONSAVEL_ACAO.ONOPTIONCLICKED","{handler:'E12R62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'Ddo_logresponsavel_acao_Activeeventkey',ctrl:'DDO_LOGRESPONSAVEL_ACAO',prop:'ActiveEventKey'},{av:'Ddo_logresponsavel_acao_Selectedvalue_get',ctrl:'DDO_LOGRESPONSAVEL_ACAO',prop:'SelectedValue_get'}],oparms:[{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_LOGRESPONSAVEL_STATUS.ONOPTIONCLICKED","{handler:'E13R62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'Ddo_logresponsavel_status_Activeeventkey',ctrl:'DDO_LOGRESPONSAVEL_STATUS',prop:'ActiveEventKey'},{av:'Ddo_logresponsavel_status_Selectedvalue_get',ctrl:'DDO_LOGRESPONSAVEL_STATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_LOGRESPONSAVEL_NOVOSTATUS.ONOPTIONCLICKED","{handler:'E14R62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'Ddo_logresponsavel_novostatus_Activeeventkey',ctrl:'DDO_LOGRESPONSAVEL_NOVOSTATUS',prop:'ActiveEventKey'},{av:'Ddo_logresponsavel_novostatus_Selectedvalue_get',ctrl:'DDO_LOGRESPONSAVEL_NOVOSTATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_LOGRESPONSAVEL_PRAZO.ONOPTIONCLICKED","{handler:'E15R62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'Ddo_logresponsavel_prazo_Activeeventkey',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'ActiveEventKey'},{av:'Ddo_logresponsavel_prazo_Filteredtext_get',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'FilteredText_get'},{av:'Ddo_logresponsavel_prazo_Filteredtextto_get',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("GRID.LOAD","{handler:'E21R62',iparms:[{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''}],oparms:[{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Caption'},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV54Dias',fld:'vDIAS',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("VSELECTED.CLICK","{handler:'E22R62',iparms:[{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0}],oparms:[{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Caption'},{ctrl:'CORREGIR',prop:'Visible'}]}");
         setEventMetadata("VNAOCONSIDERARPRAZOINICIAL.CLICK","{handler:'E18R62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'FECHAR'","{handler:'E16R62',iparms:[],oparms:[]}");
         setEventMetadata("'CORREGIR'","{handler:'E17R62',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',grid:9,pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',grid:9,pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',grid:9,pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',grid:9,pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',grid:9,pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',grid:9,pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',grid:9,pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',grid:9,pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',grid:9,pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',grid:9,pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',grid:9,pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',grid:9,pic:'ZZZZZZZZZ9',nv:0},{av:'AV54Dias',fld:'vDIAS',grid:9,pic:'ZZZ9',nv:0}],oparms:[{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV19LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV25LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV29LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV33LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV37LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLogResponsavel_DataHora_Titleformat',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat'},{av:'edtLogResponsavel_DataHora_Title',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{av:'cmbLogResponsavel_Acao'},{av:'cmbLogResponsavel_Status'},{av:'cmbLogResponsavel_NovoStatus'},{av:'edtLogResponsavel_Prazo_Titleformat',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat'},{av:'edtLogResponsavel_Prazo_Title',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'AV68Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV19LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV25LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV29LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV33LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV37LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLogResponsavel_DataHora_Titleformat',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat'},{av:'edtLogResponsavel_DataHora_Title',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{av:'cmbLogResponsavel_Acao'},{av:'cmbLogResponsavel_Status'},{av:'cmbLogResponsavel_NovoStatus'},{av:'edtLogResponsavel_Prazo_Titleformat',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat'},{av:'edtLogResponsavel_Prazo_Title',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'AV68Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV19LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV25LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV29LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV33LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV37LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLogResponsavel_DataHora_Titleformat',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat'},{av:'edtLogResponsavel_DataHora_Title',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{av:'cmbLogResponsavel_Acao'},{av:'cmbLogResponsavel_Status'},{av:'cmbLogResponsavel_NovoStatus'},{av:'edtLogResponsavel_Prazo_Titleformat',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat'},{av:'edtLogResponsavel_Prazo_Title',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'AV68Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'edtLogResponsavel_DemandaCod_Visible',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],oparms:[{av:'AV19LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV25LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV29LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV33LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV37LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtLogResponsavel_DataHora_Titleformat',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat'},{av:'edtLogResponsavel_DataHora_Title',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{av:'cmbLogResponsavel_Acao'},{av:'cmbLogResponsavel_Status'},{av:'cmbLogResponsavel_NovoStatus'},{av:'edtLogResponsavel_Prazo_Titleformat',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat'},{av:'edtLogResponsavel_Prazo_Title',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'AV68Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_logresponsavel_datahora_Activeeventkey = "";
         Ddo_logresponsavel_datahora_Filteredtext_get = "";
         Ddo_logresponsavel_datahora_Filteredtextto_get = "";
         Ddo_logresponsavel_acao_Activeeventkey = "";
         Ddo_logresponsavel_acao_Selectedvalue_get = "";
         Ddo_logresponsavel_status_Activeeventkey = "";
         Ddo_logresponsavel_status_Selectedvalue_get = "";
         Ddo_logresponsavel_novostatus_Activeeventkey = "";
         Ddo_logresponsavel_novostatus_Selectedvalue_get = "";
         Ddo_logresponsavel_prazo_Activeeventkey = "";
         Ddo_logresponsavel_prazo_Filteredtext_get = "";
         Ddo_logresponsavel_prazo_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace = "";
         AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace = "";
         AV32ddo_LogResponsavel_StatusTitleControlIdToReplace = "";
         AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = "";
         AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace = "";
         AV75Pgmname = "";
         AV20TFLogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         AV21TFLogResponsavel_DataHora_To = (DateTime)(DateTime.MinValue);
         AV27TFLogResponsavel_Acao_Sels = new GxSimpleCollection();
         AV31TFLogResponsavel_Status_Sels = new GxSimpleCollection();
         AV35TFLogResponsavel_NovoStatus_Sels = new GxSimpleCollection();
         AV38TFLogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         AV39TFLogResponsavel_Prazo_To = (DateTime)(DateTime.MinValue);
         A58Usuario_PessoaNom = "";
         A493ContagemResultado_DemandaFM = "";
         AV18WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         A1611ContagemResultado_PrzTpDias = "";
         A1234LogResponsavel_NovoStatus = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         AV62NovaData = DateTime.MinValue;
         AV61UltimoPrazo = (DateTime)(DateTime.MinValue);
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         A894LogResponsavel_Acao = "";
         AV51PrazoSugerido = (DateTime)(DateTime.MinValue);
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         A438Contratada_Sigla = "";
         AV63TipoDias = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV47DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV19LogResponsavel_DataHoraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV25LogResponsavel_AcaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV29LogResponsavel_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV33LogResponsavel_NovoStatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV37LogResponsavel_PrazoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_logresponsavel_datahora_Filteredtext_set = "";
         Ddo_logresponsavel_datahora_Filteredtextto_set = "";
         Ddo_logresponsavel_acao_Selectedvalue_set = "";
         Ddo_logresponsavel_status_Selectedvalue_set = "";
         Ddo_logresponsavel_novostatus_Selectedvalue_set = "";
         Ddo_logresponsavel_prazo_Filteredtext_set = "";
         Ddo_logresponsavel_prazo_Filteredtextto_set = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ROClassString = "";
         TempTags = "";
         AV22DDO_LogResponsavel_DataHoraAuxDate = DateTime.MinValue;
         AV23DDO_LogResponsavel_DataHoraAuxDateTo = DateTime.MinValue;
         AV40DDO_LogResponsavel_PrazoAuxDate = DateTime.MinValue;
         AV41DDO_LogResponsavel_PrazoAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         lblTbjava_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A1130LogResponsavel_Status = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV68Codigos = new GxSimpleCollection();
         scmdbuf = "";
         H00R63_A891LogResponsavel_UsuarioCod = new int[1] ;
         H00R63_n891LogResponsavel_UsuarioCod = new bool[] {false} ;
         H00R63_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00R63_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00R63_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00R63_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00R63_A1611ContagemResultado_PrzTpDias = new String[] {""} ;
         H00R63_n1611ContagemResultado_PrzTpDias = new bool[] {false} ;
         H00R63_A798ContagemResultado_PFBFSImp = new decimal[1] ;
         H00R63_n798ContagemResultado_PFBFSImp = new bool[] {false} ;
         H00R63_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00R63_n471ContagemResultado_DataDmn = new bool[] {false} ;
         H00R63_A1237ContagemResultado_PrazoMaisDias = new short[1] ;
         H00R63_n1237ContagemResultado_PrazoMaisDias = new bool[] {false} ;
         H00R63_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         H00R63_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         H00R63_A1177LogResponsavel_Prazo = new DateTime[] {DateTime.MinValue} ;
         H00R63_n1177LogResponsavel_Prazo = new bool[] {false} ;
         H00R63_A1234LogResponsavel_NovoStatus = new String[] {""} ;
         H00R63_n1234LogResponsavel_NovoStatus = new bool[] {false} ;
         H00R63_A1130LogResponsavel_Status = new String[] {""} ;
         H00R63_n1130LogResponsavel_Status = new bool[] {false} ;
         H00R63_A894LogResponsavel_Acao = new String[] {""} ;
         H00R63_A893LogResponsavel_DataHora = new DateTime[] {DateTime.MinValue} ;
         H00R63_A1797LogResponsavel_Codigo = new long[1] ;
         H00R63_A40000ContratadaUsuario_UsuarioCod = new int[1] ;
         H00R63_n40000ContratadaUsuario_UsuarioCod = new bool[] {false} ;
         H00R63_A896LogResponsavel_Owner = new int[1] ;
         H00R63_A892LogResponsavel_DemandaCod = new int[1] ;
         H00R63_n892LogResponsavel_DemandaCod = new bool[] {false} ;
         H00R65_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV69WebSession = context.GetSession();
         AV50String = "";
         AV26TFLogResponsavel_Acao_SelsJson = "";
         AV30TFLogResponsavel_Status_SelsJson = "";
         AV34TFLogResponsavel_NovoStatus_SelsJson = "";
         AV48OS = "";
         GridRow = new GXWebRow();
         GXt_dtime4 = (DateTime)(DateTime.MinValue);
         AV15Session = context.GetSession();
         AV7GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV8GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV16TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9HTTPRequest = new GxHttpRequest( context);
         AV17TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         H00R66_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H00R66_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H00R66_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H00R66_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H00R66_A438Contratada_Sigla = new String[] {""} ;
         H00R66_n438Contratada_Sigla = new bool[] {false} ;
         H00R67_A57Usuario_PessoaCod = new int[1] ;
         H00R67_A54Usuario_Ativo = new bool[] {false} ;
         H00R67_A58Usuario_PessoaNom = new String[] {""} ;
         H00R67_n58Usuario_PessoaNom = new bool[] {false} ;
         H00R67_A1Usuario_Codigo = new int[1] ;
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         bttCorregir_Jsonclick = "";
         bttButton1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_corretor__default(),
            new Object[][] {
                new Object[] {
               H00R63_A891LogResponsavel_UsuarioCod, H00R63_n891LogResponsavel_UsuarioCod, H00R63_A493ContagemResultado_DemandaFM, H00R63_n493ContagemResultado_DemandaFM, H00R63_A1553ContagemResultado_CntSrvCod, H00R63_n1553ContagemResultado_CntSrvCod, H00R63_A1611ContagemResultado_PrzTpDias, H00R63_n1611ContagemResultado_PrzTpDias, H00R63_A798ContagemResultado_PFBFSImp, H00R63_n798ContagemResultado_PFBFSImp,
               H00R63_A471ContagemResultado_DataDmn, H00R63_n471ContagemResultado_DataDmn, H00R63_A1237ContagemResultado_PrazoMaisDias, H00R63_n1237ContagemResultado_PrazoMaisDias, H00R63_A1227ContagemResultado_PrazoInicialDias, H00R63_n1227ContagemResultado_PrazoInicialDias, H00R63_A1177LogResponsavel_Prazo, H00R63_n1177LogResponsavel_Prazo, H00R63_A1234LogResponsavel_NovoStatus, H00R63_n1234LogResponsavel_NovoStatus,
               H00R63_A1130LogResponsavel_Status, H00R63_n1130LogResponsavel_Status, H00R63_A894LogResponsavel_Acao, H00R63_A893LogResponsavel_DataHora, H00R63_A1797LogResponsavel_Codigo, H00R63_A40000ContratadaUsuario_UsuarioCod, H00R63_n40000ContratadaUsuario_UsuarioCod, H00R63_A896LogResponsavel_Owner, H00R63_A892LogResponsavel_DemandaCod, H00R63_n892LogResponsavel_DemandaCod
               }
               , new Object[] {
               H00R65_AGRID_nRecordCount
               }
               , new Object[] {
               H00R66_A69ContratadaUsuario_UsuarioCod, H00R66_A1228ContratadaUsuario_AreaTrabalhoCod, H00R66_n1228ContratadaUsuario_AreaTrabalhoCod, H00R66_A66ContratadaUsuario_ContratadaCod, H00R66_A438Contratada_Sigla, H00R66_n438Contratada_Sigla
               }
               , new Object[] {
               H00R67_A57Usuario_PessoaCod, H00R67_A54Usuario_Ativo, H00R67_A58Usuario_PessoaNom, H00R67_n58Usuario_PessoaNom, H00R67_A1Usuario_Codigo
               }
            }
         );
         AV75Pgmname = "WP_Corretor";
         /* GeneXus formulas. */
         AV75Pgmname = "WP_Corretor";
         context.Gx_err = 0;
         cmbavEmissor.Enabled = 0;
         cmbavDestino.Enabled = 0;
         edtavPrazosugerido_Enabled = 0;
         edtavDias_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_9 ;
      private short nGXsfl_9_idx=1 ;
      private short A1237ContagemResultado_PrazoMaisDias ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short AV67Selecionadas ;
      private short AV71PrazoInicial ;
      private short AV59Complexidade ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short AV54Dias ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_9_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtLogResponsavel_DataHora_Titleformat ;
      private short cmbLogResponsavel_Acao_Titleformat ;
      private short cmbLogResponsavel_Status_Titleformat ;
      private short cmbLogResponsavel_NovoStatus_Titleformat ;
      private short edtLogResponsavel_Prazo_Titleformat ;
      private short GXt_int3 ;
      private short AV56Hours ;
      private short AV57Minutes ;
      private short nGXsfl_9_fel_idx=1 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV11LogResponsavel_DemandaCod ;
      private int wcpOAV11LogResponsavel_DemandaCod ;
      private int edtLogResponsavel_DemandaCod_Visible ;
      private int subGrid_Rows ;
      private int A1Usuario_Codigo ;
      private int A896LogResponsavel_Owner ;
      private int A891LogResponsavel_UsuarioCod ;
      private int AV70Codigo ;
      private int A892LogResponsavel_DemandaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int AV5Destino ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int AV6Emissor ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int AV58ContratoServicos_Codigo ;
      private int edtavTflogresponsavel_datahora_Visible ;
      private int edtavTflogresponsavel_datahora_to_Visible ;
      private int edtavTflogresponsavel_prazo_Visible ;
      private int edtavTflogresponsavel_prazo_to_Visible ;
      private int edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Visible ;
      private int edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Visible ;
      private int edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Visible ;
      private int edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Visible ;
      private int lblTbjava_Visible ;
      private int subGrid_Islastpage ;
      private int edtavPrazosugerido_Enabled ;
      private int edtavDias_Enabled ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV68Codigos_Count ;
      private int A40000ContratadaUsuario_UsuarioCod ;
      private int bttCorregir_Visible ;
      private int AV76GXV1 ;
      private int A57Usuario_PessoaCod ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavPrazosugerido_Visible ;
      private int edtavDias_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long A1797LogResponsavel_Codigo ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal AV60Unidades ;
      private String Ddo_logresponsavel_datahora_Activeeventkey ;
      private String Ddo_logresponsavel_datahora_Filteredtext_get ;
      private String Ddo_logresponsavel_datahora_Filteredtextto_get ;
      private String Ddo_logresponsavel_acao_Activeeventkey ;
      private String Ddo_logresponsavel_acao_Selectedvalue_get ;
      private String Ddo_logresponsavel_status_Activeeventkey ;
      private String Ddo_logresponsavel_status_Selectedvalue_get ;
      private String Ddo_logresponsavel_novostatus_Activeeventkey ;
      private String Ddo_logresponsavel_novostatus_Selectedvalue_get ;
      private String Ddo_logresponsavel_prazo_Activeeventkey ;
      private String Ddo_logresponsavel_prazo_Filteredtext_get ;
      private String Ddo_logresponsavel_prazo_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_9_idx="0001" ;
      private String edtLogResponsavel_DemandaCod_Internalname ;
      private String AV75Pgmname ;
      private String A58Usuario_PessoaNom ;
      private String A1611ContagemResultado_PrzTpDias ;
      private String A1234LogResponsavel_NovoStatus ;
      private String A894LogResponsavel_Acao ;
      private String edtavPrazosugerido_Internalname ;
      private String chkavSelected_Internalname ;
      private String cmbavDestino_Internalname ;
      private String cmbavEmissor_Internalname ;
      private String A438Contratada_Sigla ;
      private String AV63TipoDias ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_logresponsavel_datahora_Caption ;
      private String Ddo_logresponsavel_datahora_Tooltip ;
      private String Ddo_logresponsavel_datahora_Cls ;
      private String Ddo_logresponsavel_datahora_Filteredtext_set ;
      private String Ddo_logresponsavel_datahora_Filteredtextto_set ;
      private String Ddo_logresponsavel_datahora_Dropdownoptionstype ;
      private String Ddo_logresponsavel_datahora_Titlecontrolidtoreplace ;
      private String Ddo_logresponsavel_datahora_Filtertype ;
      private String Ddo_logresponsavel_datahora_Cleanfilter ;
      private String Ddo_logresponsavel_datahora_Rangefilterfrom ;
      private String Ddo_logresponsavel_datahora_Rangefilterto ;
      private String Ddo_logresponsavel_datahora_Searchbuttontext ;
      private String Ddo_logresponsavel_acao_Caption ;
      private String Ddo_logresponsavel_acao_Tooltip ;
      private String Ddo_logresponsavel_acao_Cls ;
      private String Ddo_logresponsavel_acao_Selectedvalue_set ;
      private String Ddo_logresponsavel_acao_Dropdownoptionstype ;
      private String Ddo_logresponsavel_acao_Titlecontrolidtoreplace ;
      private String Ddo_logresponsavel_acao_Datalisttype ;
      private String Ddo_logresponsavel_acao_Datalistfixedvalues ;
      private String Ddo_logresponsavel_acao_Cleanfilter ;
      private String Ddo_logresponsavel_acao_Searchbuttontext ;
      private String Ddo_logresponsavel_status_Caption ;
      private String Ddo_logresponsavel_status_Tooltip ;
      private String Ddo_logresponsavel_status_Cls ;
      private String Ddo_logresponsavel_status_Selectedvalue_set ;
      private String Ddo_logresponsavel_status_Dropdownoptionstype ;
      private String Ddo_logresponsavel_status_Titlecontrolidtoreplace ;
      private String Ddo_logresponsavel_status_Datalisttype ;
      private String Ddo_logresponsavel_status_Datalistfixedvalues ;
      private String Ddo_logresponsavel_status_Cleanfilter ;
      private String Ddo_logresponsavel_status_Searchbuttontext ;
      private String Ddo_logresponsavel_novostatus_Caption ;
      private String Ddo_logresponsavel_novostatus_Tooltip ;
      private String Ddo_logresponsavel_novostatus_Cls ;
      private String Ddo_logresponsavel_novostatus_Selectedvalue_set ;
      private String Ddo_logresponsavel_novostatus_Dropdownoptionstype ;
      private String Ddo_logresponsavel_novostatus_Titlecontrolidtoreplace ;
      private String Ddo_logresponsavel_novostatus_Datalisttype ;
      private String Ddo_logresponsavel_novostatus_Datalistfixedvalues ;
      private String Ddo_logresponsavel_novostatus_Cleanfilter ;
      private String Ddo_logresponsavel_novostatus_Searchbuttontext ;
      private String Ddo_logresponsavel_prazo_Caption ;
      private String Ddo_logresponsavel_prazo_Tooltip ;
      private String Ddo_logresponsavel_prazo_Cls ;
      private String Ddo_logresponsavel_prazo_Filteredtext_set ;
      private String Ddo_logresponsavel_prazo_Filteredtextto_set ;
      private String Ddo_logresponsavel_prazo_Dropdownoptionstype ;
      private String Ddo_logresponsavel_prazo_Titlecontrolidtoreplace ;
      private String Ddo_logresponsavel_prazo_Filtertype ;
      private String Ddo_logresponsavel_prazo_Cleanfilter ;
      private String Ddo_logresponsavel_prazo_Rangefilterfrom ;
      private String Ddo_logresponsavel_prazo_Rangefilterto ;
      private String Ddo_logresponsavel_prazo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String ROClassString ;
      private String edtLogResponsavel_DemandaCod_Jsonclick ;
      private String TempTags ;
      private String edtavTflogresponsavel_datahora_Internalname ;
      private String edtavTflogresponsavel_datahora_Jsonclick ;
      private String edtavTflogresponsavel_datahora_to_Internalname ;
      private String edtavTflogresponsavel_datahora_to_Jsonclick ;
      private String divDdo_logresponsavel_datahoraauxdates_Internalname ;
      private String edtavDdo_logresponsavel_datahoraauxdate_Internalname ;
      private String edtavDdo_logresponsavel_datahoraauxdate_Jsonclick ;
      private String edtavDdo_logresponsavel_datahoraauxdateto_Internalname ;
      private String edtavDdo_logresponsavel_datahoraauxdateto_Jsonclick ;
      private String edtavTflogresponsavel_prazo_Internalname ;
      private String edtavTflogresponsavel_prazo_Jsonclick ;
      private String edtavTflogresponsavel_prazo_to_Internalname ;
      private String edtavTflogresponsavel_prazo_to_Jsonclick ;
      private String divDdo_logresponsavel_prazoauxdates_Internalname ;
      private String edtavDdo_logresponsavel_prazoauxdate_Internalname ;
      private String edtavDdo_logresponsavel_prazoauxdate_Jsonclick ;
      private String edtavDdo_logresponsavel_prazoauxdateto_Internalname ;
      private String edtavDdo_logresponsavel_prazoauxdateto_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_logresponsavel_datahoratitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_logresponsavel_acaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_logresponsavel_statustitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_logresponsavel_novostatustitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_logresponsavel_prazotitlecontrolidtoreplace_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtLogResponsavel_Codigo_Internalname ;
      private String edtLogResponsavel_DataHora_Internalname ;
      private String cmbLogResponsavel_Acao_Internalname ;
      private String cmbLogResponsavel_Status_Internalname ;
      private String A1130LogResponsavel_Status ;
      private String cmbLogResponsavel_NovoStatus_Internalname ;
      private String edtLogResponsavel_Prazo_Internalname ;
      private String edtContagemResultado_PrazoInicialDias_Internalname ;
      private String edtContagemResultado_PrazoMaisDias_Internalname ;
      private String edtavDias_Internalname ;
      private String chkavNaoconsiderarprazoinicial_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String subGrid_Internalname ;
      private String Ddo_logresponsavel_datahora_Internalname ;
      private String Ddo_logresponsavel_acao_Internalname ;
      private String Ddo_logresponsavel_status_Internalname ;
      private String Ddo_logresponsavel_novostatus_Internalname ;
      private String Ddo_logresponsavel_prazo_Internalname ;
      private String edtLogResponsavel_DataHora_Title ;
      private String edtLogResponsavel_Prazo_Title ;
      private String AV50String ;
      private String bttCorregir_Internalname ;
      private String bttCorregir_Caption ;
      private String sGXsfl_9_fel_idx="0001" ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String bttCorregir_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String edtLogResponsavel_Codigo_Jsonclick ;
      private String edtLogResponsavel_DataHora_Jsonclick ;
      private String cmbavEmissor_Jsonclick ;
      private String cmbLogResponsavel_Acao_Jsonclick ;
      private String cmbavDestino_Jsonclick ;
      private String cmbLogResponsavel_Status_Jsonclick ;
      private String cmbLogResponsavel_NovoStatus_Jsonclick ;
      private String edtLogResponsavel_Prazo_Jsonclick ;
      private String edtContagemResultado_PrazoInicialDias_Jsonclick ;
      private String edtContagemResultado_PrazoMaisDias_Jsonclick ;
      private String edtavPrazosugerido_Jsonclick ;
      private String edtavDias_Jsonclick ;
      private DateTime AV20TFLogResponsavel_DataHora ;
      private DateTime AV21TFLogResponsavel_DataHora_To ;
      private DateTime AV38TFLogResponsavel_Prazo ;
      private DateTime AV39TFLogResponsavel_Prazo_To ;
      private DateTime AV61UltimoPrazo ;
      private DateTime A893LogResponsavel_DataHora ;
      private DateTime AV51PrazoSugerido ;
      private DateTime A1177LogResponsavel_Prazo ;
      private DateTime GXt_dtime4 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV62NovaData ;
      private DateTime AV22DDO_LogResponsavel_DataHoraAuxDate ;
      private DateTime AV23DDO_LogResponsavel_DataHoraAuxDateTo ;
      private DateTime AV40DDO_LogResponsavel_PrazoAuxDate ;
      private DateTime AV41DDO_LogResponsavel_PrazoAuxDateTo ;
      private bool entryPointCalled ;
      private bool A54Usuario_Ativo ;
      private bool n58Usuario_PessoaNom ;
      private bool n891LogResponsavel_UsuarioCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1611ContagemResultado_PrzTpDias ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n1234LogResponsavel_NovoStatus ;
      private bool n1237ContagemResultado_PrazoMaisDias ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n471ContagemResultado_DataDmn ;
      private bool A1149LogResponsavel_OwnerEhContratante ;
      private bool n1177LogResponsavel_Prazo ;
      private bool AV52Selected ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n438Contratada_Sigla ;
      private bool AV72NaoConsiderarPrazoInicial ;
      private bool toggleJsOutput ;
      private bool Ddo_logresponsavel_datahora_Includesortasc ;
      private bool Ddo_logresponsavel_datahora_Includesortdsc ;
      private bool Ddo_logresponsavel_datahora_Includefilter ;
      private bool Ddo_logresponsavel_datahora_Filterisrange ;
      private bool Ddo_logresponsavel_datahora_Includedatalist ;
      private bool Ddo_logresponsavel_acao_Includesortasc ;
      private bool Ddo_logresponsavel_acao_Includesortdsc ;
      private bool Ddo_logresponsavel_acao_Includefilter ;
      private bool Ddo_logresponsavel_acao_Includedatalist ;
      private bool Ddo_logresponsavel_acao_Allowmultipleselection ;
      private bool Ddo_logresponsavel_status_Includesortasc ;
      private bool Ddo_logresponsavel_status_Includesortdsc ;
      private bool Ddo_logresponsavel_status_Includefilter ;
      private bool Ddo_logresponsavel_status_Includedatalist ;
      private bool Ddo_logresponsavel_status_Allowmultipleselection ;
      private bool Ddo_logresponsavel_novostatus_Includesortasc ;
      private bool Ddo_logresponsavel_novostatus_Includesortdsc ;
      private bool Ddo_logresponsavel_novostatus_Includefilter ;
      private bool Ddo_logresponsavel_novostatus_Includedatalist ;
      private bool Ddo_logresponsavel_novostatus_Allowmultipleselection ;
      private bool Ddo_logresponsavel_prazo_Includesortasc ;
      private bool Ddo_logresponsavel_prazo_Includesortdsc ;
      private bool Ddo_logresponsavel_prazo_Includefilter ;
      private bool Ddo_logresponsavel_prazo_Filterisrange ;
      private bool Ddo_logresponsavel_prazo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1130LogResponsavel_Status ;
      private bool n40000ContratadaUsuario_UsuarioCod ;
      private bool GXt_boolean1 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private String AV26TFLogResponsavel_Acao_SelsJson ;
      private String AV30TFLogResponsavel_Status_SelsJson ;
      private String AV34TFLogResponsavel_NovoStatus_SelsJson ;
      private String AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace ;
      private String AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace ;
      private String AV32ddo_LogResponsavel_StatusTitleControlIdToReplace ;
      private String AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace ;
      private String AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV48OS ;
      private IGxSession AV69WebSession ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavNaoconsiderarprazoinicial ;
      private GXCombobox cmbavEmissor ;
      private GXCombobox cmbLogResponsavel_Acao ;
      private GXCombobox cmbavDestino ;
      private GXCombobox cmbLogResponsavel_Status ;
      private GXCombobox cmbLogResponsavel_NovoStatus ;
      private GXCheckbox chkavSelected ;
      private IDataStoreProvider pr_default ;
      private int[] H00R63_A891LogResponsavel_UsuarioCod ;
      private bool[] H00R63_n891LogResponsavel_UsuarioCod ;
      private String[] H00R63_A493ContagemResultado_DemandaFM ;
      private bool[] H00R63_n493ContagemResultado_DemandaFM ;
      private int[] H00R63_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00R63_n1553ContagemResultado_CntSrvCod ;
      private String[] H00R63_A1611ContagemResultado_PrzTpDias ;
      private bool[] H00R63_n1611ContagemResultado_PrzTpDias ;
      private decimal[] H00R63_A798ContagemResultado_PFBFSImp ;
      private bool[] H00R63_n798ContagemResultado_PFBFSImp ;
      private DateTime[] H00R63_A471ContagemResultado_DataDmn ;
      private bool[] H00R63_n471ContagemResultado_DataDmn ;
      private short[] H00R63_A1237ContagemResultado_PrazoMaisDias ;
      private bool[] H00R63_n1237ContagemResultado_PrazoMaisDias ;
      private short[] H00R63_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] H00R63_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] H00R63_A1177LogResponsavel_Prazo ;
      private bool[] H00R63_n1177LogResponsavel_Prazo ;
      private String[] H00R63_A1234LogResponsavel_NovoStatus ;
      private bool[] H00R63_n1234LogResponsavel_NovoStatus ;
      private String[] H00R63_A1130LogResponsavel_Status ;
      private bool[] H00R63_n1130LogResponsavel_Status ;
      private String[] H00R63_A894LogResponsavel_Acao ;
      private DateTime[] H00R63_A893LogResponsavel_DataHora ;
      private long[] H00R63_A1797LogResponsavel_Codigo ;
      private int[] H00R63_A40000ContratadaUsuario_UsuarioCod ;
      private bool[] H00R63_n40000ContratadaUsuario_UsuarioCod ;
      private int[] H00R63_A896LogResponsavel_Owner ;
      private int[] H00R63_A892LogResponsavel_DemandaCod ;
      private bool[] H00R63_n892LogResponsavel_DemandaCod ;
      private long[] H00R65_AGRID_nRecordCount ;
      private int[] H00R66_A69ContratadaUsuario_UsuarioCod ;
      private int[] H00R66_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H00R66_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H00R66_A66ContratadaUsuario_ContratadaCod ;
      private String[] H00R66_A438Contratada_Sigla ;
      private bool[] H00R66_n438Contratada_Sigla ;
      private int[] H00R67_A57Usuario_PessoaCod ;
      private bool[] H00R67_A54Usuario_Ativo ;
      private String[] H00R67_A58Usuario_PessoaNom ;
      private bool[] H00R67_n58Usuario_PessoaNom ;
      private int[] H00R67_A1Usuario_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV9HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV68Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27TFLogResponsavel_Acao_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31TFLogResponsavel_Status_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35TFLogResponsavel_NovoStatus_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV19LogResponsavel_DataHoraTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV25LogResponsavel_AcaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV29LogResponsavel_StatusTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV33LogResponsavel_NovoStatusTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV37LogResponsavel_PrazoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV47DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons2 ;
      private wwpbaseobjects.SdtWWPGridState AV7GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV8GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPTransactionContext AV16TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV17TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV18WWPContext ;
   }

   public class wp_corretor__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00R63( IGxContext context ,
                                             int A892LogResponsavel_DemandaCod ,
                                             IGxCollection AV68Codigos ,
                                             int AV68Codigos_Count ,
                                             int AV11LogResponsavel_DemandaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [6] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[LogResponsavel_UsuarioCod], T3.[ContagemResultado_DemandaFM], T3.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[ContratoServicos_PrazoTpDias] AS ContagemResultado_PrzTpDias, T3.[ContagemResultado_PFBFSImp], T3.[ContagemResultado_DataDmn], T3.[ContagemResultado_PrazoMaisDias], T3.[ContagemResultado_PrazoInicialDias], T1.[LogResponsavel_Prazo], T1.[LogResponsavel_NovoStatus], T1.[LogResponsavel_Status], T1.[LogResponsavel_Acao], T1.[LogResponsavel_DataHora], T1.[LogResponsavel_Codigo], COALESCE( T2.[ContratadaUsuario_UsuarioCod], 0) AS ContratadaUsuario_UsuarioCod, T1.[LogResponsavel_Owner], T1.[LogResponsavel_DemandaCod] AS LogResponsavel_DemandaCod";
         sFromString = " FROM ((([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T5.[ContratadaUsuario_UsuarioCod]) AS ContratadaUsuario_UsuarioCod, T6.[LogResponsavel_Codigo] FROM ([ContratadaUsuario] T5 WITH (NOLOCK) INNER JOIN [Contratada] T7 WITH (NOLOCK) ON T7.[Contratada_Codigo] = T5.[ContratadaUsuario_ContratadaCod]),  [LogResponsavel] T6 WITH (NOLOCK) WHERE T5.[ContratadaUsuario_UsuarioCod] = T6.[LogResponsavel_Owner] and T5.[ContratadaUsuario_ContratadaCod] = T7.[Contratada_Codigo] GROUP BY T6.[LogResponsavel_Codigo] ) T2 ON T2.[LogResponsavel_Codigo] = T1.[LogResponsavel_Codigo]) LEFT JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T3.[ContagemResultado_CntSrvCod])";
         sOrderString = "";
         if ( (0==AV68Codigos_Count) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LogResponsavel_DemandaCod] = @AV11LogResponsavel_DemandaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LogResponsavel_DemandaCod] = @AV11LogResponsavel_DemandaCod)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( AV68Codigos_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV68Codigos, "T1.[LogResponsavel_DemandaCod] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV68Codigos, "T1.[LogResponsavel_DemandaCod] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         sOrderString = sOrderString + " ORDER BY T1.[LogResponsavel_DemandaCod], T1.[LogResponsavel_Codigo]";
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_H00R65( IGxContext context ,
                                             int A892LogResponsavel_DemandaCod ,
                                             IGxCollection AV68Codigos ,
                                             int AV68Codigos_Count ,
                                             int AV11LogResponsavel_DemandaCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [1] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([LogResponsavel] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN(T5.[ContratadaUsuario_UsuarioCod]) AS ContratadaUsuario_UsuarioCod, T6.[LogResponsavel_Codigo] FROM ([ContratadaUsuario] T5 WITH (NOLOCK) INNER JOIN [Contratada] T7 WITH (NOLOCK) ON T7.[Contratada_Codigo] = T5.[ContratadaUsuario_ContratadaCod]),  [LogResponsavel] T6 WITH (NOLOCK) WHERE T5.[ContratadaUsuario_UsuarioCod] = T6.[LogResponsavel_Owner] and T5.[ContratadaUsuario_ContratadaCod] = T7.[Contratada_Codigo] GROUP BY T6.[LogResponsavel_Codigo] ) T4 ON T4.[LogResponsavel_Codigo] = T1.[LogResponsavel_Codigo]) LEFT JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[LogResponsavel_DemandaCod]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod])";
         if ( (0==AV68Codigos_Count) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LogResponsavel_DemandaCod] = @AV11LogResponsavel_DemandaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LogResponsavel_DemandaCod] = @AV11LogResponsavel_DemandaCod)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( AV68Codigos_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV68Codigos, "T1.[LogResponsavel_DemandaCod] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV68Codigos, "T1.[LogResponsavel_DemandaCod] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + "";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00R63(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
               case 1 :
                     return conditional_H00R65(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00R66 ;
          prmH00R66 = new Object[] {
          new Object[] {"@AV5Destino",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6Emissor",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00R67 ;
          prmH00R67 = new Object[] {
          } ;
          Object[] prmH00R63 ;
          prmH00R63 = new Object[] {
          new Object[] {"@AV11LogResponsavel_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00R65 ;
          prmH00R65 = new Object[] {
          new Object[] {"@AV11LogResponsavel_DemandaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00R63", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R63,11,0,true,false )
             ,new CursorDef("H00R65", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R65,1,0,true,false )
             ,new CursorDef("H00R66", "SELECT T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_Sigla] FROM ([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV5Destino or T1.[ContratadaUsuario_UsuarioCod] = @AV6Emissor) AND (T2.[Contratada_AreaTrabalhoCod] = @AV18WWPC_1Areatrabalho_codigo) ORDER BY T1.[ContratadaUsuario_ContratadaCod], T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R66,100,0,false,false )
             ,new CursorDef("H00R67", "SELECT T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Ativo], T2.[Pessoa_Nome] AS Usuario_PessoaNom, T1.[Usuario_Codigo] FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Ativo] = 1 ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00R67,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((short[]) buf[12])[0] = rslt.getShort(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((short[]) buf[14])[0] = rslt.getShort(8) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(9) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(9);
                ((String[]) buf[18])[0] = rslt.getString(10, 1) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(10);
                ((String[]) buf[20])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(11);
                ((String[]) buf[22])[0] = rslt.getString(12, 20) ;
                ((DateTime[]) buf[23])[0] = rslt.getGXDateTime(13) ;
                ((long[]) buf[24])[0] = rslt.getLong(14) ;
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((int[]) buf[27])[0] = rslt.getInt(16) ;
                ((int[]) buf[28])[0] = rslt.getInt(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
