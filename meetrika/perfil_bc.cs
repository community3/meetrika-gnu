/*
               File: Perfil_BC
        Description: Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:14:16.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class perfil_bc : GXHttpHandler, IGxSilentTrn
   {
      public perfil_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public perfil_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow032( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey032( ) ;
         standaloneModal( ) ;
         AddRow032( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E11032 */
            E11032 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z3Perfil_Codigo = A3Perfil_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_030( )
      {
         BeforeValidate032( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls032( ) ;
            }
            else
            {
               CheckExtendedTable032( ) ;
               if ( AnyError == 0 )
               {
                  ZM032( 7) ;
               }
               CloseExtendedTableCursors032( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12032( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            while ( AV15GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "Perfil_AreaTrabalhoCod") == 0 )
               {
                  AV11Insert_Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
            }
         }
      }

      protected void E11032( )
      {
         /* After Trn Routine */
      }

      protected void ZM032( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z4Perfil_Nome = A4Perfil_Nome;
            Z275Perfil_Tipo = A275Perfil_Tipo;
            Z328Perfil_GamGUID = A328Perfil_GamGUID;
            Z329Perfil_GamId = A329Perfil_GamId;
            Z276Perfil_Ativo = A276Perfil_Ativo;
            Z7Perfil_AreaTrabalhoCod = A7Perfil_AreaTrabalhoCod;
         }
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z8Perfil_AreaTrabalhoDes = A8Perfil_AreaTrabalhoDes;
         }
         if ( GX_JID == -6 )
         {
            Z3Perfil_Codigo = A3Perfil_Codigo;
            Z4Perfil_Nome = A4Perfil_Nome;
            Z275Perfil_Tipo = A275Perfil_Tipo;
            Z328Perfil_GamGUID = A328Perfil_GamGUID;
            Z329Perfil_GamId = A329Perfil_GamId;
            Z276Perfil_Ativo = A276Perfil_Ativo;
            Z7Perfil_AreaTrabalhoCod = A7Perfil_AreaTrabalhoCod;
            Z8Perfil_AreaTrabalhoDes = A8Perfil_AreaTrabalhoDes;
         }
      }

      protected void standaloneNotModal( )
      {
         AV14Pgmname = "Perfil_BC";
         Gx_BScreen = 0;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A276Perfil_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A276Perfil_Ativo = true;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A7Perfil_AreaTrabalhoCod) && ( Gx_BScreen == 0 ) )
         {
            A7Perfil_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor BC00034 */
            pr_default.execute(2, new Object[] {A7Perfil_AreaTrabalhoCod});
            A8Perfil_AreaTrabalhoDes = BC00034_A8Perfil_AreaTrabalhoDes[0];
            n8Perfil_AreaTrabalhoDes = BC00034_n8Perfil_AreaTrabalhoDes[0];
            pr_default.close(2);
         }
      }

      protected void Load032( )
      {
         /* Using cursor BC00035 */
         pr_default.execute(3, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound2 = 1;
            A4Perfil_Nome = BC00035_A4Perfil_Nome[0];
            A8Perfil_AreaTrabalhoDes = BC00035_A8Perfil_AreaTrabalhoDes[0];
            n8Perfil_AreaTrabalhoDes = BC00035_n8Perfil_AreaTrabalhoDes[0];
            A275Perfil_Tipo = BC00035_A275Perfil_Tipo[0];
            A328Perfil_GamGUID = BC00035_A328Perfil_GamGUID[0];
            A329Perfil_GamId = BC00035_A329Perfil_GamId[0];
            A276Perfil_Ativo = BC00035_A276Perfil_Ativo[0];
            A7Perfil_AreaTrabalhoCod = BC00035_A7Perfil_AreaTrabalhoCod[0];
            ZM032( -6) ;
         }
         pr_default.close(3);
         OnLoadActions032( ) ;
      }

      protected void OnLoadActions032( )
      {
      }

      protected void CheckExtendedTable032( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00034 */
         pr_default.execute(2, new Object[] {A7Perfil_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Perfil Area Trabalho'.", "ForeignKeyNotFound", 1, "PERFIL_AREATRABALHOCOD");
            AnyError = 1;
         }
         A8Perfil_AreaTrabalhoDes = BC00034_A8Perfil_AreaTrabalhoDes[0];
         n8Perfil_AreaTrabalhoDes = BC00034_n8Perfil_AreaTrabalhoDes[0];
         pr_default.close(2);
         if ( ! ( ( A275Perfil_Tipo == 0 ) || ( A275Perfil_Tipo == 1 ) || ( A275Perfil_Tipo == 2 ) || ( A275Perfil_Tipo == 3 ) || ( A275Perfil_Tipo == 4 ) || ( A275Perfil_Tipo == 5 ) || ( A275Perfil_Tipo == 6 ) || ( A275Perfil_Tipo == 10 ) || ( A275Perfil_Tipo == 99 ) ) )
         {
            GX_msglist.addItem("Campo Tipo do Perfil fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors032( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey032( )
      {
         /* Using cursor BC00036 */
         pr_default.execute(4, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound2 = 1;
         }
         else
         {
            RcdFound2 = 0;
         }
         pr_default.close(4);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00033 */
         pr_default.execute(1, new Object[] {A3Perfil_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM032( 6) ;
            RcdFound2 = 1;
            A3Perfil_Codigo = BC00033_A3Perfil_Codigo[0];
            A4Perfil_Nome = BC00033_A4Perfil_Nome[0];
            A275Perfil_Tipo = BC00033_A275Perfil_Tipo[0];
            A328Perfil_GamGUID = BC00033_A328Perfil_GamGUID[0];
            A329Perfil_GamId = BC00033_A329Perfil_GamId[0];
            A276Perfil_Ativo = BC00033_A276Perfil_Ativo[0];
            A7Perfil_AreaTrabalhoCod = BC00033_A7Perfil_AreaTrabalhoCod[0];
            Z3Perfil_Codigo = A3Perfil_Codigo;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load032( ) ;
            if ( AnyError == 1 )
            {
               RcdFound2 = 0;
               InitializeNonKey032( ) ;
            }
            Gx_mode = sMode2;
         }
         else
         {
            RcdFound2 = 0;
            InitializeNonKey032( ) ;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode2;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey032( ) ;
         if ( RcdFound2 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_030( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency032( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00032 */
            pr_default.execute(0, new Object[] {A3Perfil_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Perfil"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z4Perfil_Nome, BC00032_A4Perfil_Nome[0]) != 0 ) || ( Z275Perfil_Tipo != BC00032_A275Perfil_Tipo[0] ) || ( StringUtil.StrCmp(Z328Perfil_GamGUID, BC00032_A328Perfil_GamGUID[0]) != 0 ) || ( Z329Perfil_GamId != BC00032_A329Perfil_GamId[0] ) || ( Z276Perfil_Ativo != BC00032_A276Perfil_Ativo[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z7Perfil_AreaTrabalhoCod != BC00032_A7Perfil_AreaTrabalhoCod[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Perfil"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert032( )
      {
         BeforeValidate032( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable032( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM032( 0) ;
            CheckOptimisticConcurrency032( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm032( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert032( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00037 */
                     pr_default.execute(5, new Object[] {A4Perfil_Nome, A275Perfil_Tipo, A328Perfil_GamGUID, A329Perfil_GamId, A276Perfil_Ativo, A7Perfil_AreaTrabalhoCod});
                     A3Perfil_Codigo = BC00037_A3Perfil_Codigo[0];
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("Perfil") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load032( ) ;
            }
            EndLevel032( ) ;
         }
         CloseExtendedTableCursors032( ) ;
      }

      protected void Update032( )
      {
         BeforeValidate032( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable032( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency032( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm032( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate032( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00038 */
                     pr_default.execute(6, new Object[] {A4Perfil_Nome, A275Perfil_Tipo, A328Perfil_GamGUID, A329Perfil_GamId, A276Perfil_Ativo, A7Perfil_AreaTrabalhoCod, A3Perfil_Codigo});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Perfil") ;
                     if ( (pr_default.getStatus(6) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Perfil"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate032( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel032( ) ;
         }
         CloseExtendedTableCursors032( ) ;
      }

      protected void DeferredUpdate032( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate032( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency032( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls032( ) ;
            AfterConfirm032( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete032( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC00039 */
                  pr_default.execute(7, new Object[] {A3Perfil_Codigo});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("Perfil") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode2 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel032( ) ;
         Gx_mode = sMode2;
      }

      protected void OnDeleteControls032( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000310 */
            pr_default.execute(8, new Object[] {A7Perfil_AreaTrabalhoCod});
            A8Perfil_AreaTrabalhoDes = BC000310_A8Perfil_AreaTrabalhoDes[0];
            n8Perfil_AreaTrabalhoDes = BC000310_n8Perfil_AreaTrabalhoDes[0];
            pr_default.close(8);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC000311 */
            pr_default.execute(9, new Object[] {A3Perfil_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Menu Perfil"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC000312 */
            pr_default.execute(10, new Object[] {A3Perfil_Codigo});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Usuario x Perfil"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
         }
      }

      protected void EndLevel032( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete032( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart032( )
      {
         /* Scan By routine */
         /* Using cursor BC000313 */
         pr_default.execute(11, new Object[] {A3Perfil_Codigo});
         RcdFound2 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound2 = 1;
            A3Perfil_Codigo = BC000313_A3Perfil_Codigo[0];
            A4Perfil_Nome = BC000313_A4Perfil_Nome[0];
            A8Perfil_AreaTrabalhoDes = BC000313_A8Perfil_AreaTrabalhoDes[0];
            n8Perfil_AreaTrabalhoDes = BC000313_n8Perfil_AreaTrabalhoDes[0];
            A275Perfil_Tipo = BC000313_A275Perfil_Tipo[0];
            A328Perfil_GamGUID = BC000313_A328Perfil_GamGUID[0];
            A329Perfil_GamId = BC000313_A329Perfil_GamId[0];
            A276Perfil_Ativo = BC000313_A276Perfil_Ativo[0];
            A7Perfil_AreaTrabalhoCod = BC000313_A7Perfil_AreaTrabalhoCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext032( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound2 = 0;
         ScanKeyLoad032( ) ;
      }

      protected void ScanKeyLoad032( )
      {
         sMode2 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound2 = 1;
            A3Perfil_Codigo = BC000313_A3Perfil_Codigo[0];
            A4Perfil_Nome = BC000313_A4Perfil_Nome[0];
            A8Perfil_AreaTrabalhoDes = BC000313_A8Perfil_AreaTrabalhoDes[0];
            n8Perfil_AreaTrabalhoDes = BC000313_n8Perfil_AreaTrabalhoDes[0];
            A275Perfil_Tipo = BC000313_A275Perfil_Tipo[0];
            A328Perfil_GamGUID = BC000313_A328Perfil_GamGUID[0];
            A329Perfil_GamId = BC000313_A329Perfil_GamId[0];
            A276Perfil_Ativo = BC000313_A276Perfil_Ativo[0];
            A7Perfil_AreaTrabalhoCod = BC000313_A7Perfil_AreaTrabalhoCod[0];
         }
         Gx_mode = sMode2;
      }

      protected void ScanKeyEnd032( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm032( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert032( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate032( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete032( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete032( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate032( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes032( )
      {
      }

      protected void AddRow032( )
      {
         VarsToRow2( bcPerfil) ;
      }

      protected void ReadRow032( )
      {
         RowToVars2( bcPerfil, 1) ;
      }

      protected void InitializeNonKey032( )
      {
         A4Perfil_Nome = "";
         A8Perfil_AreaTrabalhoDes = "";
         n8Perfil_AreaTrabalhoDes = false;
         A275Perfil_Tipo = 0;
         A328Perfil_GamGUID = "";
         A329Perfil_GamId = 0;
         A7Perfil_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A276Perfil_Ativo = true;
         Z4Perfil_Nome = "";
         Z275Perfil_Tipo = 0;
         Z328Perfil_GamGUID = "";
         Z329Perfil_GamId = 0;
         Z276Perfil_Ativo = false;
         Z7Perfil_AreaTrabalhoCod = 0;
      }

      protected void InitAll032( )
      {
         A3Perfil_Codigo = 0;
         InitializeNonKey032( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A276Perfil_Ativo = i276Perfil_Ativo;
         A7Perfil_AreaTrabalhoCod = i7Perfil_AreaTrabalhoCod;
      }

      public void VarsToRow2( SdtPerfil obj2 )
      {
         obj2.gxTpr_Mode = Gx_mode;
         obj2.gxTpr_Perfil_nome = A4Perfil_Nome;
         obj2.gxTpr_Perfil_areatrabalhodes = A8Perfil_AreaTrabalhoDes;
         obj2.gxTpr_Perfil_tipo = A275Perfil_Tipo;
         obj2.gxTpr_Perfil_gamguid = A328Perfil_GamGUID;
         obj2.gxTpr_Perfil_gamid = A329Perfil_GamId;
         obj2.gxTpr_Perfil_areatrabalhocod = A7Perfil_AreaTrabalhoCod;
         obj2.gxTpr_Perfil_ativo = A276Perfil_Ativo;
         obj2.gxTpr_Perfil_codigo = A3Perfil_Codigo;
         obj2.gxTpr_Perfil_codigo_Z = Z3Perfil_Codigo;
         obj2.gxTpr_Perfil_nome_Z = Z4Perfil_Nome;
         obj2.gxTpr_Perfil_areatrabalhocod_Z = Z7Perfil_AreaTrabalhoCod;
         obj2.gxTpr_Perfil_areatrabalhodes_Z = Z8Perfil_AreaTrabalhoDes;
         obj2.gxTpr_Perfil_tipo_Z = Z275Perfil_Tipo;
         obj2.gxTpr_Perfil_gamguid_Z = Z328Perfil_GamGUID;
         obj2.gxTpr_Perfil_gamid_Z = Z329Perfil_GamId;
         obj2.gxTpr_Perfil_ativo_Z = Z276Perfil_Ativo;
         obj2.gxTpr_Perfil_areatrabalhodes_N = (short)(Convert.ToInt16(n8Perfil_AreaTrabalhoDes));
         obj2.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow2( SdtPerfil obj2 )
      {
         obj2.gxTpr_Perfil_codigo = A3Perfil_Codigo;
         return  ;
      }

      public void RowToVars2( SdtPerfil obj2 ,
                              int forceLoad )
      {
         Gx_mode = obj2.gxTpr_Mode;
         A4Perfil_Nome = obj2.gxTpr_Perfil_nome;
         A8Perfil_AreaTrabalhoDes = obj2.gxTpr_Perfil_areatrabalhodes;
         n8Perfil_AreaTrabalhoDes = false;
         A275Perfil_Tipo = obj2.gxTpr_Perfil_tipo;
         A328Perfil_GamGUID = obj2.gxTpr_Perfil_gamguid;
         A329Perfil_GamId = obj2.gxTpr_Perfil_gamid;
         A7Perfil_AreaTrabalhoCod = obj2.gxTpr_Perfil_areatrabalhocod;
         A276Perfil_Ativo = obj2.gxTpr_Perfil_ativo;
         A3Perfil_Codigo = obj2.gxTpr_Perfil_codigo;
         Z3Perfil_Codigo = obj2.gxTpr_Perfil_codigo_Z;
         Z4Perfil_Nome = obj2.gxTpr_Perfil_nome_Z;
         Z7Perfil_AreaTrabalhoCod = obj2.gxTpr_Perfil_areatrabalhocod_Z;
         Z8Perfil_AreaTrabalhoDes = obj2.gxTpr_Perfil_areatrabalhodes_Z;
         Z275Perfil_Tipo = obj2.gxTpr_Perfil_tipo_Z;
         Z328Perfil_GamGUID = obj2.gxTpr_Perfil_gamguid_Z;
         Z329Perfil_GamId = obj2.gxTpr_Perfil_gamid_Z;
         Z276Perfil_Ativo = obj2.gxTpr_Perfil_ativo_Z;
         n8Perfil_AreaTrabalhoDes = (bool)(Convert.ToBoolean(obj2.gxTpr_Perfil_areatrabalhodes_N));
         Gx_mode = obj2.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A3Perfil_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey032( ) ;
         ScanKeyStart032( ) ;
         if ( RcdFound2 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z3Perfil_Codigo = A3Perfil_Codigo;
         }
         ZM032( -6) ;
         OnLoadActions032( ) ;
         AddRow032( ) ;
         ScanKeyEnd032( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars2( bcPerfil, 0) ;
         ScanKeyStart032( ) ;
         if ( RcdFound2 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z3Perfil_Codigo = A3Perfil_Codigo;
         }
         ZM032( -6) ;
         OnLoadActions032( ) ;
         AddRow032( ) ;
         ScanKeyEnd032( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars2( bcPerfil, 0) ;
         nKeyPressed = 1;
         GetKey032( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert032( ) ;
         }
         else
         {
            if ( RcdFound2 == 1 )
            {
               if ( A3Perfil_Codigo != Z3Perfil_Codigo )
               {
                  A3Perfil_Codigo = Z3Perfil_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update032( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A3Perfil_Codigo != Z3Perfil_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert032( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert032( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow2( bcPerfil) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars2( bcPerfil, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey032( ) ;
         if ( RcdFound2 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A3Perfil_Codigo != Z3Perfil_Codigo )
            {
               A3Perfil_Codigo = Z3Perfil_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A3Perfil_Codigo != Z3Perfil_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(8);
         context.RollbackDataStores( "Perfil_BC");
         VarsToRow2( bcPerfil) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcPerfil.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcPerfil.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcPerfil )
         {
            bcPerfil = (SdtPerfil)(sdt);
            if ( StringUtil.StrCmp(bcPerfil.gxTpr_Mode, "") == 0 )
            {
               bcPerfil.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow2( bcPerfil) ;
            }
            else
            {
               RowToVars2( bcPerfil, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcPerfil.gxTpr_Mode, "") == 0 )
            {
               bcPerfil.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars2( bcPerfil, 1) ;
         return  ;
      }

      public SdtPerfil Perfil_BC
      {
         get {
            return bcPerfil ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(8);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV14Pgmname = "";
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z4Perfil_Nome = "";
         A4Perfil_Nome = "";
         Z328Perfil_GamGUID = "";
         A328Perfil_GamGUID = "";
         Z8Perfil_AreaTrabalhoDes = "";
         A8Perfil_AreaTrabalhoDes = "";
         BC00034_A8Perfil_AreaTrabalhoDes = new String[] {""} ;
         BC00034_n8Perfil_AreaTrabalhoDes = new bool[] {false} ;
         BC00035_A3Perfil_Codigo = new int[1] ;
         BC00035_A4Perfil_Nome = new String[] {""} ;
         BC00035_A8Perfil_AreaTrabalhoDes = new String[] {""} ;
         BC00035_n8Perfil_AreaTrabalhoDes = new bool[] {false} ;
         BC00035_A275Perfil_Tipo = new short[1] ;
         BC00035_A328Perfil_GamGUID = new String[] {""} ;
         BC00035_A329Perfil_GamId = new long[1] ;
         BC00035_A276Perfil_Ativo = new bool[] {false} ;
         BC00035_A7Perfil_AreaTrabalhoCod = new int[1] ;
         BC00036_A3Perfil_Codigo = new int[1] ;
         BC00033_A3Perfil_Codigo = new int[1] ;
         BC00033_A4Perfil_Nome = new String[] {""} ;
         BC00033_A275Perfil_Tipo = new short[1] ;
         BC00033_A328Perfil_GamGUID = new String[] {""} ;
         BC00033_A329Perfil_GamId = new long[1] ;
         BC00033_A276Perfil_Ativo = new bool[] {false} ;
         BC00033_A7Perfil_AreaTrabalhoCod = new int[1] ;
         sMode2 = "";
         BC00032_A3Perfil_Codigo = new int[1] ;
         BC00032_A4Perfil_Nome = new String[] {""} ;
         BC00032_A275Perfil_Tipo = new short[1] ;
         BC00032_A328Perfil_GamGUID = new String[] {""} ;
         BC00032_A329Perfil_GamId = new long[1] ;
         BC00032_A276Perfil_Ativo = new bool[] {false} ;
         BC00032_A7Perfil_AreaTrabalhoCod = new int[1] ;
         BC00037_A3Perfil_Codigo = new int[1] ;
         BC000310_A8Perfil_AreaTrabalhoDes = new String[] {""} ;
         BC000310_n8Perfil_AreaTrabalhoDes = new bool[] {false} ;
         BC000311_A277Menu_Codigo = new int[1] ;
         BC000311_A3Perfil_Codigo = new int[1] ;
         BC000312_A1Usuario_Codigo = new int[1] ;
         BC000312_A3Perfil_Codigo = new int[1] ;
         BC000313_A3Perfil_Codigo = new int[1] ;
         BC000313_A4Perfil_Nome = new String[] {""} ;
         BC000313_A8Perfil_AreaTrabalhoDes = new String[] {""} ;
         BC000313_n8Perfil_AreaTrabalhoDes = new bool[] {false} ;
         BC000313_A275Perfil_Tipo = new short[1] ;
         BC000313_A328Perfil_GamGUID = new String[] {""} ;
         BC000313_A329Perfil_GamId = new long[1] ;
         BC000313_A276Perfil_Ativo = new bool[] {false} ;
         BC000313_A7Perfil_AreaTrabalhoCod = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.perfil_bc__default(),
            new Object[][] {
                new Object[] {
               BC00032_A3Perfil_Codigo, BC00032_A4Perfil_Nome, BC00032_A275Perfil_Tipo, BC00032_A328Perfil_GamGUID, BC00032_A329Perfil_GamId, BC00032_A276Perfil_Ativo, BC00032_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               BC00033_A3Perfil_Codigo, BC00033_A4Perfil_Nome, BC00033_A275Perfil_Tipo, BC00033_A328Perfil_GamGUID, BC00033_A329Perfil_GamId, BC00033_A276Perfil_Ativo, BC00033_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               BC00034_A8Perfil_AreaTrabalhoDes, BC00034_n8Perfil_AreaTrabalhoDes
               }
               , new Object[] {
               BC00035_A3Perfil_Codigo, BC00035_A4Perfil_Nome, BC00035_A8Perfil_AreaTrabalhoDes, BC00035_n8Perfil_AreaTrabalhoDes, BC00035_A275Perfil_Tipo, BC00035_A328Perfil_GamGUID, BC00035_A329Perfil_GamId, BC00035_A276Perfil_Ativo, BC00035_A7Perfil_AreaTrabalhoCod
               }
               , new Object[] {
               BC00036_A3Perfil_Codigo
               }
               , new Object[] {
               BC00037_A3Perfil_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000310_A8Perfil_AreaTrabalhoDes, BC000310_n8Perfil_AreaTrabalhoDes
               }
               , new Object[] {
               BC000311_A277Menu_Codigo, BC000311_A3Perfil_Codigo
               }
               , new Object[] {
               BC000312_A1Usuario_Codigo, BC000312_A3Perfil_Codigo
               }
               , new Object[] {
               BC000313_A3Perfil_Codigo, BC000313_A4Perfil_Nome, BC000313_A8Perfil_AreaTrabalhoDes, BC000313_n8Perfil_AreaTrabalhoDes, BC000313_A275Perfil_Tipo, BC000313_A328Perfil_GamGUID, BC000313_A329Perfil_GamId, BC000313_A276Perfil_Ativo, BC000313_A7Perfil_AreaTrabalhoCod
               }
            }
         );
         Z276Perfil_Ativo = true;
         A276Perfil_Ativo = true;
         i276Perfil_Ativo = true;
         AV14Pgmname = "Perfil_BC";
         Z7Perfil_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         A7Perfil_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         i7Perfil_AreaTrabalhoCod = AV8WWPContext.gxTpr_Areatrabalho_codigo;
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E12032 */
         E12032 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z275Perfil_Tipo ;
      private short A275Perfil_Tipo ;
      private short Gx_BScreen ;
      private short RcdFound2 ;
      private int trnEnded ;
      private int Z3Perfil_Codigo ;
      private int A3Perfil_Codigo ;
      private int AV15GXV1 ;
      private int AV11Insert_Perfil_AreaTrabalhoCod ;
      private int Z7Perfil_AreaTrabalhoCod ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int i7Perfil_AreaTrabalhoCod ;
      private long Z329Perfil_GamId ;
      private long A329Perfil_GamId ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV14Pgmname ;
      private String Z4Perfil_Nome ;
      private String A4Perfil_Nome ;
      private String Z328Perfil_GamGUID ;
      private String A328Perfil_GamGUID ;
      private String sMode2 ;
      private bool Z276Perfil_Ativo ;
      private bool A276Perfil_Ativo ;
      private bool n8Perfil_AreaTrabalhoDes ;
      private bool Gx_longc ;
      private bool i276Perfil_Ativo ;
      private String Z8Perfil_AreaTrabalhoDes ;
      private String A8Perfil_AreaTrabalhoDes ;
      private IGxSession AV10WebSession ;
      private SdtPerfil bcPerfil ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC00034_A8Perfil_AreaTrabalhoDes ;
      private bool[] BC00034_n8Perfil_AreaTrabalhoDes ;
      private int[] BC00035_A3Perfil_Codigo ;
      private String[] BC00035_A4Perfil_Nome ;
      private String[] BC00035_A8Perfil_AreaTrabalhoDes ;
      private bool[] BC00035_n8Perfil_AreaTrabalhoDes ;
      private short[] BC00035_A275Perfil_Tipo ;
      private String[] BC00035_A328Perfil_GamGUID ;
      private long[] BC00035_A329Perfil_GamId ;
      private bool[] BC00035_A276Perfil_Ativo ;
      private int[] BC00035_A7Perfil_AreaTrabalhoCod ;
      private int[] BC00036_A3Perfil_Codigo ;
      private int[] BC00033_A3Perfil_Codigo ;
      private String[] BC00033_A4Perfil_Nome ;
      private short[] BC00033_A275Perfil_Tipo ;
      private String[] BC00033_A328Perfil_GamGUID ;
      private long[] BC00033_A329Perfil_GamId ;
      private bool[] BC00033_A276Perfil_Ativo ;
      private int[] BC00033_A7Perfil_AreaTrabalhoCod ;
      private int[] BC00032_A3Perfil_Codigo ;
      private String[] BC00032_A4Perfil_Nome ;
      private short[] BC00032_A275Perfil_Tipo ;
      private String[] BC00032_A328Perfil_GamGUID ;
      private long[] BC00032_A329Perfil_GamId ;
      private bool[] BC00032_A276Perfil_Ativo ;
      private int[] BC00032_A7Perfil_AreaTrabalhoCod ;
      private int[] BC00037_A3Perfil_Codigo ;
      private String[] BC000310_A8Perfil_AreaTrabalhoDes ;
      private bool[] BC000310_n8Perfil_AreaTrabalhoDes ;
      private int[] BC000311_A277Menu_Codigo ;
      private int[] BC000311_A3Perfil_Codigo ;
      private int[] BC000312_A1Usuario_Codigo ;
      private int[] BC000312_A3Perfil_Codigo ;
      private int[] BC000313_A3Perfil_Codigo ;
      private String[] BC000313_A4Perfil_Nome ;
      private String[] BC000313_A8Perfil_AreaTrabalhoDes ;
      private bool[] BC000313_n8Perfil_AreaTrabalhoDes ;
      private short[] BC000313_A275Perfil_Tipo ;
      private String[] BC000313_A328Perfil_GamGUID ;
      private long[] BC000313_A329Perfil_GamId ;
      private bool[] BC000313_A276Perfil_Ativo ;
      private int[] BC000313_A7Perfil_AreaTrabalhoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class perfil_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00035 ;
          prmBC00035 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00034 ;
          prmBC00034 = new Object[] {
          new Object[] {"@Perfil_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00036 ;
          prmBC00036 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00033 ;
          prmBC00033 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00032 ;
          prmBC00032 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00037 ;
          prmBC00037 = new Object[] {
          new Object[] {"@Perfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Perfil_Tipo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Perfil_GamGUID",SqlDbType.Char,40,0} ,
          new Object[] {"@Perfil_GamId",SqlDbType.Decimal,12,0} ,
          new Object[] {"@Perfil_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Perfil_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00038 ;
          prmBC00038 = new Object[] {
          new Object[] {"@Perfil_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Perfil_Tipo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Perfil_GamGUID",SqlDbType.Char,40,0} ,
          new Object[] {"@Perfil_GamId",SqlDbType.Decimal,12,0} ,
          new Object[] {"@Perfil_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Perfil_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00039 ;
          prmBC00039 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000310 ;
          prmBC000310 = new Object[] {
          new Object[] {"@Perfil_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000311 ;
          prmBC000311 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000312 ;
          prmBC000312 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC000313 ;
          prmBC000313 = new Object[] {
          new Object[] {"@Perfil_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00032", "SELECT [Perfil_Codigo], [Perfil_Nome], [Perfil_Tipo], [Perfil_GamGUID], [Perfil_GamId], [Perfil_Ativo], [Perfil_AreaTrabalhoCod] AS Perfil_AreaTrabalhoCod FROM [Perfil] WITH (UPDLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00032,1,0,true,false )
             ,new CursorDef("BC00033", "SELECT [Perfil_Codigo], [Perfil_Nome], [Perfil_Tipo], [Perfil_GamGUID], [Perfil_GamId], [Perfil_Ativo], [Perfil_AreaTrabalhoCod] AS Perfil_AreaTrabalhoCod FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00033,1,0,true,false )
             ,new CursorDef("BC00034", "SELECT [AreaTrabalho_Descricao] AS Perfil_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Perfil_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00034,1,0,true,false )
             ,new CursorDef("BC00035", "SELECT TM1.[Perfil_Codigo], TM1.[Perfil_Nome], T2.[AreaTrabalho_Descricao] AS Perfil_AreaTrabalhoDes, TM1.[Perfil_Tipo], TM1.[Perfil_GamGUID], TM1.[Perfil_GamId], TM1.[Perfil_Ativo], TM1.[Perfil_AreaTrabalhoCod] AS Perfil_AreaTrabalhoCod FROM ([Perfil] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Perfil_AreaTrabalhoCod]) WHERE TM1.[Perfil_Codigo] = @Perfil_Codigo ORDER BY TM1.[Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00035,100,0,true,false )
             ,new CursorDef("BC00036", "SELECT [Perfil_Codigo] FROM [Perfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00036,1,0,true,false )
             ,new CursorDef("BC00037", "INSERT INTO [Perfil]([Perfil_Nome], [Perfil_Tipo], [Perfil_GamGUID], [Perfil_GamId], [Perfil_Ativo], [Perfil_AreaTrabalhoCod]) VALUES(@Perfil_Nome, @Perfil_Tipo, @Perfil_GamGUID, @Perfil_GamId, @Perfil_Ativo, @Perfil_AreaTrabalhoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC00037)
             ,new CursorDef("BC00038", "UPDATE [Perfil] SET [Perfil_Nome]=@Perfil_Nome, [Perfil_Tipo]=@Perfil_Tipo, [Perfil_GamGUID]=@Perfil_GamGUID, [Perfil_GamId]=@Perfil_GamId, [Perfil_Ativo]=@Perfil_Ativo, [Perfil_AreaTrabalhoCod]=@Perfil_AreaTrabalhoCod  WHERE [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK,prmBC00038)
             ,new CursorDef("BC00039", "DELETE FROM [Perfil]  WHERE [Perfil_Codigo] = @Perfil_Codigo", GxErrorMask.GX_NOMASK,prmBC00039)
             ,new CursorDef("BC000310", "SELECT [AreaTrabalho_Descricao] AS Perfil_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @Perfil_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000310,1,0,true,false )
             ,new CursorDef("BC000311", "SELECT TOP 1 [Menu_Codigo], [Perfil_Codigo] FROM [MenuPerfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000311,1,0,true,true )
             ,new CursorDef("BC000312", "SELECT TOP 1 [Usuario_Codigo], [Perfil_Codigo] FROM [UsuarioPerfil] WITH (NOLOCK) WHERE [Perfil_Codigo] = @Perfil_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000312,1,0,true,true )
             ,new CursorDef("BC000313", "SELECT TM1.[Perfil_Codigo], TM1.[Perfil_Nome], T2.[AreaTrabalho_Descricao] AS Perfil_AreaTrabalhoDes, TM1.[Perfil_Tipo], TM1.[Perfil_GamGUID], TM1.[Perfil_GamId], TM1.[Perfil_Ativo], TM1.[Perfil_AreaTrabalhoCod] AS Perfil_AreaTrabalhoCod FROM ([Perfil] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[Perfil_AreaTrabalhoCod]) WHERE TM1.[Perfil_Codigo] = @Perfil_Codigo ORDER BY TM1.[Perfil_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000313,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 40) ;
                ((long[]) buf[4])[0] = rslt.getLong(5) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 40) ;
                ((long[]) buf[4])[0] = rslt.getLong(5) ;
                ((bool[]) buf[5])[0] = rslt.getBool(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 40) ;
                ((long[]) buf[6])[0] = rslt.getLong(6) ;
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 40) ;
                ((long[]) buf[6])[0] = rslt.getLong(6) ;
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (long)parms[3]);
                stmt.SetParameter(5, (bool)parms[4]);
                stmt.SetParameter(6, (int)parms[5]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (long)parms[3]);
                stmt.SetParameter(5, (bool)parms[4]);
                stmt.SetParameter(6, (int)parms[5]);
                stmt.SetParameter(7, (int)parms[6]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
