/*
               File: CatalogoSutentacao
        Description: Cat�logo Sustenta��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:29:42.77
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class catalogosutentacao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7CatalogoSutentacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7CatalogoSutentacao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCATALOGOSUTENTACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7CatalogoSutentacao_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Cat�logo Sustenta��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtCatalogoSutentacao_Descricao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public catalogosutentacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public catalogosutentacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_CatalogoSutentacao_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7CatalogoSutentacao_Codigo = aP1_CatalogoSutentacao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4C192( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4C192e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCatalogoSutentacao_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0, ",", "")), ((edtCatalogoSutentacao_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1750CatalogoSutentacao_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1750CatalogoSutentacao_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCatalogoSutentacao_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtCatalogoSutentacao_Codigo_Visible, edtCatalogoSutentacao_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_CatalogoSutentacao.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4C192( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4C192( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4C192e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_36_4C192( true) ;
         }
         return  ;
      }

      protected void wb_table3_36_4C192e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4C192e( true) ;
         }
         else
         {
            wb_table1_2_4C192e( false) ;
         }
      }

      protected void wb_table3_36_4C192( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_CatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_CatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_CatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_4C192e( true) ;
         }
         else
         {
            wb_table3_36_4C192e( false) ;
         }
      }

      protected void wb_table2_5_4C192( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_4C192( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_4C192e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4C192e( true) ;
         }
         else
         {
            wb_table2_5_4C192e( false) ;
         }
      }

      protected void wb_table4_13_4C192( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcatalogosutentacao_descricao_Internalname, "Descri��o", "", "", lblTextblockcatalogosutentacao_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCatalogoSutentacao_Descricao_Internalname, A1752CatalogoSutentacao_Descricao, StringUtil.RTrim( context.localUtil.Format( A1752CatalogoSutentacao_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCatalogoSutentacao_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtCatalogoSutentacao_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_CatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcatalogosutentacao_atividade_Internalname, "Atividade", "", "", lblTextblockcatalogosutentacao_atividade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCatalogoSutentacao_Atividade_Internalname, A1753CatalogoSutentacao_Atividade, StringUtil.RTrim( context.localUtil.Format( A1753CatalogoSutentacao_Atividade, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCatalogoSutentacao_Atividade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtCatalogoSutentacao_Atividade_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_CatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcatalogosutentacao_prazo_dias_Internalname, "Prazo (Dias Corridos)", "", "", lblTextblockcatalogosutentacao_prazo_dias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCatalogoSutentacao_Prazo_Dias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1754CatalogoSutentacao_Prazo_Dias), 4, 0, ",", "")), ((edtCatalogoSutentacao_Prazo_Dias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1754CatalogoSutentacao_Prazo_Dias), "ZZZ9")) : context.localUtil.Format( (decimal)(A1754CatalogoSutentacao_Prazo_Dias), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCatalogoSutentacao_Prazo_Dias_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtCatalogoSutentacao_Prazo_Dias_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcatalogosutentacao_ust_Internalname, "Story Points", "", "", lblTextblockcatalogosutentacao_ust_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCatalogoSutentacao_UST_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1755CatalogoSutentacao_UST), 4, 0, ",", "")), ((edtCatalogoSutentacao_UST_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1755CatalogoSutentacao_UST), "ZZZ9")) : context.localUtil.Format( (decimal)(A1755CatalogoSutentacao_UST), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCatalogoSutentacao_UST_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtCatalogoSutentacao_UST_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_4C192e( true) ;
         }
         else
         {
            wb_table4_13_4C192e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114C2 */
         E114C2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1752CatalogoSutentacao_Descricao = StringUtil.Upper( cgiGet( edtCatalogoSutentacao_Descricao_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1752CatalogoSutentacao_Descricao", A1752CatalogoSutentacao_Descricao);
               A1753CatalogoSutentacao_Atividade = StringUtil.Upper( cgiGet( edtCatalogoSutentacao_Atividade_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1753CatalogoSutentacao_Atividade", A1753CatalogoSutentacao_Atividade);
               if ( ( ( context.localUtil.CToN( cgiGet( edtCatalogoSutentacao_Prazo_Dias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtCatalogoSutentacao_Prazo_Dias_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CATALOGOSUTENTACAO_PRAZO_DIAS");
                  AnyError = 1;
                  GX_FocusControl = edtCatalogoSutentacao_Prazo_Dias_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1754CatalogoSutentacao_Prazo_Dias = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1754CatalogoSutentacao_Prazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1754CatalogoSutentacao_Prazo_Dias), 4, 0)));
               }
               else
               {
                  A1754CatalogoSutentacao_Prazo_Dias = (short)(context.localUtil.CToN( cgiGet( edtCatalogoSutentacao_Prazo_Dias_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1754CatalogoSutentacao_Prazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1754CatalogoSutentacao_Prazo_Dias), 4, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtCatalogoSutentacao_UST_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtCatalogoSutentacao_UST_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CATALOGOSUTENTACAO_UST");
                  AnyError = 1;
                  GX_FocusControl = edtCatalogoSutentacao_UST_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1755CatalogoSutentacao_UST = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1755CatalogoSutentacao_UST", StringUtil.LTrim( StringUtil.Str( (decimal)(A1755CatalogoSutentacao_UST), 4, 0)));
               }
               else
               {
                  A1755CatalogoSutentacao_UST = (short)(context.localUtil.CToN( cgiGet( edtCatalogoSutentacao_UST_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1755CatalogoSutentacao_UST", StringUtil.LTrim( StringUtil.Str( (decimal)(A1755CatalogoSutentacao_UST), 4, 0)));
               }
               A1750CatalogoSutentacao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCatalogoSutentacao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
               /* Read saved values. */
               Z1750CatalogoSutentacao_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1750CatalogoSutentacao_Codigo"), ",", "."));
               Z1752CatalogoSutentacao_Descricao = cgiGet( "Z1752CatalogoSutentacao_Descricao");
               Z1753CatalogoSutentacao_Atividade = cgiGet( "Z1753CatalogoSutentacao_Atividade");
               Z1754CatalogoSutentacao_Prazo_Dias = (short)(context.localUtil.CToN( cgiGet( "Z1754CatalogoSutentacao_Prazo_Dias"), ",", "."));
               Z1755CatalogoSutentacao_UST = (short)(context.localUtil.CToN( cgiGet( "Z1755CatalogoSutentacao_UST"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7CatalogoSutentacao_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCATALOGOSUTENTACAO_CODIGO"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "CatalogoSutentacao";
               A1750CatalogoSutentacao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCatalogoSutentacao_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1750CatalogoSutentacao_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1750CatalogoSutentacao_Codigo != Z1750CatalogoSutentacao_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("catalogosutentacao:[SecurityCheckFailed value for]"+"CatalogoSutentacao_Codigo:"+context.localUtil.Format( (decimal)(A1750CatalogoSutentacao_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("catalogosutentacao:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1750CatalogoSutentacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode192 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode192;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound192 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_4C0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CATALOGOSUTENTACAO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtCatalogoSutentacao_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114C2 */
                           E114C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124C2 */
                           E124C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E124C2 */
            E124C2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4C192( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes4C192( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_4C0( )
      {
         BeforeValidate4C192( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4C192( ) ;
            }
            else
            {
               CheckExtendedTable4C192( ) ;
               CloseExtendedTableCursors4C192( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption4C0( )
      {
      }

      protected void E114C2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtCatalogoSutentacao_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCatalogoSutentacao_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCatalogoSutentacao_Codigo_Visible), 5, 0)));
      }

      protected void E124C2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcatalogosutentacao.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM4C192( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1752CatalogoSutentacao_Descricao = T004C3_A1752CatalogoSutentacao_Descricao[0];
               Z1753CatalogoSutentacao_Atividade = T004C3_A1753CatalogoSutentacao_Atividade[0];
               Z1754CatalogoSutentacao_Prazo_Dias = T004C3_A1754CatalogoSutentacao_Prazo_Dias[0];
               Z1755CatalogoSutentacao_UST = T004C3_A1755CatalogoSutentacao_UST[0];
            }
            else
            {
               Z1752CatalogoSutentacao_Descricao = A1752CatalogoSutentacao_Descricao;
               Z1753CatalogoSutentacao_Atividade = A1753CatalogoSutentacao_Atividade;
               Z1754CatalogoSutentacao_Prazo_Dias = A1754CatalogoSutentacao_Prazo_Dias;
               Z1755CatalogoSutentacao_UST = A1755CatalogoSutentacao_UST;
            }
         }
         if ( GX_JID == -4 )
         {
            Z1750CatalogoSutentacao_Codigo = A1750CatalogoSutentacao_Codigo;
            Z1752CatalogoSutentacao_Descricao = A1752CatalogoSutentacao_Descricao;
            Z1753CatalogoSutentacao_Atividade = A1753CatalogoSutentacao_Atividade;
            Z1754CatalogoSutentacao_Prazo_Dias = A1754CatalogoSutentacao_Prazo_Dias;
            Z1755CatalogoSutentacao_UST = A1755CatalogoSutentacao_UST;
         }
      }

      protected void standaloneNotModal( )
      {
         edtCatalogoSutentacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCatalogoSutentacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCatalogoSutentacao_Codigo_Enabled), 5, 0)));
         edtCatalogoSutentacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCatalogoSutentacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCatalogoSutentacao_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7CatalogoSutentacao_Codigo) )
         {
            A1750CatalogoSutentacao_Codigo = AV7CatalogoSutentacao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load4C192( )
      {
         /* Using cursor T004C4 */
         pr_default.execute(2, new Object[] {A1750CatalogoSutentacao_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound192 = 1;
            A1752CatalogoSutentacao_Descricao = T004C4_A1752CatalogoSutentacao_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1752CatalogoSutentacao_Descricao", A1752CatalogoSutentacao_Descricao);
            A1753CatalogoSutentacao_Atividade = T004C4_A1753CatalogoSutentacao_Atividade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1753CatalogoSutentacao_Atividade", A1753CatalogoSutentacao_Atividade);
            A1754CatalogoSutentacao_Prazo_Dias = T004C4_A1754CatalogoSutentacao_Prazo_Dias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1754CatalogoSutentacao_Prazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1754CatalogoSutentacao_Prazo_Dias), 4, 0)));
            A1755CatalogoSutentacao_UST = T004C4_A1755CatalogoSutentacao_UST[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1755CatalogoSutentacao_UST", StringUtil.LTrim( StringUtil.Str( (decimal)(A1755CatalogoSutentacao_UST), 4, 0)));
            ZM4C192( -4) ;
         }
         pr_default.close(2);
         OnLoadActions4C192( ) ;
      }

      protected void OnLoadActions4C192( )
      {
      }

      protected void CheckExtendedTable4C192( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A1752CatalogoSutentacao_Descricao)) )
         {
            GX_msglist.addItem("Descri��o � obrigat�rio.", 1, "CATALOGOSUTENTACAO_DESCRICAO");
            AnyError = 1;
            GX_FocusControl = edtCatalogoSutentacao_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors4C192( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4C192( )
      {
         /* Using cursor T004C5 */
         pr_default.execute(3, new Object[] {A1750CatalogoSutentacao_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound192 = 1;
         }
         else
         {
            RcdFound192 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004C3 */
         pr_default.execute(1, new Object[] {A1750CatalogoSutentacao_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4C192( 4) ;
            RcdFound192 = 1;
            A1750CatalogoSutentacao_Codigo = T004C3_A1750CatalogoSutentacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
            A1752CatalogoSutentacao_Descricao = T004C3_A1752CatalogoSutentacao_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1752CatalogoSutentacao_Descricao", A1752CatalogoSutentacao_Descricao);
            A1753CatalogoSutentacao_Atividade = T004C3_A1753CatalogoSutentacao_Atividade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1753CatalogoSutentacao_Atividade", A1753CatalogoSutentacao_Atividade);
            A1754CatalogoSutentacao_Prazo_Dias = T004C3_A1754CatalogoSutentacao_Prazo_Dias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1754CatalogoSutentacao_Prazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1754CatalogoSutentacao_Prazo_Dias), 4, 0)));
            A1755CatalogoSutentacao_UST = T004C3_A1755CatalogoSutentacao_UST[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1755CatalogoSutentacao_UST", StringUtil.LTrim( StringUtil.Str( (decimal)(A1755CatalogoSutentacao_UST), 4, 0)));
            Z1750CatalogoSutentacao_Codigo = A1750CatalogoSutentacao_Codigo;
            sMode192 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load4C192( ) ;
            if ( AnyError == 1 )
            {
               RcdFound192 = 0;
               InitializeNonKey4C192( ) ;
            }
            Gx_mode = sMode192;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound192 = 0;
            InitializeNonKey4C192( ) ;
            sMode192 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode192;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4C192( ) ;
         if ( RcdFound192 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound192 = 0;
         /* Using cursor T004C6 */
         pr_default.execute(4, new Object[] {A1750CatalogoSutentacao_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T004C6_A1750CatalogoSutentacao_Codigo[0] < A1750CatalogoSutentacao_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T004C6_A1750CatalogoSutentacao_Codigo[0] > A1750CatalogoSutentacao_Codigo ) ) )
            {
               A1750CatalogoSutentacao_Codigo = T004C6_A1750CatalogoSutentacao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
               RcdFound192 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound192 = 0;
         /* Using cursor T004C7 */
         pr_default.execute(5, new Object[] {A1750CatalogoSutentacao_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T004C7_A1750CatalogoSutentacao_Codigo[0] > A1750CatalogoSutentacao_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T004C7_A1750CatalogoSutentacao_Codigo[0] < A1750CatalogoSutentacao_Codigo ) ) )
            {
               A1750CatalogoSutentacao_Codigo = T004C7_A1750CatalogoSutentacao_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
               RcdFound192 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4C192( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtCatalogoSutentacao_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4C192( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound192 == 1 )
            {
               if ( A1750CatalogoSutentacao_Codigo != Z1750CatalogoSutentacao_Codigo )
               {
                  A1750CatalogoSutentacao_Codigo = Z1750CatalogoSutentacao_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CATALOGOSUTENTACAO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtCatalogoSutentacao_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtCatalogoSutentacao_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update4C192( ) ;
                  GX_FocusControl = edtCatalogoSutentacao_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1750CatalogoSutentacao_Codigo != Z1750CatalogoSutentacao_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtCatalogoSutentacao_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4C192( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CATALOGOSUTENTACAO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtCatalogoSutentacao_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtCatalogoSutentacao_Descricao_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4C192( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1750CatalogoSutentacao_Codigo != Z1750CatalogoSutentacao_Codigo )
         {
            A1750CatalogoSutentacao_Codigo = Z1750CatalogoSutentacao_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CATALOGOSUTENTACAO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtCatalogoSutentacao_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtCatalogoSutentacao_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency4C192( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004C2 */
            pr_default.execute(0, new Object[] {A1750CatalogoSutentacao_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CatalogoSutentacao"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1752CatalogoSutentacao_Descricao, T004C2_A1752CatalogoSutentacao_Descricao[0]) != 0 ) || ( StringUtil.StrCmp(Z1753CatalogoSutentacao_Atividade, T004C2_A1753CatalogoSutentacao_Atividade[0]) != 0 ) || ( Z1754CatalogoSutentacao_Prazo_Dias != T004C2_A1754CatalogoSutentacao_Prazo_Dias[0] ) || ( Z1755CatalogoSutentacao_UST != T004C2_A1755CatalogoSutentacao_UST[0] ) )
            {
               if ( StringUtil.StrCmp(Z1752CatalogoSutentacao_Descricao, T004C2_A1752CatalogoSutentacao_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("catalogosutentacao:[seudo value changed for attri]"+"CatalogoSutentacao_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z1752CatalogoSutentacao_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T004C2_A1752CatalogoSutentacao_Descricao[0]);
               }
               if ( StringUtil.StrCmp(Z1753CatalogoSutentacao_Atividade, T004C2_A1753CatalogoSutentacao_Atividade[0]) != 0 )
               {
                  GXUtil.WriteLog("catalogosutentacao:[seudo value changed for attri]"+"CatalogoSutentacao_Atividade");
                  GXUtil.WriteLogRaw("Old: ",Z1753CatalogoSutentacao_Atividade);
                  GXUtil.WriteLogRaw("Current: ",T004C2_A1753CatalogoSutentacao_Atividade[0]);
               }
               if ( Z1754CatalogoSutentacao_Prazo_Dias != T004C2_A1754CatalogoSutentacao_Prazo_Dias[0] )
               {
                  GXUtil.WriteLog("catalogosutentacao:[seudo value changed for attri]"+"CatalogoSutentacao_Prazo_Dias");
                  GXUtil.WriteLogRaw("Old: ",Z1754CatalogoSutentacao_Prazo_Dias);
                  GXUtil.WriteLogRaw("Current: ",T004C2_A1754CatalogoSutentacao_Prazo_Dias[0]);
               }
               if ( Z1755CatalogoSutentacao_UST != T004C2_A1755CatalogoSutentacao_UST[0] )
               {
                  GXUtil.WriteLog("catalogosutentacao:[seudo value changed for attri]"+"CatalogoSutentacao_UST");
                  GXUtil.WriteLogRaw("Old: ",Z1755CatalogoSutentacao_UST);
                  GXUtil.WriteLogRaw("Current: ",T004C2_A1755CatalogoSutentacao_UST[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"CatalogoSutentacao"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4C192( )
      {
         BeforeValidate4C192( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4C192( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4C192( 0) ;
            CheckOptimisticConcurrency4C192( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4C192( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4C192( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004C8 */
                     pr_default.execute(6, new Object[] {A1752CatalogoSutentacao_Descricao, A1753CatalogoSutentacao_Atividade, A1754CatalogoSutentacao_Prazo_Dias, A1755CatalogoSutentacao_UST});
                     A1750CatalogoSutentacao_Codigo = T004C8_A1750CatalogoSutentacao_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("CatalogoSutentacao") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4C0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4C192( ) ;
            }
            EndLevel4C192( ) ;
         }
         CloseExtendedTableCursors4C192( ) ;
      }

      protected void Update4C192( )
      {
         BeforeValidate4C192( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4C192( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4C192( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4C192( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4C192( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004C9 */
                     pr_default.execute(7, new Object[] {A1752CatalogoSutentacao_Descricao, A1753CatalogoSutentacao_Atividade, A1754CatalogoSutentacao_Prazo_Dias, A1755CatalogoSutentacao_UST, A1750CatalogoSutentacao_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("CatalogoSutentacao") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CatalogoSutentacao"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4C192( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4C192( ) ;
         }
         CloseExtendedTableCursors4C192( ) ;
      }

      protected void DeferredUpdate4C192( )
      {
      }

      protected void delete( )
      {
         BeforeValidate4C192( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4C192( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4C192( ) ;
            AfterConfirm4C192( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4C192( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004C10 */
                  pr_default.execute(8, new Object[] {A1750CatalogoSutentacao_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("CatalogoSutentacao") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode192 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel4C192( ) ;
         Gx_mode = sMode192;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls4C192( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T004C11 */
            pr_default.execute(9, new Object[] {A1750CatalogoSutentacao_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
         }
      }

      protected void EndLevel4C192( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4C192( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "CatalogoSutentacao");
            if ( AnyError == 0 )
            {
               ConfirmValues4C0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "CatalogoSutentacao");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4C192( )
      {
         /* Scan By routine */
         /* Using cursor T004C12 */
         pr_default.execute(10);
         RcdFound192 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound192 = 1;
            A1750CatalogoSutentacao_Codigo = T004C12_A1750CatalogoSutentacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4C192( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound192 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound192 = 1;
            A1750CatalogoSutentacao_Codigo = T004C12_A1750CatalogoSutentacao_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4C192( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm4C192( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4C192( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4C192( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4C192( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4C192( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4C192( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4C192( )
      {
         edtCatalogoSutentacao_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCatalogoSutentacao_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCatalogoSutentacao_Descricao_Enabled), 5, 0)));
         edtCatalogoSutentacao_Atividade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCatalogoSutentacao_Atividade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCatalogoSutentacao_Atividade_Enabled), 5, 0)));
         edtCatalogoSutentacao_Prazo_Dias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCatalogoSutentacao_Prazo_Dias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCatalogoSutentacao_Prazo_Dias_Enabled), 5, 0)));
         edtCatalogoSutentacao_UST_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCatalogoSutentacao_UST_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCatalogoSutentacao_UST_Enabled), 5, 0)));
         edtCatalogoSutentacao_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCatalogoSutentacao_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCatalogoSutentacao_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4C0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117294341");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("catalogosutentacao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7CatalogoSutentacao_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1750CatalogoSutentacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1752CatalogoSutentacao_Descricao", Z1752CatalogoSutentacao_Descricao);
         GxWebStd.gx_hidden_field( context, "Z1753CatalogoSutentacao_Atividade", Z1753CatalogoSutentacao_Atividade);
         GxWebStd.gx_hidden_field( context, "Z1754CatalogoSutentacao_Prazo_Dias", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1754CatalogoSutentacao_Prazo_Dias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1755CatalogoSutentacao_UST", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1755CatalogoSutentacao_UST), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCATALOGOSUTENTACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7CatalogoSutentacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCATALOGOSUTENTACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7CatalogoSutentacao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "CatalogoSutentacao";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1750CatalogoSutentacao_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("catalogosutentacao:[SendSecurityCheck value for]"+"CatalogoSutentacao_Codigo:"+context.localUtil.Format( (decimal)(A1750CatalogoSutentacao_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("catalogosutentacao:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("catalogosutentacao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7CatalogoSutentacao_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "CatalogoSutentacao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Cat�logo Sustenta��o" ;
      }

      protected void InitializeNonKey4C192( )
      {
         A1752CatalogoSutentacao_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1752CatalogoSutentacao_Descricao", A1752CatalogoSutentacao_Descricao);
         A1753CatalogoSutentacao_Atividade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1753CatalogoSutentacao_Atividade", A1753CatalogoSutentacao_Atividade);
         A1754CatalogoSutentacao_Prazo_Dias = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1754CatalogoSutentacao_Prazo_Dias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1754CatalogoSutentacao_Prazo_Dias), 4, 0)));
         A1755CatalogoSutentacao_UST = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1755CatalogoSutentacao_UST", StringUtil.LTrim( StringUtil.Str( (decimal)(A1755CatalogoSutentacao_UST), 4, 0)));
         Z1752CatalogoSutentacao_Descricao = "";
         Z1753CatalogoSutentacao_Atividade = "";
         Z1754CatalogoSutentacao_Prazo_Dias = 0;
         Z1755CatalogoSutentacao_UST = 0;
      }

      protected void InitAll4C192( )
      {
         A1750CatalogoSutentacao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1750CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0)));
         InitializeNonKey4C192( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117294354");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("catalogosutentacao.js", "?20203117294354");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcatalogosutentacao_descricao_Internalname = "TEXTBLOCKCATALOGOSUTENTACAO_DESCRICAO";
         edtCatalogoSutentacao_Descricao_Internalname = "CATALOGOSUTENTACAO_DESCRICAO";
         lblTextblockcatalogosutentacao_atividade_Internalname = "TEXTBLOCKCATALOGOSUTENTACAO_ATIVIDADE";
         edtCatalogoSutentacao_Atividade_Internalname = "CATALOGOSUTENTACAO_ATIVIDADE";
         lblTextblockcatalogosutentacao_prazo_dias_Internalname = "TEXTBLOCKCATALOGOSUTENTACAO_PRAZO_DIAS";
         edtCatalogoSutentacao_Prazo_Dias_Internalname = "CATALOGOSUTENTACAO_PRAZO_DIAS";
         lblTextblockcatalogosutentacao_ust_Internalname = "TEXTBLOCKCATALOGOSUTENTACAO_UST";
         edtCatalogoSutentacao_UST_Internalname = "CATALOGOSUTENTACAO_UST";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtCatalogoSutentacao_Codigo_Internalname = "CATALOGOSUTENTACAO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Cat�logo Sustenta��o";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Cat�logo Sustenta��o";
         edtCatalogoSutentacao_UST_Jsonclick = "";
         edtCatalogoSutentacao_UST_Enabled = 1;
         edtCatalogoSutentacao_Prazo_Dias_Jsonclick = "";
         edtCatalogoSutentacao_Prazo_Dias_Enabled = 1;
         edtCatalogoSutentacao_Atividade_Jsonclick = "";
         edtCatalogoSutentacao_Atividade_Enabled = 1;
         edtCatalogoSutentacao_Descricao_Jsonclick = "";
         edtCatalogoSutentacao_Descricao_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtCatalogoSutentacao_Codigo_Jsonclick = "";
         edtCatalogoSutentacao_Codigo_Enabled = 0;
         edtCatalogoSutentacao_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7CatalogoSutentacao_Codigo',fld:'vCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E124C2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1752CatalogoSutentacao_Descricao = "";
         Z1753CatalogoSutentacao_Atividade = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcatalogosutentacao_descricao_Jsonclick = "";
         A1752CatalogoSutentacao_Descricao = "";
         lblTextblockcatalogosutentacao_atividade_Jsonclick = "";
         A1753CatalogoSutentacao_Atividade = "";
         lblTextblockcatalogosutentacao_prazo_dias_Jsonclick = "";
         lblTextblockcatalogosutentacao_ust_Jsonclick = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode192 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         T004C4_A1750CatalogoSutentacao_Codigo = new int[1] ;
         T004C4_A1752CatalogoSutentacao_Descricao = new String[] {""} ;
         T004C4_A1753CatalogoSutentacao_Atividade = new String[] {""} ;
         T004C4_A1754CatalogoSutentacao_Prazo_Dias = new short[1] ;
         T004C4_A1755CatalogoSutentacao_UST = new short[1] ;
         T004C5_A1750CatalogoSutentacao_Codigo = new int[1] ;
         T004C3_A1750CatalogoSutentacao_Codigo = new int[1] ;
         T004C3_A1752CatalogoSutentacao_Descricao = new String[] {""} ;
         T004C3_A1753CatalogoSutentacao_Atividade = new String[] {""} ;
         T004C3_A1754CatalogoSutentacao_Prazo_Dias = new short[1] ;
         T004C3_A1755CatalogoSutentacao_UST = new short[1] ;
         T004C6_A1750CatalogoSutentacao_Codigo = new int[1] ;
         T004C7_A1750CatalogoSutentacao_Codigo = new int[1] ;
         T004C2_A1750CatalogoSutentacao_Codigo = new int[1] ;
         T004C2_A1752CatalogoSutentacao_Descricao = new String[] {""} ;
         T004C2_A1753CatalogoSutentacao_Atividade = new String[] {""} ;
         T004C2_A1754CatalogoSutentacao_Prazo_Dias = new short[1] ;
         T004C2_A1755CatalogoSutentacao_UST = new short[1] ;
         T004C8_A1750CatalogoSutentacao_Codigo = new int[1] ;
         T004C11_A1750CatalogoSutentacao_Codigo = new int[1] ;
         T004C11_A1749Artefatos_Codigo = new int[1] ;
         T004C12_A1750CatalogoSutentacao_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.catalogosutentacao__default(),
            new Object[][] {
                new Object[] {
               T004C2_A1750CatalogoSutentacao_Codigo, T004C2_A1752CatalogoSutentacao_Descricao, T004C2_A1753CatalogoSutentacao_Atividade, T004C2_A1754CatalogoSutentacao_Prazo_Dias, T004C2_A1755CatalogoSutentacao_UST
               }
               , new Object[] {
               T004C3_A1750CatalogoSutentacao_Codigo, T004C3_A1752CatalogoSutentacao_Descricao, T004C3_A1753CatalogoSutentacao_Atividade, T004C3_A1754CatalogoSutentacao_Prazo_Dias, T004C3_A1755CatalogoSutentacao_UST
               }
               , new Object[] {
               T004C4_A1750CatalogoSutentacao_Codigo, T004C4_A1752CatalogoSutentacao_Descricao, T004C4_A1753CatalogoSutentacao_Atividade, T004C4_A1754CatalogoSutentacao_Prazo_Dias, T004C4_A1755CatalogoSutentacao_UST
               }
               , new Object[] {
               T004C5_A1750CatalogoSutentacao_Codigo
               }
               , new Object[] {
               T004C6_A1750CatalogoSutentacao_Codigo
               }
               , new Object[] {
               T004C7_A1750CatalogoSutentacao_Codigo
               }
               , new Object[] {
               T004C8_A1750CatalogoSutentacao_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004C11_A1750CatalogoSutentacao_Codigo, T004C11_A1749Artefatos_Codigo
               }
               , new Object[] {
               T004C12_A1750CatalogoSutentacao_Codigo
               }
            }
         );
      }

      private short Z1754CatalogoSutentacao_Prazo_Dias ;
      private short Z1755CatalogoSutentacao_UST ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1754CatalogoSutentacao_Prazo_Dias ;
      private short A1755CatalogoSutentacao_UST ;
      private short RcdFound192 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private int wcpOAV7CatalogoSutentacao_Codigo ;
      private int Z1750CatalogoSutentacao_Codigo ;
      private int AV7CatalogoSutentacao_Codigo ;
      private int trnEnded ;
      private int A1750CatalogoSutentacao_Codigo ;
      private int edtCatalogoSutentacao_Codigo_Enabled ;
      private int edtCatalogoSutentacao_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtCatalogoSutentacao_Descricao_Enabled ;
      private int edtCatalogoSutentacao_Atividade_Enabled ;
      private int edtCatalogoSutentacao_Prazo_Dias_Enabled ;
      private int edtCatalogoSutentacao_UST_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtCatalogoSutentacao_Descricao_Internalname ;
      private String edtCatalogoSutentacao_Codigo_Internalname ;
      private String edtCatalogoSutentacao_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcatalogosutentacao_descricao_Internalname ;
      private String lblTextblockcatalogosutentacao_descricao_Jsonclick ;
      private String edtCatalogoSutentacao_Descricao_Jsonclick ;
      private String lblTextblockcatalogosutentacao_atividade_Internalname ;
      private String lblTextblockcatalogosutentacao_atividade_Jsonclick ;
      private String edtCatalogoSutentacao_Atividade_Internalname ;
      private String edtCatalogoSutentacao_Atividade_Jsonclick ;
      private String lblTextblockcatalogosutentacao_prazo_dias_Internalname ;
      private String lblTextblockcatalogosutentacao_prazo_dias_Jsonclick ;
      private String edtCatalogoSutentacao_Prazo_Dias_Internalname ;
      private String edtCatalogoSutentacao_Prazo_Dias_Jsonclick ;
      private String lblTextblockcatalogosutentacao_ust_Internalname ;
      private String lblTextblockcatalogosutentacao_ust_Jsonclick ;
      private String edtCatalogoSutentacao_UST_Internalname ;
      private String edtCatalogoSutentacao_UST_Jsonclick ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode192 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String Z1752CatalogoSutentacao_Descricao ;
      private String Z1753CatalogoSutentacao_Atividade ;
      private String A1752CatalogoSutentacao_Descricao ;
      private String A1753CatalogoSutentacao_Atividade ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] T004C4_A1750CatalogoSutentacao_Codigo ;
      private String[] T004C4_A1752CatalogoSutentacao_Descricao ;
      private String[] T004C4_A1753CatalogoSutentacao_Atividade ;
      private short[] T004C4_A1754CatalogoSutentacao_Prazo_Dias ;
      private short[] T004C4_A1755CatalogoSutentacao_UST ;
      private int[] T004C5_A1750CatalogoSutentacao_Codigo ;
      private int[] T004C3_A1750CatalogoSutentacao_Codigo ;
      private String[] T004C3_A1752CatalogoSutentacao_Descricao ;
      private String[] T004C3_A1753CatalogoSutentacao_Atividade ;
      private short[] T004C3_A1754CatalogoSutentacao_Prazo_Dias ;
      private short[] T004C3_A1755CatalogoSutentacao_UST ;
      private int[] T004C6_A1750CatalogoSutentacao_Codigo ;
      private int[] T004C7_A1750CatalogoSutentacao_Codigo ;
      private int[] T004C2_A1750CatalogoSutentacao_Codigo ;
      private String[] T004C2_A1752CatalogoSutentacao_Descricao ;
      private String[] T004C2_A1753CatalogoSutentacao_Atividade ;
      private short[] T004C2_A1754CatalogoSutentacao_Prazo_Dias ;
      private short[] T004C2_A1755CatalogoSutentacao_UST ;
      private int[] T004C8_A1750CatalogoSutentacao_Codigo ;
      private int[] T004C11_A1750CatalogoSutentacao_Codigo ;
      private int[] T004C11_A1749Artefatos_Codigo ;
      private int[] T004C12_A1750CatalogoSutentacao_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class catalogosutentacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004C4 ;
          prmT004C4 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004C5 ;
          prmT004C5 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004C3 ;
          prmT004C3 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004C6 ;
          prmT004C6 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004C7 ;
          prmT004C7 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004C2 ;
          prmT004C2 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004C8 ;
          prmT004C8 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@CatalogoSutentacao_Atividade",SqlDbType.VarChar,50,0} ,
          new Object[] {"@CatalogoSutentacao_Prazo_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@CatalogoSutentacao_UST",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT004C9 ;
          prmT004C9 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@CatalogoSutentacao_Atividade",SqlDbType.VarChar,50,0} ,
          new Object[] {"@CatalogoSutentacao_Prazo_Dias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@CatalogoSutentacao_UST",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004C10 ;
          prmT004C10 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004C11 ;
          prmT004C11 = new Object[] {
          new Object[] {"@CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004C12 ;
          prmT004C12 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T004C2", "SELECT [CatalogoSutentacao_Codigo], [CatalogoSutentacao_Descricao], [CatalogoSutentacao_Atividade], [CatalogoSutentacao_Prazo_Dias], [CatalogoSutentacao_UST] FROM [CatalogoSutentacao] WITH (UPDLOCK) WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004C2,1,0,true,false )
             ,new CursorDef("T004C3", "SELECT [CatalogoSutentacao_Codigo], [CatalogoSutentacao_Descricao], [CatalogoSutentacao_Atividade], [CatalogoSutentacao_Prazo_Dias], [CatalogoSutentacao_UST] FROM [CatalogoSutentacao] WITH (NOLOCK) WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004C3,1,0,true,false )
             ,new CursorDef("T004C4", "SELECT TM1.[CatalogoSutentacao_Codigo], TM1.[CatalogoSutentacao_Descricao], TM1.[CatalogoSutentacao_Atividade], TM1.[CatalogoSutentacao_Prazo_Dias], TM1.[CatalogoSutentacao_UST] FROM [CatalogoSutentacao] TM1 WITH (NOLOCK) WHERE TM1.[CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo ORDER BY TM1.[CatalogoSutentacao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004C4,100,0,true,false )
             ,new CursorDef("T004C5", "SELECT [CatalogoSutentacao_Codigo] FROM [CatalogoSutentacao] WITH (NOLOCK) WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004C5,1,0,true,false )
             ,new CursorDef("T004C6", "SELECT TOP 1 [CatalogoSutentacao_Codigo] FROM [CatalogoSutentacao] WITH (NOLOCK) WHERE ( [CatalogoSutentacao_Codigo] > @CatalogoSutentacao_Codigo) ORDER BY [CatalogoSutentacao_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004C6,1,0,true,true )
             ,new CursorDef("T004C7", "SELECT TOP 1 [CatalogoSutentacao_Codigo] FROM [CatalogoSutentacao] WITH (NOLOCK) WHERE ( [CatalogoSutentacao_Codigo] < @CatalogoSutentacao_Codigo) ORDER BY [CatalogoSutentacao_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004C7,1,0,true,true )
             ,new CursorDef("T004C8", "INSERT INTO [CatalogoSutentacao]([CatalogoSutentacao_Descricao], [CatalogoSutentacao_Atividade], [CatalogoSutentacao_Prazo_Dias], [CatalogoSutentacao_UST]) VALUES(@CatalogoSutentacao_Descricao, @CatalogoSutentacao_Atividade, @CatalogoSutentacao_Prazo_Dias, @CatalogoSutentacao_UST); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004C8)
             ,new CursorDef("T004C9", "UPDATE [CatalogoSutentacao] SET [CatalogoSutentacao_Descricao]=@CatalogoSutentacao_Descricao, [CatalogoSutentacao_Atividade]=@CatalogoSutentacao_Atividade, [CatalogoSutentacao_Prazo_Dias]=@CatalogoSutentacao_Prazo_Dias, [CatalogoSutentacao_UST]=@CatalogoSutentacao_UST  WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo", GxErrorMask.GX_NOMASK,prmT004C9)
             ,new CursorDef("T004C10", "DELETE FROM [CatalogoSutentacao]  WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo", GxErrorMask.GX_NOMASK,prmT004C10)
             ,new CursorDef("T004C11", "SELECT TOP 1 [CatalogoSutentacao_Codigo], [Artefatos_Codigo] FROM [CatalogoSutentacaoArtefatos] WITH (NOLOCK) WHERE [CatalogoSutentacao_Codigo] = @CatalogoSutentacao_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004C11,1,0,true,true )
             ,new CursorDef("T004C12", "SELECT [CatalogoSutentacao_Codigo] FROM [CatalogoSutentacao] WITH (NOLOCK) ORDER BY [CatalogoSutentacao_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004C12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
