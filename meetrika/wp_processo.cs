/*
               File: WP_Processo
        Description: Processos do Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:1:16.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_processo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_processo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_processo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Servico_Codigo ,
                           String aP1_Servico_Sigla )
      {
         this.AV5Servico_Codigo = aP0_Servico_Codigo;
         this.AV6Servico_Sigla = aP1_Servico_Sigla;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavServicofluxo_servicopos = new GXCombobox();
         radavManterordem = new GXRadio();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vSERVICOFLUXO_SERVICOPOS") == 0 )
            {
               AV5Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5Servico_Codigo), "ZZZZZ9")));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvSERVICOFLUXO_SERVICOPOSLD2( AV5Servico_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV5Servico_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5Servico_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV6Servico_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Servico_Sigla", AV6Servico_Sigla);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Servico_Sigla, "@!"))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PALD2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTLD2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311911676");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_processo.aspx") + "?" + UrlEncode("" +AV5Servico_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV6Servico_Sigla))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_OBJETOCONTROLE", StringUtil.RTrim( A1436Servico_ObjetoControle));
         GxWebStd.gx_hidden_field( context, "SERVICO_OBRIGAVALORES", StringUtil.RTrim( A1429Servico_ObrigaValores));
         GxWebStd.gx_hidden_field( context, "SERVICO_UO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A633Servico_UO), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "SERVICO_UORESPEXCLUSIVA", A1077Servico_UORespExclusiva);
         GxWebStd.gx_hidden_field( context, "SERVICOGRUPO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A157ServicoGrupo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_NOME", StringUtil.RTrim( A608Servico_Nome));
         GxWebStd.gx_hidden_field( context, "SERVICO_SIGLA", StringUtil.RTrim( A605Servico_Sigla));
         GxWebStd.gx_hidden_field( context, "SERVICO_DESCRICAO", A156Servico_Descricao);
         GxWebStd.gx_hidden_field( context, "SERVICO_PERCTMP", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1534Servico_PercTmp), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_PERCPGM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1535Servico_PercPgm), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_PERCCNC", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1536Servico_PercCnc), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICOFLUXO_SERVICOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICOFLUXO_ORDEM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1532ServicoFluxo_Ordem), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICO_SIGLA", StringUtil.RTrim( AV6Servico_Sigla));
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Servico_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Servico_Sigla, "@!"))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WELD2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTLD2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_processo.aspx") + "?" + UrlEncode("" +AV5Servico_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV6Servico_Sigla)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_Processo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Processos do Servi�o" ;
      }

      protected void WBLD0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_LD2( true) ;
         }
         else
         {
            wb_table1_2_LD2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_LD2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTLD2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Processos do Servi�o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPLD0( ) ;
      }

      protected void WSLD2( )
      {
         STARTLD2( ) ;
         EVTLD2( ) ;
      }

      protected void EVTLD2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11LD2 */
                              E11LD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCONFIRMAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12LD2 */
                              E12LD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13LD2 */
                              E13LD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14LD2 */
                              E14LD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSERVICOFLUXO_SERVICOPOS.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15LD2 */
                              E15LD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSERVICOFLUXO_ORDEM.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16LD2 */
                              E16LD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17LD2 */
                              E17LD2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WELD2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PALD2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavServicofluxo_servicopos.Name = "vSERVICOFLUXO_SERVICOPOS";
            dynavServicofluxo_servicopos.WebTags = "";
            radavManterordem.Name = "vMANTERORDEM";
            radavManterordem.WebTags = "";
            radavManterordem.addItem(StringUtil.BoolToStr( true), "Mesma ordem", 0);
            radavManterordem.addItem(StringUtil.BoolToStr( false), "Inserir antes", 0);
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavServicofluxo_servicopos_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvSERVICOFLUXO_SERVICOPOSLD2( int AV5Servico_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvSERVICOFLUXO_SERVICOPOS_dataLD2( AV5Servico_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvSERVICOFLUXO_SERVICOPOS_htmlLD2( int AV5Servico_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvSERVICOFLUXO_SERVICOPOS_dataLD2( AV5Servico_Codigo) ;
         gxdynajaxindex = 1;
         dynavServicofluxo_servicopos.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavServicofluxo_servicopos.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavServicofluxo_servicopos.ItemCount > 0 )
         {
            AV12ServicoFluxo_ServicoPos = (int)(NumberUtil.Val( dynavServicofluxo_servicopos.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12ServicoFluxo_ServicoPos), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ServicoFluxo_ServicoPos), 6, 0)));
         }
      }

      protected void GXDLVvSERVICOFLUXO_SERVICOPOS_dataLD2( int AV5Servico_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Novo processo)");
         /* Using cursor H00LD2 */
         pr_default.execute(0, new Object[] {AV5Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00LD2_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00LD2_A605Servico_Sigla[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavServicofluxo_servicopos.ItemCount > 0 )
         {
            AV12ServicoFluxo_ServicoPos = (int)(NumberUtil.Val( dynavServicofluxo_servicopos.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12ServicoFluxo_ServicoPos), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ServicoFluxo_ServicoPos), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFLD2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFLD2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E14LD2 */
         E14LD2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E17LD2 */
            E17LD2 ();
            WBLD0( ) ;
         }
      }

      protected void STRUPLD0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         GXVvSERVICOFLUXO_SERVICOPOS_htmlLD2( AV5Servico_Codigo) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11LD2 */
         E11LD2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavServicofluxo_servicopos.CurrentValue = cgiGet( dynavServicofluxo_servicopos_Internalname);
            AV12ServicoFluxo_ServicoPos = (int)(NumberUtil.Val( cgiGet( dynavServicofluxo_servicopos_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ServicoFluxo_ServicoPos), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavServicofluxo_ordem_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServicofluxo_ordem_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICOFLUXO_ORDEM");
               GX_FocusControl = edtavServicofluxo_ordem_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13ServicoFluxo_Ordem = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ServicoFluxo_Ordem), 3, 0)));
            }
            else
            {
               AV13ServicoFluxo_Ordem = (short)(context.localUtil.CToN( cgiGet( edtavServicofluxo_ordem_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ServicoFluxo_Ordem), 3, 0)));
            }
            AV14ManterOrdem = StringUtil.StrToBool( cgiGet( radavManterordem_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ManterOrdem", AV14ManterOrdem);
            AV23ServicoFluxo_Nome = StringUtil.Upper( cgiGet( edtavServicofluxo_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ServicoFluxo_Nome", AV23ServicoFluxo_Nome);
            AV22ServicoFluxo_Sigla = StringUtil.Upper( cgiGet( edtavServicofluxo_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ServicoFluxo_Sigla", AV22ServicoFluxo_Sigla);
            AV16Servico_Descricao = cgiGet( edtavServico_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Servico_Descricao", AV16Servico_Descricao);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavServico_perctmp_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServico_perctmp_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICO_PERCTMP");
               GX_FocusControl = edtavServico_perctmp_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17Servico_PercTmp = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_PercTmp), 4, 0)));
            }
            else
            {
               AV17Servico_PercTmp = (short)(context.localUtil.CToN( cgiGet( edtavServico_perctmp_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_PercTmp), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavServico_percpgm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServico_percpgm_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICO_PERCPGM");
               GX_FocusControl = edtavServico_percpgm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18Servico_PercPgm = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Servico_PercPgm), 4, 0)));
            }
            else
            {
               AV18Servico_PercPgm = (short)(context.localUtil.CToN( cgiGet( edtavServico_percpgm_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Servico_PercPgm), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavServico_perccnc_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavServico_perccnc_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vSERVICO_PERCCNC");
               GX_FocusControl = edtavServico_perccnc_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19Servico_PercCnc = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Servico_PercCnc), 4, 0)));
            }
            else
            {
               AV19Servico_PercCnc = (short)(context.localUtil.CToN( cgiGet( edtavServico_perccnc_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Servico_PercCnc), 4, 0)));
            }
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvSERVICOFLUXO_SERVICOPOS_htmlLD2( AV5Servico_Codigo) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11LD2 */
         E11LD2 ();
         if (returnInSub) return;
      }

      protected void E11LD2( )
      {
         /* Start Routine */
         lblTbservico_Caption = "��"+AV6Servico_Sigla;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbservico_Internalname, "Caption", lblTbservico_Caption);
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\"> document.getElementById(\"GXUITABSPANEL_TABContainer\").setAttribute(\"class\",\"gx_usercontrol tab-container\");</script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         /* Using cursor H00LD4 */
         pr_default.execute(1, new Object[] {AV5Servico_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1522ServicoFluxo_ServicoCod = H00LD4_A1522ServicoFluxo_ServicoCod[0];
            n1522ServicoFluxo_ServicoCod = H00LD4_n1522ServicoFluxo_ServicoCod[0];
            A40000GXC1 = H00LD4_A40000GXC1[0];
            n40000GXC1 = H00LD4_n40000GXC1[0];
            A40000GXC1 = H00LD4_A40000GXC1[0];
            n40000GXC1 = H00LD4_n40000GXC1[0];
            AV13ServicoFluxo_Ordem = (short)(A40000GXC1+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ServicoFluxo_Ordem), 3, 0)));
            pr_default.readNext(1);
         }
         pr_default.close(1);
         AV14ManterOrdem = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ManterOrdem", AV14ManterOrdem);
         radavManterordem.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavManterordem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavManterordem.Visible), 5, 0)));
      }

      protected void E12LD2( )
      {
         /* 'DoConfirmar' Routine */
         if ( (0==AV12ServicoFluxo_ServicoPos) )
         {
            AV21Servico = new SdtServico(context);
            AV21Servico.gxTpr_Servico_nome = AV23ServicoFluxo_Nome;
            AV21Servico.gxTpr_Servico_sigla = AV22ServicoFluxo_Sigla;
            AV21Servico.gxTpr_Servico_descricao = AV16Servico_Descricao;
            AV21Servico.gxTpr_Servico_perctmp = AV17Servico_PercTmp;
            AV21Servico.gxTpr_Servico_percpgm = AV18Servico_PercPgm;
            AV21Servico.gxTpr_Servico_perccnc = AV19Servico_PercCnc;
            AV21Servico.gxTpr_Servico_ativo = true;
            AV21Servico.gxTpr_Servico_vinculado = AV5Servico_Codigo;
            AV21Servico.gxTpr_Servico_tipohierarquia = 2;
            /* Using cursor H00LD5 */
            pr_default.execute(2, new Object[] {AV5Servico_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A155Servico_Codigo = H00LD5_A155Servico_Codigo[0];
               A1436Servico_ObjetoControle = H00LD5_A1436Servico_ObjetoControle[0];
               A1429Servico_ObrigaValores = H00LD5_A1429Servico_ObrigaValores[0];
               n1429Servico_ObrigaValores = H00LD5_n1429Servico_ObrigaValores[0];
               A633Servico_UO = H00LD5_A633Servico_UO[0];
               n633Servico_UO = H00LD5_n633Servico_UO[0];
               A1077Servico_UORespExclusiva = H00LD5_A1077Servico_UORespExclusiva[0];
               n1077Servico_UORespExclusiva = H00LD5_n1077Servico_UORespExclusiva[0];
               A157ServicoGrupo_Codigo = H00LD5_A157ServicoGrupo_Codigo[0];
               AV21Servico.gxTpr_Servico_objetocontrole = A1436Servico_ObjetoControle;
               AV21Servico.gxTpr_Servico_obrigavalores = A1429Servico_ObrigaValores;
               if ( (0==A633Servico_UO) )
               {
                  AV21Servico.gxTv_SdtServico_Servico_uo_SetNull();
               }
               else
               {
                  AV21Servico.gxTpr_Servico_uo = A633Servico_UO;
               }
               AV21Servico.gxTpr_Servico_uorespexclusiva = A1077Servico_UORespExclusiva;
               AV21Servico.gxTpr_Servicogrupo_codigo = A157ServicoGrupo_Codigo;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            AV21Servico.Save();
            context.CommitDataStores( "WP_Processo");
            AV20ServicoFluxo = new SdtServicoFluxo(context);
            AV20ServicoFluxo.gxTpr_Servicofluxo_servicocod = AV5Servico_Codigo;
            AV20ServicoFluxo.gxTpr_Servicofluxo_servicopos = AV21Servico.gxTpr_Servico_codigo;
            AV20ServicoFluxo.gxTpr_Servicofluxo_ordem = (short)((AV14ManterOrdem ? AV13ServicoFluxo_Ordem : 0));
            AV20ServicoFluxo.Save();
         }
         else
         {
            AV20ServicoFluxo = new SdtServicoFluxo(context);
            AV20ServicoFluxo.gxTpr_Servicofluxo_servicocod = AV5Servico_Codigo;
            AV20ServicoFluxo.gxTpr_Servicofluxo_servicopos = AV12ServicoFluxo_ServicoPos;
            AV20ServicoFluxo.gxTpr_Servicofluxo_ordem = (short)((AV14ManterOrdem ? AV13ServicoFluxo_Ordem : 0));
            AV20ServicoFluxo.Save();
         }
         context.CommitDataStores( "WP_Processo");
         if ( ! AV14ManterOrdem )
         {
            new prc_reordenafluxo(context ).execute(  "S",  AV5Servico_Codigo,  AV13ServicoFluxo_Ordem,  9999,  "-") ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5Servico_Codigo), "ZZZZZ9")));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ServicoFluxo_Ordem), 3, 0)));
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E13LD2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E14LD2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9Context) ;
      }

      protected void E15LD2( )
      {
         /* Servicofluxo_servicopos_Click Routine */
         if ( (0==AV12ServicoFluxo_ServicoPos) )
         {
            AV23ServicoFluxo_Nome = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ServicoFluxo_Nome", AV23ServicoFluxo_Nome);
            AV22ServicoFluxo_Sigla = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ServicoFluxo_Sigla", AV22ServicoFluxo_Sigla);
            AV16Servico_Descricao = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Servico_Descricao", AV16Servico_Descricao);
            AV17Servico_PercTmp = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_PercTmp), 4, 0)));
            AV18Servico_PercPgm = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Servico_PercPgm), 4, 0)));
            AV19Servico_PercCnc = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Servico_PercCnc), 4, 0)));
         }
         else
         {
            /* Using cursor H00LD6 */
            pr_default.execute(3, new Object[] {AV12ServicoFluxo_ServicoPos});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A155Servico_Codigo = H00LD6_A155Servico_Codigo[0];
               A608Servico_Nome = H00LD6_A608Servico_Nome[0];
               A605Servico_Sigla = H00LD6_A605Servico_Sigla[0];
               A156Servico_Descricao = H00LD6_A156Servico_Descricao[0];
               n156Servico_Descricao = H00LD6_n156Servico_Descricao[0];
               A1534Servico_PercTmp = H00LD6_A1534Servico_PercTmp[0];
               n1534Servico_PercTmp = H00LD6_n1534Servico_PercTmp[0];
               A1535Servico_PercPgm = H00LD6_A1535Servico_PercPgm[0];
               n1535Servico_PercPgm = H00LD6_n1535Servico_PercPgm[0];
               A1536Servico_PercCnc = H00LD6_A1536Servico_PercCnc[0];
               n1536Servico_PercCnc = H00LD6_n1536Servico_PercCnc[0];
               AV23ServicoFluxo_Nome = A608Servico_Nome;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ServicoFluxo_Nome", AV23ServicoFluxo_Nome);
               AV22ServicoFluxo_Sigla = A605Servico_Sigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ServicoFluxo_Sigla", AV22ServicoFluxo_Sigla);
               AV16Servico_Descricao = A156Servico_Descricao;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Servico_Descricao", AV16Servico_Descricao);
               AV17Servico_PercTmp = A1534Servico_PercTmp;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Servico_PercTmp", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Servico_PercTmp), 4, 0)));
               AV18Servico_PercPgm = A1535Servico_PercPgm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Servico_PercPgm", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Servico_PercPgm), 4, 0)));
               AV19Servico_PercCnc = A1536Servico_PercCnc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Servico_PercCnc", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Servico_PercCnc), 4, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
         }
      }

      protected void E16LD2( )
      {
         /* Servicofluxo_ordem_Isvalid Routine */
         /* Using cursor H00LD7 */
         pr_default.execute(4, new Object[] {AV5Servico_Codigo, AV13ServicoFluxo_Ordem});
         while ( (pr_default.getStatus(4) != 101) )
         {
            A1532ServicoFluxo_Ordem = H00LD7_A1532ServicoFluxo_Ordem[0];
            n1532ServicoFluxo_Ordem = H00LD7_n1532ServicoFluxo_Ordem[0];
            A1522ServicoFluxo_ServicoCod = H00LD7_A1522ServicoFluxo_ServicoCod[0];
            n1522ServicoFluxo_ServicoCod = H00LD7_n1522ServicoFluxo_ServicoCod[0];
            radavManterordem.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavManterordem_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavManterordem.Visible), 5, 0)));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void nextLoad( )
      {
      }

      protected void E17LD2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_LD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbservico_Internalname, lblTbservico_Caption, "", "", lblTbservico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_11_LD2( true) ;
         }
         else
         {
            wb_table2_11_LD2( false) ;
         }
         return  ;
      }

      protected void wb_table2_11_LD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_LD2e( true) ;
         }
         else
         {
            wb_table1_2_LD2e( false) ;
         }
      }

      protected void wb_table2_11_LD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_14_LD2( true) ;
         }
         else
         {
            wb_table3_14_LD2( false) ;
         }
         return  ;
      }

      protected void wb_table3_14_LD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            wb_table4_80_LD2( true) ;
         }
         else
         {
            wb_table4_80_LD2( false) ;
         }
         return  ;
      }

      protected void wb_table4_80_LD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_11_LD2e( true) ;
         }
         else
         {
            wb_table2_11_LD2e( false) ;
         }
      }

      protected void wb_table4_80_LD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirmar_Internalname, "", "Confirmar", bttBtnconfirmar_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOCONFIRMAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_80_LD2e( true) ;
         }
         else
         {
            wb_table4_80_LD2e( false) ;
         }
      }

      protected void wb_table3_14_LD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_servicopos_Internalname, "Processo", "", "", lblTextblockservicofluxo_servicopos_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavServicofluxo_servicopos, dynavServicofluxo_servicopos_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV12ServicoFluxo_ServicoPos), 6, 0)), 1, dynavServicofluxo_servicopos_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSERVICOFLUXO_SERVICOPOS.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WP_Processo.htm");
            dynavServicofluxo_servicopos.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12ServicoFluxo_ServicoPos), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavServicofluxo_servicopos_Internalname, "Values", (String)(dynavServicofluxo_servicopos.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_ordem_Internalname, "Ordem", "", "", lblTextblockservicofluxo_ordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_24_LD2( true) ;
         }
         else
         {
            wb_table5_24_LD2( false) ;
         }
         return  ;
      }

      protected void wb_table5_24_LD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_nome_Internalname, "Nome", "", "", lblTextblockservicofluxo_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicofluxo_nome_Internalname, StringUtil.RTrim( AV23ServicoFluxo_Nome), StringUtil.RTrim( context.localUtil.Format( AV23ServicoFluxo_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicofluxo_nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_sigla_Internalname, "Sigla", "", "", lblTextblockservicofluxo_sigla_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicofluxo_sigla_Internalname, StringUtil.RTrim( AV22ServicoFluxo_Sigla), StringUtil.RTrim( context.localUtil.Format( AV22ServicoFluxo_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicofluxo_sigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_descricao_Internalname, "Descri��o", "", "", lblTextblockservico_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavServico_descricao_Internalname, AV16Servico_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", 0, 1, 1, 0, 600, "px", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_perctmp_Internalname, "Perc. de Tempo", "", "", lblTextblockservico_perctmp_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_51_LD2( true) ;
         }
         else
         {
            wb_table6_51_LD2( false) ;
         }
         return  ;
      }

      protected void wb_table6_51_LD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_percpgm_Internalname, "Perc. de Pagamento", "", "", lblTextblockservico_percpgm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_61_LD2( true) ;
         }
         else
         {
            wb_table7_61_LD2( false) ;
         }
         return  ;
      }

      protected void wb_table7_61_LD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCellLeft'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservico_perccnc_Internalname, "Perc. de Cancelamento", "", "", lblTextblockservico_perccnc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table8_71_LD2( true) ;
         }
         else
         {
            wb_table8_71_LD2( false) ;
         }
         return  ;
      }

      protected void wb_table8_71_LD2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_14_LD2e( true) ;
         }
         else
         {
            wb_table3_14_LD2e( false) ;
         }
      }

      protected void wb_table8_71_LD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_perccnc_Internalname, tblTablemergedservico_perccnc_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_perccnc_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19Servico_PercCnc), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV19Servico_PercCnc), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_perccnc_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_perccnc_righttext_Internalname, "�%", "", "", lblServico_perccnc_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_71_LD2e( true) ;
         }
         else
         {
            wb_table8_71_LD2e( false) ;
         }
      }

      protected void wb_table7_61_LD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_percpgm_Internalname, tblTablemergedservico_percpgm_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_percpgm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Servico_PercPgm), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18Servico_PercPgm), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_percpgm_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_percpgm_righttext_Internalname, "�%", "", "", lblServico_percpgm_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_61_LD2e( true) ;
         }
         else
         {
            wb_table7_61_LD2e( false) ;
         }
      }

      protected void wb_table6_51_LD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservico_perctmp_Internalname, tblTablemergedservico_perctmp_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_perctmp_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Servico_PercTmp), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV17Servico_PercTmp), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_perctmp_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "center", false, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServico_perctmp_righttext_Internalname, "�%", "", "", lblServico_perctmp_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_51_LD2e( true) ;
         }
         else
         {
            wb_table6_51_LD2e( false) ;
         }
      }

      protected void wb_table5_24_LD2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedservicofluxo_ordem_Internalname, tblTablemergedservicofluxo_ordem_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicofluxo_ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13ServicoFluxo_Ordem), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13ServicoFluxo_Ordem), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicofluxo_ordem_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 1, 0, "text", "", 50, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmanterordem_Internalname, "", "", "", lblTextblockmanterordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Radio button */
            ClassString = "BootstrapAttributeGrayCheckBox";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_radio_ctrl( context, radavManterordem, radavManterordem_Internalname, StringUtil.BoolToStr( AV14ManterOrdem), "", radavManterordem.Visible, 1, 0, 0, StyleString, ClassString, "", 0, radavManterordem_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", TempTags+" onclick=\"gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "HLP_WP_Processo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_24_LD2e( true) ;
         }
         else
         {
            wb_table5_24_LD2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV5Servico_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Servico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV5Servico_Codigo), "ZZZZZ9")));
         AV6Servico_Sigla = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Servico_Sigla", AV6Servico_Sigla);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSERVICO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6Servico_Sigla, "@!"))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PALD2( ) ;
         WSLD2( ) ;
         WELD2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311911733");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_processo.js", "?2020311911733");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbservico_Internalname = "TBSERVICO";
         lblTextblockservicofluxo_servicopos_Internalname = "TEXTBLOCKSERVICOFLUXO_SERVICOPOS";
         dynavServicofluxo_servicopos_Internalname = "vSERVICOFLUXO_SERVICOPOS";
         lblTextblockservicofluxo_ordem_Internalname = "TEXTBLOCKSERVICOFLUXO_ORDEM";
         edtavServicofluxo_ordem_Internalname = "vSERVICOFLUXO_ORDEM";
         lblTextblockmanterordem_Internalname = "TEXTBLOCKMANTERORDEM";
         radavManterordem_Internalname = "vMANTERORDEM";
         tblTablemergedservicofluxo_ordem_Internalname = "TABLEMERGEDSERVICOFLUXO_ORDEM";
         lblTextblockservicofluxo_nome_Internalname = "TEXTBLOCKSERVICOFLUXO_NOME";
         edtavServicofluxo_nome_Internalname = "vSERVICOFLUXO_NOME";
         lblTextblockservicofluxo_sigla_Internalname = "TEXTBLOCKSERVICOFLUXO_SIGLA";
         edtavServicofluxo_sigla_Internalname = "vSERVICOFLUXO_SIGLA";
         lblTextblockservico_descricao_Internalname = "TEXTBLOCKSERVICO_DESCRICAO";
         edtavServico_descricao_Internalname = "vSERVICO_DESCRICAO";
         lblTextblockservico_perctmp_Internalname = "TEXTBLOCKSERVICO_PERCTMP";
         edtavServico_perctmp_Internalname = "vSERVICO_PERCTMP";
         lblServico_perctmp_righttext_Internalname = "SERVICO_PERCTMP_RIGHTTEXT";
         tblTablemergedservico_perctmp_Internalname = "TABLEMERGEDSERVICO_PERCTMP";
         lblTextblockservico_percpgm_Internalname = "TEXTBLOCKSERVICO_PERCPGM";
         edtavServico_percpgm_Internalname = "vSERVICO_PERCPGM";
         lblServico_percpgm_righttext_Internalname = "SERVICO_PERCPGM_RIGHTTEXT";
         tblTablemergedservico_percpgm_Internalname = "TABLEMERGEDSERVICO_PERCPGM";
         lblTextblockservico_perccnc_Internalname = "TEXTBLOCKSERVICO_PERCCNC";
         edtavServico_perccnc_Internalname = "vSERVICO_PERCCNC";
         lblServico_perccnc_righttext_Internalname = "SERVICO_PERCCNC_RIGHTTEXT";
         tblTablemergedservico_perccnc_Internalname = "TABLEMERGEDSERVICO_PERCCNC";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         bttBtnconfirmar_Internalname = "BTNCONFIRMAR";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablecontent_Internalname = "TABLECONTENT";
         lblTbjava_Internalname = "TBJAVA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         radavManterordem_Jsonclick = "";
         edtavServicofluxo_ordem_Jsonclick = "";
         edtavServico_perctmp_Jsonclick = "";
         edtavServico_percpgm_Jsonclick = "";
         edtavServico_perccnc_Jsonclick = "";
         edtavServicofluxo_sigla_Jsonclick = "";
         edtavServicofluxo_nome_Jsonclick = "";
         dynavServicofluxo_servicopos_Jsonclick = "";
         lblTbjava_Visible = 1;
         radavManterordem.Visible = 1;
         lblTbjava_Caption = "tbJava";
         lblTbservico_Caption = "Servi�o";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Processos do Servi�o";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOCONFIRMAR'","{handler:'E12LD2',iparms:[{av:'AV12ServicoFluxo_ServicoPos',fld:'vSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'AV23ServicoFluxo_Nome',fld:'vSERVICOFLUXO_NOME',pic:'@!',nv:''},{av:'AV22ServicoFluxo_Sigla',fld:'vSERVICOFLUXO_SIGLA',pic:'@!',nv:''},{av:'AV16Servico_Descricao',fld:'vSERVICO_DESCRICAO',pic:'',nv:''},{av:'AV17Servico_PercTmp',fld:'vSERVICO_PERCTMP',pic:'ZZZ9',nv:0},{av:'AV18Servico_PercPgm',fld:'vSERVICO_PERCPGM',pic:'ZZZ9',nv:0},{av:'AV19Servico_PercCnc',fld:'vSERVICO_PERCCNC',pic:'ZZZ9',nv:0},{av:'AV5Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1436Servico_ObjetoControle',fld:'SERVICO_OBJETOCONTROLE',pic:'',nv:''},{av:'A1429Servico_ObrigaValores',fld:'SERVICO_OBRIGAVALORES',pic:'',nv:''},{av:'A633Servico_UO',fld:'SERVICO_UO',pic:'ZZZZZ9',nv:0},{av:'A1077Servico_UORespExclusiva',fld:'SERVICO_UORESPEXCLUSIVA',pic:'',nv:false},{av:'A157ServicoGrupo_Codigo',fld:'SERVICOGRUPO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV14ManterOrdem',fld:'vMANTERORDEM',pic:'',nv:false},{av:'AV13ServicoFluxo_Ordem',fld:'vSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E13LD2',iparms:[],oparms:[]}");
         setEventMetadata("VSERVICOFLUXO_SERVICOPOS.CLICK","{handler:'E15LD2',iparms:[{av:'AV12ServicoFluxo_ServicoPos',fld:'vSERVICOFLUXO_SERVICOPOS',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A608Servico_Nome',fld:'SERVICO_NOME',pic:'@!',nv:''},{av:'A605Servico_Sigla',fld:'SERVICO_SIGLA',pic:'@!',nv:''},{av:'A156Servico_Descricao',fld:'SERVICO_DESCRICAO',pic:'',nv:''},{av:'A1534Servico_PercTmp',fld:'SERVICO_PERCTMP',pic:'ZZZ9',nv:0},{av:'A1535Servico_PercPgm',fld:'SERVICO_PERCPGM',pic:'ZZZ9',nv:0},{av:'A1536Servico_PercCnc',fld:'SERVICO_PERCCNC',pic:'ZZZ9',nv:0}],oparms:[{av:'AV23ServicoFluxo_Nome',fld:'vSERVICOFLUXO_NOME',pic:'@!',nv:''},{av:'AV22ServicoFluxo_Sigla',fld:'vSERVICOFLUXO_SIGLA',pic:'@!',nv:''},{av:'AV16Servico_Descricao',fld:'vSERVICO_DESCRICAO',pic:'',nv:''},{av:'AV17Servico_PercTmp',fld:'vSERVICO_PERCTMP',pic:'ZZZ9',nv:0},{av:'AV18Servico_PercPgm',fld:'vSERVICO_PERCPGM',pic:'ZZZ9',nv:0},{av:'AV19Servico_PercCnc',fld:'vSERVICO_PERCCNC',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("VSERVICOFLUXO_ORDEM.ISVALID","{handler:'E16LD2',iparms:[{av:'A1522ServicoFluxo_ServicoCod',fld:'SERVICOFLUXO_SERVICOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1532ServicoFluxo_Ordem',fld:'SERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0},{av:'AV13ServicoFluxo_Ordem',fld:'vSERVICOFLUXO_ORDEM',pic:'ZZ9',nv:0}],oparms:[{av:'radavManterordem'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV6Servico_Sigla = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1436Servico_ObjetoControle = "";
         A1429Servico_ObrigaValores = "";
         A608Servico_Nome = "";
         A605Servico_Sigla = "";
         A156Servico_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00LD2_A155Servico_Codigo = new int[1] ;
         H00LD2_A605Servico_Sigla = new String[] {""} ;
         H00LD2_A631Servico_Vinculado = new int[1] ;
         H00LD2_n631Servico_Vinculado = new bool[] {false} ;
         H00LD2_A1530Servico_TipoHierarquia = new short[1] ;
         H00LD2_n1530Servico_TipoHierarquia = new bool[] {false} ;
         AV23ServicoFluxo_Nome = "";
         AV22ServicoFluxo_Sigla = "";
         AV16Servico_Descricao = "";
         H00LD4_A1528ServicoFluxo_Codigo = new int[1] ;
         H00LD4_A1522ServicoFluxo_ServicoCod = new int[1] ;
         H00LD4_n1522ServicoFluxo_ServicoCod = new bool[] {false} ;
         H00LD4_A40000GXC1 = new int[1] ;
         H00LD4_n40000GXC1 = new bool[] {false} ;
         AV21Servico = new SdtServico(context);
         H00LD5_A155Servico_Codigo = new int[1] ;
         H00LD5_A1436Servico_ObjetoControle = new String[] {""} ;
         H00LD5_A1429Servico_ObrigaValores = new String[] {""} ;
         H00LD5_n1429Servico_ObrigaValores = new bool[] {false} ;
         H00LD5_A633Servico_UO = new int[1] ;
         H00LD5_n633Servico_UO = new bool[] {false} ;
         H00LD5_A1077Servico_UORespExclusiva = new bool[] {false} ;
         H00LD5_n1077Servico_UORespExclusiva = new bool[] {false} ;
         H00LD5_A157ServicoGrupo_Codigo = new int[1] ;
         AV20ServicoFluxo = new SdtServicoFluxo(context);
         AV9Context = new wwpbaseobjects.SdtWWPContext(context);
         H00LD6_A155Servico_Codigo = new int[1] ;
         H00LD6_A608Servico_Nome = new String[] {""} ;
         H00LD6_A605Servico_Sigla = new String[] {""} ;
         H00LD6_A156Servico_Descricao = new String[] {""} ;
         H00LD6_n156Servico_Descricao = new bool[] {false} ;
         H00LD6_A1534Servico_PercTmp = new short[1] ;
         H00LD6_n1534Servico_PercTmp = new bool[] {false} ;
         H00LD6_A1535Servico_PercPgm = new short[1] ;
         H00LD6_n1535Servico_PercPgm = new bool[] {false} ;
         H00LD6_A1536Servico_PercCnc = new short[1] ;
         H00LD6_n1536Servico_PercCnc = new bool[] {false} ;
         H00LD7_A1528ServicoFluxo_Codigo = new int[1] ;
         H00LD7_A1532ServicoFluxo_Ordem = new short[1] ;
         H00LD7_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         H00LD7_A1522ServicoFluxo_ServicoCod = new int[1] ;
         H00LD7_n1522ServicoFluxo_ServicoCod = new bool[] {false} ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbservico_Jsonclick = "";
         lblTbjava_Jsonclick = "";
         TempTags = "";
         bttBtnconfirmar_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockservicofluxo_servicopos_Jsonclick = "";
         lblTextblockservicofluxo_ordem_Jsonclick = "";
         lblTextblockservicofluxo_nome_Jsonclick = "";
         lblTextblockservicofluxo_sigla_Jsonclick = "";
         lblTextblockservico_descricao_Jsonclick = "";
         lblTextblockservico_perctmp_Jsonclick = "";
         lblTextblockservico_percpgm_Jsonclick = "";
         lblTextblockservico_perccnc_Jsonclick = "";
         lblServico_perccnc_righttext_Jsonclick = "";
         lblServico_percpgm_righttext_Jsonclick = "";
         lblServico_perctmp_righttext_Jsonclick = "";
         lblTextblockmanterordem_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_processo__default(),
            new Object[][] {
                new Object[] {
               H00LD2_A155Servico_Codigo, H00LD2_A605Servico_Sigla, H00LD2_A631Servico_Vinculado, H00LD2_n631Servico_Vinculado, H00LD2_A1530Servico_TipoHierarquia, H00LD2_n1530Servico_TipoHierarquia
               }
               , new Object[] {
               H00LD4_A1528ServicoFluxo_Codigo, H00LD4_A1522ServicoFluxo_ServicoCod, H00LD4_n1522ServicoFluxo_ServicoCod, H00LD4_A40000GXC1, H00LD4_n40000GXC1
               }
               , new Object[] {
               H00LD5_A155Servico_Codigo, H00LD5_A1436Servico_ObjetoControle, H00LD5_A1429Servico_ObrigaValores, H00LD5_n1429Servico_ObrigaValores, H00LD5_A633Servico_UO, H00LD5_n633Servico_UO, H00LD5_A1077Servico_UORespExclusiva, H00LD5_n1077Servico_UORespExclusiva, H00LD5_A157ServicoGrupo_Codigo
               }
               , new Object[] {
               H00LD6_A155Servico_Codigo, H00LD6_A608Servico_Nome, H00LD6_A605Servico_Sigla, H00LD6_A156Servico_Descricao, H00LD6_n156Servico_Descricao, H00LD6_A1534Servico_PercTmp, H00LD6_n1534Servico_PercTmp, H00LD6_A1535Servico_PercPgm, H00LD6_n1535Servico_PercPgm, H00LD6_A1536Servico_PercCnc,
               H00LD6_n1536Servico_PercCnc
               }
               , new Object[] {
               H00LD7_A1528ServicoFluxo_Codigo, H00LD7_A1532ServicoFluxo_Ordem, H00LD7_n1532ServicoFluxo_Ordem, H00LD7_A1522ServicoFluxo_ServicoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nRcdExists_5 ;
      private short nIsMod_5 ;
      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short A1534Servico_PercTmp ;
      private short A1535Servico_PercPgm ;
      private short A1536Servico_PercCnc ;
      private short A1532ServicoFluxo_Ordem ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV13ServicoFluxo_Ordem ;
      private short AV17Servico_PercTmp ;
      private short AV18Servico_PercPgm ;
      private short AV19Servico_PercCnc ;
      private short nGXWrapped ;
      private int AV5Servico_Codigo ;
      private int wcpOAV5Servico_Codigo ;
      private int A155Servico_Codigo ;
      private int A633Servico_UO ;
      private int A157ServicoGrupo_Codigo ;
      private int A1522ServicoFluxo_ServicoCod ;
      private int gxdynajaxindex ;
      private int AV12ServicoFluxo_ServicoPos ;
      private int lblTbjava_Visible ;
      private int A40000GXC1 ;
      private int idxLst ;
      private String AV6Servico_Sigla ;
      private String wcpOAV6Servico_Sigla ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A1436Servico_ObjetoControle ;
      private String A1429Servico_ObrigaValores ;
      private String A608Servico_Nome ;
      private String A605Servico_Sigla ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String dynavServicofluxo_servicopos_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavServicofluxo_ordem_Internalname ;
      private String radavManterordem_Internalname ;
      private String AV23ServicoFluxo_Nome ;
      private String edtavServicofluxo_nome_Internalname ;
      private String AV22ServicoFluxo_Sigla ;
      private String edtavServicofluxo_sigla_Internalname ;
      private String edtavServico_descricao_Internalname ;
      private String edtavServico_perctmp_Internalname ;
      private String edtavServico_percpgm_Internalname ;
      private String edtavServico_perccnc_Internalname ;
      private String lblTbservico_Caption ;
      private String lblTbservico_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTbservico_Jsonclick ;
      private String lblTbjava_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnconfirmar_Internalname ;
      private String bttBtnconfirmar_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockservicofluxo_servicopos_Internalname ;
      private String lblTextblockservicofluxo_servicopos_Jsonclick ;
      private String dynavServicofluxo_servicopos_Jsonclick ;
      private String lblTextblockservicofluxo_ordem_Internalname ;
      private String lblTextblockservicofluxo_ordem_Jsonclick ;
      private String lblTextblockservicofluxo_nome_Internalname ;
      private String lblTextblockservicofluxo_nome_Jsonclick ;
      private String edtavServicofluxo_nome_Jsonclick ;
      private String lblTextblockservicofluxo_sigla_Internalname ;
      private String lblTextblockservicofluxo_sigla_Jsonclick ;
      private String edtavServicofluxo_sigla_Jsonclick ;
      private String lblTextblockservico_descricao_Internalname ;
      private String lblTextblockservico_descricao_Jsonclick ;
      private String lblTextblockservico_perctmp_Internalname ;
      private String lblTextblockservico_perctmp_Jsonclick ;
      private String lblTextblockservico_percpgm_Internalname ;
      private String lblTextblockservico_percpgm_Jsonclick ;
      private String lblTextblockservico_perccnc_Internalname ;
      private String lblTextblockservico_perccnc_Jsonclick ;
      private String tblTablemergedservico_perccnc_Internalname ;
      private String edtavServico_perccnc_Jsonclick ;
      private String lblServico_perccnc_righttext_Internalname ;
      private String lblServico_perccnc_righttext_Jsonclick ;
      private String tblTablemergedservico_percpgm_Internalname ;
      private String edtavServico_percpgm_Jsonclick ;
      private String lblServico_percpgm_righttext_Internalname ;
      private String lblServico_percpgm_righttext_Jsonclick ;
      private String tblTablemergedservico_perctmp_Internalname ;
      private String edtavServico_perctmp_Jsonclick ;
      private String lblServico_perctmp_righttext_Internalname ;
      private String lblServico_perctmp_righttext_Jsonclick ;
      private String tblTablemergedservicofluxo_ordem_Internalname ;
      private String edtavServicofluxo_ordem_Jsonclick ;
      private String lblTextblockmanterordem_Internalname ;
      private String lblTextblockmanterordem_Jsonclick ;
      private String radavManterordem_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1077Servico_UORespExclusiva ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV14ManterOrdem ;
      private bool returnInSub ;
      private bool n1522ServicoFluxo_ServicoCod ;
      private bool n40000GXC1 ;
      private bool n1429Servico_ObrigaValores ;
      private bool n633Servico_UO ;
      private bool n1077Servico_UORespExclusiva ;
      private bool n156Servico_Descricao ;
      private bool n1534Servico_PercTmp ;
      private bool n1535Servico_PercPgm ;
      private bool n1536Servico_PercCnc ;
      private bool n1532ServicoFluxo_Ordem ;
      private String A156Servico_Descricao ;
      private String AV16Servico_Descricao ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavServicofluxo_servicopos ;
      private GXRadio radavManterordem ;
      private IDataStoreProvider pr_default ;
      private int[] H00LD2_A155Servico_Codigo ;
      private String[] H00LD2_A605Servico_Sigla ;
      private int[] H00LD2_A631Servico_Vinculado ;
      private bool[] H00LD2_n631Servico_Vinculado ;
      private short[] H00LD2_A1530Servico_TipoHierarquia ;
      private bool[] H00LD2_n1530Servico_TipoHierarquia ;
      private int[] H00LD4_A1528ServicoFluxo_Codigo ;
      private int[] H00LD4_A1522ServicoFluxo_ServicoCod ;
      private bool[] H00LD4_n1522ServicoFluxo_ServicoCod ;
      private int[] H00LD4_A40000GXC1 ;
      private bool[] H00LD4_n40000GXC1 ;
      private int[] H00LD5_A155Servico_Codigo ;
      private String[] H00LD5_A1436Servico_ObjetoControle ;
      private String[] H00LD5_A1429Servico_ObrigaValores ;
      private bool[] H00LD5_n1429Servico_ObrigaValores ;
      private int[] H00LD5_A633Servico_UO ;
      private bool[] H00LD5_n633Servico_UO ;
      private bool[] H00LD5_A1077Servico_UORespExclusiva ;
      private bool[] H00LD5_n1077Servico_UORespExclusiva ;
      private int[] H00LD5_A157ServicoGrupo_Codigo ;
      private int[] H00LD6_A155Servico_Codigo ;
      private String[] H00LD6_A608Servico_Nome ;
      private String[] H00LD6_A605Servico_Sigla ;
      private String[] H00LD6_A156Servico_Descricao ;
      private bool[] H00LD6_n156Servico_Descricao ;
      private short[] H00LD6_A1534Servico_PercTmp ;
      private bool[] H00LD6_n1534Servico_PercTmp ;
      private short[] H00LD6_A1535Servico_PercPgm ;
      private bool[] H00LD6_n1535Servico_PercPgm ;
      private short[] H00LD6_A1536Servico_PercCnc ;
      private bool[] H00LD6_n1536Servico_PercCnc ;
      private int[] H00LD7_A1528ServicoFluxo_Codigo ;
      private short[] H00LD7_A1532ServicoFluxo_Ordem ;
      private bool[] H00LD7_n1532ServicoFluxo_Ordem ;
      private int[] H00LD7_A1522ServicoFluxo_ServicoCod ;
      private bool[] H00LD7_n1522ServicoFluxo_ServicoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV9Context ;
      private SdtServico AV21Servico ;
      private SdtServicoFluxo AV20ServicoFluxo ;
   }

   public class wp_processo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00LD2 ;
          prmH00LD2 = new Object[] {
          new Object[] {"@AV5Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00LD4 ;
          prmH00LD4 = new Object[] {
          new Object[] {"@AV5Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00LD5 ;
          prmH00LD5 = new Object[] {
          new Object[] {"@AV5Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00LD6 ;
          prmH00LD6 = new Object[] {
          new Object[] {"@AV12ServicoFluxo_ServicoPos",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00LD7 ;
          prmH00LD7 = new Object[] {
          new Object[] {"@AV5Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13ServicoFluxo_Ordem",SqlDbType.SmallInt,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00LD2", "SELECT [Servico_Codigo], [Servico_Sigla], [Servico_Vinculado], [Servico_TipoHierarquia] FROM [Servico] WITH (NOLOCK) WHERE ([Servico_Vinculado] = @AV5Servico_Codigo) AND ([Servico_TipoHierarquia] = 2) ORDER BY [Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LD2,0,0,true,false )
             ,new CursorDef("H00LD4", "SELECT T1.[ServicoFluxo_Codigo], T1.[ServicoFluxo_ServicoCod], COALESCE( T2.[GXC1], 0) AS GXC1 FROM ([ServicoFluxo] T1 WITH (NOLOCK) LEFT JOIN (SELECT COUNT(*) AS GXC1, [ServicoFluxo_ServicoCod] FROM [ServicoFluxo] WITH (NOLOCK) GROUP BY [ServicoFluxo_ServicoCod] ) T2 ON T2.[ServicoFluxo_ServicoCod] = T1.[ServicoFluxo_ServicoCod]) WHERE T1.[ServicoFluxo_ServicoCod] = @AV5Servico_Codigo ORDER BY T1.[ServicoFluxo_ServicoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LD4,100,0,false,false )
             ,new CursorDef("H00LD5", "SELECT TOP 1 [Servico_Codigo], [Servico_ObjetoControle], [Servico_ObrigaValores], [Servico_UO], [Servico_UORespExclusiva], [ServicoGrupo_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV5Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LD5,1,0,false,true )
             ,new CursorDef("H00LD6", "SELECT TOP 1 [Servico_Codigo], [Servico_Nome], [Servico_Sigla], [Servico_Descricao], [Servico_PercTmp], [Servico_PercPgm], [Servico_PercCnc] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV12ServicoFluxo_ServicoPos ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LD6,1,0,false,true )
             ,new CursorDef("H00LD7", "SELECT TOP 1 [ServicoFluxo_Codigo], [ServicoFluxo_Ordem], [ServicoFluxo_ServicoCod] FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_ServicoCod] = @AV5Servico_Codigo and [ServicoFluxo_Ordem] = @AV13ServicoFluxo_Ordem ORDER BY [ServicoFluxo_ServicoCod], [ServicoFluxo_Ordem] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LD7,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 3) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((bool[]) buf[6])[0] = rslt.getBool(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
       }
    }

 }

}
