/*
               File: GAMExampleLogin
        Description: Login
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:31:55.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexamplelogin : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public gamexamplelogin( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexamplelogin( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavLogonto = new GXCombobox();
         chkavKeepmeloggedin = new GXCheckbox();
         chkavRememberme = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA1S2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS1S2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE1S2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Login") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117315868");
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexamplelogin.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm1S2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleLogin" ;
      }

      public override String GetPgmdesc( )
      {
         return "Login" ;
      }

      protected void WB1S0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_1S2( true) ;
         }
         else
         {
            wb_table1_2_1S2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_1S2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START1S2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Login", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP1S0( ) ;
      }

      protected void WS1S2( )
      {
         START1S2( ) ;
         EVT1S2( ) ;
      }

      protected void EVT1S2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111S2 */
                           E111S2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121S2 */
                           E121S2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E131S2 */
                                 E131S2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'FORGOTPASSWORD'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E141S2 */
                           E141S2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VBUTTONFACEBOOK.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E151S2 */
                           E151S2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VBUTTONGOOGLE.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E161S2 */
                           E161S2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VBUTTONTWITTER.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E171S2 */
                           E171S2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VBUTTONGAMREMOTE.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E181S2 */
                           E181S2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E191S2 */
                           E191S2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE1S2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm1S2( ) ;
            }
         }
      }

      protected void PA1S2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavLogonto.Name = "vLOGONTO";
            cmbavLogonto.WebTags = "";
            if ( cmbavLogonto.ItemCount > 0 )
            {
               AV27LogOnTo = cmbavLogonto.getValidValue(AV27LogOnTo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LogOnTo", AV27LogOnTo);
            }
            chkavKeepmeloggedin.Name = "vKEEPMELOGGEDIN";
            chkavKeepmeloggedin.WebTags = "";
            chkavKeepmeloggedin.Caption = "Keep me logged in";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "TitleCaption", chkavKeepmeloggedin.Caption);
            chkavKeepmeloggedin.CheckedValue = "false";
            chkavRememberme.Name = "vREMEMBERME";
            chkavRememberme.WebTags = "";
            chkavRememberme.Caption = "Remember me";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "TitleCaption", chkavRememberme.Caption);
            chkavRememberme.CheckedValue = "false";
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavLogonto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavLogonto.ItemCount > 0 )
         {
            AV27LogOnTo = cmbavLogonto.getValidValue(AV27LogOnTo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LogOnTo", AV27LogOnTo);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF1S2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF1S2( )
      {
         /* Execute user event: E121S2 */
         E121S2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E191S2 */
            E191S2 ();
            WB1S0( ) ;
         }
      }

      protected void STRUP1S0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111S2 */
         E111S2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV10ButtonFacebook = cgiGet( imgavButtonfacebook_Internalname);
            AV12ButtonGoogle = cgiGet( imgavButtongoogle_Internalname);
            AV13ButtonTwitter = cgiGet( imgavButtontwitter_Internalname);
            AV11ButtonGAMRemote = cgiGet( imgavButtongamremote_Internalname);
            cmbavLogonto.CurrentValue = cgiGet( cmbavLogonto_Internalname);
            AV27LogOnTo = cgiGet( cmbavLogonto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LogOnTo", AV27LogOnTo);
            AV33UserName = cgiGet( edtavUsername_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33UserName", AV33UserName);
            AV34UserPassword = cgiGet( edtavUserpassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34UserPassword", AV34UserPassword);
            AV23KeepMeLoggedIn = StringUtil.StrToBool( cgiGet( chkavKeepmeloggedin_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23KeepMeLoggedIn", AV23KeepMeLoggedIn);
            AV28RememberMe = StringUtil.StrToBool( cgiGet( chkavRememberme_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28RememberMe", AV28RememberMe);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E111S2 */
         E111S2 ();
         if (returnInSub) return;
      }

      protected void E111S2( )
      {
         /* Start Routine */
         AV15ConnectionInfoCollection = new SdtGAM(context).getconnections();
         if ( ( AV15ConnectionInfoCollection.Count > 0 ) && (0==new SdtGAMRepository(context).getid()) )
         {
            AV21isOK = new SdtGAM(context).setconnection(((SdtGAMConnectionInfo)AV15ConnectionInfoCollection.Item(1)).gxTpr_Name, out  AV18Errors);
         }
         imgavButtonfacebook_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtonfacebook_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtonfacebook_Visible), 5, 0)));
         imgavButtongoogle_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongoogle_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtongoogle_Visible), 5, 0)));
         imgavButtontwitter_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtontwitter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtontwitter_Visible), 5, 0)));
         imgavButtongamremote_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongamremote_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtongamremote_Visible), 5, 0)));
         if ( new SdtGAMRepository(context).canauthenticatewith("Facebook") )
         {
            imgavButtonfacebook_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtonfacebook_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtonfacebook_Visible), 5, 0)));
            AV10ButtonFacebook = context.GetImagePath( "1965aed6-daad-4687-8b71-4797860e630c", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtonfacebook_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV10ButtonFacebook)) ? AV38Buttonfacebook_GXI : context.convertURL( context.PathToRelativeUrl( AV10ButtonFacebook))));
            AV38Buttonfacebook_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "1965aed6-daad-4687-8b71-4797860e630c", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtonfacebook_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV10ButtonFacebook)) ? AV38Buttonfacebook_GXI : context.convertURL( context.PathToRelativeUrl( AV10ButtonFacebook))));
         }
         if ( new SdtGAMRepository(context).canauthenticatewith("Google") )
         {
            imgavButtongoogle_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongoogle_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtongoogle_Visible), 5, 0)));
            AV12ButtonGoogle = context.GetImagePath( "57720a5a-eb93-4911-895a-35207ed2dd03", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongoogle_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV12ButtonGoogle)) ? AV39Buttongoogle_GXI : context.convertURL( context.PathToRelativeUrl( AV12ButtonGoogle))));
            AV39Buttongoogle_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "57720a5a-eb93-4911-895a-35207ed2dd03", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongoogle_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV12ButtonGoogle)) ? AV39Buttongoogle_GXI : context.convertURL( context.PathToRelativeUrl( AV12ButtonGoogle))));
         }
         if ( new SdtGAMRepository(context).canauthenticatewith("Twitter") )
         {
            imgavButtontwitter_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtontwitter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtontwitter_Visible), 5, 0)));
            AV13ButtonTwitter = context.GetImagePath( "5b513ef3-c434-4c8c-aacb-feefd834b2ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtontwitter_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV13ButtonTwitter)) ? AV40Buttontwitter_GXI : context.convertURL( context.PathToRelativeUrl( AV13ButtonTwitter))));
            AV40Buttontwitter_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "5b513ef3-c434-4c8c-aacb-feefd834b2ee", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtontwitter_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV13ButtonTwitter)) ? AV40Buttontwitter_GXI : context.convertURL( context.PathToRelativeUrl( AV13ButtonTwitter))));
         }
         if ( new SdtGAMRepository(context).canauthenticatewith("GAMRemote") )
         {
            imgavButtongamremote_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongamremote_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgavButtongamremote_Visible), 5, 0)));
            AV11ButtonGAMRemote = context.GetImagePath( "6cdd3e18-cc5b-44e0-bd22-3efaf48a6c40", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongamremote_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV11ButtonGAMRemote)) ? AV41Buttongamremote_GXI : context.convertURL( context.PathToRelativeUrl( AV11ButtonGAMRemote))));
            AV41Buttongamremote_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6cdd3e18-cc5b-44e0-bd22-3efaf48a6c40", "", context.GetTheme( )));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgavButtongamremote_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV11ButtonGAMRemote)) ? AV41Buttongamremote_GXI : context.convertURL( context.PathToRelativeUrl( AV11ButtonGAMRemote))));
         }
      }

      protected void E121S2( )
      {
         /* Refresh Routine */
         AV22isRedirect = false;
         AV19ErrorsLogin = new SdtGAMRepository(context).getlasterrors();
         if ( AV19ErrorsLogin.Count > 0 )
         {
            if ( ((SdtGAMError)AV19ErrorsLogin.Item(1)).gxTpr_Code == 161 )
            {
               context.wjLoc = formatLink("gamexampleupdateregisteruser.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
               context.wjLocDisableFrm = 1;
               AV22isRedirect = true;
            }
            else
            {
               AV34UserPassword = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34UserPassword", AV34UserPassword);
               AV18Errors = AV19ErrorsLogin;
               /* Execute user subroutine: 'DISPLAYMESSAGES' */
               S112 ();
               if (returnInSub) return;
            }
         }
         if ( ! AV22isRedirect )
         {
            AV31SessionValid = new SdtGAMSession(context).isvalid(out  AV30Session, out  AV18Errors);
            if ( AV31SessionValid && ! AV30Session.gxTpr_Isanonymous )
            {
               AV32URL = new SdtGAMRepository(context).getlasterrorsurl();
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32URL)) )
               {
                  context.wjLoc = formatLink("gamhome.aspx") ;
                  context.wjLocDisableFrm = 1;
               }
               else
               {
                  context.wjLoc = formatLink(AV32URL) ;
                  context.wjLocDisableFrm = 0;
               }
            }
            else
            {
               cmbavLogonto.removeAllItems();
               AV9AuthenticationTypes = new SdtGAMRepository(context).getenabledauthenticationtypes(AV24Language, out  AV18Errors);
               AV42GXV1 = 1;
               while ( AV42GXV1 <= AV9AuthenticationTypes.Count )
               {
                  AV8AuthenticationType = ((SdtGAMAuthenticationTypeSimple)AV9AuthenticationTypes.Item(AV42GXV1));
                  if ( AV8AuthenticationType.gxTpr_Needusername )
                  {
                     cmbavLogonto.addItem(AV8AuthenticationType.gxTpr_Name, AV8AuthenticationType.gxTpr_Description, 0);
                  }
                  AV42GXV1 = (int)(AV42GXV1+1);
               }
               if ( cmbavLogonto.ItemCount <= 1 )
               {
                  lblTblogonto_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTblogonto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTblogonto_Visible), 5, 0)));
                  cmbavLogonto.Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLogonto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavLogonto.Visible), 5, 0)));
               }
               AV21isOK = new SdtGAMRepository(context).getrememberlogin(out  AV27LogOnTo, out  AV33UserName, out  AV35UserRememberMe, out  AV18Errors);
               if ( AV35UserRememberMe == 2 )
               {
                  AV28RememberMe = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28RememberMe", AV28RememberMe);
               }
               AV29Repository = new SdtGAMRepository(context).get();
               if ( cmbavLogonto.ItemCount > 1 )
               {
                  AV27LogOnTo = AV29Repository.gxTpr_Defaultauthenticationtypename;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LogOnTo", AV27LogOnTo);
               }
               chkavRememberme.Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)));
               chkavKeepmeloggedin.Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)));
               if ( StringUtil.StrCmp(AV29Repository.gxTpr_Userremembermetype, "Login") == 0 )
               {
                  chkavRememberme.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)));
               }
               else if ( StringUtil.StrCmp(AV29Repository.gxTpr_Userremembermetype, "Auth") == 0 )
               {
                  chkavKeepmeloggedin.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)));
               }
               else if ( StringUtil.StrCmp(AV29Repository.gxTpr_Userremembermetype, "Both") == 0 )
               {
                  chkavRememberme.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)));
                  chkavKeepmeloggedin.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)));
               }
            }
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E131S2 */
         E131S2 ();
         if (returnInSub) return;
      }

      protected void E131S2( )
      {
         /* Enter Routine */
         if ( AV23KeepMeLoggedIn )
         {
            AV5AdditionalParameter.gxTpr_Rememberusertype = (short)((AV23KeepMeLoggedIn ? 3 : 1));
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
         }
         else
         {
            if ( AV28RememberMe )
            {
               AV5AdditionalParameter.gxTpr_Rememberusertype = (short)((AV28RememberMe ? 2 : 1));
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
            }
            else
            {
               AV5AdditionalParameter.gxTpr_Rememberusertype = 1;
               context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
            }
         }
         AV5AdditionalParameter.gxTpr_Authenticationtypename = AV27LogOnTo;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
         AV25LoginOK = new SdtGAMRepository(context).login(AV33UserName, AV34UserPassword, AV5AdditionalParameter, out  AV18Errors);
         if ( AV25LoginOK )
         {
            AV7ApplicationData = new SdtGAMSession(context).getapplicationdata();
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7ApplicationData)) )
            {
               AV20GAMExampleSDTApplicationData.FromJSonString(AV7ApplicationData);
            }
         }
         else
         {
            if ( AV18Errors.Count > 0 )
            {
               if ( ( ((SdtGAMError)AV18Errors.Item(1)).gxTpr_Code == 24 ) || ( ((SdtGAMError)AV18Errors.Item(1)).gxTpr_Code == 23 ) )
               {
                  context.wjLoc = formatLink("gamexamplechangepassword.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV27LogOnTo)) + "," + UrlEncode(StringUtil.RTrim(AV33UserName)) + "," + UrlEncode("" +AV35UserRememberMe) + "," + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
                  context.wjLocDisableFrm = 1;
               }
               else if ( ((SdtGAMError)AV18Errors.Item(1)).gxTpr_Code == 161 )
               {
                  context.wjLoc = formatLink("gamexampleupdateregisteruser.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
                  context.wjLocDisableFrm = 1;
               }
            }
         }
      }

      protected void S112( )
      {
         /* 'DISPLAYMESSAGES' Routine */
         AV43GXV2 = 1;
         while ( AV43GXV2 <= AV18Errors.Count )
         {
            AV17Error = ((SdtGAMError)AV18Errors.Item(AV43GXV2));
            if ( AV17Error.gxTpr_Code != 13 )
            {
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV17Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            }
            AV43GXV2 = (int)(AV43GXV2+1);
         }
      }

      protected void E141S2( )
      {
         /* 'ForgotPassword' Routine */
         context.wjLoc = formatLink("gamexamplerecoverpasswordstep1.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId));
         context.wjLocDisableFrm = 1;
      }

      protected void E151S2( )
      {
         /* Buttonfacebook_Click Routine */
         new SdtGAMRepository(context).loginfacebook() ;
      }

      protected void E161S2( )
      {
         /* Buttongoogle_Click Routine */
         new SdtGAMRepository(context).logingoogle() ;
      }

      protected void E171S2( )
      {
         /* Buttontwitter_Click Routine */
         new SdtGAMRepository(context).logintwitter() ;
      }

      protected void E181S2( )
      {
         /* Buttongamremote_Click Routine */
         new SdtGAMRepository(context).logingamremote() ;
      }

      protected void nextLoad( )
      {
      }

      protected void E191S2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_1S2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(95), 10, 0)) + "%" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(95), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblmain_Internalname, tblTblmain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            wb_table2_6_1S2( true) ;
         }
         else
         {
            wb_table2_6_1S2( false) ;
         }
         return  ;
      }

      protected void wb_table2_6_1S2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1S2e( true) ;
         }
         else
         {
            wb_table1_2_1S2e( false) ;
         }
      }

      protected void wb_table2_6_1S2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + "BACKGROUND-IMAGE: url(" + context.convertURL( "f8ced90a-a0e4-4bc3-9a3d-0e3a94f040d5") + ");";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(325), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTbllogin_Internalname, tblTbllogin_Internalname, "", "TableLogin", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='HeaderLogin'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbtitle_Internalname, "Sign in", "", "", lblTbtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "color:#FFFFFF;", "Title", 0, "", 1, 1, 0, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" background=\""+context.convertURL( context.GetImagePath( "f8ced90a-a0e4-4bc3-9a3d-0e3a94f040d5", "", context.GetTheme( )))+"\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:262px")+"\">") ;
            wb_table3_12_1S2( true) ;
         }
         else
         {
            wb_table3_12_1S2( false) ;
         }
         return  ;
      }

      protected void wb_table3_12_1S2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_6_1S2e( true) ;
         }
         else
         {
            wb_table2_6_1S2e( false) ;
         }
      }

      protected void wb_table3_12_1S2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(85), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblfields_Internalname, tblTblfields_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\">") ;
            wb_table4_17_1S2( true) ;
         }
         else
         {
            wb_table4_17_1S2( false) ;
         }
         return  ;
      }

      protected void wb_table4_17_1S2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom;height:25px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTblogonto_Internalname, "Log on to", "", "", lblTblogonto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTblogonto_Visible, 1, 0, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavLogonto, cmbavLogonto_Internalname, StringUtil.RTrim( AV27LogOnTo), 1, cmbavLogonto_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavLogonto.Visible, 1, 0, 0, 270, "px", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_GAMExampleLogin.htm");
            cmbavLogonto.CurrentValue = StringUtil.RTrim( AV27LogOnTo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLogonto_Internalname, "Values", (String)(cmbavLogonto.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom;height:25px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbusername_Internalname, "Email or name", "", "", lblTbusername_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsername_Internalname, AV33UserName, StringUtil.RTrim( context.localUtil.Format( AV33UserName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsername_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 270, "px", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GAMUserIdentification", "left", true, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom;height:25px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbpassword_Internalname, "Password", "", "", lblTbpassword_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserpassword_Internalname, StringUtil.RTrim( AV34UserPassword), StringUtil.RTrim( context.localUtil.Format( AV34UserPassword, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserpassword_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 270, "px", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;height:25px")+"\">") ;
            wb_table5_50_1S2( true) ;
         }
         else
         {
            wb_table5_50_1S2( false) ;
         }
         return  ;
      }

      protected void wb_table5_50_1S2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttLogin_Internalname, "", "Login", bttLogin_Jsonclick, 5, "Login", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:20px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbrememberme2_Internalname, "FORGOT PASSWORD?", "", "", lblTbrememberme2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'FORGOTPASSWORD\\'."+"'", "font-family:'Microsoft Sans Serif'; font-size:6.0pt; font-weight:normal; font-style:normal;", "Label", 5, "", 1, 1, 0, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_12_1S2e( true) ;
         }
         else
         {
            wb_table3_12_1S2e( false) ;
         }
      }

      protected void wb_table5_50_1S2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblrem_Internalname, tblTblrem_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "font-family:'Verdana'; font-size:7.0pt; font-weight:normal; font-style:normal;";
            GxWebStd.gx_checkbox_ctrl( context, chkavKeepmeloggedin_Internalname, StringUtil.BoolToStr( AV23KeepMeLoggedIn), "", "", chkavKeepmeloggedin.Visible, 1, "true", "Keep me logged in", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(53, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "font-family:'Verdana'; font-size:7.0pt; font-weight:normal; font-style:normal;";
            GxWebStd.gx_checkbox_ctrl( context, chkavRememberme_Internalname, StringUtil.BoolToStr( AV28RememberMe), "", "", chkavRememberme.Visible, 1, "true", "Remember me", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(56, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_50_1S2e( true) ;
         }
         else
         {
            wb_table5_50_1S2e( false) ;
         }
      }

      protected void wb_table4_17_1S2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblextauth_Internalname, tblTblextauth_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:37px")+"\">") ;
            /* Active Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavButtonfacebook_Internalname, AV10ButtonFacebook);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            AV10ButtonFacebook_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV10ButtonFacebook))&&String.IsNullOrEmpty(StringUtil.RTrim( AV38Buttonfacebook_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV10ButtonFacebook)));
            GxWebStd.gx_bitmap( context, imgavButtonfacebook_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV10ButtonFacebook)) ? AV38Buttonfacebook_GXI : context.PathToRelativeUrl( AV10ButtonFacebook)), "", "", "", context.GetTheme( ), imgavButtonfacebook_Visible, 1, "", "Sign in with Facebook", 0, -1, 0, "", 0, "", 0, 0, 5, imgavButtonfacebook_Jsonclick, "'"+""+"'"+",false,"+"'"+"EVBUTTONFACEBOOK.CLICK."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, AV10ButtonFacebook_IsBlob, false, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:65px")+"\">") ;
            /* Active Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavButtongoogle_Internalname, AV12ButtonGoogle);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            AV12ButtonGoogle_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV12ButtonGoogle))&&String.IsNullOrEmpty(StringUtil.RTrim( AV39Buttongoogle_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV12ButtonGoogle)));
            GxWebStd.gx_bitmap( context, imgavButtongoogle_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV12ButtonGoogle)) ? AV39Buttongoogle_GXI : context.PathToRelativeUrl( AV12ButtonGoogle)), "", "", "", context.GetTheme( ), imgavButtongoogle_Visible, 1, "", "Sign in with Google", 0, -1, 0, "", 0, "", 0, 0, 5, imgavButtongoogle_Jsonclick, "'"+""+"'"+",false,"+"'"+"EVBUTTONGOOGLE.CLICK."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, AV12ButtonGoogle_IsBlob, false, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavButtontwitter_Internalname, AV13ButtonTwitter);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            AV13ButtonTwitter_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV13ButtonTwitter))&&String.IsNullOrEmpty(StringUtil.RTrim( AV40Buttontwitter_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV13ButtonTwitter)));
            GxWebStd.gx_bitmap( context, imgavButtontwitter_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV13ButtonTwitter)) ? AV40Buttontwitter_GXI : context.PathToRelativeUrl( AV13ButtonTwitter)), "", "", "", context.GetTheme( ), imgavButtontwitter_Visible, 1, "", "Sign in with Twitter", 0, -1, 0, "", 0, "", 0, 0, 5, imgavButtontwitter_Jsonclick, "'"+""+"'"+",false,"+"'"+"EVBUTTONTWITTER.CLICK."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, AV13ButtonTwitter_IsBlob, false, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:9px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active Bitmap Variable */
            GxWebStd.gx_hidden_field( context, imgavButtongamremote_Internalname, AV11ButtonGAMRemote);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            AV11ButtonGAMRemote_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV11ButtonGAMRemote))&&String.IsNullOrEmpty(StringUtil.RTrim( AV41Buttongamremote_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV11ButtonGAMRemote)));
            GxWebStd.gx_bitmap( context, imgavButtongamremote_Internalname, (String.IsNullOrEmpty(StringUtil.RTrim( AV11ButtonGAMRemote)) ? AV41Buttongamremote_GXI : context.PathToRelativeUrl( AV11ButtonGAMRemote)), "", "", "", context.GetTheme( ), imgavButtongamremote_Visible, 1, "", "", 0, -1, 0, "", 0, "", 0, 0, 5, imgavButtongamremote_Jsonclick, "'"+""+"'"+",false,"+"'"+"EVBUTTONGAMREMOTE.CLICK."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, AV11ButtonGAMRemote_IsBlob, false, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_17_1S2e( true) ;
         }
         else
         {
            wb_table4_17_1S2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1S2( ) ;
         WS1S2( ) ;
         WE1S2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176033");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311732277");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gamexamplelogin.js", "?2020311732278");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbtitle_Internalname = "TBTITLE";
         imgavButtonfacebook_Internalname = "vBUTTONFACEBOOK";
         imgavButtongoogle_Internalname = "vBUTTONGOOGLE";
         imgavButtontwitter_Internalname = "vBUTTONTWITTER";
         imgavButtongamremote_Internalname = "vBUTTONGAMREMOTE";
         tblTblextauth_Internalname = "TBLEXTAUTH";
         lblTblogonto_Internalname = "TBLOGONTO";
         cmbavLogonto_Internalname = "vLOGONTO";
         lblTbusername_Internalname = "TBUSERNAME";
         edtavUsername_Internalname = "vUSERNAME";
         lblTbpassword_Internalname = "TBPASSWORD";
         edtavUserpassword_Internalname = "vUSERPASSWORD";
         chkavKeepmeloggedin_Internalname = "vKEEPMELOGGEDIN";
         chkavRememberme_Internalname = "vREMEMBERME";
         tblTblrem_Internalname = "TBLREM";
         bttLogin_Internalname = "LOGIN";
         lblTbrememberme2_Internalname = "TBREMEMBERME2";
         tblTblfields_Internalname = "TBLFIELDS";
         tblTbllogin_Internalname = "TBLLOGIN";
         tblTblmain_Internalname = "TBLMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         imgavButtongamremote_Jsonclick = "";
         imgavButtontwitter_Jsonclick = "";
         imgavButtongoogle_Jsonclick = "";
         imgavButtonfacebook_Jsonclick = "";
         edtavUserpassword_Jsonclick = "";
         edtavUsername_Jsonclick = "";
         cmbavLogonto_Jsonclick = "";
         lblTblogonto_Visible = 1;
         chkavKeepmeloggedin.Visible = 1;
         chkavRememberme.Visible = 1;
         cmbavLogonto.Visible = 1;
         imgavButtongamremote_Visible = 1;
         imgavButtontwitter_Visible = 1;
         imgavButtongoogle_Visible = 1;
         imgavButtonfacebook_Visible = 1;
         chkavRememberme.Caption = "";
         chkavKeepmeloggedin.Caption = "";
         context.GX_msglist.DisplayMode = 1;
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV27LogOnTo = "";
         AV10ButtonFacebook = "";
         AV12ButtonGoogle = "";
         AV13ButtonTwitter = "";
         AV11ButtonGAMRemote = "";
         AV33UserName = "";
         AV34UserPassword = "";
         AV15ConnectionInfoCollection = new GxExternalCollection( context, "SdtGAMConnectionInfo", "GeneXus.Programs");
         AV18Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV38Buttonfacebook_GXI = "";
         AV39Buttongoogle_GXI = "";
         AV40Buttontwitter_GXI = "";
         AV41Buttongamremote_GXI = "";
         AV19ErrorsLogin = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV6ApplicationClientId = "";
         AV30Session = new SdtGAMSession(context);
         AV32URL = "";
         AV9AuthenticationTypes = new GxExternalCollection( context, "SdtGAMAuthenticationTypeSimple", "GeneXus.Programs");
         AV24Language = "";
         AV8AuthenticationType = new SdtGAMAuthenticationTypeSimple(context);
         AV29Repository = new SdtGAMRepository(context);
         AV5AdditionalParameter = new SdtGAMLoginAdditionalParameters(context);
         AV7ApplicationData = "";
         AV20GAMExampleSDTApplicationData = new SdtGAMExampleSDTApplicationData(context);
         AV17Error = new SdtGAMError(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTbtitle_Jsonclick = "";
         lblTblogonto_Jsonclick = "";
         TempTags = "";
         lblTbusername_Jsonclick = "";
         lblTbpassword_Jsonclick = "";
         bttLogin_Jsonclick = "";
         lblTbrememberme2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV35UserRememberMe ;
      private short nGXWrapped ;
      private int imgavButtonfacebook_Visible ;
      private int imgavButtongoogle_Visible ;
      private int imgavButtontwitter_Visible ;
      private int imgavButtongamremote_Visible ;
      private int AV42GXV1 ;
      private int lblTblogonto_Visible ;
      private int AV43GXV2 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV27LogOnTo ;
      private String chkavKeepmeloggedin_Internalname ;
      private String chkavRememberme_Internalname ;
      private String cmbavLogonto_Internalname ;
      private String imgavButtonfacebook_Internalname ;
      private String imgavButtongoogle_Internalname ;
      private String imgavButtontwitter_Internalname ;
      private String imgavButtongamremote_Internalname ;
      private String edtavUsername_Internalname ;
      private String AV34UserPassword ;
      private String edtavUserpassword_Internalname ;
      private String AV6ApplicationClientId ;
      private String AV24Language ;
      private String lblTblogonto_Internalname ;
      private String sStyleString ;
      private String tblTblmain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTbllogin_Internalname ;
      private String lblTbtitle_Internalname ;
      private String lblTbtitle_Jsonclick ;
      private String tblTblfields_Internalname ;
      private String lblTblogonto_Jsonclick ;
      private String TempTags ;
      private String cmbavLogonto_Jsonclick ;
      private String lblTbusername_Internalname ;
      private String lblTbusername_Jsonclick ;
      private String edtavUsername_Jsonclick ;
      private String lblTbpassword_Internalname ;
      private String lblTbpassword_Jsonclick ;
      private String edtavUserpassword_Jsonclick ;
      private String bttLogin_Internalname ;
      private String bttLogin_Jsonclick ;
      private String lblTbrememberme2_Internalname ;
      private String lblTbrememberme2_Jsonclick ;
      private String tblTblrem_Internalname ;
      private String tblTblextauth_Internalname ;
      private String imgavButtonfacebook_Jsonclick ;
      private String imgavButtongoogle_Jsonclick ;
      private String imgavButtontwitter_Jsonclick ;
      private String imgavButtongamremote_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV23KeepMeLoggedIn ;
      private bool AV28RememberMe ;
      private bool returnInSub ;
      private bool AV21isOK ;
      private bool AV22isRedirect ;
      private bool AV31SessionValid ;
      private bool AV25LoginOK ;
      private bool AV10ButtonFacebook_IsBlob ;
      private bool AV12ButtonGoogle_IsBlob ;
      private bool AV13ButtonTwitter_IsBlob ;
      private bool AV11ButtonGAMRemote_IsBlob ;
      private String AV7ApplicationData ;
      private String AV33UserName ;
      private String AV38Buttonfacebook_GXI ;
      private String AV39Buttongoogle_GXI ;
      private String AV40Buttontwitter_GXI ;
      private String AV41Buttongamremote_GXI ;
      private String AV32URL ;
      private String AV10ButtonFacebook ;
      private String AV12ButtonGoogle ;
      private String AV13ButtonTwitter ;
      private String AV11ButtonGAMRemote ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavLogonto ;
      private GXCheckbox chkavKeepmeloggedin ;
      private GXCheckbox chkavRememberme ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtGAMAuthenticationTypeSimple ))]
      private IGxCollection AV9AuthenticationTypes ;
      [ObjectCollection(ItemType=typeof( SdtGAMConnectionInfo ))]
      private IGxCollection AV15ConnectionInfoCollection ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV18Errors ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV19ErrorsLogin ;
      private SdtGAMLoginAdditionalParameters AV5AdditionalParameter ;
      private SdtGAMAuthenticationTypeSimple AV8AuthenticationType ;
      private SdtGAMError AV17Error ;
      private SdtGAMExampleSDTApplicationData AV20GAMExampleSDTApplicationData ;
      private SdtGAMRepository AV29Repository ;
      private SdtGAMSession AV30Session ;
   }

}
