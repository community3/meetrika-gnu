/*
               File: RegrasContagemAnexos_BC
        Description: Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:26:9.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class regrascontagemanexos_bc : GXHttpHandler, IGxSilentTrn
   {
      public regrascontagemanexos_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public regrascontagemanexos_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow31123( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey31123( ) ;
         standaloneModal( ) ;
         AddRow31123( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1005RegrasContagemAnexo_Codigo = A1005RegrasContagemAnexo_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_310( )
      {
         BeforeValidate31123( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls31123( ) ;
            }
            else
            {
               CheckExtendedTable31123( ) ;
               if ( AnyError == 0 )
               {
                  ZM31123( 3) ;
                  ZM31123( 4) ;
               }
               CloseExtendedTableCursors31123( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM31123( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z1008RegrasContagemAnexo_NomeArq = A1008RegrasContagemAnexo_NomeArq;
            Z1009RegrasContagemAnexo_TipoArq = A1009RegrasContagemAnexo_TipoArq;
            Z1010RegrasContagemAnexo_Data = A1010RegrasContagemAnexo_Data;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z860RegrasContagem_Regra = A860RegrasContagem_Regra;
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -2 )
         {
            Z1005RegrasContagemAnexo_Codigo = A1005RegrasContagemAnexo_Codigo;
            Z1007RegrasContagemAnexo_Arquivo = A1007RegrasContagemAnexo_Arquivo;
            Z1008RegrasContagemAnexo_NomeArq = A1008RegrasContagemAnexo_NomeArq;
            Z1009RegrasContagemAnexo_TipoArq = A1009RegrasContagemAnexo_TipoArq;
            Z1010RegrasContagemAnexo_Data = A1010RegrasContagemAnexo_Data;
            Z1011RegrasContagemAnexo_Descricao = A1011RegrasContagemAnexo_Descricao;
            Z645TipoDocumento_Codigo = A645TipoDocumento_Codigo;
            Z860RegrasContagem_Regra = A860RegrasContagem_Regra;
            Z646TipoDocumento_Nome = A646TipoDocumento_Nome;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load31123( )
      {
         /* Using cursor BC00316 */
         pr_default.execute(4, new Object[] {A1005RegrasContagemAnexo_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound123 = 1;
            A1008RegrasContagemAnexo_NomeArq = BC00316_A1008RegrasContagemAnexo_NomeArq[0];
            n1008RegrasContagemAnexo_NomeArq = BC00316_n1008RegrasContagemAnexo_NomeArq[0];
            A1009RegrasContagemAnexo_TipoArq = BC00316_A1009RegrasContagemAnexo_TipoArq[0];
            n1009RegrasContagemAnexo_TipoArq = BC00316_n1009RegrasContagemAnexo_TipoArq[0];
            A1010RegrasContagemAnexo_Data = BC00316_A1010RegrasContagemAnexo_Data[0];
            n1010RegrasContagemAnexo_Data = BC00316_n1010RegrasContagemAnexo_Data[0];
            A1011RegrasContagemAnexo_Descricao = BC00316_A1011RegrasContagemAnexo_Descricao[0];
            n1011RegrasContagemAnexo_Descricao = BC00316_n1011RegrasContagemAnexo_Descricao[0];
            A646TipoDocumento_Nome = BC00316_A646TipoDocumento_Nome[0];
            A645TipoDocumento_Codigo = BC00316_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC00316_n645TipoDocumento_Codigo[0];
            A860RegrasContagem_Regra = BC00316_A860RegrasContagem_Regra[0];
            A1007RegrasContagemAnexo_Arquivo = BC00316_A1007RegrasContagemAnexo_Arquivo[0];
            n1007RegrasContagemAnexo_Arquivo = BC00316_n1007RegrasContagemAnexo_Arquivo[0];
            ZM31123( -2) ;
         }
         pr_default.close(4);
         OnLoadActions31123( ) ;
      }

      protected void OnLoadActions31123( )
      {
      }

      protected void CheckExtendedTable31123( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00315 */
         pr_default.execute(3, new Object[] {A860RegrasContagem_Regra});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Regras de Contagem'.", "ForeignKeyNotFound", 1, "REGRASCONTAGEM_REGRA");
            AnyError = 1;
         }
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A1010RegrasContagemAnexo_Data) || ( A1010RegrasContagemAnexo_Data >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Upload fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00314 */
         pr_default.execute(2, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A645TipoDocumento_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Tipo de Documentos'.", "ForeignKeyNotFound", 1, "TIPODOCUMENTO_CODIGO");
               AnyError = 1;
            }
         }
         A646TipoDocumento_Nome = BC00314_A646TipoDocumento_Nome[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors31123( )
      {
         pr_default.close(3);
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey31123( )
      {
         /* Using cursor BC00317 */
         pr_default.execute(5, new Object[] {A1005RegrasContagemAnexo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound123 = 1;
         }
         else
         {
            RcdFound123 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00313 */
         pr_default.execute(1, new Object[] {A1005RegrasContagemAnexo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM31123( 2) ;
            RcdFound123 = 1;
            A1005RegrasContagemAnexo_Codigo = BC00313_A1005RegrasContagemAnexo_Codigo[0];
            A1008RegrasContagemAnexo_NomeArq = BC00313_A1008RegrasContagemAnexo_NomeArq[0];
            n1008RegrasContagemAnexo_NomeArq = BC00313_n1008RegrasContagemAnexo_NomeArq[0];
            A1009RegrasContagemAnexo_TipoArq = BC00313_A1009RegrasContagemAnexo_TipoArq[0];
            n1009RegrasContagemAnexo_TipoArq = BC00313_n1009RegrasContagemAnexo_TipoArq[0];
            A1010RegrasContagemAnexo_Data = BC00313_A1010RegrasContagemAnexo_Data[0];
            n1010RegrasContagemAnexo_Data = BC00313_n1010RegrasContagemAnexo_Data[0];
            A1011RegrasContagemAnexo_Descricao = BC00313_A1011RegrasContagemAnexo_Descricao[0];
            n1011RegrasContagemAnexo_Descricao = BC00313_n1011RegrasContagemAnexo_Descricao[0];
            A645TipoDocumento_Codigo = BC00313_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC00313_n645TipoDocumento_Codigo[0];
            A860RegrasContagem_Regra = BC00313_A860RegrasContagem_Regra[0];
            A1007RegrasContagemAnexo_Arquivo = BC00313_A1007RegrasContagemAnexo_Arquivo[0];
            n1007RegrasContagemAnexo_Arquivo = BC00313_n1007RegrasContagemAnexo_Arquivo[0];
            Z1005RegrasContagemAnexo_Codigo = A1005RegrasContagemAnexo_Codigo;
            sMode123 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load31123( ) ;
            if ( AnyError == 1 )
            {
               RcdFound123 = 0;
               InitializeNonKey31123( ) ;
            }
            Gx_mode = sMode123;
         }
         else
         {
            RcdFound123 = 0;
            InitializeNonKey31123( ) ;
            sMode123 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode123;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey31123( ) ;
         if ( RcdFound123 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_310( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency31123( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00312 */
            pr_default.execute(0, new Object[] {A1005RegrasContagemAnexo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RegrasContagemAnexos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1008RegrasContagemAnexo_NomeArq, BC00312_A1008RegrasContagemAnexo_NomeArq[0]) != 0 ) || ( StringUtil.StrCmp(Z1009RegrasContagemAnexo_TipoArq, BC00312_A1009RegrasContagemAnexo_TipoArq[0]) != 0 ) || ( Z1010RegrasContagemAnexo_Data != BC00312_A1010RegrasContagemAnexo_Data[0] ) || ( Z645TipoDocumento_Codigo != BC00312_A645TipoDocumento_Codigo[0] ) || ( StringUtil.StrCmp(Z860RegrasContagem_Regra, BC00312_A860RegrasContagem_Regra[0]) != 0 ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"RegrasContagemAnexos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert31123( )
      {
         BeforeValidate31123( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable31123( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM31123( 0) ;
            CheckOptimisticConcurrency31123( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm31123( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert31123( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00318 */
                     pr_default.execute(6, new Object[] {n1007RegrasContagemAnexo_Arquivo, A1007RegrasContagemAnexo_Arquivo, n1008RegrasContagemAnexo_NomeArq, A1008RegrasContagemAnexo_NomeArq, n1009RegrasContagemAnexo_TipoArq, A1009RegrasContagemAnexo_TipoArq, n1010RegrasContagemAnexo_Data, A1010RegrasContagemAnexo_Data, n1011RegrasContagemAnexo_Descricao, A1011RegrasContagemAnexo_Descricao, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A860RegrasContagem_Regra});
                     A1005RegrasContagemAnexo_Codigo = BC00318_A1005RegrasContagemAnexo_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load31123( ) ;
            }
            EndLevel31123( ) ;
         }
         CloseExtendedTableCursors31123( ) ;
      }

      protected void Update31123( )
      {
         BeforeValidate31123( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable31123( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency31123( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm31123( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate31123( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00319 */
                     pr_default.execute(7, new Object[] {n1008RegrasContagemAnexo_NomeArq, A1008RegrasContagemAnexo_NomeArq, n1009RegrasContagemAnexo_TipoArq, A1009RegrasContagemAnexo_TipoArq, n1010RegrasContagemAnexo_Data, A1010RegrasContagemAnexo_Data, n1011RegrasContagemAnexo_Descricao, A1011RegrasContagemAnexo_Descricao, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, A860RegrasContagem_Regra, A1005RegrasContagemAnexo_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RegrasContagemAnexos"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate31123( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel31123( ) ;
         }
         CloseExtendedTableCursors31123( ) ;
      }

      protected void DeferredUpdate31123( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC003110 */
            pr_default.execute(8, new Object[] {n1007RegrasContagemAnexo_Arquivo, A1007RegrasContagemAnexo_Arquivo, A1005RegrasContagemAnexo_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate31123( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency31123( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls31123( ) ;
            AfterConfirm31123( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete31123( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003111 */
                  pr_default.execute(9, new Object[] {A1005RegrasContagemAnexo_Codigo});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("RegrasContagemAnexos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode123 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel31123( ) ;
         Gx_mode = sMode123;
      }

      protected void OnDeleteControls31123( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC003112 */
            pr_default.execute(10, new Object[] {n645TipoDocumento_Codigo, A645TipoDocumento_Codigo});
            A646TipoDocumento_Nome = BC003112_A646TipoDocumento_Nome[0];
            pr_default.close(10);
         }
      }

      protected void EndLevel31123( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete31123( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart31123( )
      {
         /* Using cursor BC003113 */
         pr_default.execute(11, new Object[] {A1005RegrasContagemAnexo_Codigo});
         RcdFound123 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound123 = 1;
            A1005RegrasContagemAnexo_Codigo = BC003113_A1005RegrasContagemAnexo_Codigo[0];
            A1008RegrasContagemAnexo_NomeArq = BC003113_A1008RegrasContagemAnexo_NomeArq[0];
            n1008RegrasContagemAnexo_NomeArq = BC003113_n1008RegrasContagemAnexo_NomeArq[0];
            A1009RegrasContagemAnexo_TipoArq = BC003113_A1009RegrasContagemAnexo_TipoArq[0];
            n1009RegrasContagemAnexo_TipoArq = BC003113_n1009RegrasContagemAnexo_TipoArq[0];
            A1010RegrasContagemAnexo_Data = BC003113_A1010RegrasContagemAnexo_Data[0];
            n1010RegrasContagemAnexo_Data = BC003113_n1010RegrasContagemAnexo_Data[0];
            A1011RegrasContagemAnexo_Descricao = BC003113_A1011RegrasContagemAnexo_Descricao[0];
            n1011RegrasContagemAnexo_Descricao = BC003113_n1011RegrasContagemAnexo_Descricao[0];
            A646TipoDocumento_Nome = BC003113_A646TipoDocumento_Nome[0];
            A645TipoDocumento_Codigo = BC003113_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC003113_n645TipoDocumento_Codigo[0];
            A860RegrasContagem_Regra = BC003113_A860RegrasContagem_Regra[0];
            A1007RegrasContagemAnexo_Arquivo = BC003113_A1007RegrasContagemAnexo_Arquivo[0];
            n1007RegrasContagemAnexo_Arquivo = BC003113_n1007RegrasContagemAnexo_Arquivo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext31123( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound123 = 0;
         ScanKeyLoad31123( ) ;
      }

      protected void ScanKeyLoad31123( )
      {
         sMode123 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound123 = 1;
            A1005RegrasContagemAnexo_Codigo = BC003113_A1005RegrasContagemAnexo_Codigo[0];
            A1008RegrasContagemAnexo_NomeArq = BC003113_A1008RegrasContagemAnexo_NomeArq[0];
            n1008RegrasContagemAnexo_NomeArq = BC003113_n1008RegrasContagemAnexo_NomeArq[0];
            A1009RegrasContagemAnexo_TipoArq = BC003113_A1009RegrasContagemAnexo_TipoArq[0];
            n1009RegrasContagemAnexo_TipoArq = BC003113_n1009RegrasContagemAnexo_TipoArq[0];
            A1010RegrasContagemAnexo_Data = BC003113_A1010RegrasContagemAnexo_Data[0];
            n1010RegrasContagemAnexo_Data = BC003113_n1010RegrasContagemAnexo_Data[0];
            A1011RegrasContagemAnexo_Descricao = BC003113_A1011RegrasContagemAnexo_Descricao[0];
            n1011RegrasContagemAnexo_Descricao = BC003113_n1011RegrasContagemAnexo_Descricao[0];
            A646TipoDocumento_Nome = BC003113_A646TipoDocumento_Nome[0];
            A645TipoDocumento_Codigo = BC003113_A645TipoDocumento_Codigo[0];
            n645TipoDocumento_Codigo = BC003113_n645TipoDocumento_Codigo[0];
            A860RegrasContagem_Regra = BC003113_A860RegrasContagem_Regra[0];
            A1007RegrasContagemAnexo_Arquivo = BC003113_A1007RegrasContagemAnexo_Arquivo[0];
            n1007RegrasContagemAnexo_Arquivo = BC003113_n1007RegrasContagemAnexo_Arquivo[0];
         }
         Gx_mode = sMode123;
      }

      protected void ScanKeyEnd31123( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm31123( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert31123( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate31123( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete31123( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete31123( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate31123( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes31123( )
      {
      }

      protected void AddRow31123( )
      {
         VarsToRow123( bcRegrasContagemAnexos) ;
      }

      protected void ReadRow31123( )
      {
         RowToVars123( bcRegrasContagemAnexos, 1) ;
      }

      protected void InitializeNonKey31123( )
      {
         A860RegrasContagem_Regra = "";
         A1007RegrasContagemAnexo_Arquivo = "";
         n1007RegrasContagemAnexo_Arquivo = false;
         A1008RegrasContagemAnexo_NomeArq = "";
         n1008RegrasContagemAnexo_NomeArq = false;
         A1009RegrasContagemAnexo_TipoArq = "";
         n1009RegrasContagemAnexo_TipoArq = false;
         A1010RegrasContagemAnexo_Data = DateTime.MinValue;
         n1010RegrasContagemAnexo_Data = false;
         A1011RegrasContagemAnexo_Descricao = "";
         n1011RegrasContagemAnexo_Descricao = false;
         A645TipoDocumento_Codigo = 0;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = "";
         Z1008RegrasContagemAnexo_NomeArq = "";
         Z1009RegrasContagemAnexo_TipoArq = "";
         Z1010RegrasContagemAnexo_Data = DateTime.MinValue;
         Z645TipoDocumento_Codigo = 0;
         Z860RegrasContagem_Regra = "";
      }

      protected void InitAll31123( )
      {
         A1005RegrasContagemAnexo_Codigo = 0;
         InitializeNonKey31123( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow123( SdtRegrasContagemAnexos obj123 )
      {
         obj123.gxTpr_Mode = Gx_mode;
         obj123.gxTpr_Regrascontagem_regra = A860RegrasContagem_Regra;
         obj123.gxTpr_Regrascontagemanexo_arquivo = A1007RegrasContagemAnexo_Arquivo;
         obj123.gxTpr_Regrascontagemanexo_nomearq = A1008RegrasContagemAnexo_NomeArq;
         obj123.gxTpr_Regrascontagemanexo_tipoarq = A1009RegrasContagemAnexo_TipoArq;
         obj123.gxTpr_Regrascontagemanexo_data = A1010RegrasContagemAnexo_Data;
         obj123.gxTpr_Regrascontagemanexo_descricao = A1011RegrasContagemAnexo_Descricao;
         obj123.gxTpr_Tipodocumento_codigo = A645TipoDocumento_Codigo;
         obj123.gxTpr_Tipodocumento_nome = A646TipoDocumento_Nome;
         obj123.gxTpr_Regrascontagemanexo_codigo = A1005RegrasContagemAnexo_Codigo;
         obj123.gxTpr_Regrascontagemanexo_codigo_Z = Z1005RegrasContagemAnexo_Codigo;
         obj123.gxTpr_Regrascontagem_regra_Z = Z860RegrasContagem_Regra;
         obj123.gxTpr_Regrascontagemanexo_nomearq_Z = Z1008RegrasContagemAnexo_NomeArq;
         obj123.gxTpr_Regrascontagemanexo_tipoarq_Z = Z1009RegrasContagemAnexo_TipoArq;
         obj123.gxTpr_Regrascontagemanexo_data_Z = Z1010RegrasContagemAnexo_Data;
         obj123.gxTpr_Tipodocumento_codigo_Z = Z645TipoDocumento_Codigo;
         obj123.gxTpr_Tipodocumento_nome_Z = Z646TipoDocumento_Nome;
         obj123.gxTpr_Regrascontagemanexo_arquivo_N = (short)(Convert.ToInt16(n1007RegrasContagemAnexo_Arquivo));
         obj123.gxTpr_Regrascontagemanexo_nomearq_N = (short)(Convert.ToInt16(n1008RegrasContagemAnexo_NomeArq));
         obj123.gxTpr_Regrascontagemanexo_tipoarq_N = (short)(Convert.ToInt16(n1009RegrasContagemAnexo_TipoArq));
         obj123.gxTpr_Regrascontagemanexo_data_N = (short)(Convert.ToInt16(n1010RegrasContagemAnexo_Data));
         obj123.gxTpr_Regrascontagemanexo_descricao_N = (short)(Convert.ToInt16(n1011RegrasContagemAnexo_Descricao));
         obj123.gxTpr_Tipodocumento_codigo_N = (short)(Convert.ToInt16(n645TipoDocumento_Codigo));
         obj123.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow123( SdtRegrasContagemAnexos obj123 )
      {
         obj123.gxTpr_Regrascontagemanexo_codigo = A1005RegrasContagemAnexo_Codigo;
         return  ;
      }

      public void RowToVars123( SdtRegrasContagemAnexos obj123 ,
                                int forceLoad )
      {
         Gx_mode = obj123.gxTpr_Mode;
         A860RegrasContagem_Regra = obj123.gxTpr_Regrascontagem_regra;
         A1007RegrasContagemAnexo_Arquivo = obj123.gxTpr_Regrascontagemanexo_arquivo;
         n1007RegrasContagemAnexo_Arquivo = false;
         A1008RegrasContagemAnexo_NomeArq = obj123.gxTpr_Regrascontagemanexo_nomearq;
         n1008RegrasContagemAnexo_NomeArq = false;
         A1009RegrasContagemAnexo_TipoArq = obj123.gxTpr_Regrascontagemanexo_tipoarq;
         n1009RegrasContagemAnexo_TipoArq = false;
         A1010RegrasContagemAnexo_Data = obj123.gxTpr_Regrascontagemanexo_data;
         n1010RegrasContagemAnexo_Data = false;
         A1011RegrasContagemAnexo_Descricao = obj123.gxTpr_Regrascontagemanexo_descricao;
         n1011RegrasContagemAnexo_Descricao = false;
         A645TipoDocumento_Codigo = obj123.gxTpr_Tipodocumento_codigo;
         n645TipoDocumento_Codigo = false;
         A646TipoDocumento_Nome = obj123.gxTpr_Tipodocumento_nome;
         A1005RegrasContagemAnexo_Codigo = obj123.gxTpr_Regrascontagemanexo_codigo;
         Z1005RegrasContagemAnexo_Codigo = obj123.gxTpr_Regrascontagemanexo_codigo_Z;
         Z860RegrasContagem_Regra = obj123.gxTpr_Regrascontagem_regra_Z;
         Z1008RegrasContagemAnexo_NomeArq = obj123.gxTpr_Regrascontagemanexo_nomearq_Z;
         Z1009RegrasContagemAnexo_TipoArq = obj123.gxTpr_Regrascontagemanexo_tipoarq_Z;
         Z1010RegrasContagemAnexo_Data = obj123.gxTpr_Regrascontagemanexo_data_Z;
         Z645TipoDocumento_Codigo = obj123.gxTpr_Tipodocumento_codigo_Z;
         Z646TipoDocumento_Nome = obj123.gxTpr_Tipodocumento_nome_Z;
         n1007RegrasContagemAnexo_Arquivo = (bool)(Convert.ToBoolean(obj123.gxTpr_Regrascontagemanexo_arquivo_N));
         n1008RegrasContagemAnexo_NomeArq = (bool)(Convert.ToBoolean(obj123.gxTpr_Regrascontagemanexo_nomearq_N));
         n1009RegrasContagemAnexo_TipoArq = (bool)(Convert.ToBoolean(obj123.gxTpr_Regrascontagemanexo_tipoarq_N));
         n1010RegrasContagemAnexo_Data = (bool)(Convert.ToBoolean(obj123.gxTpr_Regrascontagemanexo_data_N));
         n1011RegrasContagemAnexo_Descricao = (bool)(Convert.ToBoolean(obj123.gxTpr_Regrascontagemanexo_descricao_N));
         n645TipoDocumento_Codigo = (bool)(Convert.ToBoolean(obj123.gxTpr_Tipodocumento_codigo_N));
         Gx_mode = obj123.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1005RegrasContagemAnexo_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey31123( ) ;
         ScanKeyStart31123( ) ;
         if ( RcdFound123 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1005RegrasContagemAnexo_Codigo = A1005RegrasContagemAnexo_Codigo;
         }
         ZM31123( -2) ;
         OnLoadActions31123( ) ;
         AddRow31123( ) ;
         ScanKeyEnd31123( ) ;
         if ( RcdFound123 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars123( bcRegrasContagemAnexos, 0) ;
         ScanKeyStart31123( ) ;
         if ( RcdFound123 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1005RegrasContagemAnexo_Codigo = A1005RegrasContagemAnexo_Codigo;
         }
         ZM31123( -2) ;
         OnLoadActions31123( ) ;
         AddRow31123( ) ;
         ScanKeyEnd31123( ) ;
         if ( RcdFound123 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars123( bcRegrasContagemAnexos, 0) ;
         nKeyPressed = 1;
         GetKey31123( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert31123( ) ;
         }
         else
         {
            if ( RcdFound123 == 1 )
            {
               if ( A1005RegrasContagemAnexo_Codigo != Z1005RegrasContagemAnexo_Codigo )
               {
                  A1005RegrasContagemAnexo_Codigo = Z1005RegrasContagemAnexo_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update31123( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1005RegrasContagemAnexo_Codigo != Z1005RegrasContagemAnexo_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert31123( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert31123( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow123( bcRegrasContagemAnexos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars123( bcRegrasContagemAnexos, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey31123( ) ;
         if ( RcdFound123 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1005RegrasContagemAnexo_Codigo != Z1005RegrasContagemAnexo_Codigo )
            {
               A1005RegrasContagemAnexo_Codigo = Z1005RegrasContagemAnexo_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1005RegrasContagemAnexo_Codigo != Z1005RegrasContagemAnexo_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(10);
         context.RollbackDataStores( "RegrasContagemAnexos_BC");
         VarsToRow123( bcRegrasContagemAnexos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcRegrasContagemAnexos.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcRegrasContagemAnexos.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcRegrasContagemAnexos )
         {
            bcRegrasContagemAnexos = (SdtRegrasContagemAnexos)(sdt);
            if ( StringUtil.StrCmp(bcRegrasContagemAnexos.gxTpr_Mode, "") == 0 )
            {
               bcRegrasContagemAnexos.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow123( bcRegrasContagemAnexos) ;
            }
            else
            {
               RowToVars123( bcRegrasContagemAnexos, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcRegrasContagemAnexos.gxTpr_Mode, "") == 0 )
            {
               bcRegrasContagemAnexos.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars123( bcRegrasContagemAnexos, 1) ;
         return  ;
      }

      public SdtRegrasContagemAnexos RegrasContagemAnexos_BC
      {
         get {
            return bcRegrasContagemAnexos ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1008RegrasContagemAnexo_NomeArq = "";
         A1008RegrasContagemAnexo_NomeArq = "";
         Z1009RegrasContagemAnexo_TipoArq = "";
         A1009RegrasContagemAnexo_TipoArq = "";
         Z1010RegrasContagemAnexo_Data = DateTime.MinValue;
         A1010RegrasContagemAnexo_Data = DateTime.MinValue;
         Z860RegrasContagem_Regra = "";
         A860RegrasContagem_Regra = "";
         Z646TipoDocumento_Nome = "";
         A646TipoDocumento_Nome = "";
         Z1007RegrasContagemAnexo_Arquivo = "";
         A1007RegrasContagemAnexo_Arquivo = "";
         Z1011RegrasContagemAnexo_Descricao = "";
         A1011RegrasContagemAnexo_Descricao = "";
         BC00316_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         BC00316_A1008RegrasContagemAnexo_NomeArq = new String[] {""} ;
         BC00316_n1008RegrasContagemAnexo_NomeArq = new bool[] {false} ;
         BC00316_A1009RegrasContagemAnexo_TipoArq = new String[] {""} ;
         BC00316_n1009RegrasContagemAnexo_TipoArq = new bool[] {false} ;
         BC00316_A1010RegrasContagemAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC00316_n1010RegrasContagemAnexo_Data = new bool[] {false} ;
         BC00316_A1011RegrasContagemAnexo_Descricao = new String[] {""} ;
         BC00316_n1011RegrasContagemAnexo_Descricao = new bool[] {false} ;
         BC00316_A646TipoDocumento_Nome = new String[] {""} ;
         BC00316_A645TipoDocumento_Codigo = new int[1] ;
         BC00316_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC00316_A860RegrasContagem_Regra = new String[] {""} ;
         BC00316_A1007RegrasContagemAnexo_Arquivo = new String[] {""} ;
         BC00316_n1007RegrasContagemAnexo_Arquivo = new bool[] {false} ;
         BC00315_A860RegrasContagem_Regra = new String[] {""} ;
         BC00314_A646TipoDocumento_Nome = new String[] {""} ;
         BC00317_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         BC00313_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         BC00313_A1008RegrasContagemAnexo_NomeArq = new String[] {""} ;
         BC00313_n1008RegrasContagemAnexo_NomeArq = new bool[] {false} ;
         BC00313_A1009RegrasContagemAnexo_TipoArq = new String[] {""} ;
         BC00313_n1009RegrasContagemAnexo_TipoArq = new bool[] {false} ;
         BC00313_A1010RegrasContagemAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC00313_n1010RegrasContagemAnexo_Data = new bool[] {false} ;
         BC00313_A1011RegrasContagemAnexo_Descricao = new String[] {""} ;
         BC00313_n1011RegrasContagemAnexo_Descricao = new bool[] {false} ;
         BC00313_A645TipoDocumento_Codigo = new int[1] ;
         BC00313_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC00313_A860RegrasContagem_Regra = new String[] {""} ;
         BC00313_A1007RegrasContagemAnexo_Arquivo = new String[] {""} ;
         BC00313_n1007RegrasContagemAnexo_Arquivo = new bool[] {false} ;
         sMode123 = "";
         BC00312_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         BC00312_A1008RegrasContagemAnexo_NomeArq = new String[] {""} ;
         BC00312_n1008RegrasContagemAnexo_NomeArq = new bool[] {false} ;
         BC00312_A1009RegrasContagemAnexo_TipoArq = new String[] {""} ;
         BC00312_n1009RegrasContagemAnexo_TipoArq = new bool[] {false} ;
         BC00312_A1010RegrasContagemAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC00312_n1010RegrasContagemAnexo_Data = new bool[] {false} ;
         BC00312_A1011RegrasContagemAnexo_Descricao = new String[] {""} ;
         BC00312_n1011RegrasContagemAnexo_Descricao = new bool[] {false} ;
         BC00312_A645TipoDocumento_Codigo = new int[1] ;
         BC00312_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC00312_A860RegrasContagem_Regra = new String[] {""} ;
         BC00312_A1007RegrasContagemAnexo_Arquivo = new String[] {""} ;
         BC00312_n1007RegrasContagemAnexo_Arquivo = new bool[] {false} ;
         BC00318_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         BC003112_A646TipoDocumento_Nome = new String[] {""} ;
         BC003113_A1005RegrasContagemAnexo_Codigo = new int[1] ;
         BC003113_A1008RegrasContagemAnexo_NomeArq = new String[] {""} ;
         BC003113_n1008RegrasContagemAnexo_NomeArq = new bool[] {false} ;
         BC003113_A1009RegrasContagemAnexo_TipoArq = new String[] {""} ;
         BC003113_n1009RegrasContagemAnexo_TipoArq = new bool[] {false} ;
         BC003113_A1010RegrasContagemAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         BC003113_n1010RegrasContagemAnexo_Data = new bool[] {false} ;
         BC003113_A1011RegrasContagemAnexo_Descricao = new String[] {""} ;
         BC003113_n1011RegrasContagemAnexo_Descricao = new bool[] {false} ;
         BC003113_A646TipoDocumento_Nome = new String[] {""} ;
         BC003113_A645TipoDocumento_Codigo = new int[1] ;
         BC003113_n645TipoDocumento_Codigo = new bool[] {false} ;
         BC003113_A860RegrasContagem_Regra = new String[] {""} ;
         BC003113_A1007RegrasContagemAnexo_Arquivo = new String[] {""} ;
         BC003113_n1007RegrasContagemAnexo_Arquivo = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.regrascontagemanexos_bc__default(),
            new Object[][] {
                new Object[] {
               BC00312_A1005RegrasContagemAnexo_Codigo, BC00312_A1008RegrasContagemAnexo_NomeArq, BC00312_n1008RegrasContagemAnexo_NomeArq, BC00312_A1009RegrasContagemAnexo_TipoArq, BC00312_n1009RegrasContagemAnexo_TipoArq, BC00312_A1010RegrasContagemAnexo_Data, BC00312_n1010RegrasContagemAnexo_Data, BC00312_A1011RegrasContagemAnexo_Descricao, BC00312_n1011RegrasContagemAnexo_Descricao, BC00312_A645TipoDocumento_Codigo,
               BC00312_n645TipoDocumento_Codigo, BC00312_A860RegrasContagem_Regra, BC00312_A1007RegrasContagemAnexo_Arquivo, BC00312_n1007RegrasContagemAnexo_Arquivo
               }
               , new Object[] {
               BC00313_A1005RegrasContagemAnexo_Codigo, BC00313_A1008RegrasContagemAnexo_NomeArq, BC00313_n1008RegrasContagemAnexo_NomeArq, BC00313_A1009RegrasContagemAnexo_TipoArq, BC00313_n1009RegrasContagemAnexo_TipoArq, BC00313_A1010RegrasContagemAnexo_Data, BC00313_n1010RegrasContagemAnexo_Data, BC00313_A1011RegrasContagemAnexo_Descricao, BC00313_n1011RegrasContagemAnexo_Descricao, BC00313_A645TipoDocumento_Codigo,
               BC00313_n645TipoDocumento_Codigo, BC00313_A860RegrasContagem_Regra, BC00313_A1007RegrasContagemAnexo_Arquivo, BC00313_n1007RegrasContagemAnexo_Arquivo
               }
               , new Object[] {
               BC00314_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC00315_A860RegrasContagem_Regra
               }
               , new Object[] {
               BC00316_A1005RegrasContagemAnexo_Codigo, BC00316_A1008RegrasContagemAnexo_NomeArq, BC00316_n1008RegrasContagemAnexo_NomeArq, BC00316_A1009RegrasContagemAnexo_TipoArq, BC00316_n1009RegrasContagemAnexo_TipoArq, BC00316_A1010RegrasContagemAnexo_Data, BC00316_n1010RegrasContagemAnexo_Data, BC00316_A1011RegrasContagemAnexo_Descricao, BC00316_n1011RegrasContagemAnexo_Descricao, BC00316_A646TipoDocumento_Nome,
               BC00316_A645TipoDocumento_Codigo, BC00316_n645TipoDocumento_Codigo, BC00316_A860RegrasContagem_Regra, BC00316_A1007RegrasContagemAnexo_Arquivo, BC00316_n1007RegrasContagemAnexo_Arquivo
               }
               , new Object[] {
               BC00317_A1005RegrasContagemAnexo_Codigo
               }
               , new Object[] {
               BC00318_A1005RegrasContagemAnexo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003112_A646TipoDocumento_Nome
               }
               , new Object[] {
               BC003113_A1005RegrasContagemAnexo_Codigo, BC003113_A1008RegrasContagemAnexo_NomeArq, BC003113_n1008RegrasContagemAnexo_NomeArq, BC003113_A1009RegrasContagemAnexo_TipoArq, BC003113_n1009RegrasContagemAnexo_TipoArq, BC003113_A1010RegrasContagemAnexo_Data, BC003113_n1010RegrasContagemAnexo_Data, BC003113_A1011RegrasContagemAnexo_Descricao, BC003113_n1011RegrasContagemAnexo_Descricao, BC003113_A646TipoDocumento_Nome,
               BC003113_A645TipoDocumento_Codigo, BC003113_n645TipoDocumento_Codigo, BC003113_A860RegrasContagem_Regra, BC003113_A1007RegrasContagemAnexo_Arquivo, BC003113_n1007RegrasContagemAnexo_Arquivo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound123 ;
      private int trnEnded ;
      private int Z1005RegrasContagemAnexo_Codigo ;
      private int A1005RegrasContagemAnexo_Codigo ;
      private int Z645TipoDocumento_Codigo ;
      private int A645TipoDocumento_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String Z1008RegrasContagemAnexo_NomeArq ;
      private String A1008RegrasContagemAnexo_NomeArq ;
      private String Z1009RegrasContagemAnexo_TipoArq ;
      private String A1009RegrasContagemAnexo_TipoArq ;
      private String Z646TipoDocumento_Nome ;
      private String A646TipoDocumento_Nome ;
      private String sMode123 ;
      private DateTime Z1010RegrasContagemAnexo_Data ;
      private DateTime A1010RegrasContagemAnexo_Data ;
      private bool n1008RegrasContagemAnexo_NomeArq ;
      private bool n1009RegrasContagemAnexo_TipoArq ;
      private bool n1010RegrasContagemAnexo_Data ;
      private bool n1011RegrasContagemAnexo_Descricao ;
      private bool n645TipoDocumento_Codigo ;
      private bool n1007RegrasContagemAnexo_Arquivo ;
      private String Z1011RegrasContagemAnexo_Descricao ;
      private String A1011RegrasContagemAnexo_Descricao ;
      private String Z860RegrasContagem_Regra ;
      private String A860RegrasContagem_Regra ;
      private String Z1007RegrasContagemAnexo_Arquivo ;
      private String A1007RegrasContagemAnexo_Arquivo ;
      private SdtRegrasContagemAnexos bcRegrasContagemAnexos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC00316_A1005RegrasContagemAnexo_Codigo ;
      private String[] BC00316_A1008RegrasContagemAnexo_NomeArq ;
      private bool[] BC00316_n1008RegrasContagemAnexo_NomeArq ;
      private String[] BC00316_A1009RegrasContagemAnexo_TipoArq ;
      private bool[] BC00316_n1009RegrasContagemAnexo_TipoArq ;
      private DateTime[] BC00316_A1010RegrasContagemAnexo_Data ;
      private bool[] BC00316_n1010RegrasContagemAnexo_Data ;
      private String[] BC00316_A1011RegrasContagemAnexo_Descricao ;
      private bool[] BC00316_n1011RegrasContagemAnexo_Descricao ;
      private String[] BC00316_A646TipoDocumento_Nome ;
      private int[] BC00316_A645TipoDocumento_Codigo ;
      private bool[] BC00316_n645TipoDocumento_Codigo ;
      private String[] BC00316_A860RegrasContagem_Regra ;
      private String[] BC00316_A1007RegrasContagemAnexo_Arquivo ;
      private bool[] BC00316_n1007RegrasContagemAnexo_Arquivo ;
      private String[] BC00315_A860RegrasContagem_Regra ;
      private String[] BC00314_A646TipoDocumento_Nome ;
      private int[] BC00317_A1005RegrasContagemAnexo_Codigo ;
      private int[] BC00313_A1005RegrasContagemAnexo_Codigo ;
      private String[] BC00313_A1008RegrasContagemAnexo_NomeArq ;
      private bool[] BC00313_n1008RegrasContagemAnexo_NomeArq ;
      private String[] BC00313_A1009RegrasContagemAnexo_TipoArq ;
      private bool[] BC00313_n1009RegrasContagemAnexo_TipoArq ;
      private DateTime[] BC00313_A1010RegrasContagemAnexo_Data ;
      private bool[] BC00313_n1010RegrasContagemAnexo_Data ;
      private String[] BC00313_A1011RegrasContagemAnexo_Descricao ;
      private bool[] BC00313_n1011RegrasContagemAnexo_Descricao ;
      private int[] BC00313_A645TipoDocumento_Codigo ;
      private bool[] BC00313_n645TipoDocumento_Codigo ;
      private String[] BC00313_A860RegrasContagem_Regra ;
      private String[] BC00313_A1007RegrasContagemAnexo_Arquivo ;
      private bool[] BC00313_n1007RegrasContagemAnexo_Arquivo ;
      private int[] BC00312_A1005RegrasContagemAnexo_Codigo ;
      private String[] BC00312_A1008RegrasContagemAnexo_NomeArq ;
      private bool[] BC00312_n1008RegrasContagemAnexo_NomeArq ;
      private String[] BC00312_A1009RegrasContagemAnexo_TipoArq ;
      private bool[] BC00312_n1009RegrasContagemAnexo_TipoArq ;
      private DateTime[] BC00312_A1010RegrasContagemAnexo_Data ;
      private bool[] BC00312_n1010RegrasContagemAnexo_Data ;
      private String[] BC00312_A1011RegrasContagemAnexo_Descricao ;
      private bool[] BC00312_n1011RegrasContagemAnexo_Descricao ;
      private int[] BC00312_A645TipoDocumento_Codigo ;
      private bool[] BC00312_n645TipoDocumento_Codigo ;
      private String[] BC00312_A860RegrasContagem_Regra ;
      private String[] BC00312_A1007RegrasContagemAnexo_Arquivo ;
      private bool[] BC00312_n1007RegrasContagemAnexo_Arquivo ;
      private int[] BC00318_A1005RegrasContagemAnexo_Codigo ;
      private String[] BC003112_A646TipoDocumento_Nome ;
      private int[] BC003113_A1005RegrasContagemAnexo_Codigo ;
      private String[] BC003113_A1008RegrasContagemAnexo_NomeArq ;
      private bool[] BC003113_n1008RegrasContagemAnexo_NomeArq ;
      private String[] BC003113_A1009RegrasContagemAnexo_TipoArq ;
      private bool[] BC003113_n1009RegrasContagemAnexo_TipoArq ;
      private DateTime[] BC003113_A1010RegrasContagemAnexo_Data ;
      private bool[] BC003113_n1010RegrasContagemAnexo_Data ;
      private String[] BC003113_A1011RegrasContagemAnexo_Descricao ;
      private bool[] BC003113_n1011RegrasContagemAnexo_Descricao ;
      private String[] BC003113_A646TipoDocumento_Nome ;
      private int[] BC003113_A645TipoDocumento_Codigo ;
      private bool[] BC003113_n645TipoDocumento_Codigo ;
      private String[] BC003113_A860RegrasContagem_Regra ;
      private String[] BC003113_A1007RegrasContagemAnexo_Arquivo ;
      private bool[] BC003113_n1007RegrasContagemAnexo_Arquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class regrascontagemanexos_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00316 ;
          prmBC00316 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00315 ;
          prmBC00315 = new Object[] {
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmBC00314 ;
          prmBC00314 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00317 ;
          prmBC00317 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00313 ;
          prmBC00313 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00312 ;
          prmBC00312 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC00318 ;
          prmBC00318 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@RegrasContagemAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@RegrasContagemAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@RegrasContagemAnexo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RegrasContagemAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0}
          } ;
          Object[] prmBC00319 ;
          prmBC00319 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@RegrasContagemAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@RegrasContagemAnexo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RegrasContagemAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@RegrasContagem_Regra",SqlDbType.VarChar,255,0} ,
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003110 ;
          prmBC003110 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003111 ;
          prmBC003111 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003112 ;
          prmBC003112 = new Object[] {
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003113 ;
          prmBC003113 = new Object[] {
          new Object[] {"@RegrasContagemAnexo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00312", "SELECT [RegrasContagemAnexo_Codigo], [RegrasContagemAnexo_NomeArq], [RegrasContagemAnexo_TipoArq], [RegrasContagemAnexo_Data], [RegrasContagemAnexo_Descricao], [TipoDocumento_Codigo], [RegrasContagem_Regra], [RegrasContagemAnexo_Arquivo] FROM [RegrasContagemAnexos] WITH (UPDLOCK) WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00312,1,0,true,false )
             ,new CursorDef("BC00313", "SELECT [RegrasContagemAnexo_Codigo], [RegrasContagemAnexo_NomeArq], [RegrasContagemAnexo_TipoArq], [RegrasContagemAnexo_Data], [RegrasContagemAnexo_Descricao], [TipoDocumento_Codigo], [RegrasContagem_Regra], [RegrasContagemAnexo_Arquivo] FROM [RegrasContagemAnexos] WITH (NOLOCK) WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00313,1,0,true,false )
             ,new CursorDef("BC00314", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00314,1,0,true,false )
             ,new CursorDef("BC00315", "SELECT [RegrasContagem_Regra] FROM [RegrasContagem] WITH (NOLOCK) WHERE [RegrasContagem_Regra] = @RegrasContagem_Regra ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00315,1,0,true,false )
             ,new CursorDef("BC00316", "SELECT TM1.[RegrasContagemAnexo_Codigo], TM1.[RegrasContagemAnexo_NomeArq], TM1.[RegrasContagemAnexo_TipoArq], TM1.[RegrasContagemAnexo_Data], TM1.[RegrasContagemAnexo_Descricao], T2.[TipoDocumento_Nome], TM1.[TipoDocumento_Codigo], TM1.[RegrasContagem_Regra], TM1.[RegrasContagemAnexo_Arquivo] FROM ([RegrasContagemAnexos] TM1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) WHERE TM1.[RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo ORDER BY TM1.[RegrasContagemAnexo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00316,100,0,true,false )
             ,new CursorDef("BC00317", "SELECT [RegrasContagemAnexo_Codigo] FROM [RegrasContagemAnexos] WITH (NOLOCK) WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00317,1,0,true,false )
             ,new CursorDef("BC00318", "INSERT INTO [RegrasContagemAnexos]([RegrasContagemAnexo_Arquivo], [RegrasContagemAnexo_NomeArq], [RegrasContagemAnexo_TipoArq], [RegrasContagemAnexo_Data], [RegrasContagemAnexo_Descricao], [TipoDocumento_Codigo], [RegrasContagem_Regra]) VALUES(@RegrasContagemAnexo_Arquivo, @RegrasContagemAnexo_NomeArq, @RegrasContagemAnexo_TipoArq, @RegrasContagemAnexo_Data, @RegrasContagemAnexo_Descricao, @TipoDocumento_Codigo, @RegrasContagem_Regra); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC00318)
             ,new CursorDef("BC00319", "UPDATE [RegrasContagemAnexos] SET [RegrasContagemAnexo_NomeArq]=@RegrasContagemAnexo_NomeArq, [RegrasContagemAnexo_TipoArq]=@RegrasContagemAnexo_TipoArq, [RegrasContagemAnexo_Data]=@RegrasContagemAnexo_Data, [RegrasContagemAnexo_Descricao]=@RegrasContagemAnexo_Descricao, [TipoDocumento_Codigo]=@TipoDocumento_Codigo, [RegrasContagem_Regra]=@RegrasContagem_Regra  WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo", GxErrorMask.GX_NOMASK,prmBC00319)
             ,new CursorDef("BC003110", "UPDATE [RegrasContagemAnexos] SET [RegrasContagemAnexo_Arquivo]=@RegrasContagemAnexo_Arquivo  WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo", GxErrorMask.GX_NOMASK,prmBC003110)
             ,new CursorDef("BC003111", "DELETE FROM [RegrasContagemAnexos]  WHERE [RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo", GxErrorMask.GX_NOMASK,prmBC003111)
             ,new CursorDef("BC003112", "SELECT [TipoDocumento_Nome] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Codigo] = @TipoDocumento_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003112,1,0,true,false )
             ,new CursorDef("BC003113", "SELECT TM1.[RegrasContagemAnexo_Codigo], TM1.[RegrasContagemAnexo_NomeArq], TM1.[RegrasContagemAnexo_TipoArq], TM1.[RegrasContagemAnexo_Data], TM1.[RegrasContagemAnexo_Descricao], T2.[TipoDocumento_Nome], TM1.[TipoDocumento_Codigo], TM1.[RegrasContagem_Regra], TM1.[RegrasContagemAnexo_Arquivo] FROM ([RegrasContagemAnexos] TM1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = TM1.[TipoDocumento_Codigo]) WHERE TM1.[RegrasContagemAnexo_Codigo] = @RegrasContagemAnexo_Codigo ORDER BY TM1.[RegrasContagemAnexo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003113,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[12])[0] = rslt.getBLOBFile(8, "tmp", "") ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[12])[0] = rslt.getBLOBFile(8, "tmp", "") ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, "tmp", "") ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 10) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 50) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((String[]) buf[13])[0] = rslt.getBLOBFile(9, "tmp", "") ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (String)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[11]);
                }
                stmt.SetParameter(7, (String)parms[12]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 3 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(3, (DateTime)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[9]);
                }
                stmt.SetParameter(6, (String)parms[10]);
                stmt.SetParameter(7, (int)parms[11]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
