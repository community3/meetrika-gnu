/*
               File: GetWWParametrosPlanilhasFilterData
        Description: Get WWParametros Planilhas Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:25.83
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwparametrosplanilhasfilterdata : GXProcedure
   {
      public getwwparametrosplanilhasfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwparametrosplanilhasfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwparametrosplanilhasfilterdata objgetwwparametrosplanilhasfilterdata;
         objgetwwparametrosplanilhasfilterdata = new getwwparametrosplanilhasfilterdata();
         objgetwwparametrosplanilhasfilterdata.AV20DDOName = aP0_DDOName;
         objgetwwparametrosplanilhasfilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetwwparametrosplanilhasfilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetwwparametrosplanilhasfilterdata.AV24OptionsJson = "" ;
         objgetwwparametrosplanilhasfilterdata.AV27OptionsDescJson = "" ;
         objgetwwparametrosplanilhasfilterdata.AV29OptionIndexesJson = "" ;
         objgetwwparametrosplanilhasfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwparametrosplanilhasfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwparametrosplanilhasfilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwparametrosplanilhasfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_PARAMETROSPLN_AREATRABALHOCOD") == 0 )
         {
            /* Execute user subroutine: 'LOADPARAMETROSPLN_AREATRABALHOCODOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_PARAMETROSPLN_ABA") == 0 )
         {
            /* Execute user subroutine: 'LOADPARAMETROSPLN_ABAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_PARAMETROSPLN_CAMPO") == 0 )
         {
            /* Execute user subroutine: 'LOADPARAMETROSPLN_CAMPOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_PARAMETROSPLN_COLUNA") == 0 )
         {
            /* Execute user subroutine: 'LOADPARAMETROSPLN_COLUNAOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("WWParametrosPlanilhasGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWParametrosPlanilhasGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("WWParametrosPlanilhasGridState"), "");
         }
         AV49GXV1 = 1;
         while ( AV49GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV49GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_AREATRABALHOCOD") == 0 )
            {
               AV10TFParametrosPln_AreaTrabalhoCod = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_AREATRABALHOCOD_SEL") == 0 )
            {
               AV44TFParametrosPln_AreaTrabalhoCod_Sel = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_ABA") == 0 )
            {
               AV45TFParametrosPln_Aba = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_ABA_SEL") == 0 )
            {
               AV46TFParametrosPln_Aba_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_CAMPO") == 0 )
            {
               AV12TFParametrosPln_Campo = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_CAMPO_SEL") == 0 )
            {
               AV13TFParametrosPln_Campo_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_COLUNA") == 0 )
            {
               AV14TFParametrosPln_Coluna = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_COLUNA_SEL") == 0 )
            {
               AV15TFParametrosPln_Coluna_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFPARAMETROSPLN_LINHA") == 0 )
            {
               AV16TFParametrosPln_Linha = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Value, "."));
               AV17TFParametrosPln_Linha_To = (int)(NumberUtil.Val( AV34GridStateFilterValue.gxTpr_Valueto, "."));
            }
            AV49GXV1 = (int)(AV49GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "PARAMETROSPLN_CAMPO") == 0 )
            {
               AV37ParametrosPln_Campo1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "PARAMETROSPLN_CAMPO") == 0 )
               {
                  AV40ParametrosPln_Campo2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV41DynamicFiltersEnabled3 = true;
                  AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(3));
                  AV42DynamicFiltersSelector3 = AV35GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV42DynamicFiltersSelector3, "PARAMETROSPLN_CAMPO") == 0 )
                  {
                     AV43ParametrosPln_Campo3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADPARAMETROSPLN_AREATRABALHOCODOPTIONS' Routine */
         AV10TFParametrosPln_AreaTrabalhoCod = AV18SearchTxt;
         AV44TFParametrosPln_AreaTrabalhoCod_Sel = 0;
         AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 = AV37ParametrosPln_Campo1;
         AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 = AV40ParametrosPln_Campo2;
         AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 = AV41DynamicFiltersEnabled3;
         AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = AV42DynamicFiltersSelector3;
         AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 = AV43ParametrosPln_Campo3;
         AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = AV10TFParametrosPln_AreaTrabalhoCod;
         AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel = AV44TFParametrosPln_AreaTrabalhoCod_Sel;
         AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba = AV45TFParametrosPln_Aba;
         AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = AV46TFParametrosPln_Aba_Sel;
         AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo = AV12TFParametrosPln_Campo;
         AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = AV13TFParametrosPln_Campo_Sel;
         AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = AV14TFParametrosPln_Coluna;
         AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = AV15TFParametrosPln_Coluna_Sel;
         AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha = AV16TFParametrosPln_Linha;
         AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to = AV17TFParametrosPln_Linha_To;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                              AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                              AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                              AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                              AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                              AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                              AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                              AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                              AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                              AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                              AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                              AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                              AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                              AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                              AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                              AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                              AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                              AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                              A849ParametrosPln_Campo ,
                                              A6AreaTrabalho_Descricao ,
                                              A847ParametrosPln_AreaTrabalhoCod ,
                                              A2015ParametrosPln_Aba ,
                                              A850ParametrosPln_Coluna ,
                                              A851ParametrosPln_Linha },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT
                                              }
         });
         lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 = StringUtil.Concat( StringUtil.RTrim( AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1), "%", "");
         lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 = StringUtil.Concat( StringUtil.RTrim( AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2), "%", "");
         lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 = StringUtil.Concat( StringUtil.RTrim( AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3), "%", "");
         lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = StringUtil.Concat( StringUtil.RTrim( AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod), "%", "");
         lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba = StringUtil.PadR( StringUtil.RTrim( AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba), 30, "%");
         lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo = StringUtil.Concat( StringUtil.RTrim( AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo), "%", "");
         lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = StringUtil.PadR( StringUtil.RTrim( AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna), 2, "%");
         /* Using cursor P00ON2 */
         pr_default.execute(0, new Object[] {lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1, lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2, lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3, lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod, AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel, lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba, AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel, lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo, AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel, lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna, AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel, AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha, AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKON2 = false;
            A847ParametrosPln_AreaTrabalhoCod = P00ON2_A847ParametrosPln_AreaTrabalhoCod[0];
            n847ParametrosPln_AreaTrabalhoCod = P00ON2_n847ParametrosPln_AreaTrabalhoCod[0];
            A851ParametrosPln_Linha = P00ON2_A851ParametrosPln_Linha[0];
            A850ParametrosPln_Coluna = P00ON2_A850ParametrosPln_Coluna[0];
            A2015ParametrosPln_Aba = P00ON2_A2015ParametrosPln_Aba[0];
            A6AreaTrabalho_Descricao = P00ON2_A6AreaTrabalho_Descricao[0];
            n6AreaTrabalho_Descricao = P00ON2_n6AreaTrabalho_Descricao[0];
            A849ParametrosPln_Campo = P00ON2_A849ParametrosPln_Campo[0];
            A848ParametrosPln_Codigo = P00ON2_A848ParametrosPln_Codigo[0];
            A6AreaTrabalho_Descricao = P00ON2_A6AreaTrabalho_Descricao[0];
            n6AreaTrabalho_Descricao = P00ON2_n6AreaTrabalho_Descricao[0];
            AV30count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00ON2_A847ParametrosPln_AreaTrabalhoCod[0] == A847ParametrosPln_AreaTrabalhoCod ) )
            {
               BRKON2 = false;
               A848ParametrosPln_Codigo = P00ON2_A848ParametrosPln_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKON2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A6AreaTrabalho_Descricao)) )
            {
               AV22Option = StringUtil.Str( (decimal)(A847ParametrosPln_AreaTrabalhoCod), 6, 0);
               AV25OptionDesc = A6AreaTrabalho_Descricao;
               AV21InsertIndex = 1;
               while ( ( AV21InsertIndex <= AV23Options.Count ) && ( StringUtil.StrCmp(((String)AV26OptionsDesc.Item(AV21InsertIndex)), AV25OptionDesc) < 0 ) )
               {
                  AV21InsertIndex = (int)(AV21InsertIndex+1);
               }
               AV23Options.Add(AV22Option, AV21InsertIndex);
               AV26OptionsDesc.Add(AV25OptionDesc, AV21InsertIndex);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), AV21InsertIndex);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKON2 )
            {
               BRKON2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADPARAMETROSPLN_ABAOPTIONS' Routine */
         AV45TFParametrosPln_Aba = AV18SearchTxt;
         AV46TFParametrosPln_Aba_Sel = "";
         AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 = AV37ParametrosPln_Campo1;
         AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 = AV40ParametrosPln_Campo2;
         AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 = AV41DynamicFiltersEnabled3;
         AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = AV42DynamicFiltersSelector3;
         AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 = AV43ParametrosPln_Campo3;
         AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = AV10TFParametrosPln_AreaTrabalhoCod;
         AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel = AV44TFParametrosPln_AreaTrabalhoCod_Sel;
         AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba = AV45TFParametrosPln_Aba;
         AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = AV46TFParametrosPln_Aba_Sel;
         AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo = AV12TFParametrosPln_Campo;
         AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = AV13TFParametrosPln_Campo_Sel;
         AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = AV14TFParametrosPln_Coluna;
         AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = AV15TFParametrosPln_Coluna_Sel;
         AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha = AV16TFParametrosPln_Linha;
         AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to = AV17TFParametrosPln_Linha_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                              AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                              AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                              AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                              AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                              AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                              AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                              AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                              AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                              AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                              AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                              AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                              AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                              AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                              AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                              AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                              AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                              AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                              A849ParametrosPln_Campo ,
                                              A6AreaTrabalho_Descricao ,
                                              A847ParametrosPln_AreaTrabalhoCod ,
                                              A2015ParametrosPln_Aba ,
                                              A850ParametrosPln_Coluna ,
                                              A851ParametrosPln_Linha },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT
                                              }
         });
         lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 = StringUtil.Concat( StringUtil.RTrim( AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1), "%", "");
         lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 = StringUtil.Concat( StringUtil.RTrim( AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2), "%", "");
         lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 = StringUtil.Concat( StringUtil.RTrim( AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3), "%", "");
         lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = StringUtil.Concat( StringUtil.RTrim( AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod), "%", "");
         lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba = StringUtil.PadR( StringUtil.RTrim( AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba), 30, "%");
         lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo = StringUtil.Concat( StringUtil.RTrim( AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo), "%", "");
         lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = StringUtil.PadR( StringUtil.RTrim( AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna), 2, "%");
         /* Using cursor P00ON3 */
         pr_default.execute(1, new Object[] {lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1, lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2, lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3, lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod, AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel, lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba, AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel, lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo, AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel, lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna, AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel, AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha, AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKON4 = false;
            A2015ParametrosPln_Aba = P00ON3_A2015ParametrosPln_Aba[0];
            A851ParametrosPln_Linha = P00ON3_A851ParametrosPln_Linha[0];
            A850ParametrosPln_Coluna = P00ON3_A850ParametrosPln_Coluna[0];
            A847ParametrosPln_AreaTrabalhoCod = P00ON3_A847ParametrosPln_AreaTrabalhoCod[0];
            n847ParametrosPln_AreaTrabalhoCod = P00ON3_n847ParametrosPln_AreaTrabalhoCod[0];
            A6AreaTrabalho_Descricao = P00ON3_A6AreaTrabalho_Descricao[0];
            n6AreaTrabalho_Descricao = P00ON3_n6AreaTrabalho_Descricao[0];
            A849ParametrosPln_Campo = P00ON3_A849ParametrosPln_Campo[0];
            A848ParametrosPln_Codigo = P00ON3_A848ParametrosPln_Codigo[0];
            A6AreaTrabalho_Descricao = P00ON3_A6AreaTrabalho_Descricao[0];
            n6AreaTrabalho_Descricao = P00ON3_n6AreaTrabalho_Descricao[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00ON3_A2015ParametrosPln_Aba[0], A2015ParametrosPln_Aba) == 0 ) )
            {
               BRKON4 = false;
               A848ParametrosPln_Codigo = P00ON3_A848ParametrosPln_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKON4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A2015ParametrosPln_Aba)) )
            {
               AV22Option = A2015ParametrosPln_Aba;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKON4 )
            {
               BRKON4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADPARAMETROSPLN_CAMPOOPTIONS' Routine */
         AV12TFParametrosPln_Campo = AV18SearchTxt;
         AV13TFParametrosPln_Campo_Sel = "";
         AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 = AV37ParametrosPln_Campo1;
         AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 = AV40ParametrosPln_Campo2;
         AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 = AV41DynamicFiltersEnabled3;
         AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = AV42DynamicFiltersSelector3;
         AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 = AV43ParametrosPln_Campo3;
         AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = AV10TFParametrosPln_AreaTrabalhoCod;
         AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel = AV44TFParametrosPln_AreaTrabalhoCod_Sel;
         AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba = AV45TFParametrosPln_Aba;
         AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = AV46TFParametrosPln_Aba_Sel;
         AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo = AV12TFParametrosPln_Campo;
         AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = AV13TFParametrosPln_Campo_Sel;
         AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = AV14TFParametrosPln_Coluna;
         AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = AV15TFParametrosPln_Coluna_Sel;
         AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha = AV16TFParametrosPln_Linha;
         AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to = AV17TFParametrosPln_Linha_To;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                              AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                              AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                              AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                              AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                              AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                              AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                              AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                              AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                              AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                              AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                              AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                              AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                              AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                              AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                              AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                              AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                              AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                              A849ParametrosPln_Campo ,
                                              A6AreaTrabalho_Descricao ,
                                              A847ParametrosPln_AreaTrabalhoCod ,
                                              A2015ParametrosPln_Aba ,
                                              A850ParametrosPln_Coluna ,
                                              A851ParametrosPln_Linha },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT
                                              }
         });
         lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 = StringUtil.Concat( StringUtil.RTrim( AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1), "%", "");
         lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 = StringUtil.Concat( StringUtil.RTrim( AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2), "%", "");
         lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 = StringUtil.Concat( StringUtil.RTrim( AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3), "%", "");
         lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = StringUtil.Concat( StringUtil.RTrim( AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod), "%", "");
         lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba = StringUtil.PadR( StringUtil.RTrim( AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba), 30, "%");
         lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo = StringUtil.Concat( StringUtil.RTrim( AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo), "%", "");
         lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = StringUtil.PadR( StringUtil.RTrim( AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna), 2, "%");
         /* Using cursor P00ON4 */
         pr_default.execute(2, new Object[] {lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1, lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2, lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3, lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod, AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel, lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba, AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel, lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo, AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel, lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna, AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel, AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha, AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKON6 = false;
            A849ParametrosPln_Campo = P00ON4_A849ParametrosPln_Campo[0];
            A851ParametrosPln_Linha = P00ON4_A851ParametrosPln_Linha[0];
            A850ParametrosPln_Coluna = P00ON4_A850ParametrosPln_Coluna[0];
            A2015ParametrosPln_Aba = P00ON4_A2015ParametrosPln_Aba[0];
            A847ParametrosPln_AreaTrabalhoCod = P00ON4_A847ParametrosPln_AreaTrabalhoCod[0];
            n847ParametrosPln_AreaTrabalhoCod = P00ON4_n847ParametrosPln_AreaTrabalhoCod[0];
            A6AreaTrabalho_Descricao = P00ON4_A6AreaTrabalho_Descricao[0];
            n6AreaTrabalho_Descricao = P00ON4_n6AreaTrabalho_Descricao[0];
            A848ParametrosPln_Codigo = P00ON4_A848ParametrosPln_Codigo[0];
            A6AreaTrabalho_Descricao = P00ON4_A6AreaTrabalho_Descricao[0];
            n6AreaTrabalho_Descricao = P00ON4_n6AreaTrabalho_Descricao[0];
            AV30count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00ON4_A849ParametrosPln_Campo[0], A849ParametrosPln_Campo) == 0 ) )
            {
               BRKON6 = false;
               A848ParametrosPln_Codigo = P00ON4_A848ParametrosPln_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKON6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A849ParametrosPln_Campo)) )
            {
               AV22Option = A849ParametrosPln_Campo;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKON6 )
            {
               BRKON6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADPARAMETROSPLN_COLUNAOPTIONS' Routine */
         AV14TFParametrosPln_Coluna = AV18SearchTxt;
         AV15TFParametrosPln_Coluna_Sel = "";
         AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 = AV37ParametrosPln_Campo1;
         AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 = AV38DynamicFiltersEnabled2;
         AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = AV39DynamicFiltersSelector2;
         AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 = AV40ParametrosPln_Campo2;
         AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 = AV41DynamicFiltersEnabled3;
         AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = AV42DynamicFiltersSelector3;
         AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 = AV43ParametrosPln_Campo3;
         AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = AV10TFParametrosPln_AreaTrabalhoCod;
         AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel = AV44TFParametrosPln_AreaTrabalhoCod_Sel;
         AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba = AV45TFParametrosPln_Aba;
         AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = AV46TFParametrosPln_Aba_Sel;
         AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo = AV12TFParametrosPln_Campo;
         AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = AV13TFParametrosPln_Campo_Sel;
         AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = AV14TFParametrosPln_Coluna;
         AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = AV15TFParametrosPln_Coluna_Sel;
         AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha = AV16TFParametrosPln_Linha;
         AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to = AV17TFParametrosPln_Linha_To;
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                              AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                              AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                              AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                              AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                              AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                              AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                              AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                              AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                              AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                              AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                              AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                              AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                              AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                              AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                              AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                              AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                              AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                              A849ParametrosPln_Campo ,
                                              A6AreaTrabalho_Descricao ,
                                              A847ParametrosPln_AreaTrabalhoCod ,
                                              A2015ParametrosPln_Aba ,
                                              A850ParametrosPln_Coluna ,
                                              A851ParametrosPln_Linha },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT
                                              }
         });
         lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 = StringUtil.Concat( StringUtil.RTrim( AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1), "%", "");
         lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 = StringUtil.Concat( StringUtil.RTrim( AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2), "%", "");
         lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 = StringUtil.Concat( StringUtil.RTrim( AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3), "%", "");
         lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = StringUtil.Concat( StringUtil.RTrim( AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod), "%", "");
         lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba = StringUtil.PadR( StringUtil.RTrim( AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba), 30, "%");
         lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo = StringUtil.Concat( StringUtil.RTrim( AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo), "%", "");
         lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = StringUtil.PadR( StringUtil.RTrim( AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna), 2, "%");
         /* Using cursor P00ON5 */
         pr_default.execute(3, new Object[] {lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1, lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2, lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3, lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod, AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel, lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba, AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel, lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo, AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel, lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna, AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel, AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha, AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKON8 = false;
            A850ParametrosPln_Coluna = P00ON5_A850ParametrosPln_Coluna[0];
            A851ParametrosPln_Linha = P00ON5_A851ParametrosPln_Linha[0];
            A2015ParametrosPln_Aba = P00ON5_A2015ParametrosPln_Aba[0];
            A847ParametrosPln_AreaTrabalhoCod = P00ON5_A847ParametrosPln_AreaTrabalhoCod[0];
            n847ParametrosPln_AreaTrabalhoCod = P00ON5_n847ParametrosPln_AreaTrabalhoCod[0];
            A6AreaTrabalho_Descricao = P00ON5_A6AreaTrabalho_Descricao[0];
            n6AreaTrabalho_Descricao = P00ON5_n6AreaTrabalho_Descricao[0];
            A849ParametrosPln_Campo = P00ON5_A849ParametrosPln_Campo[0];
            A848ParametrosPln_Codigo = P00ON5_A848ParametrosPln_Codigo[0];
            A6AreaTrabalho_Descricao = P00ON5_A6AreaTrabalho_Descricao[0];
            n6AreaTrabalho_Descricao = P00ON5_n6AreaTrabalho_Descricao[0];
            AV30count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00ON5_A850ParametrosPln_Coluna[0], A850ParametrosPln_Coluna) == 0 ) )
            {
               BRKON8 = false;
               A848ParametrosPln_Codigo = P00ON5_A848ParametrosPln_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKON8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A850ParametrosPln_Coluna)) )
            {
               AV22Option = A850ParametrosPln_Coluna;
               AV25OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A850ParametrosPln_Coluna, "@!")));
               AV23Options.Add(AV22Option, 0);
               AV26OptionsDesc.Add(AV25OptionDesc, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKON8 )
            {
               BRKON8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFParametrosPln_AreaTrabalhoCod = "";
         AV45TFParametrosPln_Aba = "";
         AV46TFParametrosPln_Aba_Sel = "";
         AV12TFParametrosPln_Campo = "";
         AV13TFParametrosPln_Campo_Sel = "";
         AV14TFParametrosPln_Coluna = "";
         AV15TFParametrosPln_Coluna_Sel = "";
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV37ParametrosPln_Campo1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV40ParametrosPln_Campo2 = "";
         AV42DynamicFiltersSelector3 = "";
         AV43ParametrosPln_Campo3 = "";
         AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 = "";
         AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 = "";
         AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 = "";
         AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 = "";
         AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 = "";
         AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 = "";
         AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = "";
         AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba = "";
         AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel = "";
         AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo = "";
         AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel = "";
         AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = "";
         AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel = "";
         scmdbuf = "";
         lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 = "";
         lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 = "";
         lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 = "";
         lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod = "";
         lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba = "";
         lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo = "";
         lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna = "";
         A849ParametrosPln_Campo = "";
         A6AreaTrabalho_Descricao = "";
         A2015ParametrosPln_Aba = "";
         A850ParametrosPln_Coluna = "";
         P00ON2_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         P00ON2_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         P00ON2_A851ParametrosPln_Linha = new int[1] ;
         P00ON2_A850ParametrosPln_Coluna = new String[] {""} ;
         P00ON2_A2015ParametrosPln_Aba = new String[] {""} ;
         P00ON2_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00ON2_n6AreaTrabalho_Descricao = new bool[] {false} ;
         P00ON2_A849ParametrosPln_Campo = new String[] {""} ;
         P00ON2_A848ParametrosPln_Codigo = new int[1] ;
         AV22Option = "";
         AV25OptionDesc = "";
         P00ON3_A2015ParametrosPln_Aba = new String[] {""} ;
         P00ON3_A851ParametrosPln_Linha = new int[1] ;
         P00ON3_A850ParametrosPln_Coluna = new String[] {""} ;
         P00ON3_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         P00ON3_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         P00ON3_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00ON3_n6AreaTrabalho_Descricao = new bool[] {false} ;
         P00ON3_A849ParametrosPln_Campo = new String[] {""} ;
         P00ON3_A848ParametrosPln_Codigo = new int[1] ;
         P00ON4_A849ParametrosPln_Campo = new String[] {""} ;
         P00ON4_A851ParametrosPln_Linha = new int[1] ;
         P00ON4_A850ParametrosPln_Coluna = new String[] {""} ;
         P00ON4_A2015ParametrosPln_Aba = new String[] {""} ;
         P00ON4_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         P00ON4_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         P00ON4_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00ON4_n6AreaTrabalho_Descricao = new bool[] {false} ;
         P00ON4_A848ParametrosPln_Codigo = new int[1] ;
         P00ON5_A850ParametrosPln_Coluna = new String[] {""} ;
         P00ON5_A851ParametrosPln_Linha = new int[1] ;
         P00ON5_A2015ParametrosPln_Aba = new String[] {""} ;
         P00ON5_A847ParametrosPln_AreaTrabalhoCod = new int[1] ;
         P00ON5_n847ParametrosPln_AreaTrabalhoCod = new bool[] {false} ;
         P00ON5_A6AreaTrabalho_Descricao = new String[] {""} ;
         P00ON5_n6AreaTrabalho_Descricao = new bool[] {false} ;
         P00ON5_A849ParametrosPln_Campo = new String[] {""} ;
         P00ON5_A848ParametrosPln_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwparametrosplanilhasfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00ON2_A847ParametrosPln_AreaTrabalhoCod, P00ON2_n847ParametrosPln_AreaTrabalhoCod, P00ON2_A851ParametrosPln_Linha, P00ON2_A850ParametrosPln_Coluna, P00ON2_A2015ParametrosPln_Aba, P00ON2_A6AreaTrabalho_Descricao, P00ON2_n6AreaTrabalho_Descricao, P00ON2_A849ParametrosPln_Campo, P00ON2_A848ParametrosPln_Codigo
               }
               , new Object[] {
               P00ON3_A2015ParametrosPln_Aba, P00ON3_A851ParametrosPln_Linha, P00ON3_A850ParametrosPln_Coluna, P00ON3_A847ParametrosPln_AreaTrabalhoCod, P00ON3_n847ParametrosPln_AreaTrabalhoCod, P00ON3_A6AreaTrabalho_Descricao, P00ON3_n6AreaTrabalho_Descricao, P00ON3_A849ParametrosPln_Campo, P00ON3_A848ParametrosPln_Codigo
               }
               , new Object[] {
               P00ON4_A849ParametrosPln_Campo, P00ON4_A851ParametrosPln_Linha, P00ON4_A850ParametrosPln_Coluna, P00ON4_A2015ParametrosPln_Aba, P00ON4_A847ParametrosPln_AreaTrabalhoCod, P00ON4_n847ParametrosPln_AreaTrabalhoCod, P00ON4_A6AreaTrabalho_Descricao, P00ON4_n6AreaTrabalho_Descricao, P00ON4_A848ParametrosPln_Codigo
               }
               , new Object[] {
               P00ON5_A850ParametrosPln_Coluna, P00ON5_A851ParametrosPln_Linha, P00ON5_A2015ParametrosPln_Aba, P00ON5_A847ParametrosPln_AreaTrabalhoCod, P00ON5_n847ParametrosPln_AreaTrabalhoCod, P00ON5_A6AreaTrabalho_Descricao, P00ON5_n6AreaTrabalho_Descricao, P00ON5_A849ParametrosPln_Campo, P00ON5_A848ParametrosPln_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV49GXV1 ;
      private int AV44TFParametrosPln_AreaTrabalhoCod_Sel ;
      private int AV16TFParametrosPln_Linha ;
      private int AV17TFParametrosPln_Linha_To ;
      private int AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ;
      private int AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha ;
      private int AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ;
      private int A847ParametrosPln_AreaTrabalhoCod ;
      private int A851ParametrosPln_Linha ;
      private int A848ParametrosPln_Codigo ;
      private int AV21InsertIndex ;
      private long AV30count ;
      private String AV45TFParametrosPln_Aba ;
      private String AV46TFParametrosPln_Aba_Sel ;
      private String AV14TFParametrosPln_Coluna ;
      private String AV15TFParametrosPln_Coluna_Sel ;
      private String AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba ;
      private String AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ;
      private String AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ;
      private String AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ;
      private String scmdbuf ;
      private String lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba ;
      private String lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ;
      private String A2015ParametrosPln_Aba ;
      private String A850ParametrosPln_Coluna ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool AV41DynamicFiltersEnabled3 ;
      private bool AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ;
      private bool AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ;
      private bool BRKON2 ;
      private bool n847ParametrosPln_AreaTrabalhoCod ;
      private bool n6AreaTrabalho_Descricao ;
      private bool BRKON4 ;
      private bool BRKON6 ;
      private bool BRKON8 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV10TFParametrosPln_AreaTrabalhoCod ;
      private String AV12TFParametrosPln_Campo ;
      private String AV13TFParametrosPln_Campo_Sel ;
      private String AV36DynamicFiltersSelector1 ;
      private String AV37ParametrosPln_Campo1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String AV40ParametrosPln_Campo2 ;
      private String AV42DynamicFiltersSelector3 ;
      private String AV43ParametrosPln_Campo3 ;
      private String AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ;
      private String AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 ;
      private String AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ;
      private String AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 ;
      private String AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ;
      private String AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 ;
      private String AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ;
      private String AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo ;
      private String AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ;
      private String lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 ;
      private String lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 ;
      private String lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 ;
      private String lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ;
      private String lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo ;
      private String A849ParametrosPln_Campo ;
      private String A6AreaTrabalho_Descricao ;
      private String AV22Option ;
      private String AV25OptionDesc ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00ON2_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] P00ON2_n847ParametrosPln_AreaTrabalhoCod ;
      private int[] P00ON2_A851ParametrosPln_Linha ;
      private String[] P00ON2_A850ParametrosPln_Coluna ;
      private String[] P00ON2_A2015ParametrosPln_Aba ;
      private String[] P00ON2_A6AreaTrabalho_Descricao ;
      private bool[] P00ON2_n6AreaTrabalho_Descricao ;
      private String[] P00ON2_A849ParametrosPln_Campo ;
      private int[] P00ON2_A848ParametrosPln_Codigo ;
      private String[] P00ON3_A2015ParametrosPln_Aba ;
      private int[] P00ON3_A851ParametrosPln_Linha ;
      private String[] P00ON3_A850ParametrosPln_Coluna ;
      private int[] P00ON3_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] P00ON3_n847ParametrosPln_AreaTrabalhoCod ;
      private String[] P00ON3_A6AreaTrabalho_Descricao ;
      private bool[] P00ON3_n6AreaTrabalho_Descricao ;
      private String[] P00ON3_A849ParametrosPln_Campo ;
      private int[] P00ON3_A848ParametrosPln_Codigo ;
      private String[] P00ON4_A849ParametrosPln_Campo ;
      private int[] P00ON4_A851ParametrosPln_Linha ;
      private String[] P00ON4_A850ParametrosPln_Coluna ;
      private String[] P00ON4_A2015ParametrosPln_Aba ;
      private int[] P00ON4_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] P00ON4_n847ParametrosPln_AreaTrabalhoCod ;
      private String[] P00ON4_A6AreaTrabalho_Descricao ;
      private bool[] P00ON4_n6AreaTrabalho_Descricao ;
      private int[] P00ON4_A848ParametrosPln_Codigo ;
      private String[] P00ON5_A850ParametrosPln_Coluna ;
      private int[] P00ON5_A851ParametrosPln_Linha ;
      private String[] P00ON5_A2015ParametrosPln_Aba ;
      private int[] P00ON5_A847ParametrosPln_AreaTrabalhoCod ;
      private bool[] P00ON5_n847ParametrosPln_AreaTrabalhoCod ;
      private String[] P00ON5_A6AreaTrabalho_Descricao ;
      private bool[] P00ON5_n6AreaTrabalho_Descricao ;
      private String[] P00ON5_A849ParametrosPln_Campo ;
      private int[] P00ON5_A848ParametrosPln_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getwwparametrosplanilhasfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00ON2( IGxContext context ,
                                             String AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                             String AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                             bool AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                             String AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                             String AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                             bool AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                             String AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                             String AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                             int AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                             String AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                             String AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                             String AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                             String AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                             String AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                             String AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                             String AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                             int AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                             int AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                             String A849ParametrosPln_Campo ,
                                             String A6AreaTrabalho_Descricao ,
                                             int A847ParametrosPln_AreaTrabalhoCod ,
                                             String A2015ParametrosPln_Aba ,
                                             String A850ParametrosPln_Coluna ,
                                             int A851ParametrosPln_Linha )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [13] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[ParametrosPln_AreaTrabalhoCod] AS ParametrosPln_AreaTrabalhoCod, T1.[ParametrosPln_Linha], T1.[ParametrosPln_Coluna], T1.[ParametrosPln_Aba], T2.[AreaTrabalho_Descricao], T1.[ParametrosPln_Campo], T1.[ParametrosPln_Codigo] FROM ([ParametrosPlanilhas] T1 WITH (NOLOCK) LEFT JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ParametrosPln_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( (0==AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_AreaTrabalhoCod] = @AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_AreaTrabalhoCod] = @AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] like @lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] like @lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] = @AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] = @AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like @lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like @lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] = @AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] = @AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] like @lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] like @lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] = @AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] = @AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] >= @AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] >= @AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] <= @AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] <= @AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ParametrosPln_AreaTrabalhoCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00ON3( IGxContext context ,
                                             String AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                             String AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                             bool AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                             String AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                             String AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                             bool AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                             String AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                             String AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                             int AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                             String AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                             String AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                             String AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                             String AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                             String AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                             String AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                             String AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                             int AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                             int AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                             String A849ParametrosPln_Campo ,
                                             String A6AreaTrabalho_Descricao ,
                                             int A847ParametrosPln_AreaTrabalhoCod ,
                                             String A2015ParametrosPln_Aba ,
                                             String A850ParametrosPln_Coluna ,
                                             int A851ParametrosPln_Linha )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [13] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ParametrosPln_Aba], T1.[ParametrosPln_Linha], T1.[ParametrosPln_Coluna], T1.[ParametrosPln_AreaTrabalhoCod] AS ParametrosPln_AreaTrabalhoCod, T2.[AreaTrabalho_Descricao], T1.[ParametrosPln_Campo], T1.[ParametrosPln_Codigo] FROM ([ParametrosPlanilhas] T1 WITH (NOLOCK) LEFT JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ParametrosPln_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( (0==AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (0==AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_AreaTrabalhoCod] = @AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_AreaTrabalhoCod] = @AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] like @lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] like @lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] = @AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] = @AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like @lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like @lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] = @AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] = @AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] like @lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] like @lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] = @AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] = @AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! (0==AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] >= @AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] >= @AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] <= @AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] <= @AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ParametrosPln_Aba]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00ON4( IGxContext context ,
                                             String AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                             String AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                             bool AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                             String AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                             String AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                             bool AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                             String AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                             String AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                             int AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                             String AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                             String AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                             String AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                             String AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                             String AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                             String AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                             String AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                             int AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                             int AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                             String A849ParametrosPln_Campo ,
                                             String A6AreaTrabalho_Descricao ,
                                             int A847ParametrosPln_AreaTrabalhoCod ,
                                             String A2015ParametrosPln_Aba ,
                                             String A850ParametrosPln_Coluna ,
                                             int A851ParametrosPln_Linha )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [13] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ParametrosPln_Campo], T1.[ParametrosPln_Linha], T1.[ParametrosPln_Coluna], T1.[ParametrosPln_Aba], T1.[ParametrosPln_AreaTrabalhoCod] AS ParametrosPln_AreaTrabalhoCod, T2.[AreaTrabalho_Descricao], T1.[ParametrosPln_Codigo] FROM ([ParametrosPlanilhas] T1 WITH (NOLOCK) LEFT JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ParametrosPln_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( (0==AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( ! (0==AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_AreaTrabalhoCod] = @AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_AreaTrabalhoCod] = @AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] like @lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] like @lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] = @AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] = @AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like @lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like @lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] = @AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] = @AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] like @lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] like @lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] = @AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] = @AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! (0==AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] >= @AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] >= @AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] <= @AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] <= @AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ParametrosPln_Campo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00ON5( IGxContext context ,
                                             String AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1 ,
                                             String AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 ,
                                             bool AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 ,
                                             String AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2 ,
                                             String AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 ,
                                             bool AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 ,
                                             String AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3 ,
                                             String AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 ,
                                             int AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel ,
                                             String AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod ,
                                             String AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel ,
                                             String AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba ,
                                             String AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel ,
                                             String AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo ,
                                             String AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel ,
                                             String AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna ,
                                             int AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha ,
                                             int AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to ,
                                             String A849ParametrosPln_Campo ,
                                             String A6AreaTrabalho_Descricao ,
                                             int A847ParametrosPln_AreaTrabalhoCod ,
                                             String A2015ParametrosPln_Aba ,
                                             String A850ParametrosPln_Coluna ,
                                             int A851ParametrosPln_Linha )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [13] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ParametrosPln_Coluna], T1.[ParametrosPln_Linha], T1.[ParametrosPln_Aba], T1.[ParametrosPln_AreaTrabalhoCod] AS ParametrosPln_AreaTrabalhoCod, T2.[AreaTrabalho_Descricao], T1.[ParametrosPln_Campo], T1.[ParametrosPln_Codigo] FROM ([ParametrosPlanilhas] T1 WITH (NOLOCK) LEFT JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = T1.[ParametrosPln_AreaTrabalhoCod])";
         if ( ( StringUtil.StrCmp(AV51WWParametrosPlanilhasDS_1_Dynamicfiltersselector1, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWParametrosPlanilhasDS_2_Parametrospln_campo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1 + '%')";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( AV53WWParametrosPlanilhasDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWParametrosPlanilhasDS_4_Dynamicfiltersselector2, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWParametrosPlanilhasDS_5_Parametrospln_campo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2 + '%')";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( AV56WWParametrosPlanilhasDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV57WWParametrosPlanilhasDS_7_Dynamicfiltersselector3, "PARAMETROSPLN_CAMPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWParametrosPlanilhasDS_8_Parametrospln_campo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like '%' + @lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like '%' + @lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3 + '%')";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( (0==AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[AreaTrabalho_Descricao] like @lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[AreaTrabalho_Descricao] like @lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( ! (0==AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_AreaTrabalhoCod] = @AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_AreaTrabalhoCod] = @AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] like @lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] like @lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Aba] = @AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Aba] = @AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] like @lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] like @lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Campo] = @AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Campo] = @AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] like @lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] like @lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Coluna] = @AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Coluna] = @AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( ! (0==AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] >= @AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] >= @AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! (0==AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ParametrosPln_Linha] <= @AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ParametrosPln_Linha] <= @AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ParametrosPln_Coluna]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00ON2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] );
               case 1 :
                     return conditional_P00ON3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] );
               case 2 :
                     return conditional_P00ON4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] );
               case 3 :
                     return conditional_P00ON5(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00ON2 ;
          prmP00ON2 = new Object[] {
          new Object[] {"@lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba",SqlDbType.Char,30,0} ,
          new Object[] {"@AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna",SqlDbType.Char,2,0} ,
          new Object[] {"@AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00ON3 ;
          prmP00ON3 = new Object[] {
          new Object[] {"@lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba",SqlDbType.Char,30,0} ,
          new Object[] {"@AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna",SqlDbType.Char,2,0} ,
          new Object[] {"@AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00ON4 ;
          prmP00ON4 = new Object[] {
          new Object[] {"@lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba",SqlDbType.Char,30,0} ,
          new Object[] {"@AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna",SqlDbType.Char,2,0} ,
          new Object[] {"@AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00ON5 ;
          prmP00ON5 = new Object[] {
          new Object[] {"@lV52WWParametrosPlanilhasDS_2_Parametrospln_campo1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV55WWParametrosPlanilhasDS_5_Parametrospln_campo2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV58WWParametrosPlanilhasDS_8_Parametrospln_campo3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV59WWParametrosPlanilhasDS_9_Tfparametrospln_areatrabalhocod",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWParametrosPlanilhasDS_10_Tfparametrospln_areatrabalhocod_sel",SqlDbType.Int,6,0} ,
          new Object[] {"@lV61WWParametrosPlanilhasDS_11_Tfparametrospln_aba",SqlDbType.Char,30,0} ,
          new Object[] {"@AV62WWParametrosPlanilhasDS_12_Tfparametrospln_aba_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@lV63WWParametrosPlanilhasDS_13_Tfparametrospln_campo",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV64WWParametrosPlanilhasDS_14_Tfparametrospln_campo_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV65WWParametrosPlanilhasDS_15_Tfparametrospln_coluna",SqlDbType.Char,2,0} ,
          new Object[] {"@AV66WWParametrosPlanilhasDS_16_Tfparametrospln_coluna_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@AV67WWParametrosPlanilhasDS_17_Tfparametrospln_linha",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68WWParametrosPlanilhasDS_18_Tfparametrospln_linha_to",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00ON2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00ON2,100,0,true,false )
             ,new CursorDef("P00ON3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00ON3,100,0,true,false )
             ,new CursorDef("P00ON4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00ON4,100,0,true,false )
             ,new CursorDef("P00ON5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00ON5,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 2) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 30) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 30) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 30) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 30) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwparametrosplanilhasfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwparametrosplanilhasfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwparametrosplanilhasfilterdata") )
          {
             return  ;
          }
          getwwparametrosplanilhasfilterdata worker = new getwwparametrosplanilhasfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
