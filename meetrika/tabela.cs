/*
               File: Tabela
        Description: Tabela
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:18:41.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class tabela : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"TABELA_MODULOCOD") == 0 )
         {
            A190Tabela_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLATABELA_MODULOCOD1239( A190Tabela_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"TABELA_PAICOD") == 0 )
         {
            AV16Tabela_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Tabela_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLATABELA_PAICOD1239( AV16Tabela_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxAggSel20"+"_"+"") == 0 )
         {
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_31") == 0 )
         {
            A188Tabela_ModuloCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n188Tabela_ModuloCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_31( A188Tabela_ModuloCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_33") == 0 )
         {
            A181Tabela_PaiCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n181Tabela_PaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_33( A181Tabela_PaiCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_34") == 0 )
         {
            A746Tabela_MelhoraCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n746Tabela_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A746Tabela_MelhoraCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_34( A746Tabela_MelhoraCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABELA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Tabela_Codigo), "ZZZZZ9")));
               A190Tabela_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
               AV15Tabela_ModuloCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Tabela_ModuloCod), 6, 0)));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynTabela_ModuloCod.Name = "TABELA_MODULOCOD";
         dynTabela_ModuloCod.WebTags = "";
         dynTabela_PaiCod.Name = "TABELA_PAICOD";
         dynTabela_PaiCod.WebTags = "";
         chkTabela_Ativo.Name = "TABELA_ATIVO";
         chkTabela_Ativo.WebTags = "";
         chkTabela_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkTabela_Ativo_Internalname, "TitleCaption", chkTabela_Ativo.Caption);
         chkTabela_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Tabela", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtTabela_Nome_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public tabela( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public tabela( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Tabela_Codigo ,
                           ref int aP2_Tabela_SistemaCod ,
                           ref int aP3_Tabela_ModuloCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Tabela_Codigo = aP1_Tabela_Codigo;
         this.A190Tabela_SistemaCod = aP2_Tabela_SistemaCod;
         this.AV15Tabela_ModuloCod = aP3_Tabela_ModuloCod;
         executePrivate();
         aP2_Tabela_SistemaCod=this.A190Tabela_SistemaCod;
         aP3_Tabela_ModuloCod=this.AV15Tabela_ModuloCod;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynTabela_ModuloCod = new GXCombobox();
         dynTabela_PaiCod = new GXCombobox();
         chkTabela_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynTabela_ModuloCod.ItemCount > 0 )
         {
            A188Tabela_ModuloCod = (int)(NumberUtil.Val( dynTabela_ModuloCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0))), "."));
            n188Tabela_ModuloCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
         }
         if ( dynTabela_PaiCod.ItemCount > 0 )
         {
            A181Tabela_PaiCod = (int)(NumberUtil.Val( dynTabela_PaiCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0))), "."));
            n181Tabela_PaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1239( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1239e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTabela_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A172Tabela_Codigo), 6, 0, ",", "")), ((edtTabela_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTabela_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtTabela_Codigo_Visible, edtTabela_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Tabela.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1239( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTabelatitle_Internalname, "Tabela", "", "", lblTabelatitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_11_1239( true) ;
         }
         return  ;
      }

      protected void wb_table2_11_1239e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_49_1239( true) ;
         }
         return  ;
      }

      protected void wb_table3_49_1239e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1239e( true) ;
         }
         else
         {
            wb_table1_2_1239e( false) ;
         }
      }

      protected void wb_table3_49_1239( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtneliminar_Internalname, "", "Eliminar", bttBtneliminar_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtneliminar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOELIMINAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, bttBtnfechar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtncopiarcolar_Internalname, "", "Copiar e Colar", bttBtncopiarcolar_Jsonclick, 5, "Copiar e Colar", "", StyleString, ClassString, bttBtncopiarcolar_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOCOPIARCOLAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_49_1239e( true) ;
         }
         else
         {
            wb_table3_49_1239e( false) ;
         }
      }

      protected void wb_table2_11_1239( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_1239( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_1239e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_11_1239e( true) ;
         }
         else
         {
            wb_table2_11_1239e( false) ;
         }
      }

      protected void wb_table4_16_1239( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_nome_Internalname, "Nome", "", "", lblTextblocktabela_nome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTabela_Nome_Internalname, StringUtil.RTrim( A173Tabela_Nome), StringUtil.RTrim( context.localUtil.Format( A173Tabela_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTabela_Nome_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtTabela_Nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_descricao_Internalname, "Descri��o", "", "", lblTextblocktabela_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtTabela_Descricao_Internalname, A175Tabela_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", 0, 1, edtTabela_Descricao_Enabled, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_modulocod_Internalname, "Modulo", "", "", lblTextblocktabela_modulocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynTabela_ModuloCod, dynTabela_ModuloCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)), 1, dynTabela_ModuloCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynTabela_ModuloCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_Tabela.htm");
            dynTabela_ModuloCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTabela_ModuloCod_Internalname, "Values", (String)(dynTabela_ModuloCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_paicod_Internalname, "Tabela Pai", "", "", lblTextblocktabela_paicod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynTabela_PaiCod, dynTabela_PaiCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)), 1, dynTabela_PaiCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynTabela_PaiCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_Tabela.htm");
            dynTabela_PaiCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTabela_PaiCod_Internalname, "Values", (String)(dynTabela_PaiCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_melhoracod_Internalname, "a melhorar", "", "", lblTextblocktabela_melhoracod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtTabela_MelhoraCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A746Tabela_MelhoraCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(A746Tabela_MelhoraCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTabela_MelhoraCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtTabela_MelhoraCod_Enabled, 1, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocktabela_ativo_Internalname, "Ativo", "", "", lblTextblocktabela_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblocktabela_ativo_Visible, 1, 0, "HLP_Tabela.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "AttSemBordaCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkTabela_Ativo_Internalname, StringUtil.BoolToStr( A174Tabela_Ativo), "", "", chkTabela_Ativo.Visible, chkTabela_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(46, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_1239e( true) ;
         }
         else
         {
            wb_table4_16_1239e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11122 */
         E11122 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A173Tabela_Nome = StringUtil.Upper( cgiGet( edtTabela_Nome_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A173Tabela_Nome", A173Tabela_Nome);
               A175Tabela_Descricao = cgiGet( edtTabela_Descricao_Internalname);
               n175Tabela_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A175Tabela_Descricao", A175Tabela_Descricao);
               n175Tabela_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A175Tabela_Descricao)) ? true : false);
               dynTabela_ModuloCod.CurrentValue = cgiGet( dynTabela_ModuloCod_Internalname);
               A188Tabela_ModuloCod = (int)(NumberUtil.Val( cgiGet( dynTabela_ModuloCod_Internalname), "."));
               n188Tabela_ModuloCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
               n188Tabela_ModuloCod = ((0==A188Tabela_ModuloCod) ? true : false);
               dynTabela_PaiCod.CurrentValue = cgiGet( dynTabela_PaiCod_Internalname);
               A181Tabela_PaiCod = (int)(NumberUtil.Val( cgiGet( dynTabela_PaiCod_Internalname), "."));
               n181Tabela_PaiCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
               n181Tabela_PaiCod = ((0==A181Tabela_PaiCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtTabela_MelhoraCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTabela_MelhoraCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TABELA_MELHORACOD");
                  AnyError = 1;
                  GX_FocusControl = edtTabela_MelhoraCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A746Tabela_MelhoraCod = 0;
                  n746Tabela_MelhoraCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A746Tabela_MelhoraCod), 6, 0)));
               }
               else
               {
                  A746Tabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( edtTabela_MelhoraCod_Internalname), ",", "."));
                  n746Tabela_MelhoraCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A746Tabela_MelhoraCod), 6, 0)));
               }
               n746Tabela_MelhoraCod = ((0==A746Tabela_MelhoraCod) ? true : false);
               A174Tabela_Ativo = StringUtil.StrToBool( cgiGet( chkTabela_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A174Tabela_Ativo", A174Tabela_Ativo);
               A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
               /* Read saved values. */
               Z172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z172Tabela_Codigo"), ",", "."));
               Z173Tabela_Nome = cgiGet( "Z173Tabela_Nome");
               Z174Tabela_Ativo = StringUtil.StrToBool( cgiGet( "Z174Tabela_Ativo"));
               Z188Tabela_ModuloCod = (int)(context.localUtil.CToN( cgiGet( "Z188Tabela_ModuloCod"), ",", "."));
               n188Tabela_ModuloCod = ((0==A188Tabela_ModuloCod) ? true : false);
               Z181Tabela_PaiCod = (int)(context.localUtil.CToN( cgiGet( "Z181Tabela_PaiCod"), ",", "."));
               n181Tabela_PaiCod = ((0==A181Tabela_PaiCod) ? true : false);
               Z746Tabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "Z746Tabela_MelhoraCod"), ",", "."));
               n746Tabela_MelhoraCod = ((0==A746Tabela_MelhoraCod) ? true : false);
               O173Tabela_Nome = cgiGet( "O173Tabela_Nome");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N190Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "N190Tabela_SistemaCod"), ",", "."));
               N188Tabela_ModuloCod = (int)(context.localUtil.CToN( cgiGet( "N188Tabela_ModuloCod"), ",", "."));
               n188Tabela_ModuloCod = ((0==A188Tabela_ModuloCod) ? true : false);
               N181Tabela_PaiCod = (int)(context.localUtil.CToN( cgiGet( "N181Tabela_PaiCod"), ",", "."));
               n181Tabela_PaiCod = ((0==A181Tabela_PaiCod) ? true : false);
               N746Tabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "N746Tabela_MelhoraCod"), ",", "."));
               n746Tabela_MelhoraCod = ((0==A746Tabela_MelhoraCod) ? true : false);
               AV7Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( "vTABELA_CODIGO"), ",", "."));
               AV11Insert_Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_TABELA_SISTEMACOD"), ",", "."));
               A190Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "TABELA_SISTEMACOD"), ",", "."));
               AV12Insert_Tabela_ModuloCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_TABELA_MODULOCOD"), ",", "."));
               AV13Insert_Tabela_PaiCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_TABELA_PAICOD"), ",", "."));
               AV18Insert_Tabela_MelhoraCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_TABELA_MELHORACOD"), ",", "."));
               AV17Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSISTEMA_CODIGO"), ",", "."));
               AV16Tabela_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "vTABELA_SISTEMACOD"), ",", "."));
               A191Tabela_SistemaDes = cgiGet( "TABELA_SISTEMADES");
               n191Tabela_SistemaDes = false;
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A189Tabela_ModuloDes = cgiGet( "TABELA_MODULODES");
               n189Tabela_ModuloDes = false;
               A182Tabela_PaiNom = cgiGet( "TABELA_PAINOM");
               n182Tabela_PaiNom = false;
               AV19Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Tabela";
               A172Tabela_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTabela_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV17Sistema_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV16Tabela_SistemaCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A172Tabela_Codigo != Z172Tabela_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("tabela:[SecurityCheckFailed value for]"+"Tabela_Codigo:"+context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("tabela:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("tabela:[SecurityCheckFailed value for]"+"Sistema_Codigo:"+context.localUtil.Format( (decimal)(AV17Sistema_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("tabela:[SecurityCheckFailed value for]"+"Tabela_SistemaCod:"+context.localUtil.Format( (decimal)(AV16Tabela_SistemaCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A172Tabela_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode39 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode39;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound39 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_120( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "TABELA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtTabela_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11122 */
                           E11122 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12122 */
                           E12122 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOELIMINAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13122 */
                           E13122 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14122 */
                           E14122 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCOPIARCOLAR'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15122 */
                           E15122 ();
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12122 */
            E12122 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1239( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1239( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_120( )
      {
         BeforeValidate1239( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1239( ) ;
            }
            else
            {
               CheckExtendedTable1239( ) ;
               CloseExtendedTableCursors1239( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption120( )
      {
      }

      protected void E11122( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV19Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV20GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20GXV1), 8, 0)));
            while ( AV20GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV14TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV20GXV1));
               if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Tabela_SistemaCod") == 0 )
               {
                  AV11Insert_Tabela_SistemaCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_Tabela_SistemaCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Tabela_ModuloCod") == 0 )
               {
                  AV12Insert_Tabela_ModuloCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_Tabela_ModuloCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Tabela_PaiCod") == 0 )
               {
                  AV13Insert_Tabela_PaiCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_Tabela_PaiCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "Tabela_MelhoraCod") == 0 )
               {
                  AV18Insert_Tabela_MelhoraCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Insert_Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Insert_Tabela_MelhoraCod), 6, 0)));
               }
               AV20GXV1 = (int)(AV20GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20GXV1), 8, 0)));
            }
         }
         edtTabela_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_Codigo_Visible), 5, 0)));
      }

      protected void E12122( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            context.wjLoc = formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV16Tabela_SistemaCod) + "," + UrlEncode("" +AV15Tabela_ModuloCod);
            context.wjLocDisableFrm = 1;
         }
         else
         {
            context.wjLoc = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV17Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A190Tabela_SistemaCod,(int)AV15Tabela_ModuloCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwtabela.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {(int)A190Tabela_SistemaCod,(int)AV15Tabela_ModuloCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E13122( )
      {
         /* 'DoEliminar' Routine */
         new prc_desativarregistro(context ).execute(  "Tbl",  AV7Tabela_Codigo,  0) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABELA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Tabela_Codigo), "ZZZZZ9")));
         context.setWebReturnParms(new Object[] {(int)A190Tabela_SistemaCod,(int)AV15Tabela_ModuloCod});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E14122( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
      }

      protected void E15122( )
      {
         /* 'DoCopiarColar' Routine */
         context.wjLoc = formatLink("wp_copiarcolar.aspx") + "?" + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode(StringUtil.RTrim("Tab"));
         context.wjLocDisableFrm = 1;
      }

      protected void ZM1239( short GX_JID )
      {
         if ( ( GX_JID == 30 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z173Tabela_Nome = T00123_A173Tabela_Nome[0];
               Z174Tabela_Ativo = T00123_A174Tabela_Ativo[0];
               Z188Tabela_ModuloCod = T00123_A188Tabela_ModuloCod[0];
               Z181Tabela_PaiCod = T00123_A181Tabela_PaiCod[0];
               Z746Tabela_MelhoraCod = T00123_A746Tabela_MelhoraCod[0];
            }
            else
            {
               Z173Tabela_Nome = A173Tabela_Nome;
               Z174Tabela_Ativo = A174Tabela_Ativo;
               Z188Tabela_ModuloCod = A188Tabela_ModuloCod;
               Z181Tabela_PaiCod = A181Tabela_PaiCod;
               Z746Tabela_MelhoraCod = A746Tabela_MelhoraCod;
            }
         }
         if ( GX_JID == -30 )
         {
            Z190Tabela_SistemaCod = A190Tabela_SistemaCod;
            Z172Tabela_Codigo = A172Tabela_Codigo;
            Z173Tabela_Nome = A173Tabela_Nome;
            Z175Tabela_Descricao = A175Tabela_Descricao;
            Z174Tabela_Ativo = A174Tabela_Ativo;
            Z188Tabela_ModuloCod = A188Tabela_ModuloCod;
            Z181Tabela_PaiCod = A181Tabela_PaiCod;
            Z746Tabela_MelhoraCod = A746Tabela_MelhoraCod;
            Z191Tabela_SistemaDes = A191Tabela_SistemaDes;
            Z189Tabela_ModuloDes = A189Tabela_ModuloDes;
            Z182Tabela_PaiNom = A182Tabela_PaiNom;
         }
      }

      protected void standaloneNotModal( )
      {
         edtTabela_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV19Pgmname = "Tabela";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Pgmname", AV19Pgmname);
         edtTabela_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_Codigo_Enabled), 5, 0)));
         if ( ! (0==AV7Tabela_Codigo) )
         {
            A172Tabela_Codigo = AV7Tabela_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
         }
         /* Using cursor T00125 */
         pr_default.execute(3, new Object[] {A190Tabela_SistemaCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'ST_Tabela_Sistema'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A191Tabela_SistemaDes = T00125_A191Tabela_SistemaDes[0];
         n191Tabela_SistemaDes = T00125_n191Tabela_SistemaDes[0];
         pr_default.close(3);
         Dvpanel_tableattributes_Title = "Tabela do Sistema: "+A191Tabela_SistemaDes;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Dvpanel_tableattributes_Internalname, "Title", Dvpanel_tableattributes_Title);
         GXATABELA_MODULOCOD_html1239( A190Tabela_SistemaCod) ;
         AV17Sistema_Codigo = A190Tabela_SistemaCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Sistema_Codigo), 6, 0)));
         AV16Tabela_SistemaCod = A190Tabela_SistemaCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Tabela_SistemaCod), 6, 0)));
         GXATABELA_PAICOD_html1239( AV16Tabela_SistemaCod) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Tabela_ModuloCod) )
         {
            dynTabela_ModuloCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTabela_ModuloCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTabela_ModuloCod.Enabled), 5, 0)));
         }
         else
         {
            dynTabela_ModuloCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTabela_ModuloCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTabela_ModuloCod.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Tabela_PaiCod) )
         {
            dynTabela_PaiCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTabela_PaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTabela_PaiCod.Enabled), 5, 0)));
         }
         else
         {
            dynTabela_PaiCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTabela_PaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTabela_PaiCod.Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV18Insert_Tabela_MelhoraCod) )
         {
            edtTabela_MelhoraCod_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_MelhoraCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_MelhoraCod_Enabled), 5, 0)));
         }
         else
         {
            edtTabela_MelhoraCod_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_MelhoraCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_MelhoraCod_Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         bttBtncopiarcolar_Visible = (( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtncopiarcolar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtncopiarcolar_Visible), 5, 0)));
         chkTabela_Ativo.Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkTabela_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkTabela_Ativo.Visible), 5, 0)));
         lblTextblocktabela_ativo_Visible = (!( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblocktabela_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblocktabela_ativo_Visible), 5, 0)));
         bttBtn_trn_enter_Visible = (!( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV18Insert_Tabela_MelhoraCod) )
         {
            A746Tabela_MelhoraCod = AV18Insert_Tabela_MelhoraCod;
            n746Tabela_MelhoraCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A746Tabela_MelhoraCod), 6, 0)));
         }
         GXt_boolean1 = false;
         new prc_setnaopodedeletar(context ).execute(  "Tbl",  AV7Tabela_Codigo, out  GXt_boolean1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Tabela_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABELA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Tabela_Codigo), "ZZZZZ9")));
         bttBtneliminar_Visible = (( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) &&GXt_boolean1 ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtneliminar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtneliminar_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A174Tabela_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A174Tabela_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A174Tabela_Ativo", A174Tabela_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Tabela_SistemaCod) )
            {
               A190Tabela_SistemaCod = AV11Insert_Tabela_SistemaCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
            }
            AV10WebSession.Set("Sistema_Codigo", StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0));
         }
      }

      protected void Load1239( )
      {
         /* Using cursor T00128 */
         pr_default.execute(6, new Object[] {A172Tabela_Codigo, A190Tabela_SistemaCod});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound39 = 1;
            A173Tabela_Nome = T00128_A173Tabela_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A173Tabela_Nome", A173Tabela_Nome);
            A175Tabela_Descricao = T00128_A175Tabela_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A175Tabela_Descricao", A175Tabela_Descricao);
            n175Tabela_Descricao = T00128_n175Tabela_Descricao[0];
            A191Tabela_SistemaDes = T00128_A191Tabela_SistemaDes[0];
            n191Tabela_SistemaDes = T00128_n191Tabela_SistemaDes[0];
            A189Tabela_ModuloDes = T00128_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = T00128_n189Tabela_ModuloDes[0];
            A182Tabela_PaiNom = T00128_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = T00128_n182Tabela_PaiNom[0];
            A174Tabela_Ativo = T00128_A174Tabela_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A174Tabela_Ativo", A174Tabela_Ativo);
            A188Tabela_ModuloCod = T00128_A188Tabela_ModuloCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
            n188Tabela_ModuloCod = T00128_n188Tabela_ModuloCod[0];
            A181Tabela_PaiCod = T00128_A181Tabela_PaiCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
            n181Tabela_PaiCod = T00128_n181Tabela_PaiCod[0];
            A746Tabela_MelhoraCod = T00128_A746Tabela_MelhoraCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A746Tabela_MelhoraCod), 6, 0)));
            n746Tabela_MelhoraCod = T00128_n746Tabela_MelhoraCod[0];
            ZM1239( -30) ;
         }
         pr_default.close(6);
         OnLoadActions1239( ) ;
      }

      protected void OnLoadActions1239( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Tabela_SistemaCod) )
         {
            A190Tabela_SistemaCod = AV11Insert_Tabela_SistemaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
         }
         AV10WebSession.Set("Sistema_Codigo", StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Tabela_ModuloCod) )
         {
            A188Tabela_ModuloCod = AV12Insert_Tabela_ModuloCod;
            n188Tabela_ModuloCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A188Tabela_ModuloCod) )
            {
               A188Tabela_ModuloCod = 0;
               n188Tabela_ModuloCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
               n188Tabela_ModuloCod = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Tabela_PaiCod) )
         {
            A181Tabela_PaiCod = AV13Insert_Tabela_PaiCod;
            n181Tabela_PaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A181Tabela_PaiCod) )
            {
               A181Tabela_PaiCod = 0;
               n181Tabela_PaiCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
               n181Tabela_PaiCod = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
            }
         }
      }

      protected void CheckExtendedTable1239( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_Tabela_SistemaCod) )
         {
            A190Tabela_SistemaCod = AV11Insert_Tabela_SistemaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0)));
         }
         AV10WebSession.Set("Sistema_Codigo", StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Tabela_ModuloCod) )
         {
            A188Tabela_ModuloCod = AV12Insert_Tabela_ModuloCod;
            n188Tabela_ModuloCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A188Tabela_ModuloCod) )
            {
               A188Tabela_ModuloCod = 0;
               n188Tabela_ModuloCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
               n188Tabela_ModuloCod = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Tabela_PaiCod) )
         {
            A181Tabela_PaiCod = AV13Insert_Tabela_PaiCod;
            n181Tabela_PaiCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A181Tabela_PaiCod) )
            {
               A181Tabela_PaiCod = 0;
               n181Tabela_PaiCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
               n181Tabela_PaiCod = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A173Tabela_Nome)) )
         {
            GX_msglist.addItem("Nome � obrigat�rio.", 1, "TABELA_NOME");
            AnyError = 1;
            GX_FocusControl = edtTabela_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00124 */
         pr_default.execute(2, new Object[] {n188Tabela_ModuloCod, A188Tabela_ModuloCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A188Tabela_ModuloCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Tabela_Modulo'.", "ForeignKeyNotFound", 1, "TABELA_MODULOCOD");
               AnyError = 1;
               GX_FocusControl = dynTabela_ModuloCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A189Tabela_ModuloDes = T00124_A189Tabela_ModuloDes[0];
         n189Tabela_ModuloDes = T00124_n189Tabela_ModuloDes[0];
         pr_default.close(2);
         /* Using cursor T00126 */
         pr_default.execute(4, new Object[] {n181Tabela_PaiCod, A181Tabela_PaiCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A181Tabela_PaiCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Tabela_Pai'.", "ForeignKeyNotFound", 1, "TABELA_PAICOD");
               AnyError = 1;
               GX_FocusControl = dynTabela_PaiCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A182Tabela_PaiNom = T00126_A182Tabela_PaiNom[0];
         n182Tabela_PaiNom = T00126_n182Tabela_PaiNom[0];
         pr_default.close(4);
         /* Using cursor T00127 */
         pr_default.execute(5, new Object[] {n746Tabela_MelhoraCod, A746Tabela_MelhoraCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A746Tabela_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Tabela_Tabela Melhora'.", "ForeignKeyNotFound", 1, "TABELA_MELHORACOD");
               AnyError = 1;
               GX_FocusControl = edtTabela_MelhoraCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(5);
      }

      protected void CloseExtendedTableCursors1239( )
      {
         pr_default.close(2);
         pr_default.close(4);
         pr_default.close(5);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_31( int A188Tabela_ModuloCod )
      {
         /* Using cursor T00129 */
         pr_default.execute(7, new Object[] {n188Tabela_ModuloCod, A188Tabela_ModuloCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A188Tabela_ModuloCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Tabela_Modulo'.", "ForeignKeyNotFound", 1, "TABELA_MODULOCOD");
               AnyError = 1;
               GX_FocusControl = dynTabela_ModuloCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A189Tabela_ModuloDes = T00129_A189Tabela_ModuloDes[0];
         n189Tabela_ModuloDes = T00129_n189Tabela_ModuloDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A189Tabela_ModuloDes))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(7) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(7);
      }

      protected void gxLoad_33( int A181Tabela_PaiCod )
      {
         /* Using cursor T001210 */
         pr_default.execute(8, new Object[] {n181Tabela_PaiCod, A181Tabela_PaiCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A181Tabela_PaiCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Tabela_Pai'.", "ForeignKeyNotFound", 1, "TABELA_PAICOD");
               AnyError = 1;
               GX_FocusControl = dynTabela_PaiCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A182Tabela_PaiNom = T001210_A182Tabela_PaiNom[0];
         n182Tabela_PaiNom = T001210_n182Tabela_PaiNom[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A182Tabela_PaiNom))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_34( int A746Tabela_MelhoraCod )
      {
         /* Using cursor T001211 */
         pr_default.execute(9, new Object[] {n746Tabela_MelhoraCod, A746Tabela_MelhoraCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A746Tabela_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Tabela_Tabela Melhora'.", "ForeignKeyNotFound", 1, "TABELA_MELHORACOD");
               AnyError = 1;
               GX_FocusControl = edtTabela_MelhoraCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void GetKey1239( )
      {
         /* Using cursor T001212 */
         pr_default.execute(10, new Object[] {A172Tabela_Codigo});
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound39 = 1;
         }
         else
         {
            RcdFound39 = 0;
         }
         pr_default.close(10);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00123 */
         pr_default.execute(1, new Object[] {A172Tabela_Codigo});
         if ( (pr_default.getStatus(1) != 101) && ( T00123_A190Tabela_SistemaCod[0] == A190Tabela_SistemaCod ) )
         {
            ZM1239( 30) ;
            RcdFound39 = 1;
            A172Tabela_Codigo = T00123_A172Tabela_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
            A173Tabela_Nome = T00123_A173Tabela_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A173Tabela_Nome", A173Tabela_Nome);
            A175Tabela_Descricao = T00123_A175Tabela_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A175Tabela_Descricao", A175Tabela_Descricao);
            n175Tabela_Descricao = T00123_n175Tabela_Descricao[0];
            A174Tabela_Ativo = T00123_A174Tabela_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A174Tabela_Ativo", A174Tabela_Ativo);
            A188Tabela_ModuloCod = T00123_A188Tabela_ModuloCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
            n188Tabela_ModuloCod = T00123_n188Tabela_ModuloCod[0];
            A181Tabela_PaiCod = T00123_A181Tabela_PaiCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
            n181Tabela_PaiCod = T00123_n181Tabela_PaiCod[0];
            A746Tabela_MelhoraCod = T00123_A746Tabela_MelhoraCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A746Tabela_MelhoraCod), 6, 0)));
            n746Tabela_MelhoraCod = T00123_n746Tabela_MelhoraCod[0];
            O173Tabela_Nome = A173Tabela_Nome;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A173Tabela_Nome", A173Tabela_Nome);
            Z172Tabela_Codigo = A172Tabela_Codigo;
            sMode39 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1239( ) ;
            if ( AnyError == 1 )
            {
               RcdFound39 = 0;
               InitializeNonKey1239( ) ;
            }
            Gx_mode = sMode39;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound39 = 0;
            InitializeNonKey1239( ) ;
            sMode39 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode39;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1239( ) ;
         if ( RcdFound39 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound39 = 0;
         /* Using cursor T001213 */
         pr_default.execute(11, new Object[] {A172Tabela_Codigo, A190Tabela_SistemaCod});
         if ( (pr_default.getStatus(11) != 101) )
         {
            while ( (pr_default.getStatus(11) != 101) && ( ( T001213_A172Tabela_Codigo[0] < A172Tabela_Codigo ) ) && ( T001213_A190Tabela_SistemaCod[0] == A190Tabela_SistemaCod ) )
            {
               pr_default.readNext(11);
            }
            if ( (pr_default.getStatus(11) != 101) && ( ( T001213_A172Tabela_Codigo[0] > A172Tabela_Codigo ) ) && ( T001213_A190Tabela_SistemaCod[0] == A190Tabela_SistemaCod ) )
            {
               A172Tabela_Codigo = T001213_A172Tabela_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
               RcdFound39 = 1;
            }
         }
         pr_default.close(11);
      }

      protected void move_previous( )
      {
         RcdFound39 = 0;
         /* Using cursor T001214 */
         pr_default.execute(12, new Object[] {A172Tabela_Codigo, A190Tabela_SistemaCod});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T001214_A172Tabela_Codigo[0] > A172Tabela_Codigo ) ) && ( T001214_A190Tabela_SistemaCod[0] == A190Tabela_SistemaCod ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T001214_A172Tabela_Codigo[0] < A172Tabela_Codigo ) ) && ( T001214_A190Tabela_SistemaCod[0] == A190Tabela_SistemaCod ) )
            {
               A172Tabela_Codigo = T001214_A172Tabela_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
               RcdFound39 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1239( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtTabela_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1239( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound39 == 1 )
            {
               if ( A172Tabela_Codigo != Z172Tabela_Codigo )
               {
                  A172Tabela_Codigo = Z172Tabela_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "TABELA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtTabela_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtTabela_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1239( ) ;
                  GX_FocusControl = edtTabela_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A172Tabela_Codigo != Z172Tabela_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtTabela_Nome_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1239( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "TABELA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtTabela_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtTabela_Nome_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1239( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A172Tabela_Codigo != Z172Tabela_Codigo )
         {
            A172Tabela_Codigo = Z172Tabela_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "TABELA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtTabela_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtTabela_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1239( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00122 */
            pr_default.execute(0, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tabela"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z173Tabela_Nome, T00122_A173Tabela_Nome[0]) != 0 ) || ( Z174Tabela_Ativo != T00122_A174Tabela_Ativo[0] ) || ( Z188Tabela_ModuloCod != T00122_A188Tabela_ModuloCod[0] ) || ( Z181Tabela_PaiCod != T00122_A181Tabela_PaiCod[0] ) || ( Z746Tabela_MelhoraCod != T00122_A746Tabela_MelhoraCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z173Tabela_Nome, T00122_A173Tabela_Nome[0]) != 0 )
               {
                  GXUtil.WriteLog("tabela:[seudo value changed for attri]"+"Tabela_Nome");
                  GXUtil.WriteLogRaw("Old: ",Z173Tabela_Nome);
                  GXUtil.WriteLogRaw("Current: ",T00122_A173Tabela_Nome[0]);
               }
               if ( Z174Tabela_Ativo != T00122_A174Tabela_Ativo[0] )
               {
                  GXUtil.WriteLog("tabela:[seudo value changed for attri]"+"Tabela_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z174Tabela_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T00122_A174Tabela_Ativo[0]);
               }
               if ( Z188Tabela_ModuloCod != T00122_A188Tabela_ModuloCod[0] )
               {
                  GXUtil.WriteLog("tabela:[seudo value changed for attri]"+"Tabela_ModuloCod");
                  GXUtil.WriteLogRaw("Old: ",Z188Tabela_ModuloCod);
                  GXUtil.WriteLogRaw("Current: ",T00122_A188Tabela_ModuloCod[0]);
               }
               if ( Z181Tabela_PaiCod != T00122_A181Tabela_PaiCod[0] )
               {
                  GXUtil.WriteLog("tabela:[seudo value changed for attri]"+"Tabela_PaiCod");
                  GXUtil.WriteLogRaw("Old: ",Z181Tabela_PaiCod);
                  GXUtil.WriteLogRaw("Current: ",T00122_A181Tabela_PaiCod[0]);
               }
               if ( Z746Tabela_MelhoraCod != T00122_A746Tabela_MelhoraCod[0] )
               {
                  GXUtil.WriteLog("tabela:[seudo value changed for attri]"+"Tabela_MelhoraCod");
                  GXUtil.WriteLogRaw("Old: ",Z746Tabela_MelhoraCod);
                  GXUtil.WriteLogRaw("Current: ",T00122_A746Tabela_MelhoraCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Tabela"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1239( )
      {
         BeforeValidate1239( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1239( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1239( 0) ;
            CheckOptimisticConcurrency1239( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1239( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1239( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001215 */
                     pr_default.execute(13, new Object[] {A190Tabela_SistemaCod, A173Tabela_Nome, n175Tabela_Descricao, A175Tabela_Descricao, A174Tabela_Ativo, n188Tabela_ModuloCod, A188Tabela_ModuloCod, n181Tabela_PaiCod, A181Tabela_PaiCod, n746Tabela_MelhoraCod, A746Tabela_MelhoraCod});
                     A172Tabela_Codigo = T001215_A172Tabela_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
                     pr_default.close(13);
                     dsDefault.SmartCacheProvider.SetUpdated("Tabela") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption120( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1239( ) ;
            }
            EndLevel1239( ) ;
         }
         CloseExtendedTableCursors1239( ) ;
      }

      protected void Update1239( )
      {
         BeforeValidate1239( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1239( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1239( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1239( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1239( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001216 */
                     pr_default.execute(14, new Object[] {A190Tabela_SistemaCod, A173Tabela_Nome, n175Tabela_Descricao, A175Tabela_Descricao, A174Tabela_Ativo, n188Tabela_ModuloCod, A188Tabela_ModuloCod, n181Tabela_PaiCod, A181Tabela_PaiCod, n746Tabela_MelhoraCod, A746Tabela_MelhoraCod, A172Tabela_Codigo});
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("Tabela") ;
                     if ( (pr_default.getStatus(14) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Tabela"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1239( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1239( ) ;
         }
         CloseExtendedTableCursors1239( ) ;
      }

      protected void DeferredUpdate1239( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1239( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1239( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1239( ) ;
            AfterConfirm1239( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1239( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001217 */
                  pr_default.execute(15, new Object[] {A172Tabela_Codigo});
                  pr_default.close(15);
                  dsDefault.SmartCacheProvider.SetUpdated("Tabela") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode39 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1239( ) ;
         Gx_mode = sMode39;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1239( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV10WebSession.Set("Sistema_Codigo", StringUtil.Str( (decimal)(A190Tabela_SistemaCod), 6, 0));
            /* Using cursor T001218 */
            pr_default.execute(16, new Object[] {n188Tabela_ModuloCod, A188Tabela_ModuloCod});
            A189Tabela_ModuloDes = T001218_A189Tabela_ModuloDes[0];
            n189Tabela_ModuloDes = T001218_n189Tabela_ModuloDes[0];
            pr_default.close(16);
            /* Using cursor T001219 */
            pr_default.execute(17, new Object[] {n181Tabela_PaiCod, A181Tabela_PaiCod});
            A182Tabela_PaiNom = T001219_A182Tabela_PaiNom[0];
            n182Tabela_PaiNom = T001219_n182Tabela_PaiNom[0];
            pr_default.close(17);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T001220 */
            pr_default.execute(18, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(18) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(18);
            /* Using cursor T001221 */
            pr_default.execute(19, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(19) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(19);
            /* Using cursor T001222 */
            pr_default.execute(20, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(20) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Atributos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(20);
            /* Using cursor T001223 */
            pr_default.execute(21, new Object[] {A172Tabela_Codigo});
            if ( (pr_default.getStatus(21) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Funcao Dados Tabela"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(21);
         }
      }

      protected void EndLevel1239( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1239( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(16);
            pr_default.close(17);
            context.CommitDataStores( "Tabela");
            if ( AnyError == 0 )
            {
               ConfirmValues120( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(16);
            pr_default.close(17);
            context.RollbackDataStores( "Tabela");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1239( )
      {
         /* Scan By routine */
         /* Using cursor T001224 */
         pr_default.execute(22, new Object[] {A190Tabela_SistemaCod});
         RcdFound39 = 0;
         if ( (pr_default.getStatus(22) != 101) )
         {
            RcdFound39 = 1;
            A172Tabela_Codigo = T001224_A172Tabela_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1239( )
      {
         /* Scan next routine */
         pr_default.readNext(22);
         RcdFound39 = 0;
         if ( (pr_default.getStatus(22) != 101) )
         {
            RcdFound39 = 1;
            A172Tabela_Codigo = T001224_A172Tabela_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd1239( )
      {
         pr_default.close(22);
      }

      protected void AfterConfirm1239( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1239( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1239( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1239( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1239( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1239( )
      {
         /* Before Validate Rules */
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ) && ( StringUtil.StrCmp(A173Tabela_Nome, O173Tabela_Nome) != 0 ) && new prc_existenomecadastrado(context).executeUdp(  "Tab",  A173Tabela_Nome) )
         {
            GX_msglist.addItem("Nome de tabela j� cadastrado!", 1, "TABELA_NOME");
            AnyError = 1;
            GX_FocusControl = edtTabela_Nome_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void DisableAttributes1239( )
      {
         edtTabela_Nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_Nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_Nome_Enabled), 5, 0)));
         edtTabela_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_Descricao_Enabled), 5, 0)));
         dynTabela_ModuloCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTabela_ModuloCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTabela_ModuloCod.Enabled), 5, 0)));
         dynTabela_PaiCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynTabela_PaiCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynTabela_PaiCod.Enabled), 5, 0)));
         edtTabela_MelhoraCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_MelhoraCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_MelhoraCod_Enabled), 5, 0)));
         chkTabela_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkTabela_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkTabela_Ativo.Enabled), 5, 0)));
         edtTabela_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTabela_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTabela_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues120( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117184362");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Tabela_Codigo) + "," + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode("" +AV15Tabela_ModuloCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z172Tabela_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z172Tabela_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z173Tabela_Nome", StringUtil.RTrim( Z173Tabela_Nome));
         GxWebStd.gx_boolean_hidden_field( context, "Z174Tabela_Ativo", Z174Tabela_Ativo);
         GxWebStd.gx_hidden_field( context, "Z188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z188Tabela_ModuloCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z181Tabela_PaiCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z181Tabela_PaiCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z746Tabela_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O173Tabela_Nome", StringUtil.RTrim( O173Tabela_Nome));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N190Tabela_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A188Tabela_ModuloCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N181Tabela_PaiCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A181Tabela_PaiCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "N746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A746Tabela_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTABELA_MODULOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15Tabela_ModuloCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vTABELA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Tabela_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_TABELA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_Tabela_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "TABELA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A190Tabela_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_TABELA_MODULOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Insert_Tabela_ModuloCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_TABELA_PAICOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_Tabela_PaiCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_TABELA_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Insert_Tabela_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTABELA_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Tabela_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "TABELA_SISTEMADES", A191Tabela_SistemaDes);
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "TABELA_MODULODES", StringUtil.RTrim( A189Tabela_ModuloDes));
         GxWebStd.gx_hidden_field( context, "TABELA_PAINOM", StringUtil.RTrim( A182Tabela_PaiNom));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV19Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABELA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Tabela_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Tabela";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV17Sistema_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV16Tabela_SistemaCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("tabela:[SendSecurityCheck value for]"+"Tabela_Codigo:"+context.localUtil.Format( (decimal)(A172Tabela_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("tabela:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("tabela:[SendSecurityCheck value for]"+"Sistema_Codigo:"+context.localUtil.Format( (decimal)(AV17Sistema_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("tabela:[SendSecurityCheck value for]"+"Tabela_SistemaCod:"+context.localUtil.Format( (decimal)(AV16Tabela_SistemaCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("tabela.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Tabela_Codigo) + "," + UrlEncode("" +A190Tabela_SistemaCod) + "," + UrlEncode("" +AV15Tabela_ModuloCod) ;
      }

      public override String GetPgmname( )
      {
         return "Tabela" ;
      }

      public override String GetPgmdesc( )
      {
         return "Tabela" ;
      }

      protected void InitializeNonKey1239( )
      {
         A188Tabela_ModuloCod = 0;
         n188Tabela_ModuloCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A188Tabela_ModuloCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0)));
         n188Tabela_ModuloCod = ((0==A188Tabela_ModuloCod) ? true : false);
         A181Tabela_PaiCod = 0;
         n181Tabela_PaiCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A181Tabela_PaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0)));
         n181Tabela_PaiCod = ((0==A181Tabela_PaiCod) ? true : false);
         A746Tabela_MelhoraCod = 0;
         n746Tabela_MelhoraCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A746Tabela_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A746Tabela_MelhoraCod), 6, 0)));
         n746Tabela_MelhoraCod = ((0==A746Tabela_MelhoraCod) ? true : false);
         A173Tabela_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A173Tabela_Nome", A173Tabela_Nome);
         A175Tabela_Descricao = "";
         n175Tabela_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A175Tabela_Descricao", A175Tabela_Descricao);
         n175Tabela_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A175Tabela_Descricao)) ? true : false);
         A189Tabela_ModuloDes = "";
         n189Tabela_ModuloDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A189Tabela_ModuloDes", A189Tabela_ModuloDes);
         A182Tabela_PaiNom = "";
         n182Tabela_PaiNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A182Tabela_PaiNom", A182Tabela_PaiNom);
         A174Tabela_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A174Tabela_Ativo", A174Tabela_Ativo);
         O173Tabela_Nome = A173Tabela_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A173Tabela_Nome", A173Tabela_Nome);
         Z173Tabela_Nome = "";
         Z174Tabela_Ativo = false;
         Z188Tabela_ModuloCod = 0;
         Z181Tabela_PaiCod = 0;
         Z746Tabela_MelhoraCod = 0;
      }

      protected void InitAll1239( )
      {
         A172Tabela_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A172Tabela_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A172Tabela_Codigo), 6, 0)));
         InitializeNonKey1239( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A174Tabela_Ativo = i174Tabela_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A174Tabela_Ativo", A174Tabela_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117184398");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("tabela.js", "?20203117184398");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTabelatitle_Internalname = "TABELATITLE";
         lblTextblocktabela_nome_Internalname = "TEXTBLOCKTABELA_NOME";
         edtTabela_Nome_Internalname = "TABELA_NOME";
         lblTextblocktabela_descricao_Internalname = "TEXTBLOCKTABELA_DESCRICAO";
         edtTabela_Descricao_Internalname = "TABELA_DESCRICAO";
         lblTextblocktabela_modulocod_Internalname = "TEXTBLOCKTABELA_MODULOCOD";
         dynTabela_ModuloCod_Internalname = "TABELA_MODULOCOD";
         lblTextblocktabela_paicod_Internalname = "TEXTBLOCKTABELA_PAICOD";
         dynTabela_PaiCod_Internalname = "TABELA_PAICOD";
         lblTextblocktabela_melhoracod_Internalname = "TEXTBLOCKTABELA_MELHORACOD";
         edtTabela_MelhoraCod_Internalname = "TABELA_MELHORACOD";
         lblTextblocktabela_ativo_Internalname = "TEXTBLOCKTABELA_ATIVO";
         chkTabela_Ativo_Internalname = "TABELA_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtneliminar_Internalname = "BTNELIMINAR";
         bttBtnfechar_Internalname = "BTNFECHAR";
         bttBtncopiarcolar_Internalname = "BTNCOPIARCOLAR";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtTabela_Codigo_Internalname = "TABELA_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Tabela";
         Dvpanel_tableattributes_Title = "Informa��es Gerais";
         chkTabela_Ativo.Enabled = 1;
         chkTabela_Ativo.Visible = 1;
         lblTextblocktabela_ativo_Visible = 1;
         edtTabela_MelhoraCod_Jsonclick = "";
         edtTabela_MelhoraCod_Enabled = 1;
         dynTabela_PaiCod_Jsonclick = "";
         dynTabela_PaiCod.Enabled = 1;
         dynTabela_ModuloCod_Jsonclick = "";
         dynTabela_ModuloCod.Enabled = 1;
         edtTabela_Descricao_Enabled = 1;
         edtTabela_Nome_Jsonclick = "";
         edtTabela_Nome_Enabled = 1;
         bttBtncopiarcolar_Visible = 1;
         bttBtnfechar_Visible = 1;
         bttBtneliminar_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtTabela_Codigo_Jsonclick = "";
         edtTabela_Codigo_Enabled = 0;
         edtTabela_Codigo_Visible = 1;
         chkTabela_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLATABELA_MODULOCOD1239( int A190Tabela_SistemaCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATABELA_MODULOCOD_data1239( A190Tabela_SistemaCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATABELA_MODULOCOD_html1239( int A190Tabela_SistemaCod )
      {
         int gxdynajaxvalue ;
         GXDLATABELA_MODULOCOD_data1239( A190Tabela_SistemaCod) ;
         gxdynajaxindex = 1;
         dynTabela_ModuloCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTabela_ModuloCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATABELA_MODULOCOD_data1239( int A190Tabela_SistemaCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T001225 */
         pr_default.execute(23, new Object[] {A190Tabela_SistemaCod});
         while ( (pr_default.getStatus(23) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001225_A188Tabela_ModuloCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T001225_A189Tabela_ModuloDes[0]));
            pr_default.readNext(23);
         }
         pr_default.close(23);
      }

      protected void GXDLATABELA_PAICOD1239( int AV16Tabela_SistemaCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLATABELA_PAICOD_data1239( AV16Tabela_SistemaCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXATABELA_PAICOD_html1239( int AV16Tabela_SistemaCod )
      {
         int gxdynajaxvalue ;
         GXDLATABELA_PAICOD_data1239( AV16Tabela_SistemaCod) ;
         gxdynajaxindex = 1;
         dynTabela_PaiCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynTabela_PaiCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLATABELA_PAICOD_data1239( int AV16Tabela_SistemaCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T001226 */
         pr_default.execute(24, new Object[] {AV16Tabela_SistemaCod});
         while ( (pr_default.getStatus(24) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T001226_A172Tabela_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T001226_A173Tabela_Nome[0]));
            pr_default.readNext(24);
         }
         pr_default.close(24);
      }

      public void Valid_Tabela_modulocod( String GX_Parm1 ,
                                          int GX_Parm2 ,
                                          GXCombobox dynGX_Parm3 ,
                                          String GX_Parm4 )
      {
         Gx_mode = GX_Parm1;
         AV12Insert_Tabela_ModuloCod = GX_Parm2;
         dynTabela_ModuloCod = dynGX_Parm3;
         A188Tabela_ModuloCod = (int)(NumberUtil.Val( dynTabela_ModuloCod.CurrentValue, "."));
         n188Tabela_ModuloCod = false;
         A189Tabela_ModuloDes = GX_Parm4;
         n189Tabela_ModuloDes = false;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV12Insert_Tabela_ModuloCod) )
         {
            A188Tabela_ModuloCod = AV12Insert_Tabela_ModuloCod;
            n188Tabela_ModuloCod = false;
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A188Tabela_ModuloCod) )
            {
               A188Tabela_ModuloCod = 0;
               n188Tabela_ModuloCod = false;
               n188Tabela_ModuloCod = true;
            }
         }
         /* Using cursor T001227 */
         pr_default.execute(25, new Object[] {n188Tabela_ModuloCod, A188Tabela_ModuloCod});
         if ( (pr_default.getStatus(25) == 101) )
         {
            if ( ! ( (0==A188Tabela_ModuloCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Tabela_Modulo'.", "ForeignKeyNotFound", 1, "TABELA_MODULOCOD");
               AnyError = 1;
               GX_FocusControl = dynTabela_ModuloCod_Internalname;
            }
         }
         A189Tabela_ModuloDes = T001227_A189Tabela_ModuloDes[0];
         n189Tabela_ModuloDes = T001227_n189Tabela_ModuloDes[0];
         pr_default.close(25);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A189Tabela_ModuloDes = "";
            n189Tabela_ModuloDes = false;
         }
         dynTabela_ModuloCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0));
         if ( dynTabela_ModuloCod.ItemCount > 0 )
         {
            A188Tabela_ModuloCod = (int)(NumberUtil.Val( dynTabela_ModuloCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0))), "."));
            n188Tabela_ModuloCod = false;
         }
         dynTabela_ModuloCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A188Tabela_ModuloCod), 6, 0));
         isValidOutput.Add(dynTabela_ModuloCod);
         isValidOutput.Add(StringUtil.RTrim( A189Tabela_ModuloDes));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Tabela_paicod( String GX_Parm1 ,
                                       int GX_Parm2 ,
                                       GXCombobox dynGX_Parm3 ,
                                       String GX_Parm4 )
      {
         Gx_mode = GX_Parm1;
         AV13Insert_Tabela_PaiCod = GX_Parm2;
         dynTabela_PaiCod = dynGX_Parm3;
         A181Tabela_PaiCod = (int)(NumberUtil.Val( dynTabela_PaiCod.CurrentValue, "."));
         n181Tabela_PaiCod = false;
         A182Tabela_PaiNom = GX_Parm4;
         n182Tabela_PaiNom = false;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_Tabela_PaiCod) )
         {
            A181Tabela_PaiCod = AV13Insert_Tabela_PaiCod;
            n181Tabela_PaiCod = false;
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A181Tabela_PaiCod) )
            {
               A181Tabela_PaiCod = 0;
               n181Tabela_PaiCod = false;
               n181Tabela_PaiCod = true;
            }
         }
         /* Using cursor T001228 */
         pr_default.execute(26, new Object[] {n181Tabela_PaiCod, A181Tabela_PaiCod});
         if ( (pr_default.getStatus(26) == 101) )
         {
            if ( ! ( (0==A181Tabela_PaiCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Tabela_Pai'.", "ForeignKeyNotFound", 1, "TABELA_PAICOD");
               AnyError = 1;
               GX_FocusControl = dynTabela_PaiCod_Internalname;
            }
         }
         A182Tabela_PaiNom = T001228_A182Tabela_PaiNom[0];
         n182Tabela_PaiNom = T001228_n182Tabela_PaiNom[0];
         pr_default.close(26);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A182Tabela_PaiNom = "";
            n182Tabela_PaiNom = false;
         }
         dynTabela_PaiCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0));
         if ( dynTabela_PaiCod.ItemCount > 0 )
         {
            A181Tabela_PaiCod = (int)(NumberUtil.Val( dynTabela_PaiCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0))), "."));
            n181Tabela_PaiCod = false;
         }
         dynTabela_PaiCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A181Tabela_PaiCod), 6, 0));
         isValidOutput.Add(dynTabela_PaiCod);
         isValidOutput.Add(StringUtil.RTrim( A182Tabela_PaiNom));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Tabela_melhoracod( int GX_Parm1 )
      {
         A746Tabela_MelhoraCod = GX_Parm1;
         n746Tabela_MelhoraCod = false;
         /* Using cursor T001229 */
         pr_default.execute(27, new Object[] {n746Tabela_MelhoraCod, A746Tabela_MelhoraCod});
         if ( (pr_default.getStatus(27) == 101) )
         {
            if ( ! ( (0==A746Tabela_MelhoraCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Tabela_Tabela Melhora'.", "ForeignKeyNotFound", 1, "TABELA_MELHORACOD");
               AnyError = 1;
               GX_FocusControl = edtTabela_MelhoraCod_Internalname;
            }
         }
         pr_default.close(27);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV15Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12122',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV16Tabela_SistemaCod',fld:'vTABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV15Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'AV17Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[{av:'AV15Tabela_ModuloCod',fld:'vTABELA_MODULOCOD',pic:'ZZZZZ9',nv:0},{av:'AV16Tabela_SistemaCod',fld:'vTABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOELIMINAR'","{handler:'E13122',iparms:[{av:'AV7Tabela_Codigo',fld:'vTABELA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOFECHAR'","{handler:'E14122',iparms:[{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOCOPIARCOLAR'","{handler:'E15122',iparms:[{av:'A190Tabela_SistemaCod',fld:'TABELA_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(25);
         pr_default.close(16);
         pr_default.close(26);
         pr_default.close(17);
         pr_default.close(27);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z173Tabela_Nome = "";
         O173Tabela_Nome = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         lblTabelatitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtneliminar_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         bttBtncopiarcolar_Jsonclick = "";
         lblTextblocktabela_nome_Jsonclick = "";
         A173Tabela_Nome = "";
         lblTextblocktabela_descricao_Jsonclick = "";
         A175Tabela_Descricao = "";
         lblTextblocktabela_modulocod_Jsonclick = "";
         lblTextblocktabela_paicod_Jsonclick = "";
         lblTextblocktabela_melhoracod_Jsonclick = "";
         lblTextblocktabela_ativo_Jsonclick = "";
         A191Tabela_SistemaDes = "";
         A189Tabela_ModuloDes = "";
         A182Tabela_PaiNom = "";
         AV19Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode39 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV14TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z175Tabela_Descricao = "";
         Z191Tabela_SistemaDes = "";
         Z189Tabela_ModuloDes = "";
         Z182Tabela_PaiNom = "";
         T00125_A191Tabela_SistemaDes = new String[] {""} ;
         T00125_n191Tabela_SistemaDes = new bool[] {false} ;
         T00128_A190Tabela_SistemaCod = new int[1] ;
         T00128_A172Tabela_Codigo = new int[1] ;
         T00128_A173Tabela_Nome = new String[] {""} ;
         T00128_A175Tabela_Descricao = new String[] {""} ;
         T00128_n175Tabela_Descricao = new bool[] {false} ;
         T00128_A191Tabela_SistemaDes = new String[] {""} ;
         T00128_n191Tabela_SistemaDes = new bool[] {false} ;
         T00128_A189Tabela_ModuloDes = new String[] {""} ;
         T00128_n189Tabela_ModuloDes = new bool[] {false} ;
         T00128_A182Tabela_PaiNom = new String[] {""} ;
         T00128_n182Tabela_PaiNom = new bool[] {false} ;
         T00128_A174Tabela_Ativo = new bool[] {false} ;
         T00128_A188Tabela_ModuloCod = new int[1] ;
         T00128_n188Tabela_ModuloCod = new bool[] {false} ;
         T00128_A181Tabela_PaiCod = new int[1] ;
         T00128_n181Tabela_PaiCod = new bool[] {false} ;
         T00128_A746Tabela_MelhoraCod = new int[1] ;
         T00128_n746Tabela_MelhoraCod = new bool[] {false} ;
         T00124_A189Tabela_ModuloDes = new String[] {""} ;
         T00124_n189Tabela_ModuloDes = new bool[] {false} ;
         T00126_A182Tabela_PaiNom = new String[] {""} ;
         T00126_n182Tabela_PaiNom = new bool[] {false} ;
         T00127_A746Tabela_MelhoraCod = new int[1] ;
         T00127_n746Tabela_MelhoraCod = new bool[] {false} ;
         T00129_A189Tabela_ModuloDes = new String[] {""} ;
         T00129_n189Tabela_ModuloDes = new bool[] {false} ;
         T001210_A182Tabela_PaiNom = new String[] {""} ;
         T001210_n182Tabela_PaiNom = new bool[] {false} ;
         T001211_A746Tabela_MelhoraCod = new int[1] ;
         T001211_n746Tabela_MelhoraCod = new bool[] {false} ;
         T001212_A172Tabela_Codigo = new int[1] ;
         T00123_A190Tabela_SistemaCod = new int[1] ;
         T00123_A172Tabela_Codigo = new int[1] ;
         T00123_A173Tabela_Nome = new String[] {""} ;
         T00123_A175Tabela_Descricao = new String[] {""} ;
         T00123_n175Tabela_Descricao = new bool[] {false} ;
         T00123_A174Tabela_Ativo = new bool[] {false} ;
         T00123_A188Tabela_ModuloCod = new int[1] ;
         T00123_n188Tabela_ModuloCod = new bool[] {false} ;
         T00123_A181Tabela_PaiCod = new int[1] ;
         T00123_n181Tabela_PaiCod = new bool[] {false} ;
         T00123_A746Tabela_MelhoraCod = new int[1] ;
         T00123_n746Tabela_MelhoraCod = new bool[] {false} ;
         T001213_A172Tabela_Codigo = new int[1] ;
         T001213_A190Tabela_SistemaCod = new int[1] ;
         T001214_A172Tabela_Codigo = new int[1] ;
         T001214_A190Tabela_SistemaCod = new int[1] ;
         T00122_A190Tabela_SistemaCod = new int[1] ;
         T00122_A172Tabela_Codigo = new int[1] ;
         T00122_A173Tabela_Nome = new String[] {""} ;
         T00122_A175Tabela_Descricao = new String[] {""} ;
         T00122_n175Tabela_Descricao = new bool[] {false} ;
         T00122_A174Tabela_Ativo = new bool[] {false} ;
         T00122_A188Tabela_ModuloCod = new int[1] ;
         T00122_n188Tabela_ModuloCod = new bool[] {false} ;
         T00122_A181Tabela_PaiCod = new int[1] ;
         T00122_n181Tabela_PaiCod = new bool[] {false} ;
         T00122_A746Tabela_MelhoraCod = new int[1] ;
         T00122_n746Tabela_MelhoraCod = new bool[] {false} ;
         T001215_A172Tabela_Codigo = new int[1] ;
         T001218_A189Tabela_ModuloDes = new String[] {""} ;
         T001218_n189Tabela_ModuloDes = new bool[] {false} ;
         T001219_A182Tabela_PaiNom = new String[] {""} ;
         T001219_n182Tabela_PaiNom = new bool[] {false} ;
         T001220_A746Tabela_MelhoraCod = new int[1] ;
         T001220_n746Tabela_MelhoraCod = new bool[] {false} ;
         T001221_A181Tabela_PaiCod = new int[1] ;
         T001221_n181Tabela_PaiCod = new bool[] {false} ;
         T001222_A176Atributos_Codigo = new int[1] ;
         T001223_A368FuncaoDados_Codigo = new int[1] ;
         T001223_A172Tabela_Codigo = new int[1] ;
         T001224_A172Tabela_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T001225_A188Tabela_ModuloCod = new int[1] ;
         T001225_n188Tabela_ModuloCod = new bool[] {false} ;
         T001225_A189Tabela_ModuloDes = new String[] {""} ;
         T001225_n189Tabela_ModuloDes = new bool[] {false} ;
         T001225_A127Sistema_Codigo = new int[1] ;
         T001225_n127Sistema_Codigo = new bool[] {false} ;
         T001226_A172Tabela_Codigo = new int[1] ;
         T001226_A173Tabela_Nome = new String[] {""} ;
         T001227_A189Tabela_ModuloDes = new String[] {""} ;
         T001227_n189Tabela_ModuloDes = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         T001228_A182Tabela_PaiNom = new String[] {""} ;
         T001228_n182Tabela_PaiNom = new bool[] {false} ;
         T001229_A746Tabela_MelhoraCod = new int[1] ;
         T001229_n746Tabela_MelhoraCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.tabela__default(),
            new Object[][] {
                new Object[] {
               T00122_A190Tabela_SistemaCod, T00122_A172Tabela_Codigo, T00122_A173Tabela_Nome, T00122_A175Tabela_Descricao, T00122_n175Tabela_Descricao, T00122_A174Tabela_Ativo, T00122_A188Tabela_ModuloCod, T00122_n188Tabela_ModuloCod, T00122_A181Tabela_PaiCod, T00122_n181Tabela_PaiCod,
               T00122_A746Tabela_MelhoraCod, T00122_n746Tabela_MelhoraCod
               }
               , new Object[] {
               T00123_A190Tabela_SistemaCod, T00123_A172Tabela_Codigo, T00123_A173Tabela_Nome, T00123_A175Tabela_Descricao, T00123_n175Tabela_Descricao, T00123_A174Tabela_Ativo, T00123_A188Tabela_ModuloCod, T00123_n188Tabela_ModuloCod, T00123_A181Tabela_PaiCod, T00123_n181Tabela_PaiCod,
               T00123_A746Tabela_MelhoraCod, T00123_n746Tabela_MelhoraCod
               }
               , new Object[] {
               T00124_A189Tabela_ModuloDes, T00124_n189Tabela_ModuloDes
               }
               , new Object[] {
               T00125_A191Tabela_SistemaDes, T00125_n191Tabela_SistemaDes
               }
               , new Object[] {
               T00126_A182Tabela_PaiNom, T00126_n182Tabela_PaiNom
               }
               , new Object[] {
               T00127_A746Tabela_MelhoraCod
               }
               , new Object[] {
               T00128_A190Tabela_SistemaCod, T00128_A172Tabela_Codigo, T00128_A173Tabela_Nome, T00128_A175Tabela_Descricao, T00128_n175Tabela_Descricao, T00128_A191Tabela_SistemaDes, T00128_n191Tabela_SistemaDes, T00128_A189Tabela_ModuloDes, T00128_n189Tabela_ModuloDes, T00128_A182Tabela_PaiNom,
               T00128_n182Tabela_PaiNom, T00128_A174Tabela_Ativo, T00128_A188Tabela_ModuloCod, T00128_n188Tabela_ModuloCod, T00128_A181Tabela_PaiCod, T00128_n181Tabela_PaiCod, T00128_A746Tabela_MelhoraCod, T00128_n746Tabela_MelhoraCod
               }
               , new Object[] {
               T00129_A189Tabela_ModuloDes, T00129_n189Tabela_ModuloDes
               }
               , new Object[] {
               T001210_A182Tabela_PaiNom, T001210_n182Tabela_PaiNom
               }
               , new Object[] {
               T001211_A746Tabela_MelhoraCod
               }
               , new Object[] {
               T001212_A172Tabela_Codigo
               }
               , new Object[] {
               T001213_A172Tabela_Codigo, T001213_A190Tabela_SistemaCod
               }
               , new Object[] {
               T001214_A172Tabela_Codigo, T001214_A190Tabela_SistemaCod
               }
               , new Object[] {
               T001215_A172Tabela_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001218_A189Tabela_ModuloDes, T001218_n189Tabela_ModuloDes
               }
               , new Object[] {
               T001219_A182Tabela_PaiNom, T001219_n182Tabela_PaiNom
               }
               , new Object[] {
               T001220_A746Tabela_MelhoraCod
               }
               , new Object[] {
               T001221_A181Tabela_PaiCod
               }
               , new Object[] {
               T001222_A176Atributos_Codigo
               }
               , new Object[] {
               T001223_A368FuncaoDados_Codigo, T001223_A172Tabela_Codigo
               }
               , new Object[] {
               T001224_A172Tabela_Codigo
               }
               , new Object[] {
               T001225_A188Tabela_ModuloCod, T001225_A189Tabela_ModuloDes, T001225_n189Tabela_ModuloDes, T001225_A127Sistema_Codigo, T001225_n127Sistema_Codigo
               }
               , new Object[] {
               T001226_A172Tabela_Codigo, T001226_A173Tabela_Nome
               }
               , new Object[] {
               T001227_A189Tabela_ModuloDes, T001227_n189Tabela_ModuloDes
               }
               , new Object[] {
               T001228_A182Tabela_PaiNom, T001228_n182Tabela_PaiNom
               }
               , new Object[] {
               T001229_A746Tabela_MelhoraCod
               }
            }
         );
         N190Tabela_SistemaCod = 0;
         Z190Tabela_SistemaCod = 0;
         A190Tabela_SistemaCod = 0;
         Z174Tabela_Ativo = true;
         A174Tabela_Ativo = true;
         i174Tabela_Ativo = true;
         AV19Pgmname = "Tabela";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound39 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7Tabela_Codigo ;
      private int wcpOA190Tabela_SistemaCod ;
      private int wcpOAV15Tabela_ModuloCod ;
      private int Z172Tabela_Codigo ;
      private int Z188Tabela_ModuloCod ;
      private int Z181Tabela_PaiCod ;
      private int Z746Tabela_MelhoraCod ;
      private int N190Tabela_SistemaCod ;
      private int N188Tabela_ModuloCod ;
      private int N181Tabela_PaiCod ;
      private int N746Tabela_MelhoraCod ;
      private int A190Tabela_SistemaCod ;
      private int AV16Tabela_SistemaCod ;
      private int A188Tabela_ModuloCod ;
      private int A181Tabela_PaiCod ;
      private int A746Tabela_MelhoraCod ;
      private int AV7Tabela_Codigo ;
      private int AV15Tabela_ModuloCod ;
      private int trnEnded ;
      private int A172Tabela_Codigo ;
      private int edtTabela_Codigo_Enabled ;
      private int edtTabela_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtneliminar_Visible ;
      private int bttBtnfechar_Visible ;
      private int bttBtncopiarcolar_Visible ;
      private int edtTabela_Nome_Enabled ;
      private int edtTabela_Descricao_Enabled ;
      private int edtTabela_MelhoraCod_Enabled ;
      private int lblTextblocktabela_ativo_Visible ;
      private int AV11Insert_Tabela_SistemaCod ;
      private int AV12Insert_Tabela_ModuloCod ;
      private int AV13Insert_Tabela_PaiCod ;
      private int AV18Insert_Tabela_MelhoraCod ;
      private int AV17Sistema_Codigo ;
      private int AV20GXV1 ;
      private int Z190Tabela_SistemaCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z173Tabela_Nome ;
      private String O173Tabela_Nome ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkTabela_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtTabela_Nome_Internalname ;
      private String edtTabela_Codigo_Internalname ;
      private String edtTabela_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblTabelatitle_Internalname ;
      private String lblTabelatitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtneliminar_Internalname ;
      private String bttBtneliminar_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String bttBtncopiarcolar_Internalname ;
      private String bttBtncopiarcolar_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocktabela_nome_Internalname ;
      private String lblTextblocktabela_nome_Jsonclick ;
      private String A173Tabela_Nome ;
      private String edtTabela_Nome_Jsonclick ;
      private String lblTextblocktabela_descricao_Internalname ;
      private String lblTextblocktabela_descricao_Jsonclick ;
      private String edtTabela_Descricao_Internalname ;
      private String lblTextblocktabela_modulocod_Internalname ;
      private String lblTextblocktabela_modulocod_Jsonclick ;
      private String dynTabela_ModuloCod_Internalname ;
      private String dynTabela_ModuloCod_Jsonclick ;
      private String lblTextblocktabela_paicod_Internalname ;
      private String lblTextblocktabela_paicod_Jsonclick ;
      private String dynTabela_PaiCod_Internalname ;
      private String dynTabela_PaiCod_Jsonclick ;
      private String lblTextblocktabela_melhoracod_Internalname ;
      private String lblTextblocktabela_melhoracod_Jsonclick ;
      private String edtTabela_MelhoraCod_Internalname ;
      private String edtTabela_MelhoraCod_Jsonclick ;
      private String lblTextblocktabela_ativo_Internalname ;
      private String lblTextblocktabela_ativo_Jsonclick ;
      private String A189Tabela_ModuloDes ;
      private String A182Tabela_PaiNom ;
      private String AV19Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode39 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z189Tabela_ModuloDes ;
      private String Z182Tabela_PaiNom ;
      private String Dvpanel_tableattributes_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool Z174Tabela_Ativo ;
      private bool entryPointCalled ;
      private bool n188Tabela_ModuloCod ;
      private bool n181Tabela_PaiCod ;
      private bool n746Tabela_MelhoraCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A174Tabela_Ativo ;
      private bool n175Tabela_Descricao ;
      private bool n191Tabela_SistemaDes ;
      private bool n189Tabela_ModuloDes ;
      private bool n182Tabela_PaiNom ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool GXt_boolean1 ;
      private bool i174Tabela_Ativo ;
      private String A175Tabela_Descricao ;
      private String Z175Tabela_Descricao ;
      private String A191Tabela_SistemaDes ;
      private String Z191Tabela_SistemaDes ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_Tabela_SistemaCod ;
      private int aP3_Tabela_ModuloCod ;
      private GXCombobox dynTabela_ModuloCod ;
      private GXCombobox dynTabela_PaiCod ;
      private GXCheckbox chkTabela_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T00125_A191Tabela_SistemaDes ;
      private bool[] T00125_n191Tabela_SistemaDes ;
      private int[] T00128_A190Tabela_SistemaCod ;
      private int[] T00128_A172Tabela_Codigo ;
      private String[] T00128_A173Tabela_Nome ;
      private String[] T00128_A175Tabela_Descricao ;
      private bool[] T00128_n175Tabela_Descricao ;
      private String[] T00128_A191Tabela_SistemaDes ;
      private bool[] T00128_n191Tabela_SistemaDes ;
      private String[] T00128_A189Tabela_ModuloDes ;
      private bool[] T00128_n189Tabela_ModuloDes ;
      private String[] T00128_A182Tabela_PaiNom ;
      private bool[] T00128_n182Tabela_PaiNom ;
      private bool[] T00128_A174Tabela_Ativo ;
      private int[] T00128_A188Tabela_ModuloCod ;
      private bool[] T00128_n188Tabela_ModuloCod ;
      private int[] T00128_A181Tabela_PaiCod ;
      private bool[] T00128_n181Tabela_PaiCod ;
      private int[] T00128_A746Tabela_MelhoraCod ;
      private bool[] T00128_n746Tabela_MelhoraCod ;
      private String[] T00124_A189Tabela_ModuloDes ;
      private bool[] T00124_n189Tabela_ModuloDes ;
      private String[] T00126_A182Tabela_PaiNom ;
      private bool[] T00126_n182Tabela_PaiNom ;
      private int[] T00127_A746Tabela_MelhoraCod ;
      private bool[] T00127_n746Tabela_MelhoraCod ;
      private String[] T00129_A189Tabela_ModuloDes ;
      private bool[] T00129_n189Tabela_ModuloDes ;
      private String[] T001210_A182Tabela_PaiNom ;
      private bool[] T001210_n182Tabela_PaiNom ;
      private int[] T001211_A746Tabela_MelhoraCod ;
      private bool[] T001211_n746Tabela_MelhoraCod ;
      private int[] T001212_A172Tabela_Codigo ;
      private int[] T00123_A190Tabela_SistemaCod ;
      private int[] T00123_A172Tabela_Codigo ;
      private String[] T00123_A173Tabela_Nome ;
      private String[] T00123_A175Tabela_Descricao ;
      private bool[] T00123_n175Tabela_Descricao ;
      private bool[] T00123_A174Tabela_Ativo ;
      private int[] T00123_A188Tabela_ModuloCod ;
      private bool[] T00123_n188Tabela_ModuloCod ;
      private int[] T00123_A181Tabela_PaiCod ;
      private bool[] T00123_n181Tabela_PaiCod ;
      private int[] T00123_A746Tabela_MelhoraCod ;
      private bool[] T00123_n746Tabela_MelhoraCod ;
      private int[] T001213_A172Tabela_Codigo ;
      private int[] T001213_A190Tabela_SistemaCod ;
      private int[] T001214_A172Tabela_Codigo ;
      private int[] T001214_A190Tabela_SistemaCod ;
      private int[] T00122_A190Tabela_SistemaCod ;
      private int[] T00122_A172Tabela_Codigo ;
      private String[] T00122_A173Tabela_Nome ;
      private String[] T00122_A175Tabela_Descricao ;
      private bool[] T00122_n175Tabela_Descricao ;
      private bool[] T00122_A174Tabela_Ativo ;
      private int[] T00122_A188Tabela_ModuloCod ;
      private bool[] T00122_n188Tabela_ModuloCod ;
      private int[] T00122_A181Tabela_PaiCod ;
      private bool[] T00122_n181Tabela_PaiCod ;
      private int[] T00122_A746Tabela_MelhoraCod ;
      private bool[] T00122_n746Tabela_MelhoraCod ;
      private int[] T001215_A172Tabela_Codigo ;
      private String[] T001218_A189Tabela_ModuloDes ;
      private bool[] T001218_n189Tabela_ModuloDes ;
      private String[] T001219_A182Tabela_PaiNom ;
      private bool[] T001219_n182Tabela_PaiNom ;
      private int[] T001220_A746Tabela_MelhoraCod ;
      private bool[] T001220_n746Tabela_MelhoraCod ;
      private int[] T001221_A181Tabela_PaiCod ;
      private bool[] T001221_n181Tabela_PaiCod ;
      private int[] T001222_A176Atributos_Codigo ;
      private int[] T001223_A368FuncaoDados_Codigo ;
      private int[] T001223_A172Tabela_Codigo ;
      private int[] T001224_A172Tabela_Codigo ;
      private int[] T001225_A188Tabela_ModuloCod ;
      private bool[] T001225_n188Tabela_ModuloCod ;
      private String[] T001225_A189Tabela_ModuloDes ;
      private bool[] T001225_n189Tabela_ModuloDes ;
      private int[] T001225_A127Sistema_Codigo ;
      private bool[] T001225_n127Sistema_Codigo ;
      private int[] T001226_A172Tabela_Codigo ;
      private String[] T001226_A173Tabela_Nome ;
      private String[] T001227_A189Tabela_ModuloDes ;
      private bool[] T001227_n189Tabela_ModuloDes ;
      private String[] T001228_A182Tabela_PaiNom ;
      private bool[] T001228_n182Tabela_PaiNom ;
      private int[] T001229_A746Tabela_MelhoraCod ;
      private bool[] T001229_n746Tabela_MelhoraCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV14TrnContextAtt ;
   }

   public class tabela__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new UpdateCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00125 ;
          prmT00125 = new Object[] {
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00128 ;
          prmT00128 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00124 ;
          prmT00124 = new Object[] {
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00126 ;
          prmT00126 = new Object[] {
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00127 ;
          prmT00127 = new Object[] {
          new Object[] {"@Tabela_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00129 ;
          prmT00129 = new Object[] {
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001210 ;
          prmT001210 = new Object[] {
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001211 ;
          prmT001211 = new Object[] {
          new Object[] {"@Tabela_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001212 ;
          prmT001212 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00123 ;
          prmT00123 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001213 ;
          prmT001213 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001214 ;
          prmT001214 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00122 ;
          prmT00122 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001215 ;
          prmT001215 = new Object[] {
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Tabela_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Tabela_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001216 ;
          prmT001216 = new Object[] {
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@Tabela_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Tabela_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_MelhoraCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001217 ;
          prmT001217 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001218 ;
          prmT001218 = new Object[] {
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001219 ;
          prmT001219 = new Object[] {
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001220 ;
          prmT001220 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001221 ;
          prmT001221 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001222 ;
          prmT001222 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001223 ;
          prmT001223 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001224 ;
          prmT001224 = new Object[] {
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001225 ;
          prmT001225 = new Object[] {
          new Object[] {"@Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001226 ;
          prmT001226 = new Object[] {
          new Object[] {"@AV16Tabela_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001227 ;
          prmT001227 = new Object[] {
          new Object[] {"@Tabela_ModuloCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001228 ;
          prmT001228 = new Object[] {
          new Object[] {"@Tabela_PaiCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001229 ;
          prmT001229 = new Object[] {
          new Object[] {"@Tabela_MelhoraCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00122", "SELECT [Tabela_SistemaCod] AS Tabela_SistemaCod, [Tabela_Codigo], [Tabela_Nome], [Tabela_Descricao], [Tabela_Ativo], [Tabela_ModuloCod] AS Tabela_ModuloCod, [Tabela_PaiCod] AS Tabela_PaiCod, [Tabela_MelhoraCod] AS Tabela_MelhoraCod FROM [Tabela] WITH (UPDLOCK) WHERE [Tabela_Codigo] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00122,1,0,true,false )
             ,new CursorDef("T00123", "SELECT [Tabela_SistemaCod] AS Tabela_SistemaCod, [Tabela_Codigo], [Tabela_Nome], [Tabela_Descricao], [Tabela_Ativo], [Tabela_ModuloCod] AS Tabela_ModuloCod, [Tabela_PaiCod] AS Tabela_PaiCod, [Tabela_MelhoraCod] AS Tabela_MelhoraCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00123,1,0,true,false )
             ,new CursorDef("T00124", "SELECT [Modulo_Nome] AS Tabela_ModuloDes FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Tabela_ModuloCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00124,1,0,true,false )
             ,new CursorDef("T00125", "SELECT [Sistema_Nome] AS Tabela_SistemaDes FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Tabela_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00125,1,0,true,false )
             ,new CursorDef("T00126", "SELECT [Tabela_Nome] AS Tabela_PaiNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_PaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00126,1,0,true,false )
             ,new CursorDef("T00127", "SELECT [Tabela_Codigo] AS Tabela_MelhoraCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00127,1,0,true,false )
             ,new CursorDef("T00128", "SELECT TM1.[Tabela_SistemaCod] AS Tabela_SistemaCod, TM1.[Tabela_Codigo], TM1.[Tabela_Nome], TM1.[Tabela_Descricao], T2.[Sistema_Nome] AS Tabela_SistemaDes, T3.[Modulo_Nome] AS Tabela_ModuloDes, T4.[Tabela_Nome] AS Tabela_PaiNom, TM1.[Tabela_Ativo], TM1.[Tabela_ModuloCod] AS Tabela_ModuloCod, TM1.[Tabela_PaiCod] AS Tabela_PaiCod, TM1.[Tabela_MelhoraCod] AS Tabela_MelhoraCod FROM ((([Tabela] TM1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = TM1.[Tabela_SistemaCod]) LEFT JOIN [Modulo] T3 WITH (NOLOCK) ON T3.[Modulo_Codigo] = TM1.[Tabela_ModuloCod]) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = TM1.[Tabela_PaiCod]) WHERE TM1.[Tabela_Codigo] = @Tabela_Codigo and TM1.[Tabela_SistemaCod] = @Tabela_SistemaCod ORDER BY TM1.[Tabela_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00128,100,0,true,false )
             ,new CursorDef("T00129", "SELECT [Modulo_Nome] AS Tabela_ModuloDes FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Tabela_ModuloCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00129,1,0,true,false )
             ,new CursorDef("T001210", "SELECT [Tabela_Nome] AS Tabela_PaiNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_PaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001210,1,0,true,false )
             ,new CursorDef("T001211", "SELECT [Tabela_Codigo] AS Tabela_MelhoraCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001211,1,0,true,false )
             ,new CursorDef("T001212", "SELECT [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001212,1,0,true,false )
             ,new CursorDef("T001213", "SELECT TOP 1 [Tabela_Codigo], [Tabela_SistemaCod] AS Tabela_SistemaCod FROM [Tabela] WITH (NOLOCK) WHERE ( [Tabela_Codigo] > @Tabela_Codigo) and [Tabela_SistemaCod] = @Tabela_SistemaCod ORDER BY [Tabela_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001213,1,0,true,true )
             ,new CursorDef("T001214", "SELECT TOP 1 [Tabela_Codigo], [Tabela_SistemaCod] AS Tabela_SistemaCod FROM [Tabela] WITH (NOLOCK) WHERE ( [Tabela_Codigo] < @Tabela_Codigo) and [Tabela_SistemaCod] = @Tabela_SistemaCod ORDER BY [Tabela_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001214,1,0,true,true )
             ,new CursorDef("T001215", "INSERT INTO [Tabela]([Tabela_SistemaCod], [Tabela_Nome], [Tabela_Descricao], [Tabela_Ativo], [Tabela_ModuloCod], [Tabela_PaiCod], [Tabela_MelhoraCod]) VALUES(@Tabela_SistemaCod, @Tabela_Nome, @Tabela_Descricao, @Tabela_Ativo, @Tabela_ModuloCod, @Tabela_PaiCod, @Tabela_MelhoraCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001215)
             ,new CursorDef("T001216", "UPDATE [Tabela] SET [Tabela_SistemaCod]=@Tabela_SistemaCod, [Tabela_Nome]=@Tabela_Nome, [Tabela_Descricao]=@Tabela_Descricao, [Tabela_Ativo]=@Tabela_Ativo, [Tabela_ModuloCod]=@Tabela_ModuloCod, [Tabela_PaiCod]=@Tabela_PaiCod, [Tabela_MelhoraCod]=@Tabela_MelhoraCod  WHERE [Tabela_Codigo] = @Tabela_Codigo", GxErrorMask.GX_NOMASK,prmT001216)
             ,new CursorDef("T001217", "DELETE FROM [Tabela]  WHERE [Tabela_Codigo] = @Tabela_Codigo", GxErrorMask.GX_NOMASK,prmT001217)
             ,new CursorDef("T001218", "SELECT [Modulo_Nome] AS Tabela_ModuloDes FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Tabela_ModuloCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001218,1,0,true,false )
             ,new CursorDef("T001219", "SELECT [Tabela_Nome] AS Tabela_PaiNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_PaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001219,1,0,true,false )
             ,new CursorDef("T001220", "SELECT TOP 1 [Tabela_Codigo] AS Tabela_MelhoraCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_MelhoraCod] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001220,1,0,true,true )
             ,new CursorDef("T001221", "SELECT TOP 1 [Tabela_Codigo] AS Tabela_PaiCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_PaiCod] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001221,1,0,true,true )
             ,new CursorDef("T001222", "SELECT TOP 1 [Atributos_Codigo] FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_TabelaCod] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001222,1,0,true,true )
             ,new CursorDef("T001223", "SELECT TOP 1 [FuncaoDados_Codigo], [Tabela_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001223,1,0,true,true )
             ,new CursorDef("T001224", "SELECT [Tabela_Codigo] FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_SistemaCod] = @Tabela_SistemaCod ORDER BY [Tabela_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001224,100,0,true,false )
             ,new CursorDef("T001225", "SELECT [Modulo_Codigo] AS Tabela_ModuloCod, [Modulo_Nome] AS Tabela_ModuloDes, [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Tabela_SistemaCod ORDER BY [Modulo_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001225,0,0,true,false )
             ,new CursorDef("T001226", "SELECT [Tabela_Codigo], [Tabela_Nome] FROM [Tabela] WITH (NOLOCK) WHERE ([Tabela_Ativo] = 1) AND ([Tabela_SistemaCod] = @AV16Tabela_SistemaCod) ORDER BY [Tabela_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT001226,0,0,true,false )
             ,new CursorDef("T001227", "SELECT [Modulo_Nome] AS Tabela_ModuloDes FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Tabela_ModuloCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001227,1,0,true,false )
             ,new CursorDef("T001228", "SELECT [Tabela_Nome] AS Tabela_PaiNom FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_PaiCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001228,1,0,true,false )
             ,new CursorDef("T001229", "SELECT [Tabela_Codigo] AS Tabela_MelhoraCod FROM [Tabela] WITH (NOLOCK) WHERE [Tabela_Codigo] = @Tabela_MelhoraCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001229,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getString(6, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((bool[]) buf[11])[0] = rslt.getBool(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                return;
             case 7 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 26 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (bool)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[10]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                stmt.SetParameter(4, (bool)parms[4]);
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[10]);
                }
                stmt.SetParameter(8, (int)parms[11]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
