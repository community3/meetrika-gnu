/*
               File: PRC_InserirSDTDemandas
        Description: Inserir SDT Demandas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:8:9.62
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_inserirsdtdemandas : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV12Contratada_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV46SrvParaFtr = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV11Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV28StatusDmn = GetNextPar( );
                  AV31ContratadaSrv_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV41DataSrvFat = GetNextPar( );
                  AV48ContratadaOrigem_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV66PrazoEntrega = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  AV55Agrupador = GetNextPar( );
                  AV60Notificar = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV90ContratoServicosOS_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_inserirsdtdemandas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_inserirsdtdemandas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contratada_Codigo ,
                           bool aP1_SrvParaFtr ,
                           int aP2_Usuario_Codigo ,
                           String aP3_StatusDmn ,
                           int aP4_ContratadaSrv_Codigo ,
                           String aP5_DataSrvFat ,
                           int aP6_ContratadaOrigem_Codigo ,
                           DateTime aP7_PrazoEntrega ,
                           String aP8_Agrupador ,
                           bool aP9_Notificar ,
                           int aP10_ContratoServicosOS_Codigo )
      {
         this.AV12Contratada_Codigo = aP0_Contratada_Codigo;
         this.AV46SrvParaFtr = aP1_SrvParaFtr;
         this.AV11Usuario_Codigo = aP2_Usuario_Codigo;
         this.AV28StatusDmn = aP3_StatusDmn;
         this.AV31ContratadaSrv_Codigo = aP4_ContratadaSrv_Codigo;
         this.AV41DataSrvFat = aP5_DataSrvFat;
         this.AV48ContratadaOrigem_Codigo = aP6_ContratadaOrigem_Codigo;
         this.AV66PrazoEntrega = aP7_PrazoEntrega;
         this.AV55Agrupador = aP8_Agrupador;
         this.AV60Notificar = aP9_Notificar;
         this.AV90ContratoServicosOS_Codigo = aP10_ContratoServicosOS_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_Contratada_Codigo ,
                                 bool aP1_SrvParaFtr ,
                                 int aP2_Usuario_Codigo ,
                                 String aP3_StatusDmn ,
                                 int aP4_ContratadaSrv_Codigo ,
                                 String aP5_DataSrvFat ,
                                 int aP6_ContratadaOrigem_Codigo ,
                                 DateTime aP7_PrazoEntrega ,
                                 String aP8_Agrupador ,
                                 bool aP9_Notificar ,
                                 int aP10_ContratoServicosOS_Codigo )
      {
         aprc_inserirsdtdemandas objaprc_inserirsdtdemandas;
         objaprc_inserirsdtdemandas = new aprc_inserirsdtdemandas();
         objaprc_inserirsdtdemandas.AV12Contratada_Codigo = aP0_Contratada_Codigo;
         objaprc_inserirsdtdemandas.AV46SrvParaFtr = aP1_SrvParaFtr;
         objaprc_inserirsdtdemandas.AV11Usuario_Codigo = aP2_Usuario_Codigo;
         objaprc_inserirsdtdemandas.AV28StatusDmn = aP3_StatusDmn;
         objaprc_inserirsdtdemandas.AV31ContratadaSrv_Codigo = aP4_ContratadaSrv_Codigo;
         objaprc_inserirsdtdemandas.AV41DataSrvFat = aP5_DataSrvFat;
         objaprc_inserirsdtdemandas.AV48ContratadaOrigem_Codigo = aP6_ContratadaOrigem_Codigo;
         objaprc_inserirsdtdemandas.AV66PrazoEntrega = aP7_PrazoEntrega;
         objaprc_inserirsdtdemandas.AV55Agrupador = aP8_Agrupador;
         objaprc_inserirsdtdemandas.AV60Notificar = aP9_Notificar;
         objaprc_inserirsdtdemandas.AV90ContratoServicosOS_Codigo = aP10_ContratoServicosOS_Codigo;
         objaprc_inserirsdtdemandas.context.SetSubmitInitialConfig(context);
         objaprc_inserirsdtdemandas.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_inserirsdtdemandas);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_inserirsdtdemandas)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV21WWPContext) ;
            AV9SDT_Demandas.FromXml(AV10WebSession.Get("SDTDemandas"), "");
            AV64ContratoServicos_Codigo = ((SdtSDT_Demandas_Demanda)AV9SDT_Demandas.Item(1)).gxTpr_Cntsrvcod;
            /* Using cursor P004E2 */
            pr_default.execute(0, new Object[] {AV64ContratoServicos_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A155Servico_Codigo = P004E2_A155Servico_Codigo[0];
               A39Contratada_Codigo = P004E2_A39Contratada_Codigo[0];
               A52Contratada_AreaTrabalhoCod = P004E2_A52Contratada_AreaTrabalhoCod[0];
               A40Contratada_PessoaCod = P004E2_A40Contratada_PessoaCod[0];
               A1016Contrato_PrepostoPesCod = P004E2_A1016Contrato_PrepostoPesCod[0];
               n1016Contrato_PrepostoPesCod = P004E2_n1016Contrato_PrepostoPesCod[0];
               A29Contratante_Codigo = P004E2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P004E2_n29Contratante_Codigo[0];
               A291Usuario_EhContratada = P004E2_A291Usuario_EhContratada[0];
               n291Usuario_EhContratada = P004E2_n291Usuario_EhContratada[0];
               A74Contrato_Codigo = P004E2_A74Contrato_Codigo[0];
               A160ContratoServicos_Codigo = P004E2_A160ContratoServicos_Codigo[0];
               A557Servico_VlrUnidadeContratada = P004E2_A557Servico_VlrUnidadeContratada[0];
               A116Contrato_ValorUnidadeContratacao = P004E2_A116Contrato_ValorUnidadeContratacao[0];
               A605Servico_Sigla = P004E2_A605Servico_Sigla[0];
               A558Servico_Percentual = P004E2_A558Servico_Percentual[0];
               n558Servico_Percentual = P004E2_n558Servico_Percentual[0];
               A41Contratada_PessoaNom = P004E2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P004E2_n41Contratada_PessoaNom[0];
               A1015Contrato_PrepostoNom = P004E2_A1015Contrato_PrepostoNom[0];
               n1015Contrato_PrepostoNom = P004E2_n1015Contrato_PrepostoNom[0];
               A1152ContratoServicos_PrazoAnalise = P004E2_A1152ContratoServicos_PrazoAnalise[0];
               n1152ContratoServicos_PrazoAnalise = P004E2_n1152ContratoServicos_PrazoAnalise[0];
               A1454ContratoServicos_PrazoTpDias = P004E2_A1454ContratoServicos_PrazoTpDias[0];
               n1454ContratoServicos_PrazoTpDias = P004E2_n1454ContratoServicos_PrazoTpDias[0];
               A593Contratante_OSAutomatica = P004E2_A593Contratante_OSAutomatica[0];
               A524Contratada_OS = P004E2_A524Contratada_OS[0];
               n524Contratada_OS = P004E2_n524Contratada_OS[0];
               A1013Contrato_PrepostoCod = P004E2_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = P004E2_n1013Contrato_PrepostoCod[0];
               A605Servico_Sigla = P004E2_A605Servico_Sigla[0];
               A39Contratada_Codigo = P004E2_A39Contratada_Codigo[0];
               A116Contrato_ValorUnidadeContratacao = P004E2_A116Contrato_ValorUnidadeContratacao[0];
               A1013Contrato_PrepostoCod = P004E2_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = P004E2_n1013Contrato_PrepostoCod[0];
               A52Contratada_AreaTrabalhoCod = P004E2_A52Contratada_AreaTrabalhoCod[0];
               A40Contratada_PessoaCod = P004E2_A40Contratada_PessoaCod[0];
               A524Contratada_OS = P004E2_A524Contratada_OS[0];
               n524Contratada_OS = P004E2_n524Contratada_OS[0];
               A29Contratante_Codigo = P004E2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = P004E2_n29Contratante_Codigo[0];
               A593Contratante_OSAutomatica = P004E2_A593Contratante_OSAutomatica[0];
               A41Contratada_PessoaNom = P004E2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P004E2_n41Contratada_PessoaNom[0];
               A1016Contrato_PrepostoPesCod = P004E2_A1016Contrato_PrepostoPesCod[0];
               n1016Contrato_PrepostoPesCod = P004E2_n1016Contrato_PrepostoPesCod[0];
               A291Usuario_EhContratada = P004E2_A291Usuario_EhContratada[0];
               n291Usuario_EhContratada = P004E2_n291Usuario_EhContratada[0];
               A1015Contrato_PrepostoNom = P004E2_A1015Contrato_PrepostoNom[0];
               n1015Contrato_PrepostoNom = P004E2_n1015Contrato_PrepostoNom[0];
               if ( ( A557Servico_VlrUnidadeContratada > Convert.ToDecimal( 0 )) )
               {
                  AV22ContagemResultado_ValorPF = A557Servico_VlrUnidadeContratada;
               }
               else
               {
                  AV22ContagemResultado_ValorPF = A116Contrato_ValorUnidadeContratacao;
               }
               AV89Contrato_Codigo = A74Contrato_Codigo;
               AV35Servico_Sigla = A605Servico_Sigla;
               AV33Servico_Percentual = A558Servico_Percentual;
               AV18Titulo = "Servi�o " + A605Servico_Sigla;
               AV16Contratada_Nome = A41Contratada_PessoaNom;
               AV68Preposto_Nome = A1015Contrato_PrepostoNom;
               AV82DiasAnalise = A1152ContratoServicos_PrazoAnalise;
               AV87TipoDias = A1454ContratoServicos_PrazoTpDias;
               AV92OSAutomatica = A593Contratante_OSAutomatica;
               AV93Contratada_OS = A524Contratada_OS;
               AV58Usuarios.Add(A1013Contrato_PrepostoCod, 0);
               /* Using cursor P004E3 */
               pr_default.execute(1, new Object[] {A74Contrato_Codigo, n291Usuario_EhContratada, A291Usuario_EhContratada});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = P004E3_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = P004E3_A1079ContratoGestor_UsuarioCod[0];
                  AV58Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( ( AV90ContratoServicosOS_Codigo > 0 ) || AV46SrvParaFtr )
            {
               /* Using cursor P004E4 */
               pr_default.execute(2, new Object[] {AV90ContratoServicosOS_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A155Servico_Codigo = P004E4_A155Servico_Codigo[0];
                  A74Contrato_Codigo = P004E4_A74Contrato_Codigo[0];
                  A160ContratoServicos_Codigo = P004E4_A160ContratoServicos_Codigo[0];
                  A116Contrato_ValorUnidadeContratacao = P004E4_A116Contrato_ValorUnidadeContratacao[0];
                  A605Servico_Sigla = P004E4_A605Servico_Sigla[0];
                  A558Servico_Percentual = P004E4_A558Servico_Percentual[0];
                  n558Servico_Percentual = P004E4_n558Servico_Percentual[0];
                  A156Servico_Descricao = P004E4_A156Servico_Descricao[0];
                  n156Servico_Descricao = P004E4_n156Servico_Descricao[0];
                  A605Servico_Sigla = P004E4_A605Servico_Sigla[0];
                  A156Servico_Descricao = P004E4_A156Servico_Descricao[0];
                  n156Servico_Descricao = P004E4_n156Servico_Descricao[0];
                  A116Contrato_ValorUnidadeContratacao = P004E4_A116Contrato_ValorUnidadeContratacao[0];
                  AV22ContagemResultado_ValorPF = A116Contrato_ValorUnidadeContratacao;
                  AV43ServicoVnc_Sigla = A605Servico_Sigla;
                  AV44ServicoVnc_Inicial = StringUtil.Substring( A605Servico_Sigla, 1, 1);
                  AV45ServicoVnc_Percentual = A558Servico_Percentual;
                  AV47ServicoVnc_Descricao = StringUtil.Trim( A156Servico_Descricao);
                  AV18Titulo = "Servi�o " + AV35Servico_Sigla + "com Servi�o vinculado de " + AV43ServicoVnc_Sigla;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
            }
            /* Using cursor P004E5 */
            pr_default.execute(3, new Object[] {AV31ContratadaSrv_Codigo, AV11Usuario_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A69ContratadaUsuario_UsuarioCod = P004E5_A69ContratadaUsuario_UsuarioCod[0];
               A66ContratadaUsuario_ContratadaCod = P004E5_A66ContratadaUsuario_ContratadaCod[0];
               AV40ContagemResultado_ContadorFMCod = AV11Usuario_Codigo;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
            if ( (DateTime.MinValue==AV66PrazoEntrega) )
            {
               /* Execute user subroutine: 'PRAZOATE' */
               S111 ();
               if ( returnInSub )
               {
                  this.cleanup();
                  if (true) return;
               }
            }
            else
            {
               AV54DataEntrega = AV66PrazoEntrega;
            }
            AV57EmailText = StringUtil.NewLine( ) + StringUtil.NewLine( ) + "MEETRIKA - Sistema de Gest�o de Contratos de TI" + StringUtil.NewLine( ) + StringUtil.NewLine( );
            AV57EmailText = AV57EmailText + "Prezado(a) gestor," + StringUtil.NewLine( ) + StringUtil.NewLine( );
            AV105GXV1 = 1;
            while ( AV105GXV1 <= AV9SDT_Demandas.Count )
            {
               AV8SDT_Demanda = ((SdtSDT_Demandas_Demanda)AV9SDT_Demandas.Item(AV105GXV1));
               if ( ( AV8SDT_Demanda.gxTpr_Pfbfm > Convert.ToDecimal( 0 )) )
               {
                  AV78TemContagem = true;
                  if (true) break;
               }
               AV105GXV1 = (int)(AV105GXV1+1);
            }
            AV94Contratada.Load(AV12Contratada_Codigo);
            AV15i = 0;
            AV106GXV2 = 1;
            while ( AV106GXV2 <= AV9SDT_Demandas.Count )
            {
               AV8SDT_Demanda = ((SdtSDT_Demandas_Demanda)AV9SDT_Demandas.Item(AV106GXV2));
               AV52Retorno = false;
               if ( AV8SDT_Demanda.gxTpr_Naocadastrada )
               {
                  if ( AV92OSAutomatica )
                  {
                     AV93Contratada_OS = (int)(AV93Contratada_OS+1);
                     AV94Contratada.gxTpr_Contratada_os = AV93Contratada_OS;
                     AV94Contratada.Save();
                     AV8SDT_Demanda.gxTpr_Demandafm = StringUtil.Trim( StringUtil.Str( (decimal)(AV93Contratada_OS), 8, 0));
                  }
                  /*
                     INSERT RECORD ON TABLE ContagemResultado

                  */
                  A490ContagemResultado_ContratadaCod = AV12Contratada_Codigo;
                  n490ContagemResultado_ContratadaCod = false;
                  A457ContagemResultado_Demanda = StringUtil.Trim( AV8SDT_Demanda.gxTpr_Demanda);
                  n457ContagemResultado_Demanda = false;
                  A493ContagemResultado_DemandaFM = StringUtil.Trim( AV8SDT_Demanda.gxTpr_Demandafm);
                  n493ContagemResultado_DemandaFM = false;
                  A471ContagemResultado_DataDmn = AV8SDT_Demanda.gxTpr_Datadmn;
                  A494ContagemResultado_Descricao = AV8SDT_Demanda.gxTpr_Descricao;
                  n494ContagemResultado_Descricao = false;
                  A514ContagemResultado_Observacao = AV8SDT_Demanda.gxTpr_Observacao;
                  n514ContagemResultado_Observacao = false;
                  A489ContagemResultado_SistemaCod = AV8SDT_Demanda.gxTpr_Sistemacod;
                  n489ContagemResultado_SistemaCod = false;
                  if ( (0==AV8SDT_Demanda.gxTpr_Modulocod) )
                  {
                     A146Modulo_Codigo = 0;
                     n146Modulo_Codigo = false;
                     n146Modulo_Codigo = true;
                  }
                  else
                  {
                     A146Modulo_Codigo = AV8SDT_Demanda.gxTpr_Modulocod;
                     n146Modulo_Codigo = false;
                  }
                  A465ContagemResultado_Link = AV8SDT_Demanda.gxTpr_Link;
                  n465ContagemResultado_Link = false;
                  A508ContagemResultado_Owner = AV11Usuario_Codigo;
                  A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
                  n1350ContagemResultado_DataCadastro = false;
                  A512ContagemResultado_ValorPF = AV22ContagemResultado_ValorPF;
                  n512ContagemResultado_ValorPF = false;
                  A1046ContagemResultado_Agrupador = AV55Agrupador;
                  n1046ContagemResultado_Agrupador = false;
                  A472ContagemResultado_DataEntrega = DateTimeUtil.ResetTime(AV8SDT_Demanda.gxTpr_Dataent);
                  n472ContagemResultado_DataEntrega = false;
                  A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(AV8SDT_Demanda.gxTpr_Dataent);
                  n912ContagemResultado_HoraEntrega = false;
                  A1351ContagemResultado_DataPrevista = AV8SDT_Demanda.gxTpr_Dataent;
                  n1351ContagemResultado_DataPrevista = false;
                  A1515ContagemResultado_Evento = AV8SDT_Demanda.gxTpr_Evento;
                  n1515ContagemResultado_Evento = false;
                  A1553ContagemResultado_CntSrvCod = AV8SDT_Demanda.gxTpr_Cntsrvcod;
                  n1553ContagemResultado_CntSrvCod = false;
                  GXt_int1 = A1227ContagemResultado_PrazoInicialDias;
                  new prc_diasuteisentre(context ).execute(  AV8SDT_Demanda.gxTpr_Datadmn,  AV8SDT_Demanda.gxTpr_Dataent,  AV87TipoDias, out  GXt_int1) ;
                  A1227ContagemResultado_PrazoInicialDias = GXt_int1;
                  n1227ContagemResultado_PrazoInicialDias = false;
                  if ( (0==AV90ContratoServicosOS_Codigo) )
                  {
                     if ( String.IsNullOrEmpty(StringUtil.RTrim( AV28StatusDmn)) )
                     {
                        if ( AV78TemContagem )
                        {
                           if ( (Convert.ToDecimal(0)==AV8SDT_Demanda.gxTpr_Pfbfm) )
                           {
                              A484ContagemResultado_StatusDmn = "A";
                              n484ContagemResultado_StatusDmn = false;
                           }
                           else
                           {
                              A484ContagemResultado_StatusDmn = "R";
                              n484ContagemResultado_StatusDmn = false;
                           }
                        }
                        else
                        {
                           A484ContagemResultado_StatusDmn = "S";
                           n484ContagemResultado_StatusDmn = false;
                        }
                     }
                     else
                     {
                        A484ContagemResultado_StatusDmn = AV28StatusDmn;
                        n484ContagemResultado_StatusDmn = false;
                     }
                  }
                  else
                  {
                     A484ContagemResultado_StatusDmn = "B";
                     n484ContagemResultado_StatusDmn = false;
                  }
                  AV79Status = A484ContagemResultado_StatusDmn;
                  if ( (0==AV8SDT_Demanda.gxTpr_Contadorfscod) )
                  {
                     A454ContagemResultado_ContadorFSCod = 0;
                     n454ContagemResultado_ContadorFSCod = false;
                     n454ContagemResultado_ContadorFSCod = true;
                  }
                  else
                  {
                     A454ContagemResultado_ContadorFSCod = AV8SDT_Demanda.gxTpr_Contadorfscod;
                     n454ContagemResultado_ContadorFSCod = false;
                  }
                  if ( (Convert.ToDecimal(0)==AV8SDT_Demanda.gxTpr_Pfbfs) )
                  {
                     A798ContagemResultado_PFBFSImp = 0;
                     n798ContagemResultado_PFBFSImp = false;
                     n798ContagemResultado_PFBFSImp = true;
                     A799ContagemResultado_PFLFSImp = 0;
                     n799ContagemResultado_PFLFSImp = false;
                     n799ContagemResultado_PFLFSImp = true;
                  }
                  else
                  {
                     A798ContagemResultado_PFBFSImp = AV8SDT_Demanda.gxTpr_Pfbfs;
                     n798ContagemResultado_PFBFSImp = false;
                     A799ContagemResultado_PFLFSImp = AV8SDT_Demanda.gxTpr_Pflfs;
                     n799ContagemResultado_PFLFSImp = false;
                  }
                  if ( (0==AV48ContratadaOrigem_Codigo) )
                  {
                     A805ContagemResultado_ContratadaOrigemCod = 0;
                     n805ContagemResultado_ContratadaOrigemCod = false;
                     n805ContagemResultado_ContratadaOrigemCod = true;
                  }
                  else
                  {
                     A805ContagemResultado_ContratadaOrigemCod = AV48ContratadaOrigem_Codigo;
                     n805ContagemResultado_ContratadaOrigemCod = false;
                  }
                  if ( (0==AV8SDT_Demanda.gxTpr_Contadorfmcod) )
                  {
                     A890ContagemResultado_Responsavel = 0;
                     n890ContagemResultado_Responsavel = false;
                     n890ContagemResultado_Responsavel = true;
                  }
                  else
                  {
                     A890ContagemResultado_Responsavel = AV8SDT_Demanda.gxTpr_Contadorfmcod;
                     n890ContagemResultado_Responsavel = false;
                  }
                  A468ContagemResultado_NaoCnfDmnCod = 0;
                  n468ContagemResultado_NaoCnfDmnCod = false;
                  n468ContagemResultado_NaoCnfDmnCod = true;
                  A597ContagemResultado_LoteAceiteCod = 0;
                  n597ContagemResultado_LoteAceiteCod = false;
                  n597ContagemResultado_LoteAceiteCod = true;
                  A602ContagemResultado_OSVinculada = 0;
                  n602ContagemResultado_OSVinculada = false;
                  n602ContagemResultado_OSVinculada = true;
                  A1043ContagemResultado_LiqLogCod = 0;
                  n1043ContagemResultado_LiqLogCod = false;
                  n1043ContagemResultado_LiqLogCod = true;
                  A1443ContagemResultado_CntSrvPrrCod = 0;
                  n1443ContagemResultado_CntSrvPrrCod = false;
                  n1443ContagemResultado_CntSrvPrrCod = true;
                  A1583ContagemResultado_TipoRegistro = 1;
                  A1636ContagemResultado_ServicoSS = 0;
                  n1636ContagemResultado_ServicoSS = false;
                  n1636ContagemResultado_ServicoSS = true;
                  A485ContagemResultado_EhValidacao = false;
                  n485ContagemResultado_EhValidacao = false;
                  A598ContagemResultado_Baseline = false;
                  n598ContagemResultado_Baseline = false;
                  A1452ContagemResultado_SS = 0;
                  n1452ContagemResultado_SS = false;
                  /* Using cursor P004E6 */
                  pr_default.execute(4, new Object[] {A471ContagemResultado_DataDmn, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, n465ContagemResultado_Link, A465ContagemResultado_Link, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n146Modulo_Codigo, A146Modulo_Codigo, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, A508ContagemResultado_Owner, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, n798ContagemResultado_PFBFSImp, A798ContagemResultado_PFBFSImp, n799ContagemResultado_PFLFSImp, A799ContagemResultado_PFLFSImp, n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1046ContagemResultado_Agrupador, A1046ContagemResultado_Agrupador, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1443ContagemResultado_CntSrvPrrCod, A1443ContagemResultado_CntSrvPrrCod, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, A1583ContagemResultado_TipoRegistro, n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS});
                  A456ContagemResultado_Codigo = P004E6_A456ContagemResultado_Codigo[0];
                  pr_default.close(4);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                  if ( (pr_default.getStatus(4) == 1) )
                  {
                     context.Gx_err = 1;
                     Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                  }
                  else
                  {
                     context.Gx_err = 0;
                     Gx_emsg = "";
                  }
                  /* End Insert */
                  AV19ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                  AV32StatusDmnSrv = "S";
                  AV91DataInicio = DateTimeUtil.ResetTime( AV8SDT_Demanda.gxTpr_Datadmn ) ;
                  AV91DataInicio = DateTimeUtil.TAdd( AV91DataInicio, 3600*(DateTimeUtil.Hour( AV8SDT_Demanda.gxTpr_Dataent)));
                  AV91DataInicio = DateTimeUtil.TAdd( AV91DataInicio, 60*(DateTimeUtil.Minute( AV8SDT_Demanda.gxTpr_Dataent)));
                  if ( StringUtil.StrCmp(AV79Status, "S") == 0 )
                  {
                     new prc_novocicloexecucao(context ).execute(  AV19ContagemResultado_Codigo,  AV91DataInicio,  AV8SDT_Demanda.gxTpr_Dataent,  AV87TipoDias,  true) ;
                  }
                  new prc_inslogresponsavel(context ).execute( ref  AV19ContagemResultado_Codigo,  AV8SDT_Demanda.gxTpr_Contadorfmcod,  "I",  "D",  AV11Usuario_Codigo,  0,  "",  AV79Status,  "",  AV8SDT_Demanda.gxTpr_Dataent,  false) ;
                  AV85Codigos.Add(AV19ContagemResultado_Codigo, 0);
                  context.CommitDataStores( "PRC_InserirSDTDemandas");
                  new prc_disparoservicovinculado(context ).execute(  AV19ContagemResultado_Codigo,  AV21WWPContext.gxTpr_Userid) ;
               }
               else
               {
                  /* Using cursor P004E7 */
                  pr_default.execute(5, new Object[] {AV8SDT_Demanda.gxTpr_Demanda, AV8SDT_Demanda.gxTpr_Sistemacod});
                  while ( (pr_default.getStatus(5) != 101) )
                  {
                     A456ContagemResultado_Codigo = P004E7_A456ContagemResultado_Codigo[0];
                     A489ContagemResultado_SistemaCod = P004E7_A489ContagemResultado_SistemaCod[0];
                     n489ContagemResultado_SistemaCod = P004E7_n489ContagemResultado_SistemaCod[0];
                     A457ContagemResultado_Demanda = P004E7_A457ContagemResultado_Demanda[0];
                     n457ContagemResultado_Demanda = P004E7_n457ContagemResultado_Demanda[0];
                     A484ContagemResultado_StatusDmn = P004E7_A484ContagemResultado_StatusDmn[0];
                     n484ContagemResultado_StatusDmn = P004E7_n484ContagemResultado_StatusDmn[0];
                     AV19ContagemResultado_Codigo = A456ContagemResultado_Codigo;
                     if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "B") == 0 )
                     {
                        AV32StatusDmnSrv = "S";
                     }
                     else if ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "D") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "S") == 0 ) )
                     {
                        AV32StatusDmnSrv = "A";
                     }
                     else
                     {
                        AV32StatusDmnSrv = "R";
                        /* Using cursor P004E8 */
                        pr_default.execute(6, new Object[] {A456ContagemResultado_Codigo});
                        while ( (pr_default.getStatus(6) != 101) )
                        {
                           A460ContagemResultado_PFBFM = P004E8_A460ContagemResultado_PFBFM[0];
                           n460ContagemResultado_PFBFM = P004E8_n460ContagemResultado_PFBFM[0];
                           A470ContagemResultado_ContadorFMCod = P004E8_A470ContagemResultado_ContadorFMCod[0];
                           A511ContagemResultado_HoraCnt = P004E8_A511ContagemResultado_HoraCnt[0];
                           A473ContagemResultado_DataCnt = P004E8_A473ContagemResultado_DataCnt[0];
                           AV34ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
                           if ( (0==AV40ContagemResultado_ContadorFMCod) )
                           {
                              AV40ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
                           }
                           /* Exit For each command. Update data (if necessary), close cursors & exit. */
                           if (true) break;
                           pr_default.readNext(6);
                        }
                        pr_default.close(6);
                     }
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(5);
                  }
                  pr_default.close(5);
               }
               if ( ! AV52Retorno && ( AV90ContratoServicosOS_Codigo > 0 ) )
               {
                  /*
                     INSERT RECORD ON TABLE ContagemResultado

                  */
                  A490ContagemResultado_ContratadaCod = AV31ContratadaSrv_Codigo;
                  n490ContagemResultado_ContratadaCod = false;
                  A602ContagemResultado_OSVinculada = AV19ContagemResultado_Codigo;
                  n602ContagemResultado_OSVinculada = false;
                  A457ContagemResultado_Demanda = StringUtil.Trim( AV8SDT_Demanda.gxTpr_Demanda) + AV44ServicoVnc_Inicial;
                  n457ContagemResultado_Demanda = false;
                  A471ContagemResultado_DataDmn = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
                  A489ContagemResultado_SistemaCod = AV8SDT_Demanda.gxTpr_Sistemacod;
                  n489ContagemResultado_SistemaCod = false;
                  if ( (0==AV8SDT_Demanda.gxTpr_Modulocod) )
                  {
                     A146Modulo_Codigo = 0;
                     n146Modulo_Codigo = false;
                     n146Modulo_Codigo = true;
                  }
                  else
                  {
                     A146Modulo_Codigo = AV8SDT_Demanda.gxTpr_Modulocod;
                     n146Modulo_Codigo = false;
                  }
                  A484ContagemResultado_StatusDmn = AV32StatusDmnSrv;
                  n484ContagemResultado_StatusDmn = false;
                  A465ContagemResultado_Link = AV8SDT_Demanda.gxTpr_Link;
                  n465ContagemResultado_Link = false;
                  A508ContagemResultado_Owner = AV11Usuario_Codigo;
                  A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
                  n1350ContagemResultado_DataCadastro = false;
                  A494ContagemResultado_Descricao = AV47ServicoVnc_Descricao + " " + StringUtil.Trim( AV8SDT_Demanda.gxTpr_Demanda) + ")";
                  n494ContagemResultado_Descricao = false;
                  A514ContagemResultado_Observacao = AV8SDT_Demanda.gxTpr_Observacao;
                  n514ContagemResultado_Observacao = false;
                  A512ContagemResultado_ValorPF = AV22ContagemResultado_ValorPF;
                  n512ContagemResultado_ValorPF = false;
                  A1046ContagemResultado_Agrupador = AV55Agrupador;
                  n1046ContagemResultado_Agrupador = false;
                  A1553ContagemResultado_CntSrvCod = AV90ContratoServicosOS_Codigo;
                  n1553ContagemResultado_CntSrvCod = false;
                  if ( ! AV21WWPContext.gxTpr_Userehadministradorgam && ( AV31ContratadaSrv_Codigo == AV21WWPContext.gxTpr_Contratada_codigo ) )
                  {
                     A454ContagemResultado_ContadorFSCod = AV11Usuario_Codigo;
                     n454ContagemResultado_ContadorFSCod = false;
                  }
                  else
                  {
                     A454ContagemResultado_ContadorFSCod = 0;
                     n454ContagemResultado_ContadorFSCod = false;
                     n454ContagemResultado_ContadorFSCod = true;
                  }
                  if ( (0==AV48ContratadaOrigem_Codigo) )
                  {
                     A805ContagemResultado_ContratadaOrigemCod = 0;
                     n805ContagemResultado_ContratadaOrigemCod = false;
                     n805ContagemResultado_ContratadaOrigemCod = true;
                  }
                  else
                  {
                     A805ContagemResultado_ContratadaOrigemCod = AV48ContratadaOrigem_Codigo;
                     n805ContagemResultado_ContratadaOrigemCod = false;
                  }
                  A493ContagemResultado_DemandaFM = "";
                  n493ContagemResultado_DemandaFM = false;
                  n493ContagemResultado_DemandaFM = true;
                  A468ContagemResultado_NaoCnfDmnCod = 0;
                  n468ContagemResultado_NaoCnfDmnCod = false;
                  n468ContagemResultado_NaoCnfDmnCod = true;
                  A597ContagemResultado_LoteAceiteCod = 0;
                  n597ContagemResultado_LoteAceiteCod = false;
                  n597ContagemResultado_LoteAceiteCod = true;
                  A890ContagemResultado_Responsavel = 0;
                  n890ContagemResultado_Responsavel = false;
                  n890ContagemResultado_Responsavel = true;
                  A1043ContagemResultado_LiqLogCod = 0;
                  n1043ContagemResultado_LiqLogCod = false;
                  n1043ContagemResultado_LiqLogCod = true;
                  A1443ContagemResultado_CntSrvPrrCod = 0;
                  n1443ContagemResultado_CntSrvPrrCod = false;
                  n1443ContagemResultado_CntSrvPrrCod = true;
                  A1583ContagemResultado_TipoRegistro = 1;
                  A1636ContagemResultado_ServicoSS = 0;
                  n1636ContagemResultado_ServicoSS = false;
                  n1636ContagemResultado_ServicoSS = true;
                  A485ContagemResultado_EhValidacao = false;
                  n485ContagemResultado_EhValidacao = false;
                  A598ContagemResultado_Baseline = false;
                  n598ContagemResultado_Baseline = false;
                  A1452ContagemResultado_SS = 0;
                  n1452ContagemResultado_SS = false;
                  A1515ContagemResultado_Evento = 1;
                  n1515ContagemResultado_Evento = false;
                  /* Using cursor P004E9 */
                  pr_default.execute(7, new Object[] {A471ContagemResultado_DataDmn, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n457ContagemResultado_Demanda, A457ContagemResultado_Demanda, n465ContagemResultado_Link, A465ContagemResultado_Link, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n146Modulo_Codigo, A146Modulo_Codigo, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, A508ContagemResultado_Owner, n512ContagemResultado_ValorPF, A512ContagemResultado_ValorPF, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1046ContagemResultado_Agrupador, A1046ContagemResultado_Agrupador, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1443ContagemResultado_CntSrvPrrCod, A1443ContagemResultado_CntSrvPrrCod, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, A1583ContagemResultado_TipoRegistro, n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS});
                  A456ContagemResultado_Codigo = P004E9_A456ContagemResultado_Codigo[0];
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                  if ( (pr_default.getStatus(7) == 1) )
                  {
                     context.Gx_err = 1;
                     Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
                  }
                  else
                  {
                     context.Gx_err = 0;
                     Gx_emsg = "";
                  }
                  /* End Insert */
                  AV39ContagemResultado_OSVinculada = A456ContagemResultado_Codigo;
                  context.CommitDataStores( "PRC_InserirSDTDemandas");
                  new prc_disparoservicovinculado(context ).execute(  AV39ContagemResultado_OSVinculada,  AV21WWPContext.gxTpr_Userid) ;
                  if ( StringUtil.StrCmp(AV32StatusDmnSrv, "R") == 0 )
                  {
                     new prc_cstuntprdnrm(context ).execute( ref  AV90ContratoServicosOS_Codigo, ref  AV40ContagemResultado_ContadorFMCod, ref  AV42ContagemResultado_CstUntPrd, out  AV88CalculoPFinal) ;
                     /*
                        INSERT RECORD ON TABLE ContagemResultadoContagens

                     */
                     A456ContagemResultado_Codigo = AV39ContagemResultado_OSVinculada;
                     A473ContagemResultado_DataCnt = AV8SDT_Demanda.gxTpr_Datacnt;
                     A511ContagemResultado_HoraCnt = context.localUtil.Time( );
                     A460ContagemResultado_PFBFM = AV34ContagemResultado_PFBFM;
                     n460ContagemResultado_PFBFM = false;
                     A461ContagemResultado_PFLFM = (decimal)(AV34ContagemResultado_PFBFM*AV45ServicoVnc_Percentual);
                     n461ContagemResultado_PFLFM = false;
                     A800ContagemResultado_Deflator = AV45ServicoVnc_Percentual;
                     n800ContagemResultado_Deflator = false;
                     A483ContagemResultado_StatusCnt = 5;
                     A517ContagemResultado_Ultima = true;
                     A470ContagemResultado_ContadorFMCod = AV40ContagemResultado_ContadorFMCod;
                     A833ContagemResultado_CstUntPrd = AV42ContagemResultado_CstUntPrd;
                     n833ContagemResultado_CstUntPrd = false;
                     A482ContagemResultadoContagens_Esforco = 0;
                     A462ContagemResultado_Divergencia = 0;
                     A469ContagemResultado_NaoCnfCntCod = 0;
                     n469ContagemResultado_NaoCnfCntCod = false;
                     n469ContagemResultado_NaoCnfCntCod = true;
                     BatchSize = 100;
                     pr_default.initializeBatch( 8, BatchSize, this, "Executebatchp004e10");
                     /* Using cursor P004E10 */
                     pr_default.addRecord(8, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd});
                     if ( pr_default.recordCount(8) == pr_default.getBatchSize(8) )
                     {
                        Executebatchp004e10( ) ;
                     }
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  }
               }
               AV17Linha = "OS " + StringUtil.Trim( AV8SDT_Demanda.gxTpr_Demandafm) + ", Ref. " + StringUtil.Trim( AV8SDT_Demanda.gxTpr_Demanda) + ", " + StringUtil.Trim( context.localUtil.DToC( AV8SDT_Demanda.gxTpr_Datadmn, 2, "/")) + ", " + StringUtil.Trim( AV8SDT_Demanda.gxTpr_Sistemanom);
               if ( AV54DataEntrega != AV8SDT_Demanda.gxTpr_Dataent )
               {
                  AV17Linha = AV17Linha + ", " + context.localUtil.TToC( AV8SDT_Demanda.gxTpr_Dataent, 8, 5, 0, 3, "/", ":", " ");
               }
               if ( ! (Convert.ToDecimal(0)==AV8SDT_Demanda.gxTpr_Pfbfs) )
               {
                  AV17Linha = AV17Linha + " PF FS (" + StringUtil.Trim( StringUtil.Str( AV8SDT_Demanda.gxTpr_Pfbfs, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( AV8SDT_Demanda.gxTpr_Pflfs, 14, 5)) + ")";
               }
               if ( ! AV46SrvParaFtr )
               {
                  if ( ! (Convert.ToDecimal(0)==AV8SDT_Demanda.gxTpr_Pfbfm) )
                  {
                     /*
                        INSERT RECORD ON TABLE ContagemResultadoContagens

                     */
                     A456ContagemResultado_Codigo = AV19ContagemResultado_Codigo;
                     A473ContagemResultado_DataCnt = AV8SDT_Demanda.gxTpr_Datacnt;
                     A511ContagemResultado_HoraCnt = context.localUtil.Time( );
                     A460ContagemResultado_PFBFM = AV8SDT_Demanda.gxTpr_Pfbfm;
                     n460ContagemResultado_PFBFM = false;
                     A461ContagemResultado_PFLFM = AV8SDT_Demanda.gxTpr_Pflfm;
                     n461ContagemResultado_PFLFM = false;
                     A458ContagemResultado_PFBFS = AV8SDT_Demanda.gxTpr_Pfbfs;
                     n458ContagemResultado_PFBFS = false;
                     A459ContagemResultado_PFLFS = AV8SDT_Demanda.gxTpr_Pflfs;
                     n459ContagemResultado_PFLFS = false;
                     A483ContagemResultado_StatusCnt = 5;
                     A800ContagemResultado_Deflator = AV33Servico_Percentual;
                     n800ContagemResultado_Deflator = false;
                     A517ContagemResultado_Ultima = true;
                     A470ContagemResultado_ContadorFMCod = AV8SDT_Demanda.gxTpr_Contadorfmcod;
                     A833ContagemResultado_CstUntPrd = AV8SDT_Demanda.gxTpr_Custocrfm;
                     n833ContagemResultado_CstUntPrd = false;
                     A482ContagemResultadoContagens_Esforco = 0;
                     A462ContagemResultado_Divergencia = 0;
                     A469ContagemResultado_NaoCnfCntCod = 0;
                     n469ContagemResultado_NaoCnfCntCod = false;
                     n469ContagemResultado_NaoCnfCntCod = true;
                     BatchSize = 100;
                     pr_default.initializeBatch( 9, BatchSize, this, "Executebatchp004e11");
                     /* Using cursor P004E11 */
                     pr_default.addRecord(9, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd});
                     if ( pr_default.recordCount(9) == pr_default.getBatchSize(9) )
                     {
                        Executebatchp004e11( ) ;
                     }
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                     AV17Linha = AV17Linha + " PF FM (" + StringUtil.Trim( StringUtil.Str( AV8SDT_Demanda.gxTpr_Pfbfm, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( AV8SDT_Demanda.gxTpr_Pflfm, 14, 5)) + ")";
                  }
               }
               else
               {
                  /*
                     INSERT RECORD ON TABLE ContagemResultadoContagens

                  */
                  A456ContagemResultado_Codigo = AV19ContagemResultado_Codigo;
                  if ( StringUtil.StrCmp(AV41DataSrvFat, "H") == 0 )
                  {
                     A473ContagemResultado_DataCnt = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, "DEFAULT"));
                  }
                  else
                  {
                     A473ContagemResultado_DataCnt = AV8SDT_Demanda.gxTpr_Datadmn;
                  }
                  A511ContagemResultado_HoraCnt = context.localUtil.Time( );
                  A460ContagemResultado_PFBFM = AV8SDT_Demanda.gxTpr_Pfbfm;
                  n460ContagemResultado_PFBFM = false;
                  A461ContagemResultado_PFLFM = (decimal)(AV8SDT_Demanda.gxTpr_Pfbfm*AV33Servico_Percentual);
                  n461ContagemResultado_PFLFM = false;
                  A458ContagemResultado_PFBFS = AV8SDT_Demanda.gxTpr_Pfbfs;
                  n458ContagemResultado_PFBFS = false;
                  A459ContagemResultado_PFLFS = AV8SDT_Demanda.gxTpr_Pflfs;
                  n459ContagemResultado_PFLFS = false;
                  A800ContagemResultado_Deflator = AV33Servico_Percentual;
                  n800ContagemResultado_Deflator = false;
                  A483ContagemResultado_StatusCnt = 5;
                  A517ContagemResultado_Ultima = true;
                  A470ContagemResultado_ContadorFMCod = AV8SDT_Demanda.gxTpr_Contadorfmcod;
                  A833ContagemResultado_CstUntPrd = AV8SDT_Demanda.gxTpr_Custocrfm;
                  n833ContagemResultado_CstUntPrd = false;
                  A482ContagemResultadoContagens_Esforco = 0;
                  A462ContagemResultado_Divergencia = 0;
                  A469ContagemResultado_NaoCnfCntCod = 0;
                  n469ContagemResultado_NaoCnfCntCod = false;
                  n469ContagemResultado_NaoCnfCntCod = true;
                  BatchSize = 100;
                  pr_default.initializeBatch( 10, BatchSize, this, "Executebatchp004e12");
                  /* Using cursor P004E12 */
                  pr_default.addRecord(10, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd});
                  if ( pr_default.recordCount(10) == pr_default.getBatchSize(10) )
                  {
                     Executebatchp004e12( ) ;
                  }
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                  AV17Linha = AV17Linha + " PF FM (" + StringUtil.Trim( StringUtil.Str( AV8SDT_Demanda.gxTpr_Pfbfm, 14, 5)) + ", " + StringUtil.Trim( StringUtil.Str( AV8SDT_Demanda.gxTpr_Pflfm, 14, 5)) + ")";
               }
               if ( AV52Retorno && ! AV53Updated )
               {
                  AV17Linha = AV17Linha + " Servi�o sem prazo de entrega!";
               }
               H4E0( false, 18) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV17Linha, "")), 17, Gx_line+0, 799, Gx_line+15, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+18);
               AV15i = (short)(AV15i+1);
               AV106GXV2 = (int)(AV106GXV2+1);
            }
            /* Execute user subroutine: 'SALVARANEXOS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV14Totais = StringUtil.Trim( StringUtil.Str( (decimal)(AV15i), 4, 0)) + " demandas de " + StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_Demandas.Count), 9, 0)) + " solicitadas, inseridas com sucesso";
            H4E0( false, 39) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 10, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV14Totais, "")), 42, Gx_line+17, 772, Gx_line+35, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+39);
            context.CommitDataStores( "PRC_InserirSDTDemandas");
            AV10WebSession.Remove("SDTDemandas");
            if ( AV60Notificar )
            {
               if ( AV58Usuarios.Count > 0 )
               {
                  /* Execute user subroutine: 'BUSCA.URL.SISTEMA' */
                  S131 ();
                  if ( returnInSub )
                  {
                     this.cleanup();
                     if (true) return;
                  }
                  AV57EmailText = AV57EmailText + "Foi inserido no sistema Meetrika um total de " + StringUtil.Trim( StringUtil.Str( (decimal)(AV15i), 4, 0)) + " demanda(s), referente ao servi�o de " + StringUtil.Trim( AV35Servico_Sigla) + ", na �rea de trabalho " + StringUtil.Trim( AV21WWPContext.gxTpr_Areatrabalho_descricao) + ", com os seguintes prazos definidos em contrato de SLA:" + StringUtil.NewLine( ) + StringUtil.NewLine( );
                  AV69PrazoAnalise = DateTimeUtil.ServerDate( context, "DEFAULT");
                  GXt_dtime2 = DateTimeUtil.ResetTime( AV69PrazoAnalise ) ;
                  GXt_dtime3 = DateTimeUtil.ResetTime( AV69PrazoAnalise ) ;
                  new prc_adddiasuteis(context ).execute(  GXt_dtime3,  AV82DiasAnalise,  AV87TipoDias, out  GXt_dtime2) ;
                  AV69PrazoAnalise = DateTimeUtil.ResetTime(GXt_dtime2);
                  AV57EmailText = AV57EmailText + "1) Prazo final para An�lise e aceite das demandas: " + context.localUtil.DToC( AV69PrazoAnalise, 2, "/") + StringUtil.NewLine( ) + StringUtil.NewLine( );
                  AV57EmailText = AV57EmailText + "2) Prazo final para a entrega dos servi�os executados: " + context.localUtil.TToC( AV54DataEntrega, 8, 5, 0, 3, "/", ":", " ") + StringUtil.NewLine( ) + StringUtil.NewLine( );
                  AV57EmailText = AV57EmailText + "Para atendimento da demanda acesse o sistema de gest�o de solicita��es <" + StringUtil.Trim( AV95ParametrosSistema_URLApp) + ">." + StringUtil.NewLine( ) + StringUtil.NewLine( );
                  AV57EmailText = AV57EmailText + "Para maiores informa��es ou detalhamentos a respeito de sua demanda, favor contatar o Gerente do Projeto " + AV68Preposto_Nome + StringUtil.NewLine( ) + StringUtil.NewLine( );
                  AV57EmailText = AV57EmailText + StringUtil.Trim( AV21WWPContext.gxTpr_Areatrabalho_descricao) + StringUtil.NewLine( ) + StringUtil.Trim( AV21WWPContext.gxTpr_Username) + StringUtil.NewLine( ) + StringUtil.NewLine( );
                  AV56Subject = StringUtil.Trim( AV21WWPContext.gxTpr_Areatrabalho_descricao) + " Lote " + StringUtil.Trim( AV55Agrupador) + " com " + StringUtil.Trim( StringUtil.Str( (decimal)(AV9SDT_Demandas.Count), 9, 0)) + " demandas importadas (No reply)";
                  AV56Subject = StringUtil.StringReplace( AV56Subject, "  ", " ");
                  AV10WebSession.Set("DemandaCodigo", AV85Codigos.ToXml(false, true, "Collection", ""));
                  new prc_enviaremail(context ).execute(  AV21WWPContext.gxTpr_Areatrabalho_codigo,  AV58Usuarios,  AV56Subject,  AV57EmailText,  AV59Attachments, ref  AV61Resultado) ;
               }
               else
               {
                  AV61Resultado = "N�o foi achado o usu�rio preposto no contrato para enviar a notifica��o!";
               }
               H4E0( false, 32) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV61Resultado, "")), 17, Gx_line+17, 799, Gx_line+32, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+32);
            }
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H4E0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'PRAZOATE' Routine */
         AV109GXV3 = 1;
         while ( AV109GXV3 <= AV9SDT_Demandas.Count )
         {
            AV8SDT_Demanda = ((SdtSDT_Demandas_Demanda)AV9SDT_Demandas.Item(AV109GXV3));
            if ( AV8SDT_Demanda.gxTpr_Dataent > AV54DataEntrega )
            {
               AV54DataEntrega = AV8SDT_Demanda.gxTpr_Dataent;
            }
            AV109GXV3 = (int)(AV109GXV3+1);
         }
      }

      protected void S121( )
      {
         /* 'SALVARANEXOS' Routine */
         if ( new prc_criaranexoscompartilhados(context).executeUdp(  0) )
         {
            AV10WebSession.Set("Codigos", AV85Codigos.ToXml(false, true, "Collection", ""));
            new prc_vincularanexoscompartilhadoscomos(context ).execute( ) ;
            AV10WebSession.Remove("Codigos");
         }
         AV10WebSession.Remove("Codigos");
      }

      protected void S131( )
      {
         /* 'BUSCA.URL.SISTEMA' Routine */
         AV95ParametrosSistema_URLApp = "";
         /* Using cursor P004E13 */
         pr_default.execute(11);
         while ( (pr_default.getStatus(11) != 101) )
         {
            A1679ParametrosSistema_URLApp = P004E13_A1679ParametrosSistema_URLApp[0];
            n1679ParametrosSistema_URLApp = P004E13_n1679ParametrosSistema_URLApp[0];
            A330ParametrosSistema_Codigo = P004E13_A330ParametrosSistema_Codigo[0];
            AV95ParametrosSistema_URLApp = A1679ParametrosSistema_URLApp;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(11);
         }
         pr_default.close(11);
      }

      protected void H4E0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText("P�gina", 675, Gx_line+0, 710, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 733, Gx_line+9, 826, Gx_line+24, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 675, Gx_line+9, 724, Gx_line+24, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV18Titulo, "")), 148, Gx_line+45, 670, Gx_line+60, 1+256, 0, 0, 1) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV55Agrupador, "@!")), 150, Gx_line+67, 229, Gx_line+82, 0+256, 0, 0, 1) ;
               getPrinter().GxDrawText(context.localUtil.Format( AV54DataEntrega, "99/99/99 99:99"), 150, Gx_line+83, 230, Gx_line+98, 0+256, 0, 0, 1) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Agrupador:", 17, Gx_line+67, 81, Gx_line+83, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Prazo de entrega at�:", 17, Gx_line+83, 140, Gx_line+99, 0+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 10, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV16Contratada_Nome, "@!")), 252, Gx_line+28, 566, Gx_line+46, 1+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 14, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("Importa��o de demandas solicitadas", 250, Gx_line+0, 569, Gx_line+25, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+107);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      protected void Executebatchp004e10( )
      {
         pr_default .setErrorBuffers( 8 ,
         new Object[] {
         P004E10_A456ContagemResultado_Codigo, P004E10_A473ContagemResultado_DataCnt, P004E10_A511ContagemResultado_HoraCnt, P004E10_A460ContagemResultado_PFBFM, P004E10_n460ContagemResultado_PFBFM, P004E10_A461ContagemResultado_PFLFM, P004E10_n461ContagemResultado_PFLFM, P004E10_A462ContagemResultado_Divergencia, P004E10_A470ContagemResultado_ContadorFMCod, P004E10_A469ContagemResultado_NaoCnfCntCod,
         P004E10_n469ContagemResultado_NaoCnfCntCod, P004E10_A483ContagemResultado_StatusCnt, P004E10_A482ContagemResultadoContagens_Esforco, P004E10_A517ContagemResultado_Ultima, P004E10_A800ContagemResultado_Deflator, P004E10_n800ContagemResultado_Deflator, P004E10_A833ContagemResultado_CstUntPrd, P004E10_n833ContagemResultado_CstUntPrd
         });
         /* Using cursor P004E10 */
         pr_default.executeBatch(8);
         if ( (pr_default.getStatus(8) == 1) )
         {
            while ( pr_default.readNextErrorRecord(8) == 1 )
            {
               A456ContagemResultado_Codigo = P004E10_A456ContagemResultado_Codigo[0];
               A473ContagemResultado_DataCnt = P004E10_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P004E10_A511ContagemResultado_HoraCnt[0];
               A460ContagemResultado_PFBFM = P004E10_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = P004E10_n460ContagemResultado_PFBFM[0];
               A461ContagemResultado_PFLFM = P004E10_A461ContagemResultado_PFLFM[0];
               n461ContagemResultado_PFLFM = P004E10_n461ContagemResultado_PFLFM[0];
               A462ContagemResultado_Divergencia = P004E10_A462ContagemResultado_Divergencia[0];
               A470ContagemResultado_ContadorFMCod = P004E10_A470ContagemResultado_ContadorFMCod[0];
               A469ContagemResultado_NaoCnfCntCod = P004E10_A469ContagemResultado_NaoCnfCntCod[0];
               n469ContagemResultado_NaoCnfCntCod = P004E10_n469ContagemResultado_NaoCnfCntCod[0];
               A483ContagemResultado_StatusCnt = P004E10_A483ContagemResultado_StatusCnt[0];
               A482ContagemResultadoContagens_Esforco = P004E10_A482ContagemResultadoContagens_Esforco[0];
               A517ContagemResultado_Ultima = P004E10_A517ContagemResultado_Ultima[0];
               A800ContagemResultado_Deflator = P004E10_A800ContagemResultado_Deflator[0];
               n800ContagemResultado_Deflator = P004E10_n800ContagemResultado_Deflator[0];
               A833ContagemResultado_CstUntPrd = P004E10_A833ContagemResultado_CstUntPrd[0];
               n833ContagemResultado_CstUntPrd = P004E10_n833ContagemResultado_CstUntPrd[0];
               /* Using cursor P004E14 */
               pr_default.execute(12, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd});
               pr_default.close(12);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               if ( (pr_default.getStatus(12) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
            }
         }
         pr_default.close(8);
      }

      protected void Executebatchp004e11( )
      {
         pr_default .setErrorBuffers( 9 ,
         new Object[] {
         P004E11_A456ContagemResultado_Codigo, P004E11_A473ContagemResultado_DataCnt, P004E11_A511ContagemResultado_HoraCnt, P004E11_A458ContagemResultado_PFBFS, P004E11_n458ContagemResultado_PFBFS, P004E11_A459ContagemResultado_PFLFS, P004E11_n459ContagemResultado_PFLFS, P004E11_A460ContagemResultado_PFBFM, P004E11_n460ContagemResultado_PFBFM, P004E11_A461ContagemResultado_PFLFM,
         P004E11_n461ContagemResultado_PFLFM, P004E11_A462ContagemResultado_Divergencia, P004E11_A470ContagemResultado_ContadorFMCod, P004E11_A469ContagemResultado_NaoCnfCntCod, P004E11_n469ContagemResultado_NaoCnfCntCod, P004E11_A483ContagemResultado_StatusCnt, P004E11_A482ContagemResultadoContagens_Esforco, P004E11_A517ContagemResultado_Ultima, P004E11_A800ContagemResultado_Deflator, P004E11_n800ContagemResultado_Deflator,
         P004E11_A833ContagemResultado_CstUntPrd, P004E11_n833ContagemResultado_CstUntPrd
         });
         /* Using cursor P004E11 */
         pr_default.executeBatch(9);
         if ( (pr_default.getStatus(9) == 1) )
         {
            while ( pr_default.readNextErrorRecord(9) == 1 )
            {
               A456ContagemResultado_Codigo = P004E11_A456ContagemResultado_Codigo[0];
               A473ContagemResultado_DataCnt = P004E11_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P004E11_A511ContagemResultado_HoraCnt[0];
               A458ContagemResultado_PFBFS = P004E11_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = P004E11_n458ContagemResultado_PFBFS[0];
               A459ContagemResultado_PFLFS = P004E11_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = P004E11_n459ContagemResultado_PFLFS[0];
               A460ContagemResultado_PFBFM = P004E11_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = P004E11_n460ContagemResultado_PFBFM[0];
               A461ContagemResultado_PFLFM = P004E11_A461ContagemResultado_PFLFM[0];
               n461ContagemResultado_PFLFM = P004E11_n461ContagemResultado_PFLFM[0];
               A462ContagemResultado_Divergencia = P004E11_A462ContagemResultado_Divergencia[0];
               A470ContagemResultado_ContadorFMCod = P004E11_A470ContagemResultado_ContadorFMCod[0];
               A469ContagemResultado_NaoCnfCntCod = P004E11_A469ContagemResultado_NaoCnfCntCod[0];
               n469ContagemResultado_NaoCnfCntCod = P004E11_n469ContagemResultado_NaoCnfCntCod[0];
               A483ContagemResultado_StatusCnt = P004E11_A483ContagemResultado_StatusCnt[0];
               A482ContagemResultadoContagens_Esforco = P004E11_A482ContagemResultadoContagens_Esforco[0];
               A517ContagemResultado_Ultima = P004E11_A517ContagemResultado_Ultima[0];
               A800ContagemResultado_Deflator = P004E11_A800ContagemResultado_Deflator[0];
               n800ContagemResultado_Deflator = P004E11_n800ContagemResultado_Deflator[0];
               A833ContagemResultado_CstUntPrd = P004E11_A833ContagemResultado_CstUntPrd[0];
               n833ContagemResultado_CstUntPrd = P004E11_n833ContagemResultado_CstUntPrd[0];
               /* Using cursor P004E15 */
               pr_default.execute(13, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd});
               pr_default.close(13);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               if ( (pr_default.getStatus(13) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
            }
         }
         pr_default.close(9);
      }

      protected void Executebatchp004e12( )
      {
         pr_default .setErrorBuffers( 10 ,
         new Object[] {
         P004E12_A456ContagemResultado_Codigo, P004E12_A473ContagemResultado_DataCnt, P004E12_A511ContagemResultado_HoraCnt, P004E12_A458ContagemResultado_PFBFS, P004E12_n458ContagemResultado_PFBFS, P004E12_A459ContagemResultado_PFLFS, P004E12_n459ContagemResultado_PFLFS, P004E12_A460ContagemResultado_PFBFM, P004E12_n460ContagemResultado_PFBFM, P004E12_A461ContagemResultado_PFLFM,
         P004E12_n461ContagemResultado_PFLFM, P004E12_A462ContagemResultado_Divergencia, P004E12_A470ContagemResultado_ContadorFMCod, P004E12_A469ContagemResultado_NaoCnfCntCod, P004E12_n469ContagemResultado_NaoCnfCntCod, P004E12_A483ContagemResultado_StatusCnt, P004E12_A482ContagemResultadoContagens_Esforco, P004E12_A517ContagemResultado_Ultima, P004E12_A800ContagemResultado_Deflator, P004E12_n800ContagemResultado_Deflator,
         P004E12_A833ContagemResultado_CstUntPrd, P004E12_n833ContagemResultado_CstUntPrd
         });
         /* Using cursor P004E12 */
         pr_default.executeBatch(10);
         if ( (pr_default.getStatus(10) == 1) )
         {
            while ( pr_default.readNextErrorRecord(10) == 1 )
            {
               A456ContagemResultado_Codigo = P004E12_A456ContagemResultado_Codigo[0];
               A473ContagemResultado_DataCnt = P004E12_A473ContagemResultado_DataCnt[0];
               A511ContagemResultado_HoraCnt = P004E12_A511ContagemResultado_HoraCnt[0];
               A458ContagemResultado_PFBFS = P004E12_A458ContagemResultado_PFBFS[0];
               n458ContagemResultado_PFBFS = P004E12_n458ContagemResultado_PFBFS[0];
               A459ContagemResultado_PFLFS = P004E12_A459ContagemResultado_PFLFS[0];
               n459ContagemResultado_PFLFS = P004E12_n459ContagemResultado_PFLFS[0];
               A460ContagemResultado_PFBFM = P004E12_A460ContagemResultado_PFBFM[0];
               n460ContagemResultado_PFBFM = P004E12_n460ContagemResultado_PFBFM[0];
               A461ContagemResultado_PFLFM = P004E12_A461ContagemResultado_PFLFM[0];
               n461ContagemResultado_PFLFM = P004E12_n461ContagemResultado_PFLFM[0];
               A462ContagemResultado_Divergencia = P004E12_A462ContagemResultado_Divergencia[0];
               A470ContagemResultado_ContadorFMCod = P004E12_A470ContagemResultado_ContadorFMCod[0];
               A469ContagemResultado_NaoCnfCntCod = P004E12_A469ContagemResultado_NaoCnfCntCod[0];
               n469ContagemResultado_NaoCnfCntCod = P004E12_n469ContagemResultado_NaoCnfCntCod[0];
               A483ContagemResultado_StatusCnt = P004E12_A483ContagemResultado_StatusCnt[0];
               A482ContagemResultadoContagens_Esforco = P004E12_A482ContagemResultadoContagens_Esforco[0];
               A517ContagemResultado_Ultima = P004E12_A517ContagemResultado_Ultima[0];
               A800ContagemResultado_Deflator = P004E12_A800ContagemResultado_Deflator[0];
               n800ContagemResultado_Deflator = P004E12_n800ContagemResultado_Deflator[0];
               A833ContagemResultado_CstUntPrd = P004E12_A833ContagemResultado_CstUntPrd[0];
               n833ContagemResultado_CstUntPrd = P004E12_n833ContagemResultado_CstUntPrd[0];
               /* Using cursor P004E16 */
               pr_default.execute(14, new Object[] {A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt, n458ContagemResultado_PFBFS, A458ContagemResultado_PFBFS, n459ContagemResultado_PFLFS, A459ContagemResultado_PFLFS, n460ContagemResultado_PFBFM, A460ContagemResultado_PFBFM, n461ContagemResultado_PFLFM, A461ContagemResultado_PFLFM, A462ContagemResultado_Divergencia, A470ContagemResultado_ContadorFMCod, n469ContagemResultado_NaoCnfCntCod, A469ContagemResultado_NaoCnfCntCod, A483ContagemResultado_StatusCnt, A482ContagemResultadoContagens_Esforco, A517ContagemResultado_Ultima, n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, n833ContagemResultado_CstUntPrd, A833ContagemResultado_CstUntPrd});
               pr_default.close(14);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               if ( (pr_default.getStatus(14) == 1) )
               {
                  context.Gx_err = 1;
                  Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
               }
               else
               {
                  context.Gx_err = 0;
                  Gx_emsg = "";
               }
               /* End Insert */
            }
         }
         pr_default.close(10);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(10);
         pr_default.close(9);
         pr_default.close(8);
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV21WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9SDT_Demandas = new GxObjectCollection( context, "SDT_Demandas.Demanda", "GxEv3Up14_Meetrika", "SdtSDT_Demandas_Demanda", "GeneXus.Programs");
         AV10WebSession = context.GetSession();
         scmdbuf = "";
         P004E2_A155Servico_Codigo = new int[1] ;
         P004E2_A39Contratada_Codigo = new int[1] ;
         P004E2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P004E2_A40Contratada_PessoaCod = new int[1] ;
         P004E2_A1016Contrato_PrepostoPesCod = new int[1] ;
         P004E2_n1016Contrato_PrepostoPesCod = new bool[] {false} ;
         P004E2_A29Contratante_Codigo = new int[1] ;
         P004E2_n29Contratante_Codigo = new bool[] {false} ;
         P004E2_A291Usuario_EhContratada = new bool[] {false} ;
         P004E2_n291Usuario_EhContratada = new bool[] {false} ;
         P004E2_A74Contrato_Codigo = new int[1] ;
         P004E2_A160ContratoServicos_Codigo = new int[1] ;
         P004E2_A557Servico_VlrUnidadeContratada = new decimal[1] ;
         P004E2_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P004E2_A605Servico_Sigla = new String[] {""} ;
         P004E2_A558Servico_Percentual = new decimal[1] ;
         P004E2_n558Servico_Percentual = new bool[] {false} ;
         P004E2_A41Contratada_PessoaNom = new String[] {""} ;
         P004E2_n41Contratada_PessoaNom = new bool[] {false} ;
         P004E2_A1015Contrato_PrepostoNom = new String[] {""} ;
         P004E2_n1015Contrato_PrepostoNom = new bool[] {false} ;
         P004E2_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P004E2_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         P004E2_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         P004E2_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         P004E2_A593Contratante_OSAutomatica = new bool[] {false} ;
         P004E2_A524Contratada_OS = new int[1] ;
         P004E2_n524Contratada_OS = new bool[] {false} ;
         P004E2_A1013Contrato_PrepostoCod = new int[1] ;
         P004E2_n1013Contrato_PrepostoCod = new bool[] {false} ;
         A605Servico_Sigla = "";
         A41Contratada_PessoaNom = "";
         A1015Contrato_PrepostoNom = "";
         A1454ContratoServicos_PrazoTpDias = "";
         AV35Servico_Sigla = "";
         AV18Titulo = "";
         AV16Contratada_Nome = "";
         AV68Preposto_Nome = "";
         AV87TipoDias = "";
         AV58Usuarios = new GxSimpleCollection();
         P004E3_A1078ContratoGestor_ContratoCod = new int[1] ;
         P004E3_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P004E4_A155Servico_Codigo = new int[1] ;
         P004E4_A74Contrato_Codigo = new int[1] ;
         P004E4_A160ContratoServicos_Codigo = new int[1] ;
         P004E4_A116Contrato_ValorUnidadeContratacao = new decimal[1] ;
         P004E4_A605Servico_Sigla = new String[] {""} ;
         P004E4_A558Servico_Percentual = new decimal[1] ;
         P004E4_n558Servico_Percentual = new bool[] {false} ;
         P004E4_A156Servico_Descricao = new String[] {""} ;
         P004E4_n156Servico_Descricao = new bool[] {false} ;
         A156Servico_Descricao = "";
         AV43ServicoVnc_Sigla = "";
         AV44ServicoVnc_Inicial = "";
         AV47ServicoVnc_Descricao = "";
         P004E5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         P004E5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         AV54DataEntrega = (DateTime)(DateTime.MinValue);
         AV57EmailText = "";
         AV8SDT_Demanda = new SdtSDT_Demandas_Demanda(context);
         AV94Contratada = new SdtContratada(context);
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A494ContagemResultado_Descricao = "";
         A514ContagemResultado_Observacao = "";
         A465ContagemResultado_Link = "";
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         A1046ContagemResultado_Agrupador = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         AV79Status = "";
         P004E6_A456ContagemResultado_Codigo = new int[1] ;
         Gx_emsg = "";
         AV32StatusDmnSrv = "";
         AV91DataInicio = (DateTime)(DateTime.MinValue);
         AV85Codigos = new GxSimpleCollection();
         P004E7_A456ContagemResultado_Codigo = new int[1] ;
         P004E7_A489ContagemResultado_SistemaCod = new int[1] ;
         P004E7_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P004E7_A457ContagemResultado_Demanda = new String[] {""} ;
         P004E7_n457ContagemResultado_Demanda = new bool[] {false} ;
         P004E7_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004E7_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004E8_A456ContagemResultado_Codigo = new int[1] ;
         P004E8_A460ContagemResultado_PFBFM = new decimal[1] ;
         P004E8_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P004E8_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P004E8_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P004E8_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         P004E9_A456ContagemResultado_Codigo = new int[1] ;
         AV88CalculoPFinal = "";
         P004E10_A456ContagemResultado_Codigo = new int[1] ;
         P004E10_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P004E10_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P004E10_A460ContagemResultado_PFBFM = new decimal[1] ;
         P004E10_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P004E10_A461ContagemResultado_PFLFM = new decimal[1] ;
         P004E10_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P004E10_A462ContagemResultado_Divergencia = new decimal[1] ;
         P004E10_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P004E10_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         P004E10_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         P004E10_A483ContagemResultado_StatusCnt = new short[1] ;
         P004E10_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P004E10_A517ContagemResultado_Ultima = new bool[] {false} ;
         P004E10_A800ContagemResultado_Deflator = new decimal[1] ;
         P004E10_n800ContagemResultado_Deflator = new bool[] {false} ;
         P004E10_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         P004E10_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         AV17Linha = "";
         P004E11_A456ContagemResultado_Codigo = new int[1] ;
         P004E11_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P004E11_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P004E11_A458ContagemResultado_PFBFS = new decimal[1] ;
         P004E11_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P004E11_A459ContagemResultado_PFLFS = new decimal[1] ;
         P004E11_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P004E11_A460ContagemResultado_PFBFM = new decimal[1] ;
         P004E11_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P004E11_A461ContagemResultado_PFLFM = new decimal[1] ;
         P004E11_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P004E11_A462ContagemResultado_Divergencia = new decimal[1] ;
         P004E11_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P004E11_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         P004E11_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         P004E11_A483ContagemResultado_StatusCnt = new short[1] ;
         P004E11_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P004E11_A517ContagemResultado_Ultima = new bool[] {false} ;
         P004E11_A800ContagemResultado_Deflator = new decimal[1] ;
         P004E11_n800ContagemResultado_Deflator = new bool[] {false} ;
         P004E11_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         P004E11_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         P004E12_A456ContagemResultado_Codigo = new int[1] ;
         P004E12_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P004E12_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P004E12_A458ContagemResultado_PFBFS = new decimal[1] ;
         P004E12_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P004E12_A459ContagemResultado_PFLFS = new decimal[1] ;
         P004E12_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P004E12_A460ContagemResultado_PFBFM = new decimal[1] ;
         P004E12_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P004E12_A461ContagemResultado_PFLFM = new decimal[1] ;
         P004E12_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P004E12_A462ContagemResultado_Divergencia = new decimal[1] ;
         P004E12_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P004E12_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         P004E12_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         P004E12_A483ContagemResultado_StatusCnt = new short[1] ;
         P004E12_A482ContagemResultadoContagens_Esforco = new short[1] ;
         P004E12_A517ContagemResultado_Ultima = new bool[] {false} ;
         P004E12_A800ContagemResultado_Deflator = new decimal[1] ;
         P004E12_n800ContagemResultado_Deflator = new bool[] {false} ;
         P004E12_A833ContagemResultado_CstUntPrd = new decimal[1] ;
         P004E12_n833ContagemResultado_CstUntPrd = new bool[] {false} ;
         AV14Totais = "";
         AV69PrazoAnalise = DateTime.MinValue;
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         GXt_dtime3 = (DateTime)(DateTime.MinValue);
         AV95ParametrosSistema_URLApp = "";
         AV56Subject = "";
         AV59Attachments = new GxSimpleCollection();
         AV61Resultado = "";
         P004E13_A1679ParametrosSistema_URLApp = new String[] {""} ;
         P004E13_n1679ParametrosSistema_URLApp = new bool[] {false} ;
         P004E13_A330ParametrosSistema_Codigo = new int[1] ;
         A1679ParametrosSistema_URLApp = "";
         Gx_time = "";
         Gx_date = DateTime.MinValue;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_inserirsdtdemandas__default(),
            new Object[][] {
                new Object[] {
               P004E2_A155Servico_Codigo, P004E2_A39Contratada_Codigo, P004E2_A52Contratada_AreaTrabalhoCod, P004E2_A40Contratada_PessoaCod, P004E2_A1016Contrato_PrepostoPesCod, P004E2_n1016Contrato_PrepostoPesCod, P004E2_A29Contratante_Codigo, P004E2_n29Contratante_Codigo, P004E2_A291Usuario_EhContratada, P004E2_n291Usuario_EhContratada,
               P004E2_A74Contrato_Codigo, P004E2_A160ContratoServicos_Codigo, P004E2_A557Servico_VlrUnidadeContratada, P004E2_A116Contrato_ValorUnidadeContratacao, P004E2_A605Servico_Sigla, P004E2_A558Servico_Percentual, P004E2_n558Servico_Percentual, P004E2_A41Contratada_PessoaNom, P004E2_n41Contratada_PessoaNom, P004E2_A1015Contrato_PrepostoNom,
               P004E2_n1015Contrato_PrepostoNom, P004E2_A1152ContratoServicos_PrazoAnalise, P004E2_n1152ContratoServicos_PrazoAnalise, P004E2_A1454ContratoServicos_PrazoTpDias, P004E2_n1454ContratoServicos_PrazoTpDias, P004E2_A593Contratante_OSAutomatica, P004E2_A524Contratada_OS, P004E2_n524Contratada_OS, P004E2_A1013Contrato_PrepostoCod, P004E2_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               P004E3_A1078ContratoGestor_ContratoCod, P004E3_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               P004E4_A155Servico_Codigo, P004E4_A74Contrato_Codigo, P004E4_A160ContratoServicos_Codigo, P004E4_A116Contrato_ValorUnidadeContratacao, P004E4_A605Servico_Sigla, P004E4_A558Servico_Percentual, P004E4_n558Servico_Percentual, P004E4_A156Servico_Descricao, P004E4_n156Servico_Descricao
               }
               , new Object[] {
               P004E5_A69ContratadaUsuario_UsuarioCod, P004E5_A66ContratadaUsuario_ContratadaCod
               }
               , new Object[] {
               P004E6_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P004E7_A456ContagemResultado_Codigo, P004E7_A489ContagemResultado_SistemaCod, P004E7_n489ContagemResultado_SistemaCod, P004E7_A457ContagemResultado_Demanda, P004E7_n457ContagemResultado_Demanda, P004E7_A484ContagemResultado_StatusDmn, P004E7_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               P004E8_A456ContagemResultado_Codigo, P004E8_A460ContagemResultado_PFBFM, P004E8_n460ContagemResultado_PFBFM, P004E8_A470ContagemResultado_ContadorFMCod, P004E8_A511ContagemResultado_HoraCnt, P004E8_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               P004E9_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P004E13_A1679ParametrosSistema_URLApp, P004E13_n1679ParametrosSistema_URLApp, P004E13_A330ParametrosSistema_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_date = DateTimeUtil.Today( context);
         Gx_time = context.localUtil.Time( );
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short AV82DiasAnalise ;
      private short AV15i ;
      private short A1515ContagemResultado_Evento ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short GXt_int1 ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short A483ContagemResultado_StatusCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private int AV12Contratada_Codigo ;
      private int AV11Usuario_Codigo ;
      private int AV31ContratadaSrv_Codigo ;
      private int AV48ContratadaOrigem_Codigo ;
      private int AV90ContratoServicosOS_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV64ContratoServicos_Codigo ;
      private int A155Servico_Codigo ;
      private int A39Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A40Contratada_PessoaCod ;
      private int A1016Contrato_PrepostoPesCod ;
      private int A29Contratante_Codigo ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A524Contratada_OS ;
      private int A1013Contrato_PrepostoCod ;
      private int AV89Contrato_Codigo ;
      private int AV93Contratada_OS ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int AV40ContagemResultado_ContadorFMCod ;
      private int AV105GXV1 ;
      private int AV106GXV2 ;
      private int GX_INS69 ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A489ContagemResultado_SistemaCod ;
      private int A146Modulo_Codigo ;
      private int A508ContagemResultado_Owner ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A1443ContagemResultado_CntSrvPrrCod ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A1452ContagemResultado_SS ;
      private int A456ContagemResultado_Codigo ;
      private int AV19ContagemResultado_Codigo ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int AV39ContagemResultado_OSVinculada ;
      private int GX_INS72 ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int BatchSize ;
      private int Gx_OldLine ;
      private int AV109GXV3 ;
      private int A330ParametrosSistema_Codigo ;
      private decimal A557Servico_VlrUnidadeContratada ;
      private decimal A116Contrato_ValorUnidadeContratacao ;
      private decimal A558Servico_Percentual ;
      private decimal AV22ContagemResultado_ValorPF ;
      private decimal AV33Servico_Percentual ;
      private decimal AV45ServicoVnc_Percentual ;
      private decimal A512ContagemResultado_ValorPF ;
      private decimal A798ContagemResultado_PFBFSImp ;
      private decimal A799ContagemResultado_PFLFSImp ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal AV34ContagemResultado_PFBFM ;
      private decimal AV42ContagemResultado_CstUntPrd ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A800ContagemResultado_Deflator ;
      private decimal A833ContagemResultado_CstUntPrd ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A459ContagemResultado_PFLFS ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV28StatusDmn ;
      private String AV41DataSrvFat ;
      private String AV55Agrupador ;
      private String scmdbuf ;
      private String A605Servico_Sigla ;
      private String A41Contratada_PessoaNom ;
      private String A1015Contrato_PrepostoNom ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String AV35Servico_Sigla ;
      private String AV18Titulo ;
      private String AV16Contratada_Nome ;
      private String AV68Preposto_Nome ;
      private String AV87TipoDias ;
      private String AV43ServicoVnc_Sigla ;
      private String AV44ServicoVnc_Inicial ;
      private String AV57EmailText ;
      private String A1046ContagemResultado_Agrupador ;
      private String A484ContagemResultado_StatusDmn ;
      private String AV79Status ;
      private String Gx_emsg ;
      private String AV32StatusDmnSrv ;
      private String A511ContagemResultado_HoraCnt ;
      private String AV88CalculoPFinal ;
      private String AV17Linha ;
      private String AV14Totais ;
      private String AV56Subject ;
      private String AV61Resultado ;
      private String Gx_time ;
      private DateTime AV66PrazoEntrega ;
      private DateTime AV54DataEntrega ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime AV91DataInicio ;
      private DateTime GXt_dtime2 ;
      private DateTime GXt_dtime3 ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime AV69PrazoAnalise ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool AV46SrvParaFtr ;
      private bool AV60Notificar ;
      private bool n1016Contrato_PrepostoPesCod ;
      private bool n29Contratante_Codigo ;
      private bool A291Usuario_EhContratada ;
      private bool n291Usuario_EhContratada ;
      private bool n558Servico_Percentual ;
      private bool n41Contratada_PessoaNom ;
      private bool n1015Contrato_PrepostoNom ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool A593Contratante_OSAutomatica ;
      private bool n524Contratada_OS ;
      private bool n1013Contrato_PrepostoCod ;
      private bool AV92OSAutomatica ;
      private bool n156Servico_Descricao ;
      private bool returnInSub ;
      private bool AV78TemContagem ;
      private bool AV52Retorno ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n494ContagemResultado_Descricao ;
      private bool n514ContagemResultado_Observacao ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n146Modulo_Codigo ;
      private bool n465ContagemResultado_Link ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n512ContagemResultado_ValorPF ;
      private bool n1046ContagemResultado_Agrupador ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n1515ContagemResultado_Evento ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n798ContagemResultado_PFBFSImp ;
      private bool n799ContagemResultado_PFLFSImp ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n1443ContagemResultado_CntSrvPrrCod ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool A598ContagemResultado_Baseline ;
      private bool n598ContagemResultado_Baseline ;
      private bool n1452ContagemResultado_SS ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n800ContagemResultado_Deflator ;
      private bool A517ContagemResultado_Ultima ;
      private bool n833ContagemResultado_CstUntPrd ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n459ContagemResultado_PFLFS ;
      private bool AV53Updated ;
      private bool n1679ParametrosSistema_URLApp ;
      private String A156Servico_Descricao ;
      private String A514ContagemResultado_Observacao ;
      private String A465ContagemResultado_Link ;
      private String AV47ServicoVnc_Descricao ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A494ContagemResultado_Descricao ;
      private String AV95ParametrosSistema_URLApp ;
      private String A1679ParametrosSistema_URLApp ;
      private IGxSession AV10WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P004E2_A155Servico_Codigo ;
      private int[] P004E2_A39Contratada_Codigo ;
      private int[] P004E2_A52Contratada_AreaTrabalhoCod ;
      private int[] P004E2_A40Contratada_PessoaCod ;
      private int[] P004E2_A1016Contrato_PrepostoPesCod ;
      private bool[] P004E2_n1016Contrato_PrepostoPesCod ;
      private int[] P004E2_A29Contratante_Codigo ;
      private bool[] P004E2_n29Contratante_Codigo ;
      private bool[] P004E2_A291Usuario_EhContratada ;
      private bool[] P004E2_n291Usuario_EhContratada ;
      private int[] P004E2_A74Contrato_Codigo ;
      private int[] P004E2_A160ContratoServicos_Codigo ;
      private decimal[] P004E2_A557Servico_VlrUnidadeContratada ;
      private decimal[] P004E2_A116Contrato_ValorUnidadeContratacao ;
      private String[] P004E2_A605Servico_Sigla ;
      private decimal[] P004E2_A558Servico_Percentual ;
      private bool[] P004E2_n558Servico_Percentual ;
      private String[] P004E2_A41Contratada_PessoaNom ;
      private bool[] P004E2_n41Contratada_PessoaNom ;
      private String[] P004E2_A1015Contrato_PrepostoNom ;
      private bool[] P004E2_n1015Contrato_PrepostoNom ;
      private short[] P004E2_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P004E2_n1152ContratoServicos_PrazoAnalise ;
      private String[] P004E2_A1454ContratoServicos_PrazoTpDias ;
      private bool[] P004E2_n1454ContratoServicos_PrazoTpDias ;
      private bool[] P004E2_A593Contratante_OSAutomatica ;
      private int[] P004E2_A524Contratada_OS ;
      private bool[] P004E2_n524Contratada_OS ;
      private int[] P004E2_A1013Contrato_PrepostoCod ;
      private bool[] P004E2_n1013Contrato_PrepostoCod ;
      private int[] P004E3_A1078ContratoGestor_ContratoCod ;
      private int[] P004E3_A1079ContratoGestor_UsuarioCod ;
      private int[] P004E4_A155Servico_Codigo ;
      private int[] P004E4_A74Contrato_Codigo ;
      private int[] P004E4_A160ContratoServicos_Codigo ;
      private decimal[] P004E4_A116Contrato_ValorUnidadeContratacao ;
      private String[] P004E4_A605Servico_Sigla ;
      private decimal[] P004E4_A558Servico_Percentual ;
      private bool[] P004E4_n558Servico_Percentual ;
      private String[] P004E4_A156Servico_Descricao ;
      private bool[] P004E4_n156Servico_Descricao ;
      private int[] P004E5_A69ContratadaUsuario_UsuarioCod ;
      private int[] P004E5_A66ContratadaUsuario_ContratadaCod ;
      private int[] P004E6_A456ContagemResultado_Codigo ;
      private int[] P004E7_A456ContagemResultado_Codigo ;
      private int[] P004E7_A489ContagemResultado_SistemaCod ;
      private bool[] P004E7_n489ContagemResultado_SistemaCod ;
      private String[] P004E7_A457ContagemResultado_Demanda ;
      private bool[] P004E7_n457ContagemResultado_Demanda ;
      private String[] P004E7_A484ContagemResultado_StatusDmn ;
      private bool[] P004E7_n484ContagemResultado_StatusDmn ;
      private int[] P004E8_A456ContagemResultado_Codigo ;
      private decimal[] P004E8_A460ContagemResultado_PFBFM ;
      private bool[] P004E8_n460ContagemResultado_PFBFM ;
      private int[] P004E8_A470ContagemResultado_ContadorFMCod ;
      private String[] P004E8_A511ContagemResultado_HoraCnt ;
      private DateTime[] P004E8_A473ContagemResultado_DataCnt ;
      private int[] P004E9_A456ContagemResultado_Codigo ;
      private int[] P004E10_A456ContagemResultado_Codigo ;
      private DateTime[] P004E10_A473ContagemResultado_DataCnt ;
      private String[] P004E10_A511ContagemResultado_HoraCnt ;
      private decimal[] P004E10_A460ContagemResultado_PFBFM ;
      private bool[] P004E10_n460ContagemResultado_PFBFM ;
      private decimal[] P004E10_A461ContagemResultado_PFLFM ;
      private bool[] P004E10_n461ContagemResultado_PFLFM ;
      private decimal[] P004E10_A462ContagemResultado_Divergencia ;
      private int[] P004E10_A470ContagemResultado_ContadorFMCod ;
      private int[] P004E10_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] P004E10_n469ContagemResultado_NaoCnfCntCod ;
      private short[] P004E10_A483ContagemResultado_StatusCnt ;
      private short[] P004E10_A482ContagemResultadoContagens_Esforco ;
      private bool[] P004E10_A517ContagemResultado_Ultima ;
      private decimal[] P004E10_A800ContagemResultado_Deflator ;
      private bool[] P004E10_n800ContagemResultado_Deflator ;
      private decimal[] P004E10_A833ContagemResultado_CstUntPrd ;
      private bool[] P004E10_n833ContagemResultado_CstUntPrd ;
      private int[] P004E11_A456ContagemResultado_Codigo ;
      private DateTime[] P004E11_A473ContagemResultado_DataCnt ;
      private String[] P004E11_A511ContagemResultado_HoraCnt ;
      private decimal[] P004E11_A458ContagemResultado_PFBFS ;
      private bool[] P004E11_n458ContagemResultado_PFBFS ;
      private decimal[] P004E11_A459ContagemResultado_PFLFS ;
      private bool[] P004E11_n459ContagemResultado_PFLFS ;
      private decimal[] P004E11_A460ContagemResultado_PFBFM ;
      private bool[] P004E11_n460ContagemResultado_PFBFM ;
      private decimal[] P004E11_A461ContagemResultado_PFLFM ;
      private bool[] P004E11_n461ContagemResultado_PFLFM ;
      private decimal[] P004E11_A462ContagemResultado_Divergencia ;
      private int[] P004E11_A470ContagemResultado_ContadorFMCod ;
      private int[] P004E11_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] P004E11_n469ContagemResultado_NaoCnfCntCod ;
      private short[] P004E11_A483ContagemResultado_StatusCnt ;
      private short[] P004E11_A482ContagemResultadoContagens_Esforco ;
      private bool[] P004E11_A517ContagemResultado_Ultima ;
      private decimal[] P004E11_A800ContagemResultado_Deflator ;
      private bool[] P004E11_n800ContagemResultado_Deflator ;
      private decimal[] P004E11_A833ContagemResultado_CstUntPrd ;
      private bool[] P004E11_n833ContagemResultado_CstUntPrd ;
      private int[] P004E12_A456ContagemResultado_Codigo ;
      private DateTime[] P004E12_A473ContagemResultado_DataCnt ;
      private String[] P004E12_A511ContagemResultado_HoraCnt ;
      private decimal[] P004E12_A458ContagemResultado_PFBFS ;
      private bool[] P004E12_n458ContagemResultado_PFBFS ;
      private decimal[] P004E12_A459ContagemResultado_PFLFS ;
      private bool[] P004E12_n459ContagemResultado_PFLFS ;
      private decimal[] P004E12_A460ContagemResultado_PFBFM ;
      private bool[] P004E12_n460ContagemResultado_PFBFM ;
      private decimal[] P004E12_A461ContagemResultado_PFLFM ;
      private bool[] P004E12_n461ContagemResultado_PFLFM ;
      private decimal[] P004E12_A462ContagemResultado_Divergencia ;
      private int[] P004E12_A470ContagemResultado_ContadorFMCod ;
      private int[] P004E12_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] P004E12_n469ContagemResultado_NaoCnfCntCod ;
      private short[] P004E12_A483ContagemResultado_StatusCnt ;
      private short[] P004E12_A482ContagemResultadoContagens_Esforco ;
      private bool[] P004E12_A517ContagemResultado_Ultima ;
      private decimal[] P004E12_A800ContagemResultado_Deflator ;
      private bool[] P004E12_n800ContagemResultado_Deflator ;
      private decimal[] P004E12_A833ContagemResultado_CstUntPrd ;
      private bool[] P004E12_n833ContagemResultado_CstUntPrd ;
      private String[] P004E13_A1679ParametrosSistema_URLApp ;
      private bool[] P004E13_n1679ParametrosSistema_URLApp ;
      private int[] P004E13_A330ParametrosSistema_Codigo ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV58Usuarios ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV85Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV59Attachments ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Demandas_Demanda ))]
      private IGxCollection AV9SDT_Demandas ;
      private SdtSDT_Demandas_Demanda AV8SDT_Demanda ;
      private wwpbaseobjects.SdtWWPContext AV21WWPContext ;
      private SdtContratada AV94Contratada ;
   }

   public class aprc_inserirsdtdemandas__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new BatchUpdateCursor(def[8])
         ,new BatchUpdateCursor(def[9])
         ,new BatchUpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new UpdateCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004E2 ;
          prmP004E2 = new Object[] {
          new Object[] {"@AV64ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004E3 ;
          prmP004E3 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@Usuario_EhContratada",SqlDbType.Bit,4,0}
          } ;
          Object[] prmP004E4 ;
          prmP004E4 = new Object[] {
          new Object[] {"@AV90ContratoServicosOS_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004E5 ;
          prmP004E5 = new Object[] {
          new Object[] {"@AV31ContratadaSrv_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11Usuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004E6 ;
          prmP004E6 = new Object[] {
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PFBFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFSImp",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_CntSrvPrrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP004E6 ;
          cmdBufferP004E6=" INSERT INTO [ContagemResultado]([ContagemResultado_DataDmn], [ContagemResultado_DataEntrega], [ContagemResultado_ContadorFSCod], [ContagemResultado_Demanda], [ContagemResultado_Link], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_StatusDmn], [ContagemResultado_EhValidacao], [ContagemResultado_ContratadaCod], [ContagemResultado_DemandaFM], [ContagemResultado_Descricao], [ContagemResultado_SistemaCod], [Modulo_Codigo], [ContagemResultado_Observacao], [ContagemResultado_Owner], [ContagemResultado_ValorPF], [ContagemResultado_LoteAceiteCod], [ContagemResultado_Baseline], [ContagemResultado_OSVinculada], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_Responsavel], [ContagemResultado_HoraEntrega], [ContagemResultado_LiqLogCod], [ContagemResultado_Agrupador], [ContagemResultado_PrazoInicialDias], [ContagemResultado_DataCadastro], [ContagemResultado_DataPrevista], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_SS], [ContagemResultado_Evento], [ContagemResultado_CntSrvCod], [ContagemResultado_TipoRegistro], [ContagemResultado_ServicoSS], [ContagemResultado_Evidencia], [ContagemResultado_FncUsrCod], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_OSManual], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrPrz], [ContagemResultado_CntSrvPrrCst], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], "
          + " [ContagemResultado_FimExc], [ContagemResultado_InicioCrr], [ContagemResultado_FimCrr], [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_VlrAceite], [ContagemResultado_UOOwner], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal], [ContagemResultado_QuantidadeSolicitada]) VALUES(@ContagemResultado_DataDmn, @ContagemResultado_DataEntrega, @ContagemResultado_ContadorFSCod, @ContagemResultado_Demanda, @ContagemResultado_Link, @ContagemResultado_NaoCnfDmnCod, @ContagemResultado_StatusDmn, @ContagemResultado_EhValidacao, @ContagemResultado_ContratadaCod, @ContagemResultado_DemandaFM, @ContagemResultado_Descricao, @ContagemResultado_SistemaCod, @Modulo_Codigo, @ContagemResultado_Observacao, @ContagemResultado_Owner, @ContagemResultado_ValorPF, @ContagemResultado_LoteAceiteCod, @ContagemResultado_Baseline, @ContagemResultado_OSVinculada, @ContagemResultado_PFBFSImp, @ContagemResultado_PFLFSImp, @ContagemResultado_ContratadaOrigemCod, @ContagemResultado_Responsavel, @ContagemResultado_HoraEntrega, @ContagemResultado_LiqLogCod, @ContagemResultado_Agrupador, @ContagemResultado_PrazoInicialDias, @ContagemResultado_DataCadastro, @ContagemResultado_DataPrevista, @ContagemResultado_CntSrvPrrCod, @ContagemResultado_SS, @ContagemResultado_Evento, @ContagemResultado_CntSrvCod, @ContagemResultado_TipoRegistro, @ContagemResultado_ServicoSS, '', convert(int, 0), convert( DATETIME, '17530101', 112 ), '', convert(int,"
          + " 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), '', '', '', convert(bit, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0)); SELECT SCOPE_IDENTITY()" ;
          Object[] prmP004E7 ;
          prmP004E7 = new Object[] {
          new Object[] {"@AV8SDT_Demanda__Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV8SDT_Demanda__Sistemacod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004E8 ;
          prmP004E8 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004E9 ;
          prmP004E9 = new Object[] {
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Demanda",SqlDbType.VarChar,30,0} ,
          new Object[] {"@ContagemResultado_Link",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ValorPF",SqlDbType.Decimal,18,5} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Agrupador",SqlDbType.Char,15,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_CntSrvPrrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP004E9 ;
          cmdBufferP004E9=" INSERT INTO [ContagemResultado]([ContagemResultado_DataDmn], [ContagemResultado_ContadorFSCod], [ContagemResultado_Demanda], [ContagemResultado_Link], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_StatusDmn], [ContagemResultado_EhValidacao], [ContagemResultado_ContratadaCod], [ContagemResultado_DemandaFM], [ContagemResultado_Descricao], [ContagemResultado_SistemaCod], [Modulo_Codigo], [ContagemResultado_Observacao], [ContagemResultado_Owner], [ContagemResultado_ValorPF], [ContagemResultado_LoteAceiteCod], [ContagemResultado_Baseline], [ContagemResultado_OSVinculada], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_Responsavel], [ContagemResultado_LiqLogCod], [ContagemResultado_Agrupador], [ContagemResultado_DataCadastro], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_SS], [ContagemResultado_Evento], [ContagemResultado_CntSrvCod], [ContagemResultado_TipoRegistro], [ContagemResultado_ServicoSS], [ContagemResultado_DataEntrega], [ContagemResultado_Evidencia], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_HoraEntrega], [ContagemResultado_FncUsrCod], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_OSManual], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoInicialDias], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_DataPrevista], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrPrz], [ContagemResultado_CntSrvPrrCst], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], "
          + " [ContagemResultado_FimExc], [ContagemResultado_InicioCrr], [ContagemResultado_FimCrr], [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_VlrAceite], [ContagemResultado_UOOwner], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal], [ContagemResultado_QuantidadeSolicitada]) VALUES(@ContagemResultado_DataDmn, @ContagemResultado_ContadorFSCod, @ContagemResultado_Demanda, @ContagemResultado_Link, @ContagemResultado_NaoCnfDmnCod, @ContagemResultado_StatusDmn, @ContagemResultado_EhValidacao, @ContagemResultado_ContratadaCod, @ContagemResultado_DemandaFM, @ContagemResultado_Descricao, @ContagemResultado_SistemaCod, @Modulo_Codigo, @ContagemResultado_Observacao, @ContagemResultado_Owner, @ContagemResultado_ValorPF, @ContagemResultado_LoteAceiteCod, @ContagemResultado_Baseline, @ContagemResultado_OSVinculada, @ContagemResultado_ContratadaOrigemCod, @ContagemResultado_Responsavel, @ContagemResultado_LiqLogCod, @ContagemResultado_Agrupador, @ContagemResultado_DataCadastro, @ContagemResultado_CntSrvPrrCod, @ContagemResultado_SS, @ContagemResultado_Evento, @ContagemResultado_CntSrvCod, @ContagemResultado_TipoRegistro, @ContagemResultado_ServicoSS, convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert(int,"
          + " 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), '', '', '', convert(bit, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0)); SELECT SCOPE_IDENTITY()" ;
          Object[] prmP004E10 ;
          prmP004E10 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP004E11 ;
          prmP004E11 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP004E12 ;
          prmP004E12 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP004E13 ;
          prmP004E13 = new Object[] {
          } ;
          Object[] prmP004E14 ;
          prmP004E14 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP004E15 ;
          prmP004E15 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5}
          } ;
          Object[] prmP004E16 ;
          prmP004E16 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0} ,
          new Object[] {"@ContagemResultado_PFBFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFS",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFBFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_PFLFM",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContagemResultado_Divergencia",SqlDbType.Decimal,6,2} ,
          new Object[] {"@ContagemResultado_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfCntCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultadoContagens_Esforco",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_CstUntPrd",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004E2", "SELECT TOP 1 T1.[Servico_Codigo], T3.[Contratada_Codigo], T4.[Contratada_AreaTrabalhoCod] AS Contratada_AreaTrabalhoCod, T4.[Contratada_PessoaCod] AS Contratada_PessoaCod, T8.[Usuario_PessoaCod] AS Contrato_PrepostoPesCod, T5.[Contratante_Codigo], T8.[Usuario_EhContratada], T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[Servico_VlrUnidadeContratada], T3.[Contrato_ValorUnidadeContratacao], T2.[Servico_Sigla], T1.[Servico_Percentual], T7.[Pessoa_Nome] AS Contratada_PessoaNom, T9.[Pessoa_Nome] AS Contrato_PrepostoNom, T1.[ContratoServicos_PrazoAnalise], T1.[ContratoServicos_PrazoTpDias], T6.[Contratante_OSAutomatica], T4.[Contratada_OS], T3.[Contrato_PrepostoCod] AS Contrato_PrepostoCod FROM (((((((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[Contratada_Codigo]) INNER JOIN [AreaTrabalho] T5 WITH (NOLOCK) ON T5.[AreaTrabalho_Codigo] = T4.[Contratada_AreaTrabalhoCod]) LEFT JOIN [Contratante] T6 WITH (NOLOCK) ON T6.[Contratante_Codigo] = T5.[Contratante_Codigo]) INNER JOIN [Pessoa] T7 WITH (NOLOCK) ON T7.[Pessoa_Codigo] = T4.[Contratada_PessoaCod]) LEFT JOIN [Usuario] T8 WITH (NOLOCK) ON T8.[Usuario_Codigo] = T3.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T9 WITH (NOLOCK) ON T9.[Pessoa_Codigo] = T8.[Usuario_PessoaCod]) WHERE T1.[ContratoServicos_Codigo] = @AV64ContratoServicos_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004E2,1,0,true,true )
             ,new CursorDef("P004E3", "SELECT [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE ([ContratoGestor_ContratoCod] = @Contrato_Codigo) AND (@Usuario_EhContratada = 1) ORDER BY [ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004E3,100,0,false,false )
             ,new CursorDef("P004E4", "SELECT TOP 1 T1.[Servico_Codigo], T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T3.[Contrato_ValorUnidadeContratacao], T2.[Servico_Sigla], T1.[Servico_Percentual], T2.[Servico_Descricao] FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo]) INNER JOIN [Contrato] T3 WITH (NOLOCK) ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE T1.[ContratoServicos_Codigo] = @AV90ContratoServicosOS_Codigo ORDER BY T1.[ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004E4,1,0,false,true )
             ,new CursorDef("P004E5", "SELECT TOP 1 [ContratadaUsuario_UsuarioCod], [ContratadaUsuario_ContratadaCod] FROM [ContratadaUsuario] WITH (NOLOCK) WHERE [ContratadaUsuario_ContratadaCod] = @AV31ContratadaSrv_Codigo and [ContratadaUsuario_UsuarioCod] = @AV11Usuario_Codigo ORDER BY [ContratadaUsuario_ContratadaCod], [ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004E5,1,0,false,true )
             ,new CursorDef("P004E6", cmdBufferP004E6, GxErrorMask.GX_NOMASK,prmP004E6)
             ,new CursorDef("P004E7", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_SistemaCod], [ContagemResultado_Demanda], [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_Demanda] = RTRIM(LTRIM(@AV8SDT_Demanda__Demanda))) AND ([ContagemResultado_SistemaCod] = @AV8SDT_Demanda__Sistemacod) ORDER BY [ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004E7,1,0,true,true )
             ,new CursorDef("P004E8", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_PFBFM], [ContagemResultado_ContadorFMCod], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultado_DataCnt] DESC, [ContagemResultado_HoraCnt] DESC ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004E8,1,0,false,true )
             ,new CursorDef("P004E9", cmdBufferP004E9, GxErrorMask.GX_NOMASK,prmP004E9)
             ,new CursorDef("P004E10", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_TimeCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_ParecerTcn], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), '', CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP004E10)
             ,new CursorDef("P004E11", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_TimeCnt], [ContagemResultado_ParecerTcn], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, convert( DATETIME, '17530101', 112 ), '', CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP004E11)
             ,new CursorDef("P004E12", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_TimeCnt], [ContagemResultado_ParecerTcn], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, convert( DATETIME, '17530101', 112 ), '', CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP004E12)
             ,new CursorDef("P004E13", "SELECT TOP 1 [ParametrosSistema_URLApp], [ParametrosSistema_Codigo] FROM [ParametrosSistema] WITH (NOLOCK) ORDER BY [ParametrosSistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004E13,1,0,false,true )
             ,new CursorDef("P004E14", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_TimeCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_ParecerTcn], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), '', CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP004E14)
             ,new CursorDef("P004E15", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_TimeCnt], [ContagemResultado_ParecerTcn], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, convert( DATETIME, '17530101', 112 ), '', CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP004E15)
             ,new CursorDef("P004E16", "INSERT INTO [ContagemResultadoContagens]([ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_PFBFS], [ContagemResultado_PFLFS], [ContagemResultado_PFBFM], [ContagemResultado_PFLFM], [ContagemResultado_Divergencia], [ContagemResultado_ContadorFMCod], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco], [ContagemResultado_Ultima], [ContagemResultado_Deflator], [ContagemResultado_CstUntPrd], [ContagemResultado_TimeCnt], [ContagemResultado_ParecerTcn], [ContagemResultado_Planilha], [ContagemResultado_NomePla], [ContagemResultado_TipoPla], [ContagemResultadoContagens_Prazo], [ContagemResultado_NvlCnt]) VALUES(@ContagemResultado_Codigo, @ContagemResultado_DataCnt, @ContagemResultado_HoraCnt, @ContagemResultado_PFBFS, @ContagemResultado_PFLFS, @ContagemResultado_PFBFM, @ContagemResultado_PFLFM, @ContagemResultado_Divergencia, @ContagemResultado_ContadorFMCod, @ContagemResultado_NaoCnfCntCod, @ContagemResultado_StatusCnt, @ContagemResultadoContagens_Esforco, @ContagemResultado_Ultima, @ContagemResultado_Deflator, @ContagemResultado_CstUntPrd, convert( DATETIME, '17530101', 112 ), '', CONVERT(varbinary(1), ''), '', '', convert( DATETIME, '17530101', 112 ), convert(int, 0))", GxErrorMask.GX_NOMASK,prmP004E16)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((bool[]) buf[8])[0] = rslt.getBool(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((decimal[]) buf[12])[0] = rslt.getDecimal(10) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(11) ;
                ((String[]) buf[14])[0] = rslt.getString(12, 15) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(13);
                ((String[]) buf[17])[0] = rslt.getString(14, 100) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(14);
                ((String[]) buf[19])[0] = rslt.getString(15, 100) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(15);
                ((short[]) buf[21])[0] = rslt.getShort(16) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(16);
                ((String[]) buf[23])[0] = rslt.getString(17, 1) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(17);
                ((bool[]) buf[25])[0] = rslt.getBool(18) ;
                ((int[]) buf[26])[0] = rslt.getInt(19) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(19);
                ((int[]) buf[28])[0] = rslt.getInt(20) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(20);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 15) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 5) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public override void getErrorResults( int cursor ,
                                          IFieldGetter rslt ,
                                          Object[] buf )
    {
       switch ( cursor )
       {
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((int[]) buf[9])[0] = rslt.getInt(8) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((short[]) buf[11])[0] = rslt.getShort(9) ;
                ((short[]) buf[12])[0] = rslt.getShort(10) ;
                ((bool[]) buf[13])[0] = rslt.getBool(11) ;
                ((decimal[]) buf[14])[0] = rslt.getDecimal(12) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(12);
                ((decimal[]) buf[16])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(13);
                break;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((short[]) buf[15])[0] = rslt.getShort(11) ;
                ((short[]) buf[16])[0] = rslt.getShort(12) ;
                ((bool[]) buf[17])[0] = rslt.getBool(13) ;
                ((decimal[]) buf[18])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(14);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(15);
                break;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(10);
                ((short[]) buf[15])[0] = rslt.getShort(11) ;
                ((short[]) buf[16])[0] = rslt.getShort(12) ;
                ((bool[]) buf[17])[0] = rslt.getBool(13) ;
                ((decimal[]) buf[18])[0] = rslt.getDecimal(14) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(14);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(15) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(15);
                break;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(2, (bool)parms[2]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(2, (DateTime)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(7, (String)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(8, (bool)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(11, (String)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(13, (int)parms[24]);
                }
                if ( (bool)parms[25] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[26]);
                }
                stmt.SetParameter(15, (int)parms[27]);
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(16, (decimal)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 18 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(18, (bool)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 20 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(20, (decimal)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 21 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(21, (decimal)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 22 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(22, (int)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 24 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(24, (DateTime)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 26 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(26, (String)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 27 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(27, (short)parms[51]);
                }
                if ( (bool)parms[52] )
                {
                   stmt.setNull( 28 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(28, (DateTime)parms[53]);
                }
                if ( (bool)parms[54] )
                {
                   stmt.setNull( 29 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(29, (DateTime)parms[55]);
                }
                if ( (bool)parms[56] )
                {
                   stmt.setNull( 30 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(30, (int)parms[57]);
                }
                if ( (bool)parms[58] )
                {
                   stmt.setNull( 31 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(31, (int)parms[59]);
                }
                if ( (bool)parms[60] )
                {
                   stmt.setNull( 32 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(32, (short)parms[61]);
                }
                if ( (bool)parms[62] )
                {
                   stmt.setNull( 33 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(33, (int)parms[63]);
                }
                stmt.SetParameter(34, (short)parms[64]);
                if ( (bool)parms[65] )
                {
                   stmt.setNull( 35 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(35, (int)parms[66]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(2, (int)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(7, (bool)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[18]);
                }
                if ( (bool)parms[19] )
                {
                   stmt.setNull( 11 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(11, (int)parms[20]);
                }
                if ( (bool)parms[21] )
                {
                   stmt.setNull( 12 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(12, (int)parms[22]);
                }
                if ( (bool)parms[23] )
                {
                   stmt.setNull( 13 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[24]);
                }
                stmt.SetParameter(14, (int)parms[25]);
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[27]);
                }
                if ( (bool)parms[28] )
                {
                   stmt.setNull( 16 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(16, (int)parms[29]);
                }
                if ( (bool)parms[30] )
                {
                   stmt.setNull( 17 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(17, (bool)parms[31]);
                }
                if ( (bool)parms[32] )
                {
                   stmt.setNull( 18 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(18, (int)parms[33]);
                }
                if ( (bool)parms[34] )
                {
                   stmt.setNull( 19 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(19, (int)parms[35]);
                }
                if ( (bool)parms[36] )
                {
                   stmt.setNull( 20 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(20, (int)parms[37]);
                }
                if ( (bool)parms[38] )
                {
                   stmt.setNull( 21 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(21, (int)parms[39]);
                }
                if ( (bool)parms[40] )
                {
                   stmt.setNull( 22 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(22, (String)parms[41]);
                }
                if ( (bool)parms[42] )
                {
                   stmt.setNull( 23 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(23, (DateTime)parms[43]);
                }
                if ( (bool)parms[44] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[45]);
                }
                if ( (bool)parms[46] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[47]);
                }
                if ( (bool)parms[48] )
                {
                   stmt.setNull( 26 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(26, (short)parms[49]);
                }
                if ( (bool)parms[50] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[51]);
                }
                stmt.SetParameter(28, (short)parms[52]);
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 29 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(29, (int)parms[54]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                stmt.SetParameter(6, (decimal)parms[7]);
                stmt.SetParameter(7, (int)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[10]);
                }
                stmt.SetParameter(9, (short)parms[11]);
                stmt.SetParameter(10, (short)parms[12]);
                stmt.SetParameter(11, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[17]);
                }
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                stmt.SetParameter(9, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[14]);
                }
                stmt.SetParameter(11, (short)parms[15]);
                stmt.SetParameter(12, (short)parms[16]);
                stmt.SetParameter(13, (bool)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[21]);
                }
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                stmt.SetParameter(9, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[14]);
                }
                stmt.SetParameter(11, (short)parms[15]);
                stmt.SetParameter(12, (short)parms[16]);
                stmt.SetParameter(13, (bool)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[21]);
                }
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                stmt.SetParameter(6, (decimal)parms[7]);
                stmt.SetParameter(7, (int)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[10]);
                }
                stmt.SetParameter(9, (short)parms[11]);
                stmt.SetParameter(10, (short)parms[12]);
                stmt.SetParameter(11, (bool)parms[13]);
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 12 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(12, (decimal)parms[15]);
                }
                if ( (bool)parms[16] )
                {
                   stmt.setNull( 13 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(13, (decimal)parms[17]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                stmt.SetParameter(9, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[14]);
                }
                stmt.SetParameter(11, (short)parms[15]);
                stmt.SetParameter(12, (short)parms[16]);
                stmt.SetParameter(13, (bool)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[21]);
                }
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 4 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(4, (decimal)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(6, (decimal)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(7, (decimal)parms[10]);
                }
                stmt.SetParameter(8, (decimal)parms[11]);
                stmt.SetParameter(9, (int)parms[12]);
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 10 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(10, (int)parms[14]);
                }
                stmt.SetParameter(11, (short)parms[15]);
                stmt.SetParameter(12, (short)parms[16]);
                stmt.SetParameter(13, (bool)parms[17]);
                if ( (bool)parms[18] )
                {
                   stmt.setNull( 14 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(14, (decimal)parms[19]);
                }
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 15 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(15, (decimal)parms[21]);
                }
                return;
       }
    }

 }

}
