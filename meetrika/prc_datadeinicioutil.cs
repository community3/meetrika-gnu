/*
               File: PRC_DataDeInicioUtil
        Description: Data De Inicio Util
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:9:44.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_datadeinicioutil : GXProcedure
   {
      public prc_datadeinicioutil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_datadeinicioutil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( ref DateTime aP0_DataInicio )
      {
         this.AV9DataInicio = aP0_DataInicio;
         initialize();
         executePrivate();
         aP0_DataInicio=this.AV9DataInicio;
      }

      public DateTime executeUdp( )
      {
         this.AV9DataInicio = aP0_DataInicio;
         initialize();
         executePrivate();
         aP0_DataInicio=this.AV9DataInicio;
         return AV9DataInicio ;
      }

      public void executeSubmit( ref DateTime aP0_DataInicio )
      {
         prc_datadeinicioutil objprc_datadeinicioutil;
         objprc_datadeinicioutil = new prc_datadeinicioutil();
         objprc_datadeinicioutil.AV9DataInicio = aP0_DataInicio;
         objprc_datadeinicioutil.context.SetSubmitInitialConfig(context);
         objprc_datadeinicioutil.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_datadeinicioutil);
         aP0_DataInicio=this.AV9DataInicio;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_datadeinicioutil)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8Amanha = DateTimeUtil.TAdd( AV9DataInicio, 86400*(1));
         if ( ( DateTimeUtil.Dow( AV8Amanha) == 1 ) || ( DateTimeUtil.Dow( AV8Amanha) == 7 ) || new prc_ehferiado(context).executeUdp( ref  AV8Amanha) )
         {
            GXt_dtime1 = AV9DataInicio;
            new prc_adddiasuteis(context ).execute(  AV9DataInicio,  1,  "U", out  GXt_dtime1) ;
            AV9DataInicio = GXt_dtime1;
            AV9DataInicio = DateTimeUtil.TAdd( AV9DataInicio, 86400*(-1));
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8Amanha = (DateTime)(DateTime.MinValue);
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private DateTime AV9DataInicio ;
      private DateTime AV8Amanha ;
      private DateTime GXt_dtime1 ;
      private DateTime aP0_DataInicio ;
   }

}
