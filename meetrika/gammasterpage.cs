/*
               File: GAMMasterPage
        Description: GAMMaster Page
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/27/2020 10:40:4.3
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gammasterpage : GXMasterPage, System.Web.SessionState.IRequiresSessionState
   {
      public gammasterpage( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public gammasterpage( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         initialize_properties( ) ;
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            PA1U2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS1U2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE1U2( ) ;
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         getDataAreaObject().RenderHtmlHeaders();
      }

      protected void RenderHtmlOpenForm( )
      {
         getDataAreaObject().RenderHtmlOpenForm();
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm1U2( )
      {
         SendCloseFormHiddens( ) ;
         getDataAreaObject().RenderHtmlCloseForm();
         if ( ! ( WebComp_Wclogout == null ) )
         {
            WebComp_Wclogout.componentjscripts();
         }
         if ( ! ( WebComp_Wcmenu == null ) )
         {
            WebComp_Wcmenu.componentjscripts();
         }
         context.AddJavascriptSource("gammasterpage.js", "?20203271040411");
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
      }

      public override String GetPgmname( )
      {
         return "GAMMasterPage" ;
      }

      public override String GetPgmdesc( )
      {
         return "GAMMaster Page" ;
      }

      protected void WB1U0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            if ( ! ShowMPWhenPopUp( ) && context.isPopUpObject( ) )
            {
               /* Content placeholder */
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gx-content-placeholder");
               context.WriteHtmlText( ">") ;
               getDataAreaObject().RenderHtmlContent();
               context.WriteHtmlText( "</div>") ;
               wbLoad = true;
               return  ;
            }
            wb_table1_2_1U2( true) ;
         }
         else
         {
            wb_table1_2_1U2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_1U2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START1U2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( StringUtil.StrCmp(WebComp_Wclogout_Component, "") == 0 )
            {
               WebComp_Wclogout = getWebComponent(GetType(), "GeneXus.Programs", "gamlogout", new Object[] {context} );
               WebComp_Wclogout.ComponentInit();
               WebComp_Wclogout.Name = "GAMLogout";
               WebComp_Wclogout_Component = "GAMLogout";
            }
            WebComp_Wclogout.componentrestorestate("MPW0008", "");
            if ( StringUtil.StrCmp(WebComp_Wcmenu_Component, "") == 0 )
            {
               WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
               WebComp_Wcmenu.ComponentInit();
               WebComp_Wcmenu.Name = "GAMMenu";
               WebComp_Wcmenu_Component = "GAMMenu";
            }
            WebComp_Wcmenu.componentrestorestate("MPW0011", "");
         }
         wbErr = false;
         STRUP1U0( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( getDataAreaObject().ExecuteStartEvent() != 0 )
            {
               setAjaxCallMode();
            }
         }
      }

      protected void WS1U2( )
      {
         START1U2( ) ;
         EVT1U2( ) ;
      }

      protected void EVT1U2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111U2 */
                           E111U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121U2 */
                           E121U2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                              }
                              dynload_actions( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  else if ( StringUtil.StrCmp(sEvtType, "M") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-2));
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-6));
                     nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                     if ( nCmpId == 8 )
                     {
                        WebComp_Wclogout = getWebComponent(GetType(), "GeneXus.Programs", "gamlogout", new Object[] {context} );
                        WebComp_Wclogout.ComponentInit();
                        WebComp_Wclogout.Name = "GAMLogout";
                        WebComp_Wclogout_Component = "GAMLogout";
                        WebComp_Wclogout.componentprocess("MPW0008", "", sEvt);
                     }
                     else if ( nCmpId == 11 )
                     {
                        WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
                        WebComp_Wcmenu.ComponentInit();
                        WebComp_Wcmenu.Name = "GAMMenu";
                        WebComp_Wcmenu_Component = "GAMMenu";
                        WebComp_Wcmenu.componentprocess("MPW0011", "", sEvt);
                     }
                  }
                  if ( context.wbHandled == 0 )
                  {
                     getDataAreaObject().DispatchEvents();
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE1U2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm1U2( ) ;
            }
         }
      }

      protected void PA1U2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF1U2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF1U2( )
      {
         if ( ShowMPWhenPopUp( ) || ! context.isPopUpObject( ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
            {
               if ( StringUtil.StrCmp(WebComp_Wclogout_Component, "") == 0 )
               {
                  WebComp_Wclogout = getWebComponent(GetType(), "GeneXus.Programs", "gamlogout", new Object[] {context} );
                  WebComp_Wclogout.ComponentInit();
                  WebComp_Wclogout.Name = "GAMLogout";
                  WebComp_Wclogout_Component = "GAMLogout";
               }
               if ( ! context.isAjaxRequest( ) )
               {
                  WebComp_Wclogout.setjustcreated();
               }
               WebComp_Wclogout.componentprepare(new Object[] {(String)"MPW0008",(String)""});
               WebComp_Wclogout.componentbind(new Object[] {});
               if ( 1 != 0 )
               {
                  WebComp_Wclogout.componentstart();
               }
            }
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
            {
               if ( StringUtil.StrCmp(WebComp_Wcmenu_Component, "") == 0 )
               {
                  WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
                  WebComp_Wcmenu.ComponentInit();
                  WebComp_Wcmenu.Name = "GAMMenu";
                  WebComp_Wcmenu_Component = "GAMMenu";
               }
               if ( ! context.isAjaxRequest( ) )
               {
                  WebComp_Wcmenu.setjustcreated();
               }
               WebComp_Wcmenu.componentprepare(new Object[] {(String)"MPW0011",(String)""});
               WebComp_Wcmenu.componentbind(new Object[] {});
               if ( 1 != 0 )
               {
                  WebComp_Wcmenu.componentstart();
               }
            }
            fix_multi_value_controls( ) ;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E121U2 */
            E121U2 ();
            WB1U0( ) ;
         }
      }

      protected void STRUP1U0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111U2 */
         E111U2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            if ( StringUtil.StrCmp(WebComp_Wclogout_Component, "") == 0 )
            {
               WebComp_Wclogout = getWebComponent(GetType(), "GeneXus.Programs", "gamlogout", new Object[] {context} );
               WebComp_Wclogout.ComponentInit();
               WebComp_Wclogout.Name = "GAMLogout";
               WebComp_Wclogout_Component = "GAMLogout";
            }
            WebComp_Wclogout.componentrestorestate("MPW0008", "");
            if ( StringUtil.StrCmp(WebComp_Wcmenu_Component, "") == 0 )
            {
               WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
               WebComp_Wcmenu.ComponentInit();
               WebComp_Wcmenu.Name = "GAMMenu";
               WebComp_Wcmenu_Component = "GAMMenu";
            }
            WebComp_Wcmenu.componentrestorestate("MPW0011", "");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E111U2 */
         E111U2 ();
         if (returnInSub) return;
      }

      protected void E111U2( )
      {
         /* Start Routine */
         if ( new SdtGAMRepository(context).checkpermission("is_gam_administrator") )
         {
         }
         else
         {
            context.wjLoc = formatLink("gamexamplenotauthorized.aspx") ;
            context.wjLocDisableFrm = 1;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E121U2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_1U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblpage_Internalname, tblTblpage_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\" background=\""+context.convertURL( context.GetImagePath( "f4970f78-b136-449b-a430-398466720ac5", "", context.GetTheme( )))+"\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top;width:190px")+"\" class='Table'>") ;
            wb_table2_5_1U2( true) ;
         }
         else
         {
            wb_table2_5_1U2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_1U2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Table'>") ;
            /* Content placeholder */
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-content-placeholder");
            context.WriteHtmlText( ">") ;
            getDataAreaObject().RenderHtmlContent();
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1U2e( true) ;
         }
         else
         {
            wb_table1_2_1U2e( false) ;
         }
      }

      protected void wb_table2_5_1U2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblwc_Internalname, tblTblwc_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;height:9px")+"\" class='Table'>") ;
            /* WebComponent */
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent");
            context.WriteHtmlText( " id=\""+"gxHTMLWrpMPW0008"+""+"\""+"") ;
            context.WriteHtmlText( ">") ;
            if ( ! context.isAjaxRequest( ) )
            {
               context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0008"+"");
            }
            WebComp_Wclogout.componentdraw();
            if ( ! context.isAjaxRequest( ) )
            {
               context.httpAjaxContext.ajax_rspEndCmp();
            }
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\" class='Table'>") ;
            /* WebComponent */
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent");
            context.WriteHtmlText( " id=\""+"gxHTMLWrpMPW0011"+""+"\""+"") ;
            context.WriteHtmlText( ">") ;
            if ( ! context.isAjaxRequest( ) )
            {
               context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0011"+"");
            }
            WebComp_Wcmenu.componentdraw();
            if ( ! context.isAjaxRequest( ) )
            {
               context.httpAjaxContext.ajax_rspEndCmp();
            }
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1U2e( true) ;
         }
         else
         {
            wb_table2_5_1U2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1U2( ) ;
         WS1U2( ) ;
         WE1U2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void master_styles( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( StringUtil.StrCmp(WebComp_Wclogout_Component, "") == 0 )
         {
            WebComp_Wclogout = getWebComponent(GetType(), "GeneXus.Programs", "gamlogout", new Object[] {context} );
            WebComp_Wclogout.ComponentInit();
            WebComp_Wclogout.Name = "GAMLogout";
            WebComp_Wclogout_Component = "GAMLogout";
         }
         if ( ! ( WebComp_Wclogout == null ) )
         {
            WebComp_Wclogout.componentthemes();
         }
         if ( StringUtil.StrCmp(WebComp_Wcmenu_Component, "") == 0 )
         {
            WebComp_Wcmenu = getWebComponent(GetType(), "GeneXus.Programs", "gammenu", new Object[] {context} );
            WebComp_Wcmenu.ComponentInit();
            WebComp_Wcmenu.Name = "GAMMenu";
            WebComp_Wcmenu_Component = "GAMMenu";
         }
         if ( ! ( WebComp_Wcmenu == null ) )
         {
            WebComp_Wcmenu.componentthemes();
         }
         idxLst = 1;
         while ( idxLst <= (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)(getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Jscriptsrc.Item(idxLst))), "?20203271040491");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("gammasterpage.js", "?20203271040493");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         tblTblwc_Internalname = "TBLWC_MPAGE";
         tblTblpage_Internalname = "TBLPAGE_MPAGE";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Internalname = "FORM_MPAGE";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         Contholder1.setDataArea(getDataAreaObject());
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Contholder1 = new GXDataAreaControl();
         GXKey = "";
         sPrefix = "";
         WebComp_Wclogout_Component = "";
         WebComp_Wcmenu_Component = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         sStyleString = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sDynURL = "";
         Form = new GXWebForm();
         WebComp_Wclogout = new GeneXus.Http.GXNullWebComponent();
         WebComp_Wcmenu = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short initialized ;
      private short GxWebError ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGotPars ;
      private short nGXWrapped ;
      private int idxLst ;
      private String GXKey ;
      private String sPrefix ;
      private String WebComp_Wclogout_Component ;
      private String WebComp_Wcmenu_Component ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sStyleString ;
      private String tblTblpage_Internalname ;
      private String tblTblwc_Internalname ;
      private String sDynURL ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool toggleJsOutput ;
      private bool returnInSub ;
      private GXWebComponent WebComp_Wclogout ;
      private GXWebComponent WebComp_Wcmenu ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXDataAreaControl Contholder1 ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
   }

}
