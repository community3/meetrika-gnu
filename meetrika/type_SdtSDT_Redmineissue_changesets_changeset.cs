/*
               File: type_SdtSDT_Redmineissue_changesets_changeset
        Description: SDT_Redmineissue
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/18/2020 21:37:15.38
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_Redmineissue.changesets.changeset" )]
   [XmlType(TypeName =  "SDT_Redmineissue.changesets.changeset" , Namespace = "" )]
   [Serializable]
   public class SdtSDT_Redmineissue_changesets_changeset : GxUserType
   {
      public SdtSDT_Redmineissue_changesets_changeset( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_Redmineissue_changesets_changeset_Comments = "";
         gxTv_SdtSDT_Redmineissue_changesets_changeset_Committed_on = "";
      }

      public SdtSDT_Redmineissue_changesets_changeset( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_Redmineissue_changesets_changeset deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType());
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_Redmineissue_changesets_changeset)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_Redmineissue_changesets_changeset obj ;
         obj = this;
         obj.gxTpr_Revision = deserialized.gxTpr_Revision;
         obj.gxTpr_Comments = deserialized.gxTpr_Comments;
         obj.gxTpr_Committed_on = deserialized.gxTpr_Committed_on;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         if ( oReader.ExistsAttribute("revision") == 1 )
         {
            gxTv_SdtSDT_Redmineissue_changesets_changeset_Revision = (short)(NumberUtil.Val( oReader.GetAttributeByName("revision"), "."));
            if ( GXSoapError > 0 )
            {
               readOk = 1;
            }
         }
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "comments") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_changesets_changeset_Comments = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "committed_on") && ( oReader.NodeType != 2 ) && ( StringUtil.StrCmp(oReader.NamespaceURI, "") == 0 ) )
               {
                  gxTv_SdtSDT_Redmineissue_changesets_changeset_Committed_on = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_Redmineissue.changesets.changeset";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteAttribute("revision", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_Redmineissue_changesets_changeset_Revision), 4, 0)));
         oWriter.WriteElement("comments", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_changesets_changeset_Comments));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteElement("committed_on", StringUtil.RTrim( gxTv_SdtSDT_Redmineissue_changesets_changeset_Committed_on));
         if ( StringUtil.StrCmp(sNameSpace, "") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("revision", gxTv_SdtSDT_Redmineissue_changesets_changeset_Revision, false);
         AddObjectProperty("comments", gxTv_SdtSDT_Redmineissue_changesets_changeset_Comments, false);
         AddObjectProperty("committed_on", gxTv_SdtSDT_Redmineissue_changesets_changeset_Committed_on, false);
         return  ;
      }

      [SoapAttribute( AttributeName = "revision" )]
      [XmlAttribute( AttributeName = "revision" )]
      public short gxTpr_Revision
      {
         get {
            return gxTv_SdtSDT_Redmineissue_changesets_changeset_Revision ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_changesets_changeset_Revision = (short)(value);
         }

      }

      [  SoapElement( ElementName = "comments" )]
      [  XmlElement( ElementName = "comments" , Namespace = ""  )]
      public String gxTpr_Comments
      {
         get {
            return gxTv_SdtSDT_Redmineissue_changesets_changeset_Comments ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_changesets_changeset_Comments = (String)(value);
         }

      }

      [  SoapElement( ElementName = "committed_on" )]
      [  XmlElement( ElementName = "committed_on" , Namespace = ""  )]
      public String gxTpr_Committed_on
      {
         get {
            return gxTv_SdtSDT_Redmineissue_changesets_changeset_Committed_on ;
         }

         set {
            gxTv_SdtSDT_Redmineissue_changesets_changeset_Committed_on = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_Redmineissue_changesets_changeset_Comments = "";
         gxTv_SdtSDT_Redmineissue_changesets_changeset_Committed_on = "";
         sTagName = "";
         return  ;
      }

      protected short gxTv_SdtSDT_Redmineissue_changesets_changeset_Revision ;
      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtSDT_Redmineissue_changesets_changeset_Comments ;
      protected String gxTv_SdtSDT_Redmineissue_changesets_changeset_Committed_on ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_Redmineissue.changesets.changeset", Namespace = "")]
   public class SdtSDT_Redmineissue_changesets_changeset_RESTInterface : GxGenericCollectionItem<SdtSDT_Redmineissue_changesets_changeset>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_Redmineissue_changesets_changeset_RESTInterface( ) : base()
      {
      }

      public SdtSDT_Redmineissue_changesets_changeset_RESTInterface( SdtSDT_Redmineissue_changesets_changeset psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "revision" , Order = 0 )]
      public Nullable<short> gxTpr_Revision
      {
         get {
            return sdt.gxTpr_Revision ;
         }

         set {
            sdt.gxTpr_Revision = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "comments" , Order = 1 )]
      public String gxTpr_Comments
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Comments) ;
         }

         set {
            sdt.gxTpr_Comments = (String)(value);
         }

      }

      [DataMember( Name = "committed_on" , Order = 2 )]
      public String gxTpr_Committed_on
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Committed_on) ;
         }

         set {
            sdt.gxTpr_Committed_on = (String)(value);
         }

      }

      public SdtSDT_Redmineissue_changesets_changeset sdt
      {
         get {
            return (SdtSDT_Redmineissue_changesets_changeset)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_Redmineissue_changesets_changeset() ;
         }
      }

   }

}
