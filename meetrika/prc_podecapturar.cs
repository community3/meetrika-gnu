/*
               File: PRC_PodeCapturar
        Description: Pode Capturar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:13:43.40
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_podecapturar : GXProcedure
   {
      public prc_podecapturar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_podecapturar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_UserId ,
                           out bool aP1_PodeCapturar )
      {
         this.AV9UserId = aP0_UserId;
         this.AV8PodeCapturar = false ;
         initialize();
         executePrivate();
         aP1_PodeCapturar=this.AV8PodeCapturar;
      }

      public bool executeUdp( int aP0_UserId )
      {
         this.AV9UserId = aP0_UserId;
         this.AV8PodeCapturar = false ;
         initialize();
         executePrivate();
         aP1_PodeCapturar=this.AV8PodeCapturar;
         return AV8PodeCapturar ;
      }

      public void executeSubmit( int aP0_UserId ,
                                 out bool aP1_PodeCapturar )
      {
         prc_podecapturar objprc_podecapturar;
         objprc_podecapturar = new prc_podecapturar();
         objprc_podecapturar.AV9UserId = aP0_UserId;
         objprc_podecapturar.AV8PodeCapturar = false ;
         objprc_podecapturar.context.SetSubmitInitialConfig(context);
         objprc_podecapturar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_podecapturar);
         aP1_PodeCapturar=this.AV8PodeCapturar;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_podecapturar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8PodeCapturar = true;
         /* Using cursor P00X52 */
         pr_default.execute(0, new Object[] {AV9UserId});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A890ContagemResultado_Responsavel = P00X52_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P00X52_n890ContagemResultado_Responsavel[0];
            A912ContagemResultado_HoraEntrega = P00X52_A912ContagemResultado_HoraEntrega[0];
            n912ContagemResultado_HoraEntrega = P00X52_n912ContagemResultado_HoraEntrega[0];
            A472ContagemResultado_DataEntrega = P00X52_A472ContagemResultado_DataEntrega[0];
            n472ContagemResultado_DataEntrega = P00X52_n472ContagemResultado_DataEntrega[0];
            A484ContagemResultado_StatusDmn = P00X52_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00X52_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = P00X52_A456ContagemResultado_Codigo[0];
            if ( A472ContagemResultado_DataEntrega <= DateTimeUtil.ServerDate( context, "DEFAULT") )
            {
               if ( DateTimeUtil.Hour( A912ContagemResultado_HoraEntrega) >= 0 )
               {
                  AV8PodeCapturar = false;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00X52_A890ContagemResultado_Responsavel = new int[1] ;
         P00X52_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P00X52_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         P00X52_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         P00X52_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         P00X52_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         P00X52_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00X52_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P00X52_A456ContagemResultado_Codigo = new int[1] ;
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         A484ContagemResultado_StatusDmn = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_podecapturar__default(),
            new Object[][] {
                new Object[] {
               P00X52_A890ContagemResultado_Responsavel, P00X52_n890ContagemResultado_Responsavel, P00X52_A912ContagemResultado_HoraEntrega, P00X52_n912ContagemResultado_HoraEntrega, P00X52_A472ContagemResultado_DataEntrega, P00X52_n472ContagemResultado_DataEntrega, P00X52_A484ContagemResultado_StatusDmn, P00X52_n484ContagemResultado_StatusDmn, P00X52_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9UserId ;
      private int A890ContagemResultado_Responsavel ;
      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private bool AV8PodeCapturar ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n484ContagemResultado_StatusDmn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00X52_A890ContagemResultado_Responsavel ;
      private bool[] P00X52_n890ContagemResultado_Responsavel ;
      private DateTime[] P00X52_A912ContagemResultado_HoraEntrega ;
      private bool[] P00X52_n912ContagemResultado_HoraEntrega ;
      private DateTime[] P00X52_A472ContagemResultado_DataEntrega ;
      private bool[] P00X52_n472ContagemResultado_DataEntrega ;
      private String[] P00X52_A484ContagemResultado_StatusDmn ;
      private bool[] P00X52_n484ContagemResultado_StatusDmn ;
      private int[] P00X52_A456ContagemResultado_Codigo ;
      private bool aP1_PodeCapturar ;
   }

   public class prc_podecapturar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00X52 ;
          prmP00X52 = new Object[] {
          new Object[] {"@AV9UserId",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00X52", "SELECT [ContagemResultado_Responsavel], [ContagemResultado_HoraEntrega], [ContagemResultado_DataEntrega], [ContagemResultado_StatusDmn], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ([ContagemResultado_StatusDmn] = 'D') AND ([ContagemResultado_Responsavel] = @AV9UserId) ORDER BY [ContagemResultado_StatusDmn], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_Responsavel] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00X52,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
