/*
               File: WWServicoAns
        Description:  Servico Ans
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:37:35.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwservicoans : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwservicoans( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwservicoans( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_93 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_93_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_93_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ServicoAns_item1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoAns_item1", AV17ServicoAns_item1);
               AV40Servico_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Servico_Nome1", AV40Servico_Nome1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV22ServicoAns_item2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ServicoAns_item2", AV22ServicoAns_item2);
               AV41Servico_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Servico_Nome2", AV41Servico_Nome2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV27ServicoAns_item3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ServicoAns_item3", AV27ServicoAns_item3);
               AV42Servico_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Servico_Nome3", AV42Servico_Nome3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV46TFServico_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFServico_Nome", AV46TFServico_Nome);
               AV47TFServico_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFServico_Nome_Sel", AV47TFServico_Nome_Sel);
               AV50TFServicoAns_Item = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFServicoAns_Item", AV50TFServicoAns_Item);
               AV51TFServicoAns_Item_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFServicoAns_Item_Sel", AV51TFServicoAns_Item_Sel);
               AV34ManageFiltersExecutionStep = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV34ManageFiltersExecutionStep), 1, 0));
               AV48ddo_Servico_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Servico_NomeTitleControlIdToReplace", AV48ddo_Servico_NomeTitleControlIdToReplace);
               AV52ddo_ServicoAns_ItemTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ServicoAns_ItemTitleControlIdToReplace", AV52ddo_ServicoAns_ItemTitleControlIdToReplace);
               AV79Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               A319ServicoAns_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A155Servico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoAns_item1, AV40Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ServicoAns_item2, AV41Servico_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ServicoAns_item3, AV42Servico_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV46TFServico_Nome, AV47TFServico_Nome_Sel, AV50TFServicoAns_Item, AV51TFServicoAns_Item_Sel, AV34ManageFiltersExecutionStep, AV48ddo_Servico_NomeTitleControlIdToReplace, AV52ddo_ServicoAns_ItemTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A319ServicoAns_Codigo, A155Servico_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA7O2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START7O2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117373580");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwservicoans.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOANS_ITEM1", AV17ServicoAns_item1);
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_NOME1", StringUtil.RTrim( AV40Servico_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOANS_ITEM2", AV22ServicoAns_item2);
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_NOME2", StringUtil.RTrim( AV41Servico_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICOANS_ITEM3", AV27ServicoAns_item3);
         GxWebStd.gx_hidden_field( context, "GXH_vSERVICO_NOME3", StringUtil.RTrim( AV42Servico_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_NOME", StringUtil.RTrim( AV46TFServico_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_NOME_SEL", StringUtil.RTrim( AV47TFServico_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOANS_ITEM", AV50TFServicoAns_Item);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICOANS_ITEM_SEL", AV51TFServicoAns_Item_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_93", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_93), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMANAGEFILTERSDATA", AV38ManageFiltersData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMANAGEFILTERSDATA", AV38ManageFiltersData);
         }
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV53DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV53DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICO_NOMETITLEFILTERDATA", AV45Servico_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICO_NOMETITLEFILTERDATA", AV45Servico_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICOANS_ITEMTITLEFILTERDATA", AV49ServicoAns_ItemTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICOANS_ITEMTITLEFILTERDATA", AV49ServicoAns_ItemTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV79Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Icon", StringUtil.RTrim( Ddo_managefilters_Icon));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Caption", StringUtil.RTrim( Ddo_managefilters_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Tooltip", StringUtil.RTrim( Ddo_managefilters_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Cls", StringUtil.RTrim( Ddo_managefilters_Cls));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Caption", StringUtil.RTrim( Ddo_servico_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Tooltip", StringUtil.RTrim( Ddo_servico_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Cls", StringUtil.RTrim( Ddo_servico_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_servico_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_servico_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_servico_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_servico_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_servico_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filtertype", StringUtil.RTrim( Ddo_servico_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_servico_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_servico_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalisttype", StringUtil.RTrim( Ddo_servico_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalistproc", StringUtil.RTrim( Ddo_servico_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servico_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortasc", StringUtil.RTrim( Ddo_servico_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Sortdsc", StringUtil.RTrim( Ddo_servico_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Loadingdata", StringUtil.RTrim( Ddo_servico_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_servico_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_servico_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_servico_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Caption", StringUtil.RTrim( Ddo_servicoans_item_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Tooltip", StringUtil.RTrim( Ddo_servicoans_item_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Cls", StringUtil.RTrim( Ddo_servicoans_item_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Filteredtext_set", StringUtil.RTrim( Ddo_servicoans_item_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Selectedvalue_set", StringUtil.RTrim( Ddo_servicoans_item_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Dropdownoptionstype", StringUtil.RTrim( Ddo_servicoans_item_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servicoans_item_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Includesortasc", StringUtil.BoolToStr( Ddo_servicoans_item_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Includesortdsc", StringUtil.BoolToStr( Ddo_servicoans_item_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Sortedstatus", StringUtil.RTrim( Ddo_servicoans_item_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Includefilter", StringUtil.BoolToStr( Ddo_servicoans_item_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Filtertype", StringUtil.RTrim( Ddo_servicoans_item_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Filterisrange", StringUtil.BoolToStr( Ddo_servicoans_item_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Includedatalist", StringUtil.BoolToStr( Ddo_servicoans_item_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Datalisttype", StringUtil.RTrim( Ddo_servicoans_item_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Datalistproc", StringUtil.RTrim( Ddo_servicoans_item_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servicoans_item_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Sortasc", StringUtil.RTrim( Ddo_servicoans_item_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Sortdsc", StringUtil.RTrim( Ddo_servicoans_item_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Loadingdata", StringUtil.RTrim( Ddo_servicoans_item_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Cleanfilter", StringUtil.RTrim( Ddo_servicoans_item_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Noresultsfound", StringUtil.RTrim( Ddo_servicoans_item_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Searchbuttontext", StringUtil.RTrim( Ddo_servicoans_item_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_servico_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_servico_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_servico_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Activeeventkey", StringUtil.RTrim( Ddo_servicoans_item_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Filteredtext_get", StringUtil.RTrim( Ddo_servicoans_item_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICOANS_ITEM_Selectedvalue_get", StringUtil.RTrim( Ddo_servicoans_item_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MANAGEFILTERS_Activeeventkey", StringUtil.RTrim( Ddo_managefilters_Activeeventkey));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE7O2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT7O2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwservicoans.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWServicoAns" ;
      }

      public override String GetPgmdesc( )
      {
         return " Servico Ans" ;
      }

      protected void WB7O0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_7O2( true) ;
         }
         else
         {
            wb_table1_2_7O2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_7O2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_93_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(104, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_93_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(105, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavManagefiltersexecutionstep_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34ManageFiltersExecutionStep), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34ManageFiltersExecutionStep), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavManagefiltersexecutionstep_Jsonclick, 0, "Attribute", "", "", "", edtavManagefiltersexecutionstep_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWServicoAns.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_nome_Internalname, StringUtil.RTrim( AV46TFServico_Nome), StringUtil.RTrim( context.localUtil.Format( AV46TFServico_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_nome_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoAns.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_nome_sel_Internalname, StringUtil.RTrim( AV47TFServico_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV47TFServico_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_nome_sel_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoAns.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicoans_item_Internalname, AV50TFServicoAns_Item, StringUtil.RTrim( context.localUtil.Format( AV50TFServicoAns_Item, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicoans_item_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicoans_item_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoAns.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservicoans_item_sel_Internalname, AV51TFServicoAns_Item_Sel, StringUtil.RTrim( context.localUtil.Format( AV51TFServicoAns_Item_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservicoans_item_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfservicoans_item_sel_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoAns.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_93_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_nometitlecontrolidtoreplace_Internalname, AV48ddo_Servico_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_servico_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServicoAns.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICOANS_ITEMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_93_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servicoans_itemtitlecontrolidtoreplace_Internalname, AV52ddo_ServicoAns_ItemTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"", 0, edtavDdo_servicoans_itemtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWServicoAns.htm");
         }
         wbLoad = true;
      }

      protected void START7O2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Servico Ans", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP7O0( ) ;
      }

      protected void WS7O2( )
      {
         START7O2( ) ;
         EVT7O2( ) ;
      }

      protected void EVT7O2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MANAGEFILTERS.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E117O2 */
                              E117O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E127O2 */
                              E127O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E137O2 */
                              E137O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICOANS_ITEM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E147O2 */
                              E147O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E157O2 */
                              E157O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E167O2 */
                              E167O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E177O2 */
                              E177O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E187O2 */
                              E187O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E197O2 */
                              E197O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E207O2 */
                              E207O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E217O2 */
                              E217O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E227O2 */
                              E227O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E237O2 */
                              E237O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E247O2 */
                              E247O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E257O2 */
                              E257O2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_93_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_93_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_93_idx), 4, 0)), 4, "0");
                              SubsflControlProps_932( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV77Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV78Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              A319ServicoAns_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServicoAns_Codigo_Internalname), ",", "."));
                              A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
                              A608Servico_Nome = StringUtil.Upper( cgiGet( edtServico_Nome_Internalname));
                              A320ServicoAns_Item = cgiGet( edtServicoAns_Item_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E267O2 */
                                    E267O2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E277O2 */
                                    E277O2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E287O2 */
                                    E287O2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servicoans_item1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOANS_ITEM1"), AV17ServicoAns_item1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servico_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME1"), AV40Servico_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servicoans_item2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOANS_ITEM2"), AV22ServicoAns_item2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servico_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME2"), AV41Servico_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servicoans_item3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOANS_ITEM3"), AV27ServicoAns_item3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Servico_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME3"), AV42Servico_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservico_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME"), AV46TFServico_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservico_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME_SEL"), AV47TFServico_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicoans_item Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOANS_ITEM"), AV50TFServicoAns_Item) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfservicoans_item_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOANS_ITEM_SEL"), AV51TFServicoAns_Item_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE7O2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA7O2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SERVICOANS_ITEM", "do Ans", 0);
            cmbavDynamicfiltersselector1.addItem("SERVICO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SERVICOANS_ITEM", "do Ans", 0);
            cmbavDynamicfiltersselector2.addItem("SERVICO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SERVICOANS_ITEM", "do Ans", 0);
            cmbavDynamicfiltersselector3.addItem("SERVICO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_932( ) ;
         while ( nGXsfl_93_idx <= nRC_GXsfl_93 )
         {
            sendrow_932( ) ;
            nGXsfl_93_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_93_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_93_idx+1));
            sGXsfl_93_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_93_idx), 4, 0)), 4, "0");
            SubsflControlProps_932( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17ServicoAns_item1 ,
                                       String AV40Servico_Nome1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22ServicoAns_item2 ,
                                       String AV41Servico_Nome2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       String AV27ServicoAns_item3 ,
                                       String AV42Servico_Nome3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       String AV46TFServico_Nome ,
                                       String AV47TFServico_Nome_Sel ,
                                       String AV50TFServicoAns_Item ,
                                       String AV51TFServicoAns_Item_Sel ,
                                       short AV34ManageFiltersExecutionStep ,
                                       String AV48ddo_Servico_NomeTitleControlIdToReplace ,
                                       String AV52ddo_ServicoAns_ItemTitleControlIdToReplace ,
                                       String AV79Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving ,
                                       int A319ServicoAns_Codigo ,
                                       int A155Servico_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF7O2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOANS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A319ServicoAns_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICOANS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A319ServicoAns_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICOANS_ITEM", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A320ServicoAns_Item, ""))));
         GxWebStd.gx_hidden_field( context, "SERVICOANS_ITEM", A320ServicoAns_Item);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF7O2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV79Pgmname = "WWServicoAns";
         context.Gx_err = 0;
      }

      protected void RF7O2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 93;
         /* Execute user event: E277O2 */
         E277O2 ();
         nGXsfl_93_idx = 1;
         sGXsfl_93_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_93_idx), 4, 0)), 4, "0");
         SubsflControlProps_932( ) ;
         nGXsfl_93_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_932( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV59WWServicoAnsDS_1_Dynamicfiltersselector1 ,
                                                 AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 ,
                                                 AV61WWServicoAnsDS_3_Servicoans_item1 ,
                                                 AV62WWServicoAnsDS_4_Servico_nome1 ,
                                                 AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 ,
                                                 AV64WWServicoAnsDS_6_Dynamicfiltersselector2 ,
                                                 AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 ,
                                                 AV66WWServicoAnsDS_8_Servicoans_item2 ,
                                                 AV67WWServicoAnsDS_9_Servico_nome2 ,
                                                 AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 ,
                                                 AV69WWServicoAnsDS_11_Dynamicfiltersselector3 ,
                                                 AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 ,
                                                 AV71WWServicoAnsDS_13_Servicoans_item3 ,
                                                 AV72WWServicoAnsDS_14_Servico_nome3 ,
                                                 AV74WWServicoAnsDS_16_Tfservico_nome_sel ,
                                                 AV73WWServicoAnsDS_15_Tfservico_nome ,
                                                 AV76WWServicoAnsDS_18_Tfservicoans_item_sel ,
                                                 AV75WWServicoAnsDS_17_Tfservicoans_item ,
                                                 A320ServicoAns_Item ,
                                                 A608Servico_Nome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV61WWServicoAnsDS_3_Servicoans_item1 = StringUtil.Concat( StringUtil.RTrim( AV61WWServicoAnsDS_3_Servicoans_item1), "%", "");
            lV61WWServicoAnsDS_3_Servicoans_item1 = StringUtil.Concat( StringUtil.RTrim( AV61WWServicoAnsDS_3_Servicoans_item1), "%", "");
            lV66WWServicoAnsDS_8_Servicoans_item2 = StringUtil.Concat( StringUtil.RTrim( AV66WWServicoAnsDS_8_Servicoans_item2), "%", "");
            lV66WWServicoAnsDS_8_Servicoans_item2 = StringUtil.Concat( StringUtil.RTrim( AV66WWServicoAnsDS_8_Servicoans_item2), "%", "");
            lV71WWServicoAnsDS_13_Servicoans_item3 = StringUtil.Concat( StringUtil.RTrim( AV71WWServicoAnsDS_13_Servicoans_item3), "%", "");
            lV71WWServicoAnsDS_13_Servicoans_item3 = StringUtil.Concat( StringUtil.RTrim( AV71WWServicoAnsDS_13_Servicoans_item3), "%", "");
            lV73WWServicoAnsDS_15_Tfservico_nome = StringUtil.PadR( StringUtil.RTrim( AV73WWServicoAnsDS_15_Tfservico_nome), 50, "%");
            lV75WWServicoAnsDS_17_Tfservicoans_item = StringUtil.Concat( StringUtil.RTrim( AV75WWServicoAnsDS_17_Tfservicoans_item), "%", "");
            /* Using cursor H007O2 */
            pr_default.execute(0, new Object[] {lV61WWServicoAnsDS_3_Servicoans_item1, lV61WWServicoAnsDS_3_Servicoans_item1, AV62WWServicoAnsDS_4_Servico_nome1, lV66WWServicoAnsDS_8_Servicoans_item2, lV66WWServicoAnsDS_8_Servicoans_item2, AV67WWServicoAnsDS_9_Servico_nome2, lV71WWServicoAnsDS_13_Servicoans_item3, lV71WWServicoAnsDS_13_Servicoans_item3, AV72WWServicoAnsDS_14_Servico_nome3, lV73WWServicoAnsDS_15_Tfservico_nome, AV74WWServicoAnsDS_16_Tfservico_nome_sel, lV75WWServicoAnsDS_17_Tfservicoans_item, AV76WWServicoAnsDS_18_Tfservicoans_item_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_93_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A320ServicoAns_Item = H007O2_A320ServicoAns_Item[0];
               A608Servico_Nome = H007O2_A608Servico_Nome[0];
               A155Servico_Codigo = H007O2_A155Servico_Codigo[0];
               A319ServicoAns_Codigo = H007O2_A319ServicoAns_Codigo[0];
               A608Servico_Nome = H007O2_A608Servico_Nome[0];
               /* Execute user event: E287O2 */
               E287O2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 93;
            WB7O0( ) ;
         }
         nGXsfl_93_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV59WWServicoAnsDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV61WWServicoAnsDS_3_Servicoans_item1 = AV17ServicoAns_item1;
         AV62WWServicoAnsDS_4_Servico_nome1 = AV40Servico_Nome1;
         AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV64WWServicoAnsDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV66WWServicoAnsDS_8_Servicoans_item2 = AV22ServicoAns_item2;
         AV67WWServicoAnsDS_9_Servico_nome2 = AV41Servico_Nome2;
         AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV69WWServicoAnsDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV71WWServicoAnsDS_13_Servicoans_item3 = AV27ServicoAns_item3;
         AV72WWServicoAnsDS_14_Servico_nome3 = AV42Servico_Nome3;
         AV73WWServicoAnsDS_15_Tfservico_nome = AV46TFServico_Nome;
         AV74WWServicoAnsDS_16_Tfservico_nome_sel = AV47TFServico_Nome_Sel;
         AV75WWServicoAnsDS_17_Tfservicoans_item = AV50TFServicoAns_Item;
         AV76WWServicoAnsDS_18_Tfservicoans_item_sel = AV51TFServicoAns_Item_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV59WWServicoAnsDS_1_Dynamicfiltersselector1 ,
                                              AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 ,
                                              AV61WWServicoAnsDS_3_Servicoans_item1 ,
                                              AV62WWServicoAnsDS_4_Servico_nome1 ,
                                              AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 ,
                                              AV64WWServicoAnsDS_6_Dynamicfiltersselector2 ,
                                              AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 ,
                                              AV66WWServicoAnsDS_8_Servicoans_item2 ,
                                              AV67WWServicoAnsDS_9_Servico_nome2 ,
                                              AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 ,
                                              AV69WWServicoAnsDS_11_Dynamicfiltersselector3 ,
                                              AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 ,
                                              AV71WWServicoAnsDS_13_Servicoans_item3 ,
                                              AV72WWServicoAnsDS_14_Servico_nome3 ,
                                              AV74WWServicoAnsDS_16_Tfservico_nome_sel ,
                                              AV73WWServicoAnsDS_15_Tfservico_nome ,
                                              AV76WWServicoAnsDS_18_Tfservicoans_item_sel ,
                                              AV75WWServicoAnsDS_17_Tfservicoans_item ,
                                              A320ServicoAns_Item ,
                                              A608Servico_Nome ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV61WWServicoAnsDS_3_Servicoans_item1 = StringUtil.Concat( StringUtil.RTrim( AV61WWServicoAnsDS_3_Servicoans_item1), "%", "");
         lV61WWServicoAnsDS_3_Servicoans_item1 = StringUtil.Concat( StringUtil.RTrim( AV61WWServicoAnsDS_3_Servicoans_item1), "%", "");
         lV66WWServicoAnsDS_8_Servicoans_item2 = StringUtil.Concat( StringUtil.RTrim( AV66WWServicoAnsDS_8_Servicoans_item2), "%", "");
         lV66WWServicoAnsDS_8_Servicoans_item2 = StringUtil.Concat( StringUtil.RTrim( AV66WWServicoAnsDS_8_Servicoans_item2), "%", "");
         lV71WWServicoAnsDS_13_Servicoans_item3 = StringUtil.Concat( StringUtil.RTrim( AV71WWServicoAnsDS_13_Servicoans_item3), "%", "");
         lV71WWServicoAnsDS_13_Servicoans_item3 = StringUtil.Concat( StringUtil.RTrim( AV71WWServicoAnsDS_13_Servicoans_item3), "%", "");
         lV73WWServicoAnsDS_15_Tfservico_nome = StringUtil.PadR( StringUtil.RTrim( AV73WWServicoAnsDS_15_Tfservico_nome), 50, "%");
         lV75WWServicoAnsDS_17_Tfservicoans_item = StringUtil.Concat( StringUtil.RTrim( AV75WWServicoAnsDS_17_Tfservicoans_item), "%", "");
         /* Using cursor H007O3 */
         pr_default.execute(1, new Object[] {lV61WWServicoAnsDS_3_Servicoans_item1, lV61WWServicoAnsDS_3_Servicoans_item1, AV62WWServicoAnsDS_4_Servico_nome1, lV66WWServicoAnsDS_8_Servicoans_item2, lV66WWServicoAnsDS_8_Servicoans_item2, AV67WWServicoAnsDS_9_Servico_nome2, lV71WWServicoAnsDS_13_Servicoans_item3, lV71WWServicoAnsDS_13_Servicoans_item3, AV72WWServicoAnsDS_14_Servico_nome3, lV73WWServicoAnsDS_15_Tfservico_nome, AV74WWServicoAnsDS_16_Tfservico_nome_sel, lV75WWServicoAnsDS_17_Tfservicoans_item, AV76WWServicoAnsDS_18_Tfservicoans_item_sel});
         GRID_nRecordCount = H007O3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV59WWServicoAnsDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV61WWServicoAnsDS_3_Servicoans_item1 = AV17ServicoAns_item1;
         AV62WWServicoAnsDS_4_Servico_nome1 = AV40Servico_Nome1;
         AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV64WWServicoAnsDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV66WWServicoAnsDS_8_Servicoans_item2 = AV22ServicoAns_item2;
         AV67WWServicoAnsDS_9_Servico_nome2 = AV41Servico_Nome2;
         AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV69WWServicoAnsDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV71WWServicoAnsDS_13_Servicoans_item3 = AV27ServicoAns_item3;
         AV72WWServicoAnsDS_14_Servico_nome3 = AV42Servico_Nome3;
         AV73WWServicoAnsDS_15_Tfservico_nome = AV46TFServico_Nome;
         AV74WWServicoAnsDS_16_Tfservico_nome_sel = AV47TFServico_Nome_Sel;
         AV75WWServicoAnsDS_17_Tfservicoans_item = AV50TFServicoAns_Item;
         AV76WWServicoAnsDS_18_Tfservicoans_item_sel = AV51TFServicoAns_Item_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoAns_item1, AV40Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ServicoAns_item2, AV41Servico_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ServicoAns_item3, AV42Servico_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV46TFServico_Nome, AV47TFServico_Nome_Sel, AV50TFServicoAns_Item, AV51TFServicoAns_Item_Sel, AV34ManageFiltersExecutionStep, AV48ddo_Servico_NomeTitleControlIdToReplace, AV52ddo_ServicoAns_ItemTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A319ServicoAns_Codigo, A155Servico_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV59WWServicoAnsDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV61WWServicoAnsDS_3_Servicoans_item1 = AV17ServicoAns_item1;
         AV62WWServicoAnsDS_4_Servico_nome1 = AV40Servico_Nome1;
         AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV64WWServicoAnsDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV66WWServicoAnsDS_8_Servicoans_item2 = AV22ServicoAns_item2;
         AV67WWServicoAnsDS_9_Servico_nome2 = AV41Servico_Nome2;
         AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV69WWServicoAnsDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV71WWServicoAnsDS_13_Servicoans_item3 = AV27ServicoAns_item3;
         AV72WWServicoAnsDS_14_Servico_nome3 = AV42Servico_Nome3;
         AV73WWServicoAnsDS_15_Tfservico_nome = AV46TFServico_Nome;
         AV74WWServicoAnsDS_16_Tfservico_nome_sel = AV47TFServico_Nome_Sel;
         AV75WWServicoAnsDS_17_Tfservicoans_item = AV50TFServicoAns_Item;
         AV76WWServicoAnsDS_18_Tfservicoans_item_sel = AV51TFServicoAns_Item_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoAns_item1, AV40Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ServicoAns_item2, AV41Servico_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ServicoAns_item3, AV42Servico_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV46TFServico_Nome, AV47TFServico_Nome_Sel, AV50TFServicoAns_Item, AV51TFServicoAns_Item_Sel, AV34ManageFiltersExecutionStep, AV48ddo_Servico_NomeTitleControlIdToReplace, AV52ddo_ServicoAns_ItemTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A319ServicoAns_Codigo, A155Servico_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV59WWServicoAnsDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV61WWServicoAnsDS_3_Servicoans_item1 = AV17ServicoAns_item1;
         AV62WWServicoAnsDS_4_Servico_nome1 = AV40Servico_Nome1;
         AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV64WWServicoAnsDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV66WWServicoAnsDS_8_Servicoans_item2 = AV22ServicoAns_item2;
         AV67WWServicoAnsDS_9_Servico_nome2 = AV41Servico_Nome2;
         AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV69WWServicoAnsDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV71WWServicoAnsDS_13_Servicoans_item3 = AV27ServicoAns_item3;
         AV72WWServicoAnsDS_14_Servico_nome3 = AV42Servico_Nome3;
         AV73WWServicoAnsDS_15_Tfservico_nome = AV46TFServico_Nome;
         AV74WWServicoAnsDS_16_Tfservico_nome_sel = AV47TFServico_Nome_Sel;
         AV75WWServicoAnsDS_17_Tfservicoans_item = AV50TFServicoAns_Item;
         AV76WWServicoAnsDS_18_Tfservicoans_item_sel = AV51TFServicoAns_Item_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoAns_item1, AV40Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ServicoAns_item2, AV41Servico_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ServicoAns_item3, AV42Servico_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV46TFServico_Nome, AV47TFServico_Nome_Sel, AV50TFServicoAns_Item, AV51TFServicoAns_Item_Sel, AV34ManageFiltersExecutionStep, AV48ddo_Servico_NomeTitleControlIdToReplace, AV52ddo_ServicoAns_ItemTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A319ServicoAns_Codigo, A155Servico_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV59WWServicoAnsDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV61WWServicoAnsDS_3_Servicoans_item1 = AV17ServicoAns_item1;
         AV62WWServicoAnsDS_4_Servico_nome1 = AV40Servico_Nome1;
         AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV64WWServicoAnsDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV66WWServicoAnsDS_8_Servicoans_item2 = AV22ServicoAns_item2;
         AV67WWServicoAnsDS_9_Servico_nome2 = AV41Servico_Nome2;
         AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV69WWServicoAnsDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV71WWServicoAnsDS_13_Servicoans_item3 = AV27ServicoAns_item3;
         AV72WWServicoAnsDS_14_Servico_nome3 = AV42Servico_Nome3;
         AV73WWServicoAnsDS_15_Tfservico_nome = AV46TFServico_Nome;
         AV74WWServicoAnsDS_16_Tfservico_nome_sel = AV47TFServico_Nome_Sel;
         AV75WWServicoAnsDS_17_Tfservicoans_item = AV50TFServicoAns_Item;
         AV76WWServicoAnsDS_18_Tfservicoans_item_sel = AV51TFServicoAns_Item_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoAns_item1, AV40Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ServicoAns_item2, AV41Servico_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ServicoAns_item3, AV42Servico_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV46TFServico_Nome, AV47TFServico_Nome_Sel, AV50TFServicoAns_Item, AV51TFServicoAns_Item_Sel, AV34ManageFiltersExecutionStep, AV48ddo_Servico_NomeTitleControlIdToReplace, AV52ddo_ServicoAns_ItemTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A319ServicoAns_Codigo, A155Servico_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV59WWServicoAnsDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV61WWServicoAnsDS_3_Servicoans_item1 = AV17ServicoAns_item1;
         AV62WWServicoAnsDS_4_Servico_nome1 = AV40Servico_Nome1;
         AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV64WWServicoAnsDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV66WWServicoAnsDS_8_Servicoans_item2 = AV22ServicoAns_item2;
         AV67WWServicoAnsDS_9_Servico_nome2 = AV41Servico_Nome2;
         AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV69WWServicoAnsDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV71WWServicoAnsDS_13_Servicoans_item3 = AV27ServicoAns_item3;
         AV72WWServicoAnsDS_14_Servico_nome3 = AV42Servico_Nome3;
         AV73WWServicoAnsDS_15_Tfservico_nome = AV46TFServico_Nome;
         AV74WWServicoAnsDS_16_Tfservico_nome_sel = AV47TFServico_Nome_Sel;
         AV75WWServicoAnsDS_17_Tfservicoans_item = AV50TFServicoAns_Item;
         AV76WWServicoAnsDS_18_Tfservicoans_item_sel = AV51TFServicoAns_Item_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoAns_item1, AV40Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ServicoAns_item2, AV41Servico_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ServicoAns_item3, AV42Servico_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV46TFServico_Nome, AV47TFServico_Nome_Sel, AV50TFServicoAns_Item, AV51TFServicoAns_Item_Sel, AV34ManageFiltersExecutionStep, AV48ddo_Servico_NomeTitleControlIdToReplace, AV52ddo_ServicoAns_ItemTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A319ServicoAns_Codigo, A155Servico_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP7O0( )
      {
         /* Before Start, stand alone formulas. */
         AV79Pgmname = "WWServicoAns";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E267O2 */
         E267O2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMANAGEFILTERSDATA"), AV38ManageFiltersData);
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV53DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICO_NOMETITLEFILTERDATA"), AV45Servico_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICOANS_ITEMTITLEFILTERDATA"), AV49ServicoAns_ItemTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17ServicoAns_item1 = cgiGet( edtavServicoans_item1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoAns_item1", AV17ServicoAns_item1);
            AV40Servico_Nome1 = StringUtil.Upper( cgiGet( edtavServico_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Servico_Nome1", AV40Servico_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22ServicoAns_item2 = cgiGet( edtavServicoans_item2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ServicoAns_item2", AV22ServicoAns_item2);
            AV41Servico_Nome2 = StringUtil.Upper( cgiGet( edtavServico_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Servico_Nome2", AV41Servico_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            AV27ServicoAns_item3 = cgiGet( edtavServicoans_item3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ServicoAns_item3", AV27ServicoAns_item3);
            AV42Servico_Nome3 = StringUtil.Upper( cgiGet( edtavServico_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Servico_Nome3", AV42Servico_Nome3);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMANAGEFILTERSEXECUTIONSTEP");
               GX_FocusControl = edtavManagefiltersexecutionstep_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34ManageFiltersExecutionStep = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV34ManageFiltersExecutionStep), 1, 0));
            }
            else
            {
               AV34ManageFiltersExecutionStep = (short)(context.localUtil.CToN( cgiGet( edtavManagefiltersexecutionstep_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV34ManageFiltersExecutionStep), 1, 0));
            }
            AV46TFServico_Nome = StringUtil.Upper( cgiGet( edtavTfservico_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFServico_Nome", AV46TFServico_Nome);
            AV47TFServico_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfservico_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFServico_Nome_Sel", AV47TFServico_Nome_Sel);
            AV50TFServicoAns_Item = cgiGet( edtavTfservicoans_item_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFServicoAns_Item", AV50TFServicoAns_Item);
            AV51TFServicoAns_Item_Sel = cgiGet( edtavTfservicoans_item_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFServicoAns_Item_Sel", AV51TFServicoAns_Item_Sel);
            AV48ddo_Servico_NomeTitleControlIdToReplace = cgiGet( edtavDdo_servico_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Servico_NomeTitleControlIdToReplace", AV48ddo_Servico_NomeTitleControlIdToReplace);
            AV52ddo_ServicoAns_ItemTitleControlIdToReplace = cgiGet( edtavDdo_servicoans_itemtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ServicoAns_ItemTitleControlIdToReplace", AV52ddo_ServicoAns_ItemTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_93 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_93"), ",", "."));
            AV55GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV56GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_managefilters_Icon = cgiGet( "DDO_MANAGEFILTERS_Icon");
            Ddo_managefilters_Caption = cgiGet( "DDO_MANAGEFILTERS_Caption");
            Ddo_managefilters_Tooltip = cgiGet( "DDO_MANAGEFILTERS_Tooltip");
            Ddo_managefilters_Cls = cgiGet( "DDO_MANAGEFILTERS_Cls");
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_servico_nome_Caption = cgiGet( "DDO_SERVICO_NOME_Caption");
            Ddo_servico_nome_Tooltip = cgiGet( "DDO_SERVICO_NOME_Tooltip");
            Ddo_servico_nome_Cls = cgiGet( "DDO_SERVICO_NOME_Cls");
            Ddo_servico_nome_Filteredtext_set = cgiGet( "DDO_SERVICO_NOME_Filteredtext_set");
            Ddo_servico_nome_Selectedvalue_set = cgiGet( "DDO_SERVICO_NOME_Selectedvalue_set");
            Ddo_servico_nome_Dropdownoptionstype = cgiGet( "DDO_SERVICO_NOME_Dropdownoptionstype");
            Ddo_servico_nome_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICO_NOME_Titlecontrolidtoreplace");
            Ddo_servico_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includesortasc"));
            Ddo_servico_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includesortdsc"));
            Ddo_servico_nome_Sortedstatus = cgiGet( "DDO_SERVICO_NOME_Sortedstatus");
            Ddo_servico_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includefilter"));
            Ddo_servico_nome_Filtertype = cgiGet( "DDO_SERVICO_NOME_Filtertype");
            Ddo_servico_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Filterisrange"));
            Ddo_servico_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_NOME_Includedatalist"));
            Ddo_servico_nome_Datalisttype = cgiGet( "DDO_SERVICO_NOME_Datalisttype");
            Ddo_servico_nome_Datalistproc = cgiGet( "DDO_SERVICO_NOME_Datalistproc");
            Ddo_servico_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servico_nome_Sortasc = cgiGet( "DDO_SERVICO_NOME_Sortasc");
            Ddo_servico_nome_Sortdsc = cgiGet( "DDO_SERVICO_NOME_Sortdsc");
            Ddo_servico_nome_Loadingdata = cgiGet( "DDO_SERVICO_NOME_Loadingdata");
            Ddo_servico_nome_Cleanfilter = cgiGet( "DDO_SERVICO_NOME_Cleanfilter");
            Ddo_servico_nome_Noresultsfound = cgiGet( "DDO_SERVICO_NOME_Noresultsfound");
            Ddo_servico_nome_Searchbuttontext = cgiGet( "DDO_SERVICO_NOME_Searchbuttontext");
            Ddo_servicoans_item_Caption = cgiGet( "DDO_SERVICOANS_ITEM_Caption");
            Ddo_servicoans_item_Tooltip = cgiGet( "DDO_SERVICOANS_ITEM_Tooltip");
            Ddo_servicoans_item_Cls = cgiGet( "DDO_SERVICOANS_ITEM_Cls");
            Ddo_servicoans_item_Filteredtext_set = cgiGet( "DDO_SERVICOANS_ITEM_Filteredtext_set");
            Ddo_servicoans_item_Selectedvalue_set = cgiGet( "DDO_SERVICOANS_ITEM_Selectedvalue_set");
            Ddo_servicoans_item_Dropdownoptionstype = cgiGet( "DDO_SERVICOANS_ITEM_Dropdownoptionstype");
            Ddo_servicoans_item_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICOANS_ITEM_Titlecontrolidtoreplace");
            Ddo_servicoans_item_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOANS_ITEM_Includesortasc"));
            Ddo_servicoans_item_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICOANS_ITEM_Includesortdsc"));
            Ddo_servicoans_item_Sortedstatus = cgiGet( "DDO_SERVICOANS_ITEM_Sortedstatus");
            Ddo_servicoans_item_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICOANS_ITEM_Includefilter"));
            Ddo_servicoans_item_Filtertype = cgiGet( "DDO_SERVICOANS_ITEM_Filtertype");
            Ddo_servicoans_item_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICOANS_ITEM_Filterisrange"));
            Ddo_servicoans_item_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICOANS_ITEM_Includedatalist"));
            Ddo_servicoans_item_Datalisttype = cgiGet( "DDO_SERVICOANS_ITEM_Datalisttype");
            Ddo_servicoans_item_Datalistproc = cgiGet( "DDO_SERVICOANS_ITEM_Datalistproc");
            Ddo_servicoans_item_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICOANS_ITEM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servicoans_item_Sortasc = cgiGet( "DDO_SERVICOANS_ITEM_Sortasc");
            Ddo_servicoans_item_Sortdsc = cgiGet( "DDO_SERVICOANS_ITEM_Sortdsc");
            Ddo_servicoans_item_Loadingdata = cgiGet( "DDO_SERVICOANS_ITEM_Loadingdata");
            Ddo_servicoans_item_Cleanfilter = cgiGet( "DDO_SERVICOANS_ITEM_Cleanfilter");
            Ddo_servicoans_item_Noresultsfound = cgiGet( "DDO_SERVICOANS_ITEM_Noresultsfound");
            Ddo_servicoans_item_Searchbuttontext = cgiGet( "DDO_SERVICOANS_ITEM_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_servico_nome_Activeeventkey = cgiGet( "DDO_SERVICO_NOME_Activeeventkey");
            Ddo_servico_nome_Filteredtext_get = cgiGet( "DDO_SERVICO_NOME_Filteredtext_get");
            Ddo_servico_nome_Selectedvalue_get = cgiGet( "DDO_SERVICO_NOME_Selectedvalue_get");
            Ddo_servicoans_item_Activeeventkey = cgiGet( "DDO_SERVICOANS_ITEM_Activeeventkey");
            Ddo_servicoans_item_Filteredtext_get = cgiGet( "DDO_SERVICOANS_ITEM_Filteredtext_get");
            Ddo_servicoans_item_Selectedvalue_get = cgiGet( "DDO_SERVICOANS_ITEM_Selectedvalue_get");
            Ddo_managefilters_Activeeventkey = cgiGet( "DDO_MANAGEFILTERS_Activeeventkey");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOANS_ITEM1"), AV17ServicoAns_item1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME1"), AV40Servico_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOANS_ITEM2"), AV22ServicoAns_item2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME2"), AV41Servico_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICOANS_ITEM3"), AV27ServicoAns_item3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSERVICO_NOME3"), AV42Servico_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME"), AV46TFServico_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICO_NOME_SEL"), AV47TFServico_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOANS_ITEM"), AV50TFServicoAns_Item) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSERVICOANS_ITEM_SEL"), AV51TFServicoAns_Item_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E267O2 */
         E267O2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E267O2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            edtavManagefiltersexecutionstep_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavManagefiltersexecutionstep_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavManagefiltersexecutionstep_Visible), 5, 0)));
         }
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "SERVICOANS_ITEM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "SERVICOANS_ITEM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "SERVICOANS_ITEM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfservico_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_nome_Visible), 5, 0)));
         edtavTfservico_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_nome_sel_Visible), 5, 0)));
         edtavTfservicoans_item_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicoans_item_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoans_item_Visible), 5, 0)));
         edtavTfservicoans_item_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservicoans_item_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservicoans_item_sel_Visible), 5, 0)));
         Ddo_servico_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "TitleControlIdToReplace", Ddo_servico_nome_Titlecontrolidtoreplace);
         AV48ddo_Servico_NomeTitleControlIdToReplace = Ddo_servico_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Servico_NomeTitleControlIdToReplace", AV48ddo_Servico_NomeTitleControlIdToReplace);
         edtavDdo_servico_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servico_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servicoans_item_Titlecontrolidtoreplace = subGrid_Internalname+"_ServicoAns_Item";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoans_item_Internalname, "TitleControlIdToReplace", Ddo_servicoans_item_Titlecontrolidtoreplace);
         AV52ddo_ServicoAns_ItemTitleControlIdToReplace = Ddo_servicoans_item_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_ServicoAns_ItemTitleControlIdToReplace", AV52ddo_ServicoAns_ItemTitleControlIdToReplace);
         edtavDdo_servicoans_itemtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servicoans_itemtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servicoans_itemtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Servico Ans";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "do Ans", 0);
         cmbavOrderedby.addItem("2", "Servi�o", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         Ddo_managefilters_Icon = context.convertURL( (String)(context.GetImagePath( "5efb9e2b-46db-43f4-8f74-dd3f5818d30e", "", context.GetTheme( ))));
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "Icon", Ddo_managefilters_Icon);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV53DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV53DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E277O2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV45Servico_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ServicoAns_ItemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         if ( AV34ManageFiltersExecutionStep == 1 )
         {
            AV34ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV34ManageFiltersExecutionStep), 1, 0));
         }
         else if ( AV34ManageFiltersExecutionStep == 2 )
         {
            AV34ManageFiltersExecutionStep = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV34ManageFiltersExecutionStep), 1, 0));
            /* Execute user subroutine: 'LOADSAVEDFILTERS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtServico_Nome_Titleformat = 2;
         edtServico_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servi�o", AV48ddo_Servico_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Nome_Internalname, "Title", edtServico_Nome_Title);
         edtServicoAns_Item_Titleformat = 2;
         edtServicoAns_Item_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Item", AV52ddo_ServicoAns_ItemTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServicoAns_Item_Internalname, "Title", edtServicoAns_Item_Title);
         AV55GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridCurrentPage), 10, 0)));
         AV56GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56GridPageCount), 10, 0)));
         AV59WWServicoAnsDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV61WWServicoAnsDS_3_Servicoans_item1 = AV17ServicoAns_item1;
         AV62WWServicoAnsDS_4_Servico_nome1 = AV40Servico_Nome1;
         AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV64WWServicoAnsDS_6_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV66WWServicoAnsDS_8_Servicoans_item2 = AV22ServicoAns_item2;
         AV67WWServicoAnsDS_9_Servico_nome2 = AV41Servico_Nome2;
         AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV69WWServicoAnsDS_11_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV71WWServicoAnsDS_13_Servicoans_item3 = AV27ServicoAns_item3;
         AV72WWServicoAnsDS_14_Servico_nome3 = AV42Servico_Nome3;
         AV73WWServicoAnsDS_15_Tfservico_nome = AV46TFServico_Nome;
         AV74WWServicoAnsDS_16_Tfservico_nome_sel = AV47TFServico_Nome_Sel;
         AV75WWServicoAnsDS_17_Tfservicoans_item = AV50TFServicoAns_Item;
         AV76WWServicoAnsDS_18_Tfservicoans_item_sel = AV51TFServicoAns_Item_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV45Servico_NomeTitleFilterData", AV45Servico_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49ServicoAns_ItemTitleFilterData", AV49ServicoAns_ItemTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38ManageFiltersData", AV38ManageFiltersData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E127O2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV54PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV54PageToGo) ;
         }
      }

      protected void E137O2( )
      {
         /* Ddo_servico_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV46TFServico_Nome = Ddo_servico_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFServico_Nome", AV46TFServico_Nome);
            AV47TFServico_Nome_Sel = Ddo_servico_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFServico_Nome_Sel", AV47TFServico_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E147O2( )
      {
         /* Ddo_servicoans_item_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servicoans_item_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicoans_item_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoans_item_Internalname, "SortedStatus", Ddo_servicoans_item_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicoans_item_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servicoans_item_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoans_item_Internalname, "SortedStatus", Ddo_servicoans_item_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servicoans_item_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFServicoAns_Item = Ddo_servicoans_item_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFServicoAns_Item", AV50TFServicoAns_Item);
            AV51TFServicoAns_Item_Sel = Ddo_servicoans_item_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFServicoAns_Item_Sel", AV51TFServicoAns_Item_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E287O2( )
      {
         /* Grid_Load Routine */
         AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
         AV77Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("servicoans.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A319ServicoAns_Codigo) + "," + UrlEncode("" +A155Servico_Codigo);
         AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
         AV78Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("servicoans.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A319ServicoAns_Codigo) + "," + UrlEncode("" +A155Servico_Codigo);
         edtServicoAns_Item_Link = formatLink("viewservicoans.aspx") + "?" + UrlEncode("" +A319ServicoAns_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 93;
         }
         sendrow_932( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_93_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(93, GridRow);
         }
      }

      protected void E157O2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E217O2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E167O2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoAns_item1, AV40Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ServicoAns_item2, AV41Servico_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ServicoAns_item3, AV42Servico_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV46TFServico_Nome, AV47TFServico_Nome_Sel, AV50TFServicoAns_Item, AV51TFServicoAns_Item_Sel, AV34ManageFiltersExecutionStep, AV48ddo_Servico_NomeTitleControlIdToReplace, AV52ddo_ServicoAns_ItemTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A319ServicoAns_Codigo, A155Servico_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E227O2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E237O2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E177O2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoAns_item1, AV40Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ServicoAns_item2, AV41Servico_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ServicoAns_item3, AV42Servico_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV46TFServico_Nome, AV47TFServico_Nome_Sel, AV50TFServicoAns_Item, AV51TFServicoAns_Item_Sel, AV34ManageFiltersExecutionStep, AV48ddo_Servico_NomeTitleControlIdToReplace, AV52ddo_ServicoAns_ItemTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A319ServicoAns_Codigo, A155Servico_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E247O2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E187O2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ServicoAns_item1, AV40Servico_Nome1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22ServicoAns_item2, AV41Servico_Nome2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27ServicoAns_item3, AV42Servico_Nome3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV46TFServico_Nome, AV47TFServico_Nome_Sel, AV50TFServicoAns_Item, AV51TFServicoAns_Item_Sel, AV34ManageFiltersExecutionStep, AV48ddo_Servico_NomeTitleControlIdToReplace, AV52ddo_ServicoAns_ItemTitleControlIdToReplace, AV79Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A319ServicoAns_Codigo, A155Servico_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E257O2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E117O2( )
      {
         /* Ddo_managefilters_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Clean#>") == 0 )
         {
            /* Execute user subroutine: 'CLEANFILTERS' */
            S232 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Save#>") == 0 )
         {
            /* Execute user subroutine: 'SAVEGRIDSTATE' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            context.PopUp(formatLink("wwpbaseobjects.savefilteras.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWServicoAnsFilters")) + "," + UrlEncode(StringUtil.RTrim(AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika"))), new Object[] {});
            AV34ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV34ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_managefilters_Activeeventkey, "<#Manage#>") == 0 )
         {
            context.PopUp(formatLink("wwpbaseobjects.managefilters.aspx") + "?" + UrlEncode(StringUtil.RTrim("WWServicoAnsFilters")), new Object[] {});
            AV34ManageFiltersExecutionStep = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34ManageFiltersExecutionStep", StringUtil.Str( (decimal)(AV34ManageFiltersExecutionStep), 1, 0));
            context.DoAjaxRefresh();
         }
         else
         {
            GXt_char2 = AV35ManageFiltersXml;
            new wwpbaseobjects.getfilterbyname(context ).execute(  "WWServicoAnsFilters",  Ddo_managefilters_Activeeventkey, out  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_managefilters_Internalname, "ActiveEventKey", Ddo_managefilters_Activeeventkey);
            AV35ManageFiltersXml = GXt_char2;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV35ManageFiltersXml)) )
            {
               GX_msglist.addItem("O filtro selecionado n�o existe mais.");
            }
            else
            {
               /* Execute user subroutine: 'CLEANFILTERS' */
               S232 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               new wwpbaseobjects.savegridstate(context ).execute(  AV79Pgmname+"GridState",  AV35ManageFiltersXml) ;
               AV10GridState.FromXml(AV35ManageFiltersXml, "");
               AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
               S172 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
               S242 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
               S222 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               subgrid_firstpage( ) ;
               context.DoAjaxRefresh();
            }
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E197O2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E207O2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("servicoans.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S192( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_servico_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
         Ddo_servicoans_item_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoans_item_Internalname, "SortedStatus", Ddo_servicoans_item_Sortedstatus);
      }

      protected void S172( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_servico_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SortedStatus", Ddo_servico_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_servicoans_item_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoans_item_Internalname, "SortedStatus", Ddo_servicoans_item_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavServicoans_item1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoans_item1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoans_item1_Visible), 5, 0)));
         edtavServico_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOANS_ITEM") == 0 )
         {
            edtavServicoans_item1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoans_item1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoans_item1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome1_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavServicoans_item2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoans_item2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoans_item2_Visible), 5, 0)));
         edtavServico_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOANS_ITEM") == 0 )
         {
            edtavServicoans_item2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoans_item2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoans_item2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome2_Visible), 5, 0)));
         }
      }

      protected void S142( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavServicoans_item3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoans_item3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoans_item3_Visible), 5, 0)));
         edtavServico_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICOANS_ITEM") == 0 )
         {
            edtavServicoans_item3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServicoans_item3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServicoans_item3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_NOME") == 0 )
         {
            edtavServico_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_nome3_Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "SERVICOANS_ITEM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22ServicoAns_item2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ServicoAns_item2", AV22ServicoAns_item2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "SERVICOANS_ITEM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         AV27ServicoAns_item3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ServicoAns_item3", AV27ServicoAns_item3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S112( )
      {
         /* 'LOADSAVEDFILTERS' Routine */
         AV38ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV39ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV39ManageFiltersDataItem.gxTpr_Title = "Limpar filtros";
         AV39ManageFiltersDataItem.gxTpr_Eventkey = "<#Clean#>";
         AV39ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV39ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "63d2ae92-4e43-4a70-af61-0943e39ea422", "", context.GetTheme( ))));
         AV39ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
         AV38ManageFiltersData.Add(AV39ManageFiltersDataItem, 0);
         AV39ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV39ManageFiltersDataItem.gxTpr_Title = "Salvar filtro como...";
         AV39ManageFiltersDataItem.gxTpr_Eventkey = "<#Save#>";
         AV39ManageFiltersDataItem.gxTpr_Isdivider = false;
         AV39ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "6eee63e8-73c7-4738-beee-f98e3a8d2841", "", context.GetTheme( ))));
         AV38ManageFiltersData.Add(AV39ManageFiltersDataItem, 0);
         AV39ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV39ManageFiltersDataItem.gxTpr_Isdivider = true;
         AV38ManageFiltersData.Add(AV39ManageFiltersDataItem, 0);
         AV36ManageFiltersItems.FromXml(new wwpbaseobjects.loadmanagefiltersstate(context).executeUdp(  "WWServicoAnsFilters"), "");
         AV80GXV1 = 1;
         while ( AV80GXV1 <= AV36ManageFiltersItems.Count )
         {
            AV37ManageFiltersItem = ((wwpbaseobjects.SdtGridStateCollection_Item)AV36ManageFiltersItems.Item(AV80GXV1));
            AV39ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV39ManageFiltersDataItem.gxTpr_Title = AV37ManageFiltersItem.gxTpr_Title;
            AV39ManageFiltersDataItem.gxTpr_Eventkey = AV37ManageFiltersItem.gxTpr_Title;
            AV39ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV39ManageFiltersDataItem.gxTpr_Jsonclickevent = "WWPDynFilterHideAll(3)";
            AV38ManageFiltersData.Add(AV39ManageFiltersDataItem, 0);
            if ( AV38ManageFiltersData.Count == 13 )
            {
               if (true) break;
            }
            AV80GXV1 = (int)(AV80GXV1+1);
         }
         if ( AV38ManageFiltersData.Count > 3 )
         {
            AV39ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV39ManageFiltersDataItem.gxTpr_Isdivider = true;
            AV38ManageFiltersData.Add(AV39ManageFiltersDataItem, 0);
            AV39ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
            AV39ManageFiltersDataItem.gxTpr_Title = "Gerenciar filtros";
            AV39ManageFiltersDataItem.gxTpr_Eventkey = "<#Manage#>";
            AV39ManageFiltersDataItem.gxTpr_Isdivider = false;
            AV39ManageFiltersDataItem.gxTpr_Icon = context.convertURL( (String)(context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ))));
            AV39ManageFiltersDataItem.gxTpr_Jsonclickevent = "";
            AV38ManageFiltersData.Add(AV39ManageFiltersDataItem, 0);
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV46TFServico_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFServico_Nome", AV46TFServico_Nome);
         Ddo_servico_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "FilteredText_set", Ddo_servico_nome_Filteredtext_set);
         AV47TFServico_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFServico_Nome_Sel", AV47TFServico_Nome_Sel);
         Ddo_servico_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SelectedValue_set", Ddo_servico_nome_Selectedvalue_set);
         AV50TFServicoAns_Item = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFServicoAns_Item", AV50TFServicoAns_Item);
         Ddo_servicoans_item_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoans_item_Internalname, "FilteredText_set", Ddo_servicoans_item_Filteredtext_set);
         AV51TFServicoAns_Item_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFServicoAns_Item_Sel", AV51TFServicoAns_Item_Sel);
         Ddo_servicoans_item_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoans_item_Internalname, "SelectedValue_set", Ddo_servicoans_item_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "SERVICOANS_ITEM";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17ServicoAns_item1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoAns_item1", AV17ServicoAns_item1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get(AV79Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV79Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV33Session.Get(AV79Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S242( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV81GXV2 = 1;
         while ( AV81GXV2 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV81GXV2));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME") == 0 )
            {
               AV46TFServico_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFServico_Nome", AV46TFServico_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFServico_Nome)) )
               {
                  Ddo_servico_nome_Filteredtext_set = AV46TFServico_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "FilteredText_set", Ddo_servico_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICO_NOME_SEL") == 0 )
            {
               AV47TFServico_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFServico_Nome_Sel", AV47TFServico_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFServico_Nome_Sel)) )
               {
                  Ddo_servico_nome_Selectedvalue_set = AV47TFServico_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_nome_Internalname, "SelectedValue_set", Ddo_servico_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOANS_ITEM") == 0 )
            {
               AV50TFServicoAns_Item = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFServicoAns_Item", AV50TFServicoAns_Item);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFServicoAns_Item)) )
               {
                  Ddo_servicoans_item_Filteredtext_set = AV50TFServicoAns_Item;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoans_item_Internalname, "FilteredText_set", Ddo_servicoans_item_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFSERVICOANS_ITEM_SEL") == 0 )
            {
               AV51TFServicoAns_Item_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFServicoAns_Item_Sel", AV51TFServicoAns_Item_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFServicoAns_Item_Sel)) )
               {
                  Ddo_servicoans_item_Selectedvalue_set = AV51TFServicoAns_Item_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servicoans_item_Internalname, "SelectedValue_set", Ddo_servicoans_item_Selectedvalue_set);
               }
            }
            AV81GXV2 = (int)(AV81GXV2+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOANS_ITEM") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ServicoAns_item1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ServicoAns_item1", AV17ServicoAns_item1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_NOME") == 0 )
            {
               AV40Servico_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40Servico_Nome1", AV40Servico_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOANS_ITEM") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22ServicoAns_item2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ServicoAns_item2", AV22ServicoAns_item2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 )
               {
                  AV41Servico_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Servico_Nome2", AV41Servico_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S132 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICOANS_ITEM") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV27ServicoAns_item3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ServicoAns_item3", AV27ServicoAns_item3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_NOME") == 0 )
                  {
                     AV42Servico_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42Servico_Nome3", AV42Servico_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV33Session.Get(AV79Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFServico_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV46TFServico_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFServico_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV47TFServico_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFServicoAns_Item)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOANS_ITEM";
            AV11GridStateFilterValue.gxTpr_Value = AV50TFServicoAns_Item;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFServicoAns_Item_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICOANS_ITEM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV51TFServicoAns_Item_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV79Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICOANS_ITEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17ServicoAns_item1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17ServicoAns_item1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV40Servico_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV40Servico_Nome1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICOANS_ITEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22ServicoAns_item2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22ServicoAns_item2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Servico_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV41Servico_Nome2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICOANS_ITEM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27ServicoAns_item3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV27ServicoAns_item3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "SERVICO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Servico_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV42Servico_Nome3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S152( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV79Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ServicoAns";
         AV33Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_7O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_7O2( true) ;
         }
         else
         {
            wb_table2_8_7O2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_7O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_87_7O2( true) ;
         }
         else
         {
            wb_table3_87_7O2( false) ;
         }
         return  ;
      }

      protected void wb_table3_87_7O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_7O2e( true) ;
         }
         else
         {
            wb_table1_2_7O2e( false) ;
         }
      }

      protected void wb_table3_87_7O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_90_7O2( true) ;
         }
         else
         {
            wb_table4_90_7O2( false) ;
         }
         return  ;
      }

      protected void wb_table4_90_7O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_87_7O2e( true) ;
         }
         else
         {
            wb_table3_87_7O2e( false) ;
         }
      }

      protected void wb_table4_90_7O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"93\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo do Ans") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Servi�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(380), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServicoAns_Item_Titleformat == 0 )
               {
                  context.SendWebValue( edtServicoAns_Item_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServicoAns_Item_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A319ServicoAns_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A608Servico_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A320ServicoAns_Item);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServicoAns_Item_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServicoAns_Item_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtServicoAns_Item_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 93 )
         {
            wbEnd = 0;
            nRC_GXsfl_93 = (short)(nGXsfl_93_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_90_7O2e( true) ;
         }
         else
         {
            wb_table4_90_7O2e( false) ;
         }
      }

      protected void wb_table2_8_7O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblServicoanstitle_Internalname, "Servi�o Ans", "", "", lblServicoanstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Width100'>") ;
            wb_table5_13_7O2( true) ;
         }
         else
         {
            wb_table5_13_7O2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_7O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_93_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_WWServicoAns.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_27_7O2( true) ;
         }
         else
         {
            wb_table6_27_7O2( false) ;
         }
         return  ;
      }

      protected void wb_table6_27_7O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_7O2e( true) ;
         }
         else
         {
            wb_table2_8_7O2e( false) ;
         }
      }

      protected void wb_table6_27_7O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_30_7O2( true) ;
         }
         else
         {
            wb_table7_30_7O2( false) ;
         }
         return  ;
      }

      protected void wb_table7_30_7O2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_27_7O2e( true) ;
         }
         else
         {
            wb_table6_27_7O2e( false) ;
         }
      }

      protected void wb_table7_30_7O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_93_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_WWServicoAns.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_39_7O2( true) ;
         }
         else
         {
            wb_table8_39_7O2( false) ;
         }
         return  ;
      }

      protected void wb_table8_39_7O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoAns.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'" + sGXsfl_93_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "", true, "HLP_WWServicoAns.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_57_7O2( true) ;
         }
         else
         {
            wb_table9_57_7O2( false) ;
         }
         return  ;
      }

      protected void wb_table9_57_7O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoAns.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_93_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_WWServicoAns.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_75_7O2( true) ;
         }
         else
         {
            wb_table10_75_7O2( false) ;
         }
         return  ;
      }

      protected void wb_table10_75_7O2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_30_7O2e( true) ;
         }
         else
         {
            wb_table7_30_7O2e( false) ;
         }
      }

      protected void wb_table10_75_7O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_93_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "", true, "HLP_WWServicoAns.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicoans_item3_Internalname, AV27ServicoAns_item3, StringUtil.RTrim( context.localUtil.Format( AV27ServicoAns_item3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicoans_item3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicoans_item3_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoAns.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome3_Internalname, StringUtil.RTrim( AV42Servico_Nome3), StringUtil.RTrim( context.localUtil.Format( AV42Servico_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome3_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_75_7O2e( true) ;
         }
         else
         {
            wb_table10_75_7O2e( false) ;
         }
      }

      protected void wb_table9_57_7O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_93_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_WWServicoAns.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicoans_item2_Internalname, AV22ServicoAns_item2, StringUtil.RTrim( context.localUtil.Format( AV22ServicoAns_item2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicoans_item2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicoans_item2_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoAns.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome2_Internalname, StringUtil.RTrim( AV41Servico_Nome2), StringUtil.RTrim( context.localUtil.Format( AV41Servico_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome2_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_57_7O2e( true) ;
         }
         else
         {
            wb_table9_57_7O2e( false) ;
         }
      }

      protected void wb_table8_39_7O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_93_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_WWServicoAns.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServicoans_item1_Internalname, AV17ServicoAns_item1, StringUtil.RTrim( context.localUtil.Format( AV17ServicoAns_item1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServicoans_item1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServicoans_item1_Visible, 1, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoAns.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_93_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_nome1_Internalname, StringUtil.RTrim( AV40Servico_Nome1), StringUtil.RTrim( context.localUtil.Format( AV40Servico_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavServico_nome1_Visible, 1, 0, "text", "", 380, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_39_7O2e( true) ;
         }
         else
         {
            wb_table8_39_7O2e( false) ;
         }
      }

      protected void wb_table5_13_7O2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWServicoAns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MANAGEFILTERSContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_7O2e( true) ;
         }
         else
         {
            wb_table5_13_7O2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA7O2( ) ;
         WS7O2( ) ;
         WE7O2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117374137");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwservicoans.js", "?20203117374138");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_932( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_93_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_93_idx;
         edtServicoAns_Codigo_Internalname = "SERVICOANS_CODIGO_"+sGXsfl_93_idx;
         edtServico_Codigo_Internalname = "SERVICO_CODIGO_"+sGXsfl_93_idx;
         edtServico_Nome_Internalname = "SERVICO_NOME_"+sGXsfl_93_idx;
         edtServicoAns_Item_Internalname = "SERVICOANS_ITEM_"+sGXsfl_93_idx;
      }

      protected void SubsflControlProps_fel_932( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_93_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_93_fel_idx;
         edtServicoAns_Codigo_Internalname = "SERVICOANS_CODIGO_"+sGXsfl_93_fel_idx;
         edtServico_Codigo_Internalname = "SERVICO_CODIGO_"+sGXsfl_93_fel_idx;
         edtServico_Nome_Internalname = "SERVICO_NOME_"+sGXsfl_93_fel_idx;
         edtServicoAns_Item_Internalname = "SERVICOANS_ITEM_"+sGXsfl_93_fel_idx;
      }

      protected void sendrow_932( )
      {
         SubsflControlProps_932( ) ;
         WB7O0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_93_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_93_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_93_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV77Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV77Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV78Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV78Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoAns_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A319ServicoAns_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A319ServicoAns_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServicoAns_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)93,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)93,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Nome_Internalname,StringUtil.RTrim( A608Servico_Nome),StringUtil.RTrim( context.localUtil.Format( A608Servico_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)380,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)93,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServicoAns_Item_Internalname,(String)A320ServicoAns_Item,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtServicoAns_Item_Link,(String)"",(String)"",(String)"",(String)edtServicoAns_Item_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)93,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOANS_CODIGO"+"_"+sGXsfl_93_idx, GetSecureSignedToken( sGXsfl_93_idx, context.localUtil.Format( (decimal)(A319ServicoAns_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICOANS_ITEM"+"_"+sGXsfl_93_idx, GetSecureSignedToken( sGXsfl_93_idx, StringUtil.RTrim( context.localUtil.Format( A320ServicoAns_Item, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_93_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_93_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_93_idx+1));
            sGXsfl_93_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_93_idx), 4, 0)), 4, "0");
            SubsflControlProps_932( ) ;
         }
         /* End function sendrow_932 */
      }

      protected void init_default_properties( )
      {
         lblServicoanstitle_Internalname = "SERVICOANSTITLE";
         imgInsert_Internalname = "INSERT";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         Ddo_managefilters_Internalname = "DDO_MANAGEFILTERS";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavServicoans_item1_Internalname = "vSERVICOANS_ITEM1";
         edtavServico_nome1_Internalname = "vSERVICO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavServicoans_item2_Internalname = "vSERVICOANS_ITEM2";
         edtavServico_nome2_Internalname = "vSERVICO_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavServicoans_item3_Internalname = "vSERVICOANS_ITEM3";
         edtavServico_nome3_Internalname = "vSERVICO_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtServicoAns_Codigo_Internalname = "SERVICOANS_CODIGO";
         edtServico_Codigo_Internalname = "SERVICO_CODIGO";
         edtServico_Nome_Internalname = "SERVICO_NOME";
         edtServicoAns_Item_Internalname = "SERVICOANS_ITEM";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavManagefiltersexecutionstep_Internalname = "vMANAGEFILTERSEXECUTIONSTEP";
         edtavTfservico_nome_Internalname = "vTFSERVICO_NOME";
         edtavTfservico_nome_sel_Internalname = "vTFSERVICO_NOME_SEL";
         edtavTfservicoans_item_Internalname = "vTFSERVICOANS_ITEM";
         edtavTfservicoans_item_sel_Internalname = "vTFSERVICOANS_ITEM_SEL";
         Ddo_servico_nome_Internalname = "DDO_SERVICO_NOME";
         edtavDdo_servico_nometitlecontrolidtoreplace_Internalname = "vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_servicoans_item_Internalname = "DDO_SERVICOANS_ITEM";
         edtavDdo_servicoans_itemtitlecontrolidtoreplace_Internalname = "vDDO_SERVICOANS_ITEMTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtServicoAns_Item_Jsonclick = "";
         edtServico_Nome_Jsonclick = "";
         edtServico_Codigo_Jsonclick = "";
         edtServicoAns_Codigo_Jsonclick = "";
         edtavServico_nome1_Jsonclick = "";
         edtavServicoans_item1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavServico_nome2_Jsonclick = "";
         edtavServicoans_item2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavServico_nome3_Jsonclick = "";
         edtavServicoans_item3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtServicoAns_Item_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtServicoAns_Item_Titleformat = 0;
         edtServico_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavServico_nome3_Visible = 1;
         edtavServicoans_item3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavServico_nome2_Visible = 1;
         edtavServicoans_item2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavServico_nome1_Visible = 1;
         edtavServicoans_item1_Visible = 1;
         edtServicoAns_Item_Title = "Item";
         edtServico_Nome_Title = "Servi�o";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_servicoans_itemtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servico_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfservicoans_item_sel_Jsonclick = "";
         edtavTfservicoans_item_sel_Visible = 1;
         edtavTfservicoans_item_Jsonclick = "";
         edtavTfservicoans_item_Visible = 1;
         edtavTfservico_nome_sel_Jsonclick = "";
         edtavTfservico_nome_sel_Visible = 1;
         edtavTfservico_nome_Jsonclick = "";
         edtavTfservico_nome_Visible = 1;
         edtavManagefiltersexecutionstep_Jsonclick = "";
         edtavManagefiltersexecutionstep_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_servicoans_item_Searchbuttontext = "Pesquisar";
         Ddo_servicoans_item_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servicoans_item_Cleanfilter = "Limpar pesquisa";
         Ddo_servicoans_item_Loadingdata = "Carregando dados...";
         Ddo_servicoans_item_Sortdsc = "Ordenar de Z � A";
         Ddo_servicoans_item_Sortasc = "Ordenar de A � Z";
         Ddo_servicoans_item_Datalistupdateminimumcharacters = 0;
         Ddo_servicoans_item_Datalistproc = "GetWWServicoAnsFilterData";
         Ddo_servicoans_item_Datalisttype = "Dynamic";
         Ddo_servicoans_item_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servicoans_item_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servicoans_item_Filtertype = "Character";
         Ddo_servicoans_item_Includefilter = Convert.ToBoolean( -1);
         Ddo_servicoans_item_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servicoans_item_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servicoans_item_Titlecontrolidtoreplace = "";
         Ddo_servicoans_item_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servicoans_item_Cls = "ColumnSettings";
         Ddo_servicoans_item_Tooltip = "Op��es";
         Ddo_servicoans_item_Caption = "";
         Ddo_servico_nome_Searchbuttontext = "Pesquisar";
         Ddo_servico_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servico_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_nome_Loadingdata = "Carregando dados...";
         Ddo_servico_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_nome_Sortasc = "Ordenar de A � Z";
         Ddo_servico_nome_Datalistupdateminimumcharacters = 0;
         Ddo_servico_nome_Datalistproc = "GetWWServicoAnsFilterData";
         Ddo_servico_nome_Datalisttype = "Dynamic";
         Ddo_servico_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_servico_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_servico_nome_Filtertype = "Character";
         Ddo_servico_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_servico_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_nome_Titlecontrolidtoreplace = "";
         Ddo_servico_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_nome_Cls = "ColumnSettings";
         Ddo_servico_nome_Tooltip = "Op��es";
         Ddo_servico_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Ddo_managefilters_Cls = "ManageFilters";
         Ddo_managefilters_Tooltip = "Gerenciar filtros";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Servico Ans";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV48ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoAns_ItemTitleControlIdToReplace',fld:'vDDO_SERVICOANS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV45Servico_NomeTitleFilterData',fld:'vSERVICO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV49ServicoAns_ItemTitleFilterData',fld:'vSERVICOANS_ITEMTITLEFILTERDATA',pic:'',nv:null},{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'edtServico_Nome_Titleformat',ctrl:'SERVICO_NOME',prop:'Titleformat'},{av:'edtServico_Nome_Title',ctrl:'SERVICO_NOME',prop:'Title'},{av:'edtServicoAns_Item_Titleformat',ctrl:'SERVICOANS_ITEM',prop:'Titleformat'},{av:'edtServicoAns_Item_Title',ctrl:'SERVICOANS_ITEM',prop:'Title'},{av:'AV55GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV56GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV38ManageFiltersData',fld:'vMANAGEFILTERSDATA',pic:'',nv:null},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E127O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV48ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoAns_ItemTitleControlIdToReplace',fld:'vDDO_SERVICOANS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SERVICO_NOME.ONOPTIONCLICKED","{handler:'E137O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV48ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoAns_ItemTitleControlIdToReplace',fld:'vDDO_SERVICOANS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servico_nome_Activeeventkey',ctrl:'DDO_SERVICO_NOME',prop:'ActiveEventKey'},{av:'Ddo_servico_nome_Filteredtext_get',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_get'},{av:'Ddo_servico_nome_Selectedvalue_get',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servicoans_item_Sortedstatus',ctrl:'DDO_SERVICOANS_ITEM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICOANS_ITEM.ONOPTIONCLICKED","{handler:'E147O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV48ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoAns_ItemTitleControlIdToReplace',fld:'vDDO_SERVICOANS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servicoans_item_Activeeventkey',ctrl:'DDO_SERVICOANS_ITEM',prop:'ActiveEventKey'},{av:'Ddo_servicoans_item_Filteredtext_get',ctrl:'DDO_SERVICOANS_ITEM',prop:'FilteredText_get'},{av:'Ddo_servicoans_item_Selectedvalue_get',ctrl:'DDO_SERVICOANS_ITEM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servicoans_item_Sortedstatus',ctrl:'DDO_SERVICOANS_ITEM',prop:'SortedStatus'},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E287O2',iparms:[{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtServicoAns_Item_Link',ctrl:'SERVICOANS_ITEM',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E157O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV48ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoAns_ItemTitleControlIdToReplace',fld:'vDDO_SERVICOANS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E217O2',iparms:[],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E167O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV48ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoAns_ItemTitleControlIdToReplace',fld:'vDDO_SERVICOANS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'edtavServicoans_item2_Visible',ctrl:'vSERVICOANS_ITEM2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicoans_item3_Visible',ctrl:'vSERVICOANS_ITEM3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServicoans_item1_Visible',ctrl:'vSERVICOANS_ITEM1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E227O2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavServicoans_item1_Visible',ctrl:'vSERVICOANS_ITEM1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E237O2',iparms:[],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E177O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV48ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoAns_ItemTitleControlIdToReplace',fld:'vDDO_SERVICOANS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'edtavServicoans_item2_Visible',ctrl:'vSERVICOANS_ITEM2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicoans_item3_Visible',ctrl:'vSERVICOANS_ITEM3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServicoans_item1_Visible',ctrl:'vSERVICOANS_ITEM1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E247O2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavServicoans_item2_Visible',ctrl:'vSERVICOANS_ITEM2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E187O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV48ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoAns_ItemTitleControlIdToReplace',fld:'vDDO_SERVICOANS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'edtavServicoans_item2_Visible',ctrl:'vSERVICOANS_ITEM2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicoans_item3_Visible',ctrl:'vSERVICOANS_ITEM3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavServicoans_item1_Visible',ctrl:'vSERVICOANS_ITEM1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E257O2',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavServicoans_item3_Visible',ctrl:'vSERVICOANS_ITEM3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("DDO_MANAGEFILTERS.ONOPTIONCLICKED","{handler:'E117O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV48ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoAns_ItemTitleControlIdToReplace',fld:'vDDO_SERVICOANS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_managefilters_Activeeventkey',ctrl:'DDO_MANAGEFILTERS',prop:'ActiveEventKey'}],oparms:[{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'Ddo_servico_nome_Filteredtext_set',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_set'},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servico_nome_Selectedvalue_set',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_set'},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'Ddo_servicoans_item_Filteredtext_set',ctrl:'DDO_SERVICOANS_ITEM',prop:'FilteredText_set'},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'Ddo_servicoans_item_Selectedvalue_set',ctrl:'DDO_SERVICOANS_ITEM',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'Ddo_servico_nome_Sortedstatus',ctrl:'DDO_SERVICO_NOME',prop:'SortedStatus'},{av:'Ddo_servicoans_item_Sortedstatus',ctrl:'DDO_SERVICOANS_ITEM',prop:'SortedStatus'},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'edtavServicoans_item1_Visible',ctrl:'vSERVICOANS_ITEM1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'edtavServicoans_item2_Visible',ctrl:'vSERVICOANS_ITEM2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicoans_item3_Visible',ctrl:'vSERVICOANS_ITEM3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E197O2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'AV34ManageFiltersExecutionStep',fld:'vMANAGEFILTERSEXECUTIONSTEP',pic:'9',nv:0},{av:'AV48ddo_Servico_NomeTitleControlIdToReplace',fld:'vDDO_SERVICO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_ServicoAns_ItemTitleControlIdToReplace',fld:'vDDO_SERVICOANS_ITEMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV79Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV46TFServico_Nome',fld:'vTFSERVICO_NOME',pic:'@!',nv:''},{av:'Ddo_servico_nome_Filteredtext_set',ctrl:'DDO_SERVICO_NOME',prop:'FilteredText_set'},{av:'AV47TFServico_Nome_Sel',fld:'vTFSERVICO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_servico_nome_Selectedvalue_set',ctrl:'DDO_SERVICO_NOME',prop:'SelectedValue_set'},{av:'AV50TFServicoAns_Item',fld:'vTFSERVICOANS_ITEM',pic:'',nv:''},{av:'Ddo_servicoans_item_Filteredtext_set',ctrl:'DDO_SERVICOANS_ITEM',prop:'FilteredText_set'},{av:'AV51TFServicoAns_Item_Sel',fld:'vTFSERVICOANS_ITEM_SEL',pic:'',nv:''},{av:'Ddo_servicoans_item_Selectedvalue_set',ctrl:'DDO_SERVICOANS_ITEM',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ServicoAns_item1',fld:'vSERVICOANS_ITEM1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavServicoans_item1_Visible',ctrl:'vSERVICOANS_ITEM1',prop:'Visible'},{av:'edtavServico_nome1_Visible',ctrl:'vSERVICO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22ServicoAns_item2',fld:'vSERVICOANS_ITEM2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27ServicoAns_item3',fld:'vSERVICOANS_ITEM3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV40Servico_Nome1',fld:'vSERVICO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV41Servico_Nome2',fld:'vSERVICO_NOME2',pic:'@!',nv:''},{av:'AV42Servico_Nome3',fld:'vSERVICO_NOME3',pic:'@!',nv:''},{av:'edtavServicoans_item2_Visible',ctrl:'vSERVICOANS_ITEM2',prop:'Visible'},{av:'edtavServico_nome2_Visible',ctrl:'vSERVICO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavServicoans_item3_Visible',ctrl:'vSERVICOANS_ITEM3',prop:'Visible'},{av:'edtavServico_nome3_Visible',ctrl:'vSERVICO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E207O2',iparms:[{av:'A319ServicoAns_Codigo',fld:'SERVICOANS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_servico_nome_Activeeventkey = "";
         Ddo_servico_nome_Filteredtext_get = "";
         Ddo_servico_nome_Selectedvalue_get = "";
         Ddo_servicoans_item_Activeeventkey = "";
         Ddo_servicoans_item_Filteredtext_get = "";
         Ddo_servicoans_item_Selectedvalue_get = "";
         Ddo_managefilters_Activeeventkey = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ServicoAns_item1 = "";
         AV40Servico_Nome1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22ServicoAns_item2 = "";
         AV41Servico_Nome2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV27ServicoAns_item3 = "";
         AV42Servico_Nome3 = "";
         AV46TFServico_Nome = "";
         AV47TFServico_Nome_Sel = "";
         AV50TFServicoAns_Item = "";
         AV51TFServicoAns_Item_Sel = "";
         AV48ddo_Servico_NomeTitleControlIdToReplace = "";
         AV52ddo_ServicoAns_ItemTitleControlIdToReplace = "";
         AV79Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV38ManageFiltersData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV45Servico_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49ServicoAns_ItemTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_managefilters_Icon = "";
         Ddo_managefilters_Caption = "";
         Ddo_servico_nome_Filteredtext_set = "";
         Ddo_servico_nome_Selectedvalue_set = "";
         Ddo_servico_nome_Sortedstatus = "";
         Ddo_servicoans_item_Filteredtext_set = "";
         Ddo_servicoans_item_Selectedvalue_set = "";
         Ddo_servicoans_item_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV77Update_GXI = "";
         AV32Delete = "";
         AV78Delete_GXI = "";
         A608Servico_Nome = "";
         A320ServicoAns_Item = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV61WWServicoAnsDS_3_Servicoans_item1 = "";
         lV66WWServicoAnsDS_8_Servicoans_item2 = "";
         lV71WWServicoAnsDS_13_Servicoans_item3 = "";
         lV73WWServicoAnsDS_15_Tfservico_nome = "";
         lV75WWServicoAnsDS_17_Tfservicoans_item = "";
         AV59WWServicoAnsDS_1_Dynamicfiltersselector1 = "";
         AV61WWServicoAnsDS_3_Servicoans_item1 = "";
         AV62WWServicoAnsDS_4_Servico_nome1 = "";
         AV64WWServicoAnsDS_6_Dynamicfiltersselector2 = "";
         AV66WWServicoAnsDS_8_Servicoans_item2 = "";
         AV67WWServicoAnsDS_9_Servico_nome2 = "";
         AV69WWServicoAnsDS_11_Dynamicfiltersselector3 = "";
         AV71WWServicoAnsDS_13_Servicoans_item3 = "";
         AV72WWServicoAnsDS_14_Servico_nome3 = "";
         AV74WWServicoAnsDS_16_Tfservico_nome_sel = "";
         AV73WWServicoAnsDS_15_Tfservico_nome = "";
         AV76WWServicoAnsDS_18_Tfservicoans_item_sel = "";
         AV75WWServicoAnsDS_17_Tfservicoans_item = "";
         H007O2_A320ServicoAns_Item = new String[] {""} ;
         H007O2_A608Servico_Nome = new String[] {""} ;
         H007O2_A155Servico_Codigo = new int[1] ;
         H007O2_A319ServicoAns_Codigo = new int[1] ;
         H007O3_AGRID_nRecordCount = new long[1] ;
         AV7HTTPRequest = new GxHttpRequest( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV35ManageFiltersXml = "";
         GXt_char2 = "";
         AV39ManageFiltersDataItem = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item(context);
         AV36ManageFiltersItems = new GxObjectCollection( context, "GridStateCollection.Item", "", "wwpbaseobjects.SdtGridStateCollection_Item", "GeneXus.Programs");
         AV37ManageFiltersItem = new wwpbaseobjects.SdtGridStateCollection_Item(context);
         AV33Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblServicoanstitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwservicoans__default(),
            new Object[][] {
                new Object[] {
               H007O2_A320ServicoAns_Item, H007O2_A608Servico_Nome, H007O2_A155Servico_Codigo, H007O2_A319ServicoAns_Codigo
               }
               , new Object[] {
               H007O3_AGRID_nRecordCount
               }
            }
         );
         AV79Pgmname = "WWServicoAns";
         /* GeneXus formulas. */
         AV79Pgmname = "WWServicoAns";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_93 ;
      private short nGXsfl_93_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short AV34ManageFiltersExecutionStep ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_93_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 ;
      private short AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 ;
      private short AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 ;
      private short edtServico_Nome_Titleformat ;
      private short edtServicoAns_Item_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A319ServicoAns_Codigo ;
      private int A155Servico_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_servico_nome_Datalistupdateminimumcharacters ;
      private int Ddo_servicoans_item_Datalistupdateminimumcharacters ;
      private int edtavManagefiltersexecutionstep_Visible ;
      private int edtavTfservico_nome_Visible ;
      private int edtavTfservico_nome_sel_Visible ;
      private int edtavTfservicoans_item_Visible ;
      private int edtavTfservicoans_item_sel_Visible ;
      private int edtavDdo_servico_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servicoans_itemtitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV54PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavServicoans_item1_Visible ;
      private int edtavServico_nome1_Visible ;
      private int edtavServicoans_item2_Visible ;
      private int edtavServico_nome2_Visible ;
      private int edtavServicoans_item3_Visible ;
      private int edtavServico_nome3_Visible ;
      private int AV80GXV1 ;
      private int AV81GXV2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV55GridCurrentPage ;
      private long AV56GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_servico_nome_Activeeventkey ;
      private String Ddo_servico_nome_Filteredtext_get ;
      private String Ddo_servico_nome_Selectedvalue_get ;
      private String Ddo_servicoans_item_Activeeventkey ;
      private String Ddo_servicoans_item_Filteredtext_get ;
      private String Ddo_servicoans_item_Selectedvalue_get ;
      private String Ddo_managefilters_Activeeventkey ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_93_idx="0001" ;
      private String AV40Servico_Nome1 ;
      private String AV41Servico_Nome2 ;
      private String AV42Servico_Nome3 ;
      private String AV46TFServico_Nome ;
      private String AV47TFServico_Nome_Sel ;
      private String AV79Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_managefilters_Icon ;
      private String Ddo_managefilters_Caption ;
      private String Ddo_managefilters_Tooltip ;
      private String Ddo_managefilters_Cls ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_servico_nome_Caption ;
      private String Ddo_servico_nome_Tooltip ;
      private String Ddo_servico_nome_Cls ;
      private String Ddo_servico_nome_Filteredtext_set ;
      private String Ddo_servico_nome_Selectedvalue_set ;
      private String Ddo_servico_nome_Dropdownoptionstype ;
      private String Ddo_servico_nome_Titlecontrolidtoreplace ;
      private String Ddo_servico_nome_Sortedstatus ;
      private String Ddo_servico_nome_Filtertype ;
      private String Ddo_servico_nome_Datalisttype ;
      private String Ddo_servico_nome_Datalistproc ;
      private String Ddo_servico_nome_Sortasc ;
      private String Ddo_servico_nome_Sortdsc ;
      private String Ddo_servico_nome_Loadingdata ;
      private String Ddo_servico_nome_Cleanfilter ;
      private String Ddo_servico_nome_Noresultsfound ;
      private String Ddo_servico_nome_Searchbuttontext ;
      private String Ddo_servicoans_item_Caption ;
      private String Ddo_servicoans_item_Tooltip ;
      private String Ddo_servicoans_item_Cls ;
      private String Ddo_servicoans_item_Filteredtext_set ;
      private String Ddo_servicoans_item_Selectedvalue_set ;
      private String Ddo_servicoans_item_Dropdownoptionstype ;
      private String Ddo_servicoans_item_Titlecontrolidtoreplace ;
      private String Ddo_servicoans_item_Sortedstatus ;
      private String Ddo_servicoans_item_Filtertype ;
      private String Ddo_servicoans_item_Datalisttype ;
      private String Ddo_servicoans_item_Datalistproc ;
      private String Ddo_servicoans_item_Sortasc ;
      private String Ddo_servicoans_item_Sortdsc ;
      private String Ddo_servicoans_item_Loadingdata ;
      private String Ddo_servicoans_item_Cleanfilter ;
      private String Ddo_servicoans_item_Noresultsfound ;
      private String Ddo_servicoans_item_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavManagefiltersexecutionstep_Internalname ;
      private String edtavManagefiltersexecutionstep_Jsonclick ;
      private String edtavTfservico_nome_Internalname ;
      private String edtavTfservico_nome_Jsonclick ;
      private String edtavTfservico_nome_sel_Internalname ;
      private String edtavTfservico_nome_sel_Jsonclick ;
      private String edtavTfservicoans_item_Internalname ;
      private String edtavTfservicoans_item_Jsonclick ;
      private String edtavTfservicoans_item_sel_Internalname ;
      private String edtavTfservicoans_item_sel_Jsonclick ;
      private String edtavDdo_servico_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servicoans_itemtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtServicoAns_Codigo_Internalname ;
      private String edtServico_Codigo_Internalname ;
      private String A608Servico_Nome ;
      private String edtServico_Nome_Internalname ;
      private String edtServicoAns_Item_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV73WWServicoAnsDS_15_Tfservico_nome ;
      private String AV62WWServicoAnsDS_4_Servico_nome1 ;
      private String AV67WWServicoAnsDS_9_Servico_nome2 ;
      private String AV72WWServicoAnsDS_14_Servico_nome3 ;
      private String AV74WWServicoAnsDS_16_Tfservico_nome_sel ;
      private String AV73WWServicoAnsDS_15_Tfservico_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavServicoans_item1_Internalname ;
      private String edtavServico_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavServicoans_item2_Internalname ;
      private String edtavServico_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavServicoans_item3_Internalname ;
      private String edtavServico_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_servico_nome_Internalname ;
      private String Ddo_servicoans_item_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String Ddo_managefilters_Internalname ;
      private String edtServico_Nome_Title ;
      private String edtServicoAns_Item_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtServicoAns_Item_Link ;
      private String GXt_char2 ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblServicoanstitle_Internalname ;
      private String lblServicoanstitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavServicoans_item3_Jsonclick ;
      private String edtavServico_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavServicoans_item2_Jsonclick ;
      private String edtavServico_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavServicoans_item1_Jsonclick ;
      private String edtavServico_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_93_fel_idx="0001" ;
      private String ROClassString ;
      private String edtServicoAns_Codigo_Jsonclick ;
      private String edtServico_Codigo_Jsonclick ;
      private String edtServico_Nome_Jsonclick ;
      private String edtServicoAns_Item_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_servico_nome_Includesortasc ;
      private bool Ddo_servico_nome_Includesortdsc ;
      private bool Ddo_servico_nome_Includefilter ;
      private bool Ddo_servico_nome_Filterisrange ;
      private bool Ddo_servico_nome_Includedatalist ;
      private bool Ddo_servicoans_item_Includesortasc ;
      private bool Ddo_servicoans_item_Includesortdsc ;
      private bool Ddo_servicoans_item_Includefilter ;
      private bool Ddo_servicoans_item_Filterisrange ;
      private bool Ddo_servicoans_item_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 ;
      private bool AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private String AV35ManageFiltersXml ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17ServicoAns_item1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV22ServicoAns_item2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV27ServicoAns_item3 ;
      private String AV50TFServicoAns_Item ;
      private String AV51TFServicoAns_Item_Sel ;
      private String AV48ddo_Servico_NomeTitleControlIdToReplace ;
      private String AV52ddo_ServicoAns_ItemTitleControlIdToReplace ;
      private String AV77Update_GXI ;
      private String AV78Delete_GXI ;
      private String A320ServicoAns_Item ;
      private String lV61WWServicoAnsDS_3_Servicoans_item1 ;
      private String lV66WWServicoAnsDS_8_Servicoans_item2 ;
      private String lV71WWServicoAnsDS_13_Servicoans_item3 ;
      private String lV75WWServicoAnsDS_17_Tfservicoans_item ;
      private String AV59WWServicoAnsDS_1_Dynamicfiltersselector1 ;
      private String AV61WWServicoAnsDS_3_Servicoans_item1 ;
      private String AV64WWServicoAnsDS_6_Dynamicfiltersselector2 ;
      private String AV66WWServicoAnsDS_8_Servicoans_item2 ;
      private String AV69WWServicoAnsDS_11_Dynamicfiltersselector3 ;
      private String AV71WWServicoAnsDS_13_Servicoans_item3 ;
      private String AV76WWServicoAnsDS_18_Tfservicoans_item_sel ;
      private String AV75WWServicoAnsDS_17_Tfservicoans_item ;
      private String AV31Update ;
      private String AV32Delete ;
      private IGxSession AV33Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H007O2_A320ServicoAns_Item ;
      private String[] H007O2_A608Servico_Nome ;
      private int[] H007O2_A155Servico_Codigo ;
      private int[] H007O2_A319ServicoAns_Codigo ;
      private long[] H007O3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtGridStateCollection_Item ))]
      private IGxCollection AV36ManageFiltersItems ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38ManageFiltersData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV45Servico_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49ServicoAns_ItemTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtGridStateCollection_Item AV37ManageFiltersItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item AV39ManageFiltersDataItem ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV53DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwservicoans__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H007O2( IGxContext context ,
                                             String AV59WWServicoAnsDS_1_Dynamicfiltersselector1 ,
                                             short AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 ,
                                             String AV61WWServicoAnsDS_3_Servicoans_item1 ,
                                             String AV62WWServicoAnsDS_4_Servico_nome1 ,
                                             bool AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 ,
                                             String AV64WWServicoAnsDS_6_Dynamicfiltersselector2 ,
                                             short AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 ,
                                             String AV66WWServicoAnsDS_8_Servicoans_item2 ,
                                             String AV67WWServicoAnsDS_9_Servico_nome2 ,
                                             bool AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 ,
                                             String AV69WWServicoAnsDS_11_Dynamicfiltersselector3 ,
                                             short AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 ,
                                             String AV71WWServicoAnsDS_13_Servicoans_item3 ,
                                             String AV72WWServicoAnsDS_14_Servico_nome3 ,
                                             String AV74WWServicoAnsDS_16_Tfservico_nome_sel ,
                                             String AV73WWServicoAnsDS_15_Tfservico_nome ,
                                             String AV76WWServicoAnsDS_18_Tfservicoans_item_sel ,
                                             String AV75WWServicoAnsDS_17_Tfservicoans_item ,
                                             String A320ServicoAns_Item ,
                                             String A608Servico_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [18] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ServicoAns_Item], T2.[Servico_Nome], T1.[Servico_Codigo], T1.[ServicoAns_Codigo]";
         sFromString = " FROM ([ServicoAns] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV59WWServicoAnsDS_1_Dynamicfiltersselector1, "SERVICOANS_ITEM") == 0 ) && ( AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWServicoAnsDS_3_Servicoans_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV61WWServicoAnsDS_3_Servicoans_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV61WWServicoAnsDS_3_Servicoans_item1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWServicoAnsDS_1_Dynamicfiltersselector1, "SERVICOANS_ITEM") == 0 ) && ( AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWServicoAnsDS_3_Servicoans_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV61WWServicoAnsDS_3_Servicoans_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV61WWServicoAnsDS_3_Servicoans_item1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWServicoAnsDS_1_Dynamicfiltersselector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWServicoAnsDS_4_Servico_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV62WWServicoAnsDS_4_Servico_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV62WWServicoAnsDS_4_Servico_nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWServicoAnsDS_6_Dynamicfiltersselector2, "SERVICOANS_ITEM") == 0 ) && ( AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWServicoAnsDS_8_Servicoans_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV66WWServicoAnsDS_8_Servicoans_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV66WWServicoAnsDS_8_Servicoans_item2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWServicoAnsDS_6_Dynamicfiltersselector2, "SERVICOANS_ITEM") == 0 ) && ( AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWServicoAnsDS_8_Servicoans_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV66WWServicoAnsDS_8_Servicoans_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV66WWServicoAnsDS_8_Servicoans_item2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWServicoAnsDS_6_Dynamicfiltersselector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWServicoAnsDS_9_Servico_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV67WWServicoAnsDS_9_Servico_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV67WWServicoAnsDS_9_Servico_nome2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWServicoAnsDS_11_Dynamicfiltersselector3, "SERVICOANS_ITEM") == 0 ) && ( AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWServicoAnsDS_13_Servicoans_item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV71WWServicoAnsDS_13_Servicoans_item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV71WWServicoAnsDS_13_Servicoans_item3)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWServicoAnsDS_11_Dynamicfiltersselector3, "SERVICOANS_ITEM") == 0 ) && ( AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWServicoAnsDS_13_Servicoans_item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV71WWServicoAnsDS_13_Servicoans_item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV71WWServicoAnsDS_13_Servicoans_item3)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWServicoAnsDS_11_Dynamicfiltersselector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWServicoAnsDS_14_Servico_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV72WWServicoAnsDS_14_Servico_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV72WWServicoAnsDS_14_Servico_nome3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWServicoAnsDS_16_Tfservico_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWServicoAnsDS_15_Tfservico_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like @lV73WWServicoAnsDS_15_Tfservico_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like @lV73WWServicoAnsDS_15_Tfservico_nome)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWServicoAnsDS_16_Tfservico_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV74WWServicoAnsDS_16_Tfservico_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV74WWServicoAnsDS_16_Tfservico_nome_sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV76WWServicoAnsDS_18_Tfservicoans_item_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWServicoAnsDS_17_Tfservicoans_item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV75WWServicoAnsDS_17_Tfservicoans_item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV75WWServicoAnsDS_17_Tfservicoans_item)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWServicoAnsDS_18_Tfservicoans_item_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] = @AV76WWServicoAnsDS_18_Tfservicoans_item_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] = @AV76WWServicoAnsDS_18_Tfservicoans_item_sel)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoAns_Item]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoAns_Item] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Servico_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Servico_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ServicoAns_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_H007O3( IGxContext context ,
                                             String AV59WWServicoAnsDS_1_Dynamicfiltersselector1 ,
                                             short AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 ,
                                             String AV61WWServicoAnsDS_3_Servicoans_item1 ,
                                             String AV62WWServicoAnsDS_4_Servico_nome1 ,
                                             bool AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 ,
                                             String AV64WWServicoAnsDS_6_Dynamicfiltersselector2 ,
                                             short AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 ,
                                             String AV66WWServicoAnsDS_8_Servicoans_item2 ,
                                             String AV67WWServicoAnsDS_9_Servico_nome2 ,
                                             bool AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 ,
                                             String AV69WWServicoAnsDS_11_Dynamicfiltersselector3 ,
                                             short AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 ,
                                             String AV71WWServicoAnsDS_13_Servicoans_item3 ,
                                             String AV72WWServicoAnsDS_14_Servico_nome3 ,
                                             String AV74WWServicoAnsDS_16_Tfservico_nome_sel ,
                                             String AV73WWServicoAnsDS_15_Tfservico_nome ,
                                             String AV76WWServicoAnsDS_18_Tfservicoans_item_sel ,
                                             String AV75WWServicoAnsDS_17_Tfservicoans_item ,
                                             String A320ServicoAns_Item ,
                                             String A608Servico_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [13] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ServicoAns] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[Servico_Codigo])";
         if ( ( StringUtil.StrCmp(AV59WWServicoAnsDS_1_Dynamicfiltersselector1, "SERVICOANS_ITEM") == 0 ) && ( AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWServicoAnsDS_3_Servicoans_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV61WWServicoAnsDS_3_Servicoans_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV61WWServicoAnsDS_3_Servicoans_item1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWServicoAnsDS_1_Dynamicfiltersselector1, "SERVICOANS_ITEM") == 0 ) && ( AV60WWServicoAnsDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWServicoAnsDS_3_Servicoans_item1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV61WWServicoAnsDS_3_Servicoans_item1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV61WWServicoAnsDS_3_Servicoans_item1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV59WWServicoAnsDS_1_Dynamicfiltersselector1, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWServicoAnsDS_4_Servico_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV62WWServicoAnsDS_4_Servico_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV62WWServicoAnsDS_4_Servico_nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWServicoAnsDS_6_Dynamicfiltersselector2, "SERVICOANS_ITEM") == 0 ) && ( AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWServicoAnsDS_8_Servicoans_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV66WWServicoAnsDS_8_Servicoans_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV66WWServicoAnsDS_8_Servicoans_item2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWServicoAnsDS_6_Dynamicfiltersselector2, "SERVICOANS_ITEM") == 0 ) && ( AV65WWServicoAnsDS_7_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWServicoAnsDS_8_Servicoans_item2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV66WWServicoAnsDS_8_Servicoans_item2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV66WWServicoAnsDS_8_Servicoans_item2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV63WWServicoAnsDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV64WWServicoAnsDS_6_Dynamicfiltersselector2, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWServicoAnsDS_9_Servico_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV67WWServicoAnsDS_9_Servico_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV67WWServicoAnsDS_9_Servico_nome2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWServicoAnsDS_11_Dynamicfiltersselector3, "SERVICOANS_ITEM") == 0 ) && ( AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWServicoAnsDS_13_Servicoans_item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV71WWServicoAnsDS_13_Servicoans_item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV71WWServicoAnsDS_13_Servicoans_item3)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWServicoAnsDS_11_Dynamicfiltersselector3, "SERVICOANS_ITEM") == 0 ) && ( AV70WWServicoAnsDS_12_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWServicoAnsDS_13_Servicoans_item3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like '%' + @lV71WWServicoAnsDS_13_Servicoans_item3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like '%' + @lV71WWServicoAnsDS_13_Servicoans_item3)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV68WWServicoAnsDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV69WWServicoAnsDS_11_Dynamicfiltersselector3, "SERVICO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV72WWServicoAnsDS_14_Servico_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV72WWServicoAnsDS_14_Servico_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV72WWServicoAnsDS_14_Servico_nome3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWServicoAnsDS_16_Tfservico_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWServicoAnsDS_15_Tfservico_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] like @lV73WWServicoAnsDS_15_Tfservico_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] like @lV73WWServicoAnsDS_15_Tfservico_nome)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWServicoAnsDS_16_Tfservico_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Nome] = @AV74WWServicoAnsDS_16_Tfservico_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Nome] = @AV74WWServicoAnsDS_16_Tfservico_nome_sel)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV76WWServicoAnsDS_18_Tfservicoans_item_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWServicoAnsDS_17_Tfservicoans_item)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] like @lV75WWServicoAnsDS_17_Tfservicoans_item)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] like @lV75WWServicoAnsDS_17_Tfservicoans_item)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWServicoAnsDS_18_Tfservicoans_item_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoAns_Item] = @AV76WWServicoAnsDS_18_Tfservicoans_item_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoAns_Item] = @AV76WWServicoAnsDS_18_Tfservicoans_item_sel)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H007O2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H007O3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH007O2 ;
          prmH007O2 = new Object[] {
          new Object[] {"@lV61WWServicoAnsDS_3_Servicoans_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV61WWServicoAnsDS_3_Servicoans_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV62WWServicoAnsDS_4_Servico_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWServicoAnsDS_8_Servicoans_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV66WWServicoAnsDS_8_Servicoans_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV67WWServicoAnsDS_9_Servico_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWServicoAnsDS_13_Servicoans_item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV71WWServicoAnsDS_13_Servicoans_item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV72WWServicoAnsDS_14_Servico_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWServicoAnsDS_15_Tfservico_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV74WWServicoAnsDS_16_Tfservico_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWServicoAnsDS_17_Tfservicoans_item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV76WWServicoAnsDS_18_Tfservicoans_item_sel",SqlDbType.VarChar,40,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH007O3 ;
          prmH007O3 = new Object[] {
          new Object[] {"@lV61WWServicoAnsDS_3_Servicoans_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV61WWServicoAnsDS_3_Servicoans_item1",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV62WWServicoAnsDS_4_Servico_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66WWServicoAnsDS_8_Servicoans_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV66WWServicoAnsDS_8_Servicoans_item2",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV67WWServicoAnsDS_9_Servico_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV71WWServicoAnsDS_13_Servicoans_item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@lV71WWServicoAnsDS_13_Servicoans_item3",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV72WWServicoAnsDS_14_Servico_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWServicoAnsDS_15_Tfservico_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV74WWServicoAnsDS_16_Tfservico_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV75WWServicoAnsDS_17_Tfservicoans_item",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV76WWServicoAnsDS_18_Tfservicoans_item_sel",SqlDbType.VarChar,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H007O2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007O2,11,0,true,false )
             ,new CursorDef("H007O3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH007O3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                return;
       }
    }

 }

}
