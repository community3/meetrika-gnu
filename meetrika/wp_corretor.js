/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:24:41.41
*/
gx.evt.autoSkip = false;
gx.define('wp_corretor', false, function () {
   this.ServerClass =  "wp_corretor" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.A896LogResponsavel_Owner=gx.fn.getIntegerValue("LOGRESPONSAVEL_OWNER",'.') ;
      this.A1149LogResponsavel_OwnerEhContratante=gx.fn.getControlValue("LOGRESPONSAVEL_OWNEREHCONTRATANTE") ;
      this.AV11LogResponsavel_DemandaCod=gx.fn.getIntegerValue("vLOGRESPONSAVEL_DEMANDACOD",'.') ;
      this.AV75Pgmname=gx.fn.getControlValue("vPGMNAME") ;
      this.AV27TFLogResponsavel_Acao_Sels=gx.fn.getControlValue("vTFLOGRESPONSAVEL_ACAO_SELS") ;
      this.AV31TFLogResponsavel_Status_Sels=gx.fn.getControlValue("vTFLOGRESPONSAVEL_STATUS_SELS") ;
      this.AV35TFLogResponsavel_NovoStatus_Sels=gx.fn.getControlValue("vTFLOGRESPONSAVEL_NOVOSTATUS_SELS") ;
      this.A54Usuario_Ativo=gx.fn.getControlValue("USUARIO_ATIVO") ;
      this.A1Usuario_Codigo=gx.fn.getIntegerValue("USUARIO_CODIGO",'.') ;
      this.A58Usuario_PessoaNom=gx.fn.getControlValue("USUARIO_PESSOANOM") ;
      this.A891LogResponsavel_UsuarioCod=gx.fn.getIntegerValue("LOGRESPONSAVEL_USUARIOCOD",'.') ;
      this.A493ContagemResultado_DemandaFM=gx.fn.getControlValue("CONTAGEMRESULTADO_DEMANDAFM") ;
      this.AV18WWPContext=gx.fn.getControlValue("vWWPCONTEXT") ;
      this.AV70Codigo=gx.fn.getIntegerValue("vCODIGO",'.') ;
      this.A1553ContagemResultado_CntSrvCod=gx.fn.getIntegerValue("CONTAGEMRESULTADO_CNTSRVCOD",'.') ;
      this.A1611ContagemResultado_PrzTpDias=gx.fn.getControlValue("CONTAGEMRESULTADO_PRZTPDIAS") ;
      this.A798ContagemResultado_PFBFSImp=gx.fn.getDecimalValue("CONTAGEMRESULTADO_PFBFSIMP",'.',',') ;
      this.A471ContagemResultado_DataDmn=gx.fn.getDateValue("CONTAGEMRESULTADO_DATADMN") ;
      this.AV62NovaData=gx.fn.getDateValue("vNOVADATA") ;
      this.AV61UltimoPrazo=gx.fn.getDateTimeValue("vULTIMOPRAZO") ;
      this.AV67Selecionadas=gx.fn.getIntegerValue("vSELECIONADAS",'.') ;
      this.A1228ContratadaUsuario_AreaTrabalhoCod=gx.fn.getIntegerValue("CONTRATADAUSUARIO_AREATRABALHOCOD",'.') ;
      this.A69ContratadaUsuario_UsuarioCod=gx.fn.getIntegerValue("CONTRATADAUSUARIO_USUARIOCOD",'.') ;
      this.A66ContratadaUsuario_ContratadaCod=gx.fn.getIntegerValue("CONTRATADAUSUARIO_CONTRATADACOD",'.') ;
      this.A438Contratada_Sigla=gx.fn.getControlValue("CONTRATADA_SIGLA") ;
      this.AV71PrazoInicial=gx.fn.getIntegerValue("vPRAZOINICIAL",'.') ;
      this.AV58ContratoServicos_Codigo=gx.fn.getIntegerValue("vCONTRATOSERVICOS_CODIGO",'.') ;
      this.AV60Unidades=gx.fn.getDecimalValue("vUNIDADES",'.',',') ;
      this.AV59Complexidade=gx.fn.getIntegerValue("vCOMPLEXIDADE",'.') ;
      this.AV63TipoDias=gx.fn.getControlValue("vTIPODIAS") ;
   };
   this.Valid_Logresponsavel_demandacod=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(9) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("LOGRESPONSAVEL_DEMANDACOD", gx.fn.currentGridRowImpl(9));
         this.AnyError  = 0;
         this.StandaloneModal( );
         this.StandaloneNotModal( );

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Valid_Logresponsavel_codigo=function()
   {
      try {
         if(  gx.fn.currentGridRowImpl(9) ===0) {
            return true;
         }
         var gxballoon = gx.util.balloon.getNew("LOGRESPONSAVEL_CODIGO", gx.fn.currentGridRowImpl(9));
         this.AnyError  = 0;

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Prazosugerido=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vPRAZOSUGERIDO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV51PrazoSugerido)==0) || new gx.date.gxdate( this.AV51PrazoSugerido ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo Prazo Sugerido fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tflogresponsavel_datahora=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFLOGRESPONSAVEL_DATAHORA");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV20TFLogResponsavel_DataHora)==0) || new gx.date.gxdate( this.AV20TFLogResponsavel_DataHora ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFLog Responsavel_Data Hora fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tflogresponsavel_datahora_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFLOGRESPONSAVEL_DATAHORA_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV21TFLogResponsavel_DataHora_To)==0) || new gx.date.gxdate( this.AV21TFLogResponsavel_DataHora_To ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFLog Responsavel_Data Hora_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_logresponsavel_datahoraauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV22DDO_LogResponsavel_DataHoraAuxDate)==0) || new gx.date.gxdate( this.AV22DDO_LogResponsavel_DataHoraAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Log Responsavel_Data Hora Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_logresponsavel_datahoraauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV23DDO_LogResponsavel_DataHoraAuxDateTo)==0) || new gx.date.gxdate( this.AV23DDO_LogResponsavel_DataHoraAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Log Responsavel_Data Hora Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tflogresponsavel_prazo=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFLOGRESPONSAVEL_PRAZO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV38TFLogResponsavel_Prazo)==0) || new gx.date.gxdate( this.AV38TFLogResponsavel_Prazo ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFLog Responsavel_Prazo fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Tflogresponsavel_prazo_to=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vTFLOGRESPONSAVEL_PRAZO_TO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV39TFLogResponsavel_Prazo_To)==0) || new gx.date.gxdate( this.AV39TFLogResponsavel_Prazo_To ).compare( gx.date.ymdhmstot( 1753, 01, 01, 00, 00, 00) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo TFLog Responsavel_Prazo_To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_logresponsavel_prazoauxdate=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_LOGRESPONSAVEL_PRAZOAUXDATE");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV40DDO_LogResponsavel_PrazoAuxDate)==0) || new gx.date.gxdate( this.AV40DDO_LogResponsavel_PrazoAuxDate ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Log Responsavel_Prazo Aux Date fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.Validv_Ddo_logresponsavel_prazoauxdateto=function()
   {
      try {
         var gxballoon = gx.util.balloon.getNew("vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO");
         this.AnyError  = 0;
         if ( ! ( (new gx.date.gxdate('').compare(this.AV41DDO_LogResponsavel_PrazoAuxDateTo)==0) || new gx.date.gxdate( this.AV41DDO_LogResponsavel_PrazoAuxDateTo ).compare( gx.date.ymdtod( 1753, 01, 01) ) >= 0 ) )
         {
            try {
               gxballoon.setError("Campo DDO_Log Responsavel_Prazo Aux Date To fora do intervalo");
               this.AnyError = gx.num.trunc( 1 ,0) ;
            }
            catch(e){}
         }

      }
      catch(e){}
      try {
         if (gxballoon == null) return true; return gxballoon.show();
      }
      catch(e){}
      return true ;
   }
   this.e11r62_client=function()
   {
      this.executeServerEvent("DDO_LOGRESPONSAVEL_DATAHORA.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e12r62_client=function()
   {
      this.executeServerEvent("DDO_LOGRESPONSAVEL_ACAO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e13r62_client=function()
   {
      this.executeServerEvent("DDO_LOGRESPONSAVEL_STATUS.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e14r62_client=function()
   {
      this.executeServerEvent("DDO_LOGRESPONSAVEL_NOVOSTATUS.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e15r62_client=function()
   {
      this.executeServerEvent("DDO_LOGRESPONSAVEL_PRAZO.ONOPTIONCLICKED", false, null, true, true);
   };
   this.e16r62_client=function()
   {
      this.executeServerEvent("'FECHAR'", false, null, false, false);
   };
   this.e17r62_client=function()
   {
      this.executeServerEvent("'CORREGIR'", false, null, false, false);
   };
   this.e22r62_client=function()
   {
      this.executeServerEvent("VSELECTED.CLICK", true, arguments[0], false, false);
   };
   this.e18r62_client=function()
   {
      this.executeServerEvent("VNAOCONSIDERARPRAZOINICIAL.CLICK", true, null, false, true);
   };
   this.e23r62_client=function()
   {
      this.executeServerEvent("ENTER", true, arguments[0], false, false);
   };
   this.e24r62_client=function()
   {
      this.executeServerEvent("CANCEL", true, arguments[0], false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2,5,10,11,12,13,14,15,16,17,18,19,20,21,22,23,30,31,32,33,34,35,36,37,38,39,40,42,44,46,48,50,51];
   this.GXLastCtrlId =51;
   this.GridContainer = new gx.grid.grid(this, 2,"WbpLvl2",9,"Grid","Grid","GridContainer",this.CmpContext,this.IsMasterPage,"wp_corretor",[],false,1,false,true,0,true,false,false,"",0,"px","Novo registro",true,false,true,null,null,false,"",false,[1,1,1,1]);
   var GridContainer = this.GridContainer;
   GridContainer.addSingleLineEdit(892,10,"LOGRESPONSAVEL_DEMANDACOD","Demanda","","LogResponsavel_DemandaCod","int",0,"px",6,6,"right",null,[],892,"LogResponsavel_DemandaCod",true,0,false,false,"Attribute",1,"");
   GridContainer.addSingleLineEdit(1797,11,"LOGRESPONSAVEL_CODIGO","Log Responsavel_Codigo","","LogResponsavel_Codigo","int",0,"px",10,10,"right",null,[],1797,"LogResponsavel_Codigo",false,0,false,false,"Attribute",1,"");
   GridContainer.addSingleLineEdit(893,12,"LOGRESPONSAVEL_DATAHORA","","","LogResponsavel_DataHora","dtime",0,"px",14,14,"right",null,[],893,"LogResponsavel_DataHora",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addComboBox("Emissor",13,"vEMISSOR","Emissor","Emissor","int",null,0,true,false,0,"px","");
   GridContainer.addComboBox(894,14,"LOGRESPONSAVEL_ACAO","","LogResponsavel_Acao","char",null,0,true,false,0,"px","");
   GridContainer.addComboBox("Destino",15,"vDESTINO","Destino","Destino","int",null,0,true,false,0,"px","");
   GridContainer.addComboBox(1130,16,"LOGRESPONSAVEL_STATUS","","LogResponsavel_Status","char",null,0,true,false,0,"px","");
   GridContainer.addComboBox(1234,17,"LOGRESPONSAVEL_NOVOSTATUS","","LogResponsavel_NovoStatus","char",null,0,true,false,0,"px","");
   GridContainer.addSingleLineEdit(1177,18,"LOGRESPONSAVEL_PRAZO","","","LogResponsavel_Prazo","dtime",0,"px",14,14,"right",null,[],1177,"LogResponsavel_Prazo",true,5,false,false,"BootstrapAttribute",1,"");
   GridContainer.addSingleLineEdit(1227,19,"CONTAGEMRESULTADO_PRAZOINICIALDIAS","Dias","","ContagemResultado_PrazoInicialDias","int",0,"px",4,4,"right",null,[],1227,"ContagemResultado_PrazoInicialDias",true,0,false,false,"Attribute",1,"");
   GridContainer.addSingleLineEdit(1237,20,"CONTAGEMRESULTADO_PRAZOMAISDIAS","Extra","","ContagemResultado_PrazoMaisDias","int",0,"px",4,4,"right",null,[],1237,"ContagemResultado_PrazoMaisDias",true,0,false,false,"Attribute",1,"");
   GridContainer.addCheckBox("Selected",21,"vSELECTED","","","Selected","boolean","true","false","e22r62_client",true,false,0,"px","");
   GridContainer.addSingleLineEdit("Prazosugerido",22,"vPRAZOSUGERIDO","Prazo Sugerido","","PrazoSugerido","dtime",0,"px",14,14,"right",null,[],"Prazosugerido","PrazoSugerido",true,5,false,false,"Attribute",1,"");
   GridContainer.addSingleLineEdit("Dias",23,"vDIAS","Dias","","Dias","int",0,"px",4,4,"right",null,[],"Dias","Dias",true,0,false,false,"Attribute",1,"");
   this.setGrid(GridContainer);
   this.DDO_LOGRESPONSAVEL_DATAHORAContainer = gx.uc.getNew(this, 41, 5, "BootstrapDropDownOptions", "DDO_LOGRESPONSAVEL_DATAHORAContainer", "Ddo_logresponsavel_datahora");
   var DDO_LOGRESPONSAVEL_DATAHORAContainer = this.DDO_LOGRESPONSAVEL_DATAHORAContainer;
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Icon", "Icon", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Caption", "Caption", "", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.addV2CFunction('AV47DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_LOGRESPONSAVEL_DATAHORAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV47DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV47DDO_TitleSettingsIcons); });
   DDO_LOGRESPONSAVEL_DATAHORAContainer.addV2CFunction('AV19LogResponsavel_DataHoraTitleFilterData', "vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_LOGRESPONSAVEL_DATAHORAContainer.addC2VFunction(function(UC) { UC.ParentObject.AV19LogResponsavel_DataHoraTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA",UC.ParentObject.AV19LogResponsavel_DataHoraTitleFilterData); });
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Visible", "Visible", true, "bool");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setProp("Class", "Class", "", "char");
   DDO_LOGRESPONSAVEL_DATAHORAContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_LOGRESPONSAVEL_DATAHORAContainer.addEventHandler("OnOptionClicked", this.e11r62_client);
   this.setUserControl(DDO_LOGRESPONSAVEL_DATAHORAContainer);
   this.DDO_LOGRESPONSAVEL_ACAOContainer = gx.uc.getNew(this, 43, 5, "BootstrapDropDownOptions", "DDO_LOGRESPONSAVEL_ACAOContainer", "Ddo_logresponsavel_acao");
   var DDO_LOGRESPONSAVEL_ACAOContainer = this.DDO_LOGRESPONSAVEL_ACAOContainer;
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Icon", "Icon", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Caption", "Caption", "", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("IncludeFilter", "Includefilter", false, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilterType", "Filtertype", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FilterIsRange", "Filterisrange", false, "boolean");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("DataListType", "Datalisttype", "FixedValues", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", true, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "A:Atribui,B:StandBy,C:Captura,R:Rejeita,L:Libera,E:Encaminha,I:Importa,S:Solicita,D:Divergência,V:Resolvida,H:Homologa,Q:Liquida,P:Fatura,O:Aceite,N:Não Acata,M:Automática,F:Cumprido,T:Acata,X:Cancela,NO:Nota,U:Altera,UN:Rascunho,EA:Em Análise,RN:Reunião,BK:Desfaz,RE:Reinicio,PR:Prioridade", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("SearchButtonText", "Searchbuttontext", "Filtrar Selecionados", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_LOGRESPONSAVEL_ACAOContainer.addV2CFunction('AV47DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_LOGRESPONSAVEL_ACAOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV47DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV47DDO_TitleSettingsIcons); });
   DDO_LOGRESPONSAVEL_ACAOContainer.addV2CFunction('AV25LogResponsavel_AcaoTitleFilterData', "vLOGRESPONSAVEL_ACAOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_LOGRESPONSAVEL_ACAOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV25LogResponsavel_AcaoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vLOGRESPONSAVEL_ACAOTITLEFILTERDATA",UC.ParentObject.AV25LogResponsavel_AcaoTitleFilterData); });
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_LOGRESPONSAVEL_ACAOContainer.setProp("Class", "Class", "", "char");
   DDO_LOGRESPONSAVEL_ACAOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_LOGRESPONSAVEL_ACAOContainer.addEventHandler("OnOptionClicked", this.e12r62_client);
   this.setUserControl(DDO_LOGRESPONSAVEL_ACAOContainer);
   this.DDO_LOGRESPONSAVEL_STATUSContainer = gx.uc.getNew(this, 45, 5, "BootstrapDropDownOptions", "DDO_LOGRESPONSAVEL_STATUSContainer", "Ddo_logresponsavel_status");
   var DDO_LOGRESPONSAVEL_STATUSContainer = this.DDO_LOGRESPONSAVEL_STATUSContainer;
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Icon", "Icon", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Caption", "Caption", "", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("IncludeFilter", "Includefilter", false, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilterType", "Filtertype", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FilterIsRange", "Filterisrange", false, "boolean");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("DataListType", "Datalisttype", "FixedValues", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", true, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "B:Stand by,S:Solicitada,E:Em Análise,A:Em execução,R:Resolvida,C:Conferida,D:Rejeitada,H:Homologada,O:Aceite,P:A_Pagar,L:Liquidada,X:Cancelada,N:Não Faturada,J:Planejamento,I:Análise Planejamento,T:Validacao Técnica,Q:Validacao Qualidade,G:Em Homologação,M:Validação Mensuração,U:Rascunho", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("SearchButtonText", "Searchbuttontext", "Filtrar Selecionados", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_LOGRESPONSAVEL_STATUSContainer.addV2CFunction('AV47DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_LOGRESPONSAVEL_STATUSContainer.addC2VFunction(function(UC) { UC.ParentObject.AV47DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV47DDO_TitleSettingsIcons); });
   DDO_LOGRESPONSAVEL_STATUSContainer.addV2CFunction('AV29LogResponsavel_StatusTitleFilterData', "vLOGRESPONSAVEL_STATUSTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_LOGRESPONSAVEL_STATUSContainer.addC2VFunction(function(UC) { UC.ParentObject.AV29LogResponsavel_StatusTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vLOGRESPONSAVEL_STATUSTITLEFILTERDATA",UC.ParentObject.AV29LogResponsavel_StatusTitleFilterData); });
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Visible", "Visible", true, "bool");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_LOGRESPONSAVEL_STATUSContainer.setProp("Class", "Class", "", "char");
   DDO_LOGRESPONSAVEL_STATUSContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_LOGRESPONSAVEL_STATUSContainer.addEventHandler("OnOptionClicked", this.e13r62_client);
   this.setUserControl(DDO_LOGRESPONSAVEL_STATUSContainer);
   this.DDO_LOGRESPONSAVEL_NOVOSTATUSContainer = gx.uc.getNew(this, 47, 5, "BootstrapDropDownOptions", "DDO_LOGRESPONSAVEL_NOVOSTATUSContainer", "Ddo_logresponsavel_novostatus");
   var DDO_LOGRESPONSAVEL_NOVOSTATUSContainer = this.DDO_LOGRESPONSAVEL_NOVOSTATUSContainer;
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Icon", "Icon", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Caption", "Caption", "", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setDynProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("IncludeFilter", "Includefilter", false, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilterType", "Filtertype", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FilterIsRange", "Filterisrange", false, "boolean");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("IncludeDataList", "Includedatalist", true, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("DataListType", "Datalisttype", "FixedValues", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", true, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "B:Stand by,S:Solicitada,E:Em Análise,A:Em execução,R:Resolvida,C:Conferida,D:Rejeitada,H:Homologada,O:Aceite,P:A_Pagar,L:Liquidada,X:Cancelada,N:Não Faturada,J:Planejamento,I:Análise Planejamento,T:Validacao Técnica,Q:Validacao Qualidade,G:Em Homologação,M:Validação Mensuração,U:Rascunho", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("RangeFilterTo", "Rangefilterto", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("SearchButtonText", "Searchbuttontext", "Filtrar Selecionados", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.addV2CFunction('AV47DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.addC2VFunction(function(UC) { UC.ParentObject.AV47DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV47DDO_TitleSettingsIcons); });
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.addV2CFunction('AV33LogResponsavel_NovoStatusTitleFilterData', "vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.addC2VFunction(function(UC) { UC.ParentObject.AV33LogResponsavel_NovoStatusTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA",UC.ParentObject.AV33LogResponsavel_NovoStatusTitleFilterData); });
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Visible", "Visible", true, "bool");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setProp("Class", "Class", "", "char");
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.addEventHandler("OnOptionClicked", this.e14r62_client);
   this.setUserControl(DDO_LOGRESPONSAVEL_NOVOSTATUSContainer);
   this.DDO_LOGRESPONSAVEL_PRAZOContainer = gx.uc.getNew(this, 49, 5, "BootstrapDropDownOptions", "DDO_LOGRESPONSAVEL_PRAZOContainer", "Ddo_logresponsavel_prazo");
   var DDO_LOGRESPONSAVEL_PRAZOContainer = this.DDO_LOGRESPONSAVEL_PRAZOContainer;
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Icon", "Icon", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Caption", "Caption", "", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Tooltip", "Tooltip", "Opções", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Cls", "Cls", "ColumnSettings", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("ActiveEventKey", "Activeeventkey", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setDynProp("FilteredText_set", "Filteredtext_set", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("FilteredText_get", "Filteredtext_get", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setDynProp("FilteredTextTo_set", "Filteredtextto_set", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("FilteredTextTo_get", "Filteredtextto_get", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SelectedValue_set", "Selectedvalue_set", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SelectedValue_get", "Selectedvalue_get", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SelectedText_set", "Selectedtext_set", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SelectedText_get", "Selectedtext_get", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("GAMOAuthToken", "Gamoauthtoken", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("TitleControlAlign", "Titlecontrolalign", "Automatic", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("DropDownOptionsType", "Dropdownoptionstype", "GridTitleSettings", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setDynProp("TitleControlIdToReplace", "Titlecontrolidtoreplace", "", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("IncludeSortASC", "Includesortasc", false, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("IncludeSortDSC", "Includesortdsc", false, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SortedStatus", "Sortedstatus", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("IncludeFilter", "Includefilter", true, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("FilterType", "Filtertype", "Date", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("FilterIsRange", "Filterisrange", true, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("IncludeDataList", "Includedatalist", false, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("DataListType", "Datalisttype", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("AllowMultipleSelection", "Allowmultipleselection", false, "boolean");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("DataListFixedValues", "Datalistfixedvalues", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("DataListProc", "Datalistproc", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("DataListUpdateMinimumCharacters", "Datalistupdateminimumcharacters", '', "int");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("FixedFilters", "Fixedfilters", "", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SelectedFixedFilter", "Selectedfixedfilter", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("ColumnsSelectorValues", "Columnsselectorvalues", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SortASC", "Sortasc", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SortDSC", "Sortdsc", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("LoadingData", "Loadingdata", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("CleanFilter", "Cleanfilter", "Limpar pesquisa", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("RangeFilterFrom", "Rangefilterfrom", "Desde", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("RangeFilterTo", "Rangefilterto", "Até", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("NoResultsFound", "Noresultsfound", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("SearchButtonText", "Searchbuttontext", "Pesquisar", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("UpdateButtonText", "Updatebuttontext", "Update", "str");
   DDO_LOGRESPONSAVEL_PRAZOContainer.addV2CFunction('AV47DDO_TitleSettingsIcons', "vDDO_TITLESETTINGSICONS", 'SetDropDownOptionsTitleSettingsIcons');
   DDO_LOGRESPONSAVEL_PRAZOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV47DDO_TitleSettingsIcons=UC.GetDropDownOptionsTitleSettingsIcons();gx.fn.setControlValue("vDDO_TITLESETTINGSICONS",UC.ParentObject.AV47DDO_TitleSettingsIcons); });
   DDO_LOGRESPONSAVEL_PRAZOContainer.addV2CFunction('AV37LogResponsavel_PrazoTitleFilterData', "vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA", 'SetDropDownOptionsData');
   DDO_LOGRESPONSAVEL_PRAZOContainer.addC2VFunction(function(UC) { UC.ParentObject.AV37LogResponsavel_PrazoTitleFilterData=UC.GetDropDownOptionsData();gx.fn.setControlValue("vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA",UC.ParentObject.AV37LogResponsavel_PrazoTitleFilterData); });
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Visible", "Visible", true, "bool");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Enabled", "Enabled", true, "boolean");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setProp("Class", "Class", "", "char");
   DDO_LOGRESPONSAVEL_PRAZOContainer.setC2ShowFunction(function(UC) { UC.show(); });
   DDO_LOGRESPONSAVEL_PRAZOContainer.addEventHandler("OnOptionClicked", this.e15r62_client);
   this.setUserControl(DDO_LOGRESPONSAVEL_PRAZOContainer);
   GXValidFnc[2]={fld:"UNNAMEDTABLE1",grid:0};
   GXValidFnc[5]={lvl:0,type:"boolean",len:4,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vNAOCONSIDERARPRAZOINICIAL",gxz:"ZV72NaoConsiderarPrazoInicial",gxold:"OV72NaoConsiderarPrazoInicial",gxvar:"AV72NaoConsiderarPrazoInicial",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",v2v:function(Value){if(Value!==undefined)gx.O.AV72NaoConsiderarPrazoInicial=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV72NaoConsiderarPrazoInicial=gx.lang.booleanValue(Value)},v2c:function(){gx.fn.setCheckBoxValue("vNAOCONSIDERARPRAZOINICIAL",gx.O.AV72NaoConsiderarPrazoInicial,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV72NaoConsiderarPrazoInicial=gx.lang.booleanValue(this.val())},val:function(){return gx.fn.getControlValue("vNAOCONSIDERARPRAZOINICIAL")},nac:gx.falseFn,values:['true','false']};
   this.declareDomainHdlr( 5 , function() {
   });
   GXValidFnc[10]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:this.Valid_Logresponsavel_demandacod,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_DEMANDACOD",gxz:"Z892LogResponsavel_DemandaCod",gxold:"O892LogResponsavel_DemandaCod",gxvar:"A892LogResponsavel_DemandaCod",ucs:[],op:[19,20],ip:[19,20,30,11],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A892LogResponsavel_DemandaCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z892LogResponsavel_DemandaCod=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("LOGRESPONSAVEL_DEMANDACOD",row || gx.fn.currentGridRowImpl(9),gx.O.A892LogResponsavel_DemandaCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A892LogResponsavel_DemandaCod=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("LOGRESPONSAVEL_DEMANDACOD",row || gx.fn.currentGridRowImpl(9),'.')},nac:gx.falseFn};
   GXValidFnc[11]={lvl:2,type:"int",len:10,dec:0,sign:false,pic:"ZZZZZZZZZ9",ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:this.Valid_Logresponsavel_codigo,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_CODIGO",gxz:"Z1797LogResponsavel_Codigo",gxold:"O1797LogResponsavel_Codigo",gxvar:"A1797LogResponsavel_Codigo",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1797LogResponsavel_Codigo=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1797LogResponsavel_Codigo=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("LOGRESPONSAVEL_CODIGO",row || gx.fn.currentGridRowImpl(9),gx.O.A1797LogResponsavel_Codigo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1797LogResponsavel_Codigo=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("LOGRESPONSAVEL_CODIGO",row || gx.fn.currentGridRowImpl(9),'.')},nac:gx.falseFn};
   GXValidFnc[12]={lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_DATAHORA",gxz:"Z893LogResponsavel_DataHora",gxold:"O893LogResponsavel_DataHora",gxvar:"A893LogResponsavel_DataHora",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A893LogResponsavel_DataHora=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z893LogResponsavel_DataHora=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("LOGRESPONSAVEL_DATAHORA",row || gx.fn.currentGridRowImpl(9),gx.O.A893LogResponsavel_DataHora,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A893LogResponsavel_DataHora=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("LOGRESPONSAVEL_DATAHORA",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[13]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vEMISSOR",gxz:"ZV6Emissor",gxold:"OV6Emissor",gxvar:"AV6Emissor",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV6Emissor=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV6Emissor=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridComboBoxValue("vEMISSOR",row || gx.fn.currentGridRowImpl(9),gx.O.AV6Emissor)},c2v:function(){if(this.val()!==undefined)gx.O.AV6Emissor=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("vEMISSOR",row || gx.fn.currentGridRowImpl(9),'.')},nac:gx.falseFn};
   GXValidFnc[14]={lvl:2,type:"char",len:20,dec:0,sign:false,ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_ACAO",gxz:"Z894LogResponsavel_Acao",gxold:"O894LogResponsavel_Acao",gxvar:"A894LogResponsavel_Acao",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A894LogResponsavel_Acao=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z894LogResponsavel_Acao=Value},v2c:function(row){gx.fn.setGridComboBoxValue("LOGRESPONSAVEL_ACAO",row || gx.fn.currentGridRowImpl(9),gx.O.A894LogResponsavel_Acao);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A894LogResponsavel_Acao=this.val()},val:function(row){return gx.fn.getGridControlValue("LOGRESPONSAVEL_ACAO",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[15]={lvl:2,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:0,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDESTINO",gxz:"ZV5Destino",gxold:"OV5Destino",gxvar:"AV5Destino",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV5Destino=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV5Destino=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridComboBoxValue("vDESTINO",row || gx.fn.currentGridRowImpl(9),gx.O.AV5Destino)},c2v:function(){if(this.val()!==undefined)gx.O.AV5Destino=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("vDESTINO",row || gx.fn.currentGridRowImpl(9),'.')},nac:gx.falseFn};
   GXValidFnc[16]={lvl:2,type:"char",len:1,dec:0,sign:false,ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_STATUS",gxz:"Z1130LogResponsavel_Status",gxold:"O1130LogResponsavel_Status",gxvar:"A1130LogResponsavel_Status",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1130LogResponsavel_Status=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1130LogResponsavel_Status=Value},v2c:function(row){gx.fn.setGridComboBoxValue("LOGRESPONSAVEL_STATUS",row || gx.fn.currentGridRowImpl(9),gx.O.A1130LogResponsavel_Status);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1130LogResponsavel_Status=this.val()},val:function(row){return gx.fn.getGridControlValue("LOGRESPONSAVEL_STATUS",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[17]={lvl:2,type:"char",len:1,dec:0,sign:false,ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_NOVOSTATUS",gxz:"Z1234LogResponsavel_NovoStatus",gxold:"O1234LogResponsavel_NovoStatus",gxvar:"A1234LogResponsavel_NovoStatus",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"combo",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1234LogResponsavel_NovoStatus=Value},v2z:function(Value){if(Value!==undefined)gx.O.Z1234LogResponsavel_NovoStatus=Value},v2c:function(row){gx.fn.setGridComboBoxValue("LOGRESPONSAVEL_NOVOSTATUS",row || gx.fn.currentGridRowImpl(9),gx.O.A1234LogResponsavel_NovoStatus);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1234LogResponsavel_NovoStatus=this.val()},val:function(row){return gx.fn.getGridControlValue("LOGRESPONSAVEL_NOVOSTATUS",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[18]={lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_PRAZO",gxz:"Z1177LogResponsavel_Prazo",gxold:"O1177LogResponsavel_Prazo",gxvar:"A1177LogResponsavel_Prazo",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1177LogResponsavel_Prazo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1177LogResponsavel_Prazo=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("LOGRESPONSAVEL_PRAZO",row || gx.fn.currentGridRowImpl(9),gx.O.A1177LogResponsavel_Prazo,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A1177LogResponsavel_Prazo=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("LOGRESPONSAVEL_PRAZO",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[19]={lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_PRAZOINICIALDIAS",gxz:"Z1227ContagemResultado_PrazoInicialDias",gxold:"O1227ContagemResultado_PrazoInicialDias",gxvar:"A1227ContagemResultado_PrazoInicialDias",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1227ContagemResultado_PrazoInicialDias=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1227ContagemResultado_PrazoInicialDias=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_PRAZOINICIALDIAS",row || gx.fn.currentGridRowImpl(9),gx.O.A1227ContagemResultado_PrazoInicialDias,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1227ContagemResultado_PrazoInicialDias=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADO_PRAZOINICIALDIAS",row || gx.fn.currentGridRowImpl(9),'.')},nac:gx.falseFn};
   GXValidFnc[20]={lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:1,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"CONTAGEMRESULTADO_PRAZOMAISDIAS",gxz:"Z1237ContagemResultado_PrazoMaisDias",gxold:"O1237ContagemResultado_PrazoMaisDias",gxvar:"A1237ContagemResultado_PrazoMaisDias",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.A1237ContagemResultado_PrazoMaisDias=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z1237ContagemResultado_PrazoMaisDias=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("CONTAGEMRESULTADO_PRAZOMAISDIAS",row || gx.fn.currentGridRowImpl(9),gx.O.A1237ContagemResultado_PrazoMaisDias,0)},c2v:function(){if(this.val()!==undefined)gx.O.A1237ContagemResultado_PrazoMaisDias=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("CONTAGEMRESULTADO_PRAZOMAISDIAS",row || gx.fn.currentGridRowImpl(9),'.')},nac:gx.falseFn};
   GXValidFnc[21]={lvl:2,type:"boolean",len:4,dec:0,sign:false,ro:0,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vSELECTED",gxz:"ZV52Selected",gxold:"OV52Selected",gxvar:"AV52Selected",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"checkbox",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV52Selected=gx.lang.booleanValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV52Selected=gx.lang.booleanValue(Value)},v2c:function(row){gx.fn.setGridCheckBoxValue("vSELECTED",row || gx.fn.currentGridRowImpl(9),gx.O.AV52Selected,true);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.AV52Selected=gx.lang.booleanValue(this.val())},val:function(row){return gx.fn.getGridControlValue("vSELECTED",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn,values:['true','false']};
   GXValidFnc[22]={lvl:2,type:"dtime",len:8,dec:5,sign:false,ro:0,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:this.Validv_Prazosugerido,isvalid:null,rgrid:[],fld:"vPRAZOSUGERIDO",gxz:"ZV51PrazoSugerido",gxold:"OV51PrazoSugerido",gxvar:"AV51PrazoSugerido",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[22],ip:[22],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV51PrazoSugerido=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV51PrazoSugerido=gx.fn.toDatetimeValue(Value)},v2c:function(row){gx.fn.setGridControlValue("vPRAZOSUGERIDO",row || gx.fn.currentGridRowImpl(9),gx.O.AV51PrazoSugerido,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV51PrazoSugerido=gx.fn.toDatetimeValue(this.val())},val:function(row){return gx.fn.getGridDateTimeValue("vPRAZOSUGERIDO",row || gx.fn.currentGridRowImpl(9))},nac:gx.falseFn};
   GXValidFnc[23]={lvl:2,type:"int",len:4,dec:0,sign:false,pic:"ZZZ9",ro:0,isacc:0,grid:9,gxgrid:this.GridContainer,fnc:null,isvalid:null,rgrid:[],fld:"vDIAS",gxz:"ZV54Dias",gxold:"OV54Dias",gxvar:"AV54Dias",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",inputType:'text',v2v:function(Value){if(Value!==undefined)gx.O.AV54Dias=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV54Dias=gx.num.intval(Value)},v2c:function(row){gx.fn.setGridControlValue("vDIAS",row || gx.fn.currentGridRowImpl(9),gx.O.AV54Dias,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV54Dias=gx.num.intval(this.val())},val:function(row){return gx.fn.getGridIntegerValue("vDIAS",row || gx.fn.currentGridRowImpl(9),'.')},nac:gx.falseFn};
   GXValidFnc[30]={lvl:0,type:"int",len:6,dec:0,sign:false,pic:"ZZZZZ9",ro:1,grid:0,gxgrid:null,fnc:this.Valid_Logresponsavel_demandacod,isvalid:null,rgrid:[],fld:"LOGRESPONSAVEL_DEMANDACOD",gxz:"Z892LogResponsavel_DemandaCod",gxold:"O892LogResponsavel_DemandaCod",gxvar:"A892LogResponsavel_DemandaCod",ucs:[],op:[19,20],ip:[19,20,30,11],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.A892LogResponsavel_DemandaCod=gx.num.intval(Value)},v2z:function(Value){if(Value!==undefined)gx.O.Z892LogResponsavel_DemandaCod=gx.num.intval(Value)},v2c:function(){gx.fn.setControlValue("LOGRESPONSAVEL_DEMANDACOD",gx.O.A892LogResponsavel_DemandaCod,0);if (typeof(this.dom_hdl) == 'function') this.dom_hdl.call(gx.O);},c2v:function(){if(this.val()!==undefined)gx.O.A892LogResponsavel_DemandaCod=gx.num.intval(this.val())},val:function(){return gx.fn.getIntegerValue("LOGRESPONSAVEL_DEMANDACOD",'.')},nac:gx.falseFn};
   this.declareDomainHdlr( 30 , function() {
   });
   GXValidFnc[31]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tflogresponsavel_datahora,isvalid:null,rgrid:[],fld:"vTFLOGRESPONSAVEL_DATAHORA",gxz:"ZV20TFLogResponsavel_DataHora",gxold:"OV20TFLogResponsavel_DataHora",gxvar:"AV20TFLogResponsavel_DataHora",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[31],ip:[31],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV20TFLogResponsavel_DataHora=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV20TFLogResponsavel_DataHora=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFLOGRESPONSAVEL_DATAHORA",gx.O.AV20TFLogResponsavel_DataHora,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV20TFLogResponsavel_DataHora=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFLOGRESPONSAVEL_DATAHORA")},nac:gx.falseFn};
   GXValidFnc[32]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tflogresponsavel_datahora_to,isvalid:null,rgrid:[],fld:"vTFLOGRESPONSAVEL_DATAHORA_TO",gxz:"ZV21TFLogResponsavel_DataHora_To",gxold:"OV21TFLogResponsavel_DataHora_To",gxvar:"AV21TFLogResponsavel_DataHora_To",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[32],ip:[32],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV21TFLogResponsavel_DataHora_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV21TFLogResponsavel_DataHora_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFLOGRESPONSAVEL_DATAHORA_TO",gx.O.AV21TFLogResponsavel_DataHora_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV21TFLogResponsavel_DataHora_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFLOGRESPONSAVEL_DATAHORA_TO")},nac:gx.falseFn};
   GXValidFnc[33]={fld:"DDO_LOGRESPONSAVEL_DATAHORAAUXDATES",grid:0};
   GXValidFnc[34]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_logresponsavel_datahoraauxdate,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE",gxz:"ZV22DDO_LogResponsavel_DataHoraAuxDate",gxold:"OV22DDO_LogResponsavel_DataHoraAuxDate",gxvar:"AV22DDO_LogResponsavel_DataHoraAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[34],ip:[34],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV22DDO_LogResponsavel_DataHoraAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV22DDO_LogResponsavel_DataHoraAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE",gx.O.AV22DDO_LogResponsavel_DataHoraAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV22DDO_LogResponsavel_DataHoraAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATE")},nac:gx.falseFn};
   GXValidFnc[35]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_logresponsavel_datahoraauxdateto,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO",gxz:"ZV23DDO_LogResponsavel_DataHoraAuxDateTo",gxold:"OV23DDO_LogResponsavel_DataHoraAuxDateTo",gxvar:"AV23DDO_LogResponsavel_DataHoraAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[35],ip:[35],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV23DDO_LogResponsavel_DataHoraAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV23DDO_LogResponsavel_DataHoraAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO",gx.O.AV23DDO_LogResponsavel_DataHoraAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV23DDO_LogResponsavel_DataHoraAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_DATAHORAAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[36]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tflogresponsavel_prazo,isvalid:null,rgrid:[],fld:"vTFLOGRESPONSAVEL_PRAZO",gxz:"ZV38TFLogResponsavel_Prazo",gxold:"OV38TFLogResponsavel_Prazo",gxvar:"AV38TFLogResponsavel_Prazo",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[36],ip:[36],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV38TFLogResponsavel_Prazo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV38TFLogResponsavel_Prazo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFLOGRESPONSAVEL_PRAZO",gx.O.AV38TFLogResponsavel_Prazo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV38TFLogResponsavel_Prazo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFLOGRESPONSAVEL_PRAZO")},nac:gx.falseFn};
   GXValidFnc[37]={lvl:0,type:"dtime",len:8,dec:5,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Tflogresponsavel_prazo_to,isvalid:null,rgrid:[],fld:"vTFLOGRESPONSAVEL_PRAZO_TO",gxz:"ZV39TFLogResponsavel_Prazo_To",gxold:"OV39TFLogResponsavel_Prazo_To",gxvar:"AV39TFLogResponsavel_Prazo_To",dp:{f:0,st:true,wn:false,mf:false,pic:"99/99/99 99:99",dec:5},ucs:[],op:[37],ip:[37],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV39TFLogResponsavel_Prazo_To=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV39TFLogResponsavel_Prazo_To=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vTFLOGRESPONSAVEL_PRAZO_TO",gx.O.AV39TFLogResponsavel_Prazo_To,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV39TFLogResponsavel_Prazo_To=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getDateTimeValue("vTFLOGRESPONSAVEL_PRAZO_TO")},nac:gx.falseFn};
   GXValidFnc[38]={fld:"DDO_LOGRESPONSAVEL_PRAZOAUXDATES",grid:0};
   GXValidFnc[39]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_logresponsavel_prazoauxdate,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_PRAZOAUXDATE",gxz:"ZV40DDO_LogResponsavel_PrazoAuxDate",gxold:"OV40DDO_LogResponsavel_PrazoAuxDate",gxvar:"AV40DDO_LogResponsavel_PrazoAuxDate",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[39],ip:[39],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV40DDO_LogResponsavel_PrazoAuxDate=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV40DDO_LogResponsavel_PrazoAuxDate=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_PRAZOAUXDATE",gx.O.AV40DDO_LogResponsavel_PrazoAuxDate,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV40DDO_LogResponsavel_PrazoAuxDate=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_PRAZOAUXDATE")},nac:gx.falseFn};
   GXValidFnc[40]={lvl:0,type:"date",len:8,dec:0,sign:false,ro:0,grid:0,gxgrid:null,fnc:this.Validv_Ddo_logresponsavel_prazoauxdateto,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO",gxz:"ZV41DDO_LogResponsavel_PrazoAuxDateTo",gxold:"OV41DDO_LogResponsavel_PrazoAuxDateTo",gxvar:"AV41DDO_LogResponsavel_PrazoAuxDateTo",dp:{f:0,st:false,wn:false,mf:false,pic:"99/99/99",dec:0},ucs:[],op:[40],ip:[40],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV41DDO_LogResponsavel_PrazoAuxDateTo=gx.fn.toDatetimeValue(Value)},v2z:function(Value){if(Value!==undefined)gx.O.ZV41DDO_LogResponsavel_PrazoAuxDateTo=gx.fn.toDatetimeValue(Value)},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO",gx.O.AV41DDO_LogResponsavel_PrazoAuxDateTo,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV41DDO_LogResponsavel_PrazoAuxDateTo=gx.fn.toDatetimeValue(this.val())},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_PRAZOAUXDATETO")},nac:gx.falseFn};
   GXValidFnc[42]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE",gxz:"ZV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace",gxold:"OV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace",gxvar:"AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE",gx.O.AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[44]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE",gxz:"ZV28ddo_LogResponsavel_AcaoTitleControlIdToReplace",gxold:"OV28ddo_LogResponsavel_AcaoTitleControlIdToReplace",gxvar:"AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV28ddo_LogResponsavel_AcaoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE",gx.O.AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[46]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE",gxz:"ZV32ddo_LogResponsavel_StatusTitleControlIdToReplace",gxold:"OV32ddo_LogResponsavel_StatusTitleControlIdToReplace",gxvar:"AV32ddo_LogResponsavel_StatusTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV32ddo_LogResponsavel_StatusTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV32ddo_LogResponsavel_StatusTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE",gx.O.AV32ddo_LogResponsavel_StatusTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV32ddo_LogResponsavel_StatusTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[48]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE",gxz:"ZV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace",gxold:"OV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace",gxvar:"AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE",gx.O.AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[50]={lvl:0,type:"svchar",len:300,dec:0,sign:false,ro:0,multiline:true,grid:0,gxgrid:null,fnc:null,isvalid:null,rgrid:[],fld:"vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE",gxz:"ZV42ddo_LogResponsavel_PrazoTitleControlIdToReplace",gxold:"OV42ddo_LogResponsavel_PrazoTitleControlIdToReplace",gxvar:"AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace",ucs:[],op:[],ip:[],nacdep:[],ctrltype:"edit",v2v:function(Value){if(Value!==undefined)gx.O.AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace=Value},v2z:function(Value){if(Value!==undefined)gx.O.ZV42ddo_LogResponsavel_PrazoTitleControlIdToReplace=Value},v2c:function(){gx.fn.setControlValue("vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE",gx.O.AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace,0)},c2v:function(){if(this.val()!==undefined)gx.O.AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace=this.val()},val:function(){return gx.fn.getControlValue("vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE")},nac:gx.falseFn};
   GXValidFnc[51]={fld:"TBJAVA", format:1,grid:0};
   this.AV72NaoConsiderarPrazoInicial = false ;
   this.ZV72NaoConsiderarPrazoInicial = false ;
   this.OV72NaoConsiderarPrazoInicial = false ;
   this.Z892LogResponsavel_DemandaCod = 0 ;
   this.O892LogResponsavel_DemandaCod = 0 ;
   this.Z1797LogResponsavel_Codigo = 0 ;
   this.O1797LogResponsavel_Codigo = 0 ;
   this.Z893LogResponsavel_DataHora = gx.date.nullDate() ;
   this.O893LogResponsavel_DataHora = gx.date.nullDate() ;
   this.ZV6Emissor = 0 ;
   this.OV6Emissor = 0 ;
   this.Z894LogResponsavel_Acao = "" ;
   this.O894LogResponsavel_Acao = "" ;
   this.ZV5Destino = 0 ;
   this.OV5Destino = 0 ;
   this.Z1130LogResponsavel_Status = "" ;
   this.O1130LogResponsavel_Status = "" ;
   this.Z1234LogResponsavel_NovoStatus = "" ;
   this.O1234LogResponsavel_NovoStatus = "" ;
   this.Z1177LogResponsavel_Prazo = gx.date.nullDate() ;
   this.O1177LogResponsavel_Prazo = gx.date.nullDate() ;
   this.Z1227ContagemResultado_PrazoInicialDias = 0 ;
   this.O1227ContagemResultado_PrazoInicialDias = 0 ;
   this.Z1237ContagemResultado_PrazoMaisDias = 0 ;
   this.O1237ContagemResultado_PrazoMaisDias = 0 ;
   this.ZV52Selected = false ;
   this.OV52Selected = false ;
   this.ZV51PrazoSugerido = gx.date.nullDate() ;
   this.OV51PrazoSugerido = gx.date.nullDate() ;
   this.ZV54Dias = 0 ;
   this.OV54Dias = 0 ;
   this.A892LogResponsavel_DemandaCod = 0 ;
   this.AV20TFLogResponsavel_DataHora = gx.date.nullDate() ;
   this.ZV20TFLogResponsavel_DataHora = gx.date.nullDate() ;
   this.OV20TFLogResponsavel_DataHora = gx.date.nullDate() ;
   this.AV21TFLogResponsavel_DataHora_To = gx.date.nullDate() ;
   this.ZV21TFLogResponsavel_DataHora_To = gx.date.nullDate() ;
   this.OV21TFLogResponsavel_DataHora_To = gx.date.nullDate() ;
   this.AV22DDO_LogResponsavel_DataHoraAuxDate = gx.date.nullDate() ;
   this.ZV22DDO_LogResponsavel_DataHoraAuxDate = gx.date.nullDate() ;
   this.OV22DDO_LogResponsavel_DataHoraAuxDate = gx.date.nullDate() ;
   this.AV23DDO_LogResponsavel_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.ZV23DDO_LogResponsavel_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.OV23DDO_LogResponsavel_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.AV38TFLogResponsavel_Prazo = gx.date.nullDate() ;
   this.ZV38TFLogResponsavel_Prazo = gx.date.nullDate() ;
   this.OV38TFLogResponsavel_Prazo = gx.date.nullDate() ;
   this.AV39TFLogResponsavel_Prazo_To = gx.date.nullDate() ;
   this.ZV39TFLogResponsavel_Prazo_To = gx.date.nullDate() ;
   this.OV39TFLogResponsavel_Prazo_To = gx.date.nullDate() ;
   this.AV40DDO_LogResponsavel_PrazoAuxDate = gx.date.nullDate() ;
   this.ZV40DDO_LogResponsavel_PrazoAuxDate = gx.date.nullDate() ;
   this.OV40DDO_LogResponsavel_PrazoAuxDate = gx.date.nullDate() ;
   this.AV41DDO_LogResponsavel_PrazoAuxDateTo = gx.date.nullDate() ;
   this.ZV41DDO_LogResponsavel_PrazoAuxDateTo = gx.date.nullDate() ;
   this.OV41DDO_LogResponsavel_PrazoAuxDateTo = gx.date.nullDate() ;
   this.AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace = "" ;
   this.ZV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace = "" ;
   this.OV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace = "" ;
   this.AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace = "" ;
   this.ZV28ddo_LogResponsavel_AcaoTitleControlIdToReplace = "" ;
   this.OV28ddo_LogResponsavel_AcaoTitleControlIdToReplace = "" ;
   this.AV32ddo_LogResponsavel_StatusTitleControlIdToReplace = "" ;
   this.ZV32ddo_LogResponsavel_StatusTitleControlIdToReplace = "" ;
   this.OV32ddo_LogResponsavel_StatusTitleControlIdToReplace = "" ;
   this.AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = "" ;
   this.ZV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = "" ;
   this.OV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = "" ;
   this.AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace = "" ;
   this.ZV42ddo_LogResponsavel_PrazoTitleControlIdToReplace = "" ;
   this.OV42ddo_LogResponsavel_PrazoTitleControlIdToReplace = "" ;
   this.AV72NaoConsiderarPrazoInicial = false ;
   this.AV20TFLogResponsavel_DataHora = gx.date.nullDate() ;
   this.AV21TFLogResponsavel_DataHora_To = gx.date.nullDate() ;
   this.AV22DDO_LogResponsavel_DataHoraAuxDate = gx.date.nullDate() ;
   this.AV23DDO_LogResponsavel_DataHoraAuxDateTo = gx.date.nullDate() ;
   this.AV38TFLogResponsavel_Prazo = gx.date.nullDate() ;
   this.AV39TFLogResponsavel_Prazo_To = gx.date.nullDate() ;
   this.AV40DDO_LogResponsavel_PrazoAuxDate = gx.date.nullDate() ;
   this.AV41DDO_LogResponsavel_PrazoAuxDateTo = gx.date.nullDate() ;
   this.AV47DDO_TitleSettingsIcons = {} ;
   this.AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace = "" ;
   this.AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace = "" ;
   this.AV32ddo_LogResponsavel_StatusTitleControlIdToReplace = "" ;
   this.AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace = "" ;
   this.AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace = "" ;
   this.AV11LogResponsavel_DemandaCod = 0 ;
   this.A891LogResponsavel_UsuarioCod = 0 ;
   this.A493ContagemResultado_DemandaFM = "" ;
   this.A1553ContagemResultado_CntSrvCod = 0 ;
   this.A1611ContagemResultado_PrzTpDias = "" ;
   this.A798ContagemResultado_PFBFSImp = 0 ;
   this.A471ContagemResultado_DataDmn = gx.date.nullDate() ;
   this.A40000ContratadaUsuario_UsuarioCod = 0 ;
   this.A896LogResponsavel_Owner = 0 ;
   this.A1149LogResponsavel_OwnerEhContratante = false ;
   this.A892LogResponsavel_DemandaCod = 0 ;
   this.A1797LogResponsavel_Codigo = 0 ;
   this.A893LogResponsavel_DataHora = gx.date.nullDate() ;
   this.AV6Emissor = 0 ;
   this.A894LogResponsavel_Acao = "" ;
   this.AV5Destino = 0 ;
   this.A1130LogResponsavel_Status = "" ;
   this.A1234LogResponsavel_NovoStatus = "" ;
   this.A1177LogResponsavel_Prazo = gx.date.nullDate() ;
   this.A1227ContagemResultado_PrazoInicialDias = 0 ;
   this.A1237ContagemResultado_PrazoMaisDias = 0 ;
   this.AV52Selected = false ;
   this.AV51PrazoSugerido = gx.date.nullDate() ;
   this.AV54Dias = 0 ;
   this.A54Usuario_Ativo = false ;
   this.A58Usuario_PessoaNom = "" ;
   this.A1Usuario_Codigo = 0 ;
   this.A57Usuario_PessoaCod = 0 ;
   this.A69ContratadaUsuario_UsuarioCod = 0 ;
   this.A1228ContratadaUsuario_AreaTrabalhoCod = 0 ;
   this.A66ContratadaUsuario_ContratadaCod = 0 ;
   this.A438Contratada_Sigla = "" ;
   this.AV75Pgmname = "" ;
   this.AV27TFLogResponsavel_Acao_Sels = [ ] ;
   this.AV31TFLogResponsavel_Status_Sels = [ ] ;
   this.AV35TFLogResponsavel_NovoStatus_Sels = [ ] ;
   this.AV18WWPContext = {} ;
   this.AV70Codigo = 0 ;
   this.AV62NovaData = gx.date.nullDate() ;
   this.AV61UltimoPrazo = gx.date.nullDate() ;
   this.AV67Selecionadas = 0 ;
   this.AV71PrazoInicial = 0 ;
   this.AV58ContratoServicos_Codigo = 0 ;
   this.AV60Unidades = 0 ;
   this.AV59Complexidade = 0 ;
   this.AV63TipoDias = "" ;
   this.Events = {"e11r62_client": ["DDO_LOGRESPONSAVEL_DATAHORA.ONOPTIONCLICKED", true] ,"e12r62_client": ["DDO_LOGRESPONSAVEL_ACAO.ONOPTIONCLICKED", true] ,"e13r62_client": ["DDO_LOGRESPONSAVEL_STATUS.ONOPTIONCLICKED", true] ,"e14r62_client": ["DDO_LOGRESPONSAVEL_NOVOSTATUS.ONOPTIONCLICKED", true] ,"e15r62_client": ["DDO_LOGRESPONSAVEL_PRAZO.ONOPTIONCLICKED", true] ,"e16r62_client": ["'FECHAR'", true] ,"e17r62_client": ["'CORREGIR'", true] ,"e22r62_client": ["VSELECTED.CLICK", true] ,"e18r62_client": ["VNAOCONSIDERARPRAZOINICIAL.CLICK", true] ,"e23r62_client": ["ENTER", true] ,"e24r62_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],[{av:'AV19LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV25LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV29LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV33LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV37LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DATAHORA","Title")',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{ctrl:'LOGRESPONSAVEL_ACAO'},{ctrl:'LOGRESPONSAVEL_STATUS'},{ctrl:'LOGRESPONSAVEL_NOVOSTATUS'},{ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_PRAZO","Title")',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'AV68Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["DDO_LOGRESPONSAVEL_DATAHORA.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'this.DDO_LOGRESPONSAVEL_DATAHORAContainer.ActiveEventKey',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'ActiveEventKey'},{av:'this.DDO_LOGRESPONSAVEL_DATAHORAContainer.FilteredText_get',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'FilteredText_get'},{av:'this.DDO_LOGRESPONSAVEL_DATAHORAContainer.FilteredTextTo_get',ctrl:'DDO_LOGRESPONSAVEL_DATAHORA',prop:'FilteredTextTo_get'}],[{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''}]];
   this.EvtParms["DDO_LOGRESPONSAVEL_ACAO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'this.DDO_LOGRESPONSAVEL_ACAOContainer.ActiveEventKey',ctrl:'DDO_LOGRESPONSAVEL_ACAO',prop:'ActiveEventKey'},{av:'this.DDO_LOGRESPONSAVEL_ACAOContainer.SelectedValue_get',ctrl:'DDO_LOGRESPONSAVEL_ACAO',prop:'SelectedValue_get'}],[{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null}]];
   this.EvtParms["DDO_LOGRESPONSAVEL_STATUS.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'this.DDO_LOGRESPONSAVEL_STATUSContainer.ActiveEventKey',ctrl:'DDO_LOGRESPONSAVEL_STATUS',prop:'ActiveEventKey'},{av:'this.DDO_LOGRESPONSAVEL_STATUSContainer.SelectedValue_get',ctrl:'DDO_LOGRESPONSAVEL_STATUS',prop:'SelectedValue_get'}],[{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null}]];
   this.EvtParms["DDO_LOGRESPONSAVEL_NOVOSTATUS.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'this.DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.ActiveEventKey',ctrl:'DDO_LOGRESPONSAVEL_NOVOSTATUS',prop:'ActiveEventKey'},{av:'this.DDO_LOGRESPONSAVEL_NOVOSTATUSContainer.SelectedValue_get',ctrl:'DDO_LOGRESPONSAVEL_NOVOSTATUS',prop:'SelectedValue_get'}],[{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null}]];
   this.EvtParms["DDO_LOGRESPONSAVEL_PRAZO.ONOPTIONCLICKED"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'this.DDO_LOGRESPONSAVEL_PRAZOContainer.ActiveEventKey',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'ActiveEventKey'},{av:'this.DDO_LOGRESPONSAVEL_PRAZOContainer.FilteredText_get',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'FilteredText_get'},{av:'this.DDO_LOGRESPONSAVEL_PRAZOContainer.FilteredTextTo_get',ctrl:'DDO_LOGRESPONSAVEL_PRAZO',prop:'FilteredTextTo_get'}],[{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''}]];
   this.EvtParms["GRID.LOAD"] = [[{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''}],[{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Caption'},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV54Dias',fld:'vDIAS',pic:'ZZZ9',nv:0}]];
   this.EvtParms["VSELECTED.CLICK"] = [[{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0}],[{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Caption'},{ctrl:'CORREGIR',prop:'Visible'}]];
   this.EvtParms["VNAOCONSIDERARPRAZOINICIAL.CLICK"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''}],[]];
   this.EvtParms["'FECHAR'"] = [[],[]];
   this.EvtParms["'CORREGIR'"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',grid:9,pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',grid:9,pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',grid:9,pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',grid:9,pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',grid:9,pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',grid:9,pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',grid:9,pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',grid:9,pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',grid:9,pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',grid:9,pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',grid:9,pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',grid:9,pic:'ZZZZZZZZZ9',nv:0},{av:'AV54Dias',fld:'vDIAS',grid:9,pic:'ZZZ9',nv:0}],[{av:'A1797LogResponsavel_Codigo',fld:'LOGRESPONSAVEL_CODIGO',pic:'ZZZZZZZZZ9',nv:0}]];
   this.EvtParms["GRID_FIRSTPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],[{av:'AV19LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV25LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV29LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV33LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV37LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DATAHORA","Title")',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{ctrl:'LOGRESPONSAVEL_ACAO'},{ctrl:'LOGRESPONSAVEL_STATUS'},{ctrl:'LOGRESPONSAVEL_NOVOSTATUS'},{ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_PRAZO","Title")',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'AV68Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRID_PREVPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],[{av:'AV19LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV25LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV29LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV33LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV37LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DATAHORA","Title")',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{ctrl:'LOGRESPONSAVEL_ACAO'},{ctrl:'LOGRESPONSAVEL_STATUS'},{ctrl:'LOGRESPONSAVEL_NOVOSTATUS'},{ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_PRAZO","Title")',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'AV68Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRID_NEXTPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],[{av:'AV19LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV25LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV29LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV33LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV37LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DATAHORA","Title")',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{ctrl:'LOGRESPONSAVEL_ACAO'},{ctrl:'LOGRESPONSAVEL_STATUS'},{ctrl:'LOGRESPONSAVEL_NOVOSTATUS'},{ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_PRAZO","Title")',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'AV68Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]];
   this.EvtParms["GRID_LASTPAGE"] = [[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DEMANDACOD","Visible")',ctrl:'LOGRESPONSAVEL_DEMANDACOD',prop:'Visible'},{av:'A896LogResponsavel_Owner',fld:'LOGRESPONSAVEL_OWNER',pic:'ZZZZZ9',nv:0},{av:'A891LogResponsavel_UsuarioCod',fld:'LOGRESPONSAVEL_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A493ContagemResultado_DemandaFM',fld:'CONTAGEMRESULTADO_DEMANDAFM',pic:'',nv:''},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'A892LogResponsavel_DemandaCod',fld:'LOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1553ContagemResultado_CntSrvCod',fld:'CONTAGEMRESULTADO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'A1611ContagemResultado_PrzTpDias',fld:'CONTAGEMRESULTADO_PRZTPDIAS',pic:'',nv:''},{av:'A798ContagemResultado_PFBFSImp',fld:'CONTAGEMRESULTADO_PFBFSIMP',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A1234LogResponsavel_NovoStatus',fld:'LOGRESPONSAVEL_NOVOSTATUS',pic:'',hsh:true,nv:''},{av:'A1237ContagemResultado_PrazoMaisDias',fld:'CONTAGEMRESULTADO_PRAZOMAISDIAS',pic:'ZZZ9',nv:0},{av:'A1227ContagemResultado_PrazoInicialDias',fld:'CONTAGEMRESULTADO_PRAZOINICIALDIAS',pic:'ZZZ9',nv:0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'AV62NovaData',fld:'vNOVADATA',pic:'',nv:''},{av:'AV61UltimoPrazo',fld:'vULTIMOPRAZO',pic:'99/99/99 99:99',nv:''},{av:'A893LogResponsavel_DataHora',fld:'LOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A1149LogResponsavel_OwnerEhContratante',fld:'LOGRESPONSAVEL_OWNEREHCONTRATANTE',pic:'',nv:false},{av:'A894LogResponsavel_Acao',fld:'LOGRESPONSAVEL_ACAO',pic:'',hsh:true,nv:''},{av:'AV51PrazoSugerido',fld:'vPRAZOSUGERIDO',pic:'99/99/99 99:99',nv:''},{av:'A1177LogResponsavel_Prazo',fld:'LOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV52Selected',fld:'vSELECTED',pic:'',nv:false},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A438Contratada_Sigla',fld:'CONTRATADA_SIGLA',pic:'@!',nv:''},{av:'AV71PrazoInicial',fld:'vPRAZOINICIAL',pic:'ZZZ9',nv:0},{av:'AV72NaoConsiderarPrazoInicial',fld:'vNAOCONSIDERARPRAZOINICIAL',pic:'',nv:false},{av:'AV58ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV60Unidades',fld:'vUNIDADES',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV59Complexidade',fld:'vCOMPLEXIDADE',pic:'ZZ9',nv:0},{av:'AV63TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV24ddo_LogResponsavel_DataHoraTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_DATAHORATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV28ddo_LogResponsavel_AcaoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_ACAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV32ddo_LogResponsavel_StatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36ddo_LogResponsavel_NovoStatusTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_NOVOSTATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV42ddo_LogResponsavel_PrazoTitleControlIdToReplace',fld:'vDDO_LOGRESPONSAVEL_PRAZOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20TFLogResponsavel_DataHora',fld:'vTFLOGRESPONSAVEL_DATAHORA',pic:'99/99/99 99:99',nv:''},{av:'AV21TFLogResponsavel_DataHora_To',fld:'vTFLOGRESPONSAVEL_DATAHORA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV27TFLogResponsavel_Acao_Sels',fld:'vTFLOGRESPONSAVEL_ACAO_SELS',pic:'',nv:null},{av:'AV31TFLogResponsavel_Status_Sels',fld:'vTFLOGRESPONSAVEL_STATUS_SELS',pic:'',nv:null},{av:'AV35TFLogResponsavel_NovoStatus_Sels',fld:'vTFLOGRESPONSAVEL_NOVOSTATUS_SELS',pic:'',nv:null},{av:'AV38TFLogResponsavel_Prazo',fld:'vTFLOGRESPONSAVEL_PRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV39TFLogResponsavel_Prazo_To',fld:'vTFLOGRESPONSAVEL_PRAZO_TO',pic:'99/99/99 99:99',nv:''},{av:'AV11LogResponsavel_DemandaCod',fld:'vLOGRESPONSAVEL_DEMANDACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A54Usuario_Ativo',fld:'USUARIO_ATIVO',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A58Usuario_PessoaNom',fld:'USUARIO_PESSOANOM',pic:'@!',nv:''}],[{av:'AV19LogResponsavel_DataHoraTitleFilterData',fld:'vLOGRESPONSAVEL_DATAHORATITLEFILTERDATA',pic:'',nv:null},{av:'AV25LogResponsavel_AcaoTitleFilterData',fld:'vLOGRESPONSAVEL_ACAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV29LogResponsavel_StatusTitleFilterData',fld:'vLOGRESPONSAVEL_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV33LogResponsavel_NovoStatusTitleFilterData',fld:'vLOGRESPONSAVEL_NOVOSTATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV37LogResponsavel_PrazoTitleFilterData',fld:'vLOGRESPONSAVEL_PRAZOTITLEFILTERDATA',pic:'',nv:null},{av:'AV18WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_DATAHORA","Title")',ctrl:'LOGRESPONSAVEL_DATAHORA',prop:'Title'},{ctrl:'LOGRESPONSAVEL_ACAO'},{ctrl:'LOGRESPONSAVEL_STATUS'},{ctrl:'LOGRESPONSAVEL_NOVOSTATUS'},{ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Titleformat',hsh:true},{av:'gx.fn.getCtrlProperty("LOGRESPONSAVEL_PRAZO","Title")',ctrl:'LOGRESPONSAVEL_PRAZO',prop:'Title'},{av:'AV68Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV67Selecionadas',fld:'vSELECIONADAS',pic:'ZZZ9',nv:0},{av:'AV70Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{ctrl:'CORREGIR',prop:'Visible'},{av:'AV6Emissor',fld:'vEMISSOR',pic:'ZZZZZ9',nv:0},{av:'AV5Destino',fld:'vDESTINO',pic:'ZZZZZ9',nv:0}]];
   this.setVCMap("A896LogResponsavel_Owner", "LOGRESPONSAVEL_OWNER", 0, "int");
   this.setVCMap("A1149LogResponsavel_OwnerEhContratante", "LOGRESPONSAVEL_OWNEREHCONTRATANTE", 0, "boolean");
   this.setVCMap("AV11LogResponsavel_DemandaCod", "vLOGRESPONSAVEL_DEMANDACOD", 0, "int");
   this.setVCMap("AV75Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV27TFLogResponsavel_Acao_Sels", "vTFLOGRESPONSAVEL_ACAO_SELS", 0, "Collchar");
   this.setVCMap("AV31TFLogResponsavel_Status_Sels", "vTFLOGRESPONSAVEL_STATUS_SELS", 0, "Collchar");
   this.setVCMap("AV35TFLogResponsavel_NovoStatus_Sels", "vTFLOGRESPONSAVEL_NOVOSTATUS_SELS", 0, "Collchar");
   this.setVCMap("A54Usuario_Ativo", "USUARIO_ATIVO", 0, "boolean");
   this.setVCMap("A1Usuario_Codigo", "USUARIO_CODIGO", 0, "int");
   this.setVCMap("A58Usuario_PessoaNom", "USUARIO_PESSOANOM", 0, "char");
   this.setVCMap("A891LogResponsavel_UsuarioCod", "LOGRESPONSAVEL_USUARIOCOD", 0, "int");
   this.setVCMap("A493ContagemResultado_DemandaFM", "CONTAGEMRESULTADO_DEMANDAFM", 0, "svchar");
   this.setVCMap("AV18WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV70Codigo", "vCODIGO", 0, "int");
   this.setVCMap("A1553ContagemResultado_CntSrvCod", "CONTAGEMRESULTADO_CNTSRVCOD", 0, "int");
   this.setVCMap("A1611ContagemResultado_PrzTpDias", "CONTAGEMRESULTADO_PRZTPDIAS", 0, "char");
   this.setVCMap("A798ContagemResultado_PFBFSImp", "CONTAGEMRESULTADO_PFBFSIMP", 0, "decimal");
   this.setVCMap("A471ContagemResultado_DataDmn", "CONTAGEMRESULTADO_DATADMN", 0, "date");
   this.setVCMap("AV62NovaData", "vNOVADATA", 0, "date");
   this.setVCMap("AV61UltimoPrazo", "vULTIMOPRAZO", 0, "dtime");
   this.setVCMap("AV67Selecionadas", "vSELECIONADAS", 0, "int");
   this.setVCMap("A1228ContratadaUsuario_AreaTrabalhoCod", "CONTRATADAUSUARIO_AREATRABALHOCOD", 0, "int");
   this.setVCMap("A69ContratadaUsuario_UsuarioCod", "CONTRATADAUSUARIO_USUARIOCOD", 0, "int");
   this.setVCMap("A66ContratadaUsuario_ContratadaCod", "CONTRATADAUSUARIO_CONTRATADACOD", 0, "int");
   this.setVCMap("A438Contratada_Sigla", "CONTRATADA_SIGLA", 0, "char");
   this.setVCMap("AV71PrazoInicial", "vPRAZOINICIAL", 0, "int");
   this.setVCMap("AV58ContratoServicos_Codigo", "vCONTRATOSERVICOS_CODIGO", 0, "int");
   this.setVCMap("AV60Unidades", "vUNIDADES", 0, "decimal");
   this.setVCMap("AV59Complexidade", "vCOMPLEXIDADE", 0, "int");
   this.setVCMap("AV63TipoDias", "vTIPODIAS", 0, "char");
   this.setVCMap("AV11LogResponsavel_DemandaCod", "vLOGRESPONSAVEL_DEMANDACOD", 0, "int");
   this.setVCMap("AV75Pgmname", "vPGMNAME", 0, "char");
   this.setVCMap("AV27TFLogResponsavel_Acao_Sels", "vTFLOGRESPONSAVEL_ACAO_SELS", 0, "Collchar");
   this.setVCMap("AV31TFLogResponsavel_Status_Sels", "vTFLOGRESPONSAVEL_STATUS_SELS", 0, "Collchar");
   this.setVCMap("AV35TFLogResponsavel_NovoStatus_Sels", "vTFLOGRESPONSAVEL_NOVOSTATUS_SELS", 0, "Collchar");
   this.setVCMap("A54Usuario_Ativo", "USUARIO_ATIVO", 0, "boolean");
   this.setVCMap("A1Usuario_Codigo", "USUARIO_CODIGO", 0, "int");
   this.setVCMap("A58Usuario_PessoaNom", "USUARIO_PESSOANOM", 0, "char");
   this.setVCMap("A891LogResponsavel_UsuarioCod", "LOGRESPONSAVEL_USUARIOCOD", 0, "int");
   this.setVCMap("A493ContagemResultado_DemandaFM", "CONTAGEMRESULTADO_DEMANDAFM", 0, "svchar");
   this.setVCMap("AV18WWPContext", "vWWPCONTEXT", 0, "WWPBaseObjects\WWPContext");
   this.setVCMap("AV70Codigo", "vCODIGO", 0, "int");
   this.setVCMap("A1553ContagemResultado_CntSrvCod", "CONTAGEMRESULTADO_CNTSRVCOD", 0, "int");
   this.setVCMap("A1611ContagemResultado_PrzTpDias", "CONTAGEMRESULTADO_PRZTPDIAS", 0, "char");
   this.setVCMap("A798ContagemResultado_PFBFSImp", "CONTAGEMRESULTADO_PFBFSIMP", 0, "decimal");
   this.setVCMap("A471ContagemResultado_DataDmn", "CONTAGEMRESULTADO_DATADMN", 0, "date");
   this.setVCMap("AV62NovaData", "vNOVADATA", 0, "date");
   this.setVCMap("AV61UltimoPrazo", "vULTIMOPRAZO", 0, "dtime");
   this.setVCMap("AV67Selecionadas", "vSELECIONADAS", 0, "int");
   this.setVCMap("A1228ContratadaUsuario_AreaTrabalhoCod", "CONTRATADAUSUARIO_AREATRABALHOCOD", 0, "int");
   this.setVCMap("A69ContratadaUsuario_UsuarioCod", "CONTRATADAUSUARIO_USUARIOCOD", 0, "int");
   this.setVCMap("A66ContratadaUsuario_ContratadaCod", "CONTRATADAUSUARIO_CONTRATADACOD", 0, "int");
   this.setVCMap("A438Contratada_Sigla", "CONTRATADA_SIGLA", 0, "char");
   this.setVCMap("AV71PrazoInicial", "vPRAZOINICIAL", 0, "int");
   this.setVCMap("AV58ContratoServicos_Codigo", "vCONTRATOSERVICOS_CODIGO", 0, "int");
   this.setVCMap("AV60Unidades", "vUNIDADES", 0, "decimal");
   this.setVCMap("AV59Complexidade", "vCOMPLEXIDADE", 0, "int");
   this.setVCMap("AV63TipoDias", "vTIPODIAS", 0, "char");
   GridContainer.addRefreshingVar({rfrVar:"AV11LogResponsavel_DemandaCod"});
   GridContainer.addRefreshingVar({rfrVar:"A892LogResponsavel_DemandaCod", rfrProp:"Visible", gxAttId:"892"});
   GridContainer.addRefreshingVar(this.GXValidFnc[42]);
   GridContainer.addRefreshingVar(this.GXValidFnc[44]);
   GridContainer.addRefreshingVar(this.GXValidFnc[46]);
   GridContainer.addRefreshingVar(this.GXValidFnc[48]);
   GridContainer.addRefreshingVar(this.GXValidFnc[50]);
   GridContainer.addRefreshingVar({rfrVar:"AV75Pgmname"});
   GridContainer.addRefreshingVar(this.GXValidFnc[31]);
   GridContainer.addRefreshingVar(this.GXValidFnc[32]);
   GridContainer.addRefreshingVar({rfrVar:"AV27TFLogResponsavel_Acao_Sels"});
   GridContainer.addRefreshingVar({rfrVar:"AV31TFLogResponsavel_Status_Sels"});
   GridContainer.addRefreshingVar({rfrVar:"AV35TFLogResponsavel_NovoStatus_Sels"});
   GridContainer.addRefreshingVar(this.GXValidFnc[36]);
   GridContainer.addRefreshingVar(this.GXValidFnc[37]);
   GridContainer.addRefreshingVar({rfrVar:"A54Usuario_Ativo"});
   GridContainer.addRefreshingVar({rfrVar:"A1Usuario_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"A58Usuario_PessoaNom"});
   GridContainer.addRefreshingVar({rfrVar:"A896LogResponsavel_Owner"});
   GridContainer.addRefreshingVar({rfrVar:"A891LogResponsavel_UsuarioCod"});
   GridContainer.addRefreshingVar({rfrVar:"A493ContagemResultado_DemandaFM"});
   GridContainer.addRefreshingVar({rfrVar:"AV18WWPContext"});
   GridContainer.addRefreshingVar({rfrVar:"AV70Codigo"});
   GridContainer.addRefreshingVar(this.GXValidFnc[30]);
   GridContainer.addRefreshingVar({rfrVar:"A1553ContagemResultado_CntSrvCod"});
   GridContainer.addRefreshingVar({rfrVar:"A1611ContagemResultado_PrzTpDias"});
   GridContainer.addRefreshingVar({rfrVar:"A798ContagemResultado_PFBFSImp"});
   GridContainer.addRefreshingVar({rfrVar:"A1234LogResponsavel_NovoStatus", rfrProp:"Value", gxAttId:"1234"});
   GridContainer.addRefreshingVar({rfrVar:"A1237ContagemResultado_PrazoMaisDias", rfrProp:"Value", gxAttId:"1237"});
   GridContainer.addRefreshingVar({rfrVar:"A1227ContagemResultado_PrazoInicialDias", rfrProp:"Value", gxAttId:"1227"});
   GridContainer.addRefreshingVar({rfrVar:"A471ContagemResultado_DataDmn"});
   GridContainer.addRefreshingVar({rfrVar:"AV62NovaData"});
   GridContainer.addRefreshingVar({rfrVar:"AV61UltimoPrazo"});
   GridContainer.addRefreshingVar({rfrVar:"A893LogResponsavel_DataHora", rfrProp:"Value", gxAttId:"893"});
   GridContainer.addRefreshingVar({rfrVar:"A1149LogResponsavel_OwnerEhContratante"});
   GridContainer.addRefreshingVar({rfrVar:"A894LogResponsavel_Acao", rfrProp:"Value", gxAttId:"894"});
   GridContainer.addRefreshingVar({rfrVar:"AV51PrazoSugerido", rfrProp:"Value", gxAttId:"Prazosugerido"});
   GridContainer.addRefreshingVar({rfrVar:"A1177LogResponsavel_Prazo", rfrProp:"Value", gxAttId:"1177"});
   GridContainer.addRefreshingVar({rfrVar:"AV67Selecionadas"});
   GridContainer.addRefreshingVar({rfrVar:"AV52Selected", rfrProp:"Value", gxAttId:"Selected"});
   GridContainer.addRefreshingVar({rfrVar:"A1228ContratadaUsuario_AreaTrabalhoCod"});
   GridContainer.addRefreshingVar({rfrVar:"AV5Destino", rfrProp:"Value", gxAttId:"Destino"});
   GridContainer.addRefreshingVar({rfrVar:"A69ContratadaUsuario_UsuarioCod"});
   GridContainer.addRefreshingVar({rfrVar:"AV6Emissor", rfrProp:"Value", gxAttId:"Emissor"});
   GridContainer.addRefreshingVar({rfrVar:"A66ContratadaUsuario_ContratadaCod"});
   GridContainer.addRefreshingVar({rfrVar:"A438Contratada_Sigla"});
   GridContainer.addRefreshingVar({rfrVar:"AV71PrazoInicial"});
   GridContainer.addRefreshingVar(this.GXValidFnc[5]);
   GridContainer.addRefreshingVar({rfrVar:"AV58ContratoServicos_Codigo"});
   GridContainer.addRefreshingVar({rfrVar:"AV60Unidades"});
   GridContainer.addRefreshingVar({rfrVar:"AV59Complexidade"});
   GridContainer.addRefreshingVar({rfrVar:"AV63TipoDias"});
   this.InitStandaloneVars( );
});
gx.createParentObj(wp_corretor);
