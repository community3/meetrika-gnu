/*
               File: WWCatalogoSutentacao
        Description:  Catalogo Sutentacao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:5:4.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcatalogosutentacao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcatalogosutentacao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcatalogosutentacao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_88 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_88_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_88_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17CatalogoSutentacao_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CatalogoSutentacao_Descricao1", AV17CatalogoSutentacao_Descricao1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21CatalogoSutentacao_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21CatalogoSutentacao_Descricao2", AV21CatalogoSutentacao_Descricao2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25CatalogoSutentacao_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25CatalogoSutentacao_Descricao3", AV25CatalogoSutentacao_Descricao3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV35TFCatalogoSutentacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFCatalogoSutentacao_Codigo), 6, 0)));
               AV36TFCatalogoSutentacao_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFCatalogoSutentacao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFCatalogoSutentacao_Codigo_To), 6, 0)));
               AV39TFCatalogoSutentacao_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCatalogoSutentacao_Descricao", AV39TFCatalogoSutentacao_Descricao);
               AV40TFCatalogoSutentacao_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCatalogoSutentacao_Descricao_Sel", AV40TFCatalogoSutentacao_Descricao_Sel);
               AV43TFCatalogoSutentacao_Atividade = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFCatalogoSutentacao_Atividade", AV43TFCatalogoSutentacao_Atividade);
               AV44TFCatalogoSutentacao_Atividade_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFCatalogoSutentacao_Atividade_Sel", AV44TFCatalogoSutentacao_Atividade_Sel);
               AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace", AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace);
               AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace", AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace);
               AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace", AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace);
               AV72Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A1750CatalogoSutentacao_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17CatalogoSutentacao_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21CatalogoSutentacao_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25CatalogoSutentacao_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFCatalogoSutentacao_Codigo, AV36TFCatalogoSutentacao_Codigo_To, AV39TFCatalogoSutentacao_Descricao, AV40TFCatalogoSutentacao_Descricao_Sel, AV43TFCatalogoSutentacao_Atividade, AV44TFCatalogoSutentacao_Atividade_Sel, AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace, AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace, AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1750CatalogoSutentacao_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAN12( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTN12( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031195482");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcatalogosutentacao.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCATALOGOSUTENTACAO_DESCRICAO1", AV17CatalogoSutentacao_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCATALOGOSUTENTACAO_DESCRICAO2", AV21CatalogoSutentacao_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCATALOGOSUTENTACAO_DESCRICAO3", AV25CatalogoSutentacao_Descricao3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCATALOGOSUTENTACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFCatalogoSutentacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCATALOGOSUTENTACAO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFCatalogoSutentacao_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCATALOGOSUTENTACAO_DESCRICAO", AV39TFCatalogoSutentacao_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCATALOGOSUTENTACAO_DESCRICAO_SEL", AV40TFCatalogoSutentacao_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCATALOGOSUTENTACAO_ATIVIDADE", AV43TFCatalogoSutentacao_Atividade);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL", AV44TFCatalogoSutentacao_Atividade_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_88", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_88), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV46DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV46DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCATALOGOSUTENTACAO_CODIGOTITLEFILTERDATA", AV34CatalogoSutentacao_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCATALOGOSUTENTACAO_CODIGOTITLEFILTERDATA", AV34CatalogoSutentacao_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCATALOGOSUTENTACAO_DESCRICAOTITLEFILTERDATA", AV38CatalogoSutentacao_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCATALOGOSUTENTACAO_DESCRICAOTITLEFILTERDATA", AV38CatalogoSutentacao_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCATALOGOSUTENTACAO_ATIVIDADETITLEFILTERDATA", AV42CatalogoSutentacao_AtividadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCATALOGOSUTENTACAO_ATIVIDADETITLEFILTERDATA", AV42CatalogoSutentacao_AtividadeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV72Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Caption", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Cls", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_catalogosutentacao_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_catalogosutentacao_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_catalogosutentacao_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_catalogosutentacao_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_catalogosutentacao_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_catalogosutentacao_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_catalogosutentacao_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_catalogosutentacao_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_catalogosutentacao_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_catalogosutentacao_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_catalogosutentacao_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Caption", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Tooltip", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Cls", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_catalogosutentacao_atividade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_catalogosutentacao_atividade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Sortedstatus", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Includefilter", StringUtil.BoolToStr( Ddo_catalogosutentacao_atividade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Filtertype", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_catalogosutentacao_atividade_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_catalogosutentacao_atividade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Datalisttype", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Datalistproc", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_catalogosutentacao_atividade_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Sortasc", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Sortdsc", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Loadingdata", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Cleanfilter", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Noresultsfound", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_catalogosutentacao_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_catalogosutentacao_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Activeeventkey", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_catalogosutentacao_atividade_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEN12( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTN12( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcatalogosutentacao.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWCatalogoSutentacao" ;
      }

      public override String GetPgmdesc( )
      {
         return " Catalogo Sutentacao" ;
      }

      protected void WBN10( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_N12( true) ;
         }
         else
         {
            wb_table1_2_N12( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_N12e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(99, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(100, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,100);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcatalogosutentacao_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFCatalogoSutentacao_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFCatalogoSutentacao_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcatalogosutentacao_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcatalogosutentacao_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWCatalogoSutentacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcatalogosutentacao_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFCatalogoSutentacao_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFCatalogoSutentacao_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcatalogosutentacao_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcatalogosutentacao_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWCatalogoSutentacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcatalogosutentacao_descricao_Internalname, AV39TFCatalogoSutentacao_Descricao, StringUtil.RTrim( context.localUtil.Format( AV39TFCatalogoSutentacao_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcatalogosutentacao_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfcatalogosutentacao_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCatalogoSutentacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcatalogosutentacao_descricao_sel_Internalname, AV40TFCatalogoSutentacao_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV40TFCatalogoSutentacao_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcatalogosutentacao_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcatalogosutentacao_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCatalogoSutentacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcatalogosutentacao_atividade_Internalname, AV43TFCatalogoSutentacao_Atividade, StringUtil.RTrim( context.localUtil.Format( AV43TFCatalogoSutentacao_Atividade, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcatalogosutentacao_atividade_Jsonclick, 0, "Attribute", "", "", "", edtavTfcatalogosutentacao_atividade_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCatalogoSutentacao.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcatalogosutentacao_atividade_sel_Internalname, AV44TFCatalogoSutentacao_Atividade_Sel, StringUtil.RTrim( context.localUtil.Format( AV44TFCatalogoSutentacao_Atividade_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcatalogosutentacao_atividade_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcatalogosutentacao_atividade_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCatalogoSutentacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CATALOGOSUTENTACAO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_catalogosutentacao_codigotitlecontrolidtoreplace_Internalname, AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_catalogosutentacao_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCatalogoSutentacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CATALOGOSUTENTACAO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_catalogosutentacao_descricaotitlecontrolidtoreplace_Internalname, AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavDdo_catalogosutentacao_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCatalogoSutentacao.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CATALOGOSUTENTACAO_ATIVIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_88_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_catalogosutentacao_atividadetitlecontrolidtoreplace_Internalname, AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavDdo_catalogosutentacao_atividadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWCatalogoSutentacao.htm");
         }
         wbLoad = true;
      }

      protected void STARTN12( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Catalogo Sutentacao", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPN10( ) ;
      }

      protected void WSN12( )
      {
         STARTN12( ) ;
         EVTN12( ) ;
      }

      protected void EVTN12( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11N12 */
                              E11N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CATALOGOSUTENTACAO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12N12 */
                              E12N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CATALOGOSUTENTACAO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13N12 */
                              E13N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CATALOGOSUTENTACAO_ATIVIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14N12 */
                              E14N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15N12 */
                              E15N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16N12 */
                              E16N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17N12 */
                              E17N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18N12 */
                              E18N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19N12 */
                              E19N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20N12 */
                              E20N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21N12 */
                              E21N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22N12 */
                              E22N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23N12 */
                              E23N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24N12 */
                              E24N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25N12 */
                              E25N12 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 21), "'DOASSOCIARARTEFATOS'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 21), "'DOASSOCIARARTEFATOS'") == 0 ) )
                           {
                              nGXsfl_88_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
                              SubsflControlProps_882( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV69Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV70Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A1750CatalogoSutentacao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCatalogoSutentacao_Codigo_Internalname), ",", "."));
                              A1752CatalogoSutentacao_Descricao = StringUtil.Upper( cgiGet( edtCatalogoSutentacao_Descricao_Internalname));
                              A1753CatalogoSutentacao_Atividade = StringUtil.Upper( cgiGet( edtCatalogoSutentacao_Atividade_Internalname));
                              AV31AssociarArtefatos = cgiGet( edtavAssociarartefatos_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarartefatos_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31AssociarArtefatos)) ? AV71Associarartefatos_GXI : context.convertURL( context.PathToRelativeUrl( AV31AssociarArtefatos))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E26N12 */
                                    E26N12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E27N12 */
                                    E27N12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28N12 */
                                    E28N12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOASSOCIARARTEFATOS'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29N12 */
                                    E29N12 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Catalogosutentacao_descricao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCATALOGOSUTENTACAO_DESCRICAO1"), AV17CatalogoSutentacao_Descricao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Catalogosutentacao_descricao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCATALOGOSUTENTACAO_DESCRICAO2"), AV21CatalogoSutentacao_Descricao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Catalogosutentacao_descricao3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCATALOGOSUTENTACAO_DESCRICAO3"), AV25CatalogoSutentacao_Descricao3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcatalogosutentacao_codigo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCATALOGOSUTENTACAO_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFCatalogoSutentacao_Codigo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcatalogosutentacao_codigo_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCATALOGOSUTENTACAO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFCatalogoSutentacao_Codigo_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcatalogosutentacao_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCATALOGOSUTENTACAO_DESCRICAO"), AV39TFCatalogoSutentacao_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcatalogosutentacao_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCATALOGOSUTENTACAO_DESCRICAO_SEL"), AV40TFCatalogoSutentacao_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcatalogosutentacao_atividade Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCATALOGOSUTENTACAO_ATIVIDADE"), AV43TFCatalogoSutentacao_Atividade) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcatalogosutentacao_atividade_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL"), AV44TFCatalogoSutentacao_Atividade_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEN12( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAN12( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CATALOGOSUTENTACAO_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CATALOGOSUTENTACAO_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CATALOGOSUTENTACAO_DESCRICAO", "Descri��o", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_882( ) ;
         while ( nGXsfl_88_idx <= nRC_GXsfl_88 )
         {
            sendrow_882( ) ;
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17CatalogoSutentacao_Descricao1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21CatalogoSutentacao_Descricao2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25CatalogoSutentacao_Descricao3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV35TFCatalogoSutentacao_Codigo ,
                                       int AV36TFCatalogoSutentacao_Codigo_To ,
                                       String AV39TFCatalogoSutentacao_Descricao ,
                                       String AV40TFCatalogoSutentacao_Descricao_Sel ,
                                       String AV43TFCatalogoSutentacao_Atividade ,
                                       String AV44TFCatalogoSutentacao_Atividade_Sel ,
                                       String AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace ,
                                       String AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace ,
                                       String AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace ,
                                       String AV72Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A1750CatalogoSutentacao_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFN12( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CATALOGOSUTENTACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1750CatalogoSutentacao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CATALOGOSUTENTACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CATALOGOSUTENTACAO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1752CatalogoSutentacao_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "CATALOGOSUTENTACAO_DESCRICAO", A1752CatalogoSutentacao_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_CATALOGOSUTENTACAO_ATIVIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1753CatalogoSutentacao_Atividade, "@!"))));
         GxWebStd.gx_hidden_field( context, "CATALOGOSUTENTACAO_ATIVIDADE", A1753CatalogoSutentacao_Atividade);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFN12( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV72Pgmname = "WWCatalogoSutentacao";
         context.Gx_err = 0;
      }

      protected void RFN12( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 88;
         /* Execute user event: E27N12 */
         E27N12 ();
         nGXsfl_88_idx = 1;
         sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
         SubsflControlProps_882( ) ;
         nGXsfl_88_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_882( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 ,
                                                 AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 ,
                                                 AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ,
                                                 AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 ,
                                                 AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 ,
                                                 AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 ,
                                                 AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ,
                                                 AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 ,
                                                 AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 ,
                                                 AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 ,
                                                 AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ,
                                                 AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo ,
                                                 AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to ,
                                                 AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel ,
                                                 AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ,
                                                 AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel ,
                                                 AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ,
                                                 A1752CatalogoSutentacao_Descricao ,
                                                 A1750CatalogoSutentacao_Codigo ,
                                                 A1753CatalogoSutentacao_Atividade ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1), "%", "");
            lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1), "%", "");
            lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2), "%", "");
            lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2), "%", "");
            lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3), "%", "");
            lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3), "%", "");
            lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = StringUtil.Concat( StringUtil.RTrim( AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao), "%", "");
            lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = StringUtil.Concat( StringUtil.RTrim( AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade), "%", "");
            /* Using cursor H00N12 */
            pr_default.execute(0, new Object[] {lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1, lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1, lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2, lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2, lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3, lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3, AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo, AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to, lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao, AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel, lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade, AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_88_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1753CatalogoSutentacao_Atividade = H00N12_A1753CatalogoSutentacao_Atividade[0];
               A1752CatalogoSutentacao_Descricao = H00N12_A1752CatalogoSutentacao_Descricao[0];
               A1750CatalogoSutentacao_Codigo = H00N12_A1750CatalogoSutentacao_Codigo[0];
               /* Execute user event: E28N12 */
               E28N12 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 88;
            WBN10( ) ;
         }
         nGXsfl_88_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = AV17CatalogoSutentacao_Descricao1;
         AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = AV21CatalogoSutentacao_Descricao2;
         AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = AV25CatalogoSutentacao_Descricao3;
         AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo = AV35TFCatalogoSutentacao_Codigo;
         AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to = AV36TFCatalogoSutentacao_Codigo_To;
         AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = AV39TFCatalogoSutentacao_Descricao;
         AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel = AV40TFCatalogoSutentacao_Descricao_Sel;
         AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = AV43TFCatalogoSutentacao_Atividade;
         AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel = AV44TFCatalogoSutentacao_Atividade_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 ,
                                              AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 ,
                                              AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ,
                                              AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 ,
                                              AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 ,
                                              AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 ,
                                              AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ,
                                              AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 ,
                                              AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 ,
                                              AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 ,
                                              AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ,
                                              AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo ,
                                              AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to ,
                                              AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel ,
                                              AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ,
                                              AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel ,
                                              AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ,
                                              A1752CatalogoSutentacao_Descricao ,
                                              A1750CatalogoSutentacao_Codigo ,
                                              A1753CatalogoSutentacao_Atividade ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1), "%", "");
         lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1), "%", "");
         lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2), "%", "");
         lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2), "%", "");
         lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3), "%", "");
         lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3), "%", "");
         lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = StringUtil.Concat( StringUtil.RTrim( AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao), "%", "");
         lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = StringUtil.Concat( StringUtil.RTrim( AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade), "%", "");
         /* Using cursor H00N13 */
         pr_default.execute(1, new Object[] {lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1, lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1, lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2, lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2, lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3, lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3, AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo, AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to, lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao, AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel, lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade, AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel});
         GRID_nRecordCount = H00N13_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = AV17CatalogoSutentacao_Descricao1;
         AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = AV21CatalogoSutentacao_Descricao2;
         AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = AV25CatalogoSutentacao_Descricao3;
         AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo = AV35TFCatalogoSutentacao_Codigo;
         AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to = AV36TFCatalogoSutentacao_Codigo_To;
         AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = AV39TFCatalogoSutentacao_Descricao;
         AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel = AV40TFCatalogoSutentacao_Descricao_Sel;
         AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = AV43TFCatalogoSutentacao_Atividade;
         AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel = AV44TFCatalogoSutentacao_Atividade_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17CatalogoSutentacao_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21CatalogoSutentacao_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25CatalogoSutentacao_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFCatalogoSutentacao_Codigo, AV36TFCatalogoSutentacao_Codigo_To, AV39TFCatalogoSutentacao_Descricao, AV40TFCatalogoSutentacao_Descricao_Sel, AV43TFCatalogoSutentacao_Atividade, AV44TFCatalogoSutentacao_Atividade_Sel, AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace, AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace, AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1750CatalogoSutentacao_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = AV17CatalogoSutentacao_Descricao1;
         AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = AV21CatalogoSutentacao_Descricao2;
         AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = AV25CatalogoSutentacao_Descricao3;
         AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo = AV35TFCatalogoSutentacao_Codigo;
         AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to = AV36TFCatalogoSutentacao_Codigo_To;
         AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = AV39TFCatalogoSutentacao_Descricao;
         AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel = AV40TFCatalogoSutentacao_Descricao_Sel;
         AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = AV43TFCatalogoSutentacao_Atividade;
         AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel = AV44TFCatalogoSutentacao_Atividade_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17CatalogoSutentacao_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21CatalogoSutentacao_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25CatalogoSutentacao_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFCatalogoSutentacao_Codigo, AV36TFCatalogoSutentacao_Codigo_To, AV39TFCatalogoSutentacao_Descricao, AV40TFCatalogoSutentacao_Descricao_Sel, AV43TFCatalogoSutentacao_Atividade, AV44TFCatalogoSutentacao_Atividade_Sel, AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace, AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace, AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1750CatalogoSutentacao_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = AV17CatalogoSutentacao_Descricao1;
         AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = AV21CatalogoSutentacao_Descricao2;
         AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = AV25CatalogoSutentacao_Descricao3;
         AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo = AV35TFCatalogoSutentacao_Codigo;
         AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to = AV36TFCatalogoSutentacao_Codigo_To;
         AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = AV39TFCatalogoSutentacao_Descricao;
         AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel = AV40TFCatalogoSutentacao_Descricao_Sel;
         AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = AV43TFCatalogoSutentacao_Atividade;
         AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel = AV44TFCatalogoSutentacao_Atividade_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17CatalogoSutentacao_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21CatalogoSutentacao_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25CatalogoSutentacao_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFCatalogoSutentacao_Codigo, AV36TFCatalogoSutentacao_Codigo_To, AV39TFCatalogoSutentacao_Descricao, AV40TFCatalogoSutentacao_Descricao_Sel, AV43TFCatalogoSutentacao_Atividade, AV44TFCatalogoSutentacao_Atividade_Sel, AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace, AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace, AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1750CatalogoSutentacao_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = AV17CatalogoSutentacao_Descricao1;
         AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = AV21CatalogoSutentacao_Descricao2;
         AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = AV25CatalogoSutentacao_Descricao3;
         AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo = AV35TFCatalogoSutentacao_Codigo;
         AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to = AV36TFCatalogoSutentacao_Codigo_To;
         AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = AV39TFCatalogoSutentacao_Descricao;
         AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel = AV40TFCatalogoSutentacao_Descricao_Sel;
         AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = AV43TFCatalogoSutentacao_Atividade;
         AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel = AV44TFCatalogoSutentacao_Atividade_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17CatalogoSutentacao_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21CatalogoSutentacao_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25CatalogoSutentacao_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFCatalogoSutentacao_Codigo, AV36TFCatalogoSutentacao_Codigo_To, AV39TFCatalogoSutentacao_Descricao, AV40TFCatalogoSutentacao_Descricao_Sel, AV43TFCatalogoSutentacao_Atividade, AV44TFCatalogoSutentacao_Atividade_Sel, AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace, AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace, AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1750CatalogoSutentacao_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = AV17CatalogoSutentacao_Descricao1;
         AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = AV21CatalogoSutentacao_Descricao2;
         AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = AV25CatalogoSutentacao_Descricao3;
         AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo = AV35TFCatalogoSutentacao_Codigo;
         AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to = AV36TFCatalogoSutentacao_Codigo_To;
         AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = AV39TFCatalogoSutentacao_Descricao;
         AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel = AV40TFCatalogoSutentacao_Descricao_Sel;
         AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = AV43TFCatalogoSutentacao_Atividade;
         AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel = AV44TFCatalogoSutentacao_Atividade_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17CatalogoSutentacao_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21CatalogoSutentacao_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25CatalogoSutentacao_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFCatalogoSutentacao_Codigo, AV36TFCatalogoSutentacao_Codigo_To, AV39TFCatalogoSutentacao_Descricao, AV40TFCatalogoSutentacao_Descricao_Sel, AV43TFCatalogoSutentacao_Atividade, AV44TFCatalogoSutentacao_Atividade_Sel, AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace, AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace, AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1750CatalogoSutentacao_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPN10( )
      {
         /* Before Start, stand alone formulas. */
         AV72Pgmname = "WWCatalogoSutentacao";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E26N12 */
         E26N12 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV46DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vCATALOGOSUTENTACAO_CODIGOTITLEFILTERDATA"), AV34CatalogoSutentacao_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCATALOGOSUTENTACAO_DESCRICAOTITLEFILTERDATA"), AV38CatalogoSutentacao_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCATALOGOSUTENTACAO_ATIVIDADETITLEFILTERDATA"), AV42CatalogoSutentacao_AtividadeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17CatalogoSutentacao_Descricao1 = StringUtil.Upper( cgiGet( edtavCatalogosutentacao_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CatalogoSutentacao_Descricao1", AV17CatalogoSutentacao_Descricao1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21CatalogoSutentacao_Descricao2 = StringUtil.Upper( cgiGet( edtavCatalogosutentacao_descricao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21CatalogoSutentacao_Descricao2", AV21CatalogoSutentacao_Descricao2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25CatalogoSutentacao_Descricao3 = StringUtil.Upper( cgiGet( edtavCatalogosutentacao_descricao3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25CatalogoSutentacao_Descricao3", AV25CatalogoSutentacao_Descricao3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcatalogosutentacao_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcatalogosutentacao_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCATALOGOSUTENTACAO_CODIGO");
               GX_FocusControl = edtavTfcatalogosutentacao_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFCatalogoSutentacao_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFCatalogoSutentacao_Codigo), 6, 0)));
            }
            else
            {
               AV35TFCatalogoSutentacao_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcatalogosutentacao_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFCatalogoSutentacao_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcatalogosutentacao_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcatalogosutentacao_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCATALOGOSUTENTACAO_CODIGO_TO");
               GX_FocusControl = edtavTfcatalogosutentacao_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFCatalogoSutentacao_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFCatalogoSutentacao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFCatalogoSutentacao_Codigo_To), 6, 0)));
            }
            else
            {
               AV36TFCatalogoSutentacao_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcatalogosutentacao_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFCatalogoSutentacao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFCatalogoSutentacao_Codigo_To), 6, 0)));
            }
            AV39TFCatalogoSutentacao_Descricao = StringUtil.Upper( cgiGet( edtavTfcatalogosutentacao_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCatalogoSutentacao_Descricao", AV39TFCatalogoSutentacao_Descricao);
            AV40TFCatalogoSutentacao_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfcatalogosutentacao_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCatalogoSutentacao_Descricao_Sel", AV40TFCatalogoSutentacao_Descricao_Sel);
            AV43TFCatalogoSutentacao_Atividade = StringUtil.Upper( cgiGet( edtavTfcatalogosutentacao_atividade_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFCatalogoSutentacao_Atividade", AV43TFCatalogoSutentacao_Atividade);
            AV44TFCatalogoSutentacao_Atividade_Sel = StringUtil.Upper( cgiGet( edtavTfcatalogosutentacao_atividade_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFCatalogoSutentacao_Atividade_Sel", AV44TFCatalogoSutentacao_Atividade_Sel);
            AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_catalogosutentacao_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace", AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace);
            AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_catalogosutentacao_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace", AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace);
            AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace = cgiGet( edtavDdo_catalogosutentacao_atividadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace", AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_88 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_88"), ",", "."));
            AV48GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV49GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_catalogosutentacao_codigo_Caption = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Caption");
            Ddo_catalogosutentacao_codigo_Tooltip = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Tooltip");
            Ddo_catalogosutentacao_codigo_Cls = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Cls");
            Ddo_catalogosutentacao_codigo_Filteredtext_set = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Filteredtext_set");
            Ddo_catalogosutentacao_codigo_Filteredtextto_set = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Filteredtextto_set");
            Ddo_catalogosutentacao_codigo_Dropdownoptionstype = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Dropdownoptionstype");
            Ddo_catalogosutentacao_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Titlecontrolidtoreplace");
            Ddo_catalogosutentacao_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Includesortasc"));
            Ddo_catalogosutentacao_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Includesortdsc"));
            Ddo_catalogosutentacao_codigo_Sortedstatus = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Sortedstatus");
            Ddo_catalogosutentacao_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Includefilter"));
            Ddo_catalogosutentacao_codigo_Filtertype = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Filtertype");
            Ddo_catalogosutentacao_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Filterisrange"));
            Ddo_catalogosutentacao_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Includedatalist"));
            Ddo_catalogosutentacao_codigo_Sortasc = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Sortasc");
            Ddo_catalogosutentacao_codigo_Sortdsc = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Sortdsc");
            Ddo_catalogosutentacao_codigo_Cleanfilter = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Cleanfilter");
            Ddo_catalogosutentacao_codigo_Rangefilterfrom = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Rangefilterfrom");
            Ddo_catalogosutentacao_codigo_Rangefilterto = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Rangefilterto");
            Ddo_catalogosutentacao_codigo_Searchbuttontext = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Searchbuttontext");
            Ddo_catalogosutentacao_descricao_Caption = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Caption");
            Ddo_catalogosutentacao_descricao_Tooltip = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Tooltip");
            Ddo_catalogosutentacao_descricao_Cls = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Cls");
            Ddo_catalogosutentacao_descricao_Filteredtext_set = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Filteredtext_set");
            Ddo_catalogosutentacao_descricao_Selectedvalue_set = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Selectedvalue_set");
            Ddo_catalogosutentacao_descricao_Dropdownoptionstype = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Dropdownoptionstype");
            Ddo_catalogosutentacao_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_catalogosutentacao_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Includesortasc"));
            Ddo_catalogosutentacao_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Includesortdsc"));
            Ddo_catalogosutentacao_descricao_Sortedstatus = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Sortedstatus");
            Ddo_catalogosutentacao_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Includefilter"));
            Ddo_catalogosutentacao_descricao_Filtertype = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Filtertype");
            Ddo_catalogosutentacao_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Filterisrange"));
            Ddo_catalogosutentacao_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Includedatalist"));
            Ddo_catalogosutentacao_descricao_Datalisttype = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Datalisttype");
            Ddo_catalogosutentacao_descricao_Datalistproc = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Datalistproc");
            Ddo_catalogosutentacao_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_catalogosutentacao_descricao_Sortasc = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Sortasc");
            Ddo_catalogosutentacao_descricao_Sortdsc = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Sortdsc");
            Ddo_catalogosutentacao_descricao_Loadingdata = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Loadingdata");
            Ddo_catalogosutentacao_descricao_Cleanfilter = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Cleanfilter");
            Ddo_catalogosutentacao_descricao_Noresultsfound = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Noresultsfound");
            Ddo_catalogosutentacao_descricao_Searchbuttontext = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Searchbuttontext");
            Ddo_catalogosutentacao_atividade_Caption = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Caption");
            Ddo_catalogosutentacao_atividade_Tooltip = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Tooltip");
            Ddo_catalogosutentacao_atividade_Cls = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Cls");
            Ddo_catalogosutentacao_atividade_Filteredtext_set = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Filteredtext_set");
            Ddo_catalogosutentacao_atividade_Selectedvalue_set = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Selectedvalue_set");
            Ddo_catalogosutentacao_atividade_Dropdownoptionstype = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Dropdownoptionstype");
            Ddo_catalogosutentacao_atividade_Titlecontrolidtoreplace = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Titlecontrolidtoreplace");
            Ddo_catalogosutentacao_atividade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Includesortasc"));
            Ddo_catalogosutentacao_atividade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Includesortdsc"));
            Ddo_catalogosutentacao_atividade_Sortedstatus = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Sortedstatus");
            Ddo_catalogosutentacao_atividade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Includefilter"));
            Ddo_catalogosutentacao_atividade_Filtertype = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Filtertype");
            Ddo_catalogosutentacao_atividade_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Filterisrange"));
            Ddo_catalogosutentacao_atividade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Includedatalist"));
            Ddo_catalogosutentacao_atividade_Datalisttype = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Datalisttype");
            Ddo_catalogosutentacao_atividade_Datalistproc = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Datalistproc");
            Ddo_catalogosutentacao_atividade_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_catalogosutentacao_atividade_Sortasc = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Sortasc");
            Ddo_catalogosutentacao_atividade_Sortdsc = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Sortdsc");
            Ddo_catalogosutentacao_atividade_Loadingdata = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Loadingdata");
            Ddo_catalogosutentacao_atividade_Cleanfilter = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Cleanfilter");
            Ddo_catalogosutentacao_atividade_Noresultsfound = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Noresultsfound");
            Ddo_catalogosutentacao_atividade_Searchbuttontext = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_catalogosutentacao_codigo_Activeeventkey = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Activeeventkey");
            Ddo_catalogosutentacao_codigo_Filteredtext_get = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Filteredtext_get");
            Ddo_catalogosutentacao_codigo_Filteredtextto_get = cgiGet( "DDO_CATALOGOSUTENTACAO_CODIGO_Filteredtextto_get");
            Ddo_catalogosutentacao_descricao_Activeeventkey = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Activeeventkey");
            Ddo_catalogosutentacao_descricao_Filteredtext_get = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Filteredtext_get");
            Ddo_catalogosutentacao_descricao_Selectedvalue_get = cgiGet( "DDO_CATALOGOSUTENTACAO_DESCRICAO_Selectedvalue_get");
            Ddo_catalogosutentacao_atividade_Activeeventkey = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Activeeventkey");
            Ddo_catalogosutentacao_atividade_Filteredtext_get = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Filteredtext_get");
            Ddo_catalogosutentacao_atividade_Selectedvalue_get = cgiGet( "DDO_CATALOGOSUTENTACAO_ATIVIDADE_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCATALOGOSUTENTACAO_DESCRICAO1"), AV17CatalogoSutentacao_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCATALOGOSUTENTACAO_DESCRICAO2"), AV21CatalogoSutentacao_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCATALOGOSUTENTACAO_DESCRICAO3"), AV25CatalogoSutentacao_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCATALOGOSUTENTACAO_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFCatalogoSutentacao_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCATALOGOSUTENTACAO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFCatalogoSutentacao_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCATALOGOSUTENTACAO_DESCRICAO"), AV39TFCatalogoSutentacao_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCATALOGOSUTENTACAO_DESCRICAO_SEL"), AV40TFCatalogoSutentacao_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCATALOGOSUTENTACAO_ATIVIDADE"), AV43TFCatalogoSutentacao_Atividade) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL"), AV44TFCatalogoSutentacao_Atividade_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E26N12 */
         E26N12 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26N12( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "CATALOGOSUTENTACAO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "CATALOGOSUTENTACAO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "CATALOGOSUTENTACAO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfcatalogosutentacao_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcatalogosutentacao_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcatalogosutentacao_codigo_Visible), 5, 0)));
         edtavTfcatalogosutentacao_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcatalogosutentacao_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcatalogosutentacao_codigo_to_Visible), 5, 0)));
         edtavTfcatalogosutentacao_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcatalogosutentacao_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcatalogosutentacao_descricao_Visible), 5, 0)));
         edtavTfcatalogosutentacao_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcatalogosutentacao_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcatalogosutentacao_descricao_sel_Visible), 5, 0)));
         edtavTfcatalogosutentacao_atividade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcatalogosutentacao_atividade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcatalogosutentacao_atividade_Visible), 5, 0)));
         edtavTfcatalogosutentacao_atividade_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcatalogosutentacao_atividade_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcatalogosutentacao_atividade_sel_Visible), 5, 0)));
         Ddo_catalogosutentacao_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_CatalogoSutentacao_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_codigo_Internalname, "TitleControlIdToReplace", Ddo_catalogosutentacao_codigo_Titlecontrolidtoreplace);
         AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace = Ddo_catalogosutentacao_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace", AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace);
         edtavDdo_catalogosutentacao_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_catalogosutentacao_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_catalogosutentacao_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_catalogosutentacao_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_CatalogoSutentacao_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_descricao_Internalname, "TitleControlIdToReplace", Ddo_catalogosutentacao_descricao_Titlecontrolidtoreplace);
         AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace = Ddo_catalogosutentacao_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace", AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace);
         edtavDdo_catalogosutentacao_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_catalogosutentacao_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_catalogosutentacao_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_catalogosutentacao_atividade_Titlecontrolidtoreplace = subGrid_Internalname+"_CatalogoSutentacao_Atividade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_atividade_Internalname, "TitleControlIdToReplace", Ddo_catalogosutentacao_atividade_Titlecontrolidtoreplace);
         AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace = Ddo_catalogosutentacao_atividade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace", AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace);
         edtavDdo_catalogosutentacao_atividadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_catalogosutentacao_atividadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_catalogosutentacao_atividadetitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Catalogo Sutentacao";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Descri��o", 0);
         cmbavOrderedby.addItem("2", "C�digo", 0);
         cmbavOrderedby.addItem("3", "Atividade", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV46DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV46DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E27N12( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV34CatalogoSutentacao_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38CatalogoSutentacao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42CatalogoSutentacao_AtividadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtCatalogoSutentacao_Codigo_Titleformat = 2;
         edtCatalogoSutentacao_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "C�digo", AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCatalogoSutentacao_Codigo_Internalname, "Title", edtCatalogoSutentacao_Codigo_Title);
         edtCatalogoSutentacao_Descricao_Titleformat = 2;
         edtCatalogoSutentacao_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCatalogoSutentacao_Descricao_Internalname, "Title", edtCatalogoSutentacao_Descricao_Title);
         edtCatalogoSutentacao_Atividade_Titleformat = 2;
         edtCatalogoSutentacao_Atividade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Atividade", AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCatalogoSutentacao_Atividade_Internalname, "Title", edtCatalogoSutentacao_Atividade_Title);
         AV48GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48GridCurrentPage), 10, 0)));
         AV49GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49GridPageCount), 10, 0)));
         edtavAssociarartefatos_Title = "Artefatos";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAssociarartefatos_Internalname, "Title", edtavAssociarartefatos_Title);
         AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = AV17CatalogoSutentacao_Descricao1;
         AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = AV21CatalogoSutentacao_Descricao2;
         AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = AV25CatalogoSutentacao_Descricao3;
         AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo = AV35TFCatalogoSutentacao_Codigo;
         AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to = AV36TFCatalogoSutentacao_Codigo_To;
         AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = AV39TFCatalogoSutentacao_Descricao;
         AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel = AV40TFCatalogoSutentacao_Descricao_Sel;
         AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = AV43TFCatalogoSutentacao_Atividade;
         AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel = AV44TFCatalogoSutentacao_Atividade_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34CatalogoSutentacao_CodigoTitleFilterData", AV34CatalogoSutentacao_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38CatalogoSutentacao_DescricaoTitleFilterData", AV38CatalogoSutentacao_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42CatalogoSutentacao_AtividadeTitleFilterData", AV42CatalogoSutentacao_AtividadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11N12( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV47PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV47PageToGo) ;
         }
      }

      protected void E12N12( )
      {
         /* Ddo_catalogosutentacao_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_catalogosutentacao_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_catalogosutentacao_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_codigo_Internalname, "SortedStatus", Ddo_catalogosutentacao_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_catalogosutentacao_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_catalogosutentacao_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_codigo_Internalname, "SortedStatus", Ddo_catalogosutentacao_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_catalogosutentacao_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFCatalogoSutentacao_Codigo = (int)(NumberUtil.Val( Ddo_catalogosutentacao_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFCatalogoSutentacao_Codigo), 6, 0)));
            AV36TFCatalogoSutentacao_Codigo_To = (int)(NumberUtil.Val( Ddo_catalogosutentacao_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFCatalogoSutentacao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFCatalogoSutentacao_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13N12( )
      {
         /* Ddo_catalogosutentacao_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_catalogosutentacao_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_catalogosutentacao_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_descricao_Internalname, "SortedStatus", Ddo_catalogosutentacao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_catalogosutentacao_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_catalogosutentacao_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_descricao_Internalname, "SortedStatus", Ddo_catalogosutentacao_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_catalogosutentacao_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFCatalogoSutentacao_Descricao = Ddo_catalogosutentacao_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCatalogoSutentacao_Descricao", AV39TFCatalogoSutentacao_Descricao);
            AV40TFCatalogoSutentacao_Descricao_Sel = Ddo_catalogosutentacao_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCatalogoSutentacao_Descricao_Sel", AV40TFCatalogoSutentacao_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14N12( )
      {
         /* Ddo_catalogosutentacao_atividade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_catalogosutentacao_atividade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_catalogosutentacao_atividade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_atividade_Internalname, "SortedStatus", Ddo_catalogosutentacao_atividade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_catalogosutentacao_atividade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_catalogosutentacao_atividade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_atividade_Internalname, "SortedStatus", Ddo_catalogosutentacao_atividade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_catalogosutentacao_atividade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFCatalogoSutentacao_Atividade = Ddo_catalogosutentacao_atividade_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFCatalogoSutentacao_Atividade", AV43TFCatalogoSutentacao_Atividade);
            AV44TFCatalogoSutentacao_Atividade_Sel = Ddo_catalogosutentacao_atividade_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFCatalogoSutentacao_Atividade_Sel", AV44TFCatalogoSutentacao_Atividade_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E28N12( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV69Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("catalogosutentacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1750CatalogoSutentacao_Codigo);
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV70Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("catalogosutentacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1750CatalogoSutentacao_Codigo);
         AV31AssociarArtefatos = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAssociarartefatos_Internalname, AV31AssociarArtefatos);
         AV71Associarartefatos_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavAssociarartefatos_Tooltiptext = "Associar Artefatos";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 88;
         }
         sendrow_882( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_88_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(88, GridRow);
         }
      }

      protected void E15N12( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21N12( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16N12( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17CatalogoSutentacao_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21CatalogoSutentacao_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25CatalogoSutentacao_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFCatalogoSutentacao_Codigo, AV36TFCatalogoSutentacao_Codigo_To, AV39TFCatalogoSutentacao_Descricao, AV40TFCatalogoSutentacao_Descricao_Sel, AV43TFCatalogoSutentacao_Atividade, AV44TFCatalogoSutentacao_Atividade_Sel, AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace, AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace, AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1750CatalogoSutentacao_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E22N12( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23N12( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17N12( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17CatalogoSutentacao_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21CatalogoSutentacao_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25CatalogoSutentacao_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFCatalogoSutentacao_Codigo, AV36TFCatalogoSutentacao_Codigo_To, AV39TFCatalogoSutentacao_Descricao, AV40TFCatalogoSutentacao_Descricao_Sel, AV43TFCatalogoSutentacao_Atividade, AV44TFCatalogoSutentacao_Atividade_Sel, AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace, AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace, AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1750CatalogoSutentacao_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E24N12( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18N12( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17CatalogoSutentacao_Descricao1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21CatalogoSutentacao_Descricao2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25CatalogoSutentacao_Descricao3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV35TFCatalogoSutentacao_Codigo, AV36TFCatalogoSutentacao_Codigo_To, AV39TFCatalogoSutentacao_Descricao, AV40TFCatalogoSutentacao_Descricao_Sel, AV43TFCatalogoSutentacao_Atividade, AV44TFCatalogoSutentacao_Atividade_Sel, AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace, AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace, AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace, AV72Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A1750CatalogoSutentacao_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E25N12( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19N12( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E29N12( )
      {
         /* 'DoAssociarArtefatos' Routine */
         context.PopUp(formatLink("wp_associarcatalogosutentacaoartefatos.aspx") + "?" + UrlEncode("" +A1750CatalogoSutentacao_Codigo), new Object[] {});
         context.DoAjaxRefresh();
      }

      protected void E20N12( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("catalogosutentacao.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_catalogosutentacao_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_codigo_Internalname, "SortedStatus", Ddo_catalogosutentacao_codigo_Sortedstatus);
         Ddo_catalogosutentacao_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_descricao_Internalname, "SortedStatus", Ddo_catalogosutentacao_descricao_Sortedstatus);
         Ddo_catalogosutentacao_atividade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_atividade_Internalname, "SortedStatus", Ddo_catalogosutentacao_atividade_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_catalogosutentacao_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_codigo_Internalname, "SortedStatus", Ddo_catalogosutentacao_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_catalogosutentacao_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_descricao_Internalname, "SortedStatus", Ddo_catalogosutentacao_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_catalogosutentacao_atividade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_atividade_Internalname, "SortedStatus", Ddo_catalogosutentacao_atividade_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavCatalogosutentacao_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCatalogosutentacao_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCatalogosutentacao_descricao1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 )
         {
            edtavCatalogosutentacao_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCatalogosutentacao_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCatalogosutentacao_descricao1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavCatalogosutentacao_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCatalogosutentacao_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCatalogosutentacao_descricao2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 )
         {
            edtavCatalogosutentacao_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCatalogosutentacao_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCatalogosutentacao_descricao2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavCatalogosutentacao_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCatalogosutentacao_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCatalogosutentacao_descricao3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 )
         {
            edtavCatalogosutentacao_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCatalogosutentacao_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCatalogosutentacao_descricao3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CATALOGOSUTENTACAO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21CatalogoSutentacao_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21CatalogoSutentacao_Descricao2", AV21CatalogoSutentacao_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CATALOGOSUTENTACAO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25CatalogoSutentacao_Descricao3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25CatalogoSutentacao_Descricao3", AV25CatalogoSutentacao_Descricao3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV35TFCatalogoSutentacao_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFCatalogoSutentacao_Codigo), 6, 0)));
         Ddo_catalogosutentacao_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_codigo_Internalname, "FilteredText_set", Ddo_catalogosutentacao_codigo_Filteredtext_set);
         AV36TFCatalogoSutentacao_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFCatalogoSutentacao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFCatalogoSutentacao_Codigo_To), 6, 0)));
         Ddo_catalogosutentacao_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_codigo_Internalname, "FilteredTextTo_set", Ddo_catalogosutentacao_codigo_Filteredtextto_set);
         AV39TFCatalogoSutentacao_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCatalogoSutentacao_Descricao", AV39TFCatalogoSutentacao_Descricao);
         Ddo_catalogosutentacao_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_descricao_Internalname, "FilteredText_set", Ddo_catalogosutentacao_descricao_Filteredtext_set);
         AV40TFCatalogoSutentacao_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCatalogoSutentacao_Descricao_Sel", AV40TFCatalogoSutentacao_Descricao_Sel);
         Ddo_catalogosutentacao_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_descricao_Internalname, "SelectedValue_set", Ddo_catalogosutentacao_descricao_Selectedvalue_set);
         AV43TFCatalogoSutentacao_Atividade = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFCatalogoSutentacao_Atividade", AV43TFCatalogoSutentacao_Atividade);
         Ddo_catalogosutentacao_atividade_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_atividade_Internalname, "FilteredText_set", Ddo_catalogosutentacao_atividade_Filteredtext_set);
         AV44TFCatalogoSutentacao_Atividade_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFCatalogoSutentacao_Atividade_Sel", AV44TFCatalogoSutentacao_Atividade_Sel);
         Ddo_catalogosutentacao_atividade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_atividade_Internalname, "SelectedValue_set", Ddo_catalogosutentacao_atividade_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "CATALOGOSUTENTACAO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17CatalogoSutentacao_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CatalogoSutentacao_Descricao1", AV17CatalogoSutentacao_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV72Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV72Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV72Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV73GXV1 = 1;
         while ( AV73GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV73GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCATALOGOSUTENTACAO_CODIGO") == 0 )
            {
               AV35TFCatalogoSutentacao_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFCatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFCatalogoSutentacao_Codigo), 6, 0)));
               AV36TFCatalogoSutentacao_Codigo_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFCatalogoSutentacao_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFCatalogoSutentacao_Codigo_To), 6, 0)));
               if ( ! (0==AV35TFCatalogoSutentacao_Codigo) )
               {
                  Ddo_catalogosutentacao_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV35TFCatalogoSutentacao_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_codigo_Internalname, "FilteredText_set", Ddo_catalogosutentacao_codigo_Filteredtext_set);
               }
               if ( ! (0==AV36TFCatalogoSutentacao_Codigo_To) )
               {
                  Ddo_catalogosutentacao_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV36TFCatalogoSutentacao_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_codigo_Internalname, "FilteredTextTo_set", Ddo_catalogosutentacao_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCATALOGOSUTENTACAO_DESCRICAO") == 0 )
            {
               AV39TFCatalogoSutentacao_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFCatalogoSutentacao_Descricao", AV39TFCatalogoSutentacao_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFCatalogoSutentacao_Descricao)) )
               {
                  Ddo_catalogosutentacao_descricao_Filteredtext_set = AV39TFCatalogoSutentacao_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_descricao_Internalname, "FilteredText_set", Ddo_catalogosutentacao_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCATALOGOSUTENTACAO_DESCRICAO_SEL") == 0 )
            {
               AV40TFCatalogoSutentacao_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFCatalogoSutentacao_Descricao_Sel", AV40TFCatalogoSutentacao_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFCatalogoSutentacao_Descricao_Sel)) )
               {
                  Ddo_catalogosutentacao_descricao_Selectedvalue_set = AV40TFCatalogoSutentacao_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_descricao_Internalname, "SelectedValue_set", Ddo_catalogosutentacao_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCATALOGOSUTENTACAO_ATIVIDADE") == 0 )
            {
               AV43TFCatalogoSutentacao_Atividade = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFCatalogoSutentacao_Atividade", AV43TFCatalogoSutentacao_Atividade);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFCatalogoSutentacao_Atividade)) )
               {
                  Ddo_catalogosutentacao_atividade_Filteredtext_set = AV43TFCatalogoSutentacao_Atividade;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_atividade_Internalname, "FilteredText_set", Ddo_catalogosutentacao_atividade_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCATALOGOSUTENTACAO_ATIVIDADE_SEL") == 0 )
            {
               AV44TFCatalogoSutentacao_Atividade_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFCatalogoSutentacao_Atividade_Sel", AV44TFCatalogoSutentacao_Atividade_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFCatalogoSutentacao_Atividade_Sel)) )
               {
                  Ddo_catalogosutentacao_atividade_Selectedvalue_set = AV44TFCatalogoSutentacao_Atividade_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_catalogosutentacao_atividade_Internalname, "SelectedValue_set", Ddo_catalogosutentacao_atividade_Selectedvalue_set);
               }
            }
            AV73GXV1 = (int)(AV73GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17CatalogoSutentacao_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17CatalogoSutentacao_Descricao1", AV17CatalogoSutentacao_Descricao1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21CatalogoSutentacao_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21CatalogoSutentacao_Descricao2", AV21CatalogoSutentacao_Descricao2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25CatalogoSutentacao_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25CatalogoSutentacao_Descricao3", AV25CatalogoSutentacao_Descricao3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV72Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV35TFCatalogoSutentacao_Codigo) && (0==AV36TFCatalogoSutentacao_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCATALOGOSUTENTACAO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35TFCatalogoSutentacao_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV36TFCatalogoSutentacao_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFCatalogoSutentacao_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCATALOGOSUTENTACAO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFCatalogoSutentacao_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFCatalogoSutentacao_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCATALOGOSUTENTACAO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFCatalogoSutentacao_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFCatalogoSutentacao_Atividade)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCATALOGOSUTENTACAO_ATIVIDADE";
            AV11GridStateFilterValue.gxTpr_Value = AV43TFCatalogoSutentacao_Atividade;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFCatalogoSutentacao_Atividade_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCATALOGOSUTENTACAO_ATIVIDADE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFCatalogoSutentacao_Atividade_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV72Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17CatalogoSutentacao_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17CatalogoSutentacao_Descricao1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21CatalogoSutentacao_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21CatalogoSutentacao_Descricao2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25CatalogoSutentacao_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25CatalogoSutentacao_Descricao3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV72Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "CatalogoSutentacao";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_N12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_N12( true) ;
         }
         else
         {
            wb_table2_8_N12( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_N12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_82_N12( true) ;
         }
         else
         {
            wb_table3_82_N12( false) ;
         }
         return  ;
      }

      protected void wb_table3_82_N12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_N12e( true) ;
         }
         else
         {
            wb_table1_2_N12e( false) ;
         }
      }

      protected void wb_table3_82_N12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_85_N12( true) ;
         }
         else
         {
            wb_table4_85_N12( false) ;
         }
         return  ;
      }

      protected void wb_table4_85_N12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_N12e( true) ;
         }
         else
         {
            wb_table3_82_N12e( false) ;
         }
      }

      protected void wb_table4_85_N12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"88\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCatalogoSutentacao_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtCatalogoSutentacao_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCatalogoSutentacao_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCatalogoSutentacao_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtCatalogoSutentacao_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCatalogoSutentacao_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtCatalogoSutentacao_Atividade_Titleformat == 0 )
               {
                  context.SendWebValue( edtCatalogoSutentacao_Atividade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtCatalogoSutentacao_Atividade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavAssociarartefatos_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCatalogoSutentacao_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCatalogoSutentacao_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1752CatalogoSutentacao_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCatalogoSutentacao_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCatalogoSutentacao_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1753CatalogoSutentacao_Atividade);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtCatalogoSutentacao_Atividade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCatalogoSutentacao_Atividade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31AssociarArtefatos));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavAssociarartefatos_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAssociarartefatos_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 88 )
         {
            wbEnd = 0;
            nRC_GXsfl_88 = (short)(nGXsfl_88_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_85_N12e( true) ;
         }
         else
         {
            wb_table4_85_N12e( false) ;
         }
      }

      protected void wb_table2_8_N12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCatalogosutentacaotitle_Internalname, "Cat�logo Sustenta��o", "", "", lblCatalogosutentacaotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_N12( true) ;
         }
         else
         {
            wb_table5_13_N12( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_N12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWCatalogoSutentacao.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_N12( true) ;
         }
         else
         {
            wb_table6_23_N12( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_N12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_N12e( true) ;
         }
         else
         {
            wb_table2_8_N12e( false) ;
         }
      }

      protected void wb_table6_23_N12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_N12( true) ;
         }
         else
         {
            wb_table7_28_N12( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_N12e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_N12e( true) ;
         }
         else
         {
            wb_table6_23_N12e( false) ;
         }
      }

      protected void wb_table7_28_N12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWCatalogoSutentacao.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_N12( true) ;
         }
         else
         {
            wb_table8_37_N12( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_N12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCatalogoSutentacao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWCatalogoSutentacao.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_54_N12( true) ;
         }
         else
         {
            wb_table9_54_N12( false) ;
         }
         return  ;
      }

      protected void wb_table9_54_N12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCatalogoSutentacao.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWCatalogoSutentacao.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_71_N12( true) ;
         }
         else
         {
            wb_table10_71_N12( false) ;
         }
         return  ;
      }

      protected void wb_table10_71_N12e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_N12e( true) ;
         }
         else
         {
            wb_table7_28_N12e( false) ;
         }
      }

      protected void wb_table10_71_N12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWCatalogoSutentacao.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCatalogosutentacao_descricao3_Internalname, AV25CatalogoSutentacao_Descricao3, StringUtil.RTrim( context.localUtil.Format( AV25CatalogoSutentacao_Descricao3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCatalogosutentacao_descricao3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCatalogosutentacao_descricao3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_71_N12e( true) ;
         }
         else
         {
            wb_table10_71_N12e( false) ;
         }
      }

      protected void wb_table9_54_N12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWCatalogoSutentacao.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCatalogosutentacao_descricao2_Internalname, AV21CatalogoSutentacao_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV21CatalogoSutentacao_Descricao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCatalogosutentacao_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCatalogosutentacao_descricao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_54_N12e( true) ;
         }
         else
         {
            wb_table9_54_N12e( false) ;
         }
      }

      protected void wb_table8_37_N12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_88_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWCatalogoSutentacao.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_88_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCatalogosutentacao_descricao1_Internalname, AV17CatalogoSutentacao_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV17CatalogoSutentacao_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCatalogosutentacao_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavCatalogosutentacao_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_N12e( true) ;
         }
         else
         {
            wb_table8_37_N12e( false) ;
         }
      }

      protected void wb_table5_13_N12( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWCatalogoSutentacao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_N12e( true) ;
         }
         else
         {
            wb_table5_13_N12e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAN12( ) ;
         WSN12( ) ;
         WEN12( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311951050");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwcatalogosutentacao.js", "?2020311951051");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_idx;
         edtCatalogoSutentacao_Codigo_Internalname = "CATALOGOSUTENTACAO_CODIGO_"+sGXsfl_88_idx;
         edtCatalogoSutentacao_Descricao_Internalname = "CATALOGOSUTENTACAO_DESCRICAO_"+sGXsfl_88_idx;
         edtCatalogoSutentacao_Atividade_Internalname = "CATALOGOSUTENTACAO_ATIVIDADE_"+sGXsfl_88_idx;
         edtavAssociarartefatos_Internalname = "vASSOCIARARTEFATOS_"+sGXsfl_88_idx;
      }

      protected void SubsflControlProps_fel_882( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_88_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_88_fel_idx;
         edtCatalogoSutentacao_Codigo_Internalname = "CATALOGOSUTENTACAO_CODIGO_"+sGXsfl_88_fel_idx;
         edtCatalogoSutentacao_Descricao_Internalname = "CATALOGOSUTENTACAO_DESCRICAO_"+sGXsfl_88_fel_idx;
         edtCatalogoSutentacao_Atividade_Internalname = "CATALOGOSUTENTACAO_ATIVIDADE_"+sGXsfl_88_fel_idx;
         edtavAssociarartefatos_Internalname = "vASSOCIARARTEFATOS_"+sGXsfl_88_fel_idx;
      }

      protected void sendrow_882( )
      {
         SubsflControlProps_882( ) ;
         WBN10( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_88_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_88_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_88_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV69Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV69Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV70Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV70Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCatalogoSutentacao_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1750CatalogoSutentacao_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCatalogoSutentacao_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCatalogoSutentacao_Descricao_Internalname,(String)A1752CatalogoSutentacao_Descricao,StringUtil.RTrim( context.localUtil.Format( A1752CatalogoSutentacao_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCatalogoSutentacao_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCatalogoSutentacao_Atividade_Internalname,(String)A1753CatalogoSutentacao_Atividade,StringUtil.RTrim( context.localUtil.Format( A1753CatalogoSutentacao_Atividade, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCatalogoSutentacao_Atividade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)88,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAssociarartefatos_Enabled!=0)&&(edtavAssociarartefatos_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 94,'',false,'',88)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV31AssociarArtefatos_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31AssociarArtefatos))&&String.IsNullOrEmpty(StringUtil.RTrim( AV71Associarartefatos_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31AssociarArtefatos)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAssociarartefatos_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31AssociarArtefatos)) ? AV71Associarartefatos_GXI : context.PathToRelativeUrl( AV31AssociarArtefatos)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavAssociarartefatos_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavAssociarartefatos_Jsonclick,"'"+""+"'"+",false,"+"'"+"E\\'DOASSOCIARARTEFATOS\\'."+sGXsfl_88_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV31AssociarArtefatos_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CATALOGOSUTENTACAO_CODIGO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, context.localUtil.Format( (decimal)(A1750CatalogoSutentacao_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CATALOGOSUTENTACAO_DESCRICAO"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A1752CatalogoSutentacao_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_CATALOGOSUTENTACAO_ATIVIDADE"+"_"+sGXsfl_88_idx, GetSecureSignedToken( sGXsfl_88_idx, StringUtil.RTrim( context.localUtil.Format( A1753CatalogoSutentacao_Atividade, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_88_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_88_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_88_idx+1));
            sGXsfl_88_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_88_idx), 4, 0)), 4, "0");
            SubsflControlProps_882( ) ;
         }
         /* End function sendrow_882 */
      }

      protected void init_default_properties( )
      {
         lblCatalogosutentacaotitle_Internalname = "CATALOGOSUTENTACAOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavCatalogosutentacao_descricao1_Internalname = "vCATALOGOSUTENTACAO_DESCRICAO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavCatalogosutentacao_descricao2_Internalname = "vCATALOGOSUTENTACAO_DESCRICAO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavCatalogosutentacao_descricao3_Internalname = "vCATALOGOSUTENTACAO_DESCRICAO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtCatalogoSutentacao_Codigo_Internalname = "CATALOGOSUTENTACAO_CODIGO";
         edtCatalogoSutentacao_Descricao_Internalname = "CATALOGOSUTENTACAO_DESCRICAO";
         edtCatalogoSutentacao_Atividade_Internalname = "CATALOGOSUTENTACAO_ATIVIDADE";
         edtavAssociarartefatos_Internalname = "vASSOCIARARTEFATOS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfcatalogosutentacao_codigo_Internalname = "vTFCATALOGOSUTENTACAO_CODIGO";
         edtavTfcatalogosutentacao_codigo_to_Internalname = "vTFCATALOGOSUTENTACAO_CODIGO_TO";
         edtavTfcatalogosutentacao_descricao_Internalname = "vTFCATALOGOSUTENTACAO_DESCRICAO";
         edtavTfcatalogosutentacao_descricao_sel_Internalname = "vTFCATALOGOSUTENTACAO_DESCRICAO_SEL";
         edtavTfcatalogosutentacao_atividade_Internalname = "vTFCATALOGOSUTENTACAO_ATIVIDADE";
         edtavTfcatalogosutentacao_atividade_sel_Internalname = "vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL";
         Ddo_catalogosutentacao_codigo_Internalname = "DDO_CATALOGOSUTENTACAO_CODIGO";
         edtavDdo_catalogosutentacao_codigotitlecontrolidtoreplace_Internalname = "vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_catalogosutentacao_descricao_Internalname = "DDO_CATALOGOSUTENTACAO_DESCRICAO";
         edtavDdo_catalogosutentacao_descricaotitlecontrolidtoreplace_Internalname = "vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_catalogosutentacao_atividade_Internalname = "DDO_CATALOGOSUTENTACAO_ATIVIDADE";
         edtavDdo_catalogosutentacao_atividadetitlecontrolidtoreplace_Internalname = "vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavAssociarartefatos_Jsonclick = "";
         edtavAssociarartefatos_Visible = -1;
         edtavAssociarartefatos_Enabled = 1;
         edtCatalogoSutentacao_Atividade_Jsonclick = "";
         edtCatalogoSutentacao_Descricao_Jsonclick = "";
         edtCatalogoSutentacao_Codigo_Jsonclick = "";
         edtavCatalogosutentacao_descricao1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavCatalogosutentacao_descricao2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavCatalogosutentacao_descricao3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavAssociarartefatos_Tooltiptext = "Associar Artefatos";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtCatalogoSutentacao_Atividade_Titleformat = 0;
         edtCatalogoSutentacao_Descricao_Titleformat = 0;
         edtCatalogoSutentacao_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavCatalogosutentacao_descricao3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavCatalogosutentacao_descricao2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavCatalogosutentacao_descricao1_Visible = 1;
         edtavAssociarartefatos_Title = "";
         edtCatalogoSutentacao_Atividade_Title = "Atividade";
         edtCatalogoSutentacao_Descricao_Title = "Descri��o";
         edtCatalogoSutentacao_Codigo_Title = "C�digo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_catalogosutentacao_atividadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_catalogosutentacao_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_catalogosutentacao_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfcatalogosutentacao_atividade_sel_Jsonclick = "";
         edtavTfcatalogosutentacao_atividade_sel_Visible = 1;
         edtavTfcatalogosutentacao_atividade_Jsonclick = "";
         edtavTfcatalogosutentacao_atividade_Visible = 1;
         edtavTfcatalogosutentacao_descricao_sel_Jsonclick = "";
         edtavTfcatalogosutentacao_descricao_sel_Visible = 1;
         edtavTfcatalogosutentacao_descricao_Jsonclick = "";
         edtavTfcatalogosutentacao_descricao_Visible = 1;
         edtavTfcatalogosutentacao_codigo_to_Jsonclick = "";
         edtavTfcatalogosutentacao_codigo_to_Visible = 1;
         edtavTfcatalogosutentacao_codigo_Jsonclick = "";
         edtavTfcatalogosutentacao_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_catalogosutentacao_atividade_Searchbuttontext = "Pesquisar";
         Ddo_catalogosutentacao_atividade_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_catalogosutentacao_atividade_Cleanfilter = "Limpar pesquisa";
         Ddo_catalogosutentacao_atividade_Loadingdata = "Carregando dados...";
         Ddo_catalogosutentacao_atividade_Sortdsc = "Ordenar de Z � A";
         Ddo_catalogosutentacao_atividade_Sortasc = "Ordenar de A � Z";
         Ddo_catalogosutentacao_atividade_Datalistupdateminimumcharacters = 0;
         Ddo_catalogosutentacao_atividade_Datalistproc = "GetWWCatalogoSutentacaoFilterData";
         Ddo_catalogosutentacao_atividade_Datalisttype = "Dynamic";
         Ddo_catalogosutentacao_atividade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_atividade_Filterisrange = Convert.ToBoolean( 0);
         Ddo_catalogosutentacao_atividade_Filtertype = "Character";
         Ddo_catalogosutentacao_atividade_Includefilter = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_atividade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_atividade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_atividade_Titlecontrolidtoreplace = "";
         Ddo_catalogosutentacao_atividade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_catalogosutentacao_atividade_Cls = "ColumnSettings";
         Ddo_catalogosutentacao_atividade_Tooltip = "Op��es";
         Ddo_catalogosutentacao_atividade_Caption = "";
         Ddo_catalogosutentacao_descricao_Searchbuttontext = "Pesquisar";
         Ddo_catalogosutentacao_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_catalogosutentacao_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_catalogosutentacao_descricao_Loadingdata = "Carregando dados...";
         Ddo_catalogosutentacao_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_catalogosutentacao_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_catalogosutentacao_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_catalogosutentacao_descricao_Datalistproc = "GetWWCatalogoSutentacaoFilterData";
         Ddo_catalogosutentacao_descricao_Datalisttype = "Dynamic";
         Ddo_catalogosutentacao_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_catalogosutentacao_descricao_Filtertype = "Character";
         Ddo_catalogosutentacao_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_descricao_Titlecontrolidtoreplace = "";
         Ddo_catalogosutentacao_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_catalogosutentacao_descricao_Cls = "ColumnSettings";
         Ddo_catalogosutentacao_descricao_Tooltip = "Op��es";
         Ddo_catalogosutentacao_descricao_Caption = "";
         Ddo_catalogosutentacao_codigo_Searchbuttontext = "Pesquisar";
         Ddo_catalogosutentacao_codigo_Rangefilterto = "At�";
         Ddo_catalogosutentacao_codigo_Rangefilterfrom = "Desde";
         Ddo_catalogosutentacao_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_catalogosutentacao_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_catalogosutentacao_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_catalogosutentacao_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_catalogosutentacao_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_codigo_Filtertype = "Numeric";
         Ddo_catalogosutentacao_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_catalogosutentacao_codigo_Titlecontrolidtoreplace = "";
         Ddo_catalogosutentacao_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_catalogosutentacao_codigo_Cls = "ColumnSettings";
         Ddo_catalogosutentacao_codigo_Tooltip = "Op��es";
         Ddo_catalogosutentacao_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Catalogo Sutentacao";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV34CatalogoSutentacao_CodigoTitleFilterData',fld:'vCATALOGOSUTENTACAO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38CatalogoSutentacao_DescricaoTitleFilterData',fld:'vCATALOGOSUTENTACAO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV42CatalogoSutentacao_AtividadeTitleFilterData',fld:'vCATALOGOSUTENTACAO_ATIVIDADETITLEFILTERDATA',pic:'',nv:null},{av:'edtCatalogoSutentacao_Codigo_Titleformat',ctrl:'CATALOGOSUTENTACAO_CODIGO',prop:'Titleformat'},{av:'edtCatalogoSutentacao_Codigo_Title',ctrl:'CATALOGOSUTENTACAO_CODIGO',prop:'Title'},{av:'edtCatalogoSutentacao_Descricao_Titleformat',ctrl:'CATALOGOSUTENTACAO_DESCRICAO',prop:'Titleformat'},{av:'edtCatalogoSutentacao_Descricao_Title',ctrl:'CATALOGOSUTENTACAO_DESCRICAO',prop:'Title'},{av:'edtCatalogoSutentacao_Atividade_Titleformat',ctrl:'CATALOGOSUTENTACAO_ATIVIDADE',prop:'Titleformat'},{av:'edtCatalogoSutentacao_Atividade_Title',ctrl:'CATALOGOSUTENTACAO_ATIVIDADE',prop:'Title'},{av:'AV48GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV49GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavAssociarartefatos_Title',ctrl:'vASSOCIARARTEFATOS',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11N12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_CATALOGOSUTENTACAO_CODIGO.ONOPTIONCLICKED","{handler:'E12N12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_catalogosutentacao_codigo_Activeeventkey',ctrl:'DDO_CATALOGOSUTENTACAO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_catalogosutentacao_codigo_Filteredtext_get',ctrl:'DDO_CATALOGOSUTENTACAO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_catalogosutentacao_codigo_Filteredtextto_get',ctrl:'DDO_CATALOGOSUTENTACAO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_catalogosutentacao_codigo_Sortedstatus',ctrl:'DDO_CATALOGOSUTENTACAO_CODIGO',prop:'SortedStatus'},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_catalogosutentacao_descricao_Sortedstatus',ctrl:'DDO_CATALOGOSUTENTACAO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_catalogosutentacao_atividade_Sortedstatus',ctrl:'DDO_CATALOGOSUTENTACAO_ATIVIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CATALOGOSUTENTACAO_DESCRICAO.ONOPTIONCLICKED","{handler:'E13N12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_catalogosutentacao_descricao_Activeeventkey',ctrl:'DDO_CATALOGOSUTENTACAO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_catalogosutentacao_descricao_Filteredtext_get',ctrl:'DDO_CATALOGOSUTENTACAO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_catalogosutentacao_descricao_Selectedvalue_get',ctrl:'DDO_CATALOGOSUTENTACAO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_catalogosutentacao_descricao_Sortedstatus',ctrl:'DDO_CATALOGOSUTENTACAO_DESCRICAO',prop:'SortedStatus'},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_catalogosutentacao_codigo_Sortedstatus',ctrl:'DDO_CATALOGOSUTENTACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_catalogosutentacao_atividade_Sortedstatus',ctrl:'DDO_CATALOGOSUTENTACAO_ATIVIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CATALOGOSUTENTACAO_ATIVIDADE.ONOPTIONCLICKED","{handler:'E14N12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_catalogosutentacao_atividade_Activeeventkey',ctrl:'DDO_CATALOGOSUTENTACAO_ATIVIDADE',prop:'ActiveEventKey'},{av:'Ddo_catalogosutentacao_atividade_Filteredtext_get',ctrl:'DDO_CATALOGOSUTENTACAO_ATIVIDADE',prop:'FilteredText_get'},{av:'Ddo_catalogosutentacao_atividade_Selectedvalue_get',ctrl:'DDO_CATALOGOSUTENTACAO_ATIVIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_catalogosutentacao_atividade_Sortedstatus',ctrl:'DDO_CATALOGOSUTENTACAO_ATIVIDADE',prop:'SortedStatus'},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'Ddo_catalogosutentacao_codigo_Sortedstatus',ctrl:'DDO_CATALOGOSUTENTACAO_CODIGO',prop:'SortedStatus'},{av:'Ddo_catalogosutentacao_descricao_Sortedstatus',ctrl:'DDO_CATALOGOSUTENTACAO_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E28N12',iparms:[{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV31AssociarArtefatos',fld:'vASSOCIARARTEFATOS',pic:'',nv:''},{av:'edtavAssociarartefatos_Tooltiptext',ctrl:'vASSOCIARARTEFATOS',prop:'Tooltiptext'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15N12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21N12',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16N12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavCatalogosutentacao_descricao2_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCatalogosutentacao_descricao3_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavCatalogosutentacao_descricao1_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22N12',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavCatalogosutentacao_descricao1_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E23N12',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17N12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavCatalogosutentacao_descricao2_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCatalogosutentacao_descricao3_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavCatalogosutentacao_descricao1_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E24N12',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavCatalogosutentacao_descricao2_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18N12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavCatalogosutentacao_descricao2_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCatalogosutentacao_descricao3_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavCatalogosutentacao_descricao1_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E25N12',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavCatalogosutentacao_descricao3_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19N12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_catalogosutentacao_codigo_Filteredtext_set',ctrl:'DDO_CATALOGOSUTENTACAO_CODIGO',prop:'FilteredText_set'},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_catalogosutentacao_codigo_Filteredtextto_set',ctrl:'DDO_CATALOGOSUTENTACAO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_catalogosutentacao_descricao_Filteredtext_set',ctrl:'DDO_CATALOGOSUTENTACAO_DESCRICAO',prop:'FilteredText_set'},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_catalogosutentacao_descricao_Selectedvalue_set',ctrl:'DDO_CATALOGOSUTENTACAO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'Ddo_catalogosutentacao_atividade_Filteredtext_set',ctrl:'DDO_CATALOGOSUTENTACAO_ATIVIDADE',prop:'FilteredText_set'},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'Ddo_catalogosutentacao_atividade_Selectedvalue_set',ctrl:'DDO_CATALOGOSUTENTACAO_ATIVIDADE',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavCatalogosutentacao_descricao1_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavCatalogosutentacao_descricao2_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavCatalogosutentacao_descricao3_Visible',ctrl:'vCATALOGOSUTENTACAO_DESCRICAO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOASSOCIARARTEFATOS'","{handler:'E29N12',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17CatalogoSutentacao_Descricao1',fld:'vCATALOGOSUTENTACAO_DESCRICAO1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21CatalogoSutentacao_Descricao2',fld:'vCATALOGOSUTENTACAO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25CatalogoSutentacao_Descricao3',fld:'vCATALOGOSUTENTACAO_DESCRICAO3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFCatalogoSutentacao_Codigo',fld:'vTFCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFCatalogoSutentacao_Codigo_To',fld:'vTFCATALOGOSUTENTACAO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFCatalogoSutentacao_Descricao',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO',pic:'@!',nv:''},{av:'AV40TFCatalogoSutentacao_Descricao_Sel',fld:'vTFCATALOGOSUTENTACAO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV43TFCatalogoSutentacao_Atividade',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE',pic:'@!',nv:''},{av:'AV44TFCatalogoSutentacao_Atividade_Sel',fld:'vTFCATALOGOSUTENTACAO_ATIVIDADE_SEL',pic:'@!',nv:''},{av:'AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace',fld:'vDDO_CATALOGOSUTENTACAO_ATIVIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E20N12',iparms:[{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_catalogosutentacao_codigo_Activeeventkey = "";
         Ddo_catalogosutentacao_codigo_Filteredtext_get = "";
         Ddo_catalogosutentacao_codigo_Filteredtextto_get = "";
         Ddo_catalogosutentacao_descricao_Activeeventkey = "";
         Ddo_catalogosutentacao_descricao_Filteredtext_get = "";
         Ddo_catalogosutentacao_descricao_Selectedvalue_get = "";
         Ddo_catalogosutentacao_atividade_Activeeventkey = "";
         Ddo_catalogosutentacao_atividade_Filteredtext_get = "";
         Ddo_catalogosutentacao_atividade_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17CatalogoSutentacao_Descricao1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21CatalogoSutentacao_Descricao2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25CatalogoSutentacao_Descricao3 = "";
         AV39TFCatalogoSutentacao_Descricao = "";
         AV40TFCatalogoSutentacao_Descricao_Sel = "";
         AV43TFCatalogoSutentacao_Atividade = "";
         AV44TFCatalogoSutentacao_Atividade_Sel = "";
         AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace = "";
         AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace = "";
         AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace = "";
         AV72Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV46DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV34CatalogoSutentacao_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38CatalogoSutentacao_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42CatalogoSutentacao_AtividadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_catalogosutentacao_codigo_Filteredtext_set = "";
         Ddo_catalogosutentacao_codigo_Filteredtextto_set = "";
         Ddo_catalogosutentacao_codigo_Sortedstatus = "";
         Ddo_catalogosutentacao_descricao_Filteredtext_set = "";
         Ddo_catalogosutentacao_descricao_Selectedvalue_set = "";
         Ddo_catalogosutentacao_descricao_Sortedstatus = "";
         Ddo_catalogosutentacao_atividade_Filteredtext_set = "";
         Ddo_catalogosutentacao_atividade_Selectedvalue_set = "";
         Ddo_catalogosutentacao_atividade_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV69Update_GXI = "";
         AV29Delete = "";
         AV70Delete_GXI = "";
         A1752CatalogoSutentacao_Descricao = "";
         A1753CatalogoSutentacao_Atividade = "";
         AV31AssociarArtefatos = "";
         AV71Associarartefatos_GXI = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = "";
         lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = "";
         lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = "";
         lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = "";
         lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = "";
         AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 = "";
         AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 = "";
         AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 = "";
         AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 = "";
         AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 = "";
         AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 = "";
         AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel = "";
         AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao = "";
         AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel = "";
         AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade = "";
         H00N12_A1753CatalogoSutentacao_Atividade = new String[] {""} ;
         H00N12_A1752CatalogoSutentacao_Descricao = new String[] {""} ;
         H00N12_A1750CatalogoSutentacao_Codigo = new int[1] ;
         H00N13_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblCatalogosutentacaotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcatalogosutentacao__default(),
            new Object[][] {
                new Object[] {
               H00N12_A1753CatalogoSutentacao_Atividade, H00N12_A1752CatalogoSutentacao_Descricao, H00N12_A1750CatalogoSutentacao_Codigo
               }
               , new Object[] {
               H00N13_AGRID_nRecordCount
               }
            }
         );
         AV72Pgmname = "WWCatalogoSutentacao";
         /* GeneXus formulas. */
         AV72Pgmname = "WWCatalogoSutentacao";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_88 ;
      private short nGXsfl_88_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_88_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 ;
      private short AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 ;
      private short AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 ;
      private short edtCatalogoSutentacao_Codigo_Titleformat ;
      private short edtCatalogoSutentacao_Descricao_Titleformat ;
      private short edtCatalogoSutentacao_Atividade_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV35TFCatalogoSutentacao_Codigo ;
      private int AV36TFCatalogoSutentacao_Codigo_To ;
      private int A1750CatalogoSutentacao_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_catalogosutentacao_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_catalogosutentacao_atividade_Datalistupdateminimumcharacters ;
      private int edtavTfcatalogosutentacao_codigo_Visible ;
      private int edtavTfcatalogosutentacao_codigo_to_Visible ;
      private int edtavTfcatalogosutentacao_descricao_Visible ;
      private int edtavTfcatalogosutentacao_descricao_sel_Visible ;
      private int edtavTfcatalogosutentacao_atividade_Visible ;
      private int edtavTfcatalogosutentacao_atividade_sel_Visible ;
      private int edtavDdo_catalogosutentacao_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_catalogosutentacao_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_catalogosutentacao_atividadetitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo ;
      private int AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV47PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavCatalogosutentacao_descricao1_Visible ;
      private int edtavCatalogosutentacao_descricao2_Visible ;
      private int edtavCatalogosutentacao_descricao3_Visible ;
      private int AV73GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavAssociarartefatos_Enabled ;
      private int edtavAssociarartefatos_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV48GridCurrentPage ;
      private long AV49GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_catalogosutentacao_codigo_Activeeventkey ;
      private String Ddo_catalogosutentacao_codigo_Filteredtext_get ;
      private String Ddo_catalogosutentacao_codigo_Filteredtextto_get ;
      private String Ddo_catalogosutentacao_descricao_Activeeventkey ;
      private String Ddo_catalogosutentacao_descricao_Filteredtext_get ;
      private String Ddo_catalogosutentacao_descricao_Selectedvalue_get ;
      private String Ddo_catalogosutentacao_atividade_Activeeventkey ;
      private String Ddo_catalogosutentacao_atividade_Filteredtext_get ;
      private String Ddo_catalogosutentacao_atividade_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_88_idx="0001" ;
      private String AV72Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_catalogosutentacao_codigo_Caption ;
      private String Ddo_catalogosutentacao_codigo_Tooltip ;
      private String Ddo_catalogosutentacao_codigo_Cls ;
      private String Ddo_catalogosutentacao_codigo_Filteredtext_set ;
      private String Ddo_catalogosutentacao_codigo_Filteredtextto_set ;
      private String Ddo_catalogosutentacao_codigo_Dropdownoptionstype ;
      private String Ddo_catalogosutentacao_codigo_Titlecontrolidtoreplace ;
      private String Ddo_catalogosutentacao_codigo_Sortedstatus ;
      private String Ddo_catalogosutentacao_codigo_Filtertype ;
      private String Ddo_catalogosutentacao_codigo_Sortasc ;
      private String Ddo_catalogosutentacao_codigo_Sortdsc ;
      private String Ddo_catalogosutentacao_codigo_Cleanfilter ;
      private String Ddo_catalogosutentacao_codigo_Rangefilterfrom ;
      private String Ddo_catalogosutentacao_codigo_Rangefilterto ;
      private String Ddo_catalogosutentacao_codigo_Searchbuttontext ;
      private String Ddo_catalogosutentacao_descricao_Caption ;
      private String Ddo_catalogosutentacao_descricao_Tooltip ;
      private String Ddo_catalogosutentacao_descricao_Cls ;
      private String Ddo_catalogosutentacao_descricao_Filteredtext_set ;
      private String Ddo_catalogosutentacao_descricao_Selectedvalue_set ;
      private String Ddo_catalogosutentacao_descricao_Dropdownoptionstype ;
      private String Ddo_catalogosutentacao_descricao_Titlecontrolidtoreplace ;
      private String Ddo_catalogosutentacao_descricao_Sortedstatus ;
      private String Ddo_catalogosutentacao_descricao_Filtertype ;
      private String Ddo_catalogosutentacao_descricao_Datalisttype ;
      private String Ddo_catalogosutentacao_descricao_Datalistproc ;
      private String Ddo_catalogosutentacao_descricao_Sortasc ;
      private String Ddo_catalogosutentacao_descricao_Sortdsc ;
      private String Ddo_catalogosutentacao_descricao_Loadingdata ;
      private String Ddo_catalogosutentacao_descricao_Cleanfilter ;
      private String Ddo_catalogosutentacao_descricao_Noresultsfound ;
      private String Ddo_catalogosutentacao_descricao_Searchbuttontext ;
      private String Ddo_catalogosutentacao_atividade_Caption ;
      private String Ddo_catalogosutentacao_atividade_Tooltip ;
      private String Ddo_catalogosutentacao_atividade_Cls ;
      private String Ddo_catalogosutentacao_atividade_Filteredtext_set ;
      private String Ddo_catalogosutentacao_atividade_Selectedvalue_set ;
      private String Ddo_catalogosutentacao_atividade_Dropdownoptionstype ;
      private String Ddo_catalogosutentacao_atividade_Titlecontrolidtoreplace ;
      private String Ddo_catalogosutentacao_atividade_Sortedstatus ;
      private String Ddo_catalogosutentacao_atividade_Filtertype ;
      private String Ddo_catalogosutentacao_atividade_Datalisttype ;
      private String Ddo_catalogosutentacao_atividade_Datalistproc ;
      private String Ddo_catalogosutentacao_atividade_Sortasc ;
      private String Ddo_catalogosutentacao_atividade_Sortdsc ;
      private String Ddo_catalogosutentacao_atividade_Loadingdata ;
      private String Ddo_catalogosutentacao_atividade_Cleanfilter ;
      private String Ddo_catalogosutentacao_atividade_Noresultsfound ;
      private String Ddo_catalogosutentacao_atividade_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfcatalogosutentacao_codigo_Internalname ;
      private String edtavTfcatalogosutentacao_codigo_Jsonclick ;
      private String edtavTfcatalogosutentacao_codigo_to_Internalname ;
      private String edtavTfcatalogosutentacao_codigo_to_Jsonclick ;
      private String edtavTfcatalogosutentacao_descricao_Internalname ;
      private String edtavTfcatalogosutentacao_descricao_Jsonclick ;
      private String edtavTfcatalogosutentacao_descricao_sel_Internalname ;
      private String edtavTfcatalogosutentacao_descricao_sel_Jsonclick ;
      private String edtavTfcatalogosutentacao_atividade_Internalname ;
      private String edtavTfcatalogosutentacao_atividade_Jsonclick ;
      private String edtavTfcatalogosutentacao_atividade_sel_Internalname ;
      private String edtavTfcatalogosutentacao_atividade_sel_Jsonclick ;
      private String edtavDdo_catalogosutentacao_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_catalogosutentacao_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_catalogosutentacao_atividadetitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtCatalogoSutentacao_Codigo_Internalname ;
      private String edtCatalogoSutentacao_Descricao_Internalname ;
      private String edtCatalogoSutentacao_Atividade_Internalname ;
      private String edtavAssociarartefatos_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavCatalogosutentacao_descricao1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavCatalogosutentacao_descricao2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavCatalogosutentacao_descricao3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_catalogosutentacao_codigo_Internalname ;
      private String Ddo_catalogosutentacao_descricao_Internalname ;
      private String Ddo_catalogosutentacao_atividade_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtCatalogoSutentacao_Codigo_Title ;
      private String edtCatalogoSutentacao_Descricao_Title ;
      private String edtCatalogoSutentacao_Atividade_Title ;
      private String edtavAssociarartefatos_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavAssociarartefatos_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblCatalogosutentacaotitle_Internalname ;
      private String lblCatalogosutentacaotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavCatalogosutentacao_descricao3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavCatalogosutentacao_descricao2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavCatalogosutentacao_descricao1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_88_fel_idx="0001" ;
      private String ROClassString ;
      private String edtCatalogoSutentacao_Codigo_Jsonclick ;
      private String edtCatalogoSutentacao_Descricao_Jsonclick ;
      private String edtCatalogoSutentacao_Atividade_Jsonclick ;
      private String edtavAssociarartefatos_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_catalogosutentacao_codigo_Includesortasc ;
      private bool Ddo_catalogosutentacao_codigo_Includesortdsc ;
      private bool Ddo_catalogosutentacao_codigo_Includefilter ;
      private bool Ddo_catalogosutentacao_codigo_Filterisrange ;
      private bool Ddo_catalogosutentacao_codigo_Includedatalist ;
      private bool Ddo_catalogosutentacao_descricao_Includesortasc ;
      private bool Ddo_catalogosutentacao_descricao_Includesortdsc ;
      private bool Ddo_catalogosutentacao_descricao_Includefilter ;
      private bool Ddo_catalogosutentacao_descricao_Filterisrange ;
      private bool Ddo_catalogosutentacao_descricao_Includedatalist ;
      private bool Ddo_catalogosutentacao_atividade_Includesortasc ;
      private bool Ddo_catalogosutentacao_atividade_Includesortdsc ;
      private bool Ddo_catalogosutentacao_atividade_Includefilter ;
      private bool Ddo_catalogosutentacao_atividade_Filterisrange ;
      private bool Ddo_catalogosutentacao_atividade_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 ;
      private bool AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private bool AV31AssociarArtefatos_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17CatalogoSutentacao_Descricao1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21CatalogoSutentacao_Descricao2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25CatalogoSutentacao_Descricao3 ;
      private String AV39TFCatalogoSutentacao_Descricao ;
      private String AV40TFCatalogoSutentacao_Descricao_Sel ;
      private String AV43TFCatalogoSutentacao_Atividade ;
      private String AV44TFCatalogoSutentacao_Atividade_Sel ;
      private String AV37ddo_CatalogoSutentacao_CodigoTitleControlIdToReplace ;
      private String AV41ddo_CatalogoSutentacao_DescricaoTitleControlIdToReplace ;
      private String AV45ddo_CatalogoSutentacao_AtividadeTitleControlIdToReplace ;
      private String AV69Update_GXI ;
      private String AV70Delete_GXI ;
      private String A1752CatalogoSutentacao_Descricao ;
      private String A1753CatalogoSutentacao_Atividade ;
      private String AV71Associarartefatos_GXI ;
      private String lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ;
      private String lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ;
      private String lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ;
      private String lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ;
      private String lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ;
      private String AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 ;
      private String AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ;
      private String AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 ;
      private String AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ;
      private String AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 ;
      private String AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ;
      private String AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel ;
      private String AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ;
      private String AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel ;
      private String AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ;
      private String AV28Update ;
      private String AV29Delete ;
      private String AV31AssociarArtefatos ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00N12_A1753CatalogoSutentacao_Atividade ;
      private String[] H00N12_A1752CatalogoSutentacao_Descricao ;
      private int[] H00N12_A1750CatalogoSutentacao_Codigo ;
      private long[] H00N13_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34CatalogoSutentacao_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38CatalogoSutentacao_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42CatalogoSutentacao_AtividadeTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV46DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwcatalogosutentacao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00N12( IGxContext context ,
                                             String AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 ,
                                             short AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ,
                                             bool AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 ,
                                             short AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 ,
                                             String AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ,
                                             bool AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 ,
                                             short AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 ,
                                             String AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ,
                                             int AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo ,
                                             int AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to ,
                                             String AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel ,
                                             String AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ,
                                             String AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel ,
                                             String AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ,
                                             String A1752CatalogoSutentacao_Descricao ,
                                             int A1750CatalogoSutentacao_Codigo ,
                                             String A1753CatalogoSutentacao_Atividade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [17] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [CatalogoSutentacao_Atividade], [CatalogoSutentacao_Descricao], [CatalogoSutentacao_Codigo]";
         sFromString = " FROM [CatalogoSutentacao] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Codigo] >= @AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Codigo] >= @AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Codigo] <= @AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Codigo] <= @AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] = @AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] = @AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Atividade] like @lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Atividade] like @lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Atividade] = @AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Atividade] = @AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [CatalogoSutentacao_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [CatalogoSutentacao_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [CatalogoSutentacao_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [CatalogoSutentacao_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [CatalogoSutentacao_Atividade]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [CatalogoSutentacao_Atividade] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [CatalogoSutentacao_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00N13( IGxContext context ,
                                             String AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1 ,
                                             short AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 ,
                                             String AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1 ,
                                             bool AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 ,
                                             String AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2 ,
                                             short AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 ,
                                             String AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2 ,
                                             bool AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 ,
                                             String AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3 ,
                                             short AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 ,
                                             String AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3 ,
                                             int AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo ,
                                             int AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to ,
                                             String AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel ,
                                             String AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao ,
                                             String AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel ,
                                             String AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade ,
                                             String A1752CatalogoSutentacao_Descricao ,
                                             int A1750CatalogoSutentacao_Codigo ,
                                             String A1753CatalogoSutentacao_Atividade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [12] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [CatalogoSutentacao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52WWCatalogoSutentacaoDS_1_Dynamicfiltersselector1, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV53WWCatalogoSutentacaoDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV55WWCatalogoSutentacaoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV56WWCatalogoSutentacaoDS_5_Dynamicfiltersselector2, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV57WWCatalogoSutentacaoDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV59WWCatalogoSutentacaoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV60WWCatalogoSutentacaoDS_9_Dynamicfiltersselector3, "CATALOGOSUTENTACAO_DESCRICAO") == 0 ) && ( AV61WWCatalogoSutentacaoDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like '%' + @lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like '%' + @lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Codigo] >= @AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Codigo] >= @AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Codigo] <= @AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Codigo] <= @AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] like @lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] like @lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Descricao] = @AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Descricao] = @AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Atividade] like @lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Atividade] like @lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([CatalogoSutentacao_Atividade] = @AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([CatalogoSutentacao_Atividade] = @AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00N12(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
               case 1 :
                     return conditional_H00N13(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (bool)dynConstraints[21] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00N12 ;
          prmH00N12 = new Object[] {
          new Object[] {"@lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00N13 ;
          prmH00N13 = new Object[] {
          new Object[] {"@lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV54WWCatalogoSutentacaoDS_3_Catalogosutentacao_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV58WWCatalogoSutentacaoDS_7_Catalogosutentacao_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV62WWCatalogoSutentacaoDS_11_Catalogosutentacao_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV63WWCatalogoSutentacaoDS_12_Tfcatalogosutentacao_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV64WWCatalogoSutentacaoDS_13_Tfcatalogosutentacao_codigo_to",SqlDbType.Int,6,0} ,
          new Object[] {"@lV65WWCatalogoSutentacaoDS_14_Tfcatalogosutentacao_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV66WWCatalogoSutentacaoDS_15_Tfcatalogosutentacao_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV67WWCatalogoSutentacaoDS_16_Tfcatalogosutentacao_atividade",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV68WWCatalogoSutentacaoDS_17_Tfcatalogosutentacao_atividade_sel",SqlDbType.VarChar,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00N12", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N12,11,0,true,false )
             ,new CursorDef("H00N13", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N13,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

}
