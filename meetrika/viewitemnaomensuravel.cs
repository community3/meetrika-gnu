/*
               File: ViewItemNaoMensuravel
        Description: View Item Nao Mensuravel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:46:6.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class viewitemnaomensuravel : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public viewitemnaomensuravel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public viewitemnaomensuravel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ItemNaoMensuravel_AreaTrabalhoCod ,
                           String aP1_ItemNaoMensuravel_Codigo ,
                           String aP2_TabCode )
      {
         this.AV12ItemNaoMensuravel_AreaTrabalhoCod = aP0_ItemNaoMensuravel_AreaTrabalhoCod;
         this.AV9ItemNaoMensuravel_Codigo = aP1_ItemNaoMensuravel_Codigo;
         this.AV7TabCode = aP2_TabCode;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV12ItemNaoMensuravel_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vITEMNAOMENSURAVEL_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12ItemNaoMensuravel_AreaTrabalhoCod), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV9ItemNaoMensuravel_Codigo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ItemNaoMensuravel_Codigo", AV9ItemNaoMensuravel_Codigo);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV9ItemNaoMensuravel_Codigo, "@!"))));
                  AV7TabCode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAEA2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTEA2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311746661");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("viewitemnaomensuravel.aspx") + "?" + UrlEncode("" +AV12ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV9ItemNaoMensuravel_Codigo)) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vITEMNAOMENSURAVEL_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12ItemNaoMensuravel_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vITEMNAOMENSURAVEL_CODIGO", StringUtil.RTrim( AV9ItemNaoMensuravel_Codigo));
         GxWebStd.gx_hidden_field( context, "vTABCODE", StringUtil.RTrim( AV7TabCode));
         GxWebStd.gx_hidden_field( context, "gxhash_ITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vITEMNAOMENSURAVEL_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12ItemNaoMensuravel_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV9ItemNaoMensuravel_Codigo, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vITEMNAOMENSURAVEL_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12ItemNaoMensuravel_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV9ItemNaoMensuravel_Codigo, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ViewItemNaoMensuravel";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("viewitemnaomensuravel:[SendSecurityCheck value for]"+"ItemNaoMensuravel_Codigo:"+StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            WebComp_Tabbedview.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEEA2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTEA2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("viewitemnaomensuravel.aspx") + "?" + UrlEncode("" +AV12ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV9ItemNaoMensuravel_Codigo)) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode)) ;
      }

      public override String GetPgmname( )
      {
         return "ViewItemNaoMensuravel" ;
      }

      public override String GetPgmdesc( )
      {
         return "View Item Nao Mensuravel" ;
      }

      protected void WBEA0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_EA2( true) ;
         }
         else
         {
            wb_table1_2_EA2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EA2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTEA2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "View Item Nao Mensuravel", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPEA0( ) ;
      }

      protected void WSEA2( )
      {
         STARTEA2( ) ;
         EVTEA2( ) ;
      }

      protected void EVTEA2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11EA2 */
                              E11EA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12EA2 */
                              E12EA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 16 )
                        {
                           OldTabbedview = cgiGet( "W0016");
                           if ( ( StringUtil.Len( OldTabbedview) == 0 ) || ( StringUtil.StrCmp(OldTabbedview, WebComp_Tabbedview_Component) != 0 ) )
                           {
                              WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", OldTabbedview, new Object[] {context} );
                              WebComp_Tabbedview.ComponentInit();
                              WebComp_Tabbedview.Name = "OldTabbedview";
                              WebComp_Tabbedview_Component = OldTabbedview;
                           }
                           if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
                           {
                              WebComp_Tabbedview.componentprocess("W0016", "", sEvt);
                           }
                           WebComp_Tabbedview_Component = OldTabbedview;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEA2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAEA2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEA2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFEA2( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  WebComp_Tabbedview.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00EA2 */
            pr_default.execute(0, new Object[] {AV12ItemNaoMensuravel_AreaTrabalhoCod, AV9ItemNaoMensuravel_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A718ItemNaoMensuravel_AreaTrabalhoCod = H00EA2_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
               A715ItemNaoMensuravel_Codigo = H00EA2_A715ItemNaoMensuravel_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_ITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!"))));
               /* Execute user event: E12EA2 */
               E12EA2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBEA0( ) ;
         }
      }

      protected void STRUPEA0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11EA2 */
         E11EA2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A715ItemNaoMensuravel_Codigo = StringUtil.Upper( cgiGet( edtItemNaoMensuravel_Codigo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_ITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!"))));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "ViewItemNaoMensuravel";
            A715ItemNaoMensuravel_Codigo = cgiGet( edtItemNaoMensuravel_Codigo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_ITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!"))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!"));
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("viewitemnaomensuravel:[SecurityCheckFailed value for]"+"ItemNaoMensuravel_Codigo:"+StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11EA2 */
         E11EA2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11EA2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         lblWorkwithlink_Link = formatLink("wwitemnaomensuravel.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         AV15GXLvl9 = 0;
         /* Using cursor H00EA3 */
         pr_default.execute(1, new Object[] {AV12ItemNaoMensuravel_AreaTrabalhoCod, AV9ItemNaoMensuravel_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A715ItemNaoMensuravel_Codigo = H00EA3_A715ItemNaoMensuravel_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A715ItemNaoMensuravel_Codigo", A715ItemNaoMensuravel_Codigo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_ITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!"))));
            A718ItemNaoMensuravel_AreaTrabalhoCod = H00EA3_A718ItemNaoMensuravel_AreaTrabalhoCod[0];
            A714ItemNaoMensuravel_Descricao = H00EA3_A714ItemNaoMensuravel_Descricao[0];
            AV15GXLvl9 = 1;
            Form.Caption = A714ItemNaoMensuravel_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = true;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( AV15GXLvl9 == 0 )
         {
            Form.Caption = "Registro n�o encontrado ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = false;
         }
         if ( AV8Exists )
         {
            /* Execute user subroutine: 'LOADTABS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabbedview_Component), StringUtil.Lower( "WWPBaseObjects.WWPTabbedView")) != 0 )
            {
               WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", "wwpbaseobjects.wwptabbedview", new Object[] {context} );
               WebComp_Tabbedview.ComponentInit();
               WebComp_Tabbedview.Name = "WWPBaseObjects.WWPTabbedView";
               WebComp_Tabbedview_Component = "WWPBaseObjects.WWPTabbedView";
            }
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.setjustcreated();
               WebComp_Tabbedview.componentprepare(new Object[] {(String)"W0016",(String)"",(IGxCollection)AV10Tabs,(String)AV7TabCode});
               WebComp_Tabbedview.componentbind(new Object[] {(String)"",(String)""});
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12EA2( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'LOADTABS' Routine */
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "General";
         AV11Tab.gxTpr_Description = "Dados";
         AV11Tab.gxTpr_Webcomponent = formatLink("itemnaomensuravelgeneral.aspx") + "?" + UrlEncode("" +AV12ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV9ItemNaoMensuravel_Codigo));
         AV11Tab.gxTpr_Link = formatLink("viewitemnaomensuravel.aspx") + "?" + UrlEncode("" +AV12ItemNaoMensuravel_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV9ItemNaoMensuravel_Codigo)) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 0;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
      }

      protected void wb_table1_2_EA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableContentNoMargin", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_5_EA2( true) ;
         }
         else
         {
            wb_table2_5_EA2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_EA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='TableTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblWorkwithlink_Internalname, "Voltar", lblWorkwithlink_Link, "", lblWorkwithlink_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockLink", 0, "", 1, 1, 0, "HLP_ViewItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0016"+"", StringUtil.RTrim( WebComp_Tabbedview_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0016"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0016"+"");
                  }
                  WebComp_Tabbedview.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EA2e( true) ;
         }
         else
         {
            wb_table1_2_EA2e( false) ;
         }
      }

      protected void wb_table2_5_EA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Item N�o Mensuravel :: ", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtItemNaoMensuravel_Codigo_Internalname, StringUtil.RTrim( A715ItemNaoMensuravel_Codigo), StringUtil.RTrim( context.localUtil.Format( A715ItemNaoMensuravel_Codigo, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtItemNaoMensuravel_Codigo_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "CodigoINM20", "left", true, "HLP_ViewItemNaoMensuravel.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_EA2e( true) ;
         }
         else
         {
            wb_table2_5_EA2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV12ItemNaoMensuravel_AreaTrabalhoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ItemNaoMensuravel_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12ItemNaoMensuravel_AreaTrabalhoCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vITEMNAOMENSURAVEL_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12ItemNaoMensuravel_AreaTrabalhoCod), "ZZZZZ9")));
         AV9ItemNaoMensuravel_Codigo = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ItemNaoMensuravel_Codigo", AV9ItemNaoMensuravel_Codigo);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vITEMNAOMENSURAVEL_CODIGO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV9ItemNaoMensuravel_Codigo, "@!"))));
         AV7TabCode = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEA2( ) ;
         WSEA2( ) ;
         WEEA2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020311746680");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("viewitemnaomensuravel.js", "?2020311746680");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtItemNaoMensuravel_Codigo_Internalname = "ITEMNAOMENSURAVEL_CODIGO";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblWorkwithlink_Internalname = "WORKWITHLINK";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtItemNaoMensuravel_Codigo_Jsonclick = "";
         lblWorkwithlink_Link = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "View Item Nao Mensuravel";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV9ItemNaoMensuravel_Codigo = "";
         wcpOAV7TabCode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A715ItemNaoMensuravel_Codigo = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabbedview = "";
         WebComp_Tabbedview_Component = "";
         scmdbuf = "";
         H00EA2_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         H00EA2_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00EA3_A715ItemNaoMensuravel_Codigo = new String[] {""} ;
         H00EA3_A718ItemNaoMensuravel_AreaTrabalhoCod = new int[1] ;
         H00EA3_A714ItemNaoMensuravel_Descricao = new String[] {""} ;
         A714ItemNaoMensuravel_Descricao = "";
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         sStyleString = "";
         lblWorkwithlink_Jsonclick = "";
         lblViewtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.viewitemnaomensuravel__default(),
            new Object[][] {
                new Object[] {
               H00EA2_A718ItemNaoMensuravel_AreaTrabalhoCod, H00EA2_A715ItemNaoMensuravel_Codigo
               }
               , new Object[] {
               H00EA3_A715ItemNaoMensuravel_Codigo, H00EA3_A718ItemNaoMensuravel_AreaTrabalhoCod, H00EA3_A714ItemNaoMensuravel_Descricao
               }
            }
         );
         WebComp_Tabbedview = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV15GXLvl9 ;
      private short nGXWrapped ;
      private int AV12ItemNaoMensuravel_AreaTrabalhoCod ;
      private int wcpOAV12ItemNaoMensuravel_AreaTrabalhoCod ;
      private int A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private int idxLst ;
      private String AV9ItemNaoMensuravel_Codigo ;
      private String AV7TabCode ;
      private String wcpOAV9ItemNaoMensuravel_Codigo ;
      private String wcpOAV7TabCode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A715ItemNaoMensuravel_Codigo ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabbedview ;
      private String WebComp_Tabbedview_Component ;
      private String scmdbuf ;
      private String edtItemNaoMensuravel_Codigo_Internalname ;
      private String hsh ;
      private String lblWorkwithlink_Link ;
      private String lblWorkwithlink_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblWorkwithlink_Jsonclick ;
      private String tblTablemergedviewtitle_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String edtItemNaoMensuravel_Codigo_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV8Exists ;
      private String A714ItemNaoMensuravel_Descricao ;
      private GXWebComponent WebComp_Tabbedview ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00EA2_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] H00EA2_A715ItemNaoMensuravel_Codigo ;
      private String[] H00EA3_A715ItemNaoMensuravel_Codigo ;
      private int[] H00EA3_A718ItemNaoMensuravel_AreaTrabalhoCod ;
      private String[] H00EA3_A714ItemNaoMensuravel_Descricao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem ))]
      private IGxCollection AV10Tabs ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem AV11Tab ;
   }

   public class viewitemnaomensuravel__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EA2 ;
          prmH00EA2 = new Object[] {
          new Object[] {"@AV12ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0}
          } ;
          Object[] prmH00EA3 ;
          prmH00EA3 = new Object[] {
          new Object[] {"@AV12ItemNaoMensuravel_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9ItemNaoMensuravel_Codigo",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EA2", "SELECT [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo] FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE [ItemNaoMensuravel_AreaTrabalhoCod] = @AV12ItemNaoMensuravel_AreaTrabalhoCod and [ItemNaoMensuravel_Codigo] = @AV9ItemNaoMensuravel_Codigo ORDER BY [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EA2,1,0,true,true )
             ,new CursorDef("H00EA3", "SELECT [ItemNaoMensuravel_Codigo], [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Descricao] FROM [ItemNaoMensuravel] WITH (NOLOCK) WHERE ([ItemNaoMensuravel_AreaTrabalhoCod] = @AV12ItemNaoMensuravel_AreaTrabalhoCod and [ItemNaoMensuravel_Codigo] = @AV9ItemNaoMensuravel_Codigo) AND ([ItemNaoMensuravel_AreaTrabalhoCod] = @AV12ItemNaoMensuravel_AreaTrabalhoCod and [ItemNaoMensuravel_Codigo] = @AV9ItemNaoMensuravel_Codigo) ORDER BY [ItemNaoMensuravel_AreaTrabalhoCod], [ItemNaoMensuravel_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EA3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
       }
    }

 }

}
