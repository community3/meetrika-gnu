/*
               File: GetGeral_Tp_UOGeral_UnidadeOrganizacionalWCFilterData
        Description: Get Geral_Tp_UOGeral_Unidade Organizacional WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:17.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata : GXProcedure
   {
      public getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV18DDOName = aP0_DDOName;
         this.AV16SearchTxt = aP1_SearchTxt;
         this.AV17SearchTxtTo = aP2_SearchTxtTo;
         this.AV22OptionsJson = "" ;
         this.AV25OptionsDescJson = "" ;
         this.AV27OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
         return AV27OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata objgetgeral_tp_uogeral_unidadeorganizacionalwcfilterdata;
         objgetgeral_tp_uogeral_unidadeorganizacionalwcfilterdata = new getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata();
         objgetgeral_tp_uogeral_unidadeorganizacionalwcfilterdata.AV18DDOName = aP0_DDOName;
         objgetgeral_tp_uogeral_unidadeorganizacionalwcfilterdata.AV16SearchTxt = aP1_SearchTxt;
         objgetgeral_tp_uogeral_unidadeorganizacionalwcfilterdata.AV17SearchTxtTo = aP2_SearchTxtTo;
         objgetgeral_tp_uogeral_unidadeorganizacionalwcfilterdata.AV22OptionsJson = "" ;
         objgetgeral_tp_uogeral_unidadeorganizacionalwcfilterdata.AV25OptionsDescJson = "" ;
         objgetgeral_tp_uogeral_unidadeorganizacionalwcfilterdata.AV27OptionIndexesJson = "" ;
         objgetgeral_tp_uogeral_unidadeorganizacionalwcfilterdata.context.SetSubmitInitialConfig(context);
         objgetgeral_tp_uogeral_unidadeorganizacionalwcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetgeral_tp_uogeral_unidadeorganizacionalwcfilterdata);
         aP3_OptionsJson=this.AV22OptionsJson;
         aP4_OptionsDescJson=this.AV25OptionsDescJson;
         aP5_OptionIndexesJson=this.AV27OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV21Options = (IGxCollection)(new GxSimpleCollection());
         AV24OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV26OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUNIDADEORGANIZACIONAL_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_ESTADO_UF") == 0 )
         {
            /* Execute user subroutine: 'LOADESTADO_UFOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV18DDOName), "DDO_UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
         {
            /* Execute user subroutine: 'LOADUNIDADEORGANIZACIONAL_VINCULADAOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV22OptionsJson = AV21Options.ToJSonString(false);
         AV25OptionsDescJson = AV24OptionsDesc.ToJSonString(false);
         AV27OptionIndexesJson = AV26OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV29Session.Get("Geral_Tp_UOGeral_UnidadeOrganizacionalWCGridState"), "") == 0 )
         {
            AV31GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "Geral_Tp_UOGeral_UnidadeOrganizacionalWCGridState"), "");
         }
         else
         {
            AV31GridState.FromXml(AV29Session.Get("Geral_Tp_UOGeral_UnidadeOrganizacionalWCGridState"), "");
         }
         AV52GXV1 = 1;
         while ( AV52GXV1 <= AV31GridState.gxTpr_Filtervalues.Count )
         {
            AV32GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV52GXV1));
            if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "UNIDADEORGANIZACIONAL_ATIVO") == 0 )
            {
               AV34UnidadeOrganizacional_Ativo = BooleanUtil.Val( AV32GridStateFilterValue.gxTpr_Value);
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV10TFUnidadeOrganizacional_Nome = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME_SEL") == 0 )
            {
               AV11TFUnidadeOrganizacional_Nome_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFESTADO_UF") == 0 )
            {
               AV12TFEstado_UF = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFESTADO_UF_SEL") == 0 )
            {
               AV13TFEstado_UF_Sel = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_VINCULADA") == 0 )
            {
               AV14TFUnidadeOrganizacional_Vinculada = AV32GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_VINCULADA_SEL") == 0 )
            {
               AV15TFUnidadeOrganizacional_Vinculada_Sel = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Name, "PARM_&TPUO_CODIGO") == 0 )
            {
               AV49TpUo_Codigo = (int)(NumberUtil.Val( AV32GridStateFilterValue.gxTpr_Value, "."));
            }
            AV52GXV1 = (int)(AV52GXV1+1);
         }
         if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(1));
            AV35DynamicFiltersSelector1 = AV33GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV36UnidadeOrganizacional_Nome1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "ESTADO_UF") == 0 )
            {
               AV37Estado_UF1 = AV33GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
            {
               AV38UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( AV33GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV39DynamicFiltersEnabled2 = true;
               AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(2));
               AV40DynamicFiltersSelector2 = AV33GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
               {
                  AV41UnidadeOrganizacional_Nome2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "ESTADO_UF") == 0 )
               {
                  AV42Estado_UF2 = AV33GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
               {
                  AV43UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( AV33GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV31GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV44DynamicFiltersEnabled3 = true;
                  AV33GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV31GridState.gxTpr_Dynamicfilters.Item(3));
                  AV45DynamicFiltersSelector3 = AV33GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
                  {
                     AV46UnidadeOrganizacional_Nome3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "ESTADO_UF") == 0 )
                  {
                     AV47Estado_UF3 = AV33GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
                  {
                     AV48UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( AV33GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUNIDADEORGANIZACIONAL_NOMEOPTIONS' Routine */
         AV10TFUnidadeOrganizacional_Nome = AV16SearchTxt;
         AV11TFUnidadeOrganizacional_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36UnidadeOrganizacional_Nome1 ,
                                              AV37Estado_UF1 ,
                                              AV38UnidadeOrganizacional_Vinculada1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41UnidadeOrganizacional_Nome2 ,
                                              AV42Estado_UF2 ,
                                              AV43UnidadeOrganizacional_Vinculada2 ,
                                              AV44DynamicFiltersEnabled3 ,
                                              AV45DynamicFiltersSelector3 ,
                                              AV46UnidadeOrganizacional_Nome3 ,
                                              AV47Estado_UF3 ,
                                              AV48UnidadeOrganizacional_Vinculada3 ,
                                              AV11TFUnidadeOrganizacional_Nome_Sel ,
                                              AV10TFUnidadeOrganizacional_Nome ,
                                              AV13TFEstado_UF_Sel ,
                                              AV12TFEstado_UF ,
                                              AV15TFUnidadeOrganizacional_Vinculada_Sel ,
                                              AV14TFUnidadeOrganizacional_Vinculada ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A23Estado_UF ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              A629UnidadeOrganizacional_Ativo ,
                                              AV49TpUo_Codigo ,
                                              A609TpUo_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV36UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36UnidadeOrganizacional_Nome1), 50, "%");
         lV41UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41UnidadeOrganizacional_Nome2), 50, "%");
         lV46UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV46UnidadeOrganizacional_Nome3), 50, "%");
         lV10TFUnidadeOrganizacional_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome), 50, "%");
         lV12TFEstado_UF = StringUtil.PadR( StringUtil.RTrim( AV12TFEstado_UF), 2, "%");
         lV14TFUnidadeOrganizacional_Vinculada = StringUtil.PadR( StringUtil.RTrim( AV14TFUnidadeOrganizacional_Vinculada), 50, "%");
         /* Using cursor P00NX2 */
         pr_default.execute(0, new Object[] {AV49TpUo_Codigo, lV36UnidadeOrganizacional_Nome1, AV37Estado_UF1, AV38UnidadeOrganizacional_Vinculada1, lV41UnidadeOrganizacional_Nome2, AV42Estado_UF2, AV43UnidadeOrganizacional_Vinculada2, lV46UnidadeOrganizacional_Nome3, AV47Estado_UF3, AV48UnidadeOrganizacional_Vinculada3, lV10TFUnidadeOrganizacional_Nome, AV11TFUnidadeOrganizacional_Nome_Sel, lV12TFEstado_UF, AV13TFEstado_UF_Sel, lV14TFUnidadeOrganizacional_Vinculada, AV15TFUnidadeOrganizacional_Vinculada_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKNX2 = false;
            A609TpUo_Codigo = P00NX2_A609TpUo_Codigo[0];
            A629UnidadeOrganizacional_Ativo = P00NX2_A629UnidadeOrganizacional_Ativo[0];
            A612UnidadeOrganizacional_Nome = P00NX2_A612UnidadeOrganizacional_Nome[0];
            A613UnidadeOrganizacional_Vinculada = P00NX2_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P00NX2_n613UnidadeOrganizacional_Vinculada[0];
            A23Estado_UF = P00NX2_A23Estado_UF[0];
            A611UnidadeOrganizacional_Codigo = P00NX2_A611UnidadeOrganizacional_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00NX2_A609TpUo_Codigo[0] == A609TpUo_Codigo ) && ( StringUtil.StrCmp(P00NX2_A612UnidadeOrganizacional_Nome[0], A612UnidadeOrganizacional_Nome) == 0 ) )
            {
               BRKNX2 = false;
               A611UnidadeOrganizacional_Codigo = P00NX2_A611UnidadeOrganizacional_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKNX2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A612UnidadeOrganizacional_Nome)) )
            {
               AV20Option = A612UnidadeOrganizacional_Nome;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNX2 )
            {
               BRKNX2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADESTADO_UFOPTIONS' Routine */
         AV12TFEstado_UF = AV16SearchTxt;
         AV13TFEstado_UF_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36UnidadeOrganizacional_Nome1 ,
                                              AV37Estado_UF1 ,
                                              AV38UnidadeOrganizacional_Vinculada1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41UnidadeOrganizacional_Nome2 ,
                                              AV42Estado_UF2 ,
                                              AV43UnidadeOrganizacional_Vinculada2 ,
                                              AV44DynamicFiltersEnabled3 ,
                                              AV45DynamicFiltersSelector3 ,
                                              AV46UnidadeOrganizacional_Nome3 ,
                                              AV47Estado_UF3 ,
                                              AV48UnidadeOrganizacional_Vinculada3 ,
                                              AV11TFUnidadeOrganizacional_Nome_Sel ,
                                              AV10TFUnidadeOrganizacional_Nome ,
                                              AV13TFEstado_UF_Sel ,
                                              AV12TFEstado_UF ,
                                              AV15TFUnidadeOrganizacional_Vinculada_Sel ,
                                              AV14TFUnidadeOrganizacional_Vinculada ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A23Estado_UF ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              A629UnidadeOrganizacional_Ativo ,
                                              AV49TpUo_Codigo ,
                                              A609TpUo_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV36UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36UnidadeOrganizacional_Nome1), 50, "%");
         lV41UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41UnidadeOrganizacional_Nome2), 50, "%");
         lV46UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV46UnidadeOrganizacional_Nome3), 50, "%");
         lV10TFUnidadeOrganizacional_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome), 50, "%");
         lV12TFEstado_UF = StringUtil.PadR( StringUtil.RTrim( AV12TFEstado_UF), 2, "%");
         lV14TFUnidadeOrganizacional_Vinculada = StringUtil.PadR( StringUtil.RTrim( AV14TFUnidadeOrganizacional_Vinculada), 50, "%");
         /* Using cursor P00NX3 */
         pr_default.execute(1, new Object[] {AV49TpUo_Codigo, lV36UnidadeOrganizacional_Nome1, AV37Estado_UF1, AV38UnidadeOrganizacional_Vinculada1, lV41UnidadeOrganizacional_Nome2, AV42Estado_UF2, AV43UnidadeOrganizacional_Vinculada2, lV46UnidadeOrganizacional_Nome3, AV47Estado_UF3, AV48UnidadeOrganizacional_Vinculada3, lV10TFUnidadeOrganizacional_Nome, AV11TFUnidadeOrganizacional_Nome_Sel, lV12TFEstado_UF, AV13TFEstado_UF_Sel, lV14TFUnidadeOrganizacional_Vinculada, AV15TFUnidadeOrganizacional_Vinculada_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKNX4 = false;
            A609TpUo_Codigo = P00NX3_A609TpUo_Codigo[0];
            A629UnidadeOrganizacional_Ativo = P00NX3_A629UnidadeOrganizacional_Ativo[0];
            A23Estado_UF = P00NX3_A23Estado_UF[0];
            A613UnidadeOrganizacional_Vinculada = P00NX3_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P00NX3_n613UnidadeOrganizacional_Vinculada[0];
            A612UnidadeOrganizacional_Nome = P00NX3_A612UnidadeOrganizacional_Nome[0];
            A611UnidadeOrganizacional_Codigo = P00NX3_A611UnidadeOrganizacional_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( P00NX3_A609TpUo_Codigo[0] == A609TpUo_Codigo ) && ( StringUtil.StrCmp(P00NX3_A23Estado_UF[0], A23Estado_UF) == 0 ) )
            {
               BRKNX4 = false;
               A611UnidadeOrganizacional_Codigo = P00NX3_A611UnidadeOrganizacional_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKNX4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A23Estado_UF)) )
            {
               AV20Option = A23Estado_UF;
               AV21Options.Add(AV20Option, 0);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNX4 )
            {
               BRKNX4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADUNIDADEORGANIZACIONAL_VINCULADAOPTIONS' Routine */
         AV14TFUnidadeOrganizacional_Vinculada = AV16SearchTxt;
         AV15TFUnidadeOrganizacional_Vinculada_Sel = 0;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV35DynamicFiltersSelector1 ,
                                              AV36UnidadeOrganizacional_Nome1 ,
                                              AV37Estado_UF1 ,
                                              AV38UnidadeOrganizacional_Vinculada1 ,
                                              AV39DynamicFiltersEnabled2 ,
                                              AV40DynamicFiltersSelector2 ,
                                              AV41UnidadeOrganizacional_Nome2 ,
                                              AV42Estado_UF2 ,
                                              AV43UnidadeOrganizacional_Vinculada2 ,
                                              AV44DynamicFiltersEnabled3 ,
                                              AV45DynamicFiltersSelector3 ,
                                              AV46UnidadeOrganizacional_Nome3 ,
                                              AV47Estado_UF3 ,
                                              AV48UnidadeOrganizacional_Vinculada3 ,
                                              AV11TFUnidadeOrganizacional_Nome_Sel ,
                                              AV10TFUnidadeOrganizacional_Nome ,
                                              AV13TFEstado_UF_Sel ,
                                              AV12TFEstado_UF ,
                                              AV15TFUnidadeOrganizacional_Vinculada_Sel ,
                                              AV14TFUnidadeOrganizacional_Vinculada ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A23Estado_UF ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              A629UnidadeOrganizacional_Ativo ,
                                              AV49TpUo_Codigo ,
                                              A609TpUo_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV36UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV36UnidadeOrganizacional_Nome1), 50, "%");
         lV41UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV41UnidadeOrganizacional_Nome2), 50, "%");
         lV46UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV46UnidadeOrganizacional_Nome3), 50, "%");
         lV10TFUnidadeOrganizacional_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome), 50, "%");
         lV12TFEstado_UF = StringUtil.PadR( StringUtil.RTrim( AV12TFEstado_UF), 2, "%");
         lV14TFUnidadeOrganizacional_Vinculada = StringUtil.PadR( StringUtil.RTrim( AV14TFUnidadeOrganizacional_Vinculada), 50, "%");
         /* Using cursor P00NX4 */
         pr_default.execute(2, new Object[] {AV49TpUo_Codigo, lV36UnidadeOrganizacional_Nome1, AV37Estado_UF1, AV38UnidadeOrganizacional_Vinculada1, lV41UnidadeOrganizacional_Nome2, AV42Estado_UF2, AV43UnidadeOrganizacional_Vinculada2, lV46UnidadeOrganizacional_Nome3, AV47Estado_UF3, AV48UnidadeOrganizacional_Vinculada3, lV10TFUnidadeOrganizacional_Nome, AV11TFUnidadeOrganizacional_Nome_Sel, lV12TFEstado_UF, AV13TFEstado_UF_Sel, lV14TFUnidadeOrganizacional_Vinculada, AV15TFUnidadeOrganizacional_Vinculada_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKNX6 = false;
            A609TpUo_Codigo = P00NX4_A609TpUo_Codigo[0];
            A629UnidadeOrganizacional_Ativo = P00NX4_A629UnidadeOrganizacional_Ativo[0];
            A613UnidadeOrganizacional_Vinculada = P00NX4_A613UnidadeOrganizacional_Vinculada[0];
            n613UnidadeOrganizacional_Vinculada = P00NX4_n613UnidadeOrganizacional_Vinculada[0];
            A23Estado_UF = P00NX4_A23Estado_UF[0];
            A612UnidadeOrganizacional_Nome = P00NX4_A612UnidadeOrganizacional_Nome[0];
            A611UnidadeOrganizacional_Codigo = P00NX4_A611UnidadeOrganizacional_Codigo[0];
            AV28count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( P00NX4_A609TpUo_Codigo[0] == A609TpUo_Codigo ) && ( P00NX4_A613UnidadeOrganizacional_Vinculada[0] == A613UnidadeOrganizacional_Vinculada ) )
            {
               BRKNX6 = false;
               A611UnidadeOrganizacional_Codigo = P00NX4_A611UnidadeOrganizacional_Codigo[0];
               AV28count = (long)(AV28count+1);
               BRKNX6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A612UnidadeOrganizacional_Nome)) )
            {
               AV20Option = StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0);
               AV23OptionDesc = A612UnidadeOrganizacional_Nome;
               AV19InsertIndex = 1;
               while ( ( AV19InsertIndex <= AV21Options.Count ) && ( StringUtil.StrCmp(((String)AV24OptionsDesc.Item(AV19InsertIndex)), AV23OptionDesc) < 0 ) )
               {
                  AV19InsertIndex = (int)(AV19InsertIndex+1);
               }
               AV21Options.Add(AV20Option, AV19InsertIndex);
               AV24OptionsDesc.Add(AV23OptionDesc, AV19InsertIndex);
               AV26OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV28count), "Z,ZZZ,ZZZ,ZZ9")), AV19InsertIndex);
            }
            if ( AV21Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKNX6 )
            {
               BRKNX6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV21Options = new GxSimpleCollection();
         AV24OptionsDesc = new GxSimpleCollection();
         AV26OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Session = context.GetSession();
         AV31GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV32GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV34UnidadeOrganizacional_Ativo = true;
         AV10TFUnidadeOrganizacional_Nome = "";
         AV11TFUnidadeOrganizacional_Nome_Sel = "";
         AV12TFEstado_UF = "";
         AV13TFEstado_UF_Sel = "";
         AV14TFUnidadeOrganizacional_Vinculada = "";
         AV33GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV35DynamicFiltersSelector1 = "";
         AV36UnidadeOrganizacional_Nome1 = "";
         AV37Estado_UF1 = "";
         AV40DynamicFiltersSelector2 = "";
         AV41UnidadeOrganizacional_Nome2 = "";
         AV42Estado_UF2 = "";
         AV45DynamicFiltersSelector3 = "";
         AV46UnidadeOrganizacional_Nome3 = "";
         AV47Estado_UF3 = "";
         scmdbuf = "";
         lV36UnidadeOrganizacional_Nome1 = "";
         lV41UnidadeOrganizacional_Nome2 = "";
         lV46UnidadeOrganizacional_Nome3 = "";
         lV10TFUnidadeOrganizacional_Nome = "";
         lV12TFEstado_UF = "";
         lV14TFUnidadeOrganizacional_Vinculada = "";
         A612UnidadeOrganizacional_Nome = "";
         A23Estado_UF = "";
         P00NX2_A609TpUo_Codigo = new int[1] ;
         P00NX2_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P00NX2_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P00NX2_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P00NX2_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P00NX2_A23Estado_UF = new String[] {""} ;
         P00NX2_A611UnidadeOrganizacional_Codigo = new int[1] ;
         AV20Option = "";
         P00NX3_A609TpUo_Codigo = new int[1] ;
         P00NX3_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P00NX3_A23Estado_UF = new String[] {""} ;
         P00NX3_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P00NX3_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P00NX3_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P00NX3_A611UnidadeOrganizacional_Codigo = new int[1] ;
         P00NX4_A609TpUo_Codigo = new int[1] ;
         P00NX4_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         P00NX4_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         P00NX4_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         P00NX4_A23Estado_UF = new String[] {""} ;
         P00NX4_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         P00NX4_A611UnidadeOrganizacional_Codigo = new int[1] ;
         AV23OptionDesc = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00NX2_A609TpUo_Codigo, P00NX2_A629UnidadeOrganizacional_Ativo, P00NX2_A612UnidadeOrganizacional_Nome, P00NX2_A613UnidadeOrganizacional_Vinculada, P00NX2_n613UnidadeOrganizacional_Vinculada, P00NX2_A23Estado_UF, P00NX2_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               P00NX3_A609TpUo_Codigo, P00NX3_A629UnidadeOrganizacional_Ativo, P00NX3_A23Estado_UF, P00NX3_A613UnidadeOrganizacional_Vinculada, P00NX3_n613UnidadeOrganizacional_Vinculada, P00NX3_A612UnidadeOrganizacional_Nome, P00NX3_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               P00NX4_A609TpUo_Codigo, P00NX4_A629UnidadeOrganizacional_Ativo, P00NX4_A613UnidadeOrganizacional_Vinculada, P00NX4_n613UnidadeOrganizacional_Vinculada, P00NX4_A23Estado_UF, P00NX4_A612UnidadeOrganizacional_Nome, P00NX4_A611UnidadeOrganizacional_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV52GXV1 ;
      private int AV15TFUnidadeOrganizacional_Vinculada_Sel ;
      private int AV49TpUo_Codigo ;
      private int AV38UnidadeOrganizacional_Vinculada1 ;
      private int AV43UnidadeOrganizacional_Vinculada2 ;
      private int AV48UnidadeOrganizacional_Vinculada3 ;
      private int A613UnidadeOrganizacional_Vinculada ;
      private int A609TpUo_Codigo ;
      private int A611UnidadeOrganizacional_Codigo ;
      private int AV19InsertIndex ;
      private long AV28count ;
      private String AV10TFUnidadeOrganizacional_Nome ;
      private String AV11TFUnidadeOrganizacional_Nome_Sel ;
      private String AV12TFEstado_UF ;
      private String AV13TFEstado_UF_Sel ;
      private String AV14TFUnidadeOrganizacional_Vinculada ;
      private String AV36UnidadeOrganizacional_Nome1 ;
      private String AV37Estado_UF1 ;
      private String AV41UnidadeOrganizacional_Nome2 ;
      private String AV42Estado_UF2 ;
      private String AV46UnidadeOrganizacional_Nome3 ;
      private String AV47Estado_UF3 ;
      private String scmdbuf ;
      private String lV36UnidadeOrganizacional_Nome1 ;
      private String lV41UnidadeOrganizacional_Nome2 ;
      private String lV46UnidadeOrganizacional_Nome3 ;
      private String lV10TFUnidadeOrganizacional_Nome ;
      private String lV12TFEstado_UF ;
      private String lV14TFUnidadeOrganizacional_Vinculada ;
      private String A612UnidadeOrganizacional_Nome ;
      private String A23Estado_UF ;
      private bool returnInSub ;
      private bool AV34UnidadeOrganizacional_Ativo ;
      private bool AV39DynamicFiltersEnabled2 ;
      private bool AV44DynamicFiltersEnabled3 ;
      private bool A629UnidadeOrganizacional_Ativo ;
      private bool BRKNX2 ;
      private bool n613UnidadeOrganizacional_Vinculada ;
      private bool BRKNX4 ;
      private bool BRKNX6 ;
      private String AV27OptionIndexesJson ;
      private String AV22OptionsJson ;
      private String AV25OptionsDescJson ;
      private String AV18DDOName ;
      private String AV16SearchTxt ;
      private String AV17SearchTxtTo ;
      private String AV35DynamicFiltersSelector1 ;
      private String AV40DynamicFiltersSelector2 ;
      private String AV45DynamicFiltersSelector3 ;
      private String AV20Option ;
      private String AV23OptionDesc ;
      private IGxSession AV29Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00NX2_A609TpUo_Codigo ;
      private bool[] P00NX2_A629UnidadeOrganizacional_Ativo ;
      private String[] P00NX2_A612UnidadeOrganizacional_Nome ;
      private int[] P00NX2_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NX2_n613UnidadeOrganizacional_Vinculada ;
      private String[] P00NX2_A23Estado_UF ;
      private int[] P00NX2_A611UnidadeOrganizacional_Codigo ;
      private int[] P00NX3_A609TpUo_Codigo ;
      private bool[] P00NX3_A629UnidadeOrganizacional_Ativo ;
      private String[] P00NX3_A23Estado_UF ;
      private int[] P00NX3_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NX3_n613UnidadeOrganizacional_Vinculada ;
      private String[] P00NX3_A612UnidadeOrganizacional_Nome ;
      private int[] P00NX3_A611UnidadeOrganizacional_Codigo ;
      private int[] P00NX4_A609TpUo_Codigo ;
      private bool[] P00NX4_A629UnidadeOrganizacional_Ativo ;
      private int[] P00NX4_A613UnidadeOrganizacional_Vinculada ;
      private bool[] P00NX4_n613UnidadeOrganizacional_Vinculada ;
      private String[] P00NX4_A23Estado_UF ;
      private String[] P00NX4_A612UnidadeOrganizacional_Nome ;
      private int[] P00NX4_A611UnidadeOrganizacional_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV31GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV32GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV33GridStateDynamicFilter ;
   }

   public class getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00NX2( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             String AV36UnidadeOrganizacional_Nome1 ,
                                             String AV37Estado_UF1 ,
                                             int AV38UnidadeOrganizacional_Vinculada1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41UnidadeOrganizacional_Nome2 ,
                                             String AV42Estado_UF2 ,
                                             int AV43UnidadeOrganizacional_Vinculada2 ,
                                             bool AV44DynamicFiltersEnabled3 ,
                                             String AV45DynamicFiltersSelector3 ,
                                             String AV46UnidadeOrganizacional_Nome3 ,
                                             String AV47Estado_UF3 ,
                                             int AV48UnidadeOrganizacional_Vinculada3 ,
                                             String AV11TFUnidadeOrganizacional_Nome_Sel ,
                                             String AV10TFUnidadeOrganizacional_Nome ,
                                             String AV13TFEstado_UF_Sel ,
                                             String AV12TFEstado_UF ,
                                             int AV15TFUnidadeOrganizacional_Vinculada_Sel ,
                                             String AV14TFUnidadeOrganizacional_Vinculada ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             String A23Estado_UF ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             bool A629UnidadeOrganizacional_Ativo ,
                                             int AV49TpUo_Codigo ,
                                             int A609TpUo_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [16] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [TpUo_Codigo], [UnidadeOrganizacional_Ativo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Vinculada], [Estado_UF], [UnidadeOrganizacional_Codigo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([TpUo_Codigo] = @AV49TpUo_Codigo)";
         scmdbuf = scmdbuf + " and ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV36UnidadeOrganizacional_Nome1 + '%')";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Estado_UF1)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV37Estado_UF1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV38UnidadeOrganizacional_Vinculada1) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV38UnidadeOrganizacional_Vinculada1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV41UnidadeOrganizacional_Nome2 + '%')";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Estado_UF2)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV42Estado_UF2)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV43UnidadeOrganizacional_Vinculada2) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV43UnidadeOrganizacional_Vinculada2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV46UnidadeOrganizacional_Nome3 + '%')";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Estado_UF3)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV47Estado_UF3)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV48UnidadeOrganizacional_Vinculada3) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV48UnidadeOrganizacional_Vinculada3)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV10TFUnidadeOrganizacional_Nome)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] = @AV11TFUnidadeOrganizacional_Nome_Sel)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFEstado_UF_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFEstado_UF)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] like @lV12TFEstado_UF)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFEstado_UF_Sel)) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV13TFEstado_UF_Sel)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( (0==AV15TFUnidadeOrganizacional_Vinculada_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFUnidadeOrganizacional_Vinculada)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV14TFUnidadeOrganizacional_Vinculada)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV15TFUnidadeOrganizacional_Vinculada_Sel) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV15TFUnidadeOrganizacional_Vinculada_Sel)";
         }
         else
         {
            GXv_int1[15] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [TpUo_Codigo], [UnidadeOrganizacional_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00NX3( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             String AV36UnidadeOrganizacional_Nome1 ,
                                             String AV37Estado_UF1 ,
                                             int AV38UnidadeOrganizacional_Vinculada1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41UnidadeOrganizacional_Nome2 ,
                                             String AV42Estado_UF2 ,
                                             int AV43UnidadeOrganizacional_Vinculada2 ,
                                             bool AV44DynamicFiltersEnabled3 ,
                                             String AV45DynamicFiltersSelector3 ,
                                             String AV46UnidadeOrganizacional_Nome3 ,
                                             String AV47Estado_UF3 ,
                                             int AV48UnidadeOrganizacional_Vinculada3 ,
                                             String AV11TFUnidadeOrganizacional_Nome_Sel ,
                                             String AV10TFUnidadeOrganizacional_Nome ,
                                             String AV13TFEstado_UF_Sel ,
                                             String AV12TFEstado_UF ,
                                             int AV15TFUnidadeOrganizacional_Vinculada_Sel ,
                                             String AV14TFUnidadeOrganizacional_Vinculada ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             String A23Estado_UF ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             bool A629UnidadeOrganizacional_Ativo ,
                                             int AV49TpUo_Codigo ,
                                             int A609TpUo_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [16] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [TpUo_Codigo], [UnidadeOrganizacional_Ativo], [Estado_UF], [UnidadeOrganizacional_Vinculada], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Codigo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([TpUo_Codigo] = @AV49TpUo_Codigo)";
         scmdbuf = scmdbuf + " and ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV36UnidadeOrganizacional_Nome1 + '%')";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Estado_UF1)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV37Estado_UF1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV38UnidadeOrganizacional_Vinculada1) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV38UnidadeOrganizacional_Vinculada1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV41UnidadeOrganizacional_Nome2 + '%')";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Estado_UF2)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV42Estado_UF2)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV43UnidadeOrganizacional_Vinculada2) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV43UnidadeOrganizacional_Vinculada2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV46UnidadeOrganizacional_Nome3 + '%')";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Estado_UF3)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV47Estado_UF3)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV48UnidadeOrganizacional_Vinculada3) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV48UnidadeOrganizacional_Vinculada3)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV10TFUnidadeOrganizacional_Nome)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] = @AV11TFUnidadeOrganizacional_Nome_Sel)";
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFEstado_UF_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFEstado_UF)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] like @lV12TFEstado_UF)";
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFEstado_UF_Sel)) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV13TFEstado_UF_Sel)";
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( (0==AV15TFUnidadeOrganizacional_Vinculada_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFUnidadeOrganizacional_Vinculada)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV14TFUnidadeOrganizacional_Vinculada)";
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV15TFUnidadeOrganizacional_Vinculada_Sel) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV15TFUnidadeOrganizacional_Vinculada_Sel)";
         }
         else
         {
            GXv_int3[15] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [TpUo_Codigo], [Estado_UF]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00NX4( IGxContext context ,
                                             String AV35DynamicFiltersSelector1 ,
                                             String AV36UnidadeOrganizacional_Nome1 ,
                                             String AV37Estado_UF1 ,
                                             int AV38UnidadeOrganizacional_Vinculada1 ,
                                             bool AV39DynamicFiltersEnabled2 ,
                                             String AV40DynamicFiltersSelector2 ,
                                             String AV41UnidadeOrganizacional_Nome2 ,
                                             String AV42Estado_UF2 ,
                                             int AV43UnidadeOrganizacional_Vinculada2 ,
                                             bool AV44DynamicFiltersEnabled3 ,
                                             String AV45DynamicFiltersSelector3 ,
                                             String AV46UnidadeOrganizacional_Nome3 ,
                                             String AV47Estado_UF3 ,
                                             int AV48UnidadeOrganizacional_Vinculada3 ,
                                             String AV11TFUnidadeOrganizacional_Nome_Sel ,
                                             String AV10TFUnidadeOrganizacional_Nome ,
                                             String AV13TFEstado_UF_Sel ,
                                             String AV12TFEstado_UF ,
                                             int AV15TFUnidadeOrganizacional_Vinculada_Sel ,
                                             String AV14TFUnidadeOrganizacional_Vinculada ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             String A23Estado_UF ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             bool A629UnidadeOrganizacional_Ativo ,
                                             int AV49TpUo_Codigo ,
                                             int A609TpUo_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [16] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT [TpUo_Codigo], [UnidadeOrganizacional_Ativo], [UnidadeOrganizacional_Vinculada], [Estado_UF], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Codigo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([TpUo_Codigo] = @AV49TpUo_Codigo)";
         scmdbuf = scmdbuf + " and ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV36UnidadeOrganizacional_Nome1 + '%')";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37Estado_UF1)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV37Estado_UF1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV35DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV38UnidadeOrganizacional_Vinculada1) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV38UnidadeOrganizacional_Vinculada1)";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV41UnidadeOrganizacional_Nome2 + '%')";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42Estado_UF2)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV42Estado_UF2)";
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV39DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV40DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV43UnidadeOrganizacional_Vinculada2) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV43UnidadeOrganizacional_Vinculada2)";
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV46UnidadeOrganizacional_Nome3 + '%')";
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47Estado_UF3)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV47Estado_UF3)";
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV44DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV45DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV48UnidadeOrganizacional_Vinculada3) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV48UnidadeOrganizacional_Vinculada3)";
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUnidadeOrganizacional_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV10TFUnidadeOrganizacional_Nome)";
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeOrganizacional_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] = @AV11TFUnidadeOrganizacional_Nome_Sel)";
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFEstado_UF_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFEstado_UF)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] like @lV12TFEstado_UF)";
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFEstado_UF_Sel)) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV13TFEstado_UF_Sel)";
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( (0==AV15TFUnidadeOrganizacional_Vinculada_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFUnidadeOrganizacional_Vinculada)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV14TFUnidadeOrganizacional_Vinculada)";
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV15TFUnidadeOrganizacional_Vinculada_Sel) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV15TFUnidadeOrganizacional_Vinculada_Sel)";
         }
         else
         {
            GXv_int5[15] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [TpUo_Codigo], [UnidadeOrganizacional_Vinculada]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00NX2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (bool)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] );
               case 1 :
                     return conditional_P00NX3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (bool)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] );
               case 2 :
                     return conditional_P00NX4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (bool)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00NX2 ;
          prmP00NX2 = new Object[] {
          new Object[] {"@AV49TpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV36UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV37Estado_UF1",SqlDbType.Char,2,0} ,
          new Object[] {"@AV38UnidadeOrganizacional_Vinculada1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV42Estado_UF2",SqlDbType.Char,2,0} ,
          new Object[] {"@AV43UnidadeOrganizacional_Vinculada2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV46UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV47Estado_UF3",SqlDbType.Char,2,0} ,
          new Object[] {"@AV48UnidadeOrganizacional_Vinculada3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFUnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUnidadeOrganizacional_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFEstado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@AV13TFEstado_UF_Sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV14TFUnidadeOrganizacional_Vinculada",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFUnidadeOrganizacional_Vinculada_Sel",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00NX3 ;
          prmP00NX3 = new Object[] {
          new Object[] {"@AV49TpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV36UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV37Estado_UF1",SqlDbType.Char,2,0} ,
          new Object[] {"@AV38UnidadeOrganizacional_Vinculada1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV42Estado_UF2",SqlDbType.Char,2,0} ,
          new Object[] {"@AV43UnidadeOrganizacional_Vinculada2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV46UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV47Estado_UF3",SqlDbType.Char,2,0} ,
          new Object[] {"@AV48UnidadeOrganizacional_Vinculada3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFUnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUnidadeOrganizacional_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFEstado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@AV13TFEstado_UF_Sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV14TFUnidadeOrganizacional_Vinculada",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFUnidadeOrganizacional_Vinculada_Sel",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00NX4 ;
          prmP00NX4 = new Object[] {
          new Object[] {"@AV49TpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV36UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV37Estado_UF1",SqlDbType.Char,2,0} ,
          new Object[] {"@AV38UnidadeOrganizacional_Vinculada1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV42Estado_UF2",SqlDbType.Char,2,0} ,
          new Object[] {"@AV43UnidadeOrganizacional_Vinculada2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV46UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV47Estado_UF3",SqlDbType.Char,2,0} ,
          new Object[] {"@AV48UnidadeOrganizacional_Vinculada3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFUnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUnidadeOrganizacional_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFEstado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@AV13TFEstado_UF_Sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV14TFUnidadeOrganizacional_Vinculada",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFUnidadeOrganizacional_Vinculada_Sel",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00NX2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NX2,100,0,true,false )
             ,new CursorDef("P00NX3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NX3,100,0,true,false )
             ,new CursorDef("P00NX4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00NX4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 2) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 2) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata") )
          {
             return  ;
          }
          getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata worker = new getgeral_tp_uogeral_unidadeorganizacionalwcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
