/*
               File: PRC_TirarDaGestaoDeContratos
        Description: Tirar Da Gest�o De Contratos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:8:51.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_tirardagestaodecontratos : GXProcedure
   {
      public prc_tirardagestaodecontratos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_tirardagestaodecontratos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoGestor_UsuarioCod )
      {
         this.A1079ContratoGestor_UsuarioCod = aP0_ContratoGestor_UsuarioCod;
         initialize();
         executePrivate();
         aP0_ContratoGestor_UsuarioCod=this.A1079ContratoGestor_UsuarioCod;
      }

      public int executeUdp( )
      {
         this.A1079ContratoGestor_UsuarioCod = aP0_ContratoGestor_UsuarioCod;
         initialize();
         executePrivate();
         aP0_ContratoGestor_UsuarioCod=this.A1079ContratoGestor_UsuarioCod;
         return A1079ContratoGestor_UsuarioCod ;
      }

      public void executeSubmit( ref int aP0_ContratoGestor_UsuarioCod )
      {
         prc_tirardagestaodecontratos objprc_tirardagestaodecontratos;
         objprc_tirardagestaodecontratos = new prc_tirardagestaodecontratos();
         objprc_tirardagestaodecontratos.A1079ContratoGestor_UsuarioCod = aP0_ContratoGestor_UsuarioCod;
         objprc_tirardagestaodecontratos.context.SetSubmitInitialConfig(context);
         objprc_tirardagestaodecontratos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_tirardagestaodecontratos);
         aP0_ContratoGestor_UsuarioCod=this.A1079ContratoGestor_UsuarioCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_tirardagestaodecontratos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized DELETE. */
         /* Using cursor P00882 */
         pr_default.execute(0, new Object[] {A1079ContratoGestor_UsuarioCod});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("ContratoGestor") ;
         /* End optimized DELETE. */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_TirarDaGestaoDeContratos");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_tirardagestaodecontratos__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1079ContratoGestor_UsuarioCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoGestor_UsuarioCod ;
      private IDataStoreProvider pr_default ;
   }

   public class prc_tirardagestaodecontratos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00882 ;
          prmP00882 = new Object[] {
          new Object[] {"@ContratoGestor_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00882", "DELETE FROM [ContratoGestor]  WHERE [ContratoGestor_UsuarioCod] = @ContratoGestor_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00882)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
