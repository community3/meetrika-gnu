/*
               File: WebPanel4
        Description: Web Panel4
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:24:43.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class webpanel4 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public webpanel4( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public webpanel4( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContagemresultado_servico1 = new GXCombobox();
         dynavContagemresultado_cntsrvprrcod1 = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_SERVICO1") == 0 )
            {
               AV8AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8AreaTrabalho_Codigo), 6, 0)));
               AV9Contratada_DoUsuario = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratada_DoUsuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratada_DoUsuario), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDSVvCONTAGEMRESULTADO_SERVICO1SB2( AV8AreaTrabalho_Codigo, AV9Contratada_DoUsuario) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_CNTSRVPRRCOD1") == 0 )
            {
               AV6ContagemResultado_Servico1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_Servico1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Servico1), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_CNTSRVPRRCOD1SB2( AV6ContagemResultado_Servico1) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PASB2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTSB2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031221244331");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("webpanel4.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vAREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8AreaTrabalho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_DOUSUARIO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Contratada_DoUsuario), 6, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WESB2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTSB2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("webpanel4.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WebPanel4" ;
      }

      public override String GetPgmdesc( )
      {
         return "Web Panel4" ;
      }

      protected void WBSB0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 2,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_servico1, dynavContagemresultado_servico1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_Servico1), 6, 0)), 1, dynavContagemresultado_servico1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,2);\"", "", true, "HLP_WebPanel4.htm");
            dynavContagemresultado_servico1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_Servico1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_servico1_Internalname, "Values", (String)(dynavContagemresultado_servico1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 3,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_cntsrvprrcod1, dynavContagemresultado_cntsrvprrcod1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_CntSrvPrrCod1), 6, 0)), 1, dynavContagemresultado_cntsrvprrcod1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,3);\"", "", true, "HLP_WebPanel4.htm");
            dynavContagemresultado_cntsrvprrcod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_CntSrvPrrCod1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_cntsrvprrcod1_Internalname, "Values", (String)(dynavContagemresultado_cntsrvprrcod1.ToJavascriptSource()));
         }
         wbLoad = true;
      }

      protected void STARTSB2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Web Panel4", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPSB0( ) ;
      }

      protected void WSSB2( )
      {
         STARTSB2( ) ;
         EVTSB2( ) ;
      }

      protected void EVTSB2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11SB2 */
                              E11SB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12SB2 */
                              E12SB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WESB2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PASB2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContagemresultado_servico1.Name = "vCONTAGEMRESULTADO_SERVICO1";
            dynavContagemresultado_servico1.WebTags = "";
            dynavContagemresultado_cntsrvprrcod1.Name = "vCONTAGEMRESULTADO_CNTSRVPRRCOD1";
            dynavContagemresultado_cntsrvprrcod1.WebTags = "";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContagemresultado_servico1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTAGEMRESULTADO_CNTSRVPRRCOD1_htmlSB2( AV6ContagemResultado_Servico1) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTSRVPRRCOD1SB2( int AV6ContagemResultado_Servico1 )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_CNTSRVPRRCOD1_dataSB2( AV6ContagemResultado_Servico1) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_CNTSRVPRRCOD1_htmlSB2( int AV6ContagemResultado_Servico1 )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_CNTSRVPRRCOD1_dataSB2( AV6ContagemResultado_Servico1) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_cntsrvprrcod1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_cntsrvprrcod1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_cntsrvprrcod1.ItemCount > 0 )
         {
            AV5ContagemResultado_CntSrvPrrCod1 = (int)(NumberUtil.Val( dynavContagemresultado_cntsrvprrcod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_CntSrvPrrCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_CntSrvPrrCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_CntSrvPrrCod1), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_CNTSRVPRRCOD1_dataSB2( int AV6ContagemResultado_Servico1 )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Normal");
         /* Using cursor H00SB2 */
         pr_default.execute(0, new Object[] {AV6ContagemResultado_Servico1});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00SB2_A1336ContratoServicosPrioridade_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00SB2_A1337ContratoServicosPrioridade_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDSVvCONTAGEMRESULTADO_SERVICO1SB2( int AV8AreaTrabalho_Codigo ,
                                                          int AV9Contratada_DoUsuario )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDSVvCONTAGEMRESULTADO_SERVICO1_dataSB2( AV8AreaTrabalho_Codigo, AV9Contratada_DoUsuario) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_SERVICO1_htmlSB2( int AV8AreaTrabalho_Codigo ,
                                                             int AV9Contratada_DoUsuario )
      {
         int gxdynajaxvalue ;
         GXDSVvCONTAGEMRESULTADO_SERVICO1_dataSB2( AV8AreaTrabalho_Codigo, AV9Contratada_DoUsuario) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_servico1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_servico1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_servico1.ItemCount > 0 )
         {
            AV6ContagemResultado_Servico1 = (int)(NumberUtil.Val( dynavContagemresultado_servico1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_Servico1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_Servico1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Servico1), 6, 0)));
         }
      }

      protected void GXDSVvCONTAGEMRESULTADO_SERVICO1_dataSB2( int AV8AreaTrabalho_Codigo ,
                                                               int AV9Contratada_DoUsuario )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Todos");
         IGxCollection gxcolvCONTAGEMRESULTADO_SERVICO1 ;
         SdtSDT_Codigos gxcolitemvCONTAGEMRESULTADO_SERVICO1 ;
         new dp_servicoscontratados(context ).execute(  AV8AreaTrabalho_Codigo,  AV9Contratada_DoUsuario, out  gxcolvCONTAGEMRESULTADO_SERVICO1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8AreaTrabalho_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratada_DoUsuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratada_DoUsuario), 6, 0)));
         int gxindex = 1 ;
         while ( gxindex <= gxcolvCONTAGEMRESULTADO_SERVICO1.Count )
         {
            gxcolitemvCONTAGEMRESULTADO_SERVICO1 = ((SdtSDT_Codigos)gxcolvCONTAGEMRESULTADO_SERVICO1.Item(gxindex));
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.Str( (decimal)(gxcolitemvCONTAGEMRESULTADO_SERVICO1.gxTpr_Codigo), 6, 0)));
            gxdynajaxctrldescr.Add(gxcolitemvCONTAGEMRESULTADO_SERVICO1.gxTpr_Descricao);
            gxindex = (int)(gxindex+1);
         }
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContagemresultado_servico1.ItemCount > 0 )
         {
            AV6ContagemResultado_Servico1 = (int)(NumberUtil.Val( dynavContagemresultado_servico1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6ContagemResultado_Servico1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_Servico1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Servico1), 6, 0)));
         }
         if ( dynavContagemresultado_cntsrvprrcod1.ItemCount > 0 )
         {
            AV5ContagemResultado_CntSrvPrrCod1 = (int)(NumberUtil.Val( dynavContagemresultado_cntsrvprrcod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_CntSrvPrrCod1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_CntSrvPrrCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_CntSrvPrrCod1), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSB2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFSB2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E12SB2 */
            E12SB2 ();
            WBSB0( ) ;
         }
      }

      protected void STRUPSB0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11SB2 */
         E11SB2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTAGEMRESULTADO_SERVICO1_htmlSB2( AV8AreaTrabalho_Codigo, AV9Contratada_DoUsuario) ;
         GXVvCONTAGEMRESULTADO_CNTSRVPRRCOD1_htmlSB2( AV6ContagemResultado_Servico1) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavContagemresultado_servico1.CurrentValue = cgiGet( dynavContagemresultado_servico1_Internalname);
            AV6ContagemResultado_Servico1 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_servico1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_Servico1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Servico1), 6, 0)));
            dynavContagemresultado_cntsrvprrcod1.CurrentValue = cgiGet( dynavContagemresultado_cntsrvprrcod1_Internalname);
            AV5ContagemResultado_CntSrvPrrCod1 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_cntsrvprrcod1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_CntSrvPrrCod1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_CntSrvPrrCod1), 6, 0)));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvCONTAGEMRESULTADO_SERVICO1_htmlSB2( AV8AreaTrabalho_Codigo, AV9Contratada_DoUsuario) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11SB2 */
         E11SB2 ();
         if (returnInSub) return;
      }

      protected void E11SB2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
         AV8AreaTrabalho_Codigo = AV7WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8AreaTrabalho_Codigo), 6, 0)));
         AV9Contratada_DoUsuario = AV7WWPContext.gxTpr_Contratada_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratada_DoUsuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratada_DoUsuario), 6, 0)));
         GXt_objcol_SdtSDT_Codigos1 = AV11SDT_COdigos;
         new dp_wp_dashboard_contrato(context ).execute(  AV7WWPContext.gxTpr_Areatrabalho_codigo,  AV7WWPContext.gxTpr_Userid, out  GXt_objcol_SdtSDT_Codigos1) ;
         AV11SDT_COdigos = GXt_objcol_SdtSDT_Codigos1;
         GX_msglist.addItem(AV11SDT_COdigos.ToXml(false, true, "SDT_CodigosCollection", "GxEv3Up14_Meetrika"));
      }

      protected void nextLoad( )
      {
      }

      protected void E12SB2( )
      {
         /* Load Routine */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASB2( ) ;
         WSSB2( ) ;
         WESB2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031221244341");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("webpanel4.js", "?202031221244341");
         }
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         dynavContagemresultado_servico1_Internalname = "vCONTAGEMRESULTADO_SERVICO1";
         dynavContagemresultado_cntsrvprrcod1_Internalname = "vCONTAGEMRESULTADO_CNTSRVPRRCOD1";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         dynavContagemresultado_cntsrvprrcod1_Jsonclick = "";
         dynavContagemresultado_servico1_Jsonclick = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Web Panel4";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Contagemresultado_servico1( GXCombobox dynGX_Parm1 ,
                                                     GXCombobox dynGX_Parm2 )
      {
         dynavContagemresultado_servico1 = dynGX_Parm1;
         AV6ContagemResultado_Servico1 = (int)(NumberUtil.Val( dynavContagemresultado_servico1.CurrentValue, "."));
         dynavContagemresultado_cntsrvprrcod1 = dynGX_Parm2;
         AV5ContagemResultado_CntSrvPrrCod1 = (int)(NumberUtil.Val( dynavContagemresultado_cntsrvprrcod1.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_CNTSRVPRRCOD1_htmlSB2( AV6ContagemResultado_Servico1) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_cntsrvprrcod1.ItemCount > 0 )
         {
            AV5ContagemResultado_CntSrvPrrCod1 = (int)(NumberUtil.Val( dynavContagemresultado_cntsrvprrcod1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_CntSrvPrrCod1), 6, 0))), "."));
         }
         dynavContagemresultado_cntsrvprrcod1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_CntSrvPrrCod1), 6, 0));
         isValidOutput.Add(dynavContagemresultado_cntsrvprrcod1);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00SB2_A1335ContratoServicosPrioridade_CntSrvCod = new int[1] ;
         H00SB2_A1336ContratoServicosPrioridade_Codigo = new int[1] ;
         H00SB2_A1337ContratoServicosPrioridade_Nome = new String[] {""} ;
         H00SB2_A155Servico_Codigo = new int[1] ;
         H00SB2_n155Servico_Codigo = new bool[] {false} ;
         AV7WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV11SDT_COdigos = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_Meetrika", "SdtSDT_Codigos", "GeneXus.Programs");
         GXt_objcol_SdtSDT_Codigos1 = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_Meetrika", "SdtSDT_Codigos", "GeneXus.Programs");
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.webpanel4__default(),
            new Object[][] {
                new Object[] {
               H00SB2_A1335ContratoServicosPrioridade_CntSrvCod, H00SB2_A1336ContratoServicosPrioridade_Codigo, H00SB2_A1337ContratoServicosPrioridade_Nome, H00SB2_A155Servico_Codigo, H00SB2_n155Servico_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short wbTemp ;
      private int AV8AreaTrabalho_Codigo ;
      private int AV9Contratada_DoUsuario ;
      private int AV6ContagemResultado_Servico1 ;
      private int AV5ContagemResultado_CntSrvPrrCod1 ;
      private int gxdynajaxindex ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String dynavContagemresultado_servico1_Internalname ;
      private String dynavContagemresultado_servico1_Jsonclick ;
      private String dynavContagemresultado_cntsrvprrcod1_Internalname ;
      private String dynavContagemresultado_cntsrvprrcod1_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContagemresultado_servico1 ;
      private GXCombobox dynavContagemresultado_cntsrvprrcod1 ;
      private IDataStoreProvider pr_default ;
      private int[] H00SB2_A1335ContratoServicosPrioridade_CntSrvCod ;
      private int[] H00SB2_A1336ContratoServicosPrioridade_Codigo ;
      private String[] H00SB2_A1337ContratoServicosPrioridade_Nome ;
      private int[] H00SB2_A155Servico_Codigo ;
      private bool[] H00SB2_n155Servico_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV11SDT_COdigos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection GXt_objcol_SdtSDT_Codigos1 ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV7WWPContext ;
   }

   public class webpanel4__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00SB2 ;
          prmH00SB2 = new Object[] {
          new Object[] {"@AV6ContagemResultado_Servico1",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00SB2", "SELECT T2.[ContratoServicos_Codigo] AS ContratoServicosPrioridade_CntSrvCod, T1.[ContratoServicosPrioridade_Codigo], T1.[ContratoServicosPrioridade_Nome], T2.[Servico_Codigo] FROM ([ContratoServicosPrioridade] T1 WITH (NOLOCK) INNER JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContratoServicosPrioridade_CntSrvCod]) WHERE T2.[Servico_Codigo] = @AV6ContagemResultado_Servico1 ORDER BY T1.[ContratoServicosPrioridade_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SB2,0,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
