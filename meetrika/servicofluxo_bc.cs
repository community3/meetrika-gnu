/*
               File: ServicoFluxo_BC
        Description: Processo do Servi�o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:59.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicofluxo_bc : GXHttpHandler, IGxSilentTrn
   {
      public servicofluxo_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servicofluxo_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow3V176( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey3V176( ) ;
         standaloneModal( ) ;
         AddRow3V176( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E113V2 */
            E113V2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1528ServicoFluxo_Codigo = A1528ServicoFluxo_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_3V0( )
      {
         BeforeValidate3V176( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3V176( ) ;
            }
            else
            {
               CheckExtendedTable3V176( ) ;
               if ( AnyError == 0 )
               {
                  ZM3V176( 4) ;
                  ZM3V176( 5) ;
               }
               CloseExtendedTableCursors3V176( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E123V2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV37Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV38GXV1 = 1;
            while ( AV38GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV14TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV38GXV1));
               if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "ServicoFluxo_ServicoCod") == 0 )
               {
                  AV11Insert_ServicoFluxo_ServicoCod = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               else if ( StringUtil.StrCmp(AV14TrnContextAtt.gxTpr_Attributename, "ServicoFluxo_ServicoPos") == 0 )
               {
                  AV13Insert_ServicoFluxo_ServicoPos = (int)(NumberUtil.Val( AV14TrnContextAtt.gxTpr_Attributevalue, "."));
               }
               AV38GXV1 = (int)(AV38GXV1+1);
            }
         }
      }

      protected void E113V2( )
      {
         /* After Trn Routine */
         if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            new prc_reordenafluxo(context ).execute(  "S",  A1522ServicoFluxo_ServicoCod,  AV15ServicoFluxo_Ordem,  9999,  "+") ;
         }
         else if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            if ( AV17RenumerarDesde > AV24OrdemAnterior )
            {
               new prc_reordenafluxo(context ).execute(  "S",  A1522ServicoFluxo_ServicoCod,  AV24OrdemAnterior,  AV17RenumerarDesde,  "+") ;
            }
            else if ( AV17RenumerarDesde < AV24OrdemAnterior )
            {
               new prc_reordenafluxo(context ).execute(  "S",  A1522ServicoFluxo_ServicoCod,  AV17RenumerarDesde,  AV24OrdemAnterior,  "-") ;
            }
         }
         if ( false )
         {
         }
      }

      protected void E133V2( )
      {
         /* ServicoFluxo_Ordem_Isvalid Routine */
         AV17RenumerarDesde = A1532ServicoFluxo_Ordem;
      }

      protected void ZM3V176( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z1532ServicoFluxo_Ordem = A1532ServicoFluxo_Ordem;
            Z1522ServicoFluxo_ServicoCod = A1522ServicoFluxo_ServicoCod;
            Z1526ServicoFluxo_ServicoPos = A1526ServicoFluxo_ServicoPos;
            Z155Servico_Codigo = A155Servico_Codigo;
         }
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z1523ServicoFluxo_ServicoSigla = A1523ServicoFluxo_ServicoSigla;
            Z1533ServicoFluxo_ServicoTpHrq = A1533ServicoFluxo_ServicoTpHrq;
            Z155Servico_Codigo = A155Servico_Codigo;
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z1558ServicoFluxo_SrvPosNome = A1558ServicoFluxo_SrvPosNome;
            Z1527ServicoFluxo_SrvPosSigla = A1527ServicoFluxo_SrvPosSigla;
            Z1554ServicoFluxo_SrvPosPrcTmp = A1554ServicoFluxo_SrvPosPrcTmp;
            Z1555ServicoFluxo_SrvPosPrcPgm = A1555ServicoFluxo_SrvPosPrcPgm;
            Z1556ServicoFluxo_SrvPosPrcCnc = A1556ServicoFluxo_SrvPosPrcCnc;
            Z155Servico_Codigo = A155Servico_Codigo;
         }
         if ( GX_JID == -3 )
         {
            Z1528ServicoFluxo_Codigo = A1528ServicoFluxo_Codigo;
            Z1532ServicoFluxo_Ordem = A1532ServicoFluxo_Ordem;
            Z1522ServicoFluxo_ServicoCod = A1522ServicoFluxo_ServicoCod;
            Z1526ServicoFluxo_ServicoPos = A1526ServicoFluxo_ServicoPos;
            Z1523ServicoFluxo_ServicoSigla = A1523ServicoFluxo_ServicoSigla;
            Z1533ServicoFluxo_ServicoTpHrq = A1533ServicoFluxo_ServicoTpHrq;
            Z1558ServicoFluxo_SrvPosNome = A1558ServicoFluxo_SrvPosNome;
            Z1527ServicoFluxo_SrvPosSigla = A1527ServicoFluxo_SrvPosSigla;
            Z1554ServicoFluxo_SrvPosPrcTmp = A1554ServicoFluxo_SrvPosPrcTmp;
            Z1555ServicoFluxo_SrvPosPrcPgm = A1555ServicoFluxo_SrvPosPrcPgm;
            Z1556ServicoFluxo_SrvPosPrcCnc = A1556ServicoFluxo_SrvPosPrcCnc;
         }
      }

      protected void standaloneNotModal( )
      {
         AV37Pgmname = "ServicoFluxo_BC";
      }

      protected void standaloneModal( )
      {
      }

      protected void Load3V176( )
      {
         /* Using cursor BC003V6 */
         pr_default.execute(4, new Object[] {A1528ServicoFluxo_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound176 = 1;
            A1523ServicoFluxo_ServicoSigla = BC003V6_A1523ServicoFluxo_ServicoSigla[0];
            n1523ServicoFluxo_ServicoSigla = BC003V6_n1523ServicoFluxo_ServicoSigla[0];
            A1533ServicoFluxo_ServicoTpHrq = BC003V6_A1533ServicoFluxo_ServicoTpHrq[0];
            n1533ServicoFluxo_ServicoTpHrq = BC003V6_n1533ServicoFluxo_ServicoTpHrq[0];
            A1532ServicoFluxo_Ordem = BC003V6_A1532ServicoFluxo_Ordem[0];
            n1532ServicoFluxo_Ordem = BC003V6_n1532ServicoFluxo_Ordem[0];
            A1558ServicoFluxo_SrvPosNome = BC003V6_A1558ServicoFluxo_SrvPosNome[0];
            n1558ServicoFluxo_SrvPosNome = BC003V6_n1558ServicoFluxo_SrvPosNome[0];
            A1527ServicoFluxo_SrvPosSigla = BC003V6_A1527ServicoFluxo_SrvPosSigla[0];
            n1527ServicoFluxo_SrvPosSigla = BC003V6_n1527ServicoFluxo_SrvPosSigla[0];
            A1554ServicoFluxo_SrvPosPrcTmp = BC003V6_A1554ServicoFluxo_SrvPosPrcTmp[0];
            n1554ServicoFluxo_SrvPosPrcTmp = BC003V6_n1554ServicoFluxo_SrvPosPrcTmp[0];
            A1555ServicoFluxo_SrvPosPrcPgm = BC003V6_A1555ServicoFluxo_SrvPosPrcPgm[0];
            n1555ServicoFluxo_SrvPosPrcPgm = BC003V6_n1555ServicoFluxo_SrvPosPrcPgm[0];
            A1556ServicoFluxo_SrvPosPrcCnc = BC003V6_A1556ServicoFluxo_SrvPosPrcCnc[0];
            n1556ServicoFluxo_SrvPosPrcCnc = BC003V6_n1556ServicoFluxo_SrvPosPrcCnc[0];
            A1522ServicoFluxo_ServicoCod = BC003V6_A1522ServicoFluxo_ServicoCod[0];
            A1526ServicoFluxo_ServicoPos = BC003V6_A1526ServicoFluxo_ServicoPos[0];
            n1526ServicoFluxo_ServicoPos = BC003V6_n1526ServicoFluxo_ServicoPos[0];
            ZM3V176( -3) ;
         }
         pr_default.close(4);
         OnLoadActions3V176( ) ;
      }

      protected void OnLoadActions3V176( )
      {
      }

      protected void CheckExtendedTable3V176( )
      {
         standaloneModal( ) ;
         /* Using cursor BC003V4 */
         pr_default.execute(2, new Object[] {A1522ServicoFluxo_ServicoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico_Servico Fluxo'.", "ForeignKeyNotFound", 1, "SERVICOFLUXO_SERVICOCOD");
            AnyError = 1;
         }
         A1523ServicoFluxo_ServicoSigla = BC003V4_A1523ServicoFluxo_ServicoSigla[0];
         n1523ServicoFluxo_ServicoSigla = BC003V4_n1523ServicoFluxo_ServicoSigla[0];
         A1533ServicoFluxo_ServicoTpHrq = BC003V4_A1533ServicoFluxo_ServicoTpHrq[0];
         n1533ServicoFluxo_ServicoTpHrq = BC003V4_n1533ServicoFluxo_ServicoTpHrq[0];
         pr_default.close(2);
         /* Using cursor BC003V5 */
         pr_default.execute(3, new Object[] {n1526ServicoFluxo_ServicoPos, A1526ServicoFluxo_ServicoPos});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A1526ServicoFluxo_ServicoPos) ) )
            {
               GX_msglist.addItem("N�o existe 'Servico Fluxo_Servico Posterior'.", "ForeignKeyNotFound", 1, "SERVICOFLUXO_SERVICOPOS");
               AnyError = 1;
            }
         }
         A1558ServicoFluxo_SrvPosNome = BC003V5_A1558ServicoFluxo_SrvPosNome[0];
         n1558ServicoFluxo_SrvPosNome = BC003V5_n1558ServicoFluxo_SrvPosNome[0];
         A1527ServicoFluxo_SrvPosSigla = BC003V5_A1527ServicoFluxo_SrvPosSigla[0];
         n1527ServicoFluxo_SrvPosSigla = BC003V5_n1527ServicoFluxo_SrvPosSigla[0];
         A1554ServicoFluxo_SrvPosPrcTmp = BC003V5_A1554ServicoFluxo_SrvPosPrcTmp[0];
         n1554ServicoFluxo_SrvPosPrcTmp = BC003V5_n1554ServicoFluxo_SrvPosPrcTmp[0];
         A1555ServicoFluxo_SrvPosPrcPgm = BC003V5_A1555ServicoFluxo_SrvPosPrcPgm[0];
         n1555ServicoFluxo_SrvPosPrcPgm = BC003V5_n1555ServicoFluxo_SrvPosPrcPgm[0];
         A1556ServicoFluxo_SrvPosPrcCnc = BC003V5_A1556ServicoFluxo_SrvPosPrcCnc[0];
         n1556ServicoFluxo_SrvPosPrcCnc = BC003V5_n1556ServicoFluxo_SrvPosPrcCnc[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors3V176( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey3V176( )
      {
         /* Using cursor BC003V7 */
         pr_default.execute(5, new Object[] {A1528ServicoFluxo_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound176 = 1;
         }
         else
         {
            RcdFound176 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC003V3 */
         pr_default.execute(1, new Object[] {A1528ServicoFluxo_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3V176( 3) ;
            RcdFound176 = 1;
            A1528ServicoFluxo_Codigo = BC003V3_A1528ServicoFluxo_Codigo[0];
            A1532ServicoFluxo_Ordem = BC003V3_A1532ServicoFluxo_Ordem[0];
            n1532ServicoFluxo_Ordem = BC003V3_n1532ServicoFluxo_Ordem[0];
            A1522ServicoFluxo_ServicoCod = BC003V3_A1522ServicoFluxo_ServicoCod[0];
            A1526ServicoFluxo_ServicoPos = BC003V3_A1526ServicoFluxo_ServicoPos[0];
            n1526ServicoFluxo_ServicoPos = BC003V3_n1526ServicoFluxo_ServicoPos[0];
            Z1528ServicoFluxo_Codigo = A1528ServicoFluxo_Codigo;
            sMode176 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load3V176( ) ;
            if ( AnyError == 1 )
            {
               RcdFound176 = 0;
               InitializeNonKey3V176( ) ;
            }
            Gx_mode = sMode176;
         }
         else
         {
            RcdFound176 = 0;
            InitializeNonKey3V176( ) ;
            sMode176 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode176;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3V176( ) ;
         if ( RcdFound176 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_3V0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency3V176( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC003V2 */
            pr_default.execute(0, new Object[] {A1528ServicoFluxo_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoFluxo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1532ServicoFluxo_Ordem != BC003V2_A1532ServicoFluxo_Ordem[0] ) || ( Z1522ServicoFluxo_ServicoCod != BC003V2_A1522ServicoFluxo_ServicoCod[0] ) || ( Z1526ServicoFluxo_ServicoPos != BC003V2_A1526ServicoFluxo_ServicoPos[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ServicoFluxo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3V176( )
      {
         BeforeValidate3V176( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3V176( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3V176( 0) ;
            CheckOptimisticConcurrency3V176( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3V176( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3V176( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003V8 */
                     pr_default.execute(6, new Object[] {n1532ServicoFluxo_Ordem, A1532ServicoFluxo_Ordem, A1522ServicoFluxo_ServicoCod, n1526ServicoFluxo_ServicoPos, A1526ServicoFluxo_ServicoPos});
                     A1528ServicoFluxo_Codigo = BC003V8_A1528ServicoFluxo_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoFluxo") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3V176( ) ;
            }
            EndLevel3V176( ) ;
         }
         CloseExtendedTableCursors3V176( ) ;
      }

      protected void Update3V176( )
      {
         BeforeValidate3V176( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3V176( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3V176( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3V176( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3V176( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC003V9 */
                     pr_default.execute(7, new Object[] {n1532ServicoFluxo_Ordem, A1532ServicoFluxo_Ordem, A1522ServicoFluxo_ServicoCod, n1526ServicoFluxo_ServicoPos, A1526ServicoFluxo_ServicoPos, A1528ServicoFluxo_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoFluxo") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoFluxo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3V176( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3V176( ) ;
         }
         CloseExtendedTableCursors3V176( ) ;
      }

      protected void DeferredUpdate3V176( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate3V176( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3V176( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3V176( ) ;
            AfterConfirm3V176( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3V176( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC003V10 */
                  pr_default.execute(8, new Object[] {A1528ServicoFluxo_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("ServicoFluxo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode176 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel3V176( ) ;
         Gx_mode = sMode176;
      }

      protected void OnDeleteControls3V176( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC003V11 */
            pr_default.execute(9, new Object[] {A1522ServicoFluxo_ServicoCod});
            A1523ServicoFluxo_ServicoSigla = BC003V11_A1523ServicoFluxo_ServicoSigla[0];
            n1523ServicoFluxo_ServicoSigla = BC003V11_n1523ServicoFluxo_ServicoSigla[0];
            A1533ServicoFluxo_ServicoTpHrq = BC003V11_A1533ServicoFluxo_ServicoTpHrq[0];
            n1533ServicoFluxo_ServicoTpHrq = BC003V11_n1533ServicoFluxo_ServicoTpHrq[0];
            pr_default.close(9);
            /* Using cursor BC003V12 */
            pr_default.execute(10, new Object[] {n1526ServicoFluxo_ServicoPos, A1526ServicoFluxo_ServicoPos});
            A1558ServicoFluxo_SrvPosNome = BC003V12_A1558ServicoFluxo_SrvPosNome[0];
            n1558ServicoFluxo_SrvPosNome = BC003V12_n1558ServicoFluxo_SrvPosNome[0];
            A1527ServicoFluxo_SrvPosSigla = BC003V12_A1527ServicoFluxo_SrvPosSigla[0];
            n1527ServicoFluxo_SrvPosSigla = BC003V12_n1527ServicoFluxo_SrvPosSigla[0];
            A1554ServicoFluxo_SrvPosPrcTmp = BC003V12_A1554ServicoFluxo_SrvPosPrcTmp[0];
            n1554ServicoFluxo_SrvPosPrcTmp = BC003V12_n1554ServicoFluxo_SrvPosPrcTmp[0];
            A1555ServicoFluxo_SrvPosPrcPgm = BC003V12_A1555ServicoFluxo_SrvPosPrcPgm[0];
            n1555ServicoFluxo_SrvPosPrcPgm = BC003V12_n1555ServicoFluxo_SrvPosPrcPgm[0];
            A1556ServicoFluxo_SrvPosPrcCnc = BC003V12_A1556ServicoFluxo_SrvPosPrcCnc[0];
            n1556ServicoFluxo_SrvPosPrcCnc = BC003V12_n1556ServicoFluxo_SrvPosPrcCnc[0];
            pr_default.close(10);
         }
      }

      protected void EndLevel3V176( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3V176( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart3V176( )
      {
         /* Scan By routine */
         /* Using cursor BC003V13 */
         pr_default.execute(11, new Object[] {A1528ServicoFluxo_Codigo});
         RcdFound176 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound176 = 1;
            A1528ServicoFluxo_Codigo = BC003V13_A1528ServicoFluxo_Codigo[0];
            A1523ServicoFluxo_ServicoSigla = BC003V13_A1523ServicoFluxo_ServicoSigla[0];
            n1523ServicoFluxo_ServicoSigla = BC003V13_n1523ServicoFluxo_ServicoSigla[0];
            A1533ServicoFluxo_ServicoTpHrq = BC003V13_A1533ServicoFluxo_ServicoTpHrq[0];
            n1533ServicoFluxo_ServicoTpHrq = BC003V13_n1533ServicoFluxo_ServicoTpHrq[0];
            A1532ServicoFluxo_Ordem = BC003V13_A1532ServicoFluxo_Ordem[0];
            n1532ServicoFluxo_Ordem = BC003V13_n1532ServicoFluxo_Ordem[0];
            A1558ServicoFluxo_SrvPosNome = BC003V13_A1558ServicoFluxo_SrvPosNome[0];
            n1558ServicoFluxo_SrvPosNome = BC003V13_n1558ServicoFluxo_SrvPosNome[0];
            A1527ServicoFluxo_SrvPosSigla = BC003V13_A1527ServicoFluxo_SrvPosSigla[0];
            n1527ServicoFluxo_SrvPosSigla = BC003V13_n1527ServicoFluxo_SrvPosSigla[0];
            A1554ServicoFluxo_SrvPosPrcTmp = BC003V13_A1554ServicoFluxo_SrvPosPrcTmp[0];
            n1554ServicoFluxo_SrvPosPrcTmp = BC003V13_n1554ServicoFluxo_SrvPosPrcTmp[0];
            A1555ServicoFluxo_SrvPosPrcPgm = BC003V13_A1555ServicoFluxo_SrvPosPrcPgm[0];
            n1555ServicoFluxo_SrvPosPrcPgm = BC003V13_n1555ServicoFluxo_SrvPosPrcPgm[0];
            A1556ServicoFluxo_SrvPosPrcCnc = BC003V13_A1556ServicoFluxo_SrvPosPrcCnc[0];
            n1556ServicoFluxo_SrvPosPrcCnc = BC003V13_n1556ServicoFluxo_SrvPosPrcCnc[0];
            A1522ServicoFluxo_ServicoCod = BC003V13_A1522ServicoFluxo_ServicoCod[0];
            A1526ServicoFluxo_ServicoPos = BC003V13_A1526ServicoFluxo_ServicoPos[0];
            n1526ServicoFluxo_ServicoPos = BC003V13_n1526ServicoFluxo_ServicoPos[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext3V176( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound176 = 0;
         ScanKeyLoad3V176( ) ;
      }

      protected void ScanKeyLoad3V176( )
      {
         sMode176 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound176 = 1;
            A1528ServicoFluxo_Codigo = BC003V13_A1528ServicoFluxo_Codigo[0];
            A1523ServicoFluxo_ServicoSigla = BC003V13_A1523ServicoFluxo_ServicoSigla[0];
            n1523ServicoFluxo_ServicoSigla = BC003V13_n1523ServicoFluxo_ServicoSigla[0];
            A1533ServicoFluxo_ServicoTpHrq = BC003V13_A1533ServicoFluxo_ServicoTpHrq[0];
            n1533ServicoFluxo_ServicoTpHrq = BC003V13_n1533ServicoFluxo_ServicoTpHrq[0];
            A1532ServicoFluxo_Ordem = BC003V13_A1532ServicoFluxo_Ordem[0];
            n1532ServicoFluxo_Ordem = BC003V13_n1532ServicoFluxo_Ordem[0];
            A1558ServicoFluxo_SrvPosNome = BC003V13_A1558ServicoFluxo_SrvPosNome[0];
            n1558ServicoFluxo_SrvPosNome = BC003V13_n1558ServicoFluxo_SrvPosNome[0];
            A1527ServicoFluxo_SrvPosSigla = BC003V13_A1527ServicoFluxo_SrvPosSigla[0];
            n1527ServicoFluxo_SrvPosSigla = BC003V13_n1527ServicoFluxo_SrvPosSigla[0];
            A1554ServicoFluxo_SrvPosPrcTmp = BC003V13_A1554ServicoFluxo_SrvPosPrcTmp[0];
            n1554ServicoFluxo_SrvPosPrcTmp = BC003V13_n1554ServicoFluxo_SrvPosPrcTmp[0];
            A1555ServicoFluxo_SrvPosPrcPgm = BC003V13_A1555ServicoFluxo_SrvPosPrcPgm[0];
            n1555ServicoFluxo_SrvPosPrcPgm = BC003V13_n1555ServicoFluxo_SrvPosPrcPgm[0];
            A1556ServicoFluxo_SrvPosPrcCnc = BC003V13_A1556ServicoFluxo_SrvPosPrcCnc[0];
            n1556ServicoFluxo_SrvPosPrcCnc = BC003V13_n1556ServicoFluxo_SrvPosPrcCnc[0];
            A1522ServicoFluxo_ServicoCod = BC003V13_A1522ServicoFluxo_ServicoCod[0];
            A1526ServicoFluxo_ServicoPos = BC003V13_A1526ServicoFluxo_ServicoPos[0];
            n1526ServicoFluxo_ServicoPos = BC003V13_n1526ServicoFluxo_ServicoPos[0];
         }
         Gx_mode = sMode176;
      }

      protected void ScanKeyEnd3V176( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm3V176( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3V176( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3V176( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3V176( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3V176( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3V176( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3V176( )
      {
      }

      protected void AddRow3V176( )
      {
         VarsToRow176( bcServicoFluxo) ;
      }

      protected void ReadRow3V176( )
      {
         RowToVars176( bcServicoFluxo, 1) ;
      }

      protected void InitializeNonKey3V176( )
      {
         A1522ServicoFluxo_ServicoCod = 0;
         A1523ServicoFluxo_ServicoSigla = "";
         n1523ServicoFluxo_ServicoSigla = false;
         A1533ServicoFluxo_ServicoTpHrq = 0;
         n1533ServicoFluxo_ServicoTpHrq = false;
         A1532ServicoFluxo_Ordem = 0;
         n1532ServicoFluxo_Ordem = false;
         A1526ServicoFluxo_ServicoPos = 0;
         n1526ServicoFluxo_ServicoPos = false;
         A1558ServicoFluxo_SrvPosNome = "";
         n1558ServicoFluxo_SrvPosNome = false;
         A1527ServicoFluxo_SrvPosSigla = "";
         n1527ServicoFluxo_SrvPosSigla = false;
         A1554ServicoFluxo_SrvPosPrcTmp = 0;
         n1554ServicoFluxo_SrvPosPrcTmp = false;
         A1555ServicoFluxo_SrvPosPrcPgm = 0;
         n1555ServicoFluxo_SrvPosPrcPgm = false;
         A1556ServicoFluxo_SrvPosPrcCnc = 0;
         n1556ServicoFluxo_SrvPosPrcCnc = false;
         Z1532ServicoFluxo_Ordem = 0;
         Z1522ServicoFluxo_ServicoCod = 0;
         Z1526ServicoFluxo_ServicoPos = 0;
      }

      protected void InitAll3V176( )
      {
         A1528ServicoFluxo_Codigo = 0;
         InitializeNonKey3V176( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow176( SdtServicoFluxo obj176 )
      {
         obj176.gxTpr_Mode = Gx_mode;
         obj176.gxTpr_Servicofluxo_servicocod = A1522ServicoFluxo_ServicoCod;
         obj176.gxTpr_Servicofluxo_servicosigla = A1523ServicoFluxo_ServicoSigla;
         obj176.gxTpr_Servicofluxo_servicotphrq = A1533ServicoFluxo_ServicoTpHrq;
         obj176.gxTpr_Servicofluxo_ordem = A1532ServicoFluxo_Ordem;
         obj176.gxTpr_Servicofluxo_servicopos = A1526ServicoFluxo_ServicoPos;
         obj176.gxTpr_Servicofluxo_srvposnome = A1558ServicoFluxo_SrvPosNome;
         obj176.gxTpr_Servicofluxo_srvpossigla = A1527ServicoFluxo_SrvPosSigla;
         obj176.gxTpr_Servicofluxo_srvposprctmp = A1554ServicoFluxo_SrvPosPrcTmp;
         obj176.gxTpr_Servicofluxo_srvposprcpgm = A1555ServicoFluxo_SrvPosPrcPgm;
         obj176.gxTpr_Servicofluxo_srvposprccnc = A1556ServicoFluxo_SrvPosPrcCnc;
         obj176.gxTpr_Servicofluxo_codigo = A1528ServicoFluxo_Codigo;
         obj176.gxTpr_Servicofluxo_codigo_Z = Z1528ServicoFluxo_Codigo;
         obj176.gxTpr_Servicofluxo_servicocod_Z = Z1522ServicoFluxo_ServicoCod;
         obj176.gxTpr_Servicofluxo_servicosigla_Z = Z1523ServicoFluxo_ServicoSigla;
         obj176.gxTpr_Servicofluxo_servicotphrq_Z = Z1533ServicoFluxo_ServicoTpHrq;
         obj176.gxTpr_Servicofluxo_ordem_Z = Z1532ServicoFluxo_Ordem;
         obj176.gxTpr_Servicofluxo_servicopos_Z = Z1526ServicoFluxo_ServicoPos;
         obj176.gxTpr_Servicofluxo_srvposnome_Z = Z1558ServicoFluxo_SrvPosNome;
         obj176.gxTpr_Servicofluxo_srvpossigla_Z = Z1527ServicoFluxo_SrvPosSigla;
         obj176.gxTpr_Servicofluxo_srvposprctmp_Z = Z1554ServicoFluxo_SrvPosPrcTmp;
         obj176.gxTpr_Servicofluxo_srvposprcpgm_Z = Z1555ServicoFluxo_SrvPosPrcPgm;
         obj176.gxTpr_Servicofluxo_srvposprccnc_Z = Z1556ServicoFluxo_SrvPosPrcCnc;
         obj176.gxTpr_Servicofluxo_servicosigla_N = (short)(Convert.ToInt16(n1523ServicoFluxo_ServicoSigla));
         obj176.gxTpr_Servicofluxo_servicotphrq_N = (short)(Convert.ToInt16(n1533ServicoFluxo_ServicoTpHrq));
         obj176.gxTpr_Servicofluxo_ordem_N = (short)(Convert.ToInt16(n1532ServicoFluxo_Ordem));
         obj176.gxTpr_Servicofluxo_servicopos_N = (short)(Convert.ToInt16(n1526ServicoFluxo_ServicoPos));
         obj176.gxTpr_Servicofluxo_srvposnome_N = (short)(Convert.ToInt16(n1558ServicoFluxo_SrvPosNome));
         obj176.gxTpr_Servicofluxo_srvpossigla_N = (short)(Convert.ToInt16(n1527ServicoFluxo_SrvPosSigla));
         obj176.gxTpr_Servicofluxo_srvposprctmp_N = (short)(Convert.ToInt16(n1554ServicoFluxo_SrvPosPrcTmp));
         obj176.gxTpr_Servicofluxo_srvposprcpgm_N = (short)(Convert.ToInt16(n1555ServicoFluxo_SrvPosPrcPgm));
         obj176.gxTpr_Servicofluxo_srvposprccnc_N = (short)(Convert.ToInt16(n1556ServicoFluxo_SrvPosPrcCnc));
         obj176.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow176( SdtServicoFluxo obj176 )
      {
         obj176.gxTpr_Servicofluxo_codigo = A1528ServicoFluxo_Codigo;
         return  ;
      }

      public void RowToVars176( SdtServicoFluxo obj176 ,
                                int forceLoad )
      {
         Gx_mode = obj176.gxTpr_Mode;
         A1522ServicoFluxo_ServicoCod = obj176.gxTpr_Servicofluxo_servicocod;
         A1523ServicoFluxo_ServicoSigla = obj176.gxTpr_Servicofluxo_servicosigla;
         n1523ServicoFluxo_ServicoSigla = false;
         A1533ServicoFluxo_ServicoTpHrq = obj176.gxTpr_Servicofluxo_servicotphrq;
         n1533ServicoFluxo_ServicoTpHrq = false;
         A1532ServicoFluxo_Ordem = obj176.gxTpr_Servicofluxo_ordem;
         n1532ServicoFluxo_Ordem = false;
         A1526ServicoFluxo_ServicoPos = obj176.gxTpr_Servicofluxo_servicopos;
         n1526ServicoFluxo_ServicoPos = false;
         A1558ServicoFluxo_SrvPosNome = obj176.gxTpr_Servicofluxo_srvposnome;
         n1558ServicoFluxo_SrvPosNome = false;
         A1527ServicoFluxo_SrvPosSigla = obj176.gxTpr_Servicofluxo_srvpossigla;
         n1527ServicoFluxo_SrvPosSigla = false;
         A1554ServicoFluxo_SrvPosPrcTmp = obj176.gxTpr_Servicofluxo_srvposprctmp;
         n1554ServicoFluxo_SrvPosPrcTmp = false;
         A1555ServicoFluxo_SrvPosPrcPgm = obj176.gxTpr_Servicofluxo_srvposprcpgm;
         n1555ServicoFluxo_SrvPosPrcPgm = false;
         A1556ServicoFluxo_SrvPosPrcCnc = obj176.gxTpr_Servicofluxo_srvposprccnc;
         n1556ServicoFluxo_SrvPosPrcCnc = false;
         A1528ServicoFluxo_Codigo = obj176.gxTpr_Servicofluxo_codigo;
         Z1528ServicoFluxo_Codigo = obj176.gxTpr_Servicofluxo_codigo_Z;
         Z1522ServicoFluxo_ServicoCod = obj176.gxTpr_Servicofluxo_servicocod_Z;
         Z1523ServicoFluxo_ServicoSigla = obj176.gxTpr_Servicofluxo_servicosigla_Z;
         Z1533ServicoFluxo_ServicoTpHrq = obj176.gxTpr_Servicofluxo_servicotphrq_Z;
         Z1532ServicoFluxo_Ordem = obj176.gxTpr_Servicofluxo_ordem_Z;
         Z1526ServicoFluxo_ServicoPos = obj176.gxTpr_Servicofluxo_servicopos_Z;
         Z1558ServicoFluxo_SrvPosNome = obj176.gxTpr_Servicofluxo_srvposnome_Z;
         Z1527ServicoFluxo_SrvPosSigla = obj176.gxTpr_Servicofluxo_srvpossigla_Z;
         Z1554ServicoFluxo_SrvPosPrcTmp = obj176.gxTpr_Servicofluxo_srvposprctmp_Z;
         Z1555ServicoFluxo_SrvPosPrcPgm = obj176.gxTpr_Servicofluxo_srvposprcpgm_Z;
         Z1556ServicoFluxo_SrvPosPrcCnc = obj176.gxTpr_Servicofluxo_srvposprccnc_Z;
         n1523ServicoFluxo_ServicoSigla = (bool)(Convert.ToBoolean(obj176.gxTpr_Servicofluxo_servicosigla_N));
         n1533ServicoFluxo_ServicoTpHrq = (bool)(Convert.ToBoolean(obj176.gxTpr_Servicofluxo_servicotphrq_N));
         n1532ServicoFluxo_Ordem = (bool)(Convert.ToBoolean(obj176.gxTpr_Servicofluxo_ordem_N));
         n1526ServicoFluxo_ServicoPos = (bool)(Convert.ToBoolean(obj176.gxTpr_Servicofluxo_servicopos_N));
         n1558ServicoFluxo_SrvPosNome = (bool)(Convert.ToBoolean(obj176.gxTpr_Servicofluxo_srvposnome_N));
         n1527ServicoFluxo_SrvPosSigla = (bool)(Convert.ToBoolean(obj176.gxTpr_Servicofluxo_srvpossigla_N));
         n1554ServicoFluxo_SrvPosPrcTmp = (bool)(Convert.ToBoolean(obj176.gxTpr_Servicofluxo_srvposprctmp_N));
         n1555ServicoFluxo_SrvPosPrcPgm = (bool)(Convert.ToBoolean(obj176.gxTpr_Servicofluxo_srvposprcpgm_N));
         n1556ServicoFluxo_SrvPosPrcCnc = (bool)(Convert.ToBoolean(obj176.gxTpr_Servicofluxo_srvposprccnc_N));
         Gx_mode = obj176.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1528ServicoFluxo_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey3V176( ) ;
         ScanKeyStart3V176( ) ;
         if ( RcdFound176 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1528ServicoFluxo_Codigo = A1528ServicoFluxo_Codigo;
         }
         ZM3V176( -3) ;
         OnLoadActions3V176( ) ;
         AddRow3V176( ) ;
         ScanKeyEnd3V176( ) ;
         if ( RcdFound176 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars176( bcServicoFluxo, 0) ;
         ScanKeyStart3V176( ) ;
         if ( RcdFound176 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1528ServicoFluxo_Codigo = A1528ServicoFluxo_Codigo;
         }
         ZM3V176( -3) ;
         OnLoadActions3V176( ) ;
         AddRow3V176( ) ;
         ScanKeyEnd3V176( ) ;
         if ( RcdFound176 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars176( bcServicoFluxo, 0) ;
         nKeyPressed = 1;
         GetKey3V176( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert3V176( ) ;
         }
         else
         {
            if ( RcdFound176 == 1 )
            {
               if ( A1528ServicoFluxo_Codigo != Z1528ServicoFluxo_Codigo )
               {
                  A1528ServicoFluxo_Codigo = Z1528ServicoFluxo_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update3V176( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1528ServicoFluxo_Codigo != Z1528ServicoFluxo_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3V176( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert3V176( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow176( bcServicoFluxo) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars176( bcServicoFluxo, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey3V176( ) ;
         if ( RcdFound176 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1528ServicoFluxo_Codigo != Z1528ServicoFluxo_Codigo )
            {
               A1528ServicoFluxo_Codigo = Z1528ServicoFluxo_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1528ServicoFluxo_Codigo != Z1528ServicoFluxo_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
         context.RollbackDataStores( "ServicoFluxo_BC");
         VarsToRow176( bcServicoFluxo) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcServicoFluxo.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcServicoFluxo.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcServicoFluxo )
         {
            bcServicoFluxo = (SdtServicoFluxo)(sdt);
            if ( StringUtil.StrCmp(bcServicoFluxo.gxTpr_Mode, "") == 0 )
            {
               bcServicoFluxo.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow176( bcServicoFluxo) ;
            }
            else
            {
               RowToVars176( bcServicoFluxo, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcServicoFluxo.gxTpr_Mode, "") == 0 )
            {
               bcServicoFluxo.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars176( bcServicoFluxo, 1) ;
         return  ;
      }

      public SdtServicoFluxo ServicoFluxo_BC
      {
         get {
            return bcServicoFluxo ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
         pr_default.close(10);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV37Pgmname = "";
         AV14TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z1523ServicoFluxo_ServicoSigla = "";
         A1523ServicoFluxo_ServicoSigla = "";
         Z1558ServicoFluxo_SrvPosNome = "";
         A1558ServicoFluxo_SrvPosNome = "";
         Z1527ServicoFluxo_SrvPosSigla = "";
         A1527ServicoFluxo_SrvPosSigla = "";
         BC003V6_A1528ServicoFluxo_Codigo = new int[1] ;
         BC003V6_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         BC003V6_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         BC003V6_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         BC003V6_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         BC003V6_A1532ServicoFluxo_Ordem = new short[1] ;
         BC003V6_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         BC003V6_A1558ServicoFluxo_SrvPosNome = new String[] {""} ;
         BC003V6_n1558ServicoFluxo_SrvPosNome = new bool[] {false} ;
         BC003V6_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         BC003V6_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         BC003V6_A1554ServicoFluxo_SrvPosPrcTmp = new short[1] ;
         BC003V6_n1554ServicoFluxo_SrvPosPrcTmp = new bool[] {false} ;
         BC003V6_A1555ServicoFluxo_SrvPosPrcPgm = new short[1] ;
         BC003V6_n1555ServicoFluxo_SrvPosPrcPgm = new bool[] {false} ;
         BC003V6_A1556ServicoFluxo_SrvPosPrcCnc = new short[1] ;
         BC003V6_n1556ServicoFluxo_SrvPosPrcCnc = new bool[] {false} ;
         BC003V6_A1522ServicoFluxo_ServicoCod = new int[1] ;
         BC003V6_A1526ServicoFluxo_ServicoPos = new int[1] ;
         BC003V6_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         BC003V4_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         BC003V4_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         BC003V4_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         BC003V4_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         BC003V5_A1558ServicoFluxo_SrvPosNome = new String[] {""} ;
         BC003V5_n1558ServicoFluxo_SrvPosNome = new bool[] {false} ;
         BC003V5_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         BC003V5_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         BC003V5_A1554ServicoFluxo_SrvPosPrcTmp = new short[1] ;
         BC003V5_n1554ServicoFluxo_SrvPosPrcTmp = new bool[] {false} ;
         BC003V5_A1555ServicoFluxo_SrvPosPrcPgm = new short[1] ;
         BC003V5_n1555ServicoFluxo_SrvPosPrcPgm = new bool[] {false} ;
         BC003V5_A1556ServicoFluxo_SrvPosPrcCnc = new short[1] ;
         BC003V5_n1556ServicoFluxo_SrvPosPrcCnc = new bool[] {false} ;
         BC003V7_A1528ServicoFluxo_Codigo = new int[1] ;
         BC003V3_A1528ServicoFluxo_Codigo = new int[1] ;
         BC003V3_A1532ServicoFluxo_Ordem = new short[1] ;
         BC003V3_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         BC003V3_A1522ServicoFluxo_ServicoCod = new int[1] ;
         BC003V3_A1526ServicoFluxo_ServicoPos = new int[1] ;
         BC003V3_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         sMode176 = "";
         BC003V2_A1528ServicoFluxo_Codigo = new int[1] ;
         BC003V2_A1532ServicoFluxo_Ordem = new short[1] ;
         BC003V2_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         BC003V2_A1522ServicoFluxo_ServicoCod = new int[1] ;
         BC003V2_A1526ServicoFluxo_ServicoPos = new int[1] ;
         BC003V2_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         BC003V8_A1528ServicoFluxo_Codigo = new int[1] ;
         BC003V11_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         BC003V11_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         BC003V11_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         BC003V11_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         BC003V12_A1558ServicoFluxo_SrvPosNome = new String[] {""} ;
         BC003V12_n1558ServicoFluxo_SrvPosNome = new bool[] {false} ;
         BC003V12_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         BC003V12_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         BC003V12_A1554ServicoFluxo_SrvPosPrcTmp = new short[1] ;
         BC003V12_n1554ServicoFluxo_SrvPosPrcTmp = new bool[] {false} ;
         BC003V12_A1555ServicoFluxo_SrvPosPrcPgm = new short[1] ;
         BC003V12_n1555ServicoFluxo_SrvPosPrcPgm = new bool[] {false} ;
         BC003V12_A1556ServicoFluxo_SrvPosPrcCnc = new short[1] ;
         BC003V12_n1556ServicoFluxo_SrvPosPrcCnc = new bool[] {false} ;
         BC003V13_A1528ServicoFluxo_Codigo = new int[1] ;
         BC003V13_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         BC003V13_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         BC003V13_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         BC003V13_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         BC003V13_A1532ServicoFluxo_Ordem = new short[1] ;
         BC003V13_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         BC003V13_A1558ServicoFluxo_SrvPosNome = new String[] {""} ;
         BC003V13_n1558ServicoFluxo_SrvPosNome = new bool[] {false} ;
         BC003V13_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         BC003V13_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         BC003V13_A1554ServicoFluxo_SrvPosPrcTmp = new short[1] ;
         BC003V13_n1554ServicoFluxo_SrvPosPrcTmp = new bool[] {false} ;
         BC003V13_A1555ServicoFluxo_SrvPosPrcPgm = new short[1] ;
         BC003V13_n1555ServicoFluxo_SrvPosPrcPgm = new bool[] {false} ;
         BC003V13_A1556ServicoFluxo_SrvPosPrcCnc = new short[1] ;
         BC003V13_n1556ServicoFluxo_SrvPosPrcCnc = new bool[] {false} ;
         BC003V13_A1522ServicoFluxo_ServicoCod = new int[1] ;
         BC003V13_A1526ServicoFluxo_ServicoPos = new int[1] ;
         BC003V13_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicofluxo_bc__default(),
            new Object[][] {
                new Object[] {
               BC003V2_A1528ServicoFluxo_Codigo, BC003V2_A1532ServicoFluxo_Ordem, BC003V2_n1532ServicoFluxo_Ordem, BC003V2_A1522ServicoFluxo_ServicoCod, BC003V2_A1526ServicoFluxo_ServicoPos, BC003V2_n1526ServicoFluxo_ServicoPos
               }
               , new Object[] {
               BC003V3_A1528ServicoFluxo_Codigo, BC003V3_A1532ServicoFluxo_Ordem, BC003V3_n1532ServicoFluxo_Ordem, BC003V3_A1522ServicoFluxo_ServicoCod, BC003V3_A1526ServicoFluxo_ServicoPos, BC003V3_n1526ServicoFluxo_ServicoPos
               }
               , new Object[] {
               BC003V4_A1523ServicoFluxo_ServicoSigla, BC003V4_n1523ServicoFluxo_ServicoSigla, BC003V4_A1533ServicoFluxo_ServicoTpHrq, BC003V4_n1533ServicoFluxo_ServicoTpHrq
               }
               , new Object[] {
               BC003V5_A1558ServicoFluxo_SrvPosNome, BC003V5_n1558ServicoFluxo_SrvPosNome, BC003V5_A1527ServicoFluxo_SrvPosSigla, BC003V5_n1527ServicoFluxo_SrvPosSigla, BC003V5_A1554ServicoFluxo_SrvPosPrcTmp, BC003V5_n1554ServicoFluxo_SrvPosPrcTmp, BC003V5_A1555ServicoFluxo_SrvPosPrcPgm, BC003V5_n1555ServicoFluxo_SrvPosPrcPgm, BC003V5_A1556ServicoFluxo_SrvPosPrcCnc, BC003V5_n1556ServicoFluxo_SrvPosPrcCnc
               }
               , new Object[] {
               BC003V6_A1528ServicoFluxo_Codigo, BC003V6_A1523ServicoFluxo_ServicoSigla, BC003V6_n1523ServicoFluxo_ServicoSigla, BC003V6_A1533ServicoFluxo_ServicoTpHrq, BC003V6_n1533ServicoFluxo_ServicoTpHrq, BC003V6_A1532ServicoFluxo_Ordem, BC003V6_n1532ServicoFluxo_Ordem, BC003V6_A1558ServicoFluxo_SrvPosNome, BC003V6_n1558ServicoFluxo_SrvPosNome, BC003V6_A1527ServicoFluxo_SrvPosSigla,
               BC003V6_n1527ServicoFluxo_SrvPosSigla, BC003V6_A1554ServicoFluxo_SrvPosPrcTmp, BC003V6_n1554ServicoFluxo_SrvPosPrcTmp, BC003V6_A1555ServicoFluxo_SrvPosPrcPgm, BC003V6_n1555ServicoFluxo_SrvPosPrcPgm, BC003V6_A1556ServicoFluxo_SrvPosPrcCnc, BC003V6_n1556ServicoFluxo_SrvPosPrcCnc, BC003V6_A1522ServicoFluxo_ServicoCod, BC003V6_A1526ServicoFluxo_ServicoPos, BC003V6_n1526ServicoFluxo_ServicoPos
               }
               , new Object[] {
               BC003V7_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               BC003V8_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC003V11_A1523ServicoFluxo_ServicoSigla, BC003V11_n1523ServicoFluxo_ServicoSigla, BC003V11_A1533ServicoFluxo_ServicoTpHrq, BC003V11_n1533ServicoFluxo_ServicoTpHrq
               }
               , new Object[] {
               BC003V12_A1558ServicoFluxo_SrvPosNome, BC003V12_n1558ServicoFluxo_SrvPosNome, BC003V12_A1527ServicoFluxo_SrvPosSigla, BC003V12_n1527ServicoFluxo_SrvPosSigla, BC003V12_A1554ServicoFluxo_SrvPosPrcTmp, BC003V12_n1554ServicoFluxo_SrvPosPrcTmp, BC003V12_A1555ServicoFluxo_SrvPosPrcPgm, BC003V12_n1555ServicoFluxo_SrvPosPrcPgm, BC003V12_A1556ServicoFluxo_SrvPosPrcCnc, BC003V12_n1556ServicoFluxo_SrvPosPrcCnc
               }
               , new Object[] {
               BC003V13_A1528ServicoFluxo_Codigo, BC003V13_A1523ServicoFluxo_ServicoSigla, BC003V13_n1523ServicoFluxo_ServicoSigla, BC003V13_A1533ServicoFluxo_ServicoTpHrq, BC003V13_n1533ServicoFluxo_ServicoTpHrq, BC003V13_A1532ServicoFluxo_Ordem, BC003V13_n1532ServicoFluxo_Ordem, BC003V13_A1558ServicoFluxo_SrvPosNome, BC003V13_n1558ServicoFluxo_SrvPosNome, BC003V13_A1527ServicoFluxo_SrvPosSigla,
               BC003V13_n1527ServicoFluxo_SrvPosSigla, BC003V13_A1554ServicoFluxo_SrvPosPrcTmp, BC003V13_n1554ServicoFluxo_SrvPosPrcTmp, BC003V13_A1555ServicoFluxo_SrvPosPrcPgm, BC003V13_n1555ServicoFluxo_SrvPosPrcPgm, BC003V13_A1556ServicoFluxo_SrvPosPrcCnc, BC003V13_n1556ServicoFluxo_SrvPosPrcCnc, BC003V13_A1522ServicoFluxo_ServicoCod, BC003V13_A1526ServicoFluxo_ServicoPos, BC003V13_n1526ServicoFluxo_ServicoPos
               }
            }
         );
         AV37Pgmname = "ServicoFluxo_BC";
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E123V2 */
         E123V2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short AV15ServicoFluxo_Ordem ;
      private short AV17RenumerarDesde ;
      private short AV24OrdemAnterior ;
      private short A1532ServicoFluxo_Ordem ;
      private short GX_JID ;
      private short Z1532ServicoFluxo_Ordem ;
      private short Z1533ServicoFluxo_ServicoTpHrq ;
      private short A1533ServicoFluxo_ServicoTpHrq ;
      private short Z1554ServicoFluxo_SrvPosPrcTmp ;
      private short A1554ServicoFluxo_SrvPosPrcTmp ;
      private short Z1555ServicoFluxo_SrvPosPrcPgm ;
      private short A1555ServicoFluxo_SrvPosPrcPgm ;
      private short Z1556ServicoFluxo_SrvPosPrcCnc ;
      private short A1556ServicoFluxo_SrvPosPrcCnc ;
      private short RcdFound176 ;
      private int trnEnded ;
      private int Z1528ServicoFluxo_Codigo ;
      private int A1528ServicoFluxo_Codigo ;
      private int AV38GXV1 ;
      private int AV11Insert_ServicoFluxo_ServicoCod ;
      private int AV13Insert_ServicoFluxo_ServicoPos ;
      private int A1522ServicoFluxo_ServicoCod ;
      private int Z1522ServicoFluxo_ServicoCod ;
      private int Z1526ServicoFluxo_ServicoPos ;
      private int A1526ServicoFluxo_ServicoPos ;
      private int Z155Servico_Codigo ;
      private int A155Servico_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV37Pgmname ;
      private String Z1523ServicoFluxo_ServicoSigla ;
      private String A1523ServicoFluxo_ServicoSigla ;
      private String Z1558ServicoFluxo_SrvPosNome ;
      private String A1558ServicoFluxo_SrvPosNome ;
      private String Z1527ServicoFluxo_SrvPosSigla ;
      private String A1527ServicoFluxo_SrvPosSigla ;
      private String sMode176 ;
      private bool n1523ServicoFluxo_ServicoSigla ;
      private bool n1533ServicoFluxo_ServicoTpHrq ;
      private bool n1532ServicoFluxo_Ordem ;
      private bool n1558ServicoFluxo_SrvPosNome ;
      private bool n1527ServicoFluxo_SrvPosSigla ;
      private bool n1554ServicoFluxo_SrvPosPrcTmp ;
      private bool n1555ServicoFluxo_SrvPosPrcPgm ;
      private bool n1556ServicoFluxo_SrvPosPrcCnc ;
      private bool n1526ServicoFluxo_ServicoPos ;
      private IGxSession AV10WebSession ;
      private SdtServicoFluxo bcServicoFluxo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC003V6_A1528ServicoFluxo_Codigo ;
      private String[] BC003V6_A1523ServicoFluxo_ServicoSigla ;
      private bool[] BC003V6_n1523ServicoFluxo_ServicoSigla ;
      private short[] BC003V6_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] BC003V6_n1533ServicoFluxo_ServicoTpHrq ;
      private short[] BC003V6_A1532ServicoFluxo_Ordem ;
      private bool[] BC003V6_n1532ServicoFluxo_Ordem ;
      private String[] BC003V6_A1558ServicoFluxo_SrvPosNome ;
      private bool[] BC003V6_n1558ServicoFluxo_SrvPosNome ;
      private String[] BC003V6_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] BC003V6_n1527ServicoFluxo_SrvPosSigla ;
      private short[] BC003V6_A1554ServicoFluxo_SrvPosPrcTmp ;
      private bool[] BC003V6_n1554ServicoFluxo_SrvPosPrcTmp ;
      private short[] BC003V6_A1555ServicoFluxo_SrvPosPrcPgm ;
      private bool[] BC003V6_n1555ServicoFluxo_SrvPosPrcPgm ;
      private short[] BC003V6_A1556ServicoFluxo_SrvPosPrcCnc ;
      private bool[] BC003V6_n1556ServicoFluxo_SrvPosPrcCnc ;
      private int[] BC003V6_A1522ServicoFluxo_ServicoCod ;
      private int[] BC003V6_A1526ServicoFluxo_ServicoPos ;
      private bool[] BC003V6_n1526ServicoFluxo_ServicoPos ;
      private String[] BC003V4_A1523ServicoFluxo_ServicoSigla ;
      private bool[] BC003V4_n1523ServicoFluxo_ServicoSigla ;
      private short[] BC003V4_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] BC003V4_n1533ServicoFluxo_ServicoTpHrq ;
      private String[] BC003V5_A1558ServicoFluxo_SrvPosNome ;
      private bool[] BC003V5_n1558ServicoFluxo_SrvPosNome ;
      private String[] BC003V5_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] BC003V5_n1527ServicoFluxo_SrvPosSigla ;
      private short[] BC003V5_A1554ServicoFluxo_SrvPosPrcTmp ;
      private bool[] BC003V5_n1554ServicoFluxo_SrvPosPrcTmp ;
      private short[] BC003V5_A1555ServicoFluxo_SrvPosPrcPgm ;
      private bool[] BC003V5_n1555ServicoFluxo_SrvPosPrcPgm ;
      private short[] BC003V5_A1556ServicoFluxo_SrvPosPrcCnc ;
      private bool[] BC003V5_n1556ServicoFluxo_SrvPosPrcCnc ;
      private int[] BC003V7_A1528ServicoFluxo_Codigo ;
      private int[] BC003V3_A1528ServicoFluxo_Codigo ;
      private short[] BC003V3_A1532ServicoFluxo_Ordem ;
      private bool[] BC003V3_n1532ServicoFluxo_Ordem ;
      private int[] BC003V3_A1522ServicoFluxo_ServicoCod ;
      private int[] BC003V3_A1526ServicoFluxo_ServicoPos ;
      private bool[] BC003V3_n1526ServicoFluxo_ServicoPos ;
      private int[] BC003V2_A1528ServicoFluxo_Codigo ;
      private short[] BC003V2_A1532ServicoFluxo_Ordem ;
      private bool[] BC003V2_n1532ServicoFluxo_Ordem ;
      private int[] BC003V2_A1522ServicoFluxo_ServicoCod ;
      private int[] BC003V2_A1526ServicoFluxo_ServicoPos ;
      private bool[] BC003V2_n1526ServicoFluxo_ServicoPos ;
      private int[] BC003V8_A1528ServicoFluxo_Codigo ;
      private String[] BC003V11_A1523ServicoFluxo_ServicoSigla ;
      private bool[] BC003V11_n1523ServicoFluxo_ServicoSigla ;
      private short[] BC003V11_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] BC003V11_n1533ServicoFluxo_ServicoTpHrq ;
      private String[] BC003V12_A1558ServicoFluxo_SrvPosNome ;
      private bool[] BC003V12_n1558ServicoFluxo_SrvPosNome ;
      private String[] BC003V12_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] BC003V12_n1527ServicoFluxo_SrvPosSigla ;
      private short[] BC003V12_A1554ServicoFluxo_SrvPosPrcTmp ;
      private bool[] BC003V12_n1554ServicoFluxo_SrvPosPrcTmp ;
      private short[] BC003V12_A1555ServicoFluxo_SrvPosPrcPgm ;
      private bool[] BC003V12_n1555ServicoFluxo_SrvPosPrcPgm ;
      private short[] BC003V12_A1556ServicoFluxo_SrvPosPrcCnc ;
      private bool[] BC003V12_n1556ServicoFluxo_SrvPosPrcCnc ;
      private int[] BC003V13_A1528ServicoFluxo_Codigo ;
      private String[] BC003V13_A1523ServicoFluxo_ServicoSigla ;
      private bool[] BC003V13_n1523ServicoFluxo_ServicoSigla ;
      private short[] BC003V13_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] BC003V13_n1533ServicoFluxo_ServicoTpHrq ;
      private short[] BC003V13_A1532ServicoFluxo_Ordem ;
      private bool[] BC003V13_n1532ServicoFluxo_Ordem ;
      private String[] BC003V13_A1558ServicoFluxo_SrvPosNome ;
      private bool[] BC003V13_n1558ServicoFluxo_SrvPosNome ;
      private String[] BC003V13_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] BC003V13_n1527ServicoFluxo_SrvPosSigla ;
      private short[] BC003V13_A1554ServicoFluxo_SrvPosPrcTmp ;
      private bool[] BC003V13_n1554ServicoFluxo_SrvPosPrcTmp ;
      private short[] BC003V13_A1555ServicoFluxo_SrvPosPrcPgm ;
      private bool[] BC003V13_n1555ServicoFluxo_SrvPosPrcPgm ;
      private short[] BC003V13_A1556ServicoFluxo_SrvPosPrcCnc ;
      private bool[] BC003V13_n1556ServicoFluxo_SrvPosPrcCnc ;
      private int[] BC003V13_A1522ServicoFluxo_ServicoCod ;
      private int[] BC003V13_A1526ServicoFluxo_ServicoPos ;
      private bool[] BC003V13_n1526ServicoFluxo_ServicoPos ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV14TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class servicofluxo_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC003V6 ;
          prmBC003V6 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003V4 ;
          prmBC003V4 = new Object[] {
          new Object[] {"@ServicoFluxo_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003V5 ;
          prmBC003V5 = new Object[] {
          new Object[] {"@ServicoFluxo_ServicoPos",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003V7 ;
          prmBC003V7 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003V3 ;
          prmBC003V3 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003V2 ;
          prmBC003V2 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003V8 ;
          prmBC003V8 = new Object[] {
          new Object[] {"@ServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ServicoFluxo_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoFluxo_ServicoPos",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003V9 ;
          prmBC003V9 = new Object[] {
          new Object[] {"@ServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@ServicoFluxo_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoFluxo_ServicoPos",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003V10 ;
          prmBC003V10 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003V11 ;
          prmBC003V11 = new Object[] {
          new Object[] {"@ServicoFluxo_ServicoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003V12 ;
          prmBC003V12 = new Object[] {
          new Object[] {"@ServicoFluxo_ServicoPos",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC003V13 ;
          prmBC003V13 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC003V2", "SELECT [ServicoFluxo_Codigo], [ServicoFluxo_Ordem], [ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, [ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos FROM [ServicoFluxo] WITH (UPDLOCK) WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003V2,1,0,true,false )
             ,new CursorDef("BC003V3", "SELECT [ServicoFluxo_Codigo], [ServicoFluxo_Ordem], [ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, [ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003V3,1,0,true,false )
             ,new CursorDef("BC003V4", "SELECT [Servico_Sigla] AS ServicoFluxo_ServicoSigla, [Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ServicoFluxo_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003V4,1,0,true,false )
             ,new CursorDef("BC003V5", "SELECT [Servico_Nome] AS ServicoFluxo_SrvPosNome, [Servico_Sigla] AS ServicoFluxo_SrvPosSigla, [Servico_PercTmp] AS ServicoFluxo_SrvPosPrcTmp, [Servico_PercPgm] AS ServicoFluxo_SrvPosPrcPgm, [Servico_PercCnc] AS ServicoFluxo_SrvPosPrcCnc FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ServicoFluxo_ServicoPos ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003V5,1,0,true,false )
             ,new CursorDef("BC003V6", "SELECT TM1.[ServicoFluxo_Codigo], T2.[Servico_Sigla] AS ServicoFluxo_ServicoSigla, T2.[Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq, TM1.[ServicoFluxo_Ordem], T3.[Servico_Nome] AS ServicoFluxo_SrvPosNome, T3.[Servico_Sigla] AS ServicoFluxo_SrvPosSigla, T3.[Servico_PercTmp] AS ServicoFluxo_SrvPosPrcTmp, T3.[Servico_PercPgm] AS ServicoFluxo_SrvPosPrcPgm, T3.[Servico_PercCnc] AS ServicoFluxo_SrvPosPrcCnc, TM1.[ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, TM1.[ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos FROM (([ServicoFluxo] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[ServicoFluxo_ServicoCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = TM1.[ServicoFluxo_ServicoPos]) WHERE TM1.[ServicoFluxo_Codigo] = @ServicoFluxo_Codigo ORDER BY TM1.[ServicoFluxo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003V6,100,0,true,false )
             ,new CursorDef("BC003V7", "SELECT [ServicoFluxo_Codigo] FROM [ServicoFluxo] WITH (NOLOCK) WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003V7,1,0,true,false )
             ,new CursorDef("BC003V8", "INSERT INTO [ServicoFluxo]([ServicoFluxo_Ordem], [ServicoFluxo_ServicoCod], [ServicoFluxo_ServicoPos]) VALUES(@ServicoFluxo_Ordem, @ServicoFluxo_ServicoCod, @ServicoFluxo_ServicoPos); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC003V8)
             ,new CursorDef("BC003V9", "UPDATE [ServicoFluxo] SET [ServicoFluxo_Ordem]=@ServicoFluxo_Ordem, [ServicoFluxo_ServicoCod]=@ServicoFluxo_ServicoCod, [ServicoFluxo_ServicoPos]=@ServicoFluxo_ServicoPos  WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo", GxErrorMask.GX_NOMASK,prmBC003V9)
             ,new CursorDef("BC003V10", "DELETE FROM [ServicoFluxo]  WHERE [ServicoFluxo_Codigo] = @ServicoFluxo_Codigo", GxErrorMask.GX_NOMASK,prmBC003V10)
             ,new CursorDef("BC003V11", "SELECT [Servico_Sigla] AS ServicoFluxo_ServicoSigla, [Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ServicoFluxo_ServicoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003V11,1,0,true,false )
             ,new CursorDef("BC003V12", "SELECT [Servico_Nome] AS ServicoFluxo_SrvPosNome, [Servico_Sigla] AS ServicoFluxo_SrvPosSigla, [Servico_PercTmp] AS ServicoFluxo_SrvPosPrcTmp, [Servico_PercPgm] AS ServicoFluxo_SrvPosPrcPgm, [Servico_PercCnc] AS ServicoFluxo_SrvPosPrcCnc FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ServicoFluxo_ServicoPos ",true, GxErrorMask.GX_NOMASK, false, this,prmBC003V12,1,0,true,false )
             ,new CursorDef("BC003V13", "SELECT TM1.[ServicoFluxo_Codigo], T2.[Servico_Sigla] AS ServicoFluxo_ServicoSigla, T2.[Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq, TM1.[ServicoFluxo_Ordem], T3.[Servico_Nome] AS ServicoFluxo_SrvPosNome, T3.[Servico_Sigla] AS ServicoFluxo_SrvPosSigla, T3.[Servico_PercTmp] AS ServicoFluxo_SrvPosPrcTmp, T3.[Servico_PercPgm] AS ServicoFluxo_SrvPosPrcPgm, T3.[Servico_PercCnc] AS ServicoFluxo_SrvPosPrcCnc, TM1.[ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, TM1.[ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos FROM (([ServicoFluxo] TM1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = TM1.[ServicoFluxo_ServicoCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = TM1.[ServicoFluxo_ServicoPos]) WHERE TM1.[ServicoFluxo_Codigo] = @ServicoFluxo_Codigo ORDER BY TM1.[ServicoFluxo_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC003V13,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 50) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((short[]) buf[11])[0] = rslt.getShort(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((short[]) buf[13])[0] = rslt.getShort(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((short[]) buf[15])[0] = rslt.getShort(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                ((int[]) buf[18])[0] = rslt.getInt(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(1, (short)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
