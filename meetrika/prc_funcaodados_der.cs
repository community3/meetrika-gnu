/*
               File: PRC_FuncaoDados_DER
        Description: FuncaoDados_DER
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:42.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_funcaodados_der : GXProcedure
   {
      public prc_funcaodados_der( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_funcaodados_der( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoDados_Codigo ,
                           out short aP1_FuncaoDados_DER )
      {
         this.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.AV9FuncaoDados_DER = 0 ;
         initialize();
         executePrivate();
         aP1_FuncaoDados_DER=this.AV9FuncaoDados_DER;
      }

      public short executeUdp( int aP0_FuncaoDados_Codigo )
      {
         this.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         this.AV9FuncaoDados_DER = 0 ;
         initialize();
         executePrivate();
         aP1_FuncaoDados_DER=this.AV9FuncaoDados_DER;
         return AV9FuncaoDados_DER ;
      }

      public void executeSubmit( int aP0_FuncaoDados_Codigo ,
                                 out short aP1_FuncaoDados_DER )
      {
         prc_funcaodados_der objprc_funcaodados_der;
         objprc_funcaodados_der = new prc_funcaodados_der();
         objprc_funcaodados_der.AV8FuncaoDados_Codigo = aP0_FuncaoDados_Codigo;
         objprc_funcaodados_der.AV9FuncaoDados_DER = 0 ;
         objprc_funcaodados_der.context.SetSubmitInitialConfig(context);
         objprc_funcaodados_der.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_funcaodados_der);
         aP1_FuncaoDados_DER=this.AV9FuncaoDados_DER;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_funcaodados_der)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9FuncaoDados_DER = 0;
         /* Using cursor P00212 */
         pr_default.execute(0, new Object[] {AV8FuncaoDados_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A368FuncaoDados_Codigo = P00212_A368FuncaoDados_Codigo[0];
            A391FuncaoDados_FuncaoDadosCod = P00212_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = P00212_n391FuncaoDados_FuncaoDadosCod[0];
            A1024FuncaoDados_DERImp = P00212_A1024FuncaoDados_DERImp[0];
            n1024FuncaoDados_DERImp = P00212_n1024FuncaoDados_DERImp[0];
            if ( (0==A391FuncaoDados_FuncaoDadosCod) )
            {
               /* Using cursor P00213 */
               pr_default.execute(1, new Object[] {AV8FuncaoDados_Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A172Tabela_Codigo = P00213_A172Tabela_Codigo[0];
                  A368FuncaoDados_Codigo = P00213_A368FuncaoDados_Codigo[0];
                  /* Using cursor P00214 */
                  pr_default.execute(2, new Object[] {A172Tabela_Codigo});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     A356Atributos_TabelaCod = P00214_A356Atributos_TabelaCod[0];
                     A177Atributos_Nome = P00214_A177Atributos_Nome[0];
                     AV12Exist = false;
                     AV19GXV1 = 1;
                     while ( AV19GXV1 <= AV11Atributos.Count )
                     {
                        AV13Atributos_Nome = AV11Atributos.GetString(AV19GXV1);
                        if ( StringUtil.StrCmp(AV13Atributos_Nome, A177Atributos_Nome) == 0 )
                        {
                           AV12Exist = true;
                           if (true) break;
                        }
                        AV19GXV1 = (int)(AV19GXV1+1);
                     }
                     if ( ! AV12Exist )
                     {
                        AV9FuncaoDados_DER = (short)(AV9FuncaoDados_DER+1);
                        AV11Atributos.Add(A177Atributos_Nome, 0);
                     }
                     pr_default.readNext(2);
                  }
                  pr_default.close(2);
                  pr_default.readNext(1);
               }
               pr_default.close(1);
            }
            else
            {
               AV10FuncaoDados_FuncaoDadosCod = A391FuncaoDados_FuncaoDadosCod;
               /* Execute user subroutine: 'FUNCAOEXTERNA' */
               S111 ();
               if ( returnInSub )
               {
                  pr_default.close(0);
                  this.cleanup();
                  if (true) return;
               }
            }
            if ( (0==AV9FuncaoDados_DER) )
            {
               AV9FuncaoDados_DER = A1024FuncaoDados_DERImp;
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'FUNCAOEXTERNA' Routine */
         /* Using cursor P00215 */
         pr_default.execute(3, new Object[] {AV10FuncaoDados_FuncaoDadosCod});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A368FuncaoDados_Codigo = P00215_A368FuncaoDados_Codigo[0];
            GXt_int1 = A374FuncaoDados_DER;
            new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int1) ;
            A374FuncaoDados_DER = GXt_int1;
            AV9FuncaoDados_DER = A374FuncaoDados_DER;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
         returnInSub = true;
         if (true) return;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00212_A368FuncaoDados_Codigo = new int[1] ;
         P00212_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         P00212_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         P00212_A1024FuncaoDados_DERImp = new short[1] ;
         P00212_n1024FuncaoDados_DERImp = new bool[] {false} ;
         P00213_A172Tabela_Codigo = new int[1] ;
         P00213_A368FuncaoDados_Codigo = new int[1] ;
         P00214_A356Atributos_TabelaCod = new int[1] ;
         P00214_A177Atributos_Nome = new String[] {""} ;
         A177Atributos_Nome = "";
         AV11Atributos = new GxSimpleCollection();
         AV13Atributos_Nome = "";
         P00215_A368FuncaoDados_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_funcaodados_der__default(),
            new Object[][] {
                new Object[] {
               P00212_A368FuncaoDados_Codigo, P00212_A391FuncaoDados_FuncaoDadosCod, P00212_n391FuncaoDados_FuncaoDadosCod, P00212_A1024FuncaoDados_DERImp, P00212_n1024FuncaoDados_DERImp
               }
               , new Object[] {
               P00213_A172Tabela_Codigo, P00213_A368FuncaoDados_Codigo
               }
               , new Object[] {
               P00214_A356Atributos_TabelaCod, P00214_A177Atributos_Nome
               }
               , new Object[] {
               P00215_A368FuncaoDados_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9FuncaoDados_DER ;
      private short A1024FuncaoDados_DERImp ;
      private short A374FuncaoDados_DER ;
      private short GXt_int1 ;
      private int AV8FuncaoDados_Codigo ;
      private int A368FuncaoDados_Codigo ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int A172Tabela_Codigo ;
      private int A356Atributos_TabelaCod ;
      private int AV19GXV1 ;
      private int AV10FuncaoDados_FuncaoDadosCod ;
      private String scmdbuf ;
      private String A177Atributos_Nome ;
      private String AV13Atributos_Nome ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool n1024FuncaoDados_DERImp ;
      private bool AV12Exist ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00212_A368FuncaoDados_Codigo ;
      private int[] P00212_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] P00212_n391FuncaoDados_FuncaoDadosCod ;
      private short[] P00212_A1024FuncaoDados_DERImp ;
      private bool[] P00212_n1024FuncaoDados_DERImp ;
      private int[] P00213_A172Tabela_Codigo ;
      private int[] P00213_A368FuncaoDados_Codigo ;
      private int[] P00214_A356Atributos_TabelaCod ;
      private String[] P00214_A177Atributos_Nome ;
      private int[] P00215_A368FuncaoDados_Codigo ;
      private short aP1_FuncaoDados_DER ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV11Atributos ;
   }

   public class prc_funcaodados_der__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00212 ;
          prmP00212 = new Object[] {
          new Object[] {"@AV8FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00213 ;
          prmP00213 = new Object[] {
          new Object[] {"@AV8FuncaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00214 ;
          prmP00214 = new Object[] {
          new Object[] {"@Tabela_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00215 ;
          prmP00215 = new Object[] {
          new Object[] {"@AV10FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00212", "SELECT [FuncaoDados_Codigo], [FuncaoDados_FuncaoDadosCod], [FuncaoDados_DERImp] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV8FuncaoDados_Codigo ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00212,1,0,true,true )
             ,new CursorDef("P00213", "SELECT [Tabela_Codigo], [FuncaoDados_Codigo] FROM [FuncaoDadosTabela] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV8FuncaoDados_Codigo ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00213,100,0,true,false )
             ,new CursorDef("P00214", "SELECT DISTINCT NULL AS [Atributos_TabelaCod], [Atributos_Nome] FROM ( SELECT TOP(100) PERCENT [Atributos_TabelaCod], [Atributos_Nome] FROM [Atributos] WITH (NOLOCK) WHERE [Atributos_TabelaCod] = @Tabela_Codigo ORDER BY [Atributos_TabelaCod]) DistinctT ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00214,100,0,false,false )
             ,new CursorDef("P00215", "SELECT [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV10FuncaoDados_FuncaoDadosCod ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00215,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
