/*
               File: PRC_AlertaLida
        Description: Alerta Lida
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:12:19.64
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_alertalida : GXProcedure
   {
      public prc_alertalida( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_alertalida( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Alerta_Codigo ,
                           ref int aP1_AlertaRead_UserCod )
      {
         this.AV9Alerta_Codigo = aP0_Alerta_Codigo;
         this.A1890AlertaRead_UserCod = aP1_AlertaRead_UserCod;
         initialize();
         executePrivate();
         aP1_AlertaRead_UserCod=this.A1890AlertaRead_UserCod;
      }

      public int executeUdp( int aP0_Alerta_Codigo )
      {
         this.AV9Alerta_Codigo = aP0_Alerta_Codigo;
         this.A1890AlertaRead_UserCod = aP1_AlertaRead_UserCod;
         initialize();
         executePrivate();
         aP1_AlertaRead_UserCod=this.A1890AlertaRead_UserCod;
         return A1890AlertaRead_UserCod ;
      }

      public void executeSubmit( int aP0_Alerta_Codigo ,
                                 ref int aP1_AlertaRead_UserCod )
      {
         prc_alertalida objprc_alertalida;
         objprc_alertalida = new prc_alertalida();
         objprc_alertalida.AV9Alerta_Codigo = aP0_Alerta_Codigo;
         objprc_alertalida.A1890AlertaRead_UserCod = aP1_AlertaRead_UserCod;
         objprc_alertalida.context.SetSubmitInitialConfig(context);
         objprc_alertalida.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_alertalida);
         aP1_AlertaRead_UserCod=this.A1890AlertaRead_UserCod;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_alertalida)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00TR3 */
         pr_default.execute(0, new Object[] {AV9Alerta_Codigo});
         if ( (pr_default.getStatus(0) != 101) )
         {
            A40000GXC1 = P00TR3_A40000GXC1[0];
         }
         else
         {
            A40000GXC1 = 0;
         }
         pr_default.close(0);
         AV8AlertaRead_Codigo = (int)(A40000GXC1+1);
         /*
            INSERT RECORD ON TABLE AlertaRead

         */
         A1881Alerta_Codigo = AV9Alerta_Codigo;
         A1889AlertaRead_Codigo = AV8AlertaRead_Codigo;
         A1891AlertaRead_DataHora = DateTimeUtil.ServerNow( context, "DEFAULT");
         /* Using cursor P00TR4 */
         pr_default.execute(1, new Object[] {A1881Alerta_Codigo, A1889AlertaRead_Codigo, A1890AlertaRead_UserCod, A1891AlertaRead_DataHora});
         pr_default.close(1);
         dsDefault.SmartCacheProvider.SetUpdated("AlertaRead") ;
         if ( (pr_default.getStatus(1) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_AlertaLida");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00TR3_A40000GXC1 = new int[1] ;
         A1891AlertaRead_DataHora = (DateTime)(DateTime.MinValue);
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_alertalida__default(),
            new Object[][] {
                new Object[] {
               P00TR3_A40000GXC1
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9Alerta_Codigo ;
      private int A1890AlertaRead_UserCod ;
      private int A40000GXC1 ;
      private int AV8AlertaRead_Codigo ;
      private int GX_INS212 ;
      private int A1881Alerta_Codigo ;
      private int A1889AlertaRead_Codigo ;
      private String scmdbuf ;
      private String Gx_emsg ;
      private DateTime A1891AlertaRead_DataHora ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP1_AlertaRead_UserCod ;
      private IDataStoreProvider pr_default ;
      private int[] P00TR3_A40000GXC1 ;
   }

   public class prc_alertalida__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00TR3 ;
          prmP00TR3 = new Object[] {
          new Object[] {"@AV9Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00TR4 ;
          prmP00TR4 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_UserCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AlertaRead_DataHora",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00TR3", "SELECT COALESCE( T1.[GXC1], 0) AS GXC1 FROM (SELECT COUNT(*) AS GXC1 FROM [AlertaRead] WITH (NOLOCK) WHERE [Alerta_Codigo] = @AV9Alerta_Codigo ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00TR3,1,0,true,false )
             ,new CursorDef("P00TR4", "INSERT INTO [AlertaRead]([Alerta_Codigo], [AlertaRead_Codigo], [AlertaRead_UserCod], [AlertaRead_DataHora]) VALUES(@Alerta_Codigo, @AlertaRead_Codigo, @AlertaRead_UserCod, @AlertaRead_DataHora)", GxErrorMask.GX_NOMASK,prmP00TR4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameterDatetime(4, (DateTime)parms[3]);
                return;
       }
    }

 }

}
