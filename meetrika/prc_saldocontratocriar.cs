/*
               File: PRC_SaldoContratoCriar
        Description: PRC_Saldo Contrato Criar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:15:41.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_saldocontratocriar : GXProcedure
   {
      public prc_saldocontratocriar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_saldocontratocriar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo ,
                           int aP1_SaldoContrato_UnidadeMedicao_Codigo ,
                           DateTime aP2_SaldoContrato_VigenciaInicio ,
                           DateTime aP3_SaldoContrato_VigenciaFim ,
                           out int aP4_SaldoContrato_Codigo )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV15SaldoContrato_UnidadeMedicao_Codigo = aP1_SaldoContrato_UnidadeMedicao_Codigo;
         this.AV9SaldoContrato_VigenciaInicio = aP2_SaldoContrato_VigenciaInicio;
         this.AV13SaldoContrato_VigenciaFim = aP3_SaldoContrato_VigenciaFim;
         this.AV12SaldoContrato_Codigo = 0 ;
         initialize();
         executePrivate();
         aP4_SaldoContrato_Codigo=this.AV12SaldoContrato_Codigo;
      }

      public int executeUdp( int aP0_Contrato_Codigo ,
                             int aP1_SaldoContrato_UnidadeMedicao_Codigo ,
                             DateTime aP2_SaldoContrato_VigenciaInicio ,
                             DateTime aP3_SaldoContrato_VigenciaFim )
      {
         this.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV15SaldoContrato_UnidadeMedicao_Codigo = aP1_SaldoContrato_UnidadeMedicao_Codigo;
         this.AV9SaldoContrato_VigenciaInicio = aP2_SaldoContrato_VigenciaInicio;
         this.AV13SaldoContrato_VigenciaFim = aP3_SaldoContrato_VigenciaFim;
         this.AV12SaldoContrato_Codigo = 0 ;
         initialize();
         executePrivate();
         aP4_SaldoContrato_Codigo=this.AV12SaldoContrato_Codigo;
         return AV12SaldoContrato_Codigo ;
      }

      public void executeSubmit( int aP0_Contrato_Codigo ,
                                 int aP1_SaldoContrato_UnidadeMedicao_Codigo ,
                                 DateTime aP2_SaldoContrato_VigenciaInicio ,
                                 DateTime aP3_SaldoContrato_VigenciaFim ,
                                 out int aP4_SaldoContrato_Codigo )
      {
         prc_saldocontratocriar objprc_saldocontratocriar;
         objprc_saldocontratocriar = new prc_saldocontratocriar();
         objprc_saldocontratocriar.AV8Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_saldocontratocriar.AV15SaldoContrato_UnidadeMedicao_Codigo = aP1_SaldoContrato_UnidadeMedicao_Codigo;
         objprc_saldocontratocriar.AV9SaldoContrato_VigenciaInicio = aP2_SaldoContrato_VigenciaInicio;
         objprc_saldocontratocriar.AV13SaldoContrato_VigenciaFim = aP3_SaldoContrato_VigenciaFim;
         objprc_saldocontratocriar.AV12SaldoContrato_Codigo = 0 ;
         objprc_saldocontratocriar.context.SetSubmitInitialConfig(context);
         objprc_saldocontratocriar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_saldocontratocriar);
         aP4_SaldoContrato_Codigo=this.AV12SaldoContrato_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_saldocontratocriar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Optimized UPDATE. */
         /* Using cursor P00BE2 */
         pr_default.execute(0, new Object[] {AV8Contrato_Codigo, AV15SaldoContrato_UnidadeMedicao_Codigo});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("SaldoContrato") ;
         dsDefault.SmartCacheProvider.SetUpdated("SaldoContrato") ;
         /* End optimized UPDATE. */
         /*
            INSERT RECORD ON TABLE SaldoContrato

         */
         A74Contrato_Codigo = AV8Contrato_Codigo;
         A1571SaldoContrato_VigenciaInicio = AV9SaldoContrato_VigenciaInicio;
         A1572SaldoContrato_VigenciaFim = AV13SaldoContrato_VigenciaFim;
         A1573SaldoContrato_Credito = AV11SaldoContrato_Credito;
         A1574SaldoContrato_Reservado = 0;
         A1575SaldoContrato_Executado = 0;
         A1576SaldoContrato_Saldo = 0;
         A1781SaldoContrato_Ativo = true;
         A1783SaldoContrato_UnidadeMedicao_Codigo = AV15SaldoContrato_UnidadeMedicao_Codigo;
         /* Using cursor P00BE3 */
         pr_default.execute(1, new Object[] {A74Contrato_Codigo, A1571SaldoContrato_VigenciaInicio, A1572SaldoContrato_VigenciaFim, A1573SaldoContrato_Credito, A1574SaldoContrato_Reservado, A1575SaldoContrato_Executado, A1576SaldoContrato_Saldo, A1781SaldoContrato_Ativo, A1783SaldoContrato_UnidadeMedicao_Codigo});
         A1561SaldoContrato_Codigo = P00BE3_A1561SaldoContrato_Codigo[0];
         pr_default.close(1);
         dsDefault.SmartCacheProvider.SetUpdated("SaldoContrato") ;
         if ( (pr_default.getStatus(1) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV12SaldoContrato_Codigo = A1561SaldoContrato_Codigo;
         new prc_historicoconsumo(context ).execute(  AV12SaldoContrato_Codigo,  AV8Contrato_Codigo,  0,  0,  0,  AV11SaldoContrato_Credito,  "INI") ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_SaldoContratoCriar");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         A1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         A1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         P00BE3_A1561SaldoContrato_Codigo = new int[1] ;
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_saldocontratocriar__default(),
            new Object[][] {
                new Object[] {
               }
               , new Object[] {
               P00BE3_A1561SaldoContrato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Contrato_Codigo ;
      private int AV15SaldoContrato_UnidadeMedicao_Codigo ;
      private int AV12SaldoContrato_Codigo ;
      private int GX_INS179 ;
      private int A74Contrato_Codigo ;
      private int A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private decimal A1573SaldoContrato_Credito ;
      private decimal AV11SaldoContrato_Credito ;
      private decimal A1574SaldoContrato_Reservado ;
      private decimal A1575SaldoContrato_Executado ;
      private decimal A1576SaldoContrato_Saldo ;
      private String Gx_emsg ;
      private DateTime AV9SaldoContrato_VigenciaInicio ;
      private DateTime AV13SaldoContrato_VigenciaFim ;
      private DateTime A1571SaldoContrato_VigenciaInicio ;
      private DateTime A1572SaldoContrato_VigenciaFim ;
      private bool A1781SaldoContrato_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00BE3_A1561SaldoContrato_Codigo ;
      private int aP4_SaldoContrato_Codigo ;
   }

   public class prc_saldocontratocriar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00BE2 ;
          prmP00BE2 = new Object[] {
          new Object[] {"@AV8Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15SaldoContrato_UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00BE3 ;
          prmP00BE3 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SaldoContrato_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@SaldoContrato_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@SaldoContrato_Credito",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Reservado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Executado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@SaldoContrato_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@SaldoContrato_UnidadeMedicao_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00BE2", "UPDATE [SaldoContrato] SET [SaldoContrato_Ativo]=CONVERT(BIT, 0)  WHERE ([Contrato_Codigo] = @AV8Contrato_Codigo) AND ([SaldoContrato_Ativo] = 1) AND ([SaldoContrato_UnidadeMedicao_Codigo] = @AV15SaldoContrato_UnidadeMedicao_Codigo)", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00BE2)
             ,new CursorDef("P00BE3", "INSERT INTO [SaldoContrato]([Contrato_Codigo], [SaldoContrato_VigenciaInicio], [SaldoContrato_VigenciaFim], [SaldoContrato_Credito], [SaldoContrato_Reservado], [SaldoContrato_Executado], [SaldoContrato_Saldo], [SaldoContrato_Ativo], [SaldoContrato_UnidadeMedicao_Codigo], [SaldoContrato_Estimado]) VALUES(@Contrato_Codigo, @SaldoContrato_VigenciaInicio, @SaldoContrato_VigenciaFim, @SaldoContrato_Credito, @SaldoContrato_Reservado, @SaldoContrato_Executado, @SaldoContrato_Saldo, @SaldoContrato_Ativo, @SaldoContrato_UnidadeMedicao_Codigo, convert(int, 0)); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP00BE3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (decimal)parms[3]);
                stmt.SetParameter(5, (decimal)parms[4]);
                stmt.SetParameter(6, (decimal)parms[5]);
                stmt.SetParameter(7, (decimal)parms[6]);
                stmt.SetParameter(8, (bool)parms[7]);
                stmt.SetParameter(9, (int)parms[8]);
                return;
       }
    }

 }

}
