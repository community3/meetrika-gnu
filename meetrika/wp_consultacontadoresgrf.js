/**@preserve  GeneXus C# 10_3_14-114418 on 3/12/2020 21:14:8.82
*/
gx.evt.autoSkip = false;
gx.define('wp_consultacontadoresgrf', false, function () {
   this.ServerClass =  "wp_consultacontadoresgrf" ;
   this.PackageName =  "GeneXus.Programs" ;
   this.setObjectType("web");
   this.hasEnterEvent = false;
   this.skipOnEnter = false;
   this.autoRefresh = true;
   this.fullAjax = true;
   this.supportAjaxEvents =  true ;
   this.SetStandaloneVars=function()
   {
      this.AV13AreaTrabalho_Codigo=gx.fn.getIntegerValue("vAREATRABALHO_CODIGO",'.') ;
      this.AV5ContagemResultado_ContadorFMCod=gx.fn.getIntegerValue("vCONTAGEMRESULTADO_CONTADORFMCOD",'.') ;
      this.AV6Filtro_Ano=gx.fn.getIntegerValue("vFILTRO_ANO",'.') ;
      this.AV7Filtro_Mes=gx.fn.getIntegerValue("vFILTRO_MES",'.') ;
      this.AV31Graficar=gx.fn.getControlValue("vGRAFICAR") ;
      this.AV32Servico_Codigo=gx.fn.getIntegerValue("vSERVICO_CODIGO",'.') ;
      this.AV33UserEhContratada=gx.fn.getControlValue("vUSEREHCONTRATADA") ;
      this.AV35UserEhContratante=gx.fn.getControlValue("vUSEREHCONTRATANTE") ;
      this.AV41DataCnt=gx.fn.getDateValue("vDATACNT") ;
      this.AV38Contratada_PessoaCod=gx.fn.getIntegerValue("vCONTRATADA_PESSOACOD",'.') ;
   };
   this.e13bv2_client=function()
   {
      this.executeServerEvent("ENTER", true, null, false, false);
   };
   this.e14bv2_client=function()
   {
      this.executeServerEvent("CANCEL", true, null, false, false);
   };
   this.GXValidFnc = [];
   var GXValidFnc = this.GXValidFnc ;
   this.GXCtrlIds=[2];
   this.GXLastCtrlId =2;
   this.GXCHARTCONTROL1Container = gx.uc.getNew(this, 7, 0, "gxChart", "GXCHARTCONTROL1Container", "Gxchartcontrol1");
   var GXCHARTCONTROL1Container = this.GXCHARTCONTROL1Container;
   GXCHARTCONTROL1Container.setDynProp("Title", "Title", "Consulta de Contadores", "str");
   GXCHARTCONTROL1Container.setProp("Type", "Charttype", "BAR", "str");
   GXCHARTCONTROL1Container.setDynProp("X_AxisTitle", "X_axistitle", "Demandas", "str");
   GXCHARTCONTROL1Container.setProp("Y_AxisTitle", "Y_axistitle", "Meses", "str");
   GXCHARTCONTROL1Container.setProp("Width", "Width", "900", "str");
   GXCHARTCONTROL1Container.setProp("Height", "Height", "500", "str");
   GXCHARTCONTROL1Container.setProp("Scale", "Scale", "auto", "str");
   GXCHARTCONTROL1Container.setProp("CustomParameters", "Customparameters", "", "char");
   GXCHARTCONTROL1Container.setProp("ClickedSerieName", "Clickedseriename", "", "char");
   GXCHARTCONTROL1Container.setProp("ClickedCategoryName", "Clickedcategoryname", "", "char");
   GXCHARTCONTROL1Container.setProp("DrawShadows", "Drawshadows", "1", "str");
   GXCHARTCONTROL1Container.setProp("DrawBorder", "Drawborder", "1", "str");
   GXCHARTCONTROL1Container.setProp("ShowValues", "Showvalues", "1", "str");
   GXCHARTCONTROL1Container.setProp("Opacity", "Opacity", 255, "num");
   GXCHARTCONTROL1Container.setProp("LegendPosition", "Legendposition", "left", "str");
   GXCHARTCONTROL1Container.setProp("BackgroundColor1", "Backgroundcolor1", gx.color.fromRGB(211,211,211), "color");
   GXCHARTCONTROL1Container.setProp("BackgroundColor2", "Backgroundcolor2", gx.color.fromRGB(243,243,243), "color");
   GXCHARTCONTROL1Container.setProp("GraphColor1", "Graphcolor1", gx.color.fromRGB(240,255,210), "color");
   GXCHARTCONTROL1Container.setProp("GraphColor2", "Graphcolor2", gx.color.fromRGB(50,50,80), "color");
   GXCHARTCONTROL1Container.setProp("EventType", "Eventtype", "none", "str");
   GXCHARTCONTROL1Container.addV2CFunction('GxChartData', "vGXCHARTDATA", 'SetData');
   GXCHARTCONTROL1Container.addC2VFunction(function(UC) { UC.ParentObject.GxChartData=UC.GetData();gx.fn.setControlValue("vGXCHARTDATA",UC.ParentObject.GxChartData); });
   GXCHARTCONTROL1Container.setProp("ServiceUrl", "Serviceurl", "http://www.gxchart.com/service/", "str");
   GXCHARTCONTROL1Container.setProp("ServiceName", "Servicename", "drawchart.aspx", "str");
   GXCHARTCONTROL1Container.setProp("Visible", "Visible", true, "bool");
   GXCHARTCONTROL1Container.setProp("Enabled", "Enabled", true, "boolean");
   GXCHARTCONTROL1Container.setProp("Class", "Class", "", "char");
   GXCHARTCONTROL1Container.setC2ShowFunction(function(UC) { UC.show(); });
   this.setUserControl(GXCHARTCONTROL1Container);
   GXValidFnc[2]={fld:"TABLE1",grid:0};
   this.GxChartData = {} ;
   this.AV13AreaTrabalho_Codigo = 0 ;
   this.AV5ContagemResultado_ContadorFMCod = 0 ;
   this.AV6Filtro_Ano = 0 ;
   this.AV7Filtro_Mes = 0 ;
   this.AV31Graficar = "" ;
   this.AV32Servico_Codigo = 0 ;
   this.AV33UserEhContratada = false ;
   this.AV35UserEhContratante = false ;
   this.AV41DataCnt = gx.date.nullDate() ;
   this.AV38Contratada_PessoaCod = 0 ;
   this.A470ContagemResultado_ContadorFMCod = 0 ;
   this.A474ContagemResultado_ContadorFMNom = "" ;
   this.A473ContagemResultado_DataCnt = gx.date.nullDate() ;
   this.A458ContagemResultado_PFBFS = 0 ;
   this.A460ContagemResultado_PFBFM = 0 ;
   this.A468ContagemResultado_NaoCnfDmnCod = 0 ;
   this.A469ContagemResultado_NaoCnfCntCod = 0 ;
   this.A462ContagemResultado_Divergencia = 0 ;
   this.A490ContagemResultado_ContratadaCod = 0 ;
   this.A484ContagemResultado_StatusDmn = "" ;
   this.A517ContagemResultado_Ultima = false ;
   this.A601ContagemResultado_Servico = 0 ;
   this.A1000ContagemResultado_CrFMEhContratante = false ;
   this.A999ContagemResultado_CrFMEhContratada = false ;
   this.A456ContagemResultado_Codigo = 0 ;
   this.A1553ContagemResultado_CntSrvCod = 0 ;
   this.A479ContagemResultado_CrFMPessoaCod = 0 ;
   this.Events = {"e13bv2_client": ["ENTER", true] ,"e14bv2_client": ["CANCEL", true]};
   this.EvtParms["REFRESH"] = [[],[]];
   this.setVCMap("A490ContagemResultado_ContratadaCod", "CONTAGEMRESULTADO_CONTRATADACOD", 0, "int");
   this.setVCMap("AV13AreaTrabalho_Codigo", "vAREATRABALHO_CODIGO", 0, "int");
   this.setVCMap("AV5ContagemResultado_ContadorFMCod", "vCONTAGEMRESULTADO_CONTADORFMCOD", 0, "int");
   this.setVCMap("AV6Filtro_Ano", "vFILTRO_ANO", 0, "int");
   this.setVCMap("AV7Filtro_Mes", "vFILTRO_MES", 0, "int");
   this.setVCMap("AV31Graficar", "vGRAFICAR", 0, "char");
   this.setVCMap("AV32Servico_Codigo", "vSERVICO_CODIGO", 0, "int");
   this.setVCMap("AV33UserEhContratada", "vUSEREHCONTRATADA", 0, "boolean");
   this.setVCMap("AV35UserEhContratante", "vUSEREHCONTRATANTE", 0, "boolean");
   this.setVCMap("AV41DataCnt", "vDATACNT", 0, "date");
   this.setVCMap("AV38Contratada_PessoaCod", "vCONTRATADA_PESSOACOD", 0, "int");
   this.InitStandaloneVars( );
});
gx.createParentObj(wp_consultacontadoresgrf);
