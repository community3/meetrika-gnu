/*
               File: WWFuncaoDados
        Description:  Grupo L�gico de Dados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:38:43.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwfuncaodados : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwfuncaodados( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwfuncaodados( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbFuncaoDados_Tipo = new GXCombobox();
         cmbFuncaoDados_Complexidade = new GXCombobox();
         cmbFuncaoDados_Ativo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_94 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_94_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_94_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV35OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
               AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
               AV14FuncaoDados_SistemaAreaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14FuncaoDados_SistemaAreaCod), 6, 0)));
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17FuncaoDados_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoDados_Nome1", AV17FuncaoDados_Nome1);
               AV18FuncaoDados_SistemaDes1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoDados_SistemaDes1", AV18FuncaoDados_SistemaDes1);
               AV20DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
               AV22FuncaoDados_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22FuncaoDados_Nome2", AV22FuncaoDados_Nome2);
               AV23FuncaoDados_SistemaDes2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoDados_SistemaDes2", AV23FuncaoDados_SistemaDes2);
               AV25DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
               AV27FuncaoDados_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27FuncaoDados_Nome3", AV27FuncaoDados_Nome3);
               AV28FuncaoDados_SistemaDes3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoDados_SistemaDes3", AV28FuncaoDados_SistemaDes3);
               AV19DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV24DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
               AV39TFFuncaoDados_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoDados_Nome", AV39TFFuncaoDados_Nome);
               AV40TFFuncaoDados_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoDados_Nome_Sel", AV40TFFuncaoDados_Nome_Sel);
               AV41ddo_FuncaoDados_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_FuncaoDados_NomeTitleControlIdToReplace", AV41ddo_FuncaoDados_NomeTitleControlIdToReplace);
               AV45ddo_FuncaoDados_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_FuncaoDados_TipoTitleControlIdToReplace", AV45ddo_FuncaoDados_TipoTitleControlIdToReplace);
               AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
               AV53ddo_FuncaoDados_DERTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_FuncaoDados_DERTitleControlIdToReplace", AV53ddo_FuncaoDados_DERTitleControlIdToReplace);
               AV57ddo_FuncaoDados_RLRTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_FuncaoDados_RLRTitleControlIdToReplace", AV57ddo_FuncaoDados_RLRTitleControlIdToReplace);
               AV61ddo_FuncaoDados_PFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_FuncaoDados_PFTitleControlIdToReplace", AV61ddo_FuncaoDados_PFTitleControlIdToReplace);
               AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace", AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV44TFFuncaoDados_Tipo_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV48TFFuncaoDados_Complexidade_Sels);
               AV51TFFuncaoDados_DER = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0)));
               AV52TFFuncaoDados_DER_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0)));
               AV55TFFuncaoDados_RLR = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0)));
               AV56TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0)));
               AV59TFFuncaoDados_PF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5)));
               AV60TFFuncaoDados_PF_To = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV64TFFuncaoDados_Ativo_Sels);
               AV100Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV30DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
               AV29DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
               A368FuncaoDados_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV34FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A370FuncaoDados_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A370FuncaoDados_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A370FuncaoDados_SistemaCod), 6, 0)));
               A391FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n391FuncaoDados_FuncaoDadosCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV35OrderedBy, AV13OrderedDsc, AV14FuncaoDados_SistemaAreaCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoDados_Nome1, AV18FuncaoDados_SistemaDes1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22FuncaoDados_Nome2, AV23FuncaoDados_SistemaDes2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27FuncaoDados_Nome3, AV28FuncaoDados_SistemaDes3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV64TFFuncaoDados_Ativo_Sels, AV100Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A368FuncaoDados_Codigo, AV34FuncaoDados_SistemaCod, A370FuncaoDados_SistemaCod, A391FuncaoDados_FuncaoDadosCod) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA9B2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START9B2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117384378");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwfuncaodados.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAODADOS_SISTEMAAREACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14FuncaoDados_SistemaAreaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAODADOS_NOME1", AV17FuncaoDados_Nome1);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAODADOS_SISTEMADES1", AV18FuncaoDados_SistemaDes1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV20DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAODADOS_NOME2", AV22FuncaoDados_Nome2);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAODADOS_SISTEMADES2", AV23FuncaoDados_SistemaDes2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV25DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAODADOS_NOME3", AV27FuncaoDados_Nome3);
         GxWebStd.gx_hidden_field( context, "GXH_vFUNCAODADOS_SISTEMADES3", AV28FuncaoDados_SistemaDes3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV19DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV24DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAODADOS_NOME", AV39TFFuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "GXH_vTFFUNCAODADOS_NOME_SEL", AV40TFFuncaoDados_Nome_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_94", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_94), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV68GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV69GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV66DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV66DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_NOMETITLEFILTERDATA", AV38FuncaoDados_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_NOMETITLEFILTERDATA", AV38FuncaoDados_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_TIPOTITLEFILTERDATA", AV42FuncaoDados_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_TIPOTITLEFILTERDATA", AV42FuncaoDados_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA", AV46FuncaoDados_ComplexidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA", AV46FuncaoDados_ComplexidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_DERTITLEFILTERDATA", AV50FuncaoDados_DERTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_DERTITLEFILTERDATA", AV50FuncaoDados_DERTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_RLRTITLEFILTERDATA", AV54FuncaoDados_RLRTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_RLRTITLEFILTERDATA", AV54FuncaoDados_RLRTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_PFTITLEFILTERDATA", AV58FuncaoDados_PFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_PFTITLEFILTERDATA", AV58FuncaoDados_PFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vFUNCAODADOS_ATIVOTITLEFILTERDATA", AV62FuncaoDados_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vFUNCAODADOS_ATIVOTITLEFILTERDATA", AV62FuncaoDados_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAODADOS_TIPO_SELS", AV44TFFuncaoDados_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAODADOS_TIPO_SELS", AV44TFFuncaoDados_Tipo_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAODADOS_COMPLEXIDADE_SELS", AV48TFFuncaoDados_Complexidade_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAODADOS_COMPLEXIDADE_SELS", AV48TFFuncaoDados_Complexidade_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFFUNCAODADOS_ATIVO_SELS", AV64TFFuncaoDados_Ativo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFFUNCAODADOS_ATIVO_SELS", AV64TFFuncaoDados_Ativo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV100Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV30DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV29DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vFUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A370FuncaoDados_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "FUNCAODADOS_CONTAR", A755FuncaoDados_Contar);
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Caption", StringUtil.RTrim( Ddo_funcaodados_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Tooltip", StringUtil.RTrim( Ddo_funcaodados_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Cls", StringUtil.RTrim( Ddo_funcaodados_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Sortedstatus", StringUtil.RTrim( Ddo_funcaodados_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Filtertype", StringUtil.RTrim( Ddo_funcaodados_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Datalistproc", StringUtil.RTrim( Ddo_funcaodados_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaodados_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Sortasc", StringUtil.RTrim( Ddo_funcaodados_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Loadingdata", StringUtil.RTrim( Ddo_funcaodados_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Noresultsfound", StringUtil.RTrim( Ddo_funcaodados_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Caption", StringUtil.RTrim( Ddo_funcaodados_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Tooltip", StringUtil.RTrim( Ddo_funcaodados_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Cls", StringUtil.RTrim( Ddo_funcaodados_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_funcaodados_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaodados_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Sortasc", StringUtil.RTrim( Ddo_funcaodados_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Caption", StringUtil.RTrim( Ddo_funcaodados_complexidade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Tooltip", StringUtil.RTrim( Ddo_funcaodados_complexidade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Cls", StringUtil.RTrim( Ddo_funcaodados_complexidade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_complexidade_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_complexidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_complexidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_complexidade_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaodados_complexidade_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_complexidade_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_complexidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_complexidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Caption", StringUtil.RTrim( Ddo_funcaodados_der_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Tooltip", StringUtil.RTrim( Ddo_funcaodados_der_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Cls", StringUtil.RTrim( Ddo_funcaodados_der_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_der_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_der_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_der_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_der_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_der_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filtertype", StringUtil.RTrim( Ddo_funcaodados_der_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_der_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_der_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_der_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_der_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_der_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_der_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Caption", StringUtil.RTrim( Ddo_funcaodados_rlr_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Tooltip", StringUtil.RTrim( Ddo_funcaodados_rlr_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Cls", StringUtil.RTrim( Ddo_funcaodados_rlr_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_rlr_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_rlr_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filtertype", StringUtil.RTrim( Ddo_funcaodados_rlr_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_rlr_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_rlr_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_rlr_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_rlr_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_rlr_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Caption", StringUtil.RTrim( Ddo_funcaodados_pf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Tooltip", StringUtil.RTrim( Ddo_funcaodados_pf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Cls", StringUtil.RTrim( Ddo_funcaodados_pf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filteredtext_set", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filteredtextto_set", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_pf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_pf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filtertype", StringUtil.RTrim( Ddo_funcaodados_pf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filterisrange", StringUtil.BoolToStr( Ddo_funcaodados_pf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_pf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_pf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaodados_pf_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Rangefilterto", StringUtil.RTrim( Ddo_funcaodados_pf_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_pf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Caption", StringUtil.RTrim( Ddo_funcaodados_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Tooltip", StringUtil.RTrim( Ddo_funcaodados_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Cls", StringUtil.RTrim( Ddo_funcaodados_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaodados_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaodados_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaodados_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_funcaodados_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_funcaodados_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_funcaodados_ativo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaodados_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Sortasc", StringUtil.RTrim( Ddo_funcaodados_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_funcaodados_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_funcaodados_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_funcaodados_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_complexidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_complexidade_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_der_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_DER_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_der_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_rlr_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_RLR_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_rlr_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_pf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filteredtext_get", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_PF_Filteredtextto_get", StringUtil.RTrim( Ddo_funcaodados_pf_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_funcaodados_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_FUNCAODADOS_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaodados_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE9B2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT9B2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwfuncaodados.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWFuncaoDados" ;
      }

      public override String GetPgmdesc( )
      {
         return " Grupo L�gico de Dados" ;
      }

      protected void WB9B0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_9B2( true) ;
         }
         else
         {
            wb_table1_2_9B2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9B2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV19DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV24DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(110, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaodados_nome_Internalname, AV39TFFuncaoDados_Nome, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", 0, edtavTffuncaodados_nome_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaodados_nome_sel_Internalname, AV40TFFuncaoDados_Nome_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,112);\"", 0, edtavTffuncaodados_nome_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_der_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51TFFuncaoDados_DER), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51TFFuncaoDados_DER), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_der_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_der_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_der_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV52TFFuncaoDados_DER_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,114);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_der_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_der_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_rlr_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFFuncaoDados_RLR), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFFuncaoDados_RLR), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_rlr_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_rlr_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_rlr_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV56TFFuncaoDados_RLR_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_rlr_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_rlr_to_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_pf_Internalname, StringUtil.LTrim( StringUtil.NToC( AV59TFFuncaoDados_PF, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV59TFFuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_pf_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_pf_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaodados_pf_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV60TFFuncaoDados_PF_To, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV60TFFuncaoDados_PF_To, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaodados_pf_to_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaodados_pf_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", 0, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,122);\"", 0, edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_COMPLEXIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"", 0, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_DERContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", 0, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_RLRContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", 0, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_PFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,130);\"", 0, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_FUNCAODADOS_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,132);\"", 0, edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
         }
         wbLoad = true;
      }

      protected void START9B2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Grupo L�gico de Dados", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP9B0( ) ;
      }

      protected void WS9B2( )
      {
         START9B2( ) ;
         EVT9B2( ) ;
      }

      protected void EVT9B2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E119B2 */
                              E119B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E129B2 */
                              E129B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E139B2 */
                              E139B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_COMPLEXIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E149B2 */
                              E149B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_DER.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E159B2 */
                              E159B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_RLR.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E169B2 */
                              E169B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_PF.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E179B2 */
                              E179B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAODADOS_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E189B2 */
                              E189B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E199B2 */
                              E199B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E209B2 */
                              E209B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E219B2 */
                              E219B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E229B2 */
                              E229B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E239B2 */
                              E239B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E249B2 */
                              E249B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E259B2 */
                              E259B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E269B2 */
                              E269B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E279B2 */
                              E279B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E289B2 */
                              E289B2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_94_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
                              SubsflControlProps_942( ) ;
                              AV31Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV98Update_GXI : context.convertURL( context.PathToRelativeUrl( AV31Update))));
                              AV32Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV99Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV32Delete))));
                              A368FuncaoDados_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoDados_Codigo_Internalname), ",", "."));
                              A369FuncaoDados_Nome = cgiGet( edtFuncaoDados_Nome_Internalname);
                              cmbFuncaoDados_Tipo.Name = cmbFuncaoDados_Tipo_Internalname;
                              cmbFuncaoDados_Tipo.CurrentValue = cgiGet( cmbFuncaoDados_Tipo_Internalname);
                              A373FuncaoDados_Tipo = cgiGet( cmbFuncaoDados_Tipo_Internalname);
                              cmbFuncaoDados_Complexidade.Name = cmbFuncaoDados_Complexidade_Internalname;
                              cmbFuncaoDados_Complexidade.CurrentValue = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
                              A376FuncaoDados_Complexidade = cgiGet( cmbFuncaoDados_Complexidade_Internalname);
                              A374FuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_DER_Internalname), ",", "."));
                              A375FuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoDados_RLR_Internalname), ",", "."));
                              A377FuncaoDados_PF = context.localUtil.CToN( cgiGet( edtFuncaoDados_PF_Internalname), ",", ".");
                              cmbFuncaoDados_Ativo.Name = cmbFuncaoDados_Ativo_Internalname;
                              cmbFuncaoDados_Ativo.CurrentValue = cgiGet( cmbFuncaoDados_Ativo_Internalname);
                              A394FuncaoDados_Ativo = cgiGet( cmbFuncaoDados_Ativo_Internalname);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E299B2 */
                                    E299B2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E309B2 */
                                    E309B2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E319B2 */
                                    E319B2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV35OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaodados_sistemaareacod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vFUNCAODADOS_SISTEMAAREACOD"), ",", ".") != Convert.ToDecimal( AV14FuncaoDados_SistemaAreaCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaodados_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_NOME1"), AV17FuncaoDados_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaodados_sistemades1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_SISTEMADES1"), AV18FuncaoDados_SistemaDes1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaodados_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_NOME2"), AV22FuncaoDados_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaodados_sistemades2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_SISTEMADES2"), AV23FuncaoDados_SistemaDes2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaodados_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_NOME3"), AV27FuncaoDados_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Funcaodados_sistemades3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_SISTEMADES3"), AV28FuncaoDados_SistemaDes3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaodados_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAODADOS_NOME"), AV39TFFuncaoDados_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tffuncaodados_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAODADOS_NOME_SEL"), AV40TFFuncaoDados_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9B2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA9B2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV35OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAODADOS_NOME", "Dados", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAODADOS_SISTEMADES", "Sistema", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("FUNCAODADOS_NOME", "Dados", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAODADOS_SISTEMADES", "Sistema", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("FUNCAODADOS_NOME", "Dados", 0);
            cmbavDynamicfiltersselector3.addItem("FUNCAODADOS_SISTEMADES", "Sistema", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            }
            GXCCtl = "FUNCAODADOS_TIPO_" + sGXsfl_94_idx;
            cmbFuncaoDados_Tipo.Name = GXCCtl;
            cmbFuncaoDados_Tipo.WebTags = "";
            cmbFuncaoDados_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoDados_Tipo.addItem("ALI", "ALI", 0);
            cmbFuncaoDados_Tipo.addItem("AIE", "AIE", 0);
            cmbFuncaoDados_Tipo.addItem("DC", "DC", 0);
            if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
            {
               A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
            }
            GXCCtl = "FUNCAODADOS_COMPLEXIDADE_" + sGXsfl_94_idx;
            cmbFuncaoDados_Complexidade.Name = GXCCtl;
            cmbFuncaoDados_Complexidade.WebTags = "";
            cmbFuncaoDados_Complexidade.addItem("E", "", 0);
            cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
            {
               A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            }
            GXCCtl = "FUNCAODADOS_ATIVO_" + sGXsfl_94_idx;
            cmbFuncaoDados_Ativo.Name = GXCCtl;
            cmbFuncaoDados_Ativo.WebTags = "";
            cmbFuncaoDados_Ativo.addItem("A", "Ativa", 0);
            cmbFuncaoDados_Ativo.addItem("E", "Exclu�da", 0);
            cmbFuncaoDados_Ativo.addItem("R", "Rejeitada", 0);
            if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
            {
               A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_942( ) ;
         while ( nGXsfl_94_idx <= nRC_GXsfl_94 )
         {
            sendrow_942( ) ;
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV35OrderedBy ,
                                       bool AV13OrderedDsc ,
                                       int AV14FuncaoDados_SistemaAreaCod ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17FuncaoDados_Nome1 ,
                                       String AV18FuncaoDados_SistemaDes1 ,
                                       String AV20DynamicFiltersSelector2 ,
                                       short AV21DynamicFiltersOperator2 ,
                                       String AV22FuncaoDados_Nome2 ,
                                       String AV23FuncaoDados_SistemaDes2 ,
                                       String AV25DynamicFiltersSelector3 ,
                                       short AV26DynamicFiltersOperator3 ,
                                       String AV27FuncaoDados_Nome3 ,
                                       String AV28FuncaoDados_SistemaDes3 ,
                                       bool AV19DynamicFiltersEnabled2 ,
                                       bool AV24DynamicFiltersEnabled3 ,
                                       String AV39TFFuncaoDados_Nome ,
                                       String AV40TFFuncaoDados_Nome_Sel ,
                                       String AV41ddo_FuncaoDados_NomeTitleControlIdToReplace ,
                                       String AV45ddo_FuncaoDados_TipoTitleControlIdToReplace ,
                                       String AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace ,
                                       String AV53ddo_FuncaoDados_DERTitleControlIdToReplace ,
                                       String AV57ddo_FuncaoDados_RLRTitleControlIdToReplace ,
                                       String AV61ddo_FuncaoDados_PFTitleControlIdToReplace ,
                                       String AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace ,
                                       IGxCollection AV44TFFuncaoDados_Tipo_Sels ,
                                       IGxCollection AV48TFFuncaoDados_Complexidade_Sels ,
                                       short AV51TFFuncaoDados_DER ,
                                       short AV52TFFuncaoDados_DER_To ,
                                       short AV55TFFuncaoDados_RLR ,
                                       short AV56TFFuncaoDados_RLR_To ,
                                       decimal AV59TFFuncaoDados_PF ,
                                       decimal AV60TFFuncaoDados_PF_To ,
                                       IGxCollection AV64TFFuncaoDados_Ativo_Sels ,
                                       String AV100Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV30DynamicFiltersIgnoreFirst ,
                                       bool AV29DynamicFiltersRemoving ,
                                       int A368FuncaoDados_Codigo ,
                                       int AV34FuncaoDados_SistemaCod ,
                                       int A370FuncaoDados_SistemaCod ,
                                       int A391FuncaoDados_FuncaoDadosCod )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF9B2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A369FuncaoDados_Nome, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_NOME", A369FuncaoDados_Nome);
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_TIPO", StringUtil.RTrim( A373FuncaoDados_Tipo));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_COMPLEXIDADE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_COMPLEXIDADE", StringUtil.RTrim( A376FuncaoDados_Complexidade));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_DER", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_DER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_RLR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_RLR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_PF", GetSecureSignedToken( "", context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_PF", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_ATIVO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A394FuncaoDados_Ativo, ""))));
         GxWebStd.gx_hidden_field( context, "FUNCAODADOS_ATIVO", StringUtil.RTrim( A394FuncaoDados_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV35OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV20DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9B2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV100Pgmname = "WWFuncaoDados";
         context.Gx_err = 0;
      }

      protected void RF9B2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 94;
         /* Execute user event: E309B2 */
         E309B2 ();
         nGXsfl_94_idx = 1;
         sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
         SubsflControlProps_942( ) ;
         nGXsfl_94_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_942( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A376FuncaoDados_Complexidade ,
                                                 AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels ,
                                                 A373FuncaoDados_Tipo ,
                                                 AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels ,
                                                 A394FuncaoDados_Ativo ,
                                                 AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels ,
                                                 AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod ,
                                                 AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1 ,
                                                 AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 ,
                                                 AV75WWFuncaoDadosDS_4_Funcaodados_nome1 ,
                                                 AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 ,
                                                 AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 ,
                                                 AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2 ,
                                                 AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 ,
                                                 AV80WWFuncaoDadosDS_9_Funcaodados_nome2 ,
                                                 AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 ,
                                                 AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 ,
                                                 AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3 ,
                                                 AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 ,
                                                 AV85WWFuncaoDadosDS_14_Funcaodados_nome3 ,
                                                 AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 ,
                                                 AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel ,
                                                 AV87WWFuncaoDadosDS_16_Tffuncaodados_nome ,
                                                 AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels.Count ,
                                                 AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels.Count ,
                                                 A372FuncaoDados_SistemaAreaCod ,
                                                 A369FuncaoDados_Nome ,
                                                 A371FuncaoDados_SistemaDes ,
                                                 AV35OrderedBy ,
                                                 AV13OrderedDsc ,
                                                 AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels.Count ,
                                                 AV91WWFuncaoDadosDS_20_Tffuncaodados_der ,
                                                 A374FuncaoDados_DER ,
                                                 AV92WWFuncaoDadosDS_21_Tffuncaodados_der_to ,
                                                 AV93WWFuncaoDadosDS_22_Tffuncaodados_rlr ,
                                                 A375FuncaoDados_RLR ,
                                                 AV94WWFuncaoDadosDS_23_Tffuncaodados_rlr_to ,
                                                 AV95WWFuncaoDadosDS_24_Tffuncaodados_pf ,
                                                 A377FuncaoDados_PF ,
                                                 AV96WWFuncaoDadosDS_25_Tffuncaodados_pf_to },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                                 }
            });
            lV75WWFuncaoDadosDS_4_Funcaodados_nome1 = StringUtil.Concat( StringUtil.RTrim( AV75WWFuncaoDadosDS_4_Funcaodados_nome1), "%", "");
            lV75WWFuncaoDadosDS_4_Funcaodados_nome1 = StringUtil.Concat( StringUtil.RTrim( AV75WWFuncaoDadosDS_4_Funcaodados_nome1), "%", "");
            lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 = StringUtil.Concat( StringUtil.RTrim( AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1), "%", "");
            lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 = StringUtil.Concat( StringUtil.RTrim( AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1), "%", "");
            lV80WWFuncaoDadosDS_9_Funcaodados_nome2 = StringUtil.Concat( StringUtil.RTrim( AV80WWFuncaoDadosDS_9_Funcaodados_nome2), "%", "");
            lV80WWFuncaoDadosDS_9_Funcaodados_nome2 = StringUtil.Concat( StringUtil.RTrim( AV80WWFuncaoDadosDS_9_Funcaodados_nome2), "%", "");
            lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 = StringUtil.Concat( StringUtil.RTrim( AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2), "%", "");
            lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 = StringUtil.Concat( StringUtil.RTrim( AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2), "%", "");
            lV85WWFuncaoDadosDS_14_Funcaodados_nome3 = StringUtil.Concat( StringUtil.RTrim( AV85WWFuncaoDadosDS_14_Funcaodados_nome3), "%", "");
            lV85WWFuncaoDadosDS_14_Funcaodados_nome3 = StringUtil.Concat( StringUtil.RTrim( AV85WWFuncaoDadosDS_14_Funcaodados_nome3), "%", "");
            lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 = StringUtil.Concat( StringUtil.RTrim( AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3), "%", "");
            lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 = StringUtil.Concat( StringUtil.RTrim( AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3), "%", "");
            lV87WWFuncaoDadosDS_16_Tffuncaodados_nome = StringUtil.Concat( StringUtil.RTrim( AV87WWFuncaoDadosDS_16_Tffuncaodados_nome), "%", "");
            /* Using cursor H009B2 */
            pr_default.execute(0, new Object[] {AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels.Count, AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod, lV75WWFuncaoDadosDS_4_Funcaodados_nome1, lV75WWFuncaoDadosDS_4_Funcaodados_nome1, lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1, lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1, lV80WWFuncaoDadosDS_9_Funcaodados_nome2, lV80WWFuncaoDadosDS_9_Funcaodados_nome2, lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2, lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2, lV85WWFuncaoDadosDS_14_Funcaodados_nome3, lV85WWFuncaoDadosDS_14_Funcaodados_nome3, lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3, lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3, lV87WWFuncaoDadosDS_16_Tffuncaodados_nome, AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel});
            nGXsfl_94_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A371FuncaoDados_SistemaDes = H009B2_A371FuncaoDados_SistemaDes[0];
               n371FuncaoDados_SistemaDes = H009B2_n371FuncaoDados_SistemaDes[0];
               A372FuncaoDados_SistemaAreaCod = H009B2_A372FuncaoDados_SistemaAreaCod[0];
               n372FuncaoDados_SistemaAreaCod = H009B2_n372FuncaoDados_SistemaAreaCod[0];
               A370FuncaoDados_SistemaCod = H009B2_A370FuncaoDados_SistemaCod[0];
               A391FuncaoDados_FuncaoDadosCod = H009B2_A391FuncaoDados_FuncaoDadosCod[0];
               n391FuncaoDados_FuncaoDadosCod = H009B2_n391FuncaoDados_FuncaoDadosCod[0];
               A394FuncaoDados_Ativo = H009B2_A394FuncaoDados_Ativo[0];
               A373FuncaoDados_Tipo = H009B2_A373FuncaoDados_Tipo[0];
               A369FuncaoDados_Nome = H009B2_A369FuncaoDados_Nome[0];
               A755FuncaoDados_Contar = H009B2_A755FuncaoDados_Contar[0];
               n755FuncaoDados_Contar = H009B2_n755FuncaoDados_Contar[0];
               A368FuncaoDados_Codigo = H009B2_A368FuncaoDados_Codigo[0];
               A371FuncaoDados_SistemaDes = H009B2_A371FuncaoDados_SistemaDes[0];
               n371FuncaoDados_SistemaDes = H009B2_n371FuncaoDados_SistemaDes[0];
               A372FuncaoDados_SistemaAreaCod = H009B2_A372FuncaoDados_SistemaAreaCod[0];
               n372FuncaoDados_SistemaAreaCod = H009B2_n372FuncaoDados_SistemaAreaCod[0];
               GXt_char1 = A376FuncaoDados_Complexidade;
               new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
               A376FuncaoDados_Complexidade = GXt_char1;
               if ( ( AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels.Count <= 0 ) || ( (AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels.IndexOf(StringUtil.RTrim( A376FuncaoDados_Complexidade))>0) ) )
               {
                  GXt_int2 = A374FuncaoDados_DER;
                  new prc_funcaodados_der(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
                  A374FuncaoDados_DER = GXt_int2;
                  if ( (0==AV91WWFuncaoDadosDS_20_Tffuncaodados_der) || ( ( A374FuncaoDados_DER >= AV91WWFuncaoDadosDS_20_Tffuncaodados_der ) ) )
                  {
                     if ( (0==AV92WWFuncaoDadosDS_21_Tffuncaodados_der_to) || ( ( A374FuncaoDados_DER <= AV92WWFuncaoDadosDS_21_Tffuncaodados_der_to ) ) )
                     {
                        GXt_int2 = A375FuncaoDados_RLR;
                        new prc_funcaodados_rlr(context ).execute(  A368FuncaoDados_Codigo, out  GXt_int2) ;
                        A375FuncaoDados_RLR = GXt_int2;
                        if ( (0==AV93WWFuncaoDadosDS_22_Tffuncaodados_rlr) || ( ( A375FuncaoDados_RLR >= AV93WWFuncaoDadosDS_22_Tffuncaodados_rlr ) ) )
                        {
                           if ( (0==AV94WWFuncaoDadosDS_23_Tffuncaodados_rlr_to) || ( ( A375FuncaoDados_RLR <= AV94WWFuncaoDadosDS_23_Tffuncaodados_rlr_to ) ) )
                           {
                              if ( A755FuncaoDados_Contar )
                              {
                                 GXt_int2 = (short)(A377FuncaoDados_PF);
                                 new prc_fdpf(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_int2) ;
                                 A377FuncaoDados_PF = (decimal)(GXt_int2);
                              }
                              else
                              {
                                 A377FuncaoDados_PF = 0;
                              }
                              if ( (Convert.ToDecimal(0)==AV95WWFuncaoDadosDS_24_Tffuncaodados_pf) || ( ( A377FuncaoDados_PF >= AV95WWFuncaoDadosDS_24_Tffuncaodados_pf ) ) )
                              {
                                 if ( (Convert.ToDecimal(0)==AV96WWFuncaoDadosDS_25_Tffuncaodados_pf_to) || ( ( A377FuncaoDados_PF <= AV96WWFuncaoDadosDS_25_Tffuncaodados_pf_to ) ) )
                                 {
                                    /* Execute user event: E319B2 */
                                    E319B2 ();
                                 }
                              }
                           }
                        }
                     }
                  }
               }
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 94;
            WB9B0( ) ;
         }
         nGXsfl_94_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod = AV14FuncaoDados_SistemaAreaCod;
         AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV75WWFuncaoDadosDS_4_Funcaodados_nome1 = AV17FuncaoDados_Nome1;
         AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 = AV18FuncaoDados_SistemaDes1;
         AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV80WWFuncaoDadosDS_9_Funcaodados_nome2 = AV22FuncaoDados_Nome2;
         AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 = AV23FuncaoDados_SistemaDes2;
         AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV85WWFuncaoDadosDS_14_Funcaodados_nome3 = AV27FuncaoDados_Nome3;
         AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 = AV28FuncaoDados_SistemaDes3;
         AV87WWFuncaoDadosDS_16_Tffuncaodados_nome = AV39TFFuncaoDados_Nome;
         AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel = AV40TFFuncaoDados_Nome_Sel;
         AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels = AV44TFFuncaoDados_Tipo_Sels;
         AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels = AV48TFFuncaoDados_Complexidade_Sels;
         AV91WWFuncaoDadosDS_20_Tffuncaodados_der = AV51TFFuncaoDados_DER;
         AV92WWFuncaoDadosDS_21_Tffuncaodados_der_to = AV52TFFuncaoDados_DER_To;
         AV93WWFuncaoDadosDS_22_Tffuncaodados_rlr = AV55TFFuncaoDados_RLR;
         AV94WWFuncaoDadosDS_23_Tffuncaodados_rlr_to = AV56TFFuncaoDados_RLR_To;
         AV95WWFuncaoDadosDS_24_Tffuncaodados_pf = AV59TFFuncaoDados_PF;
         AV96WWFuncaoDadosDS_25_Tffuncaodados_pf_to = AV60TFFuncaoDados_PF_To;
         AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels = AV64TFFuncaoDados_Ativo_Sels;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV35OrderedBy, AV13OrderedDsc, AV14FuncaoDados_SistemaAreaCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoDados_Nome1, AV18FuncaoDados_SistemaDes1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22FuncaoDados_Nome2, AV23FuncaoDados_SistemaDes2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27FuncaoDados_Nome3, AV28FuncaoDados_SistemaDes3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV64TFFuncaoDados_Ativo_Sels, AV100Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A368FuncaoDados_Codigo, AV34FuncaoDados_SistemaCod, A370FuncaoDados_SistemaCod, A391FuncaoDados_FuncaoDadosCod) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod = AV14FuncaoDados_SistemaAreaCod;
         AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV75WWFuncaoDadosDS_4_Funcaodados_nome1 = AV17FuncaoDados_Nome1;
         AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 = AV18FuncaoDados_SistemaDes1;
         AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV80WWFuncaoDadosDS_9_Funcaodados_nome2 = AV22FuncaoDados_Nome2;
         AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 = AV23FuncaoDados_SistemaDes2;
         AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV85WWFuncaoDadosDS_14_Funcaodados_nome3 = AV27FuncaoDados_Nome3;
         AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 = AV28FuncaoDados_SistemaDes3;
         AV87WWFuncaoDadosDS_16_Tffuncaodados_nome = AV39TFFuncaoDados_Nome;
         AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel = AV40TFFuncaoDados_Nome_Sel;
         AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels = AV44TFFuncaoDados_Tipo_Sels;
         AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels = AV48TFFuncaoDados_Complexidade_Sels;
         AV91WWFuncaoDadosDS_20_Tffuncaodados_der = AV51TFFuncaoDados_DER;
         AV92WWFuncaoDadosDS_21_Tffuncaodados_der_to = AV52TFFuncaoDados_DER_To;
         AV93WWFuncaoDadosDS_22_Tffuncaodados_rlr = AV55TFFuncaoDados_RLR;
         AV94WWFuncaoDadosDS_23_Tffuncaodados_rlr_to = AV56TFFuncaoDados_RLR_To;
         AV95WWFuncaoDadosDS_24_Tffuncaodados_pf = AV59TFFuncaoDados_PF;
         AV96WWFuncaoDadosDS_25_Tffuncaodados_pf_to = AV60TFFuncaoDados_PF_To;
         AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels = AV64TFFuncaoDados_Ativo_Sels;
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV35OrderedBy, AV13OrderedDsc, AV14FuncaoDados_SistemaAreaCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoDados_Nome1, AV18FuncaoDados_SistemaDes1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22FuncaoDados_Nome2, AV23FuncaoDados_SistemaDes2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27FuncaoDados_Nome3, AV28FuncaoDados_SistemaDes3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV64TFFuncaoDados_Ativo_Sels, AV100Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A368FuncaoDados_Codigo, AV34FuncaoDados_SistemaCod, A370FuncaoDados_SistemaCod, A391FuncaoDados_FuncaoDadosCod) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod = AV14FuncaoDados_SistemaAreaCod;
         AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV75WWFuncaoDadosDS_4_Funcaodados_nome1 = AV17FuncaoDados_Nome1;
         AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 = AV18FuncaoDados_SistemaDes1;
         AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV80WWFuncaoDadosDS_9_Funcaodados_nome2 = AV22FuncaoDados_Nome2;
         AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 = AV23FuncaoDados_SistemaDes2;
         AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV85WWFuncaoDadosDS_14_Funcaodados_nome3 = AV27FuncaoDados_Nome3;
         AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 = AV28FuncaoDados_SistemaDes3;
         AV87WWFuncaoDadosDS_16_Tffuncaodados_nome = AV39TFFuncaoDados_Nome;
         AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel = AV40TFFuncaoDados_Nome_Sel;
         AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels = AV44TFFuncaoDados_Tipo_Sels;
         AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels = AV48TFFuncaoDados_Complexidade_Sels;
         AV91WWFuncaoDadosDS_20_Tffuncaodados_der = AV51TFFuncaoDados_DER;
         AV92WWFuncaoDadosDS_21_Tffuncaodados_der_to = AV52TFFuncaoDados_DER_To;
         AV93WWFuncaoDadosDS_22_Tffuncaodados_rlr = AV55TFFuncaoDados_RLR;
         AV94WWFuncaoDadosDS_23_Tffuncaodados_rlr_to = AV56TFFuncaoDados_RLR_To;
         AV95WWFuncaoDadosDS_24_Tffuncaodados_pf = AV59TFFuncaoDados_PF;
         AV96WWFuncaoDadosDS_25_Tffuncaodados_pf_to = AV60TFFuncaoDados_PF_To;
         AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels = AV64TFFuncaoDados_Ativo_Sels;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV35OrderedBy, AV13OrderedDsc, AV14FuncaoDados_SistemaAreaCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoDados_Nome1, AV18FuncaoDados_SistemaDes1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22FuncaoDados_Nome2, AV23FuncaoDados_SistemaDes2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27FuncaoDados_Nome3, AV28FuncaoDados_SistemaDes3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV64TFFuncaoDados_Ativo_Sels, AV100Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A368FuncaoDados_Codigo, AV34FuncaoDados_SistemaCod, A370FuncaoDados_SistemaCod, A391FuncaoDados_FuncaoDadosCod) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod = AV14FuncaoDados_SistemaAreaCod;
         AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV75WWFuncaoDadosDS_4_Funcaodados_nome1 = AV17FuncaoDados_Nome1;
         AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 = AV18FuncaoDados_SistemaDes1;
         AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV80WWFuncaoDadosDS_9_Funcaodados_nome2 = AV22FuncaoDados_Nome2;
         AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 = AV23FuncaoDados_SistemaDes2;
         AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV85WWFuncaoDadosDS_14_Funcaodados_nome3 = AV27FuncaoDados_Nome3;
         AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 = AV28FuncaoDados_SistemaDes3;
         AV87WWFuncaoDadosDS_16_Tffuncaodados_nome = AV39TFFuncaoDados_Nome;
         AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel = AV40TFFuncaoDados_Nome_Sel;
         AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels = AV44TFFuncaoDados_Tipo_Sels;
         AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels = AV48TFFuncaoDados_Complexidade_Sels;
         AV91WWFuncaoDadosDS_20_Tffuncaodados_der = AV51TFFuncaoDados_DER;
         AV92WWFuncaoDadosDS_21_Tffuncaodados_der_to = AV52TFFuncaoDados_DER_To;
         AV93WWFuncaoDadosDS_22_Tffuncaodados_rlr = AV55TFFuncaoDados_RLR;
         AV94WWFuncaoDadosDS_23_Tffuncaodados_rlr_to = AV56TFFuncaoDados_RLR_To;
         AV95WWFuncaoDadosDS_24_Tffuncaodados_pf = AV59TFFuncaoDados_PF;
         AV96WWFuncaoDadosDS_25_Tffuncaodados_pf_to = AV60TFFuncaoDados_PF_To;
         AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels = AV64TFFuncaoDados_Ativo_Sels;
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV35OrderedBy, AV13OrderedDsc, AV14FuncaoDados_SistemaAreaCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoDados_Nome1, AV18FuncaoDados_SistemaDes1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22FuncaoDados_Nome2, AV23FuncaoDados_SistemaDes2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27FuncaoDados_Nome3, AV28FuncaoDados_SistemaDes3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV64TFFuncaoDados_Ativo_Sels, AV100Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A368FuncaoDados_Codigo, AV34FuncaoDados_SistemaCod, A370FuncaoDados_SistemaCod, A391FuncaoDados_FuncaoDadosCod) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod = AV14FuncaoDados_SistemaAreaCod;
         AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV75WWFuncaoDadosDS_4_Funcaodados_nome1 = AV17FuncaoDados_Nome1;
         AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 = AV18FuncaoDados_SistemaDes1;
         AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV80WWFuncaoDadosDS_9_Funcaodados_nome2 = AV22FuncaoDados_Nome2;
         AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 = AV23FuncaoDados_SistemaDes2;
         AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV85WWFuncaoDadosDS_14_Funcaodados_nome3 = AV27FuncaoDados_Nome3;
         AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 = AV28FuncaoDados_SistemaDes3;
         AV87WWFuncaoDadosDS_16_Tffuncaodados_nome = AV39TFFuncaoDados_Nome;
         AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel = AV40TFFuncaoDados_Nome_Sel;
         AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels = AV44TFFuncaoDados_Tipo_Sels;
         AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels = AV48TFFuncaoDados_Complexidade_Sels;
         AV91WWFuncaoDadosDS_20_Tffuncaodados_der = AV51TFFuncaoDados_DER;
         AV92WWFuncaoDadosDS_21_Tffuncaodados_der_to = AV52TFFuncaoDados_DER_To;
         AV93WWFuncaoDadosDS_22_Tffuncaodados_rlr = AV55TFFuncaoDados_RLR;
         AV94WWFuncaoDadosDS_23_Tffuncaodados_rlr_to = AV56TFFuncaoDados_RLR_To;
         AV95WWFuncaoDadosDS_24_Tffuncaodados_pf = AV59TFFuncaoDados_PF;
         AV96WWFuncaoDadosDS_25_Tffuncaodados_pf_to = AV60TFFuncaoDados_PF_To;
         AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels = AV64TFFuncaoDados_Ativo_Sels;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV35OrderedBy, AV13OrderedDsc, AV14FuncaoDados_SistemaAreaCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoDados_Nome1, AV18FuncaoDados_SistemaDes1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22FuncaoDados_Nome2, AV23FuncaoDados_SistemaDes2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27FuncaoDados_Nome3, AV28FuncaoDados_SistemaDes3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV64TFFuncaoDados_Ativo_Sels, AV100Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A368FuncaoDados_Codigo, AV34FuncaoDados_SistemaCod, A370FuncaoDados_SistemaCod, A391FuncaoDados_FuncaoDadosCod) ;
         }
         return (int)(0) ;
      }

      protected void STRUP9B0( )
      {
         /* Before Start, stand alone formulas. */
         AV100Pgmname = "WWFuncaoDados";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E299B2 */
         E299B2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV66DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_NOMETITLEFILTERDATA"), AV38FuncaoDados_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_TIPOTITLEFILTERDATA"), AV42FuncaoDados_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA"), AV46FuncaoDados_ComplexidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_DERTITLEFILTERDATA"), AV50FuncaoDados_DERTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_RLRTITLEFILTERDATA"), AV54FuncaoDados_RLRTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_PFTITLEFILTERDATA"), AV58FuncaoDados_PFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vFUNCAODADOS_ATIVOTITLEFILTERDATA"), AV62FuncaoDados_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV35OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaodados_sistemaareacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaodados_sistemaareacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAODADOS_SISTEMAAREACOD");
               GX_FocusControl = edtavFuncaodados_sistemaareacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14FuncaoDados_SistemaAreaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14FuncaoDados_SistemaAreaCod), 6, 0)));
            }
            else
            {
               AV14FuncaoDados_SistemaAreaCod = (int)(context.localUtil.CToN( cgiGet( edtavFuncaodados_sistemaareacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14FuncaoDados_SistemaAreaCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17FuncaoDados_Nome1 = cgiGet( edtavFuncaodados_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoDados_Nome1", AV17FuncaoDados_Nome1);
            AV18FuncaoDados_SistemaDes1 = StringUtil.Upper( cgiGet( edtavFuncaodados_sistemades1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoDados_SistemaDes1", AV18FuncaoDados_SistemaDes1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV20DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV21DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
            AV22FuncaoDados_Nome2 = cgiGet( edtavFuncaodados_nome2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22FuncaoDados_Nome2", AV22FuncaoDados_Nome2);
            AV23FuncaoDados_SistemaDes2 = StringUtil.Upper( cgiGet( edtavFuncaodados_sistemades2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoDados_SistemaDes2", AV23FuncaoDados_SistemaDes2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV25DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV26DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
            AV27FuncaoDados_Nome3 = cgiGet( edtavFuncaodados_nome3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27FuncaoDados_Nome3", AV27FuncaoDados_Nome3);
            AV28FuncaoDados_SistemaDes3 = StringUtil.Upper( cgiGet( edtavFuncaodados_sistemades3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoDados_SistemaDes3", AV28FuncaoDados_SistemaDes3);
            AV19DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
            AV24DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
            AV39TFFuncaoDados_Nome = cgiGet( edtavTffuncaodados_nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoDados_Nome", AV39TFFuncaoDados_Nome);
            AV40TFFuncaoDados_Nome_Sel = cgiGet( edtavTffuncaodados_nome_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoDados_Nome_Sel", AV40TFFuncaoDados_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_DER");
               GX_FocusControl = edtavTffuncaodados_der_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFFuncaoDados_DER = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0)));
            }
            else
            {
               AV51TFFuncaoDados_DER = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_DER_TO");
               GX_FocusControl = edtavTffuncaodados_der_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFFuncaoDados_DER_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0)));
            }
            else
            {
               AV52TFFuncaoDados_DER_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_der_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_RLR");
               GX_FocusControl = edtavTffuncaodados_rlr_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFFuncaoDados_RLR = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0)));
            }
            else
            {
               AV55TFFuncaoDados_RLR = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_RLR_TO");
               GX_FocusControl = edtavTffuncaodados_rlr_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV56TFFuncaoDados_RLR_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0)));
            }
            else
            {
               AV56TFFuncaoDados_RLR_To = (short)(context.localUtil.CToN( cgiGet( edtavTffuncaodados_rlr_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_PF");
               GX_FocusControl = edtavTffuncaodados_pf_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFFuncaoDados_PF = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5)));
            }
            else
            {
               AV59TFFuncaoDados_PF = context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFFUNCAODADOS_PF_TO");
               GX_FocusControl = edtavTffuncaodados_pf_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60TFFuncaoDados_PF_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5)));
            }
            else
            {
               AV60TFFuncaoDados_PF_To = context.localUtil.CToN( cgiGet( edtavTffuncaodados_pf_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5)));
            }
            AV41ddo_FuncaoDados_NomeTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_FuncaoDados_NomeTitleControlIdToReplace", AV41ddo_FuncaoDados_NomeTitleControlIdToReplace);
            AV45ddo_FuncaoDados_TipoTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_FuncaoDados_TipoTitleControlIdToReplace", AV45ddo_FuncaoDados_TipoTitleControlIdToReplace);
            AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
            AV53ddo_FuncaoDados_DERTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_FuncaoDados_DERTitleControlIdToReplace", AV53ddo_FuncaoDados_DERTitleControlIdToReplace);
            AV57ddo_FuncaoDados_RLRTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_FuncaoDados_RLRTitleControlIdToReplace", AV57ddo_FuncaoDados_RLRTitleControlIdToReplace);
            AV61ddo_FuncaoDados_PFTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_FuncaoDados_PFTitleControlIdToReplace", AV61ddo_FuncaoDados_PFTitleControlIdToReplace);
            AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace", AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_94 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_94"), ",", "."));
            AV68GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV69GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcaodados_nome_Caption = cgiGet( "DDO_FUNCAODADOS_NOME_Caption");
            Ddo_funcaodados_nome_Tooltip = cgiGet( "DDO_FUNCAODADOS_NOME_Tooltip");
            Ddo_funcaodados_nome_Cls = cgiGet( "DDO_FUNCAODADOS_NOME_Cls");
            Ddo_funcaodados_nome_Filteredtext_set = cgiGet( "DDO_FUNCAODADOS_NOME_Filteredtext_set");
            Ddo_funcaodados_nome_Selectedvalue_set = cgiGet( "DDO_FUNCAODADOS_NOME_Selectedvalue_set");
            Ddo_funcaodados_nome_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_NOME_Dropdownoptionstype");
            Ddo_funcaodados_nome_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_NOME_Titlecontrolidtoreplace");
            Ddo_funcaodados_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_NOME_Includesortasc"));
            Ddo_funcaodados_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_NOME_Includesortdsc"));
            Ddo_funcaodados_nome_Sortedstatus = cgiGet( "DDO_FUNCAODADOS_NOME_Sortedstatus");
            Ddo_funcaodados_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_NOME_Includefilter"));
            Ddo_funcaodados_nome_Filtertype = cgiGet( "DDO_FUNCAODADOS_NOME_Filtertype");
            Ddo_funcaodados_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_NOME_Filterisrange"));
            Ddo_funcaodados_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_NOME_Includedatalist"));
            Ddo_funcaodados_nome_Datalisttype = cgiGet( "DDO_FUNCAODADOS_NOME_Datalisttype");
            Ddo_funcaodados_nome_Datalistproc = cgiGet( "DDO_FUNCAODADOS_NOME_Datalistproc");
            Ddo_funcaodados_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_FUNCAODADOS_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaodados_nome_Sortasc = cgiGet( "DDO_FUNCAODADOS_NOME_Sortasc");
            Ddo_funcaodados_nome_Sortdsc = cgiGet( "DDO_FUNCAODADOS_NOME_Sortdsc");
            Ddo_funcaodados_nome_Loadingdata = cgiGet( "DDO_FUNCAODADOS_NOME_Loadingdata");
            Ddo_funcaodados_nome_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_NOME_Cleanfilter");
            Ddo_funcaodados_nome_Noresultsfound = cgiGet( "DDO_FUNCAODADOS_NOME_Noresultsfound");
            Ddo_funcaodados_nome_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_NOME_Searchbuttontext");
            Ddo_funcaodados_tipo_Caption = cgiGet( "DDO_FUNCAODADOS_TIPO_Caption");
            Ddo_funcaodados_tipo_Tooltip = cgiGet( "DDO_FUNCAODADOS_TIPO_Tooltip");
            Ddo_funcaodados_tipo_Cls = cgiGet( "DDO_FUNCAODADOS_TIPO_Cls");
            Ddo_funcaodados_tipo_Selectedvalue_set = cgiGet( "DDO_FUNCAODADOS_TIPO_Selectedvalue_set");
            Ddo_funcaodados_tipo_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_TIPO_Dropdownoptionstype");
            Ddo_funcaodados_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_TIPO_Titlecontrolidtoreplace");
            Ddo_funcaodados_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_TIPO_Includesortasc"));
            Ddo_funcaodados_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_TIPO_Includesortdsc"));
            Ddo_funcaodados_tipo_Sortedstatus = cgiGet( "DDO_FUNCAODADOS_TIPO_Sortedstatus");
            Ddo_funcaodados_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_TIPO_Includefilter"));
            Ddo_funcaodados_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_TIPO_Includedatalist"));
            Ddo_funcaodados_tipo_Datalisttype = cgiGet( "DDO_FUNCAODADOS_TIPO_Datalisttype");
            Ddo_funcaodados_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_TIPO_Allowmultipleselection"));
            Ddo_funcaodados_tipo_Datalistfixedvalues = cgiGet( "DDO_FUNCAODADOS_TIPO_Datalistfixedvalues");
            Ddo_funcaodados_tipo_Sortasc = cgiGet( "DDO_FUNCAODADOS_TIPO_Sortasc");
            Ddo_funcaodados_tipo_Sortdsc = cgiGet( "DDO_FUNCAODADOS_TIPO_Sortdsc");
            Ddo_funcaodados_tipo_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_TIPO_Cleanfilter");
            Ddo_funcaodados_tipo_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_TIPO_Searchbuttontext");
            Ddo_funcaodados_complexidade_Caption = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Caption");
            Ddo_funcaodados_complexidade_Tooltip = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Tooltip");
            Ddo_funcaodados_complexidade_Cls = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Cls");
            Ddo_funcaodados_complexidade_Selectedvalue_set = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_set");
            Ddo_funcaodados_complexidade_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Dropdownoptionstype");
            Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Titlecontrolidtoreplace");
            Ddo_funcaodados_complexidade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Includesortasc"));
            Ddo_funcaodados_complexidade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Includesortdsc"));
            Ddo_funcaodados_complexidade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Includefilter"));
            Ddo_funcaodados_complexidade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Includedatalist"));
            Ddo_funcaodados_complexidade_Datalisttype = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Datalisttype");
            Ddo_funcaodados_complexidade_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Allowmultipleselection"));
            Ddo_funcaodados_complexidade_Datalistfixedvalues = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Datalistfixedvalues");
            Ddo_funcaodados_complexidade_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Cleanfilter");
            Ddo_funcaodados_complexidade_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Searchbuttontext");
            Ddo_funcaodados_der_Caption = cgiGet( "DDO_FUNCAODADOS_DER_Caption");
            Ddo_funcaodados_der_Tooltip = cgiGet( "DDO_FUNCAODADOS_DER_Tooltip");
            Ddo_funcaodados_der_Cls = cgiGet( "DDO_FUNCAODADOS_DER_Cls");
            Ddo_funcaodados_der_Filteredtext_set = cgiGet( "DDO_FUNCAODADOS_DER_Filteredtext_set");
            Ddo_funcaodados_der_Filteredtextto_set = cgiGet( "DDO_FUNCAODADOS_DER_Filteredtextto_set");
            Ddo_funcaodados_der_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_DER_Dropdownoptionstype");
            Ddo_funcaodados_der_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_DER_Titlecontrolidtoreplace");
            Ddo_funcaodados_der_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_DER_Includesortasc"));
            Ddo_funcaodados_der_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_DER_Includesortdsc"));
            Ddo_funcaodados_der_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_DER_Includefilter"));
            Ddo_funcaodados_der_Filtertype = cgiGet( "DDO_FUNCAODADOS_DER_Filtertype");
            Ddo_funcaodados_der_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_DER_Filterisrange"));
            Ddo_funcaodados_der_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_DER_Includedatalist"));
            Ddo_funcaodados_der_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_DER_Cleanfilter");
            Ddo_funcaodados_der_Rangefilterfrom = cgiGet( "DDO_FUNCAODADOS_DER_Rangefilterfrom");
            Ddo_funcaodados_der_Rangefilterto = cgiGet( "DDO_FUNCAODADOS_DER_Rangefilterto");
            Ddo_funcaodados_der_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_DER_Searchbuttontext");
            Ddo_funcaodados_rlr_Caption = cgiGet( "DDO_FUNCAODADOS_RLR_Caption");
            Ddo_funcaodados_rlr_Tooltip = cgiGet( "DDO_FUNCAODADOS_RLR_Tooltip");
            Ddo_funcaodados_rlr_Cls = cgiGet( "DDO_FUNCAODADOS_RLR_Cls");
            Ddo_funcaodados_rlr_Filteredtext_set = cgiGet( "DDO_FUNCAODADOS_RLR_Filteredtext_set");
            Ddo_funcaodados_rlr_Filteredtextto_set = cgiGet( "DDO_FUNCAODADOS_RLR_Filteredtextto_set");
            Ddo_funcaodados_rlr_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_RLR_Dropdownoptionstype");
            Ddo_funcaodados_rlr_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_RLR_Titlecontrolidtoreplace");
            Ddo_funcaodados_rlr_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_RLR_Includesortasc"));
            Ddo_funcaodados_rlr_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_RLR_Includesortdsc"));
            Ddo_funcaodados_rlr_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_RLR_Includefilter"));
            Ddo_funcaodados_rlr_Filtertype = cgiGet( "DDO_FUNCAODADOS_RLR_Filtertype");
            Ddo_funcaodados_rlr_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_RLR_Filterisrange"));
            Ddo_funcaodados_rlr_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_RLR_Includedatalist"));
            Ddo_funcaodados_rlr_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_RLR_Cleanfilter");
            Ddo_funcaodados_rlr_Rangefilterfrom = cgiGet( "DDO_FUNCAODADOS_RLR_Rangefilterfrom");
            Ddo_funcaodados_rlr_Rangefilterto = cgiGet( "DDO_FUNCAODADOS_RLR_Rangefilterto");
            Ddo_funcaodados_rlr_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_RLR_Searchbuttontext");
            Ddo_funcaodados_pf_Caption = cgiGet( "DDO_FUNCAODADOS_PF_Caption");
            Ddo_funcaodados_pf_Tooltip = cgiGet( "DDO_FUNCAODADOS_PF_Tooltip");
            Ddo_funcaodados_pf_Cls = cgiGet( "DDO_FUNCAODADOS_PF_Cls");
            Ddo_funcaodados_pf_Filteredtext_set = cgiGet( "DDO_FUNCAODADOS_PF_Filteredtext_set");
            Ddo_funcaodados_pf_Filteredtextto_set = cgiGet( "DDO_FUNCAODADOS_PF_Filteredtextto_set");
            Ddo_funcaodados_pf_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_PF_Dropdownoptionstype");
            Ddo_funcaodados_pf_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_PF_Titlecontrolidtoreplace");
            Ddo_funcaodados_pf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_PF_Includesortasc"));
            Ddo_funcaodados_pf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_PF_Includesortdsc"));
            Ddo_funcaodados_pf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_PF_Includefilter"));
            Ddo_funcaodados_pf_Filtertype = cgiGet( "DDO_FUNCAODADOS_PF_Filtertype");
            Ddo_funcaodados_pf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_PF_Filterisrange"));
            Ddo_funcaodados_pf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_PF_Includedatalist"));
            Ddo_funcaodados_pf_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_PF_Cleanfilter");
            Ddo_funcaodados_pf_Rangefilterfrom = cgiGet( "DDO_FUNCAODADOS_PF_Rangefilterfrom");
            Ddo_funcaodados_pf_Rangefilterto = cgiGet( "DDO_FUNCAODADOS_PF_Rangefilterto");
            Ddo_funcaodados_pf_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_PF_Searchbuttontext");
            Ddo_funcaodados_ativo_Caption = cgiGet( "DDO_FUNCAODADOS_ATIVO_Caption");
            Ddo_funcaodados_ativo_Tooltip = cgiGet( "DDO_FUNCAODADOS_ATIVO_Tooltip");
            Ddo_funcaodados_ativo_Cls = cgiGet( "DDO_FUNCAODADOS_ATIVO_Cls");
            Ddo_funcaodados_ativo_Selectedvalue_set = cgiGet( "DDO_FUNCAODADOS_ATIVO_Selectedvalue_set");
            Ddo_funcaodados_ativo_Dropdownoptionstype = cgiGet( "DDO_FUNCAODADOS_ATIVO_Dropdownoptionstype");
            Ddo_funcaodados_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_FUNCAODADOS_ATIVO_Titlecontrolidtoreplace");
            Ddo_funcaodados_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_ATIVO_Includesortasc"));
            Ddo_funcaodados_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_ATIVO_Includesortdsc"));
            Ddo_funcaodados_ativo_Sortedstatus = cgiGet( "DDO_FUNCAODADOS_ATIVO_Sortedstatus");
            Ddo_funcaodados_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_ATIVO_Includefilter"));
            Ddo_funcaodados_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_ATIVO_Includedatalist"));
            Ddo_funcaodados_ativo_Datalisttype = cgiGet( "DDO_FUNCAODADOS_ATIVO_Datalisttype");
            Ddo_funcaodados_ativo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_FUNCAODADOS_ATIVO_Allowmultipleselection"));
            Ddo_funcaodados_ativo_Datalistfixedvalues = cgiGet( "DDO_FUNCAODADOS_ATIVO_Datalistfixedvalues");
            Ddo_funcaodados_ativo_Sortasc = cgiGet( "DDO_FUNCAODADOS_ATIVO_Sortasc");
            Ddo_funcaodados_ativo_Sortdsc = cgiGet( "DDO_FUNCAODADOS_ATIVO_Sortdsc");
            Ddo_funcaodados_ativo_Cleanfilter = cgiGet( "DDO_FUNCAODADOS_ATIVO_Cleanfilter");
            Ddo_funcaodados_ativo_Searchbuttontext = cgiGet( "DDO_FUNCAODADOS_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcaodados_nome_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_NOME_Activeeventkey");
            Ddo_funcaodados_nome_Filteredtext_get = cgiGet( "DDO_FUNCAODADOS_NOME_Filteredtext_get");
            Ddo_funcaodados_nome_Selectedvalue_get = cgiGet( "DDO_FUNCAODADOS_NOME_Selectedvalue_get");
            Ddo_funcaodados_tipo_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_TIPO_Activeeventkey");
            Ddo_funcaodados_tipo_Selectedvalue_get = cgiGet( "DDO_FUNCAODADOS_TIPO_Selectedvalue_get");
            Ddo_funcaodados_complexidade_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Activeeventkey");
            Ddo_funcaodados_complexidade_Selectedvalue_get = cgiGet( "DDO_FUNCAODADOS_COMPLEXIDADE_Selectedvalue_get");
            Ddo_funcaodados_der_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_DER_Activeeventkey");
            Ddo_funcaodados_der_Filteredtext_get = cgiGet( "DDO_FUNCAODADOS_DER_Filteredtext_get");
            Ddo_funcaodados_der_Filteredtextto_get = cgiGet( "DDO_FUNCAODADOS_DER_Filteredtextto_get");
            Ddo_funcaodados_rlr_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_RLR_Activeeventkey");
            Ddo_funcaodados_rlr_Filteredtext_get = cgiGet( "DDO_FUNCAODADOS_RLR_Filteredtext_get");
            Ddo_funcaodados_rlr_Filteredtextto_get = cgiGet( "DDO_FUNCAODADOS_RLR_Filteredtextto_get");
            Ddo_funcaodados_pf_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_PF_Activeeventkey");
            Ddo_funcaodados_pf_Filteredtext_get = cgiGet( "DDO_FUNCAODADOS_PF_Filteredtext_get");
            Ddo_funcaodados_pf_Filteredtextto_get = cgiGet( "DDO_FUNCAODADOS_PF_Filteredtextto_get");
            Ddo_funcaodados_ativo_Activeeventkey = cgiGet( "DDO_FUNCAODADOS_ATIVO_Activeeventkey");
            Ddo_funcaodados_ativo_Selectedvalue_get = cgiGet( "DDO_FUNCAODADOS_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV35OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vFUNCAODADOS_SISTEMAAREACOD"), ",", ".") != Convert.ToDecimal( AV14FuncaoDados_SistemaAreaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_NOME1"), AV17FuncaoDados_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_SISTEMADES1"), AV18FuncaoDados_SistemaDes1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV20DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV21DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_NOME2"), AV22FuncaoDados_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_SISTEMADES2"), AV23FuncaoDados_SistemaDes2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV25DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_NOME3"), AV27FuncaoDados_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vFUNCAODADOS_SISTEMADES3"), AV28FuncaoDados_SistemaDes3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV19DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV24DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAODADOS_NOME"), AV39TFFuncaoDados_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFFUNCAODADOS_NOME_SEL"), AV40TFFuncaoDados_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E299B2 */
         E299B2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E299B2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV20DynamicFiltersSelector2 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersSelector3 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTffuncaodados_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_nome_Visible), 5, 0)));
         edtavTffuncaodados_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_nome_sel_Visible), 5, 0)));
         edtavTffuncaodados_der_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_der_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_der_Visible), 5, 0)));
         edtavTffuncaodados_der_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_der_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_der_to_Visible), 5, 0)));
         edtavTffuncaodados_rlr_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_rlr_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_rlr_Visible), 5, 0)));
         edtavTffuncaodados_rlr_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_rlr_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_rlr_to_Visible), 5, 0)));
         edtavTffuncaodados_pf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_pf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_pf_Visible), 5, 0)));
         edtavTffuncaodados_pf_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTffuncaodados_pf_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaodados_pf_to_Visible), 5, 0)));
         Ddo_funcaodados_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_nome_Titlecontrolidtoreplace);
         AV41ddo_FuncaoDados_NomeTitleControlIdToReplace = Ddo_funcaodados_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_FuncaoDados_NomeTitleControlIdToReplace", AV41ddo_FuncaoDados_NomeTitleControlIdToReplace);
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_tipo_Titlecontrolidtoreplace);
         AV45ddo_FuncaoDados_TipoTitleControlIdToReplace = Ddo_funcaodados_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_FuncaoDados_TipoTitleControlIdToReplace", AV45ddo_FuncaoDados_TipoTitleControlIdToReplace);
         edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Complexidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_complexidade_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_complexidade_Titlecontrolidtoreplace);
         AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = Ddo_funcaodados_complexidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace", AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace);
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_der_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_DER";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_der_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_der_Titlecontrolidtoreplace);
         AV53ddo_FuncaoDados_DERTitleControlIdToReplace = Ddo_funcaodados_der_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ddo_FuncaoDados_DERTitleControlIdToReplace", AV53ddo_FuncaoDados_DERTitleControlIdToReplace);
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_rlr_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_RLR";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_rlr_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_rlr_Titlecontrolidtoreplace);
         AV57ddo_FuncaoDados_RLRTitleControlIdToReplace = Ddo_funcaodados_rlr_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_FuncaoDados_RLRTitleControlIdToReplace", AV57ddo_FuncaoDados_RLRTitleControlIdToReplace);
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_pf_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_PF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_pf_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_pf_Titlecontrolidtoreplace);
         AV61ddo_FuncaoDados_PFTitleControlIdToReplace = Ddo_funcaodados_pf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_FuncaoDados_PFTitleControlIdToReplace", AV61ddo_FuncaoDados_PFTitleControlIdToReplace);
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaodados_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoDados_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "TitleControlIdToReplace", Ddo_funcaodados_ativo_Titlecontrolidtoreplace);
         AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace = Ddo_funcaodados_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace", AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace);
         edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Grupo L�gico de Dados";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Dados", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         cmbavOrderedby.addItem("3", "Status", 0);
         if ( AV35OrderedBy < 1 )
         {
            AV35OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 = AV66DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3) ;
         AV66DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3;
      }

      protected void E309B2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV38FuncaoDados_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42FuncaoDados_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46FuncaoDados_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50FuncaoDados_DERTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54FuncaoDados_RLRTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58FuncaoDados_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62FuncaoDados_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAODADOS_SISTEMADES") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_SISTEMADES") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV24DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAODADOS_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAODADOS_SISTEMADES") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoDados_Nome_Titleformat = 2;
         edtFuncaoDados_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_Nome_Internalname, "Title", edtFuncaoDados_Nome_Title);
         cmbFuncaoDados_Tipo_Titleformat = 2;
         cmbFuncaoDados_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo", AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Tipo_Internalname, "Title", cmbFuncaoDados_Tipo.Title.Text);
         cmbFuncaoDados_Complexidade_Titleformat = 2;
         cmbFuncaoDados_Complexidade.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Complexidade", AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Complexidade_Internalname, "Title", cmbFuncaoDados_Complexidade.Title.Text);
         edtFuncaoDados_DER_Titleformat = 2;
         edtFuncaoDados_DER_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "DER", AV53ddo_FuncaoDados_DERTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_DER_Internalname, "Title", edtFuncaoDados_DER_Title);
         edtFuncaoDados_RLR_Titleformat = 2;
         edtFuncaoDados_RLR_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "RLR", AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_RLR_Internalname, "Title", edtFuncaoDados_RLR_Title);
         edtFuncaoDados_PF_Titleformat = 2;
         edtFuncaoDados_PF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "PF", AV61ddo_FuncaoDados_PFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtFuncaoDados_PF_Internalname, "Title", edtFuncaoDados_PF_Title);
         cmbFuncaoDados_Ativo_Titleformat = 2;
         cmbFuncaoDados_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status", AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Ativo_Internalname, "Title", cmbFuncaoDados_Ativo.Title.Text);
         AV68GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV68GridCurrentPage), 10, 0)));
         AV69GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV69GridPageCount), 10, 0)));
         AV14FuncaoDados_SistemaAreaCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14FuncaoDados_SistemaAreaCod), 6, 0)));
         AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod = AV14FuncaoDados_SistemaAreaCod;
         AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV75WWFuncaoDadosDS_4_Funcaodados_nome1 = AV17FuncaoDados_Nome1;
         AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 = AV18FuncaoDados_SistemaDes1;
         AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 = AV19DynamicFiltersEnabled2;
         AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2 = AV20DynamicFiltersSelector2;
         AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 = AV21DynamicFiltersOperator2;
         AV80WWFuncaoDadosDS_9_Funcaodados_nome2 = AV22FuncaoDados_Nome2;
         AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 = AV23FuncaoDados_SistemaDes2;
         AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 = AV24DynamicFiltersEnabled3;
         AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3 = AV25DynamicFiltersSelector3;
         AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 = AV26DynamicFiltersOperator3;
         AV85WWFuncaoDadosDS_14_Funcaodados_nome3 = AV27FuncaoDados_Nome3;
         AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 = AV28FuncaoDados_SistemaDes3;
         AV87WWFuncaoDadosDS_16_Tffuncaodados_nome = AV39TFFuncaoDados_Nome;
         AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel = AV40TFFuncaoDados_Nome_Sel;
         AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels = AV44TFFuncaoDados_Tipo_Sels;
         AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels = AV48TFFuncaoDados_Complexidade_Sels;
         AV91WWFuncaoDadosDS_20_Tffuncaodados_der = AV51TFFuncaoDados_DER;
         AV92WWFuncaoDadosDS_21_Tffuncaodados_der_to = AV52TFFuncaoDados_DER_To;
         AV93WWFuncaoDadosDS_22_Tffuncaodados_rlr = AV55TFFuncaoDados_RLR;
         AV94WWFuncaoDadosDS_23_Tffuncaodados_rlr_to = AV56TFFuncaoDados_RLR_To;
         AV95WWFuncaoDadosDS_24_Tffuncaodados_pf = AV59TFFuncaoDados_PF;
         AV96WWFuncaoDadosDS_25_Tffuncaodados_pf_to = AV60TFFuncaoDados_PF_To;
         AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels = AV64TFFuncaoDados_Ativo_Sels;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38FuncaoDados_NomeTitleFilterData", AV38FuncaoDados_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42FuncaoDados_TipoTitleFilterData", AV42FuncaoDados_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46FuncaoDados_ComplexidadeTitleFilterData", AV46FuncaoDados_ComplexidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50FuncaoDados_DERTitleFilterData", AV50FuncaoDados_DERTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54FuncaoDados_RLRTitleFilterData", AV54FuncaoDados_RLRTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58FuncaoDados_PFTitleFilterData", AV58FuncaoDados_PFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62FuncaoDados_AtivoTitleFilterData", AV62FuncaoDados_AtivoTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E119B2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV67PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV67PageToGo) ;
         }
      }

      protected void E129B2( )
      {
         /* Ddo_funcaodados_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV35OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV35OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFFuncaoDados_Nome = Ddo_funcaodados_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoDados_Nome", AV39TFFuncaoDados_Nome);
            AV40TFFuncaoDados_Nome_Sel = Ddo_funcaodados_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoDados_Nome_Sel", AV40TFFuncaoDados_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E139B2( )
      {
         /* Ddo_funcaodados_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV35OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV35OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFFuncaoDados_Tipo_SelsJson = Ddo_funcaodados_tipo_Selectedvalue_get;
            AV44TFFuncaoDados_Tipo_Sels.FromJSonString(AV43TFFuncaoDados_Tipo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44TFFuncaoDados_Tipo_Sels", AV44TFFuncaoDados_Tipo_Sels);
      }

      protected void E149B2( )
      {
         /* Ddo_funcaodados_complexidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_complexidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFFuncaoDados_Complexidade_SelsJson = Ddo_funcaodados_complexidade_Selectedvalue_get;
            AV48TFFuncaoDados_Complexidade_Sels.FromJSonString(AV47TFFuncaoDados_Complexidade_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48TFFuncaoDados_Complexidade_Sels", AV48TFFuncaoDados_Complexidade_Sels);
      }

      protected void E159B2( )
      {
         /* Ddo_funcaodados_der_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_der_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFFuncaoDados_DER = (short)(NumberUtil.Val( Ddo_funcaodados_der_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0)));
            AV52TFFuncaoDados_DER_To = (short)(NumberUtil.Val( Ddo_funcaodados_der_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E169B2( )
      {
         /* Ddo_funcaodados_rlr_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_rlr_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFFuncaoDados_RLR = (short)(NumberUtil.Val( Ddo_funcaodados_rlr_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0)));
            AV56TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( Ddo_funcaodados_rlr_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E179B2( )
      {
         /* Ddo_funcaodados_pf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_pf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFFuncaoDados_PF = NumberUtil.Val( Ddo_funcaodados_pf_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5)));
            AV60TFFuncaoDados_PF_To = NumberUtil.Val( Ddo_funcaodados_pf_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E189B2( )
      {
         /* Ddo_funcaodados_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaodados_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV35OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV35OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_funcaodados_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_funcaodados_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFFuncaoDados_Ativo_SelsJson = Ddo_funcaodados_ativo_Selectedvalue_get;
            AV64TFFuncaoDados_Ativo_Sels.FromJSonString(AV63TFFuncaoDados_Ativo_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64TFFuncaoDados_Ativo_Sels", AV64TFFuncaoDados_Ativo_Sels);
      }

      private void E319B2( )
      {
         /* Grid_Load Routine */
         AV31Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV31Update);
         AV98Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("funcaodados.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A368FuncaoDados_Codigo) + "," + UrlEncode("" +AV34FuncaoDados_SistemaCod);
         AV32Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV32Delete);
         AV99Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("funcaodados.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A368FuncaoDados_Codigo) + "," + UrlEncode("" +AV34FuncaoDados_SistemaCod);
         edtFuncaoDados_Nome_Link = formatLink("viewfuncaodados.aspx") + "?" + UrlEncode("" +A368FuncaoDados_Codigo) + "," + UrlEncode("" +A370FuncaoDados_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         edtavUpdate_Visible = (((0==A391FuncaoDados_FuncaoDadosCod)) ? 1 : 0);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 94;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_942( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_94_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(94, GridRow);
         }
      }

      protected void E199B2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E249B2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV19DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV35OrderedBy, AV13OrderedDsc, AV14FuncaoDados_SistemaAreaCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoDados_Nome1, AV18FuncaoDados_SistemaDes1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22FuncaoDados_Nome2, AV23FuncaoDados_SistemaDes2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27FuncaoDados_Nome3, AV28FuncaoDados_SistemaDes3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV64TFFuncaoDados_Ativo_Sels, AV100Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A368FuncaoDados_Codigo, AV34FuncaoDados_SistemaCod, A370FuncaoDados_SistemaCod, A391FuncaoDados_FuncaoDadosCod) ;
      }

      protected void E209B2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV30DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersIgnoreFirst", AV30DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV35OrderedBy, AV13OrderedDsc, AV14FuncaoDados_SistemaAreaCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoDados_Nome1, AV18FuncaoDados_SistemaDes1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22FuncaoDados_Nome2, AV23FuncaoDados_SistemaDes2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27FuncaoDados_Nome3, AV28FuncaoDados_SistemaDes3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV64TFFuncaoDados_Ativo_Sels, AV100Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A368FuncaoDados_Codigo, AV34FuncaoDados_SistemaCod, A370FuncaoDados_SistemaCod, A391FuncaoDados_FuncaoDadosCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E259B2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E269B2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV24DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV35OrderedBy, AV13OrderedDsc, AV14FuncaoDados_SistemaAreaCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoDados_Nome1, AV18FuncaoDados_SistemaDes1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22FuncaoDados_Nome2, AV23FuncaoDados_SistemaDes2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27FuncaoDados_Nome3, AV28FuncaoDados_SistemaDes3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV64TFFuncaoDados_Ativo_Sels, AV100Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A368FuncaoDados_Codigo, AV34FuncaoDados_SistemaCod, A370FuncaoDados_SistemaCod, A391FuncaoDados_FuncaoDadosCod) ;
      }

      protected void E219B2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV35OrderedBy, AV13OrderedDsc, AV14FuncaoDados_SistemaAreaCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoDados_Nome1, AV18FuncaoDados_SistemaDes1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22FuncaoDados_Nome2, AV23FuncaoDados_SistemaDes2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27FuncaoDados_Nome3, AV28FuncaoDados_SistemaDes3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV64TFFuncaoDados_Ativo_Sels, AV100Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A368FuncaoDados_Codigo, AV34FuncaoDados_SistemaCod, A370FuncaoDados_SistemaCod, A391FuncaoDados_FuncaoDadosCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E279B2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E229B2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV29DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV29DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29DynamicFiltersRemoving", AV29DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV35OrderedBy, AV13OrderedDsc, AV14FuncaoDados_SistemaAreaCod, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17FuncaoDados_Nome1, AV18FuncaoDados_SistemaDes1, AV20DynamicFiltersSelector2, AV21DynamicFiltersOperator2, AV22FuncaoDados_Nome2, AV23FuncaoDados_SistemaDes2, AV25DynamicFiltersSelector3, AV26DynamicFiltersOperator3, AV27FuncaoDados_Nome3, AV28FuncaoDados_SistemaDes3, AV19DynamicFiltersEnabled2, AV24DynamicFiltersEnabled3, AV39TFFuncaoDados_Nome, AV40TFFuncaoDados_Nome_Sel, AV41ddo_FuncaoDados_NomeTitleControlIdToReplace, AV45ddo_FuncaoDados_TipoTitleControlIdToReplace, AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace, AV53ddo_FuncaoDados_DERTitleControlIdToReplace, AV57ddo_FuncaoDados_RLRTitleControlIdToReplace, AV61ddo_FuncaoDados_PFTitleControlIdToReplace, AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace, AV44TFFuncaoDados_Tipo_Sels, AV48TFFuncaoDados_Complexidade_Sels, AV51TFFuncaoDados_DER, AV52TFFuncaoDados_DER_To, AV55TFFuncaoDados_RLR, AV56TFFuncaoDados_RLR_To, AV59TFFuncaoDados_PF, AV60TFFuncaoDados_PF_To, AV64TFFuncaoDados_Ativo_Sels, AV100Pgmname, AV10GridState, AV30DynamicFiltersIgnoreFirst, AV29DynamicFiltersRemoving, A368FuncaoDados_Codigo, AV34FuncaoDados_SistemaCod, A370FuncaoDados_SistemaCod, A391FuncaoDados_FuncaoDadosCod) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E289B2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E239B2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44TFFuncaoDados_Tipo_Sels", AV44TFFuncaoDados_Tipo_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48TFFuncaoDados_Complexidade_Sels", AV48TFFuncaoDados_Complexidade_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64TFFuncaoDados_Ativo_Sels", AV64TFFuncaoDados_Ativo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S182( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_funcaodados_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
         Ddo_funcaodados_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
         Ddo_funcaodados_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV35OrderedBy == 1 )
         {
            Ddo_funcaodados_nome_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "SortedStatus", Ddo_funcaodados_nome_Sortedstatus);
         }
         else if ( AV35OrderedBy == 2 )
         {
            Ddo_funcaodados_tipo_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "SortedStatus", Ddo_funcaodados_tipo_Sortedstatus);
         }
         else if ( AV35OrderedBy == 3 )
         {
            Ddo_funcaodados_ativo_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "SortedStatus", Ddo_funcaodados_ativo_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaodados_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome1_Visible), 5, 0)));
         edtavFuncaodados_sistemades1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_sistemades1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_sistemades1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 )
         {
            edtavFuncaodados_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAODADOS_SISTEMADES") == 0 )
         {
            edtavFuncaodados_sistemades1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_sistemades1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_sistemades1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavFuncaodados_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome2_Visible), 5, 0)));
         edtavFuncaodados_sistemades2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_sistemades2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_sistemades2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 )
         {
            edtavFuncaodados_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_SISTEMADES") == 0 )
         {
            edtavFuncaodados_sistemades2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_sistemades2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_sistemades2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavFuncaodados_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome3_Visible), 5, 0)));
         edtavFuncaodados_sistemades3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_sistemades3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_sistemades3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAODADOS_NOME") == 0 )
         {
            edtavFuncaodados_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAODADOS_SISTEMADES") == 0 )
         {
            edtavFuncaodados_sistemades3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaodados_sistemades3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaodados_sistemades3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV19DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
         AV20DynamicFiltersSelector2 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
         AV21DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
         AV22FuncaoDados_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22FuncaoDados_Nome2", AV22FuncaoDados_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV24DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
         AV25DynamicFiltersSelector3 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
         AV26DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
         AV27FuncaoDados_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27FuncaoDados_Nome3", AV27FuncaoDados_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV14FuncaoDados_SistemaAreaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14FuncaoDados_SistemaAreaCod), 6, 0)));
         AV39TFFuncaoDados_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoDados_Nome", AV39TFFuncaoDados_Nome);
         Ddo_funcaodados_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "FilteredText_set", Ddo_funcaodados_nome_Filteredtext_set);
         AV40TFFuncaoDados_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoDados_Nome_Sel", AV40TFFuncaoDados_Nome_Sel);
         Ddo_funcaodados_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "SelectedValue_set", Ddo_funcaodados_nome_Selectedvalue_set);
         AV44TFFuncaoDados_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaodados_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "SelectedValue_set", Ddo_funcaodados_tipo_Selectedvalue_set);
         AV48TFFuncaoDados_Complexidade_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaodados_complexidade_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_complexidade_Internalname, "SelectedValue_set", Ddo_funcaodados_complexidade_Selectedvalue_set);
         AV51TFFuncaoDados_DER = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0)));
         Ddo_funcaodados_der_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_der_Internalname, "FilteredText_set", Ddo_funcaodados_der_Filteredtext_set);
         AV52TFFuncaoDados_DER_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0)));
         Ddo_funcaodados_der_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_der_Internalname, "FilteredTextTo_set", Ddo_funcaodados_der_Filteredtextto_set);
         AV55TFFuncaoDados_RLR = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0)));
         Ddo_funcaodados_rlr_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_rlr_Internalname, "FilteredText_set", Ddo_funcaodados_rlr_Filteredtext_set);
         AV56TFFuncaoDados_RLR_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0)));
         Ddo_funcaodados_rlr_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_rlr_Internalname, "FilteredTextTo_set", Ddo_funcaodados_rlr_Filteredtextto_set);
         AV59TFFuncaoDados_PF = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5)));
         Ddo_funcaodados_pf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_pf_Internalname, "FilteredText_set", Ddo_funcaodados_pf_Filteredtext_set);
         AV60TFFuncaoDados_PF_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5)));
         Ddo_funcaodados_pf_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_pf_Internalname, "FilteredTextTo_set", Ddo_funcaodados_pf_Filteredtextto_set);
         AV64TFFuncaoDados_Ativo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_funcaodados_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "SelectedValue_set", Ddo_funcaodados_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "FUNCAODADOS_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17FuncaoDados_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoDados_Nome1", AV17FuncaoDados_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV33Session.Get(AV100Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV100Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV33Session.Get(AV100Pgmname+"GridState"), "");
         }
         AV35OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)));
         AV13OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV101GXV1 = 1;
         while ( AV101GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV101GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "FUNCAODADOS_SISTEMAAREACOD") == 0 )
            {
               AV14FuncaoDados_SistemaAreaCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14FuncaoDados_SistemaAreaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14FuncaoDados_SistemaAreaCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME") == 0 )
            {
               AV39TFFuncaoDados_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFFuncaoDados_Nome", AV39TFFuncaoDados_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFFuncaoDados_Nome)) )
               {
                  Ddo_funcaodados_nome_Filteredtext_set = AV39TFFuncaoDados_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "FilteredText_set", Ddo_funcaodados_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_NOME_SEL") == 0 )
            {
               AV40TFFuncaoDados_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFFuncaoDados_Nome_Sel", AV40TFFuncaoDados_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFFuncaoDados_Nome_Sel)) )
               {
                  Ddo_funcaodados_nome_Selectedvalue_set = AV40TFFuncaoDados_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_nome_Internalname, "SelectedValue_set", Ddo_funcaodados_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_TIPO_SEL") == 0 )
            {
               AV43TFFuncaoDados_Tipo_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV44TFFuncaoDados_Tipo_Sels.FromJSonString(AV43TFFuncaoDados_Tipo_SelsJson);
               if ( ! ( AV44TFFuncaoDados_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_funcaodados_tipo_Selectedvalue_set = AV43TFFuncaoDados_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_tipo_Internalname, "SelectedValue_set", Ddo_funcaodados_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_COMPLEXIDADE_SEL") == 0 )
            {
               AV47TFFuncaoDados_Complexidade_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV48TFFuncaoDados_Complexidade_Sels.FromJSonString(AV47TFFuncaoDados_Complexidade_SelsJson);
               if ( ! ( AV48TFFuncaoDados_Complexidade_Sels.Count == 0 ) )
               {
                  Ddo_funcaodados_complexidade_Selectedvalue_set = AV47TFFuncaoDados_Complexidade_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_complexidade_Internalname, "SelectedValue_set", Ddo_funcaodados_complexidade_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_DER") == 0 )
            {
               AV51TFFuncaoDados_DER = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFFuncaoDados_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0)));
               AV52TFFuncaoDados_DER_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFFuncaoDados_DER_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0)));
               if ( ! (0==AV51TFFuncaoDados_DER) )
               {
                  Ddo_funcaodados_der_Filteredtext_set = StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_der_Internalname, "FilteredText_set", Ddo_funcaodados_der_Filteredtext_set);
               }
               if ( ! (0==AV52TFFuncaoDados_DER_To) )
               {
                  Ddo_funcaodados_der_Filteredtextto_set = StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_der_Internalname, "FilteredTextTo_set", Ddo_funcaodados_der_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_RLR") == 0 )
            {
               AV55TFFuncaoDados_RLR = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFFuncaoDados_RLR", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0)));
               AV56TFFuncaoDados_RLR_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFFuncaoDados_RLR_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0)));
               if ( ! (0==AV55TFFuncaoDados_RLR) )
               {
                  Ddo_funcaodados_rlr_Filteredtext_set = StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_rlr_Internalname, "FilteredText_set", Ddo_funcaodados_rlr_Filteredtext_set);
               }
               if ( ! (0==AV56TFFuncaoDados_RLR_To) )
               {
                  Ddo_funcaodados_rlr_Filteredtextto_set = StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_rlr_Internalname, "FilteredTextTo_set", Ddo_funcaodados_rlr_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_PF") == 0 )
            {
               AV59TFFuncaoDados_PF = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFFuncaoDados_PF", StringUtil.LTrim( StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5)));
               AV60TFFuncaoDados_PF_To = NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFFuncaoDados_PF_To", StringUtil.LTrim( StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5)));
               if ( ! (Convert.ToDecimal(0)==AV59TFFuncaoDados_PF) )
               {
                  Ddo_funcaodados_pf_Filteredtext_set = StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_pf_Internalname, "FilteredText_set", Ddo_funcaodados_pf_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV60TFFuncaoDados_PF_To) )
               {
                  Ddo_funcaodados_pf_Filteredtextto_set = StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_pf_Internalname, "FilteredTextTo_set", Ddo_funcaodados_pf_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFFUNCAODADOS_ATIVO_SEL") == 0 )
            {
               AV63TFFuncaoDados_Ativo_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV64TFFuncaoDados_Ativo_Sels.FromJSonString(AV63TFFuncaoDados_Ativo_SelsJson);
               if ( ! ( AV64TFFuncaoDados_Ativo_Sels.Count == 0 ) )
               {
                  Ddo_funcaodados_ativo_Selectedvalue_set = AV63TFFuncaoDados_Ativo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_funcaodados_ativo_Internalname, "SelectedValue_set", Ddo_funcaodados_ativo_Selectedvalue_set);
               }
            }
            AV101GXV1 = (int)(AV101GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17FuncaoDados_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17FuncaoDados_Nome1", AV17FuncaoDados_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAODADOS_SISTEMADES") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV18FuncaoDados_SistemaDes1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18FuncaoDados_SistemaDes1", AV18FuncaoDados_SistemaDes1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV19DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersEnabled2", AV19DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV20DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersSelector2", AV20DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV22FuncaoDados_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22FuncaoDados_Nome2", AV22FuncaoDados_Nome2);
               }
               else if ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_SISTEMADES") == 0 )
               {
                  AV21DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)));
                  AV23FuncaoDados_SistemaDes2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23FuncaoDados_SistemaDes2", AV23FuncaoDados_SistemaDes2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV24DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersEnabled3", AV24DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV25DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector3", AV25DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAODADOS_NOME") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV27FuncaoDados_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27FuncaoDados_Nome3", AV27FuncaoDados_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAODADOS_SISTEMADES") == 0 )
                  {
                     AV26DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)));
                     AV28FuncaoDados_SistemaDes3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28FuncaoDados_SistemaDes3", AV28FuncaoDados_SistemaDes3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV29DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S172( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV33Session.Get(AV100Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV35OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV14FuncaoDados_SistemaAreaCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "FUNCAODADOS_SISTEMAAREACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV14FuncaoDados_SistemaAreaCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFFuncaoDados_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV39TFFuncaoDados_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFFuncaoDados_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV40TFFuncaoDados_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV44TFFuncaoDados_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV44TFFuncaoDados_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV48TFFuncaoDados_Complexidade_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_COMPLEXIDADE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV48TFFuncaoDados_Complexidade_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV51TFFuncaoDados_DER) && (0==AV52TFFuncaoDados_DER_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_DER";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV51TFFuncaoDados_DER), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV52TFFuncaoDados_DER_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV55TFFuncaoDados_RLR) && (0==AV56TFFuncaoDados_RLR_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_RLR";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV55TFFuncaoDados_RLR), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV56TFFuncaoDados_RLR_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV59TFFuncaoDados_PF) && (Convert.ToDecimal(0)==AV60TFFuncaoDados_PF_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_PF";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV59TFFuncaoDados_PF, 14, 5);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV60TFFuncaoDados_PF_To, 14, 5);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV64TFFuncaoDados_Ativo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFFUNCAODADOS_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV64TFFuncaoDados_Ativo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV100Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV30DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAODADOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17FuncaoDados_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17FuncaoDados_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "FUNCAODADOS_SISTEMADES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18FuncaoDados_SistemaDes1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18FuncaoDados_SistemaDes1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV19DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV20DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22FuncaoDados_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV22FuncaoDados_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV20DynamicFiltersSelector2, "FUNCAODADOS_SISTEMADES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoDados_SistemaDes2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23FuncaoDados_SistemaDes2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV21DynamicFiltersOperator2;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV24DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAODADOS_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27FuncaoDados_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV27FuncaoDados_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector3, "FUNCAODADOS_SISTEMADES") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoDados_SistemaDes3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV28FuncaoDados_SistemaDes3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator3;
            }
            if ( AV29DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV100Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "FuncaoDados";
         AV33Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_9B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_9B2( true) ;
         }
         else
         {
            wb_table2_8_9B2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_9B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_88_9B2( true) ;
         }
         else
         {
            wb_table3_88_9B2( false) ;
         }
         return  ;
      }

      protected void wb_table3_88_9B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9B2e( true) ;
         }
         else
         {
            wb_table1_2_9B2e( false) ;
         }
      }

      protected void wb_table3_88_9B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table4_91_9B2( true) ;
         }
         else
         {
            wb_table4_91_9B2( false) ;
         }
         return  ;
      }

      protected void wb_table4_91_9B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_88_9B2e( true) ;
         }
         else
         {
            wb_table3_88_9B2e( false) ;
         }
      }

      protected void wb_table4_91_9B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"94\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoDados_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoDados_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoDados_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoDados_Complexidade_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoDados_Complexidade.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoDados_Complexidade.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_DER_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_DER_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_DER_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_RLR_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_RLR_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_RLR_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoDados_PF_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoDados_PF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoDados_PF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoDados_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoDados_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoDados_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV31Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A369FuncaoDados_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFuncaoDados_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A373FuncaoDados_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoDados_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoDados_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A376FuncaoDados_Complexidade));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoDados_Complexidade.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoDados_Complexidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_DER_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_DER_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_RLR_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_RLR_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoDados_PF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoDados_PF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A394FuncaoDados_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoDados_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoDados_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 94 )
         {
            wbEnd = 0;
            nRC_GXsfl_94 = (short)(nGXsfl_94_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_91_9B2e( true) ;
         }
         else
         {
            wb_table4_91_9B2e( false) ;
         }
      }

      protected void wb_table2_8_9B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFuncaodadostitle_Internalname, "Fun��es de Dados", "", "", lblFuncaodadostitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_13_9B2( true) ;
         }
         else
         {
            wb_table5_13_9B2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_9B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WWFuncaoDados.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_22_9B2( true) ;
         }
         else
         {
            wb_table6_22_9B2( false) ;
         }
         return  ;
      }

      protected void wb_table6_22_9B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_9B2e( true) ;
         }
         else
         {
            wb_table2_8_9B2e( false) ;
         }
      }

      protected void wb_table6_22_9B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextfuncaodados_sistemaareacod_Internalname, "Area", "", "", lblFiltertextfuncaodados_sistemaareacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaodados_sistemaareacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14FuncaoDados_SistemaAreaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14FuncaoDados_SistemaAreaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaodados_sistemaareacod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_31_9B2( true) ;
         }
         else
         {
            wb_table7_31_9B2( false) ;
         }
         return  ;
      }

      protected void wb_table7_31_9B2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_22_9B2e( true) ;
         }
         else
         {
            wb_table6_22_9B2e( false) ;
         }
      }

      protected void wb_table7_31_9B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_WWFuncaoDados.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_40_9B2( true) ;
         }
         else
         {
            wb_table8_40_9B2( false) ;
         }
         return  ;
      }

      protected void wb_table8_40_9B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoDados.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV20DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_WWFuncaoDados.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV20DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_58_9B2( true) ;
         }
         else
         {
            wb_table9_58_9B2( false) ;
         }
         return  ;
      }

      protected void wb_table9_58_9B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoDados.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "", true, "HLP_WWFuncaoDados.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_76_9B2( true) ;
         }
         else
         {
            wb_table10_76_9B2( false) ;
         }
         return  ;
      }

      protected void wb_table10_76_9B2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_31_9B2e( true) ;
         }
         else
         {
            wb_table7_31_9B2e( false) ;
         }
      }

      protected void wb_table10_76_9B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "", true, "HLP_WWFuncaoDados.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaodados_nome3_Internalname, AV27FuncaoDados_Nome3, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", 0, edtavFuncaodados_nome3_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaodados_sistemades3_Internalname, AV28FuncaoDados_SistemaDes3, StringUtil.RTrim( context.localUtil.Format( AV28FuncaoDados_SistemaDes3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaodados_sistemades3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaodados_sistemades3_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_76_9B2e( true) ;
         }
         else
         {
            wb_table10_76_9B2e( false) ;
         }
      }

      protected void wb_table9_58_9B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "", true, "HLP_WWFuncaoDados.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV21DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaodados_nome2_Internalname, AV22FuncaoDados_Nome2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", 0, edtavFuncaodados_nome2_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaodados_sistemades2_Internalname, AV23FuncaoDados_SistemaDes2, StringUtil.RTrim( context.localUtil.Format( AV23FuncaoDados_SistemaDes2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaodados_sistemades2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaodados_sistemades2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_58_9B2e( true) ;
         }
         else
         {
            wb_table9_58_9B2e( false) ;
         }
      }

      protected void wb_table8_40_9B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_WWFuncaoDados.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaodados_nome1_Internalname, AV17FuncaoDados_Nome1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", 0, edtavFuncaodados_nome1_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_WWFuncaoDados.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaodados_sistemades1_Internalname, AV18FuncaoDados_SistemaDes1, StringUtil.RTrim( context.localUtil.Format( AV18FuncaoDados_SistemaDes1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaodados_sistemades1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaodados_sistemades1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWFuncaoDados.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_40_9B2e( true) ;
         }
         else
         {
            wb_table8_40_9B2e( false) ;
         }
      }

      protected void wb_table5_13_9B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_9B2e( true) ;
         }
         else
         {
            wb_table5_13_9B2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9B2( ) ;
         WS9B2( ) ;
         WE9B2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117385497");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwfuncaodados.js", "?20203117385497");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_942( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_94_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_94_idx;
         edtFuncaoDados_Codigo_Internalname = "FUNCAODADOS_CODIGO_"+sGXsfl_94_idx;
         edtFuncaoDados_Nome_Internalname = "FUNCAODADOS_NOME_"+sGXsfl_94_idx;
         cmbFuncaoDados_Tipo_Internalname = "FUNCAODADOS_TIPO_"+sGXsfl_94_idx;
         cmbFuncaoDados_Complexidade_Internalname = "FUNCAODADOS_COMPLEXIDADE_"+sGXsfl_94_idx;
         edtFuncaoDados_DER_Internalname = "FUNCAODADOS_DER_"+sGXsfl_94_idx;
         edtFuncaoDados_RLR_Internalname = "FUNCAODADOS_RLR_"+sGXsfl_94_idx;
         edtFuncaoDados_PF_Internalname = "FUNCAODADOS_PF_"+sGXsfl_94_idx;
         cmbFuncaoDados_Ativo_Internalname = "FUNCAODADOS_ATIVO_"+sGXsfl_94_idx;
      }

      protected void SubsflControlProps_fel_942( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_94_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_94_fel_idx;
         edtFuncaoDados_Codigo_Internalname = "FUNCAODADOS_CODIGO_"+sGXsfl_94_fel_idx;
         edtFuncaoDados_Nome_Internalname = "FUNCAODADOS_NOME_"+sGXsfl_94_fel_idx;
         cmbFuncaoDados_Tipo_Internalname = "FUNCAODADOS_TIPO_"+sGXsfl_94_fel_idx;
         cmbFuncaoDados_Complexidade_Internalname = "FUNCAODADOS_COMPLEXIDADE_"+sGXsfl_94_fel_idx;
         edtFuncaoDados_DER_Internalname = "FUNCAODADOS_DER_"+sGXsfl_94_fel_idx;
         edtFuncaoDados_RLR_Internalname = "FUNCAODADOS_RLR_"+sGXsfl_94_fel_idx;
         edtFuncaoDados_PF_Internalname = "FUNCAODADOS_PF_"+sGXsfl_94_fel_idx;
         cmbFuncaoDados_Ativo_Internalname = "FUNCAODADOS_ATIVO_"+sGXsfl_94_fel_idx;
      }

      protected void sendrow_942( )
      {
         SubsflControlProps_942( ) ;
         WB9B0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_94_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_94_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_94_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV31Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV31Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV98Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV31Update)) ? AV98Update_GXI : context.PathToRelativeUrl( AV31Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV31Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV32Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV99Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Delete)) ? AV99Delete_GXI : context.PathToRelativeUrl( AV32Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV32Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A368FuncaoDados_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_Nome_Internalname,(String)A369FuncaoDados_Nome,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtFuncaoDados_Nome_Link,(String)"",(String)"",(String)"",(String)edtFuncaoDados_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_94_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAODADOS_TIPO_" + sGXsfl_94_idx;
               cmbFuncaoDados_Tipo.Name = GXCCtl;
               cmbFuncaoDados_Tipo.WebTags = "";
               cmbFuncaoDados_Tipo.addItem("", "(Nenhum)", 0);
               cmbFuncaoDados_Tipo.addItem("ALI", "ALI", 0);
               cmbFuncaoDados_Tipo.addItem("AIE", "AIE", 0);
               cmbFuncaoDados_Tipo.addItem("DC", "DC", 0);
               if ( cmbFuncaoDados_Tipo.ItemCount > 0 )
               {
                  A373FuncaoDados_Tipo = cmbFuncaoDados_Tipo.getValidValue(A373FuncaoDados_Tipo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Tipo,(String)cmbFuncaoDados_Tipo_Internalname,StringUtil.RTrim( A373FuncaoDados_Tipo),(short)1,(String)cmbFuncaoDados_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoDados_Tipo.CurrentValue = StringUtil.RTrim( A373FuncaoDados_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Tipo_Internalname, "Values", (String)(cmbFuncaoDados_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "FUNCAODADOS_COMPLEXIDADE_" + sGXsfl_94_idx;
            cmbFuncaoDados_Complexidade.Name = GXCCtl;
            cmbFuncaoDados_Complexidade.WebTags = "";
            cmbFuncaoDados_Complexidade.addItem("E", "", 0);
            cmbFuncaoDados_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoDados_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoDados_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoDados_Complexidade.ItemCount > 0 )
            {
               A376FuncaoDados_Complexidade = cmbFuncaoDados_Complexidade.getValidValue(A376FuncaoDados_Complexidade);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Complexidade,(String)cmbFuncaoDados_Complexidade_Internalname,StringUtil.RTrim( A376FuncaoDados_Complexidade),(short)1,(String)cmbFuncaoDados_Complexidade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoDados_Complexidade.CurrentValue = StringUtil.RTrim( A376FuncaoDados_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Complexidade_Internalname, "Values", (String)(cmbFuncaoDados_Complexidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_DER_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A374FuncaoDados_DER), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_DER_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_RLR_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A375FuncaoDados_RLR), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_RLR_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoDados_PF_Internalname,StringUtil.LTrim( StringUtil.NToC( A377FuncaoDados_PF, 14, 5, ",", "")),context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoDados_PF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_94_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAODADOS_ATIVO_" + sGXsfl_94_idx;
               cmbFuncaoDados_Ativo.Name = GXCCtl;
               cmbFuncaoDados_Ativo.WebTags = "";
               cmbFuncaoDados_Ativo.addItem("A", "Ativa", 0);
               cmbFuncaoDados_Ativo.addItem("E", "Exclu�da", 0);
               cmbFuncaoDados_Ativo.addItem("R", "Rejeitada", 0);
               if ( cmbFuncaoDados_Ativo.ItemCount > 0 )
               {
                  A394FuncaoDados_Ativo = cmbFuncaoDados_Ativo.getValidValue(A394FuncaoDados_Ativo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoDados_Ativo,(String)cmbFuncaoDados_Ativo_Internalname,StringUtil.RTrim( A394FuncaoDados_Ativo),(short)1,(String)cmbFuncaoDados_Ativo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoDados_Ativo.CurrentValue = StringUtil.RTrim( A394FuncaoDados_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbFuncaoDados_Ativo_Internalname, "Values", (String)(cmbFuncaoDados_Ativo.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_CODIGO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A368FuncaoDados_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_NOME"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A369FuncaoDados_Nome, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_TIPO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A373FuncaoDados_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_COMPLEXIDADE"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A376FuncaoDados_Complexidade, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_DER"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A374FuncaoDados_DER), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_RLR"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A375FuncaoDados_RLR), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_PF"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( A377FuncaoDados_PF, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, "gxhash_FUNCAODADOS_ATIVO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A394FuncaoDados_Ativo, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         /* End function sendrow_942 */
      }

      protected void init_default_properties( )
      {
         lblFuncaodadostitle_Internalname = "FUNCAODADOSTITLE";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextfuncaodados_sistemaareacod_Internalname = "FILTERTEXTFUNCAODADOS_SISTEMAAREACOD";
         edtavFuncaodados_sistemaareacod_Internalname = "vFUNCAODADOS_SISTEMAAREACOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavFuncaodados_nome1_Internalname = "vFUNCAODADOS_NOME1";
         edtavFuncaodados_sistemades1_Internalname = "vFUNCAODADOS_SISTEMADES1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavFuncaodados_nome2_Internalname = "vFUNCAODADOS_NOME2";
         edtavFuncaodados_sistemades2_Internalname = "vFUNCAODADOS_SISTEMADES2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavFuncaodados_nome3_Internalname = "vFUNCAODADOS_NOME3";
         edtavFuncaodados_sistemades3_Internalname = "vFUNCAODADOS_SISTEMADES3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtFuncaoDados_Codigo_Internalname = "FUNCAODADOS_CODIGO";
         edtFuncaoDados_Nome_Internalname = "FUNCAODADOS_NOME";
         cmbFuncaoDados_Tipo_Internalname = "FUNCAODADOS_TIPO";
         cmbFuncaoDados_Complexidade_Internalname = "FUNCAODADOS_COMPLEXIDADE";
         edtFuncaoDados_DER_Internalname = "FUNCAODADOS_DER";
         edtFuncaoDados_RLR_Internalname = "FUNCAODADOS_RLR";
         edtFuncaoDados_PF_Internalname = "FUNCAODADOS_PF";
         cmbFuncaoDados_Ativo_Internalname = "FUNCAODADOS_ATIVO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTffuncaodados_nome_Internalname = "vTFFUNCAODADOS_NOME";
         edtavTffuncaodados_nome_sel_Internalname = "vTFFUNCAODADOS_NOME_SEL";
         edtavTffuncaodados_der_Internalname = "vTFFUNCAODADOS_DER";
         edtavTffuncaodados_der_to_Internalname = "vTFFUNCAODADOS_DER_TO";
         edtavTffuncaodados_rlr_Internalname = "vTFFUNCAODADOS_RLR";
         edtavTffuncaodados_rlr_to_Internalname = "vTFFUNCAODADOS_RLR_TO";
         edtavTffuncaodados_pf_Internalname = "vTFFUNCAODADOS_PF";
         edtavTffuncaodados_pf_to_Internalname = "vTFFUNCAODADOS_PF_TO";
         Ddo_funcaodados_nome_Internalname = "DDO_FUNCAODADOS_NOME";
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_tipo_Internalname = "DDO_FUNCAODADOS_TIPO";
         edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_complexidade_Internalname = "DDO_FUNCAODADOS_COMPLEXIDADE";
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_der_Internalname = "DDO_FUNCAODADOS_DER";
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_rlr_Internalname = "DDO_FUNCAODADOS_RLR";
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_pf_Internalname = "DDO_FUNCAODADOS_PF";
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE";
         Ddo_funcaodados_ativo_Internalname = "DDO_FUNCAODADOS_ATIVO";
         edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname = "vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbFuncaoDados_Ativo_Jsonclick = "";
         edtFuncaoDados_PF_Jsonclick = "";
         edtFuncaoDados_RLR_Jsonclick = "";
         edtFuncaoDados_DER_Jsonclick = "";
         cmbFuncaoDados_Complexidade_Jsonclick = "";
         cmbFuncaoDados_Tipo_Jsonclick = "";
         edtFuncaoDados_Nome_Jsonclick = "";
         edtFuncaoDados_Codigo_Jsonclick = "";
         edtavFuncaodados_sistemades1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavFuncaodados_sistemades2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavFuncaodados_sistemades3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavFuncaodados_sistemaareacod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtFuncaoDados_Nome_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         cmbFuncaoDados_Ativo_Titleformat = 0;
         edtFuncaoDados_PF_Titleformat = 0;
         edtFuncaoDados_RLR_Titleformat = 0;
         edtFuncaoDados_DER_Titleformat = 0;
         cmbFuncaoDados_Complexidade_Titleformat = 0;
         cmbFuncaoDados_Tipo_Titleformat = 0;
         edtFuncaoDados_Nome_Titleformat = 0;
         edtavUpdate_Visible = -1;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavFuncaodados_sistemades3_Visible = 1;
         edtavFuncaodados_nome3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavFuncaodados_sistemades2_Visible = 1;
         edtavFuncaodados_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavFuncaodados_sistemades1_Visible = 1;
         edtavFuncaodados_nome1_Visible = 1;
         cmbFuncaoDados_Ativo.Title.Text = "Status";
         edtFuncaoDados_PF_Title = "PF";
         edtFuncaoDados_RLR_Title = "RLR";
         edtFuncaoDados_DER_Title = "DER";
         cmbFuncaoDados_Complexidade.Title.Text = "Complexidade";
         cmbFuncaoDados_Tipo.Title.Text = "Tipo";
         edtFuncaoDados_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaodados_pf_to_Jsonclick = "";
         edtavTffuncaodados_pf_to_Visible = 1;
         edtavTffuncaodados_pf_Jsonclick = "";
         edtavTffuncaodados_pf_Visible = 1;
         edtavTffuncaodados_rlr_to_Jsonclick = "";
         edtavTffuncaodados_rlr_to_Visible = 1;
         edtavTffuncaodados_rlr_Jsonclick = "";
         edtavTffuncaodados_rlr_Visible = 1;
         edtavTffuncaodados_der_to_Jsonclick = "";
         edtavTffuncaodados_der_to_Visible = 1;
         edtavTffuncaodados_der_Jsonclick = "";
         edtavTffuncaodados_der_Visible = 1;
         edtavTffuncaodados_nome_sel_Visible = 1;
         edtavTffuncaodados_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_funcaodados_ativo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaodados_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_ativo_Datalistfixedvalues = "A:Ativa,E:Exclu�da,R:Rejeitada";
         Ddo_funcaodados_ativo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Datalisttype = "FixedValues";
         Ddo_funcaodados_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaodados_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaodados_ativo_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_ativo_Cls = "ColumnSettings";
         Ddo_funcaodados_ativo_Tooltip = "Op��es";
         Ddo_funcaodados_ativo_Caption = "";
         Ddo_funcaodados_pf_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_pf_Rangefilterto = "At�";
         Ddo_funcaodados_pf_Rangefilterfrom = "Desde";
         Ddo_funcaodados_pf_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_pf_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_pf_Filtertype = "Numeric";
         Ddo_funcaodados_pf_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_pf_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_pf_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_pf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_pf_Cls = "ColumnSettings";
         Ddo_funcaodados_pf_Tooltip = "Op��es";
         Ddo_funcaodados_pf_Caption = "";
         Ddo_funcaodados_rlr_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_rlr_Rangefilterto = "At�";
         Ddo_funcaodados_rlr_Rangefilterfrom = "Desde";
         Ddo_funcaodados_rlr_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_rlr_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_rlr_Filtertype = "Numeric";
         Ddo_funcaodados_rlr_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_rlr_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_rlr_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_rlr_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_rlr_Cls = "ColumnSettings";
         Ddo_funcaodados_rlr_Tooltip = "Op��es";
         Ddo_funcaodados_rlr_Caption = "";
         Ddo_funcaodados_der_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_der_Rangefilterto = "At�";
         Ddo_funcaodados_der_Rangefilterfrom = "Desde";
         Ddo_funcaodados_der_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_der_Includedatalist = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Filterisrange = Convert.ToBoolean( -1);
         Ddo_funcaodados_der_Filtertype = "Numeric";
         Ddo_funcaodados_der_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_der_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_der_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_der_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_der_Cls = "ColumnSettings";
         Ddo_funcaodados_der_Tooltip = "Op��es";
         Ddo_funcaodados_der_Caption = "";
         Ddo_funcaodados_complexidade_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaodados_complexidade_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_complexidade_Datalistfixedvalues = "E:,B:Baixa,M:M�dia,A:Alta";
         Ddo_funcaodados_complexidade_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaodados_complexidade_Datalisttype = "FixedValues";
         Ddo_funcaodados_complexidade_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_complexidade_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Includesortdsc = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Includesortasc = Convert.ToBoolean( 0);
         Ddo_funcaodados_complexidade_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_complexidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_complexidade_Cls = "ColumnSettings";
         Ddo_funcaodados_complexidade_Tooltip = "Op��es";
         Ddo_funcaodados_complexidade_Caption = "";
         Ddo_funcaodados_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_funcaodados_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_tipo_Datalistfixedvalues = "ALI:ALI,AIE:AIE,DC:DC";
         Ddo_funcaodados_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Datalisttype = "FixedValues";
         Ddo_funcaodados_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_funcaodados_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaodados_tipo_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_tipo_Cls = "ColumnSettings";
         Ddo_funcaodados_tipo_Tooltip = "Op��es";
         Ddo_funcaodados_tipo_Caption = "";
         Ddo_funcaodados_nome_Searchbuttontext = "Pesquisar";
         Ddo_funcaodados_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaodados_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaodados_nome_Loadingdata = "Carregando dados...";
         Ddo_funcaodados_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaodados_nome_Sortasc = "Ordenar de A � Z";
         Ddo_funcaodados_nome_Datalistupdateminimumcharacters = 0;
         Ddo_funcaodados_nome_Datalistproc = "GetWWFuncaoDadosFilterData";
         Ddo_funcaodados_nome_Datalisttype = "Dynamic";
         Ddo_funcaodados_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaodados_nome_Filtertype = "Character";
         Ddo_funcaodados_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaodados_nome_Titlecontrolidtoreplace = "";
         Ddo_funcaodados_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaodados_nome_Cls = "ColumnSettings";
         Ddo_funcaodados_nome_Tooltip = "Op��es";
         Ddo_funcaodados_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Grupo L�gico de Dados";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV38FuncaoDados_NomeTitleFilterData',fld:'vFUNCAODADOS_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV42FuncaoDados_TipoTitleFilterData',fld:'vFUNCAODADOS_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46FuncaoDados_ComplexidadeTitleFilterData',fld:'vFUNCAODADOS_COMPLEXIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV50FuncaoDados_DERTitleFilterData',fld:'vFUNCAODADOS_DERTITLEFILTERDATA',pic:'',nv:null},{av:'AV54FuncaoDados_RLRTitleFilterData',fld:'vFUNCAODADOS_RLRTITLEFILTERDATA',pic:'',nv:null},{av:'AV58FuncaoDados_PFTitleFilterData',fld:'vFUNCAODADOS_PFTITLEFILTERDATA',pic:'',nv:null},{av:'AV62FuncaoDados_AtivoTitleFilterData',fld:'vFUNCAODADOS_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtFuncaoDados_Nome_Titleformat',ctrl:'FUNCAODADOS_NOME',prop:'Titleformat'},{av:'edtFuncaoDados_Nome_Title',ctrl:'FUNCAODADOS_NOME',prop:'Title'},{av:'cmbFuncaoDados_Tipo'},{av:'cmbFuncaoDados_Complexidade'},{av:'edtFuncaoDados_DER_Titleformat',ctrl:'FUNCAODADOS_DER',prop:'Titleformat'},{av:'edtFuncaoDados_DER_Title',ctrl:'FUNCAODADOS_DER',prop:'Title'},{av:'edtFuncaoDados_RLR_Titleformat',ctrl:'FUNCAODADOS_RLR',prop:'Titleformat'},{av:'edtFuncaoDados_RLR_Title',ctrl:'FUNCAODADOS_RLR',prop:'Title'},{av:'edtFuncaoDados_PF_Titleformat',ctrl:'FUNCAODADOS_PF',prop:'Titleformat'},{av:'edtFuncaoDados_PF_Title',ctrl:'FUNCAODADOS_PF',prop:'Title'},{av:'cmbFuncaoDados_Ativo'},{av:'AV68GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV69GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E119B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAODADOS_NOME.ONOPTIONCLICKED","{handler:'E129B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaodados_nome_Activeeventkey',ctrl:'DDO_FUNCAODADOS_NOME',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_nome_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_NOME',prop:'FilteredText_get'},{av:'Ddo_funcaodados_nome_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaodados_nome_Sortedstatus',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SortedStatus'},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaodados_tipo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SortedStatus'},{av:'Ddo_funcaodados_ativo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAODADOS_TIPO.ONOPTIONCLICKED","{handler:'E139B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaodados_tipo_Activeeventkey',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_tipo_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaodados_tipo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SortedStatus'},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_nome_Sortedstatus',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcaodados_ativo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAODADOS_COMPLEXIDADE.ONOPTIONCLICKED","{handler:'E149B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaodados_complexidade_Activeeventkey',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_complexidade_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'SelectedValue_get'}],oparms:[{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null}]}");
         setEventMetadata("DDO_FUNCAODADOS_DER.ONOPTIONCLICKED","{handler:'E159B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaodados_der_Activeeventkey',ctrl:'DDO_FUNCAODADOS_DER',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_der_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredText_get'},{av:'Ddo_funcaodados_der_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredTextTo_get'}],oparms:[{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAODADOS_RLR.ONOPTIONCLICKED","{handler:'E169B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaodados_rlr_Activeeventkey',ctrl:'DDO_FUNCAODADOS_RLR',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_rlr_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredText_get'},{av:'Ddo_funcaodados_rlr_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("DDO_FUNCAODADOS_PF.ONOPTIONCLICKED","{handler:'E179B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaodados_pf_Activeeventkey',ctrl:'DDO_FUNCAODADOS_PF',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_pf_Filteredtext_get',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredText_get'},{av:'Ddo_funcaodados_pf_Filteredtextto_get',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredTextTo_get'}],oparms:[{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0}]}");
         setEventMetadata("DDO_FUNCAODADOS_ATIVO.ONOPTIONCLICKED","{handler:'E189B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_funcaodados_ativo_Activeeventkey',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_funcaodados_ativo_Selectedvalue_get',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaodados_ativo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SortedStatus'},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_nome_Sortedstatus',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SortedStatus'},{av:'Ddo_funcaodados_tipo_Sortedstatus',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E319B2',iparms:[{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV31Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV32Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'edtFuncaoDados_Nome_Link',ctrl:'FUNCAODADOS_NOME',prop:'Link'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E199B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E249B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E209B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'},{av:'edtavFuncaodados_sistemades2_Visible',ctrl:'vFUNCAODADOS_SISTEMADES2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaodados_nome3_Visible',ctrl:'vFUNCAODADOS_NOME3',prop:'Visible'},{av:'edtavFuncaodados_sistemades3_Visible',ctrl:'vFUNCAODADOS_SISTEMADES3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'},{av:'edtavFuncaodados_sistemades1_Visible',ctrl:'vFUNCAODADOS_SISTEMADES1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E259B2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'},{av:'edtavFuncaodados_sistemades1_Visible',ctrl:'vFUNCAODADOS_SISTEMADES1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E269B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E219B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'},{av:'edtavFuncaodados_sistemades2_Visible',ctrl:'vFUNCAODADOS_SISTEMADES2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaodados_nome3_Visible',ctrl:'vFUNCAODADOS_NOME3',prop:'Visible'},{av:'edtavFuncaodados_sistemades3_Visible',ctrl:'vFUNCAODADOS_SISTEMADES3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'},{av:'edtavFuncaodados_sistemades1_Visible',ctrl:'vFUNCAODADOS_SISTEMADES1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E279B2',iparms:[{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'},{av:'edtavFuncaodados_sistemades2_Visible',ctrl:'vFUNCAODADOS_SISTEMADES2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E229B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'},{av:'edtavFuncaodados_sistemades2_Visible',ctrl:'vFUNCAODADOS_SISTEMADES2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaodados_nome3_Visible',ctrl:'vFUNCAODADOS_NOME3',prop:'Visible'},{av:'edtavFuncaodados_sistemades3_Visible',ctrl:'vFUNCAODADOS_SISTEMADES3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'},{av:'edtavFuncaodados_sistemades1_Visible',ctrl:'vFUNCAODADOS_SISTEMADES1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E289B2',iparms:[{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavFuncaodados_nome3_Visible',ctrl:'vFUNCAODADOS_NOME3',prop:'Visible'},{av:'edtavFuncaodados_sistemades3_Visible',ctrl:'vFUNCAODADOS_SISTEMADES3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E239B2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV35OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'AV41ddo_FuncaoDados_NomeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_FuncaoDados_TipoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_COMPLEXIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoDados_DERTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_DERTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV57ddo_FuncaoDados_RLRTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_RLRTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_FuncaoDados_PFTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_PFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace',fld:'vDDO_FUNCAODADOS_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'AV100Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV29DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A368FuncaoDados_Codigo',fld:'FUNCAODADOS_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV34FuncaoDados_SistemaCod',fld:'vFUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A370FuncaoDados_SistemaCod',fld:'FUNCAODADOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV14FuncaoDados_SistemaAreaCod',fld:'vFUNCAODADOS_SISTEMAAREACOD',pic:'ZZZZZ9',nv:0},{av:'AV39TFFuncaoDados_Nome',fld:'vTFFUNCAODADOS_NOME',pic:'',nv:''},{av:'Ddo_funcaodados_nome_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_NOME',prop:'FilteredText_set'},{av:'AV40TFFuncaoDados_Nome_Sel',fld:'vTFFUNCAODADOS_NOME_SEL',pic:'',nv:''},{av:'Ddo_funcaodados_nome_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_NOME',prop:'SelectedValue_set'},{av:'AV44TFFuncaoDados_Tipo_Sels',fld:'vTFFUNCAODADOS_TIPO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_tipo_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_TIPO',prop:'SelectedValue_set'},{av:'AV48TFFuncaoDados_Complexidade_Sels',fld:'vTFFUNCAODADOS_COMPLEXIDADE_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_complexidade_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_COMPLEXIDADE',prop:'SelectedValue_set'},{av:'AV51TFFuncaoDados_DER',fld:'vTFFUNCAODADOS_DER',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_der_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredText_set'},{av:'AV52TFFuncaoDados_DER_To',fld:'vTFFUNCAODADOS_DER_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_der_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_DER',prop:'FilteredTextTo_set'},{av:'AV55TFFuncaoDados_RLR',fld:'vTFFUNCAODADOS_RLR',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_rlr_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredText_set'},{av:'AV56TFFuncaoDados_RLR_To',fld:'vTFFUNCAODADOS_RLR_TO',pic:'ZZZ9',nv:0},{av:'Ddo_funcaodados_rlr_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_RLR',prop:'FilteredTextTo_set'},{av:'AV59TFFuncaoDados_PF',fld:'vTFFUNCAODADOS_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaodados_pf_Filteredtext_set',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredText_set'},{av:'AV60TFFuncaoDados_PF_To',fld:'vTFFUNCAODADOS_PF_TO',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'Ddo_funcaodados_pf_Filteredtextto_set',ctrl:'DDO_FUNCAODADOS_PF',prop:'FilteredTextTo_set'},{av:'AV64TFFuncaoDados_Ativo_Sels',fld:'vTFFUNCAODADOS_ATIVO_SELS',pic:'',nv:null},{av:'Ddo_funcaodados_ativo_Selectedvalue_set',ctrl:'DDO_FUNCAODADOS_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17FuncaoDados_Nome1',fld:'vFUNCAODADOS_NOME1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaodados_nome1_Visible',ctrl:'vFUNCAODADOS_NOME1',prop:'Visible'},{av:'edtavFuncaodados_sistemades1_Visible',ctrl:'vFUNCAODADOS_SISTEMADES1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV19DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV20DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22FuncaoDados_Nome2',fld:'vFUNCAODADOS_NOME2',pic:'',nv:''},{av:'AV24DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV25DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV26DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV27FuncaoDados_Nome3',fld:'vFUNCAODADOS_NOME3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV18FuncaoDados_SistemaDes1',fld:'vFUNCAODADOS_SISTEMADES1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV23FuncaoDados_SistemaDes2',fld:'vFUNCAODADOS_SISTEMADES2',pic:'@!',nv:''},{av:'AV28FuncaoDados_SistemaDes3',fld:'vFUNCAODADOS_SISTEMADES3',pic:'@!',nv:''},{av:'edtavFuncaodados_nome2_Visible',ctrl:'vFUNCAODADOS_NOME2',prop:'Visible'},{av:'edtavFuncaodados_sistemades2_Visible',ctrl:'vFUNCAODADOS_SISTEMADES2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavFuncaodados_nome3_Visible',ctrl:'vFUNCAODADOS_NOME3',prop:'Visible'},{av:'edtavFuncaodados_sistemades3_Visible',ctrl:'vFUNCAODADOS_SISTEMADES3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcaodados_nome_Activeeventkey = "";
         Ddo_funcaodados_nome_Filteredtext_get = "";
         Ddo_funcaodados_nome_Selectedvalue_get = "";
         Ddo_funcaodados_tipo_Activeeventkey = "";
         Ddo_funcaodados_tipo_Selectedvalue_get = "";
         Ddo_funcaodados_complexidade_Activeeventkey = "";
         Ddo_funcaodados_complexidade_Selectedvalue_get = "";
         Ddo_funcaodados_der_Activeeventkey = "";
         Ddo_funcaodados_der_Filteredtext_get = "";
         Ddo_funcaodados_der_Filteredtextto_get = "";
         Ddo_funcaodados_rlr_Activeeventkey = "";
         Ddo_funcaodados_rlr_Filteredtext_get = "";
         Ddo_funcaodados_rlr_Filteredtextto_get = "";
         Ddo_funcaodados_pf_Activeeventkey = "";
         Ddo_funcaodados_pf_Filteredtext_get = "";
         Ddo_funcaodados_pf_Filteredtextto_get = "";
         Ddo_funcaodados_ativo_Activeeventkey = "";
         Ddo_funcaodados_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17FuncaoDados_Nome1 = "";
         AV18FuncaoDados_SistemaDes1 = "";
         AV20DynamicFiltersSelector2 = "";
         AV22FuncaoDados_Nome2 = "";
         AV23FuncaoDados_SistemaDes2 = "";
         AV25DynamicFiltersSelector3 = "";
         AV27FuncaoDados_Nome3 = "";
         AV28FuncaoDados_SistemaDes3 = "";
         AV39TFFuncaoDados_Nome = "";
         AV40TFFuncaoDados_Nome_Sel = "";
         AV41ddo_FuncaoDados_NomeTitleControlIdToReplace = "";
         AV45ddo_FuncaoDados_TipoTitleControlIdToReplace = "";
         AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace = "";
         AV53ddo_FuncaoDados_DERTitleControlIdToReplace = "";
         AV57ddo_FuncaoDados_RLRTitleControlIdToReplace = "";
         AV61ddo_FuncaoDados_PFTitleControlIdToReplace = "";
         AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace = "";
         AV44TFFuncaoDados_Tipo_Sels = new GxSimpleCollection();
         AV48TFFuncaoDados_Complexidade_Sels = new GxSimpleCollection();
         AV64TFFuncaoDados_Ativo_Sels = new GxSimpleCollection();
         AV100Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV66DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV38FuncaoDados_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42FuncaoDados_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46FuncaoDados_ComplexidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50FuncaoDados_DERTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54FuncaoDados_RLRTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58FuncaoDados_PFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62FuncaoDados_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaodados_nome_Filteredtext_set = "";
         Ddo_funcaodados_nome_Selectedvalue_set = "";
         Ddo_funcaodados_nome_Sortedstatus = "";
         Ddo_funcaodados_tipo_Selectedvalue_set = "";
         Ddo_funcaodados_tipo_Sortedstatus = "";
         Ddo_funcaodados_complexidade_Selectedvalue_set = "";
         Ddo_funcaodados_der_Filteredtext_set = "";
         Ddo_funcaodados_der_Filteredtextto_set = "";
         Ddo_funcaodados_rlr_Filteredtext_set = "";
         Ddo_funcaodados_rlr_Filteredtextto_set = "";
         Ddo_funcaodados_pf_Filteredtext_set = "";
         Ddo_funcaodados_pf_Filteredtextto_set = "";
         Ddo_funcaodados_ativo_Selectedvalue_set = "";
         Ddo_funcaodados_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV31Update = "";
         AV98Update_GXI = "";
         AV32Delete = "";
         AV99Delete_GXI = "";
         A369FuncaoDados_Nome = "";
         A373FuncaoDados_Tipo = "";
         A376FuncaoDados_Complexidade = "";
         A394FuncaoDados_Ativo = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels = new GxSimpleCollection();
         AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels = new GxSimpleCollection();
         AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV75WWFuncaoDadosDS_4_Funcaodados_nome1 = "";
         lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 = "";
         lV80WWFuncaoDadosDS_9_Funcaodados_nome2 = "";
         lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 = "";
         lV85WWFuncaoDadosDS_14_Funcaodados_nome3 = "";
         lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 = "";
         lV87WWFuncaoDadosDS_16_Tffuncaodados_nome = "";
         AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1 = "";
         AV75WWFuncaoDadosDS_4_Funcaodados_nome1 = "";
         AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 = "";
         AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2 = "";
         AV80WWFuncaoDadosDS_9_Funcaodados_nome2 = "";
         AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 = "";
         AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3 = "";
         AV85WWFuncaoDadosDS_14_Funcaodados_nome3 = "";
         AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 = "";
         AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel = "";
         AV87WWFuncaoDadosDS_16_Tffuncaodados_nome = "";
         A371FuncaoDados_SistemaDes = "";
         H009B2_A371FuncaoDados_SistemaDes = new String[] {""} ;
         H009B2_n371FuncaoDados_SistemaDes = new bool[] {false} ;
         H009B2_A372FuncaoDados_SistemaAreaCod = new int[1] ;
         H009B2_n372FuncaoDados_SistemaAreaCod = new bool[] {false} ;
         H009B2_A370FuncaoDados_SistemaCod = new int[1] ;
         H009B2_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         H009B2_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         H009B2_A394FuncaoDados_Ativo = new String[] {""} ;
         H009B2_A373FuncaoDados_Tipo = new String[] {""} ;
         H009B2_A369FuncaoDados_Nome = new String[] {""} ;
         H009B2_A755FuncaoDados_Contar = new bool[] {false} ;
         H009B2_n755FuncaoDados_Contar = new bool[] {false} ;
         H009B2_A368FuncaoDados_Codigo = new int[1] ;
         GXt_char1 = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV43TFFuncaoDados_Tipo_SelsJson = "";
         AV47TFFuncaoDados_Complexidade_SelsJson = "";
         AV63TFFuncaoDados_Ativo_SelsJson = "";
         GridRow = new GXWebRow();
         AV33Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFuncaodadostitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextfuncaodados_sistemaareacod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwfuncaodados__default(),
            new Object[][] {
                new Object[] {
               H009B2_A371FuncaoDados_SistemaDes, H009B2_n371FuncaoDados_SistemaDes, H009B2_A372FuncaoDados_SistemaAreaCod, H009B2_n372FuncaoDados_SistemaAreaCod, H009B2_A370FuncaoDados_SistemaCod, H009B2_A391FuncaoDados_FuncaoDadosCod, H009B2_n391FuncaoDados_FuncaoDadosCod, H009B2_A394FuncaoDados_Ativo, H009B2_A373FuncaoDados_Tipo, H009B2_A369FuncaoDados_Nome,
               H009B2_A755FuncaoDados_Contar, H009B2_n755FuncaoDados_Contar, H009B2_A368FuncaoDados_Codigo
               }
            }
         );
         AV100Pgmname = "WWFuncaoDados";
         /* GeneXus formulas. */
         AV100Pgmname = "WWFuncaoDados";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_94 ;
      private short nGXsfl_94_idx=1 ;
      private short AV35OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV21DynamicFiltersOperator2 ;
      private short AV26DynamicFiltersOperator3 ;
      private short AV51TFFuncaoDados_DER ;
      private short AV52TFFuncaoDados_DER_To ;
      private short AV55TFFuncaoDados_RLR ;
      private short AV56TFFuncaoDados_RLR_To ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A374FuncaoDados_DER ;
      private short A375FuncaoDados_RLR ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_94_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 ;
      private short AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 ;
      private short AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 ;
      private short AV91WWFuncaoDadosDS_20_Tffuncaodados_der ;
      private short AV92WWFuncaoDadosDS_21_Tffuncaodados_der_to ;
      private short AV93WWFuncaoDadosDS_22_Tffuncaodados_rlr ;
      private short AV94WWFuncaoDadosDS_23_Tffuncaodados_rlr_to ;
      private short GXt_int2 ;
      private short edtFuncaoDados_Nome_Titleformat ;
      private short cmbFuncaoDados_Tipo_Titleformat ;
      private short cmbFuncaoDados_Complexidade_Titleformat ;
      private short edtFuncaoDados_DER_Titleformat ;
      private short edtFuncaoDados_RLR_Titleformat ;
      private short edtFuncaoDados_PF_Titleformat ;
      private short cmbFuncaoDados_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV14FuncaoDados_SistemaAreaCod ;
      private int A368FuncaoDados_Codigo ;
      private int AV34FuncaoDados_SistemaCod ;
      private int A370FuncaoDados_SistemaCod ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcaodados_nome_Datalistupdateminimumcharacters ;
      private int edtavTffuncaodados_nome_Visible ;
      private int edtavTffuncaodados_nome_sel_Visible ;
      private int edtavTffuncaodados_der_Visible ;
      private int edtavTffuncaodados_der_to_Visible ;
      private int edtavTffuncaodados_rlr_Visible ;
      private int edtavTffuncaodados_rlr_to_Visible ;
      private int edtavTffuncaodados_pf_Visible ;
      private int edtavTffuncaodados_pf_to_Visible ;
      private int edtavDdo_funcaodados_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_dertitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_pftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels_Count ;
      private int AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels_Count ;
      private int AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels_Count ;
      private int AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod ;
      private int A372FuncaoDados_SistemaAreaCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV67PageToGo ;
      private int edtavUpdate_Visible ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavFuncaodados_nome1_Visible ;
      private int edtavFuncaodados_sistemades1_Visible ;
      private int edtavFuncaodados_nome2_Visible ;
      private int edtavFuncaodados_sistemades2_Visible ;
      private int edtavFuncaodados_nome3_Visible ;
      private int edtavFuncaodados_sistemades3_Visible ;
      private int AV101GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV68GridCurrentPage ;
      private long AV69GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV59TFFuncaoDados_PF ;
      private decimal AV60TFFuncaoDados_PF_To ;
      private decimal A377FuncaoDados_PF ;
      private decimal AV95WWFuncaoDadosDS_24_Tffuncaodados_pf ;
      private decimal AV96WWFuncaoDadosDS_25_Tffuncaodados_pf_to ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcaodados_nome_Activeeventkey ;
      private String Ddo_funcaodados_nome_Filteredtext_get ;
      private String Ddo_funcaodados_nome_Selectedvalue_get ;
      private String Ddo_funcaodados_tipo_Activeeventkey ;
      private String Ddo_funcaodados_tipo_Selectedvalue_get ;
      private String Ddo_funcaodados_complexidade_Activeeventkey ;
      private String Ddo_funcaodados_complexidade_Selectedvalue_get ;
      private String Ddo_funcaodados_der_Activeeventkey ;
      private String Ddo_funcaodados_der_Filteredtext_get ;
      private String Ddo_funcaodados_der_Filteredtextto_get ;
      private String Ddo_funcaodados_rlr_Activeeventkey ;
      private String Ddo_funcaodados_rlr_Filteredtext_get ;
      private String Ddo_funcaodados_rlr_Filteredtextto_get ;
      private String Ddo_funcaodados_pf_Activeeventkey ;
      private String Ddo_funcaodados_pf_Filteredtext_get ;
      private String Ddo_funcaodados_pf_Filteredtextto_get ;
      private String Ddo_funcaodados_ativo_Activeeventkey ;
      private String Ddo_funcaodados_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_94_idx="0001" ;
      private String AV100Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcaodados_nome_Caption ;
      private String Ddo_funcaodados_nome_Tooltip ;
      private String Ddo_funcaodados_nome_Cls ;
      private String Ddo_funcaodados_nome_Filteredtext_set ;
      private String Ddo_funcaodados_nome_Selectedvalue_set ;
      private String Ddo_funcaodados_nome_Dropdownoptionstype ;
      private String Ddo_funcaodados_nome_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_nome_Sortedstatus ;
      private String Ddo_funcaodados_nome_Filtertype ;
      private String Ddo_funcaodados_nome_Datalisttype ;
      private String Ddo_funcaodados_nome_Datalistproc ;
      private String Ddo_funcaodados_nome_Sortasc ;
      private String Ddo_funcaodados_nome_Sortdsc ;
      private String Ddo_funcaodados_nome_Loadingdata ;
      private String Ddo_funcaodados_nome_Cleanfilter ;
      private String Ddo_funcaodados_nome_Noresultsfound ;
      private String Ddo_funcaodados_nome_Searchbuttontext ;
      private String Ddo_funcaodados_tipo_Caption ;
      private String Ddo_funcaodados_tipo_Tooltip ;
      private String Ddo_funcaodados_tipo_Cls ;
      private String Ddo_funcaodados_tipo_Selectedvalue_set ;
      private String Ddo_funcaodados_tipo_Dropdownoptionstype ;
      private String Ddo_funcaodados_tipo_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_tipo_Sortedstatus ;
      private String Ddo_funcaodados_tipo_Datalisttype ;
      private String Ddo_funcaodados_tipo_Datalistfixedvalues ;
      private String Ddo_funcaodados_tipo_Sortasc ;
      private String Ddo_funcaodados_tipo_Sortdsc ;
      private String Ddo_funcaodados_tipo_Cleanfilter ;
      private String Ddo_funcaodados_tipo_Searchbuttontext ;
      private String Ddo_funcaodados_complexidade_Caption ;
      private String Ddo_funcaodados_complexidade_Tooltip ;
      private String Ddo_funcaodados_complexidade_Cls ;
      private String Ddo_funcaodados_complexidade_Selectedvalue_set ;
      private String Ddo_funcaodados_complexidade_Dropdownoptionstype ;
      private String Ddo_funcaodados_complexidade_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_complexidade_Datalisttype ;
      private String Ddo_funcaodados_complexidade_Datalistfixedvalues ;
      private String Ddo_funcaodados_complexidade_Cleanfilter ;
      private String Ddo_funcaodados_complexidade_Searchbuttontext ;
      private String Ddo_funcaodados_der_Caption ;
      private String Ddo_funcaodados_der_Tooltip ;
      private String Ddo_funcaodados_der_Cls ;
      private String Ddo_funcaodados_der_Filteredtext_set ;
      private String Ddo_funcaodados_der_Filteredtextto_set ;
      private String Ddo_funcaodados_der_Dropdownoptionstype ;
      private String Ddo_funcaodados_der_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_der_Filtertype ;
      private String Ddo_funcaodados_der_Cleanfilter ;
      private String Ddo_funcaodados_der_Rangefilterfrom ;
      private String Ddo_funcaodados_der_Rangefilterto ;
      private String Ddo_funcaodados_der_Searchbuttontext ;
      private String Ddo_funcaodados_rlr_Caption ;
      private String Ddo_funcaodados_rlr_Tooltip ;
      private String Ddo_funcaodados_rlr_Cls ;
      private String Ddo_funcaodados_rlr_Filteredtext_set ;
      private String Ddo_funcaodados_rlr_Filteredtextto_set ;
      private String Ddo_funcaodados_rlr_Dropdownoptionstype ;
      private String Ddo_funcaodados_rlr_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_rlr_Filtertype ;
      private String Ddo_funcaodados_rlr_Cleanfilter ;
      private String Ddo_funcaodados_rlr_Rangefilterfrom ;
      private String Ddo_funcaodados_rlr_Rangefilterto ;
      private String Ddo_funcaodados_rlr_Searchbuttontext ;
      private String Ddo_funcaodados_pf_Caption ;
      private String Ddo_funcaodados_pf_Tooltip ;
      private String Ddo_funcaodados_pf_Cls ;
      private String Ddo_funcaodados_pf_Filteredtext_set ;
      private String Ddo_funcaodados_pf_Filteredtextto_set ;
      private String Ddo_funcaodados_pf_Dropdownoptionstype ;
      private String Ddo_funcaodados_pf_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_pf_Filtertype ;
      private String Ddo_funcaodados_pf_Cleanfilter ;
      private String Ddo_funcaodados_pf_Rangefilterfrom ;
      private String Ddo_funcaodados_pf_Rangefilterto ;
      private String Ddo_funcaodados_pf_Searchbuttontext ;
      private String Ddo_funcaodados_ativo_Caption ;
      private String Ddo_funcaodados_ativo_Tooltip ;
      private String Ddo_funcaodados_ativo_Cls ;
      private String Ddo_funcaodados_ativo_Selectedvalue_set ;
      private String Ddo_funcaodados_ativo_Dropdownoptionstype ;
      private String Ddo_funcaodados_ativo_Titlecontrolidtoreplace ;
      private String Ddo_funcaodados_ativo_Sortedstatus ;
      private String Ddo_funcaodados_ativo_Datalisttype ;
      private String Ddo_funcaodados_ativo_Datalistfixedvalues ;
      private String Ddo_funcaodados_ativo_Sortasc ;
      private String Ddo_funcaodados_ativo_Sortdsc ;
      private String Ddo_funcaodados_ativo_Cleanfilter ;
      private String Ddo_funcaodados_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTffuncaodados_nome_Internalname ;
      private String edtavTffuncaodados_nome_sel_Internalname ;
      private String edtavTffuncaodados_der_Internalname ;
      private String edtavTffuncaodados_der_Jsonclick ;
      private String edtavTffuncaodados_der_to_Internalname ;
      private String edtavTffuncaodados_der_to_Jsonclick ;
      private String edtavTffuncaodados_rlr_Internalname ;
      private String edtavTffuncaodados_rlr_Jsonclick ;
      private String edtavTffuncaodados_rlr_to_Internalname ;
      private String edtavTffuncaodados_rlr_to_Jsonclick ;
      private String edtavTffuncaodados_pf_Internalname ;
      private String edtavTffuncaodados_pf_Jsonclick ;
      private String edtavTffuncaodados_pf_to_Internalname ;
      private String edtavTffuncaodados_pf_to_Jsonclick ;
      private String edtavDdo_funcaodados_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_complexidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_dertitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_rlrtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_pftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaodados_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtFuncaoDados_Codigo_Internalname ;
      private String edtFuncaoDados_Nome_Internalname ;
      private String cmbFuncaoDados_Tipo_Internalname ;
      private String A373FuncaoDados_Tipo ;
      private String cmbFuncaoDados_Complexidade_Internalname ;
      private String A376FuncaoDados_Complexidade ;
      private String edtFuncaoDados_DER_Internalname ;
      private String edtFuncaoDados_RLR_Internalname ;
      private String edtFuncaoDados_PF_Internalname ;
      private String cmbFuncaoDados_Ativo_Internalname ;
      private String A394FuncaoDados_Ativo ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String GXt_char1 ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavFuncaodados_sistemaareacod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavFuncaodados_nome1_Internalname ;
      private String edtavFuncaodados_sistemades1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavFuncaodados_nome2_Internalname ;
      private String edtavFuncaodados_sistemades2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavFuncaodados_nome3_Internalname ;
      private String edtavFuncaodados_sistemades3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_funcaodados_nome_Internalname ;
      private String Ddo_funcaodados_tipo_Internalname ;
      private String Ddo_funcaodados_complexidade_Internalname ;
      private String Ddo_funcaodados_der_Internalname ;
      private String Ddo_funcaodados_rlr_Internalname ;
      private String Ddo_funcaodados_pf_Internalname ;
      private String Ddo_funcaodados_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtFuncaoDados_Nome_Title ;
      private String edtFuncaoDados_DER_Title ;
      private String edtFuncaoDados_RLR_Title ;
      private String edtFuncaoDados_PF_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtFuncaoDados_Nome_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblFuncaodadostitle_Internalname ;
      private String lblFuncaodadostitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextfuncaodados_sistemaareacod_Internalname ;
      private String lblFiltertextfuncaodados_sistemaareacod_Jsonclick ;
      private String edtavFuncaodados_sistemaareacod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavFuncaodados_sistemades3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavFuncaodados_sistemades2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavFuncaodados_sistemades1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String sGXsfl_94_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncaoDados_Codigo_Jsonclick ;
      private String edtFuncaoDados_Nome_Jsonclick ;
      private String cmbFuncaoDados_Tipo_Jsonclick ;
      private String cmbFuncaoDados_Complexidade_Jsonclick ;
      private String edtFuncaoDados_DER_Jsonclick ;
      private String edtFuncaoDados_RLR_Jsonclick ;
      private String edtFuncaoDados_PF_Jsonclick ;
      private String cmbFuncaoDados_Ativo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool AV19DynamicFiltersEnabled2 ;
      private bool AV24DynamicFiltersEnabled3 ;
      private bool AV30DynamicFiltersIgnoreFirst ;
      private bool AV29DynamicFiltersRemoving ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool toggleJsOutput ;
      private bool A755FuncaoDados_Contar ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcaodados_nome_Includesortasc ;
      private bool Ddo_funcaodados_nome_Includesortdsc ;
      private bool Ddo_funcaodados_nome_Includefilter ;
      private bool Ddo_funcaodados_nome_Filterisrange ;
      private bool Ddo_funcaodados_nome_Includedatalist ;
      private bool Ddo_funcaodados_tipo_Includesortasc ;
      private bool Ddo_funcaodados_tipo_Includesortdsc ;
      private bool Ddo_funcaodados_tipo_Includefilter ;
      private bool Ddo_funcaodados_tipo_Includedatalist ;
      private bool Ddo_funcaodados_tipo_Allowmultipleselection ;
      private bool Ddo_funcaodados_complexidade_Includesortasc ;
      private bool Ddo_funcaodados_complexidade_Includesortdsc ;
      private bool Ddo_funcaodados_complexidade_Includefilter ;
      private bool Ddo_funcaodados_complexidade_Includedatalist ;
      private bool Ddo_funcaodados_complexidade_Allowmultipleselection ;
      private bool Ddo_funcaodados_der_Includesortasc ;
      private bool Ddo_funcaodados_der_Includesortdsc ;
      private bool Ddo_funcaodados_der_Includefilter ;
      private bool Ddo_funcaodados_der_Filterisrange ;
      private bool Ddo_funcaodados_der_Includedatalist ;
      private bool Ddo_funcaodados_rlr_Includesortasc ;
      private bool Ddo_funcaodados_rlr_Includesortdsc ;
      private bool Ddo_funcaodados_rlr_Includefilter ;
      private bool Ddo_funcaodados_rlr_Filterisrange ;
      private bool Ddo_funcaodados_rlr_Includedatalist ;
      private bool Ddo_funcaodados_pf_Includesortasc ;
      private bool Ddo_funcaodados_pf_Includesortdsc ;
      private bool Ddo_funcaodados_pf_Includefilter ;
      private bool Ddo_funcaodados_pf_Filterisrange ;
      private bool Ddo_funcaodados_pf_Includedatalist ;
      private bool Ddo_funcaodados_ativo_Includesortasc ;
      private bool Ddo_funcaodados_ativo_Includesortdsc ;
      private bool Ddo_funcaodados_ativo_Includefilter ;
      private bool Ddo_funcaodados_ativo_Includedatalist ;
      private bool Ddo_funcaodados_ativo_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 ;
      private bool AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 ;
      private bool n371FuncaoDados_SistemaDes ;
      private bool n372FuncaoDados_SistemaAreaCod ;
      private bool n755FuncaoDados_Contar ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV31Update_IsBlob ;
      private bool AV32Delete_IsBlob ;
      private String AV43TFFuncaoDados_Tipo_SelsJson ;
      private String AV47TFFuncaoDados_Complexidade_SelsJson ;
      private String AV63TFFuncaoDados_Ativo_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17FuncaoDados_Nome1 ;
      private String AV18FuncaoDados_SistemaDes1 ;
      private String AV20DynamicFiltersSelector2 ;
      private String AV22FuncaoDados_Nome2 ;
      private String AV23FuncaoDados_SistemaDes2 ;
      private String AV25DynamicFiltersSelector3 ;
      private String AV27FuncaoDados_Nome3 ;
      private String AV28FuncaoDados_SistemaDes3 ;
      private String AV39TFFuncaoDados_Nome ;
      private String AV40TFFuncaoDados_Nome_Sel ;
      private String AV41ddo_FuncaoDados_NomeTitleControlIdToReplace ;
      private String AV45ddo_FuncaoDados_TipoTitleControlIdToReplace ;
      private String AV49ddo_FuncaoDados_ComplexidadeTitleControlIdToReplace ;
      private String AV53ddo_FuncaoDados_DERTitleControlIdToReplace ;
      private String AV57ddo_FuncaoDados_RLRTitleControlIdToReplace ;
      private String AV61ddo_FuncaoDados_PFTitleControlIdToReplace ;
      private String AV65ddo_FuncaoDados_AtivoTitleControlIdToReplace ;
      private String AV98Update_GXI ;
      private String AV99Delete_GXI ;
      private String A369FuncaoDados_Nome ;
      private String lV75WWFuncaoDadosDS_4_Funcaodados_nome1 ;
      private String lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 ;
      private String lV80WWFuncaoDadosDS_9_Funcaodados_nome2 ;
      private String lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 ;
      private String lV85WWFuncaoDadosDS_14_Funcaodados_nome3 ;
      private String lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 ;
      private String lV87WWFuncaoDadosDS_16_Tffuncaodados_nome ;
      private String AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1 ;
      private String AV75WWFuncaoDadosDS_4_Funcaodados_nome1 ;
      private String AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 ;
      private String AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2 ;
      private String AV80WWFuncaoDadosDS_9_Funcaodados_nome2 ;
      private String AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 ;
      private String AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3 ;
      private String AV85WWFuncaoDadosDS_14_Funcaodados_nome3 ;
      private String AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 ;
      private String AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel ;
      private String AV87WWFuncaoDadosDS_16_Tffuncaodados_nome ;
      private String A371FuncaoDados_SistemaDes ;
      private String AV31Update ;
      private String AV32Delete ;
      private IGxSession AV33Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbFuncaoDados_Tipo ;
      private GXCombobox cmbFuncaoDados_Complexidade ;
      private GXCombobox cmbFuncaoDados_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H009B2_A371FuncaoDados_SistemaDes ;
      private bool[] H009B2_n371FuncaoDados_SistemaDes ;
      private int[] H009B2_A372FuncaoDados_SistemaAreaCod ;
      private bool[] H009B2_n372FuncaoDados_SistemaAreaCod ;
      private int[] H009B2_A370FuncaoDados_SistemaCod ;
      private int[] H009B2_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] H009B2_n391FuncaoDados_FuncaoDadosCod ;
      private String[] H009B2_A394FuncaoDados_Ativo ;
      private String[] H009B2_A373FuncaoDados_Tipo ;
      private String[] H009B2_A369FuncaoDados_Nome ;
      private bool[] H009B2_A755FuncaoDados_Contar ;
      private bool[] H009B2_n755FuncaoDados_Contar ;
      private int[] H009B2_A368FuncaoDados_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV44TFFuncaoDados_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV48TFFuncaoDados_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV64TFFuncaoDados_Ativo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38FuncaoDados_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42FuncaoDados_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46FuncaoDados_ComplexidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50FuncaoDados_DERTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54FuncaoDados_RLRTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58FuncaoDados_PFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62FuncaoDados_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV66DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons3 ;
   }

   public class wwfuncaodados__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H009B2( IGxContext context ,
                                             String A376FuncaoDados_Complexidade ,
                                             IGxCollection AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels ,
                                             String A373FuncaoDados_Tipo ,
                                             IGxCollection AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels ,
                                             String A394FuncaoDados_Ativo ,
                                             IGxCollection AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels ,
                                             int AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod ,
                                             String AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1 ,
                                             short AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 ,
                                             String AV75WWFuncaoDadosDS_4_Funcaodados_nome1 ,
                                             String AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1 ,
                                             bool AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 ,
                                             String AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2 ,
                                             short AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 ,
                                             String AV80WWFuncaoDadosDS_9_Funcaodados_nome2 ,
                                             String AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2 ,
                                             bool AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 ,
                                             String AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3 ,
                                             short AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 ,
                                             String AV85WWFuncaoDadosDS_14_Funcaodados_nome3 ,
                                             String AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3 ,
                                             String AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel ,
                                             String AV87WWFuncaoDadosDS_16_Tffuncaodados_nome ,
                                             int AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels_Count ,
                                             int AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels_Count ,
                                             int A372FuncaoDados_SistemaAreaCod ,
                                             String A369FuncaoDados_Nome ,
                                             String A371FuncaoDados_SistemaDes ,
                                             short AV35OrderedBy ,
                                             bool AV13OrderedDsc ,
                                             int AV90WWFuncaoDadosDS_19_Tffuncaodados_complexidade_sels_Count ,
                                             short AV91WWFuncaoDadosDS_20_Tffuncaodados_der ,
                                             short A374FuncaoDados_DER ,
                                             short AV92WWFuncaoDadosDS_21_Tffuncaodados_der_to ,
                                             short AV93WWFuncaoDadosDS_22_Tffuncaodados_rlr ,
                                             short A375FuncaoDados_RLR ,
                                             short AV94WWFuncaoDadosDS_23_Tffuncaodados_rlr_to ,
                                             decimal AV95WWFuncaoDadosDS_24_Tffuncaodados_pf ,
                                             decimal A377FuncaoDados_PF ,
                                             decimal AV96WWFuncaoDadosDS_25_Tffuncaodados_pf_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [16] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T2.[Sistema_Nome] AS FuncaoDados_SistemaDes, T2.[Sistema_AreaTrabalhoCod] AS FuncaoDados_SistemaAreaCod, T1.[FuncaoDados_SistemaCod] AS FuncaoDados_SistemaCod, T1.[FuncaoDados_FuncaoDadosCod], T1.[FuncaoDados_Ativo], T1.[FuncaoDados_Tipo], T1.[FuncaoDados_Nome], T1.[FuncaoDados_Contar], T1.[FuncaoDados_Codigo] FROM ([FuncaoDados] T1 WITH (NOLOCK) INNER JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[FuncaoDados_SistemaCod])";
         if ( ! (0==AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_AreaTrabalhoCod] = @AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_AreaTrabalhoCod] = @AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1, "FUNCAODADOS_NOME") == 0 ) && ( AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncaoDadosDS_4_Funcaodados_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV75WWFuncaoDadosDS_4_Funcaodados_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like @lV75WWFuncaoDadosDS_4_Funcaodados_nome1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1, "FUNCAODADOS_NOME") == 0 ) && ( AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV75WWFuncaoDadosDS_4_Funcaodados_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV75WWFuncaoDadosDS_4_Funcaodados_nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like '%' + @lV75WWFuncaoDadosDS_4_Funcaodados_nome1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV73WWFuncaoDadosDS_2_Dynamicfiltersselector1, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV74WWFuncaoDadosDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV76WWFuncaoDadosDS_5_Funcaodados_sistemades1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2, "FUNCAODADOS_NOME") == 0 ) && ( AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncaoDadosDS_9_Funcaodados_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV80WWFuncaoDadosDS_9_Funcaodados_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like @lV80WWFuncaoDadosDS_9_Funcaodados_nome2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2, "FUNCAODADOS_NOME") == 0 ) && ( AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWFuncaoDadosDS_9_Funcaodados_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV80WWFuncaoDadosDS_9_Funcaodados_nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like '%' + @lV80WWFuncaoDadosDS_9_Funcaodados_nome2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV77WWFuncaoDadosDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV78WWFuncaoDadosDS_7_Dynamicfiltersselector2, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV79WWFuncaoDadosDS_8_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV81WWFuncaoDadosDS_10_Funcaodados_sistemades2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3, "FUNCAODADOS_NOME") == 0 ) && ( AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWFuncaoDadosDS_14_Funcaodados_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV85WWFuncaoDadosDS_14_Funcaodados_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like @lV85WWFuncaoDadosDS_14_Funcaodados_nome3)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3, "FUNCAODADOS_NOME") == 0 ) && ( AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWFuncaoDadosDS_14_Funcaodados_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like '%' + @lV85WWFuncaoDadosDS_14_Funcaodados_nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like '%' + @lV85WWFuncaoDadosDS_14_Funcaodados_nome3)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like @lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like @lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV82WWFuncaoDadosDS_11_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV83WWFuncaoDadosDS_12_Dynamicfiltersselector3, "FUNCAODADOS_SISTEMADES") == 0 ) && ( AV84WWFuncaoDadosDS_13_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWFuncaoDadosDS_15_Funcaodados_sistemades3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Sistema_Nome] like '%' + @lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Sistema_Nome] like '%' + @lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWFuncaoDadosDS_16_Tffuncaodados_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] like @lV87WWFuncaoDadosDS_16_Tffuncaodados_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] like @lV87WWFuncaoDadosDS_16_Tffuncaodados_nome)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoDados_Nome] = @AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoDados_Nome] = @AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels, "T1.[FuncaoDados_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV89WWFuncaoDadosDS_18_Tffuncaodados_tipo_sels, "T1.[FuncaoDados_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels, "T1.[FuncaoDados_Ativo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV97WWFuncaoDadosDS_26_Tffuncaodados_ativo_sels, "T1.[FuncaoDados_Ativo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV35OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_Nome]";
         }
         else if ( ( AV35OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_Nome] DESC";
         }
         else if ( ( AV35OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_Tipo]";
         }
         else if ( ( AV35OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_Tipo] DESC";
         }
         else if ( ( AV35OrderedBy == 3 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_Ativo]";
         }
         else if ( ( AV35OrderedBy == 3 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoDados_Ativo] DESC";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H009B2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (int)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (short)dynConstraints[28] , (bool)dynConstraints[29] , (int)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (short)dynConstraints[33] , (short)dynConstraints[34] , (short)dynConstraints[35] , (short)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009B2 ;
          prmH009B2 = new Object[] {
          new Object[] {"@AV90WWFuCount",SqlDbType.Int,9,0} ,
          new Object[] {"@AV72WWFuncaoDadosDS_1_Funcaodados_sistemaareacod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV75WWFuncaoDadosDS_4_Funcaodados_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV75WWFuncaoDadosDS_4_Funcaodados_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV76WWFuncaoDadosDS_5_Funcaodados_sistemades1",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV80WWFuncaoDadosDS_9_Funcaodados_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV80WWFuncaoDadosDS_9_Funcaodados_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV81WWFuncaoDadosDS_10_Funcaodados_sistemades2",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV85WWFuncaoDadosDS_14_Funcaodados_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV85WWFuncaoDadosDS_14_Funcaodados_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV86WWFuncaoDadosDS_15_Funcaodados_sistemades3",SqlDbType.VarChar,100,0} ,
          new Object[] {"@lV87WWFuncaoDadosDS_16_Tffuncaodados_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV88WWFuncaoDadosDS_17_Tffuncaodados_nome_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009B2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009B2,11,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((String[]) buf[8])[0] = rslt.getString(6, 3) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.getBool(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
       }
    }

 }

}
