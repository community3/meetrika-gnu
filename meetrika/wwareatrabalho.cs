/*
               File: WWAreaTrabalho
        Description:  �rea de Trabalho
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:49:31.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwareatrabalho : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwareatrabalho( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwareatrabalho( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavAreatrabalho_ativo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavAreatrabalho_ativo2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         cmbavAreatrabalho_ativo3 = new GXCombobox();
         cmbAreaTrabalho_ValidaOSFM = new GXCombobox();
         cmbAreaTrabalho_CalculoPFinal = new GXCombobox();
         cmbAreaTrabalho_VerTA = new GXCombobox();
         chkAreaTrabalho_Ativo = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_94 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_94_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_94_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17AreaTrabalho_Descricao1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
               AV90Organizacao_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90Organizacao_Nome1", AV90Organizacao_Nome1);
               AV129AreaTrabalho_Ativo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129AreaTrabalho_Ativo1", AV129AreaTrabalho_Ativo1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21AreaTrabalho_Descricao2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AreaTrabalho_Descricao2", AV21AreaTrabalho_Descricao2);
               AV92Organizacao_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92Organizacao_Nome2", AV92Organizacao_Nome2);
               AV130AreaTrabalho_Ativo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130AreaTrabalho_Ativo2", AV130AreaTrabalho_Ativo2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25AreaTrabalho_Descricao3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AreaTrabalho_Descricao3", AV25AreaTrabalho_Descricao3);
               AV131Organizacao_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131Organizacao_Nome3", AV131Organizacao_Nome3);
               AV132AreaTrabalho_Ativo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132AreaTrabalho_Ativo3", AV132AreaTrabalho_Ativo3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV61TFAreaTrabalho_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFAreaTrabalho_Descricao", AV61TFAreaTrabalho_Descricao);
               AV62TFAreaTrabalho_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFAreaTrabalho_Descricao_Sel", AV62TFAreaTrabalho_Descricao_Sel);
               AV65TFContratante_RazaoSocial = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratante_RazaoSocial", AV65TFContratante_RazaoSocial);
               AV66TFContratante_RazaoSocial_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratante_RazaoSocial_Sel", AV66TFContratante_RazaoSocial_Sel);
               AV69TFContratante_CNPJ = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratante_CNPJ", AV69TFContratante_CNPJ);
               AV70TFContratante_CNPJ_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratante_CNPJ_Sel", AV70TFContratante_CNPJ_Sel);
               AV73TFContratante_Telefone = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratante_Telefone", AV73TFContratante_Telefone);
               AV74TFContratante_Telefone_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratante_Telefone_Sel", AV74TFContratante_Telefone_Sel);
               AV77TFAreaTrabalho_ValidaOSFM_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAreaTrabalho_ValidaOSFM_Sel", StringUtil.Str( (decimal)(AV77TFAreaTrabalho_ValidaOSFM_Sel), 1, 0));
               AV80TFAreaTrabalho_DiasParaPagar = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFAreaTrabalho_DiasParaPagar), 4, 0)));
               AV81TFAreaTrabalho_DiasParaPagar_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFAreaTrabalho_DiasParaPagar_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFAreaTrabalho_DiasParaPagar_To), 4, 0)));
               AV134TFEstado_UF = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134TFEstado_UF", AV134TFEstado_UF);
               AV135TFEstado_UF_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135TFEstado_UF_Sel", AV135TFEstado_UF_Sel);
               AV118TFMunicipio_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118TFMunicipio_Nome", AV118TFMunicipio_Nome);
               AV119TFMunicipio_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119TFMunicipio_Nome_Sel", AV119TFMunicipio_Nome_Sel);
               AV84TFAreaTrabalho_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAreaTrabalho_Ativo_Sel", StringUtil.Str( (decimal)(AV84TFAreaTrabalho_Ativo_Sel), 1, 0));
               AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace", AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace);
               AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace", AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace);
               AV71ddo_Contratante_CNPJTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_Contratante_CNPJTitleControlIdToReplace", AV71ddo_Contratante_CNPJTitleControlIdToReplace);
               AV75ddo_Contratante_TelefoneTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Contratante_TelefoneTitleControlIdToReplace", AV75ddo_Contratante_TelefoneTitleControlIdToReplace);
               AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace", AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace);
               AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace", AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace);
               AV136ddo_Estado_UFTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136ddo_Estado_UFTitleControlIdToReplace", AV136ddo_Estado_UFTitleControlIdToReplace);
               AV120ddo_Municipio_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120ddo_Municipio_NomeTitleControlIdToReplace", AV120ddo_Municipio_NomeTitleControlIdToReplace);
               AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace", AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace);
               AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace", AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace);
               AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace", AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV123TFAreaTrabalho_CalculoPFinal_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV144TFAreaTrabalho_VerTA_Sels);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV188Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A29Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n29Contratante_Codigo = false;
               A25Municipio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n25Municipio_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25Municipio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A25Municipio_Codigo), 6, 0)));
               A72AreaTrabalho_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV90Organizacao_Nome1, AV129AreaTrabalho_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21AreaTrabalho_Descricao2, AV92Organizacao_Nome2, AV130AreaTrabalho_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25AreaTrabalho_Descricao3, AV131Organizacao_Nome3, AV132AreaTrabalho_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV61TFAreaTrabalho_Descricao, AV62TFAreaTrabalho_Descricao_Sel, AV65TFContratante_RazaoSocial, AV66TFContratante_RazaoSocial_Sel, AV69TFContratante_CNPJ, AV70TFContratante_CNPJ_Sel, AV73TFContratante_Telefone, AV74TFContratante_Telefone_Sel, AV77TFAreaTrabalho_ValidaOSFM_Sel, AV80TFAreaTrabalho_DiasParaPagar, AV81TFAreaTrabalho_DiasParaPagar_To, AV134TFEstado_UF, AV135TFEstado_UF_Sel, AV118TFMunicipio_Nome, AV119TFMunicipio_Nome_Sel, AV84TFAreaTrabalho_Ativo_Sel, AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV71ddo_Contratante_CNPJTitleControlIdToReplace, AV75ddo_Contratante_TelefoneTitleControlIdToReplace, AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace, AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace, AV136ddo_Estado_UFTitleControlIdToReplace, AV120ddo_Municipio_NomeTitleControlIdToReplace, AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace, AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace, AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace, AV123TFAreaTrabalho_CalculoPFinal_Sels, AV144TFAreaTrabalho_VerTA_Sels, AV6WWPContext, AV188Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A5AreaTrabalho_Codigo, A29Contratante_Codigo, A25Municipio_Codigo, A72AreaTrabalho_Ativo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA0T2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START0T2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203120493211");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwareatrabalho.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vAREATRABALHO_DESCRICAO1", AV17AreaTrabalho_Descricao1);
         GxWebStd.gx_hidden_field( context, "GXH_vORGANIZACAO_NOME1", StringUtil.RTrim( AV90Organizacao_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vAREATRABALHO_ATIVO1", StringUtil.RTrim( AV129AreaTrabalho_Ativo1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vAREATRABALHO_DESCRICAO2", AV21AreaTrabalho_Descricao2);
         GxWebStd.gx_hidden_field( context, "GXH_vORGANIZACAO_NOME2", StringUtil.RTrim( AV92Organizacao_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vAREATRABALHO_ATIVO2", StringUtil.RTrim( AV130AreaTrabalho_Ativo2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vAREATRABALHO_DESCRICAO3", AV25AreaTrabalho_Descricao3);
         GxWebStd.gx_hidden_field( context, "GXH_vORGANIZACAO_NOME3", StringUtil.RTrim( AV131Organizacao_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vAREATRABALHO_ATIVO3", StringUtil.RTrim( AV132AreaTrabalho_Ativo3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAREATRABALHO_DESCRICAO", AV61TFAreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFAREATRABALHO_DESCRICAO_SEL", AV62TFAreaTrabalho_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_RAZAOSOCIAL", StringUtil.RTrim( AV65TFContratante_RazaoSocial));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_RAZAOSOCIAL_SEL", StringUtil.RTrim( AV66TFContratante_RazaoSocial_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_CNPJ", AV69TFContratante_CNPJ);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_CNPJ_SEL", AV70TFContratante_CNPJ_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_TELEFONE", StringUtil.RTrim( AV73TFContratante_Telefone));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATANTE_TELEFONE_SEL", StringUtil.RTrim( AV74TFContratante_Telefone_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAREATRABALHO_VALIDAOSFM_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFAreaTrabalho_ValidaOSFM_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAREATRABALHO_DIASPARAPAGAR", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFAreaTrabalho_DiasParaPagar), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAREATRABALHO_DIASPARAPAGAR_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81TFAreaTrabalho_DiasParaPagar_To), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFESTADO_UF", StringUtil.RTrim( AV134TFEstado_UF));
         GxWebStd.gx_hidden_field( context, "GXH_vTFESTADO_UF_SEL", StringUtil.RTrim( AV135TFEstado_UF_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMUNICIPIO_NOME", StringUtil.RTrim( AV118TFMunicipio_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFMUNICIPIO_NOME_SEL", StringUtil.RTrim( AV119TFMunicipio_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFAREATRABALHO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84TFAreaTrabalho_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_94", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_94), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV88GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV89GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV86DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV86DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAREATRABALHO_DESCRICAOTITLEFILTERDATA", AV60AreaTrabalho_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAREATRABALHO_DESCRICAOTITLEFILTERDATA", AV60AreaTrabalho_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTE_RAZAOSOCIALTITLEFILTERDATA", AV64Contratante_RazaoSocialTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTE_RAZAOSOCIALTITLEFILTERDATA", AV64Contratante_RazaoSocialTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTE_CNPJTITLEFILTERDATA", AV68Contratante_CNPJTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTE_CNPJTITLEFILTERDATA", AV68Contratante_CNPJTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTE_TELEFONETITLEFILTERDATA", AV72Contratante_TelefoneTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTE_TELEFONETITLEFILTERDATA", AV72Contratante_TelefoneTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAREATRABALHO_VALIDAOSFMTITLEFILTERDATA", AV76AreaTrabalho_ValidaOSFMTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAREATRABALHO_VALIDAOSFMTITLEFILTERDATA", AV76AreaTrabalho_ValidaOSFMTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAREATRABALHO_DIASPARAPAGARTITLEFILTERDATA", AV79AreaTrabalho_DiasParaPagarTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAREATRABALHO_DIASPARAPAGARTITLEFILTERDATA", AV79AreaTrabalho_DiasParaPagarTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vESTADO_UFTITLEFILTERDATA", AV133Estado_UFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vESTADO_UFTITLEFILTERDATA", AV133Estado_UFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vMUNICIPIO_NOMETITLEFILTERDATA", AV117Municipio_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMUNICIPIO_NOMETITLEFILTERDATA", AV117Municipio_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAREATRABALHO_CALCULOPFINALTITLEFILTERDATA", AV121AreaTrabalho_CalculoPFinalTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAREATRABALHO_CALCULOPFINALTITLEFILTERDATA", AV121AreaTrabalho_CalculoPFinalTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAREATRABALHO_VERTATITLEFILTERDATA", AV142AreaTrabalho_VerTATitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAREATRABALHO_VERTATITLEFILTERDATA", AV142AreaTrabalho_VerTATitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vAREATRABALHO_ATIVOTITLEFILTERDATA", AV83AreaTrabalho_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vAREATRABALHO_ATIVOTITLEFILTERDATA", AV83AreaTrabalho_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFAREATRABALHO_CALCULOPFINAL_SELS", AV123TFAreaTrabalho_CalculoPFinal_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFAREATRABALHO_CALCULOPFINAL_SELS", AV123TFAreaTrabalho_CalculoPFinal_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFAREATRABALHO_VERTA_SELS", AV144TFAreaTrabalho_VerTA_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFAREATRABALHO_VERTA_SELS", AV144TFAreaTrabalho_VerTA_Sels);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV188Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "MUNICIPIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25Municipio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTERAZ", StringUtil.RTrim( A64ContratanteUsuario_ContratanteRaz));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTEFAN", StringUtil.RTrim( A65ContratanteUsuario_ContratanteFan));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1228ContratadaUsuario_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A69ContratadaUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A66ContratadaUsuario_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADAPESSOACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A67ContratadaUsuario_ContratadaPessoaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATADAUSUARIO_CONTRATADAPESSOANOM", StringUtil.RTrim( A68ContratadaUsuario_ContratadaPessoaNom));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATADACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1136ContratoGestor_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1079ContratoGestor_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOGESTOR_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1078ContratoGestor_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_areatrabalho_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_areatrabalho_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_areatrabalho_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_areatrabalho_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_areatrabalho_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_areatrabalho_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_areatrabalho_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_areatrabalho_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_areatrabalho_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_areatrabalho_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_areatrabalho_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_areatrabalho_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_areatrabalho_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_areatrabalho_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_areatrabalho_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_areatrabalho_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_areatrabalho_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_areatrabalho_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_areatrabalho_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_areatrabalho_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_areatrabalho_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_areatrabalho_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_areatrabalho_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Caption", StringUtil.RTrim( Ddo_contratante_razaosocial_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Tooltip", StringUtil.RTrim( Ddo_contratante_razaosocial_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Cls", StringUtil.RTrim( Ddo_contratante_razaosocial_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Filteredtext_set", StringUtil.RTrim( Ddo_contratante_razaosocial_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Selectedvalue_set", StringUtil.RTrim( Ddo_contratante_razaosocial_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratante_razaosocial_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratante_razaosocial_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Includesortasc", StringUtil.BoolToStr( Ddo_contratante_razaosocial_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Includesortdsc", StringUtil.BoolToStr( Ddo_contratante_razaosocial_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Sortedstatus", StringUtil.RTrim( Ddo_contratante_razaosocial_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Includefilter", StringUtil.BoolToStr( Ddo_contratante_razaosocial_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Filtertype", StringUtil.RTrim( Ddo_contratante_razaosocial_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Filterisrange", StringUtil.BoolToStr( Ddo_contratante_razaosocial_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Includedatalist", StringUtil.BoolToStr( Ddo_contratante_razaosocial_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Datalisttype", StringUtil.RTrim( Ddo_contratante_razaosocial_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Datalistproc", StringUtil.RTrim( Ddo_contratante_razaosocial_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratante_razaosocial_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Sortasc", StringUtil.RTrim( Ddo_contratante_razaosocial_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Sortdsc", StringUtil.RTrim( Ddo_contratante_razaosocial_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Loadingdata", StringUtil.RTrim( Ddo_contratante_razaosocial_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Cleanfilter", StringUtil.RTrim( Ddo_contratante_razaosocial_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Noresultsfound", StringUtil.RTrim( Ddo_contratante_razaosocial_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Searchbuttontext", StringUtil.RTrim( Ddo_contratante_razaosocial_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Caption", StringUtil.RTrim( Ddo_contratante_cnpj_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Tooltip", StringUtil.RTrim( Ddo_contratante_cnpj_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Cls", StringUtil.RTrim( Ddo_contratante_cnpj_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Filteredtext_set", StringUtil.RTrim( Ddo_contratante_cnpj_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Selectedvalue_set", StringUtil.RTrim( Ddo_contratante_cnpj_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratante_cnpj_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratante_cnpj_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Includesortasc", StringUtil.BoolToStr( Ddo_contratante_cnpj_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Includesortdsc", StringUtil.BoolToStr( Ddo_contratante_cnpj_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Sortedstatus", StringUtil.RTrim( Ddo_contratante_cnpj_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Includefilter", StringUtil.BoolToStr( Ddo_contratante_cnpj_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Filtertype", StringUtil.RTrim( Ddo_contratante_cnpj_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Filterisrange", StringUtil.BoolToStr( Ddo_contratante_cnpj_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Includedatalist", StringUtil.BoolToStr( Ddo_contratante_cnpj_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Datalisttype", StringUtil.RTrim( Ddo_contratante_cnpj_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Datalistproc", StringUtil.RTrim( Ddo_contratante_cnpj_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratante_cnpj_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Sortasc", StringUtil.RTrim( Ddo_contratante_cnpj_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Sortdsc", StringUtil.RTrim( Ddo_contratante_cnpj_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Loadingdata", StringUtil.RTrim( Ddo_contratante_cnpj_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Cleanfilter", StringUtil.RTrim( Ddo_contratante_cnpj_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Noresultsfound", StringUtil.RTrim( Ddo_contratante_cnpj_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Searchbuttontext", StringUtil.RTrim( Ddo_contratante_cnpj_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Caption", StringUtil.RTrim( Ddo_contratante_telefone_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Tooltip", StringUtil.RTrim( Ddo_contratante_telefone_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Cls", StringUtil.RTrim( Ddo_contratante_telefone_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Filteredtext_set", StringUtil.RTrim( Ddo_contratante_telefone_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Selectedvalue_set", StringUtil.RTrim( Ddo_contratante_telefone_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratante_telefone_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratante_telefone_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Includesortasc", StringUtil.BoolToStr( Ddo_contratante_telefone_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Includesortdsc", StringUtil.BoolToStr( Ddo_contratante_telefone_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Sortedstatus", StringUtil.RTrim( Ddo_contratante_telefone_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Includefilter", StringUtil.BoolToStr( Ddo_contratante_telefone_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Filtertype", StringUtil.RTrim( Ddo_contratante_telefone_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Filterisrange", StringUtil.BoolToStr( Ddo_contratante_telefone_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Includedatalist", StringUtil.BoolToStr( Ddo_contratante_telefone_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Datalisttype", StringUtil.RTrim( Ddo_contratante_telefone_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Datalistproc", StringUtil.RTrim( Ddo_contratante_telefone_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratante_telefone_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Sortasc", StringUtil.RTrim( Ddo_contratante_telefone_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Sortdsc", StringUtil.RTrim( Ddo_contratante_telefone_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Loadingdata", StringUtil.RTrim( Ddo_contratante_telefone_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Cleanfilter", StringUtil.RTrim( Ddo_contratante_telefone_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Noresultsfound", StringUtil.RTrim( Ddo_contratante_telefone_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Searchbuttontext", StringUtil.RTrim( Ddo_contratante_telefone_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Caption", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Tooltip", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Cls", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Selectedvalue_set", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Dropdownoptionstype", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Includesortasc", StringUtil.BoolToStr( Ddo_areatrabalho_validaosfm_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Includesortdsc", StringUtil.BoolToStr( Ddo_areatrabalho_validaosfm_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Sortedstatus", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Includefilter", StringUtil.BoolToStr( Ddo_areatrabalho_validaosfm_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Includedatalist", StringUtil.BoolToStr( Ddo_areatrabalho_validaosfm_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Datalisttype", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Datalistfixedvalues", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Sortasc", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Sortdsc", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Cleanfilter", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Searchbuttontext", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Caption", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Tooltip", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Cls", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Filteredtext_set", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Filteredtextto_set", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Dropdownoptionstype", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Includesortasc", StringUtil.BoolToStr( Ddo_areatrabalho_diasparapagar_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Includesortdsc", StringUtil.BoolToStr( Ddo_areatrabalho_diasparapagar_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Sortedstatus", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Includefilter", StringUtil.BoolToStr( Ddo_areatrabalho_diasparapagar_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Filtertype", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Filterisrange", StringUtil.BoolToStr( Ddo_areatrabalho_diasparapagar_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Includedatalist", StringUtil.BoolToStr( Ddo_areatrabalho_diasparapagar_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Sortasc", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Sortdsc", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Cleanfilter", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Rangefilterfrom", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Rangefilterto", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Searchbuttontext", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Caption", StringUtil.RTrim( Ddo_estado_uf_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Tooltip", StringUtil.RTrim( Ddo_estado_uf_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Cls", StringUtil.RTrim( Ddo_estado_uf_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filteredtext_set", StringUtil.RTrim( Ddo_estado_uf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Selectedvalue_set", StringUtil.RTrim( Ddo_estado_uf_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Dropdownoptionstype", StringUtil.RTrim( Ddo_estado_uf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_estado_uf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includesortasc", StringUtil.BoolToStr( Ddo_estado_uf_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includesortdsc", StringUtil.BoolToStr( Ddo_estado_uf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Sortedstatus", StringUtil.RTrim( Ddo_estado_uf_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includefilter", StringUtil.BoolToStr( Ddo_estado_uf_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filtertype", StringUtil.RTrim( Ddo_estado_uf_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filterisrange", StringUtil.BoolToStr( Ddo_estado_uf_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Includedatalist", StringUtil.BoolToStr( Ddo_estado_uf_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Datalisttype", StringUtil.RTrim( Ddo_estado_uf_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Datalistproc", StringUtil.RTrim( Ddo_estado_uf_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_estado_uf_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Sortasc", StringUtil.RTrim( Ddo_estado_uf_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Sortdsc", StringUtil.RTrim( Ddo_estado_uf_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Loadingdata", StringUtil.RTrim( Ddo_estado_uf_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Cleanfilter", StringUtil.RTrim( Ddo_estado_uf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Noresultsfound", StringUtil.RTrim( Ddo_estado_uf_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Searchbuttontext", StringUtil.RTrim( Ddo_estado_uf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Caption", StringUtil.RTrim( Ddo_municipio_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Tooltip", StringUtil.RTrim( Ddo_municipio_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Cls", StringUtil.RTrim( Ddo_municipio_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_municipio_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_municipio_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_municipio_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_municipio_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_municipio_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_municipio_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_municipio_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_municipio_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filtertype", StringUtil.RTrim( Ddo_municipio_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_municipio_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_municipio_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalisttype", StringUtil.RTrim( Ddo_municipio_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalistproc", StringUtil.RTrim( Ddo_municipio_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_municipio_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortasc", StringUtil.RTrim( Ddo_municipio_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Sortdsc", StringUtil.RTrim( Ddo_municipio_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Loadingdata", StringUtil.RTrim( Ddo_municipio_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_municipio_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_municipio_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_municipio_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Caption", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Tooltip", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Cls", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Selectedvalue_set", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Dropdownoptionstype", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Includesortasc", StringUtil.BoolToStr( Ddo_areatrabalho_calculopfinal_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Includesortdsc", StringUtil.BoolToStr( Ddo_areatrabalho_calculopfinal_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Sortedstatus", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Includefilter", StringUtil.BoolToStr( Ddo_areatrabalho_calculopfinal_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Includedatalist", StringUtil.BoolToStr( Ddo_areatrabalho_calculopfinal_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Datalisttype", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Allowmultipleselection", StringUtil.BoolToStr( Ddo_areatrabalho_calculopfinal_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Datalistfixedvalues", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Sortasc", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Sortdsc", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Cleanfilter", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Searchbuttontext", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Caption", StringUtil.RTrim( Ddo_areatrabalho_verta_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Tooltip", StringUtil.RTrim( Ddo_areatrabalho_verta_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Cls", StringUtil.RTrim( Ddo_areatrabalho_verta_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Selectedvalue_set", StringUtil.RTrim( Ddo_areatrabalho_verta_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Dropdownoptionstype", StringUtil.RTrim( Ddo_areatrabalho_verta_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_areatrabalho_verta_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Includesortasc", StringUtil.BoolToStr( Ddo_areatrabalho_verta_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Includesortdsc", StringUtil.BoolToStr( Ddo_areatrabalho_verta_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Sortedstatus", StringUtil.RTrim( Ddo_areatrabalho_verta_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Includefilter", StringUtil.BoolToStr( Ddo_areatrabalho_verta_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Includedatalist", StringUtil.BoolToStr( Ddo_areatrabalho_verta_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Datalisttype", StringUtil.RTrim( Ddo_areatrabalho_verta_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Allowmultipleselection", StringUtil.BoolToStr( Ddo_areatrabalho_verta_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Datalistfixedvalues", StringUtil.RTrim( Ddo_areatrabalho_verta_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Sortasc", StringUtil.RTrim( Ddo_areatrabalho_verta_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Sortdsc", StringUtil.RTrim( Ddo_areatrabalho_verta_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Cleanfilter", StringUtil.RTrim( Ddo_areatrabalho_verta_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Searchbuttontext", StringUtil.RTrim( Ddo_areatrabalho_verta_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Caption", StringUtil.RTrim( Ddo_areatrabalho_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_areatrabalho_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Cls", StringUtil.RTrim( Ddo_areatrabalho_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_areatrabalho_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_areatrabalho_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_areatrabalho_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_areatrabalho_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_areatrabalho_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_areatrabalho_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_areatrabalho_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_areatrabalho_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_areatrabalho_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_areatrabalho_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_areatrabalho_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_areatrabalho_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_areatrabalho_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_areatrabalho_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_areatrabalho_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_areatrabalho_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_areatrabalho_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Activeeventkey", StringUtil.RTrim( Ddo_contratante_razaosocial_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Filteredtext_get", StringUtil.RTrim( Ddo_contratante_razaosocial_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_RAZAOSOCIAL_Selectedvalue_get", StringUtil.RTrim( Ddo_contratante_razaosocial_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Activeeventkey", StringUtil.RTrim( Ddo_contratante_cnpj_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Filteredtext_get", StringUtil.RTrim( Ddo_contratante_cnpj_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_CNPJ_Selectedvalue_get", StringUtil.RTrim( Ddo_contratante_cnpj_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Activeeventkey", StringUtil.RTrim( Ddo_contratante_telefone_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Filteredtext_get", StringUtil.RTrim( Ddo_contratante_telefone_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATANTE_TELEFONE_Selectedvalue_get", StringUtil.RTrim( Ddo_contratante_telefone_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Activeeventkey", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VALIDAOSFM_Selectedvalue_get", StringUtil.RTrim( Ddo_areatrabalho_validaosfm_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Activeeventkey", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Filteredtext_get", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_DIASPARAPAGAR_Filteredtextto_get", StringUtil.RTrim( Ddo_areatrabalho_diasparapagar_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Activeeventkey", StringUtil.RTrim( Ddo_estado_uf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Filteredtext_get", StringUtil.RTrim( Ddo_estado_uf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_ESTADO_UF_Selectedvalue_get", StringUtil.RTrim( Ddo_estado_uf_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_municipio_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_municipio_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_MUNICIPIO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_municipio_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Activeeventkey", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_CALCULOPFINAL_Selectedvalue_get", StringUtil.RTrim( Ddo_areatrabalho_calculopfinal_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Activeeventkey", StringUtil.RTrim( Ddo_areatrabalho_verta_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_VERTA_Selectedvalue_get", StringUtil.RTrim( Ddo_areatrabalho_verta_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_areatrabalho_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_AREATRABALHO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_areatrabalho_ativo_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE0T2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT0T2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwareatrabalho.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWAreaTrabalho" ;
      }

      public override String GetPgmdesc( )
      {
         return " �rea de Trabalho" ;
      }

      protected void WB0T0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_0T2( true) ;
         }
         else
         {
            wb_table1_2_0T2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_0T2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(119, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(120, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfareatrabalho_descricao_Internalname, AV61TFAreaTrabalho_Descricao, StringUtil.RTrim( context.localUtil.Format( AV61TFAreaTrabalho_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfareatrabalho_descricao_Jsonclick, 0, "Attribute", "", "", "", edtavTfareatrabalho_descricao_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfareatrabalho_descricao_sel_Internalname, AV62TFAreaTrabalho_Descricao_Sel, StringUtil.RTrim( context.localUtil.Format( AV62TFAreaTrabalho_Descricao_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfareatrabalho_descricao_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfareatrabalho_descricao_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_razaosocial_Internalname, StringUtil.RTrim( AV65TFContratante_RazaoSocial), StringUtil.RTrim( context.localUtil.Format( AV65TFContratante_RazaoSocial, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_razaosocial_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratante_razaosocial_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_razaosocial_sel_Internalname, StringUtil.RTrim( AV66TFContratante_RazaoSocial_Sel), StringUtil.RTrim( context.localUtil.Format( AV66TFContratante_RazaoSocial_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_razaosocial_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratante_razaosocial_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_cnpj_Internalname, AV69TFContratante_CNPJ, StringUtil.RTrim( context.localUtil.Format( AV69TFContratante_CNPJ, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_cnpj_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_cnpj_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_cnpj_sel_Internalname, AV70TFContratante_CNPJ_Sel, StringUtil.RTrim( context.localUtil.Format( AV70TFContratante_CNPJ_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_cnpj_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_cnpj_sel_Visible, 1, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_telefone_Internalname, StringUtil.RTrim( AV73TFContratante_Telefone), StringUtil.RTrim( context.localUtil.Format( AV73TFContratante_Telefone, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_telefone_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_telefone_Visible, 1, 0, "text", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratante_telefone_sel_Internalname, StringUtil.RTrim( AV74TFContratante_Telefone_Sel), StringUtil.RTrim( context.localUtil.Format( AV74TFContratante_Telefone_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,128);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratante_telefone_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratante_telefone_sel_Visible, 1, 0, "text", "", 100, "px", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfareatrabalho_validaosfm_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFAreaTrabalho_ValidaOSFM_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV77TFAreaTrabalho_ValidaOSFM_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfareatrabalho_validaosfm_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfareatrabalho_validaosfm_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfareatrabalho_diasparapagar_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFAreaTrabalho_DiasParaPagar), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV80TFAreaTrabalho_DiasParaPagar), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,130);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfareatrabalho_diasparapagar_Jsonclick, 0, "Attribute", "", "", "", edtavTfareatrabalho_diasparapagar_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfareatrabalho_diasparapagar_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81TFAreaTrabalho_DiasParaPagar_To), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV81TFAreaTrabalho_DiasParaPagar_To), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfareatrabalho_diasparapagar_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfareatrabalho_diasparapagar_to_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_uf_Internalname, StringUtil.RTrim( AV134TFEstado_UF), StringUtil.RTrim( context.localUtil.Format( AV134TFEstado_UF, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_uf_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_uf_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_uf_sel_Internalname, StringUtil.RTrim( AV135TFEstado_UF_Sel), StringUtil.RTrim( context.localUtil.Format( AV135TFEstado_UF_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_uf_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_uf_sel_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmunicipio_nome_Internalname, StringUtil.RTrim( AV118TFMunicipio_Nome), StringUtil.RTrim( context.localUtil.Format( AV118TFMunicipio_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,134);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmunicipio_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfmunicipio_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfmunicipio_nome_sel_Internalname, StringUtil.RTrim( AV119TFMunicipio_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV119TFMunicipio_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,135);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfmunicipio_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfmunicipio_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfareatrabalho_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84TFAreaTrabalho_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV84TFAreaTrabalho_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfareatrabalho_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfareatrabalho_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWAreaTrabalho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AREATRABALHO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Internalname, AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"", 0, edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAreaTrabalho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTE_RAZAOSOCIALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Internalname, AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,140);\"", 0, edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAreaTrabalho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTE_CNPJContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 142,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Internalname, AV71ddo_Contratante_CNPJTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,142);\"", 0, edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAreaTrabalho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATANTE_TELEFONEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratante_telefonetitlecontrolidtoreplace_Internalname, AV75ddo_Contratante_TelefoneTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,144);\"", 0, edtavDdo_contratante_telefonetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAreaTrabalho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AREATRABALHO_VALIDAOSFMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_areatrabalho_validaosfmtitlecontrolidtoreplace_Internalname, AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"", 0, edtavDdo_areatrabalho_validaosfmtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAreaTrabalho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AREATRABALHO_DIASPARAPAGARContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_areatrabalho_diasparapagartitlecontrolidtoreplace_Internalname, AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,148);\"", 0, edtavDdo_areatrabalho_diasparapagartitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAreaTrabalho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_ESTADO_UFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 150,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_estado_uftitlecontrolidtoreplace_Internalname, AV136ddo_Estado_UFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,150);\"", 0, edtavDdo_estado_uftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAreaTrabalho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_MUNICIPIO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 152,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname, AV120ddo_Municipio_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,152);\"", 0, edtavDdo_municipio_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAreaTrabalho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AREATRABALHO_CALCULOPFINALContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_areatrabalho_calculopfinaltitlecontrolidtoreplace_Internalname, AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,154);\"", 0, edtavDdo_areatrabalho_calculopfinaltitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAreaTrabalho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AREATRABALHO_VERTAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 156,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_areatrabalho_vertatitlecontrolidtoreplace_Internalname, AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,156);\"", 0, edtavDdo_areatrabalho_vertatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAreaTrabalho.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_AREATRABALHO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'" + sGXsfl_94_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_areatrabalho_ativotitlecontrolidtoreplace_Internalname, AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,158);\"", 0, edtavDdo_areatrabalho_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWAreaTrabalho.htm");
         }
         wbLoad = true;
      }

      protected void START0T2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " �rea de Trabalho", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0T0( ) ;
      }

      protected void WS0T2( )
      {
         START0T2( ) ;
         EVT0T2( ) ;
      }

      protected void EVT0T2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E110T2 */
                              E110T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AREATRABALHO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E120T2 */
                              E120T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTE_RAZAOSOCIAL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E130T2 */
                              E130T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTE_CNPJ.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E140T2 */
                              E140T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATANTE_TELEFONE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E150T2 */
                              E150T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AREATRABALHO_VALIDAOSFM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E160T2 */
                              E160T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AREATRABALHO_DIASPARAPAGAR.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E170T2 */
                              E170T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ESTADO_UF.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E180T2 */
                              E180T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_MUNICIPIO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E190T2 */
                              E190T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AREATRABALHO_CALCULOPFINAL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E200T2 */
                              E200T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AREATRABALHO_VERTA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E210T2 */
                              E210T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AREATRABALHO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E220T2 */
                              E220T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E230T2 */
                              E230T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E240T2 */
                              E240T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E250T2 */
                              E250T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E260T2 */
                              E260T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E270T2 */
                              E270T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E280T2 */
                              E280T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E290T2 */
                              E290T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E300T2 */
                              E300T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E310T2 */
                              E310T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E320T2 */
                              E320T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E330T2 */
                              E330T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "VSELECIONAR.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "VSELECIONAR.CLICK") == 0 ) )
                           {
                              nGXsfl_94_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
                              SubsflControlProps_942( ) ;
                              AV29Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV184Update_GXI : context.convertURL( context.PathToRelativeUrl( AV29Update))));
                              AV28Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)) ? AV185Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV28Delete))));
                              AV94Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV94Display)) ? AV186Display_GXI : context.convertURL( context.PathToRelativeUrl( AV94Display))));
                              A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", "."));
                              A29Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratante_Codigo_Internalname), ",", "."));
                              n29Contratante_Codigo = false;
                              A6AreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtAreaTrabalho_Descricao_Internalname));
                              A9Contratante_RazaoSocial = StringUtil.Upper( cgiGet( edtContratante_RazaoSocial_Internalname));
                              n9Contratante_RazaoSocial = false;
                              A12Contratante_CNPJ = cgiGet( edtContratante_CNPJ_Internalname);
                              n12Contratante_CNPJ = false;
                              A31Contratante_Telefone = cgiGet( edtContratante_Telefone_Internalname);
                              n31Contratante_Telefone = false;
                              cmbAreaTrabalho_ValidaOSFM.Name = cmbAreaTrabalho_ValidaOSFM_Internalname;
                              cmbAreaTrabalho_ValidaOSFM.CurrentValue = cgiGet( cmbAreaTrabalho_ValidaOSFM_Internalname);
                              A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cgiGet( cmbAreaTrabalho_ValidaOSFM_Internalname));
                              n834AreaTrabalho_ValidaOSFM = false;
                              A855AreaTrabalho_DiasParaPagar = (short)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_DiasParaPagar_Internalname), ",", "."));
                              n855AreaTrabalho_DiasParaPagar = false;
                              A1216AreaTrabalho_OrganizacaoCod = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_OrganizacaoCod_Internalname), ",", "."));
                              n1216AreaTrabalho_OrganizacaoCod = false;
                              A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
                              A26Municipio_Nome = StringUtil.Upper( cgiGet( edtMunicipio_Nome_Internalname));
                              cmbAreaTrabalho_CalculoPFinal.Name = cmbAreaTrabalho_CalculoPFinal_Internalname;
                              cmbAreaTrabalho_CalculoPFinal.CurrentValue = cgiGet( cmbAreaTrabalho_CalculoPFinal_Internalname);
                              A642AreaTrabalho_CalculoPFinal = cgiGet( cmbAreaTrabalho_CalculoPFinal_Internalname);
                              cmbAreaTrabalho_VerTA.Name = cmbAreaTrabalho_VerTA_Internalname;
                              cmbAreaTrabalho_VerTA.CurrentValue = cgiGet( cmbAreaTrabalho_VerTA_Internalname);
                              A2081AreaTrabalho_VerTA = (short)(NumberUtil.Val( cgiGet( cmbAreaTrabalho_VerTA_Internalname), "."));
                              n2081AreaTrabalho_VerTA = false;
                              A72AreaTrabalho_Ativo = StringUtil.StrToBool( cgiGet( chkAreaTrabalho_Ativo_Internalname));
                              AV141Selecionar = cgiGet( edtavSelecionar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelecionar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV141Selecionar)) ? AV187Selecionar_GXI : context.convertURL( context.PathToRelativeUrl( AV141Selecionar))));
                              A10Contratante_NomeFantasia = StringUtil.Upper( cgiGet( edtContratante_NomeFantasia_Internalname));
                              A830AreaTrabalho_ServicoPadrao = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_ServicoPadrao_Internalname), ",", "."));
                              n830AreaTrabalho_ServicoPadrao = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E340T2 */
                                    E340T2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E350T2 */
                                    E350T2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E360T2 */
                                    E360T2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VSELECIONAR.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E370T2 */
                                    E370T2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Areatrabalho_descricao1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_DESCRICAO1"), AV17AreaTrabalho_Descricao1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Organizacao_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vORGANIZACAO_NOME1"), AV90Organizacao_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Areatrabalho_ativo1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_ATIVO1"), AV129AreaTrabalho_Ativo1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Areatrabalho_descricao2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_DESCRICAO2"), AV21AreaTrabalho_Descricao2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Organizacao_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vORGANIZACAO_NOME2"), AV92Organizacao_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Areatrabalho_ativo2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_ATIVO2"), AV130AreaTrabalho_Ativo2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Areatrabalho_descricao3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_DESCRICAO3"), AV25AreaTrabalho_Descricao3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Organizacao_nome3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vORGANIZACAO_NOME3"), AV131Organizacao_Nome3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Areatrabalho_ativo3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_ATIVO3"), AV132AreaTrabalho_Ativo3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfareatrabalho_descricao Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAREATRABALHO_DESCRICAO"), AV61TFAreaTrabalho_Descricao) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfareatrabalho_descricao_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAREATRABALHO_DESCRICAO_SEL"), AV62TFAreaTrabalho_Descricao_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_razaosocial Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAZAOSOCIAL"), AV65TFContratante_RazaoSocial) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_razaosocial_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAZAOSOCIAL_SEL"), AV66TFContratante_RazaoSocial_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_cnpj Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_CNPJ"), AV69TFContratante_CNPJ) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_cnpj_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_CNPJ_SEL"), AV70TFContratante_CNPJ_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_telefone Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_TELEFONE"), AV73TFContratante_Telefone) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfcontratante_telefone_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_TELEFONE_SEL"), AV74TFContratante_Telefone_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfareatrabalho_validaosfm_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAREATRABALHO_VALIDAOSFM_SEL"), ",", ".") != Convert.ToDecimal( AV77TFAreaTrabalho_ValidaOSFM_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfareatrabalho_diasparapagar Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAREATRABALHO_DIASPARAPAGAR"), ",", ".") != Convert.ToDecimal( AV80TFAreaTrabalho_DiasParaPagar )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfareatrabalho_diasparapagar_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAREATRABALHO_DIASPARAPAGAR_TO"), ",", ".") != Convert.ToDecimal( AV81TFAreaTrabalho_DiasParaPagar_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfestado_uf Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF"), AV134TFEstado_UF) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfestado_uf_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF_SEL"), AV135TFEstado_UF_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmunicipio_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME"), AV118TFMunicipio_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfmunicipio_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME_SEL"), AV119TFMunicipio_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfareatrabalho_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAREATRABALHO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV84TFAreaTrabalho_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0T2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA0T2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("AREATRABALHO_DESCRICAO", "Descri��o", 0);
            cmbavDynamicfiltersselector1.addItem("ORGANIZACAO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("AREATRABALHO_ATIVO", "Ativa", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavAreatrabalho_ativo1.Name = "vAREATRABALHO_ATIVO1";
            cmbavAreatrabalho_ativo1.WebTags = "";
            cmbavAreatrabalho_ativo1.addItem("", "(Todas)", 0);
            cmbavAreatrabalho_ativo1.addItem("S", "Sim", 0);
            cmbavAreatrabalho_ativo1.addItem("N", "N�o", 0);
            if ( cmbavAreatrabalho_ativo1.ItemCount > 0 )
            {
               AV129AreaTrabalho_Ativo1 = cmbavAreatrabalho_ativo1.getValidValue(AV129AreaTrabalho_Ativo1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129AreaTrabalho_Ativo1", AV129AreaTrabalho_Ativo1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("AREATRABALHO_DESCRICAO", "Descri��o", 0);
            cmbavDynamicfiltersselector2.addItem("ORGANIZACAO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("AREATRABALHO_ATIVO", "Ativa", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavAreatrabalho_ativo2.Name = "vAREATRABALHO_ATIVO2";
            cmbavAreatrabalho_ativo2.WebTags = "";
            cmbavAreatrabalho_ativo2.addItem("", "(Todas)", 0);
            cmbavAreatrabalho_ativo2.addItem("S", "Sim", 0);
            cmbavAreatrabalho_ativo2.addItem("N", "N�o", 0);
            if ( cmbavAreatrabalho_ativo2.ItemCount > 0 )
            {
               AV130AreaTrabalho_Ativo2 = cmbavAreatrabalho_ativo2.getValidValue(AV130AreaTrabalho_Ativo2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130AreaTrabalho_Ativo2", AV130AreaTrabalho_Ativo2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("AREATRABALHO_DESCRICAO", "Descri��o", 0);
            cmbavDynamicfiltersselector3.addItem("ORGANIZACAO_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("AREATRABALHO_ATIVO", "Ativa", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            cmbavAreatrabalho_ativo3.Name = "vAREATRABALHO_ATIVO3";
            cmbavAreatrabalho_ativo3.WebTags = "";
            cmbavAreatrabalho_ativo3.addItem("", "(Todas)", 0);
            cmbavAreatrabalho_ativo3.addItem("S", "Sim", 0);
            cmbavAreatrabalho_ativo3.addItem("N", "N�o", 0);
            if ( cmbavAreatrabalho_ativo3.ItemCount > 0 )
            {
               AV132AreaTrabalho_Ativo3 = cmbavAreatrabalho_ativo3.getValidValue(AV132AreaTrabalho_Ativo3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132AreaTrabalho_Ativo3", AV132AreaTrabalho_Ativo3);
            }
            GXCCtl = "AREATRABALHO_VALIDAOSFM_" + sGXsfl_94_idx;
            cmbAreaTrabalho_ValidaOSFM.Name = GXCCtl;
            cmbAreaTrabalho_ValidaOSFM.WebTags = "";
            cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            if ( cmbAreaTrabalho_ValidaOSFM.ItemCount > 0 )
            {
               A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cmbAreaTrabalho_ValidaOSFM.getValidValue(StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM)));
               n834AreaTrabalho_ValidaOSFM = false;
            }
            GXCCtl = "AREATRABALHO_CALCULOPFINAL_" + sGXsfl_94_idx;
            cmbAreaTrabalho_CalculoPFinal.Name = GXCCtl;
            cmbAreaTrabalho_CalculoPFinal.WebTags = "";
            cmbAreaTrabalho_CalculoPFinal.addItem("MB", "Menor PFB", 0);
            cmbAreaTrabalho_CalculoPFinal.addItem("BM", "PFB FM", 0);
            if ( cmbAreaTrabalho_CalculoPFinal.ItemCount > 0 )
            {
               A642AreaTrabalho_CalculoPFinal = cmbAreaTrabalho_CalculoPFinal.getValidValue(A642AreaTrabalho_CalculoPFinal);
            }
            GXCCtl = "AREATRABALHO_VERTA_" + sGXsfl_94_idx;
            cmbAreaTrabalho_VerTA.Name = GXCCtl;
            cmbAreaTrabalho_VerTA.WebTags = "";
            cmbAreaTrabalho_VerTA.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Padr�o", 0);
            cmbAreaTrabalho_VerTA.addItem("1", "Vers�o 1", 0);
            if ( cmbAreaTrabalho_VerTA.ItemCount > 0 )
            {
               A2081AreaTrabalho_VerTA = (short)(NumberUtil.Val( cmbAreaTrabalho_VerTA.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0))), "."));
               n2081AreaTrabalho_VerTA = false;
            }
            GXCCtl = "AREATRABALHO_ATIVO_" + sGXsfl_94_idx;
            chkAreaTrabalho_Ativo.Name = GXCCtl;
            chkAreaTrabalho_Ativo.WebTags = "";
            chkAreaTrabalho_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAreaTrabalho_Ativo_Internalname, "TitleCaption", chkAreaTrabalho_Ativo.Caption);
            chkAreaTrabalho_Ativo.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_942( ) ;
         while ( nGXsfl_94_idx <= nRC_GXsfl_94 )
         {
            sendrow_942( ) ;
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17AreaTrabalho_Descricao1 ,
                                       String AV90Organizacao_Nome1 ,
                                       String AV129AreaTrabalho_Ativo1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21AreaTrabalho_Descricao2 ,
                                       String AV92Organizacao_Nome2 ,
                                       String AV130AreaTrabalho_Ativo2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25AreaTrabalho_Descricao3 ,
                                       String AV131Organizacao_Nome3 ,
                                       String AV132AreaTrabalho_Ativo3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV61TFAreaTrabalho_Descricao ,
                                       String AV62TFAreaTrabalho_Descricao_Sel ,
                                       String AV65TFContratante_RazaoSocial ,
                                       String AV66TFContratante_RazaoSocial_Sel ,
                                       String AV69TFContratante_CNPJ ,
                                       String AV70TFContratante_CNPJ_Sel ,
                                       String AV73TFContratante_Telefone ,
                                       String AV74TFContratante_Telefone_Sel ,
                                       short AV77TFAreaTrabalho_ValidaOSFM_Sel ,
                                       short AV80TFAreaTrabalho_DiasParaPagar ,
                                       short AV81TFAreaTrabalho_DiasParaPagar_To ,
                                       String AV134TFEstado_UF ,
                                       String AV135TFEstado_UF_Sel ,
                                       String AV118TFMunicipio_Nome ,
                                       String AV119TFMunicipio_Nome_Sel ,
                                       short AV84TFAreaTrabalho_Ativo_Sel ,
                                       String AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace ,
                                       String AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace ,
                                       String AV71ddo_Contratante_CNPJTitleControlIdToReplace ,
                                       String AV75ddo_Contratante_TelefoneTitleControlIdToReplace ,
                                       String AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace ,
                                       String AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace ,
                                       String AV136ddo_Estado_UFTitleControlIdToReplace ,
                                       String AV120ddo_Municipio_NomeTitleControlIdToReplace ,
                                       String AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace ,
                                       String AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace ,
                                       String AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace ,
                                       IGxCollection AV123TFAreaTrabalho_CalculoPFinal_Sels ,
                                       IGxCollection AV144TFAreaTrabalho_VerTA_Sels ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV188Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       int A5AreaTrabalho_Codigo ,
                                       int A29Contratante_Codigo ,
                                       int A25Municipio_Codigo ,
                                       bool A72AreaTrabalho_Ativo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF0T2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_DESCRICAO", A6AreaTrabalho_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_VALIDAOSFM", GetSecureSignedToken( "", A834AreaTrabalho_ValidaOSFM));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_VALIDAOSFM", StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_DIASPARAPAGAR", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_DIASPARAPAGAR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_ORGANIZACAOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_ORGANIZACAOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CALCULOPFINAL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A642AreaTrabalho_CalculoPFinal, ""))));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CALCULOPFINAL", StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_VERTA", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2081AreaTrabalho_VerTA), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_VERTA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2081AreaTrabalho_VerTA), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_ATIVO", GetSecureSignedToken( "", A72AreaTrabalho_Ativo));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_ATIVO", StringUtil.BoolToStr( A72AreaTrabalho_Ativo));
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_SERVICOPADRAO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A830AreaTrabalho_ServicoPadrao), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_SERVICOPADRAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavAreatrabalho_ativo1.ItemCount > 0 )
         {
            AV129AreaTrabalho_Ativo1 = cmbavAreatrabalho_ativo1.getValidValue(AV129AreaTrabalho_Ativo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129AreaTrabalho_Ativo1", AV129AreaTrabalho_Ativo1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavAreatrabalho_ativo2.ItemCount > 0 )
         {
            AV130AreaTrabalho_Ativo2 = cmbavAreatrabalho_ativo2.getValidValue(AV130AreaTrabalho_Ativo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130AreaTrabalho_Ativo2", AV130AreaTrabalho_Ativo2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
         if ( cmbavAreatrabalho_ativo3.ItemCount > 0 )
         {
            AV132AreaTrabalho_Ativo3 = cmbavAreatrabalho_ativo3.getValidValue(AV132AreaTrabalho_Ativo3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132AreaTrabalho_Ativo3", AV132AreaTrabalho_Ativo3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0T2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV188Pgmname = "WWAreaTrabalho";
         context.Gx_err = 0;
      }

      protected void RF0T2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 94;
         /* Execute user event: E350T2 */
         E350T2 ();
         nGXsfl_94_idx = 1;
         sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
         SubsflControlProps_942( ) ;
         nGXsfl_94_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_942( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A5AreaTrabalho_Codigo ,
                                                 AV148Udparg1 ,
                                                 A642AreaTrabalho_CalculoPFinal ,
                                                 AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels ,
                                                 A2081AreaTrabalho_VerTA ,
                                                 AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels ,
                                                 AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 ,
                                                 AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 ,
                                                 AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 ,
                                                 AV152WWAreaTrabalhoDS_5_Organizacao_nome1 ,
                                                 AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 ,
                                                 AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 ,
                                                 AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 ,
                                                 AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 ,
                                                 AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 ,
                                                 AV158WWAreaTrabalhoDS_11_Organizacao_nome2 ,
                                                 AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 ,
                                                 AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 ,
                                                 AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 ,
                                                 AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 ,
                                                 AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 ,
                                                 AV164WWAreaTrabalhoDS_17_Organizacao_nome3 ,
                                                 AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 ,
                                                 AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel ,
                                                 AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao ,
                                                 AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel ,
                                                 AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial ,
                                                 AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel ,
                                                 AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj ,
                                                 AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel ,
                                                 AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone ,
                                                 AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel ,
                                                 AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar ,
                                                 AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to ,
                                                 AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel ,
                                                 AV177WWAreaTrabalhoDS_30_Tfestado_uf ,
                                                 AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel ,
                                                 AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome ,
                                                 AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels.Count ,
                                                 AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels.Count ,
                                                 AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel ,
                                                 A6AreaTrabalho_Descricao ,
                                                 A1214Organizacao_Nome ,
                                                 A72AreaTrabalho_Ativo ,
                                                 A9Contratante_RazaoSocial ,
                                                 A12Contratante_CNPJ ,
                                                 A31Contratante_Telefone ,
                                                 A834AreaTrabalho_ValidaOSFM ,
                                                 A855AreaTrabalho_DiasParaPagar ,
                                                 A23Estado_UF ,
                                                 A26Municipio_Nome ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 AV6WWPContext.gxTpr_Contratante_codigo ,
                                                 AV6WWPContext.gxTpr_Contratada_codigo },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1), "%", "");
            lV152WWAreaTrabalhoDS_5_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV152WWAreaTrabalhoDS_5_Organizacao_nome1), 50, "%");
            lV152WWAreaTrabalhoDS_5_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV152WWAreaTrabalhoDS_5_Organizacao_nome1), 50, "%");
            lV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2), "%", "");
            lV158WWAreaTrabalhoDS_11_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV158WWAreaTrabalhoDS_11_Organizacao_nome2), 50, "%");
            lV158WWAreaTrabalhoDS_11_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV158WWAreaTrabalhoDS_11_Organizacao_nome2), 50, "%");
            lV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3), "%", "");
            lV164WWAreaTrabalhoDS_17_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV164WWAreaTrabalhoDS_17_Organizacao_nome3), 50, "%");
            lV164WWAreaTrabalhoDS_17_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV164WWAreaTrabalhoDS_17_Organizacao_nome3), 50, "%");
            lV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao = StringUtil.Concat( StringUtil.RTrim( AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao), "%", "");
            lV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial), 100, "%");
            lV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj), "%", "");
            lV172WWAreaTrabalhoDS_25_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone), 20, "%");
            lV177WWAreaTrabalhoDS_30_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV177WWAreaTrabalhoDS_30_Tfestado_uf), 2, "%");
            lV179WWAreaTrabalhoDS_32_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome), 50, "%");
            /* Using cursor H000T2 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Contratante_codigo, AV6WWPContext.gxTpr_Contratada_codigo, lV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1, lV152WWAreaTrabalhoDS_5_Organizacao_nome1, lV152WWAreaTrabalhoDS_5_Organizacao_nome1, lV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2, lV158WWAreaTrabalhoDS_11_Organizacao_nome2, lV158WWAreaTrabalhoDS_11_Organizacao_nome2, lV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3, lV164WWAreaTrabalhoDS_17_Organizacao_nome3, lV164WWAreaTrabalhoDS_17_Organizacao_nome3, lV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao, AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel, lV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial, AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel, lV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj, AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel, lV172WWAreaTrabalhoDS_25_Tfcontratante_telefone, AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel, AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar, AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to, lV177WWAreaTrabalhoDS_30_Tfestado_uf, AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel, lV179WWAreaTrabalhoDS_32_Tfmunicipio_nome, AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_94_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A335Contratante_PessoaCod = H000T2_A335Contratante_PessoaCod[0];
               A1214Organizacao_Nome = H000T2_A1214Organizacao_Nome[0];
               n1214Organizacao_Nome = H000T2_n1214Organizacao_Nome[0];
               A25Municipio_Codigo = H000T2_A25Municipio_Codigo[0];
               n25Municipio_Codigo = H000T2_n25Municipio_Codigo[0];
               A830AreaTrabalho_ServicoPadrao = H000T2_A830AreaTrabalho_ServicoPadrao[0];
               n830AreaTrabalho_ServicoPadrao = H000T2_n830AreaTrabalho_ServicoPadrao[0];
               A10Contratante_NomeFantasia = H000T2_A10Contratante_NomeFantasia[0];
               A72AreaTrabalho_Ativo = H000T2_A72AreaTrabalho_Ativo[0];
               A2081AreaTrabalho_VerTA = H000T2_A2081AreaTrabalho_VerTA[0];
               n2081AreaTrabalho_VerTA = H000T2_n2081AreaTrabalho_VerTA[0];
               A642AreaTrabalho_CalculoPFinal = H000T2_A642AreaTrabalho_CalculoPFinal[0];
               A26Municipio_Nome = H000T2_A26Municipio_Nome[0];
               A23Estado_UF = H000T2_A23Estado_UF[0];
               A1216AreaTrabalho_OrganizacaoCod = H000T2_A1216AreaTrabalho_OrganizacaoCod[0];
               n1216AreaTrabalho_OrganizacaoCod = H000T2_n1216AreaTrabalho_OrganizacaoCod[0];
               A855AreaTrabalho_DiasParaPagar = H000T2_A855AreaTrabalho_DiasParaPagar[0];
               n855AreaTrabalho_DiasParaPagar = H000T2_n855AreaTrabalho_DiasParaPagar[0];
               A834AreaTrabalho_ValidaOSFM = H000T2_A834AreaTrabalho_ValidaOSFM[0];
               n834AreaTrabalho_ValidaOSFM = H000T2_n834AreaTrabalho_ValidaOSFM[0];
               A31Contratante_Telefone = H000T2_A31Contratante_Telefone[0];
               n31Contratante_Telefone = H000T2_n31Contratante_Telefone[0];
               A12Contratante_CNPJ = H000T2_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = H000T2_n12Contratante_CNPJ[0];
               A9Contratante_RazaoSocial = H000T2_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H000T2_n9Contratante_RazaoSocial[0];
               A6AreaTrabalho_Descricao = H000T2_A6AreaTrabalho_Descricao[0];
               A29Contratante_Codigo = H000T2_A29Contratante_Codigo[0];
               n29Contratante_Codigo = H000T2_n29Contratante_Codigo[0];
               A5AreaTrabalho_Codigo = H000T2_A5AreaTrabalho_Codigo[0];
               A1214Organizacao_Nome = H000T2_A1214Organizacao_Nome[0];
               n1214Organizacao_Nome = H000T2_n1214Organizacao_Nome[0];
               A335Contratante_PessoaCod = H000T2_A335Contratante_PessoaCod[0];
               A25Municipio_Codigo = H000T2_A25Municipio_Codigo[0];
               n25Municipio_Codigo = H000T2_n25Municipio_Codigo[0];
               A10Contratante_NomeFantasia = H000T2_A10Contratante_NomeFantasia[0];
               A31Contratante_Telefone = H000T2_A31Contratante_Telefone[0];
               n31Contratante_Telefone = H000T2_n31Contratante_Telefone[0];
               A12Contratante_CNPJ = H000T2_A12Contratante_CNPJ[0];
               n12Contratante_CNPJ = H000T2_n12Contratante_CNPJ[0];
               A9Contratante_RazaoSocial = H000T2_A9Contratante_RazaoSocial[0];
               n9Contratante_RazaoSocial = H000T2_n9Contratante_RazaoSocial[0];
               A26Municipio_Nome = H000T2_A26Municipio_Nome[0];
               A23Estado_UF = H000T2_A23Estado_UF[0];
               /* Execute user event: E360T2 */
               E360T2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 94;
            WB0T0( ) ;
         }
         nGXsfl_94_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 = AV17AreaTrabalho_Descricao1;
         AV152WWAreaTrabalhoDS_5_Organizacao_nome1 = AV90Organizacao_Nome1;
         AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 = AV129AreaTrabalho_Ativo1;
         AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 = AV21AreaTrabalho_Descricao2;
         AV158WWAreaTrabalhoDS_11_Organizacao_nome2 = AV92Organizacao_Nome2;
         AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 = AV130AreaTrabalho_Ativo2;
         AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 = AV25AreaTrabalho_Descricao3;
         AV164WWAreaTrabalhoDS_17_Organizacao_nome3 = AV131Organizacao_Nome3;
         AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 = AV132AreaTrabalho_Ativo3;
         AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao = AV61TFAreaTrabalho_Descricao;
         AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel = AV62TFAreaTrabalho_Descricao_Sel;
         AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial = AV65TFContratante_RazaoSocial;
         AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel = AV66TFContratante_RazaoSocial_Sel;
         AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj = AV69TFContratante_CNPJ;
         AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel = AV70TFContratante_CNPJ_Sel;
         AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone = AV73TFContratante_Telefone;
         AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel = AV74TFContratante_Telefone_Sel;
         AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel = AV77TFAreaTrabalho_ValidaOSFM_Sel;
         AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar = AV80TFAreaTrabalho_DiasParaPagar;
         AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to = AV81TFAreaTrabalho_DiasParaPagar_To;
         AV177WWAreaTrabalhoDS_30_Tfestado_uf = AV134TFEstado_UF;
         AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel = AV135TFEstado_UF_Sel;
         AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome = AV118TFMunicipio_Nome;
         AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel = AV119TFMunicipio_Nome_Sel;
         AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels = AV123TFAreaTrabalho_CalculoPFinal_Sels;
         AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels = AV144TFAreaTrabalho_VerTA_Sels;
         AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel = AV84TFAreaTrabalho_Ativo_Sel;
         AV148Udparg1 = new prc_areasdousuario(context).executeUdp(  AV6WWPContext.gxTpr_Userid);
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A5AreaTrabalho_Codigo ,
                                              AV148Udparg1 ,
                                              A642AreaTrabalho_CalculoPFinal ,
                                              AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels ,
                                              A2081AreaTrabalho_VerTA ,
                                              AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels ,
                                              AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 ,
                                              AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 ,
                                              AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 ,
                                              AV152WWAreaTrabalhoDS_5_Organizacao_nome1 ,
                                              AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 ,
                                              AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 ,
                                              AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 ,
                                              AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 ,
                                              AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 ,
                                              AV158WWAreaTrabalhoDS_11_Organizacao_nome2 ,
                                              AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 ,
                                              AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 ,
                                              AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 ,
                                              AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 ,
                                              AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 ,
                                              AV164WWAreaTrabalhoDS_17_Organizacao_nome3 ,
                                              AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 ,
                                              AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel ,
                                              AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao ,
                                              AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel ,
                                              AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial ,
                                              AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel ,
                                              AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj ,
                                              AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel ,
                                              AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone ,
                                              AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel ,
                                              AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar ,
                                              AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to ,
                                              AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel ,
                                              AV177WWAreaTrabalhoDS_30_Tfestado_uf ,
                                              AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel ,
                                              AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome ,
                                              AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels.Count ,
                                              AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels.Count ,
                                              AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel ,
                                              A6AreaTrabalho_Descricao ,
                                              A1214Organizacao_Nome ,
                                              A72AreaTrabalho_Ativo ,
                                              A9Contratante_RazaoSocial ,
                                              A12Contratante_CNPJ ,
                                              A31Contratante_Telefone ,
                                              A834AreaTrabalho_ValidaOSFM ,
                                              A855AreaTrabalho_DiasParaPagar ,
                                              A23Estado_UF ,
                                              A26Municipio_Nome ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              AV6WWPContext.gxTpr_Contratante_codigo ,
                                              AV6WWPContext.gxTpr_Contratada_codigo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 = StringUtil.Concat( StringUtil.RTrim( AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1), "%", "");
         lV152WWAreaTrabalhoDS_5_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV152WWAreaTrabalhoDS_5_Organizacao_nome1), 50, "%");
         lV152WWAreaTrabalhoDS_5_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV152WWAreaTrabalhoDS_5_Organizacao_nome1), 50, "%");
         lV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 = StringUtil.Concat( StringUtil.RTrim( AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2), "%", "");
         lV158WWAreaTrabalhoDS_11_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV158WWAreaTrabalhoDS_11_Organizacao_nome2), 50, "%");
         lV158WWAreaTrabalhoDS_11_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV158WWAreaTrabalhoDS_11_Organizacao_nome2), 50, "%");
         lV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 = StringUtil.Concat( StringUtil.RTrim( AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3), "%", "");
         lV164WWAreaTrabalhoDS_17_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV164WWAreaTrabalhoDS_17_Organizacao_nome3), 50, "%");
         lV164WWAreaTrabalhoDS_17_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV164WWAreaTrabalhoDS_17_Organizacao_nome3), 50, "%");
         lV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao = StringUtil.Concat( StringUtil.RTrim( AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao), "%", "");
         lV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial = StringUtil.PadR( StringUtil.RTrim( AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial), 100, "%");
         lV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj = StringUtil.Concat( StringUtil.RTrim( AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj), "%", "");
         lV172WWAreaTrabalhoDS_25_Tfcontratante_telefone = StringUtil.PadR( StringUtil.RTrim( AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone), 20, "%");
         lV177WWAreaTrabalhoDS_30_Tfestado_uf = StringUtil.PadR( StringUtil.RTrim( AV177WWAreaTrabalhoDS_30_Tfestado_uf), 2, "%");
         lV179WWAreaTrabalhoDS_32_Tfmunicipio_nome = StringUtil.PadR( StringUtil.RTrim( AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome), 50, "%");
         /* Using cursor H000T3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Contratante_codigo, AV6WWPContext.gxTpr_Contratada_codigo, lV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1, lV152WWAreaTrabalhoDS_5_Organizacao_nome1, lV152WWAreaTrabalhoDS_5_Organizacao_nome1, lV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2, lV158WWAreaTrabalhoDS_11_Organizacao_nome2, lV158WWAreaTrabalhoDS_11_Organizacao_nome2, lV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3, lV164WWAreaTrabalhoDS_17_Organizacao_nome3, lV164WWAreaTrabalhoDS_17_Organizacao_nome3, lV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao, AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel, lV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial, AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel, lV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj, AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel, lV172WWAreaTrabalhoDS_25_Tfcontratante_telefone, AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel, AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar, AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to, lV177WWAreaTrabalhoDS_30_Tfestado_uf, AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel, lV179WWAreaTrabalhoDS_32_Tfmunicipio_nome, AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel});
         GRID_nRecordCount = H000T3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 = AV17AreaTrabalho_Descricao1;
         AV152WWAreaTrabalhoDS_5_Organizacao_nome1 = AV90Organizacao_Nome1;
         AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 = AV129AreaTrabalho_Ativo1;
         AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 = AV21AreaTrabalho_Descricao2;
         AV158WWAreaTrabalhoDS_11_Organizacao_nome2 = AV92Organizacao_Nome2;
         AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 = AV130AreaTrabalho_Ativo2;
         AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 = AV25AreaTrabalho_Descricao3;
         AV164WWAreaTrabalhoDS_17_Organizacao_nome3 = AV131Organizacao_Nome3;
         AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 = AV132AreaTrabalho_Ativo3;
         AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao = AV61TFAreaTrabalho_Descricao;
         AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel = AV62TFAreaTrabalho_Descricao_Sel;
         AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial = AV65TFContratante_RazaoSocial;
         AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel = AV66TFContratante_RazaoSocial_Sel;
         AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj = AV69TFContratante_CNPJ;
         AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel = AV70TFContratante_CNPJ_Sel;
         AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone = AV73TFContratante_Telefone;
         AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel = AV74TFContratante_Telefone_Sel;
         AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel = AV77TFAreaTrabalho_ValidaOSFM_Sel;
         AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar = AV80TFAreaTrabalho_DiasParaPagar;
         AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to = AV81TFAreaTrabalho_DiasParaPagar_To;
         AV177WWAreaTrabalhoDS_30_Tfestado_uf = AV134TFEstado_UF;
         AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel = AV135TFEstado_UF_Sel;
         AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome = AV118TFMunicipio_Nome;
         AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel = AV119TFMunicipio_Nome_Sel;
         AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels = AV123TFAreaTrabalho_CalculoPFinal_Sels;
         AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels = AV144TFAreaTrabalho_VerTA_Sels;
         AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel = AV84TFAreaTrabalho_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV90Organizacao_Nome1, AV129AreaTrabalho_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21AreaTrabalho_Descricao2, AV92Organizacao_Nome2, AV130AreaTrabalho_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25AreaTrabalho_Descricao3, AV131Organizacao_Nome3, AV132AreaTrabalho_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV61TFAreaTrabalho_Descricao, AV62TFAreaTrabalho_Descricao_Sel, AV65TFContratante_RazaoSocial, AV66TFContratante_RazaoSocial_Sel, AV69TFContratante_CNPJ, AV70TFContratante_CNPJ_Sel, AV73TFContratante_Telefone, AV74TFContratante_Telefone_Sel, AV77TFAreaTrabalho_ValidaOSFM_Sel, AV80TFAreaTrabalho_DiasParaPagar, AV81TFAreaTrabalho_DiasParaPagar_To, AV134TFEstado_UF, AV135TFEstado_UF_Sel, AV118TFMunicipio_Nome, AV119TFMunicipio_Nome_Sel, AV84TFAreaTrabalho_Ativo_Sel, AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV71ddo_Contratante_CNPJTitleControlIdToReplace, AV75ddo_Contratante_TelefoneTitleControlIdToReplace, AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace, AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace, AV136ddo_Estado_UFTitleControlIdToReplace, AV120ddo_Municipio_NomeTitleControlIdToReplace, AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace, AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace, AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace, AV123TFAreaTrabalho_CalculoPFinal_Sels, AV144TFAreaTrabalho_VerTA_Sels, AV6WWPContext, AV188Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A5AreaTrabalho_Codigo, A29Contratante_Codigo, A25Municipio_Codigo, A72AreaTrabalho_Ativo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 = AV17AreaTrabalho_Descricao1;
         AV152WWAreaTrabalhoDS_5_Organizacao_nome1 = AV90Organizacao_Nome1;
         AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 = AV129AreaTrabalho_Ativo1;
         AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 = AV21AreaTrabalho_Descricao2;
         AV158WWAreaTrabalhoDS_11_Organizacao_nome2 = AV92Organizacao_Nome2;
         AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 = AV130AreaTrabalho_Ativo2;
         AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 = AV25AreaTrabalho_Descricao3;
         AV164WWAreaTrabalhoDS_17_Organizacao_nome3 = AV131Organizacao_Nome3;
         AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 = AV132AreaTrabalho_Ativo3;
         AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao = AV61TFAreaTrabalho_Descricao;
         AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel = AV62TFAreaTrabalho_Descricao_Sel;
         AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial = AV65TFContratante_RazaoSocial;
         AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel = AV66TFContratante_RazaoSocial_Sel;
         AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj = AV69TFContratante_CNPJ;
         AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel = AV70TFContratante_CNPJ_Sel;
         AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone = AV73TFContratante_Telefone;
         AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel = AV74TFContratante_Telefone_Sel;
         AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel = AV77TFAreaTrabalho_ValidaOSFM_Sel;
         AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar = AV80TFAreaTrabalho_DiasParaPagar;
         AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to = AV81TFAreaTrabalho_DiasParaPagar_To;
         AV177WWAreaTrabalhoDS_30_Tfestado_uf = AV134TFEstado_UF;
         AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel = AV135TFEstado_UF_Sel;
         AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome = AV118TFMunicipio_Nome;
         AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel = AV119TFMunicipio_Nome_Sel;
         AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels = AV123TFAreaTrabalho_CalculoPFinal_Sels;
         AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels = AV144TFAreaTrabalho_VerTA_Sels;
         AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel = AV84TFAreaTrabalho_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV90Organizacao_Nome1, AV129AreaTrabalho_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21AreaTrabalho_Descricao2, AV92Organizacao_Nome2, AV130AreaTrabalho_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25AreaTrabalho_Descricao3, AV131Organizacao_Nome3, AV132AreaTrabalho_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV61TFAreaTrabalho_Descricao, AV62TFAreaTrabalho_Descricao_Sel, AV65TFContratante_RazaoSocial, AV66TFContratante_RazaoSocial_Sel, AV69TFContratante_CNPJ, AV70TFContratante_CNPJ_Sel, AV73TFContratante_Telefone, AV74TFContratante_Telefone_Sel, AV77TFAreaTrabalho_ValidaOSFM_Sel, AV80TFAreaTrabalho_DiasParaPagar, AV81TFAreaTrabalho_DiasParaPagar_To, AV134TFEstado_UF, AV135TFEstado_UF_Sel, AV118TFMunicipio_Nome, AV119TFMunicipio_Nome_Sel, AV84TFAreaTrabalho_Ativo_Sel, AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV71ddo_Contratante_CNPJTitleControlIdToReplace, AV75ddo_Contratante_TelefoneTitleControlIdToReplace, AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace, AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace, AV136ddo_Estado_UFTitleControlIdToReplace, AV120ddo_Municipio_NomeTitleControlIdToReplace, AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace, AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace, AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace, AV123TFAreaTrabalho_CalculoPFinal_Sels, AV144TFAreaTrabalho_VerTA_Sels, AV6WWPContext, AV188Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A5AreaTrabalho_Codigo, A29Contratante_Codigo, A25Municipio_Codigo, A72AreaTrabalho_Ativo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 = AV17AreaTrabalho_Descricao1;
         AV152WWAreaTrabalhoDS_5_Organizacao_nome1 = AV90Organizacao_Nome1;
         AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 = AV129AreaTrabalho_Ativo1;
         AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 = AV21AreaTrabalho_Descricao2;
         AV158WWAreaTrabalhoDS_11_Organizacao_nome2 = AV92Organizacao_Nome2;
         AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 = AV130AreaTrabalho_Ativo2;
         AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 = AV25AreaTrabalho_Descricao3;
         AV164WWAreaTrabalhoDS_17_Organizacao_nome3 = AV131Organizacao_Nome3;
         AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 = AV132AreaTrabalho_Ativo3;
         AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao = AV61TFAreaTrabalho_Descricao;
         AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel = AV62TFAreaTrabalho_Descricao_Sel;
         AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial = AV65TFContratante_RazaoSocial;
         AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel = AV66TFContratante_RazaoSocial_Sel;
         AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj = AV69TFContratante_CNPJ;
         AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel = AV70TFContratante_CNPJ_Sel;
         AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone = AV73TFContratante_Telefone;
         AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel = AV74TFContratante_Telefone_Sel;
         AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel = AV77TFAreaTrabalho_ValidaOSFM_Sel;
         AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar = AV80TFAreaTrabalho_DiasParaPagar;
         AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to = AV81TFAreaTrabalho_DiasParaPagar_To;
         AV177WWAreaTrabalhoDS_30_Tfestado_uf = AV134TFEstado_UF;
         AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel = AV135TFEstado_UF_Sel;
         AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome = AV118TFMunicipio_Nome;
         AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel = AV119TFMunicipio_Nome_Sel;
         AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels = AV123TFAreaTrabalho_CalculoPFinal_Sels;
         AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels = AV144TFAreaTrabalho_VerTA_Sels;
         AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel = AV84TFAreaTrabalho_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV90Organizacao_Nome1, AV129AreaTrabalho_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21AreaTrabalho_Descricao2, AV92Organizacao_Nome2, AV130AreaTrabalho_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25AreaTrabalho_Descricao3, AV131Organizacao_Nome3, AV132AreaTrabalho_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV61TFAreaTrabalho_Descricao, AV62TFAreaTrabalho_Descricao_Sel, AV65TFContratante_RazaoSocial, AV66TFContratante_RazaoSocial_Sel, AV69TFContratante_CNPJ, AV70TFContratante_CNPJ_Sel, AV73TFContratante_Telefone, AV74TFContratante_Telefone_Sel, AV77TFAreaTrabalho_ValidaOSFM_Sel, AV80TFAreaTrabalho_DiasParaPagar, AV81TFAreaTrabalho_DiasParaPagar_To, AV134TFEstado_UF, AV135TFEstado_UF_Sel, AV118TFMunicipio_Nome, AV119TFMunicipio_Nome_Sel, AV84TFAreaTrabalho_Ativo_Sel, AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV71ddo_Contratante_CNPJTitleControlIdToReplace, AV75ddo_Contratante_TelefoneTitleControlIdToReplace, AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace, AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace, AV136ddo_Estado_UFTitleControlIdToReplace, AV120ddo_Municipio_NomeTitleControlIdToReplace, AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace, AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace, AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace, AV123TFAreaTrabalho_CalculoPFinal_Sels, AV144TFAreaTrabalho_VerTA_Sels, AV6WWPContext, AV188Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A5AreaTrabalho_Codigo, A29Contratante_Codigo, A25Municipio_Codigo, A72AreaTrabalho_Ativo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 = AV17AreaTrabalho_Descricao1;
         AV152WWAreaTrabalhoDS_5_Organizacao_nome1 = AV90Organizacao_Nome1;
         AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 = AV129AreaTrabalho_Ativo1;
         AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 = AV21AreaTrabalho_Descricao2;
         AV158WWAreaTrabalhoDS_11_Organizacao_nome2 = AV92Organizacao_Nome2;
         AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 = AV130AreaTrabalho_Ativo2;
         AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 = AV25AreaTrabalho_Descricao3;
         AV164WWAreaTrabalhoDS_17_Organizacao_nome3 = AV131Organizacao_Nome3;
         AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 = AV132AreaTrabalho_Ativo3;
         AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao = AV61TFAreaTrabalho_Descricao;
         AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel = AV62TFAreaTrabalho_Descricao_Sel;
         AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial = AV65TFContratante_RazaoSocial;
         AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel = AV66TFContratante_RazaoSocial_Sel;
         AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj = AV69TFContratante_CNPJ;
         AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel = AV70TFContratante_CNPJ_Sel;
         AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone = AV73TFContratante_Telefone;
         AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel = AV74TFContratante_Telefone_Sel;
         AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel = AV77TFAreaTrabalho_ValidaOSFM_Sel;
         AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar = AV80TFAreaTrabalho_DiasParaPagar;
         AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to = AV81TFAreaTrabalho_DiasParaPagar_To;
         AV177WWAreaTrabalhoDS_30_Tfestado_uf = AV134TFEstado_UF;
         AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel = AV135TFEstado_UF_Sel;
         AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome = AV118TFMunicipio_Nome;
         AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel = AV119TFMunicipio_Nome_Sel;
         AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels = AV123TFAreaTrabalho_CalculoPFinal_Sels;
         AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels = AV144TFAreaTrabalho_VerTA_Sels;
         AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel = AV84TFAreaTrabalho_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV90Organizacao_Nome1, AV129AreaTrabalho_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21AreaTrabalho_Descricao2, AV92Organizacao_Nome2, AV130AreaTrabalho_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25AreaTrabalho_Descricao3, AV131Organizacao_Nome3, AV132AreaTrabalho_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV61TFAreaTrabalho_Descricao, AV62TFAreaTrabalho_Descricao_Sel, AV65TFContratante_RazaoSocial, AV66TFContratante_RazaoSocial_Sel, AV69TFContratante_CNPJ, AV70TFContratante_CNPJ_Sel, AV73TFContratante_Telefone, AV74TFContratante_Telefone_Sel, AV77TFAreaTrabalho_ValidaOSFM_Sel, AV80TFAreaTrabalho_DiasParaPagar, AV81TFAreaTrabalho_DiasParaPagar_To, AV134TFEstado_UF, AV135TFEstado_UF_Sel, AV118TFMunicipio_Nome, AV119TFMunicipio_Nome_Sel, AV84TFAreaTrabalho_Ativo_Sel, AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV71ddo_Contratante_CNPJTitleControlIdToReplace, AV75ddo_Contratante_TelefoneTitleControlIdToReplace, AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace, AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace, AV136ddo_Estado_UFTitleControlIdToReplace, AV120ddo_Municipio_NomeTitleControlIdToReplace, AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace, AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace, AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace, AV123TFAreaTrabalho_CalculoPFinal_Sels, AV144TFAreaTrabalho_VerTA_Sels, AV6WWPContext, AV188Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A5AreaTrabalho_Codigo, A29Contratante_Codigo, A25Municipio_Codigo, A72AreaTrabalho_Ativo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 = AV17AreaTrabalho_Descricao1;
         AV152WWAreaTrabalhoDS_5_Organizacao_nome1 = AV90Organizacao_Nome1;
         AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 = AV129AreaTrabalho_Ativo1;
         AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 = AV21AreaTrabalho_Descricao2;
         AV158WWAreaTrabalhoDS_11_Organizacao_nome2 = AV92Organizacao_Nome2;
         AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 = AV130AreaTrabalho_Ativo2;
         AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 = AV25AreaTrabalho_Descricao3;
         AV164WWAreaTrabalhoDS_17_Organizacao_nome3 = AV131Organizacao_Nome3;
         AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 = AV132AreaTrabalho_Ativo3;
         AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao = AV61TFAreaTrabalho_Descricao;
         AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel = AV62TFAreaTrabalho_Descricao_Sel;
         AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial = AV65TFContratante_RazaoSocial;
         AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel = AV66TFContratante_RazaoSocial_Sel;
         AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj = AV69TFContratante_CNPJ;
         AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel = AV70TFContratante_CNPJ_Sel;
         AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone = AV73TFContratante_Telefone;
         AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel = AV74TFContratante_Telefone_Sel;
         AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel = AV77TFAreaTrabalho_ValidaOSFM_Sel;
         AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar = AV80TFAreaTrabalho_DiasParaPagar;
         AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to = AV81TFAreaTrabalho_DiasParaPagar_To;
         AV177WWAreaTrabalhoDS_30_Tfestado_uf = AV134TFEstado_UF;
         AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel = AV135TFEstado_UF_Sel;
         AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome = AV118TFMunicipio_Nome;
         AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel = AV119TFMunicipio_Nome_Sel;
         AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels = AV123TFAreaTrabalho_CalculoPFinal_Sels;
         AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels = AV144TFAreaTrabalho_VerTA_Sels;
         AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel = AV84TFAreaTrabalho_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV90Organizacao_Nome1, AV129AreaTrabalho_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21AreaTrabalho_Descricao2, AV92Organizacao_Nome2, AV130AreaTrabalho_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25AreaTrabalho_Descricao3, AV131Organizacao_Nome3, AV132AreaTrabalho_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV61TFAreaTrabalho_Descricao, AV62TFAreaTrabalho_Descricao_Sel, AV65TFContratante_RazaoSocial, AV66TFContratante_RazaoSocial_Sel, AV69TFContratante_CNPJ, AV70TFContratante_CNPJ_Sel, AV73TFContratante_Telefone, AV74TFContratante_Telefone_Sel, AV77TFAreaTrabalho_ValidaOSFM_Sel, AV80TFAreaTrabalho_DiasParaPagar, AV81TFAreaTrabalho_DiasParaPagar_To, AV134TFEstado_UF, AV135TFEstado_UF_Sel, AV118TFMunicipio_Nome, AV119TFMunicipio_Nome_Sel, AV84TFAreaTrabalho_Ativo_Sel, AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV71ddo_Contratante_CNPJTitleControlIdToReplace, AV75ddo_Contratante_TelefoneTitleControlIdToReplace, AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace, AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace, AV136ddo_Estado_UFTitleControlIdToReplace, AV120ddo_Municipio_NomeTitleControlIdToReplace, AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace, AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace, AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace, AV123TFAreaTrabalho_CalculoPFinal_Sels, AV144TFAreaTrabalho_VerTA_Sels, AV6WWPContext, AV188Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A5AreaTrabalho_Codigo, A29Contratante_Codigo, A25Municipio_Codigo, A72AreaTrabalho_Ativo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP0T0( )
      {
         /* Before Start, stand alone formulas. */
         AV188Pgmname = "WWAreaTrabalho";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E340T2 */
         E340T2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV86DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vAREATRABALHO_DESCRICAOTITLEFILTERDATA"), AV60AreaTrabalho_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTE_RAZAOSOCIALTITLEFILTERDATA"), AV64Contratante_RazaoSocialTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTE_CNPJTITLEFILTERDATA"), AV68Contratante_CNPJTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATANTE_TELEFONETITLEFILTERDATA"), AV72Contratante_TelefoneTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAREATRABALHO_VALIDAOSFMTITLEFILTERDATA"), AV76AreaTrabalho_ValidaOSFMTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAREATRABALHO_DIASPARAPAGARTITLEFILTERDATA"), AV79AreaTrabalho_DiasParaPagarTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vESTADO_UFTITLEFILTERDATA"), AV133Estado_UFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vMUNICIPIO_NOMETITLEFILTERDATA"), AV117Municipio_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAREATRABALHO_CALCULOPFINALTITLEFILTERDATA"), AV121AreaTrabalho_CalculoPFinalTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAREATRABALHO_VERTATITLEFILTERDATA"), AV142AreaTrabalho_VerTATitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vAREATRABALHO_ATIVOTITLEFILTERDATA"), AV83AreaTrabalho_AtivoTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17AreaTrabalho_Descricao1 = StringUtil.Upper( cgiGet( edtavAreatrabalho_descricao1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
            AV90Organizacao_Nome1 = StringUtil.Upper( cgiGet( edtavOrganizacao_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90Organizacao_Nome1", AV90Organizacao_Nome1);
            cmbavAreatrabalho_ativo1.Name = cmbavAreatrabalho_ativo1_Internalname;
            cmbavAreatrabalho_ativo1.CurrentValue = cgiGet( cmbavAreatrabalho_ativo1_Internalname);
            AV129AreaTrabalho_Ativo1 = cgiGet( cmbavAreatrabalho_ativo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129AreaTrabalho_Ativo1", AV129AreaTrabalho_Ativo1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21AreaTrabalho_Descricao2 = StringUtil.Upper( cgiGet( edtavAreatrabalho_descricao2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AreaTrabalho_Descricao2", AV21AreaTrabalho_Descricao2);
            AV92Organizacao_Nome2 = StringUtil.Upper( cgiGet( edtavOrganizacao_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92Organizacao_Nome2", AV92Organizacao_Nome2);
            cmbavAreatrabalho_ativo2.Name = cmbavAreatrabalho_ativo2_Internalname;
            cmbavAreatrabalho_ativo2.CurrentValue = cgiGet( cmbavAreatrabalho_ativo2_Internalname);
            AV130AreaTrabalho_Ativo2 = cgiGet( cmbavAreatrabalho_ativo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130AreaTrabalho_Ativo2", AV130AreaTrabalho_Ativo2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25AreaTrabalho_Descricao3 = StringUtil.Upper( cgiGet( edtavAreatrabalho_descricao3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AreaTrabalho_Descricao3", AV25AreaTrabalho_Descricao3);
            AV131Organizacao_Nome3 = StringUtil.Upper( cgiGet( edtavOrganizacao_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131Organizacao_Nome3", AV131Organizacao_Nome3);
            cmbavAreatrabalho_ativo3.Name = cmbavAreatrabalho_ativo3_Internalname;
            cmbavAreatrabalho_ativo3.CurrentValue = cgiGet( cmbavAreatrabalho_ativo3_Internalname);
            AV132AreaTrabalho_Ativo3 = cgiGet( cmbavAreatrabalho_ativo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132AreaTrabalho_Ativo3", AV132AreaTrabalho_Ativo3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV61TFAreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtavTfareatrabalho_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFAreaTrabalho_Descricao", AV61TFAreaTrabalho_Descricao);
            AV62TFAreaTrabalho_Descricao_Sel = StringUtil.Upper( cgiGet( edtavTfareatrabalho_descricao_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFAreaTrabalho_Descricao_Sel", AV62TFAreaTrabalho_Descricao_Sel);
            AV65TFContratante_RazaoSocial = StringUtil.Upper( cgiGet( edtavTfcontratante_razaosocial_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratante_RazaoSocial", AV65TFContratante_RazaoSocial);
            AV66TFContratante_RazaoSocial_Sel = StringUtil.Upper( cgiGet( edtavTfcontratante_razaosocial_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratante_RazaoSocial_Sel", AV66TFContratante_RazaoSocial_Sel);
            AV69TFContratante_CNPJ = cgiGet( edtavTfcontratante_cnpj_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratante_CNPJ", AV69TFContratante_CNPJ);
            AV70TFContratante_CNPJ_Sel = cgiGet( edtavTfcontratante_cnpj_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratante_CNPJ_Sel", AV70TFContratante_CNPJ_Sel);
            AV73TFContratante_Telefone = cgiGet( edtavTfcontratante_telefone_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratante_Telefone", AV73TFContratante_Telefone);
            AV74TFContratante_Telefone_Sel = cgiGet( edtavTfcontratante_telefone_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratante_Telefone_Sel", AV74TFContratante_Telefone_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfareatrabalho_validaosfm_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfareatrabalho_validaosfm_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAREATRABALHO_VALIDAOSFM_SEL");
               GX_FocusControl = edtavTfareatrabalho_validaosfm_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77TFAreaTrabalho_ValidaOSFM_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAreaTrabalho_ValidaOSFM_Sel", StringUtil.Str( (decimal)(AV77TFAreaTrabalho_ValidaOSFM_Sel), 1, 0));
            }
            else
            {
               AV77TFAreaTrabalho_ValidaOSFM_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfareatrabalho_validaosfm_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAreaTrabalho_ValidaOSFM_Sel", StringUtil.Str( (decimal)(AV77TFAreaTrabalho_ValidaOSFM_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfareatrabalho_diasparapagar_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfareatrabalho_diasparapagar_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAREATRABALHO_DIASPARAPAGAR");
               GX_FocusControl = edtavTfareatrabalho_diasparapagar_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV80TFAreaTrabalho_DiasParaPagar = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFAreaTrabalho_DiasParaPagar), 4, 0)));
            }
            else
            {
               AV80TFAreaTrabalho_DiasParaPagar = (short)(context.localUtil.CToN( cgiGet( edtavTfareatrabalho_diasparapagar_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFAreaTrabalho_DiasParaPagar), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfareatrabalho_diasparapagar_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfareatrabalho_diasparapagar_to_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAREATRABALHO_DIASPARAPAGAR_TO");
               GX_FocusControl = edtavTfareatrabalho_diasparapagar_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81TFAreaTrabalho_DiasParaPagar_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFAreaTrabalho_DiasParaPagar_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFAreaTrabalho_DiasParaPagar_To), 4, 0)));
            }
            else
            {
               AV81TFAreaTrabalho_DiasParaPagar_To = (short)(context.localUtil.CToN( cgiGet( edtavTfareatrabalho_diasparapagar_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFAreaTrabalho_DiasParaPagar_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFAreaTrabalho_DiasParaPagar_To), 4, 0)));
            }
            AV134TFEstado_UF = StringUtil.Upper( cgiGet( edtavTfestado_uf_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134TFEstado_UF", AV134TFEstado_UF);
            AV135TFEstado_UF_Sel = StringUtil.Upper( cgiGet( edtavTfestado_uf_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135TFEstado_UF_Sel", AV135TFEstado_UF_Sel);
            AV118TFMunicipio_Nome = StringUtil.Upper( cgiGet( edtavTfmunicipio_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118TFMunicipio_Nome", AV118TFMunicipio_Nome);
            AV119TFMunicipio_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfmunicipio_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119TFMunicipio_Nome_Sel", AV119TFMunicipio_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfareatrabalho_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfareatrabalho_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAREATRABALHO_ATIVO_SEL");
               GX_FocusControl = edtavTfareatrabalho_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV84TFAreaTrabalho_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAreaTrabalho_Ativo_Sel", StringUtil.Str( (decimal)(AV84TFAreaTrabalho_Ativo_Sel), 1, 0));
            }
            else
            {
               AV84TFAreaTrabalho_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfareatrabalho_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAreaTrabalho_Ativo_Sel", StringUtil.Str( (decimal)(AV84TFAreaTrabalho_Ativo_Sel), 1, 0));
            }
            AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace", AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace);
            AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace = cgiGet( edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace", AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace);
            AV71ddo_Contratante_CNPJTitleControlIdToReplace = cgiGet( edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_Contratante_CNPJTitleControlIdToReplace", AV71ddo_Contratante_CNPJTitleControlIdToReplace);
            AV75ddo_Contratante_TelefoneTitleControlIdToReplace = cgiGet( edtavDdo_contratante_telefonetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Contratante_TelefoneTitleControlIdToReplace", AV75ddo_Contratante_TelefoneTitleControlIdToReplace);
            AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace = cgiGet( edtavDdo_areatrabalho_validaosfmtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace", AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace);
            AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace = cgiGet( edtavDdo_areatrabalho_diasparapagartitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace", AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace);
            AV136ddo_Estado_UFTitleControlIdToReplace = cgiGet( edtavDdo_estado_uftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136ddo_Estado_UFTitleControlIdToReplace", AV136ddo_Estado_UFTitleControlIdToReplace);
            AV120ddo_Municipio_NomeTitleControlIdToReplace = cgiGet( edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120ddo_Municipio_NomeTitleControlIdToReplace", AV120ddo_Municipio_NomeTitleControlIdToReplace);
            AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace = cgiGet( edtavDdo_areatrabalho_calculopfinaltitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace", AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace);
            AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace = cgiGet( edtavDdo_areatrabalho_vertatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace", AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace);
            AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_areatrabalho_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace", AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_94 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_94"), ",", "."));
            AV88GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV89GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_areatrabalho_descricao_Caption = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Caption");
            Ddo_areatrabalho_descricao_Tooltip = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Tooltip");
            Ddo_areatrabalho_descricao_Cls = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Cls");
            Ddo_areatrabalho_descricao_Filteredtext_set = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Filteredtext_set");
            Ddo_areatrabalho_descricao_Selectedvalue_set = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Selectedvalue_set");
            Ddo_areatrabalho_descricao_Dropdownoptionstype = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Dropdownoptionstype");
            Ddo_areatrabalho_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_areatrabalho_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_DESCRICAO_Includesortasc"));
            Ddo_areatrabalho_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_DESCRICAO_Includesortdsc"));
            Ddo_areatrabalho_descricao_Sortedstatus = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Sortedstatus");
            Ddo_areatrabalho_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_DESCRICAO_Includefilter"));
            Ddo_areatrabalho_descricao_Filtertype = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Filtertype");
            Ddo_areatrabalho_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_DESCRICAO_Filterisrange"));
            Ddo_areatrabalho_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_DESCRICAO_Includedatalist"));
            Ddo_areatrabalho_descricao_Datalisttype = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Datalisttype");
            Ddo_areatrabalho_descricao_Datalistproc = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Datalistproc");
            Ddo_areatrabalho_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_AREATRABALHO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_areatrabalho_descricao_Sortasc = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Sortasc");
            Ddo_areatrabalho_descricao_Sortdsc = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Sortdsc");
            Ddo_areatrabalho_descricao_Loadingdata = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Loadingdata");
            Ddo_areatrabalho_descricao_Cleanfilter = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Cleanfilter");
            Ddo_areatrabalho_descricao_Noresultsfound = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Noresultsfound");
            Ddo_areatrabalho_descricao_Searchbuttontext = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Searchbuttontext");
            Ddo_contratante_razaosocial_Caption = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Caption");
            Ddo_contratante_razaosocial_Tooltip = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Tooltip");
            Ddo_contratante_razaosocial_Cls = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Cls");
            Ddo_contratante_razaosocial_Filteredtext_set = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Filteredtext_set");
            Ddo_contratante_razaosocial_Selectedvalue_set = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Selectedvalue_set");
            Ddo_contratante_razaosocial_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Dropdownoptionstype");
            Ddo_contratante_razaosocial_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Titlecontrolidtoreplace");
            Ddo_contratante_razaosocial_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Includesortasc"));
            Ddo_contratante_razaosocial_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Includesortdsc"));
            Ddo_contratante_razaosocial_Sortedstatus = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Sortedstatus");
            Ddo_contratante_razaosocial_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Includefilter"));
            Ddo_contratante_razaosocial_Filtertype = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Filtertype");
            Ddo_contratante_razaosocial_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Filterisrange"));
            Ddo_contratante_razaosocial_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Includedatalist"));
            Ddo_contratante_razaosocial_Datalisttype = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Datalisttype");
            Ddo_contratante_razaosocial_Datalistproc = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Datalistproc");
            Ddo_contratante_razaosocial_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratante_razaosocial_Sortasc = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Sortasc");
            Ddo_contratante_razaosocial_Sortdsc = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Sortdsc");
            Ddo_contratante_razaosocial_Loadingdata = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Loadingdata");
            Ddo_contratante_razaosocial_Cleanfilter = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Cleanfilter");
            Ddo_contratante_razaosocial_Noresultsfound = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Noresultsfound");
            Ddo_contratante_razaosocial_Searchbuttontext = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Searchbuttontext");
            Ddo_contratante_cnpj_Caption = cgiGet( "DDO_CONTRATANTE_CNPJ_Caption");
            Ddo_contratante_cnpj_Tooltip = cgiGet( "DDO_CONTRATANTE_CNPJ_Tooltip");
            Ddo_contratante_cnpj_Cls = cgiGet( "DDO_CONTRATANTE_CNPJ_Cls");
            Ddo_contratante_cnpj_Filteredtext_set = cgiGet( "DDO_CONTRATANTE_CNPJ_Filteredtext_set");
            Ddo_contratante_cnpj_Selectedvalue_set = cgiGet( "DDO_CONTRATANTE_CNPJ_Selectedvalue_set");
            Ddo_contratante_cnpj_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTE_CNPJ_Dropdownoptionstype");
            Ddo_contratante_cnpj_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTE_CNPJ_Titlecontrolidtoreplace");
            Ddo_contratante_cnpj_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_CNPJ_Includesortasc"));
            Ddo_contratante_cnpj_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_CNPJ_Includesortdsc"));
            Ddo_contratante_cnpj_Sortedstatus = cgiGet( "DDO_CONTRATANTE_CNPJ_Sortedstatus");
            Ddo_contratante_cnpj_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_CNPJ_Includefilter"));
            Ddo_contratante_cnpj_Filtertype = cgiGet( "DDO_CONTRATANTE_CNPJ_Filtertype");
            Ddo_contratante_cnpj_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_CNPJ_Filterisrange"));
            Ddo_contratante_cnpj_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_CNPJ_Includedatalist"));
            Ddo_contratante_cnpj_Datalisttype = cgiGet( "DDO_CONTRATANTE_CNPJ_Datalisttype");
            Ddo_contratante_cnpj_Datalistproc = cgiGet( "DDO_CONTRATANTE_CNPJ_Datalistproc");
            Ddo_contratante_cnpj_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATANTE_CNPJ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratante_cnpj_Sortasc = cgiGet( "DDO_CONTRATANTE_CNPJ_Sortasc");
            Ddo_contratante_cnpj_Sortdsc = cgiGet( "DDO_CONTRATANTE_CNPJ_Sortdsc");
            Ddo_contratante_cnpj_Loadingdata = cgiGet( "DDO_CONTRATANTE_CNPJ_Loadingdata");
            Ddo_contratante_cnpj_Cleanfilter = cgiGet( "DDO_CONTRATANTE_CNPJ_Cleanfilter");
            Ddo_contratante_cnpj_Noresultsfound = cgiGet( "DDO_CONTRATANTE_CNPJ_Noresultsfound");
            Ddo_contratante_cnpj_Searchbuttontext = cgiGet( "DDO_CONTRATANTE_CNPJ_Searchbuttontext");
            Ddo_contratante_telefone_Caption = cgiGet( "DDO_CONTRATANTE_TELEFONE_Caption");
            Ddo_contratante_telefone_Tooltip = cgiGet( "DDO_CONTRATANTE_TELEFONE_Tooltip");
            Ddo_contratante_telefone_Cls = cgiGet( "DDO_CONTRATANTE_TELEFONE_Cls");
            Ddo_contratante_telefone_Filteredtext_set = cgiGet( "DDO_CONTRATANTE_TELEFONE_Filteredtext_set");
            Ddo_contratante_telefone_Selectedvalue_set = cgiGet( "DDO_CONTRATANTE_TELEFONE_Selectedvalue_set");
            Ddo_contratante_telefone_Dropdownoptionstype = cgiGet( "DDO_CONTRATANTE_TELEFONE_Dropdownoptionstype");
            Ddo_contratante_telefone_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATANTE_TELEFONE_Titlecontrolidtoreplace");
            Ddo_contratante_telefone_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_TELEFONE_Includesortasc"));
            Ddo_contratante_telefone_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_TELEFONE_Includesortdsc"));
            Ddo_contratante_telefone_Sortedstatus = cgiGet( "DDO_CONTRATANTE_TELEFONE_Sortedstatus");
            Ddo_contratante_telefone_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_TELEFONE_Includefilter"));
            Ddo_contratante_telefone_Filtertype = cgiGet( "DDO_CONTRATANTE_TELEFONE_Filtertype");
            Ddo_contratante_telefone_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_TELEFONE_Filterisrange"));
            Ddo_contratante_telefone_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATANTE_TELEFONE_Includedatalist"));
            Ddo_contratante_telefone_Datalisttype = cgiGet( "DDO_CONTRATANTE_TELEFONE_Datalisttype");
            Ddo_contratante_telefone_Datalistproc = cgiGet( "DDO_CONTRATANTE_TELEFONE_Datalistproc");
            Ddo_contratante_telefone_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATANTE_TELEFONE_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratante_telefone_Sortasc = cgiGet( "DDO_CONTRATANTE_TELEFONE_Sortasc");
            Ddo_contratante_telefone_Sortdsc = cgiGet( "DDO_CONTRATANTE_TELEFONE_Sortdsc");
            Ddo_contratante_telefone_Loadingdata = cgiGet( "DDO_CONTRATANTE_TELEFONE_Loadingdata");
            Ddo_contratante_telefone_Cleanfilter = cgiGet( "DDO_CONTRATANTE_TELEFONE_Cleanfilter");
            Ddo_contratante_telefone_Noresultsfound = cgiGet( "DDO_CONTRATANTE_TELEFONE_Noresultsfound");
            Ddo_contratante_telefone_Searchbuttontext = cgiGet( "DDO_CONTRATANTE_TELEFONE_Searchbuttontext");
            Ddo_areatrabalho_validaosfm_Caption = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Caption");
            Ddo_areatrabalho_validaosfm_Tooltip = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Tooltip");
            Ddo_areatrabalho_validaosfm_Cls = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Cls");
            Ddo_areatrabalho_validaosfm_Selectedvalue_set = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Selectedvalue_set");
            Ddo_areatrabalho_validaosfm_Dropdownoptionstype = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Dropdownoptionstype");
            Ddo_areatrabalho_validaosfm_Titlecontrolidtoreplace = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Titlecontrolidtoreplace");
            Ddo_areatrabalho_validaosfm_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Includesortasc"));
            Ddo_areatrabalho_validaosfm_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Includesortdsc"));
            Ddo_areatrabalho_validaosfm_Sortedstatus = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Sortedstatus");
            Ddo_areatrabalho_validaosfm_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Includefilter"));
            Ddo_areatrabalho_validaosfm_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Includedatalist"));
            Ddo_areatrabalho_validaosfm_Datalisttype = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Datalisttype");
            Ddo_areatrabalho_validaosfm_Datalistfixedvalues = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Datalistfixedvalues");
            Ddo_areatrabalho_validaosfm_Sortasc = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Sortasc");
            Ddo_areatrabalho_validaosfm_Sortdsc = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Sortdsc");
            Ddo_areatrabalho_validaosfm_Cleanfilter = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Cleanfilter");
            Ddo_areatrabalho_validaosfm_Searchbuttontext = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Searchbuttontext");
            Ddo_areatrabalho_diasparapagar_Caption = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Caption");
            Ddo_areatrabalho_diasparapagar_Tooltip = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Tooltip");
            Ddo_areatrabalho_diasparapagar_Cls = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Cls");
            Ddo_areatrabalho_diasparapagar_Filteredtext_set = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Filteredtext_set");
            Ddo_areatrabalho_diasparapagar_Filteredtextto_set = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Filteredtextto_set");
            Ddo_areatrabalho_diasparapagar_Dropdownoptionstype = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Dropdownoptionstype");
            Ddo_areatrabalho_diasparapagar_Titlecontrolidtoreplace = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Titlecontrolidtoreplace");
            Ddo_areatrabalho_diasparapagar_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Includesortasc"));
            Ddo_areatrabalho_diasparapagar_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Includesortdsc"));
            Ddo_areatrabalho_diasparapagar_Sortedstatus = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Sortedstatus");
            Ddo_areatrabalho_diasparapagar_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Includefilter"));
            Ddo_areatrabalho_diasparapagar_Filtertype = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Filtertype");
            Ddo_areatrabalho_diasparapagar_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Filterisrange"));
            Ddo_areatrabalho_diasparapagar_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Includedatalist"));
            Ddo_areatrabalho_diasparapagar_Sortasc = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Sortasc");
            Ddo_areatrabalho_diasparapagar_Sortdsc = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Sortdsc");
            Ddo_areatrabalho_diasparapagar_Cleanfilter = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Cleanfilter");
            Ddo_areatrabalho_diasparapagar_Rangefilterfrom = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Rangefilterfrom");
            Ddo_areatrabalho_diasparapagar_Rangefilterto = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Rangefilterto");
            Ddo_areatrabalho_diasparapagar_Searchbuttontext = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Searchbuttontext");
            Ddo_estado_uf_Caption = cgiGet( "DDO_ESTADO_UF_Caption");
            Ddo_estado_uf_Tooltip = cgiGet( "DDO_ESTADO_UF_Tooltip");
            Ddo_estado_uf_Cls = cgiGet( "DDO_ESTADO_UF_Cls");
            Ddo_estado_uf_Filteredtext_set = cgiGet( "DDO_ESTADO_UF_Filteredtext_set");
            Ddo_estado_uf_Selectedvalue_set = cgiGet( "DDO_ESTADO_UF_Selectedvalue_set");
            Ddo_estado_uf_Dropdownoptionstype = cgiGet( "DDO_ESTADO_UF_Dropdownoptionstype");
            Ddo_estado_uf_Titlecontrolidtoreplace = cgiGet( "DDO_ESTADO_UF_Titlecontrolidtoreplace");
            Ddo_estado_uf_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includesortasc"));
            Ddo_estado_uf_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includesortdsc"));
            Ddo_estado_uf_Sortedstatus = cgiGet( "DDO_ESTADO_UF_Sortedstatus");
            Ddo_estado_uf_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includefilter"));
            Ddo_estado_uf_Filtertype = cgiGet( "DDO_ESTADO_UF_Filtertype");
            Ddo_estado_uf_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Filterisrange"));
            Ddo_estado_uf_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_ESTADO_UF_Includedatalist"));
            Ddo_estado_uf_Datalisttype = cgiGet( "DDO_ESTADO_UF_Datalisttype");
            Ddo_estado_uf_Datalistproc = cgiGet( "DDO_ESTADO_UF_Datalistproc");
            Ddo_estado_uf_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_ESTADO_UF_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_estado_uf_Sortasc = cgiGet( "DDO_ESTADO_UF_Sortasc");
            Ddo_estado_uf_Sortdsc = cgiGet( "DDO_ESTADO_UF_Sortdsc");
            Ddo_estado_uf_Loadingdata = cgiGet( "DDO_ESTADO_UF_Loadingdata");
            Ddo_estado_uf_Cleanfilter = cgiGet( "DDO_ESTADO_UF_Cleanfilter");
            Ddo_estado_uf_Noresultsfound = cgiGet( "DDO_ESTADO_UF_Noresultsfound");
            Ddo_estado_uf_Searchbuttontext = cgiGet( "DDO_ESTADO_UF_Searchbuttontext");
            Ddo_municipio_nome_Caption = cgiGet( "DDO_MUNICIPIO_NOME_Caption");
            Ddo_municipio_nome_Tooltip = cgiGet( "DDO_MUNICIPIO_NOME_Tooltip");
            Ddo_municipio_nome_Cls = cgiGet( "DDO_MUNICIPIO_NOME_Cls");
            Ddo_municipio_nome_Filteredtext_set = cgiGet( "DDO_MUNICIPIO_NOME_Filteredtext_set");
            Ddo_municipio_nome_Selectedvalue_set = cgiGet( "DDO_MUNICIPIO_NOME_Selectedvalue_set");
            Ddo_municipio_nome_Dropdownoptionstype = cgiGet( "DDO_MUNICIPIO_NOME_Dropdownoptionstype");
            Ddo_municipio_nome_Titlecontrolidtoreplace = cgiGet( "DDO_MUNICIPIO_NOME_Titlecontrolidtoreplace");
            Ddo_municipio_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includesortasc"));
            Ddo_municipio_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includesortdsc"));
            Ddo_municipio_nome_Sortedstatus = cgiGet( "DDO_MUNICIPIO_NOME_Sortedstatus");
            Ddo_municipio_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includefilter"));
            Ddo_municipio_nome_Filtertype = cgiGet( "DDO_MUNICIPIO_NOME_Filtertype");
            Ddo_municipio_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Filterisrange"));
            Ddo_municipio_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_MUNICIPIO_NOME_Includedatalist"));
            Ddo_municipio_nome_Datalisttype = cgiGet( "DDO_MUNICIPIO_NOME_Datalisttype");
            Ddo_municipio_nome_Datalistproc = cgiGet( "DDO_MUNICIPIO_NOME_Datalistproc");
            Ddo_municipio_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_MUNICIPIO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_municipio_nome_Sortasc = cgiGet( "DDO_MUNICIPIO_NOME_Sortasc");
            Ddo_municipio_nome_Sortdsc = cgiGet( "DDO_MUNICIPIO_NOME_Sortdsc");
            Ddo_municipio_nome_Loadingdata = cgiGet( "DDO_MUNICIPIO_NOME_Loadingdata");
            Ddo_municipio_nome_Cleanfilter = cgiGet( "DDO_MUNICIPIO_NOME_Cleanfilter");
            Ddo_municipio_nome_Noresultsfound = cgiGet( "DDO_MUNICIPIO_NOME_Noresultsfound");
            Ddo_municipio_nome_Searchbuttontext = cgiGet( "DDO_MUNICIPIO_NOME_Searchbuttontext");
            Ddo_areatrabalho_calculopfinal_Caption = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Caption");
            Ddo_areatrabalho_calculopfinal_Tooltip = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Tooltip");
            Ddo_areatrabalho_calculopfinal_Cls = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Cls");
            Ddo_areatrabalho_calculopfinal_Selectedvalue_set = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Selectedvalue_set");
            Ddo_areatrabalho_calculopfinal_Dropdownoptionstype = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Dropdownoptionstype");
            Ddo_areatrabalho_calculopfinal_Titlecontrolidtoreplace = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Titlecontrolidtoreplace");
            Ddo_areatrabalho_calculopfinal_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Includesortasc"));
            Ddo_areatrabalho_calculopfinal_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Includesortdsc"));
            Ddo_areatrabalho_calculopfinal_Sortedstatus = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Sortedstatus");
            Ddo_areatrabalho_calculopfinal_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Includefilter"));
            Ddo_areatrabalho_calculopfinal_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Includedatalist"));
            Ddo_areatrabalho_calculopfinal_Datalisttype = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Datalisttype");
            Ddo_areatrabalho_calculopfinal_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Allowmultipleselection"));
            Ddo_areatrabalho_calculopfinal_Datalistfixedvalues = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Datalistfixedvalues");
            Ddo_areatrabalho_calculopfinal_Sortasc = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Sortasc");
            Ddo_areatrabalho_calculopfinal_Sortdsc = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Sortdsc");
            Ddo_areatrabalho_calculopfinal_Cleanfilter = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Cleanfilter");
            Ddo_areatrabalho_calculopfinal_Searchbuttontext = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Searchbuttontext");
            Ddo_areatrabalho_verta_Caption = cgiGet( "DDO_AREATRABALHO_VERTA_Caption");
            Ddo_areatrabalho_verta_Tooltip = cgiGet( "DDO_AREATRABALHO_VERTA_Tooltip");
            Ddo_areatrabalho_verta_Cls = cgiGet( "DDO_AREATRABALHO_VERTA_Cls");
            Ddo_areatrabalho_verta_Selectedvalue_set = cgiGet( "DDO_AREATRABALHO_VERTA_Selectedvalue_set");
            Ddo_areatrabalho_verta_Dropdownoptionstype = cgiGet( "DDO_AREATRABALHO_VERTA_Dropdownoptionstype");
            Ddo_areatrabalho_verta_Titlecontrolidtoreplace = cgiGet( "DDO_AREATRABALHO_VERTA_Titlecontrolidtoreplace");
            Ddo_areatrabalho_verta_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_VERTA_Includesortasc"));
            Ddo_areatrabalho_verta_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_VERTA_Includesortdsc"));
            Ddo_areatrabalho_verta_Sortedstatus = cgiGet( "DDO_AREATRABALHO_VERTA_Sortedstatus");
            Ddo_areatrabalho_verta_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_VERTA_Includefilter"));
            Ddo_areatrabalho_verta_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_VERTA_Includedatalist"));
            Ddo_areatrabalho_verta_Datalisttype = cgiGet( "DDO_AREATRABALHO_VERTA_Datalisttype");
            Ddo_areatrabalho_verta_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_VERTA_Allowmultipleselection"));
            Ddo_areatrabalho_verta_Datalistfixedvalues = cgiGet( "DDO_AREATRABALHO_VERTA_Datalistfixedvalues");
            Ddo_areatrabalho_verta_Sortasc = cgiGet( "DDO_AREATRABALHO_VERTA_Sortasc");
            Ddo_areatrabalho_verta_Sortdsc = cgiGet( "DDO_AREATRABALHO_VERTA_Sortdsc");
            Ddo_areatrabalho_verta_Cleanfilter = cgiGet( "DDO_AREATRABALHO_VERTA_Cleanfilter");
            Ddo_areatrabalho_verta_Searchbuttontext = cgiGet( "DDO_AREATRABALHO_VERTA_Searchbuttontext");
            Ddo_areatrabalho_ativo_Caption = cgiGet( "DDO_AREATRABALHO_ATIVO_Caption");
            Ddo_areatrabalho_ativo_Tooltip = cgiGet( "DDO_AREATRABALHO_ATIVO_Tooltip");
            Ddo_areatrabalho_ativo_Cls = cgiGet( "DDO_AREATRABALHO_ATIVO_Cls");
            Ddo_areatrabalho_ativo_Selectedvalue_set = cgiGet( "DDO_AREATRABALHO_ATIVO_Selectedvalue_set");
            Ddo_areatrabalho_ativo_Dropdownoptionstype = cgiGet( "DDO_AREATRABALHO_ATIVO_Dropdownoptionstype");
            Ddo_areatrabalho_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_AREATRABALHO_ATIVO_Titlecontrolidtoreplace");
            Ddo_areatrabalho_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_ATIVO_Includesortasc"));
            Ddo_areatrabalho_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_ATIVO_Includesortdsc"));
            Ddo_areatrabalho_ativo_Sortedstatus = cgiGet( "DDO_AREATRABALHO_ATIVO_Sortedstatus");
            Ddo_areatrabalho_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_ATIVO_Includefilter"));
            Ddo_areatrabalho_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_AREATRABALHO_ATIVO_Includedatalist"));
            Ddo_areatrabalho_ativo_Datalisttype = cgiGet( "DDO_AREATRABALHO_ATIVO_Datalisttype");
            Ddo_areatrabalho_ativo_Datalistfixedvalues = cgiGet( "DDO_AREATRABALHO_ATIVO_Datalistfixedvalues");
            Ddo_areatrabalho_ativo_Sortasc = cgiGet( "DDO_AREATRABALHO_ATIVO_Sortasc");
            Ddo_areatrabalho_ativo_Sortdsc = cgiGet( "DDO_AREATRABALHO_ATIVO_Sortdsc");
            Ddo_areatrabalho_ativo_Cleanfilter = cgiGet( "DDO_AREATRABALHO_ATIVO_Cleanfilter");
            Ddo_areatrabalho_ativo_Searchbuttontext = cgiGet( "DDO_AREATRABALHO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_areatrabalho_descricao_Activeeventkey = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Activeeventkey");
            Ddo_areatrabalho_descricao_Filteredtext_get = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Filteredtext_get");
            Ddo_areatrabalho_descricao_Selectedvalue_get = cgiGet( "DDO_AREATRABALHO_DESCRICAO_Selectedvalue_get");
            Ddo_contratante_razaosocial_Activeeventkey = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Activeeventkey");
            Ddo_contratante_razaosocial_Filteredtext_get = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Filteredtext_get");
            Ddo_contratante_razaosocial_Selectedvalue_get = cgiGet( "DDO_CONTRATANTE_RAZAOSOCIAL_Selectedvalue_get");
            Ddo_contratante_cnpj_Activeeventkey = cgiGet( "DDO_CONTRATANTE_CNPJ_Activeeventkey");
            Ddo_contratante_cnpj_Filteredtext_get = cgiGet( "DDO_CONTRATANTE_CNPJ_Filteredtext_get");
            Ddo_contratante_cnpj_Selectedvalue_get = cgiGet( "DDO_CONTRATANTE_CNPJ_Selectedvalue_get");
            Ddo_contratante_telefone_Activeeventkey = cgiGet( "DDO_CONTRATANTE_TELEFONE_Activeeventkey");
            Ddo_contratante_telefone_Filteredtext_get = cgiGet( "DDO_CONTRATANTE_TELEFONE_Filteredtext_get");
            Ddo_contratante_telefone_Selectedvalue_get = cgiGet( "DDO_CONTRATANTE_TELEFONE_Selectedvalue_get");
            Ddo_areatrabalho_validaosfm_Activeeventkey = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Activeeventkey");
            Ddo_areatrabalho_validaosfm_Selectedvalue_get = cgiGet( "DDO_AREATRABALHO_VALIDAOSFM_Selectedvalue_get");
            Ddo_areatrabalho_diasparapagar_Activeeventkey = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Activeeventkey");
            Ddo_areatrabalho_diasparapagar_Filteredtext_get = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Filteredtext_get");
            Ddo_areatrabalho_diasparapagar_Filteredtextto_get = cgiGet( "DDO_AREATRABALHO_DIASPARAPAGAR_Filteredtextto_get");
            Ddo_estado_uf_Activeeventkey = cgiGet( "DDO_ESTADO_UF_Activeeventkey");
            Ddo_estado_uf_Filteredtext_get = cgiGet( "DDO_ESTADO_UF_Filteredtext_get");
            Ddo_estado_uf_Selectedvalue_get = cgiGet( "DDO_ESTADO_UF_Selectedvalue_get");
            Ddo_municipio_nome_Activeeventkey = cgiGet( "DDO_MUNICIPIO_NOME_Activeeventkey");
            Ddo_municipio_nome_Filteredtext_get = cgiGet( "DDO_MUNICIPIO_NOME_Filteredtext_get");
            Ddo_municipio_nome_Selectedvalue_get = cgiGet( "DDO_MUNICIPIO_NOME_Selectedvalue_get");
            Ddo_areatrabalho_calculopfinal_Activeeventkey = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Activeeventkey");
            Ddo_areatrabalho_calculopfinal_Selectedvalue_get = cgiGet( "DDO_AREATRABALHO_CALCULOPFINAL_Selectedvalue_get");
            Ddo_areatrabalho_verta_Activeeventkey = cgiGet( "DDO_AREATRABALHO_VERTA_Activeeventkey");
            Ddo_areatrabalho_verta_Selectedvalue_get = cgiGet( "DDO_AREATRABALHO_VERTA_Selectedvalue_get");
            Ddo_areatrabalho_ativo_Activeeventkey = cgiGet( "DDO_AREATRABALHO_ATIVO_Activeeventkey");
            Ddo_areatrabalho_ativo_Selectedvalue_get = cgiGet( "DDO_AREATRABALHO_ATIVO_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_DESCRICAO1"), AV17AreaTrabalho_Descricao1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vORGANIZACAO_NOME1"), AV90Organizacao_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_ATIVO1"), AV129AreaTrabalho_Ativo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_DESCRICAO2"), AV21AreaTrabalho_Descricao2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vORGANIZACAO_NOME2"), AV92Organizacao_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_ATIVO2"), AV130AreaTrabalho_Ativo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_DESCRICAO3"), AV25AreaTrabalho_Descricao3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vORGANIZACAO_NOME3"), AV131Organizacao_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vAREATRABALHO_ATIVO3"), AV132AreaTrabalho_Ativo3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAREATRABALHO_DESCRICAO"), AV61TFAreaTrabalho_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFAREATRABALHO_DESCRICAO_SEL"), AV62TFAreaTrabalho_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAZAOSOCIAL"), AV65TFContratante_RazaoSocial) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_RAZAOSOCIAL_SEL"), AV66TFContratante_RazaoSocial_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_CNPJ"), AV69TFContratante_CNPJ) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_CNPJ_SEL"), AV70TFContratante_CNPJ_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_TELEFONE"), AV73TFContratante_Telefone) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFCONTRATANTE_TELEFONE_SEL"), AV74TFContratante_Telefone_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAREATRABALHO_VALIDAOSFM_SEL"), ",", ".") != Convert.ToDecimal( AV77TFAreaTrabalho_ValidaOSFM_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAREATRABALHO_DIASPARAPAGAR"), ",", ".") != Convert.ToDecimal( AV80TFAreaTrabalho_DiasParaPagar )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAREATRABALHO_DIASPARAPAGAR_TO"), ",", ".") != Convert.ToDecimal( AV81TFAreaTrabalho_DiasParaPagar_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF"), AV134TFEstado_UF) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFESTADO_UF_SEL"), AV135TFEstado_UF_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME"), AV118TFMunicipio_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFMUNICIPIO_NOME_SEL"), AV119TFMunicipio_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFAREATRABALHO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV84TFAreaTrabalho_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E340T2 */
         E340T2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E340T2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "AREATRABALHO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "AREATRABALHO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "AREATRABALHO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfareatrabalho_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfareatrabalho_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfareatrabalho_descricao_Visible), 5, 0)));
         edtavTfareatrabalho_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfareatrabalho_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfareatrabalho_descricao_sel_Visible), 5, 0)));
         edtavTfcontratante_razaosocial_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_razaosocial_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_razaosocial_Visible), 5, 0)));
         edtavTfcontratante_razaosocial_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_razaosocial_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_razaosocial_sel_Visible), 5, 0)));
         edtavTfcontratante_cnpj_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_cnpj_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_cnpj_Visible), 5, 0)));
         edtavTfcontratante_cnpj_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_cnpj_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_cnpj_sel_Visible), 5, 0)));
         edtavTfcontratante_telefone_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_telefone_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_telefone_Visible), 5, 0)));
         edtavTfcontratante_telefone_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratante_telefone_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratante_telefone_sel_Visible), 5, 0)));
         edtavTfareatrabalho_validaosfm_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfareatrabalho_validaosfm_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfareatrabalho_validaosfm_sel_Visible), 5, 0)));
         edtavTfareatrabalho_diasparapagar_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfareatrabalho_diasparapagar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfareatrabalho_diasparapagar_Visible), 5, 0)));
         edtavTfareatrabalho_diasparapagar_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfareatrabalho_diasparapagar_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfareatrabalho_diasparapagar_to_Visible), 5, 0)));
         edtavTfestado_uf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfestado_uf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_uf_Visible), 5, 0)));
         edtavTfestado_uf_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfestado_uf_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_uf_sel_Visible), 5, 0)));
         edtavTfmunicipio_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmunicipio_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmunicipio_nome_Visible), 5, 0)));
         edtavTfmunicipio_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfmunicipio_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfmunicipio_nome_sel_Visible), 5, 0)));
         edtavTfareatrabalho_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfareatrabalho_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfareatrabalho_ativo_sel_Visible), 5, 0)));
         Ddo_areatrabalho_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_AreaTrabalho_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_descricao_Internalname, "TitleControlIdToReplace", Ddo_areatrabalho_descricao_Titlecontrolidtoreplace);
         AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace = Ddo_areatrabalho_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace", AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace);
         edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratante_razaosocial_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratante_RazaoSocial";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "TitleControlIdToReplace", Ddo_contratante_razaosocial_Titlecontrolidtoreplace);
         AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace = Ddo_contratante_razaosocial_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace", AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace);
         edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratante_cnpj_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratante_CNPJ";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "TitleControlIdToReplace", Ddo_contratante_cnpj_Titlecontrolidtoreplace);
         AV71ddo_Contratante_CNPJTitleControlIdToReplace = Ddo_contratante_cnpj_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71ddo_Contratante_CNPJTitleControlIdToReplace", AV71ddo_Contratante_CNPJTitleControlIdToReplace);
         edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratante_telefone_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratante_Telefone";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "TitleControlIdToReplace", Ddo_contratante_telefone_Titlecontrolidtoreplace);
         AV75ddo_Contratante_TelefoneTitleControlIdToReplace = Ddo_contratante_telefone_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_Contratante_TelefoneTitleControlIdToReplace", AV75ddo_Contratante_TelefoneTitleControlIdToReplace);
         edtavDdo_contratante_telefonetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratante_telefonetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratante_telefonetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_areatrabalho_validaosfm_Titlecontrolidtoreplace = subGrid_Internalname+"_AreaTrabalho_ValidaOSFM";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_validaosfm_Internalname, "TitleControlIdToReplace", Ddo_areatrabalho_validaosfm_Titlecontrolidtoreplace);
         AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace = Ddo_areatrabalho_validaosfm_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace", AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace);
         edtavDdo_areatrabalho_validaosfmtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_areatrabalho_validaosfmtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_areatrabalho_validaosfmtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_areatrabalho_diasparapagar_Titlecontrolidtoreplace = subGrid_Internalname+"_AreaTrabalho_DiasParaPagar";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_diasparapagar_Internalname, "TitleControlIdToReplace", Ddo_areatrabalho_diasparapagar_Titlecontrolidtoreplace);
         AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace = Ddo_areatrabalho_diasparapagar_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace", AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace);
         edtavDdo_areatrabalho_diasparapagartitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_areatrabalho_diasparapagartitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_areatrabalho_diasparapagartitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_estado_uf_Titlecontrolidtoreplace = subGrid_Internalname+"_Estado_UF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "TitleControlIdToReplace", Ddo_estado_uf_Titlecontrolidtoreplace);
         AV136ddo_Estado_UFTitleControlIdToReplace = Ddo_estado_uf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV136ddo_Estado_UFTitleControlIdToReplace", AV136ddo_Estado_UFTitleControlIdToReplace);
         edtavDdo_estado_uftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_estado_uftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_estado_uftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_municipio_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Municipio_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "TitleControlIdToReplace", Ddo_municipio_nome_Titlecontrolidtoreplace);
         AV120ddo_Municipio_NomeTitleControlIdToReplace = Ddo_municipio_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120ddo_Municipio_NomeTitleControlIdToReplace", AV120ddo_Municipio_NomeTitleControlIdToReplace);
         edtavDdo_municipio_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_municipio_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_areatrabalho_calculopfinal_Titlecontrolidtoreplace = subGrid_Internalname+"_AreaTrabalho_CalculoPFinal";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_calculopfinal_Internalname, "TitleControlIdToReplace", Ddo_areatrabalho_calculopfinal_Titlecontrolidtoreplace);
         AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace = Ddo_areatrabalho_calculopfinal_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace", AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace);
         edtavDdo_areatrabalho_calculopfinaltitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_areatrabalho_calculopfinaltitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_areatrabalho_calculopfinaltitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_areatrabalho_verta_Titlecontrolidtoreplace = subGrid_Internalname+"_AreaTrabalho_VerTA";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_verta_Internalname, "TitleControlIdToReplace", Ddo_areatrabalho_verta_Titlecontrolidtoreplace);
         AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace = Ddo_areatrabalho_verta_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace", AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace);
         edtavDdo_areatrabalho_vertatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_areatrabalho_vertatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_areatrabalho_vertatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_areatrabalho_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_AreaTrabalho_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_ativo_Internalname, "TitleControlIdToReplace", Ddo_areatrabalho_ativo_Titlecontrolidtoreplace);
         AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace = Ddo_areatrabalho_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace", AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace);
         edtavDdo_areatrabalho_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_areatrabalho_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_areatrabalho_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " �rea de Trabalho";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Descri��o", 0);
         cmbavOrderedby.addItem("2", "Contratante", 0);
         cmbavOrderedby.addItem("3", "CNPJ", 0);
         cmbavOrderedby.addItem("4", "Telefone", 0);
         cmbavOrderedby.addItem("5", "Valida OS FM", 0);
         cmbavOrderedby.addItem("6", "Dias para pagar", 0);
         cmbavOrderedby.addItem("7", "Estado", 0);
         cmbavOrderedby.addItem("8", "Munic�pio", 0);
         cmbavOrderedby.addItem("9", "Calculo P. Final", 0);
         cmbavOrderedby.addItem("10", "Vers�o do TA", 0);
         cmbavOrderedby.addItem("11", "Ativo?", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV86DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV86DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E350T2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV60AreaTrabalho_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64Contratante_RazaoSocialTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68Contratante_CNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72Contratante_TelefoneTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76AreaTrabalho_ValidaOSFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79AreaTrabalho_DiasParaPagarTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV133Estado_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV117Municipio_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV121AreaTrabalho_CalculoPFinalTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV142AreaTrabalho_VerTATitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV83AreaTrabalho_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtAreaTrabalho_Descricao_Titleformat = 2;
         edtAreaTrabalho_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_Descricao_Internalname, "Title", edtAreaTrabalho_Descricao_Title);
         edtContratante_RazaoSocial_Titleformat = 2;
         edtContratante_RazaoSocial_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contratante", AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_RazaoSocial_Internalname, "Title", edtContratante_RazaoSocial_Title);
         edtContratante_CNPJ_Titleformat = 2;
         edtContratante_CNPJ_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "CNPJ", AV71ddo_Contratante_CNPJTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_CNPJ_Internalname, "Title", edtContratante_CNPJ_Title);
         edtContratante_Telefone_Titleformat = 2;
         edtContratante_Telefone_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Telefone", AV75ddo_Contratante_TelefoneTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratante_Telefone_Internalname, "Title", edtContratante_Telefone_Title);
         cmbAreaTrabalho_ValidaOSFM_Titleformat = 2;
         cmbAreaTrabalho_ValidaOSFM.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valida OS FM", AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_ValidaOSFM_Internalname, "Title", cmbAreaTrabalho_ValidaOSFM.Title.Text);
         edtAreaTrabalho_DiasParaPagar_Titleformat = 2;
         edtAreaTrabalho_DiasParaPagar_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Dias para pagar", AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAreaTrabalho_DiasParaPagar_Internalname, "Title", edtAreaTrabalho_DiasParaPagar_Title);
         edtEstado_UF_Titleformat = 2;
         edtEstado_UF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Estado", AV136ddo_Estado_UFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEstado_UF_Internalname, "Title", edtEstado_UF_Title);
         edtMunicipio_Nome_Titleformat = 2;
         edtMunicipio_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Munic�pio", AV120ddo_Municipio_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtMunicipio_Nome_Internalname, "Title", edtMunicipio_Nome_Title);
         cmbAreaTrabalho_CalculoPFinal_Titleformat = 2;
         cmbAreaTrabalho_CalculoPFinal.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Calculo P. Final", AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_CalculoPFinal_Internalname, "Title", cmbAreaTrabalho_CalculoPFinal.Title.Text);
         cmbAreaTrabalho_VerTA_Titleformat = 2;
         cmbAreaTrabalho_VerTA.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Vers�o do TA", AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_VerTA_Internalname, "Title", cmbAreaTrabalho_VerTA.Title.Text);
         chkAreaTrabalho_Ativo_Titleformat = 2;
         chkAreaTrabalho_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo?", AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAreaTrabalho_Ativo_Internalname, "Title", chkAreaTrabalho_Ativo.Title.Text);
         AV88GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV88GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV88GridCurrentPage), 10, 0)));
         AV89GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV89GridPageCount), 10, 0)));
         AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 = AV17AreaTrabalho_Descricao1;
         AV152WWAreaTrabalhoDS_5_Organizacao_nome1 = AV90Organizacao_Nome1;
         AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 = AV129AreaTrabalho_Ativo1;
         AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 = AV21AreaTrabalho_Descricao2;
         AV158WWAreaTrabalhoDS_11_Organizacao_nome2 = AV92Organizacao_Nome2;
         AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 = AV130AreaTrabalho_Ativo2;
         AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 = AV25AreaTrabalho_Descricao3;
         AV164WWAreaTrabalhoDS_17_Organizacao_nome3 = AV131Organizacao_Nome3;
         AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 = AV132AreaTrabalho_Ativo3;
         AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao = AV61TFAreaTrabalho_Descricao;
         AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel = AV62TFAreaTrabalho_Descricao_Sel;
         AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial = AV65TFContratante_RazaoSocial;
         AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel = AV66TFContratante_RazaoSocial_Sel;
         AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj = AV69TFContratante_CNPJ;
         AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel = AV70TFContratante_CNPJ_Sel;
         AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone = AV73TFContratante_Telefone;
         AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel = AV74TFContratante_Telefone_Sel;
         AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel = AV77TFAreaTrabalho_ValidaOSFM_Sel;
         AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar = AV80TFAreaTrabalho_DiasParaPagar;
         AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to = AV81TFAreaTrabalho_DiasParaPagar_To;
         AV177WWAreaTrabalhoDS_30_Tfestado_uf = AV134TFEstado_UF;
         AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel = AV135TFEstado_UF_Sel;
         AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome = AV118TFMunicipio_Nome;
         AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel = AV119TFMunicipio_Nome_Sel;
         AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels = AV123TFAreaTrabalho_CalculoPFinal_Sels;
         AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels = AV144TFAreaTrabalho_VerTA_Sels;
         AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel = AV84TFAreaTrabalho_Ativo_Sel;
         AV148Udparg1 = new prc_areasdousuario(context).executeUdp(  AV6WWPContext.gxTpr_Userid);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV60AreaTrabalho_DescricaoTitleFilterData", AV60AreaTrabalho_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV64Contratante_RazaoSocialTitleFilterData", AV64Contratante_RazaoSocialTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68Contratante_CNPJTitleFilterData", AV68Contratante_CNPJTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV72Contratante_TelefoneTitleFilterData", AV72Contratante_TelefoneTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV76AreaTrabalho_ValidaOSFMTitleFilterData", AV76AreaTrabalho_ValidaOSFMTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79AreaTrabalho_DiasParaPagarTitleFilterData", AV79AreaTrabalho_DiasParaPagarTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV133Estado_UFTitleFilterData", AV133Estado_UFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV117Municipio_NomeTitleFilterData", AV117Municipio_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV121AreaTrabalho_CalculoPFinalTitleFilterData", AV121AreaTrabalho_CalculoPFinalTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV142AreaTrabalho_VerTATitleFilterData", AV142AreaTrabalho_VerTATitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV83AreaTrabalho_AtivoTitleFilterData", AV83AreaTrabalho_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E110T2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV87PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV87PageToGo) ;
         }
      }

      protected void E120T2( )
      {
         /* Ddo_areatrabalho_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_areatrabalho_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_descricao_Internalname, "SortedStatus", Ddo_areatrabalho_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_descricao_Internalname, "SortedStatus", Ddo_areatrabalho_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV61TFAreaTrabalho_Descricao = Ddo_areatrabalho_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFAreaTrabalho_Descricao", AV61TFAreaTrabalho_Descricao);
            AV62TFAreaTrabalho_Descricao_Sel = Ddo_areatrabalho_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFAreaTrabalho_Descricao_Sel", AV62TFAreaTrabalho_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E130T2( )
      {
         /* Ddo_contratante_razaosocial_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratante_razaosocial_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_razaosocial_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SortedStatus", Ddo_contratante_razaosocial_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_razaosocial_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_razaosocial_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SortedStatus", Ddo_contratante_razaosocial_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_razaosocial_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV65TFContratante_RazaoSocial = Ddo_contratante_razaosocial_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratante_RazaoSocial", AV65TFContratante_RazaoSocial);
            AV66TFContratante_RazaoSocial_Sel = Ddo_contratante_razaosocial_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratante_RazaoSocial_Sel", AV66TFContratante_RazaoSocial_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E140T2( )
      {
         /* Ddo_contratante_cnpj_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratante_cnpj_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_cnpj_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SortedStatus", Ddo_contratante_cnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_cnpj_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_cnpj_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SortedStatus", Ddo_contratante_cnpj_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_cnpj_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV69TFContratante_CNPJ = Ddo_contratante_cnpj_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratante_CNPJ", AV69TFContratante_CNPJ);
            AV70TFContratante_CNPJ_Sel = Ddo_contratante_cnpj_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratante_CNPJ_Sel", AV70TFContratante_CNPJ_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E150T2( )
      {
         /* Ddo_contratante_telefone_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratante_telefone_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_telefone_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SortedStatus", Ddo_contratante_telefone_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_telefone_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratante_telefone_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SortedStatus", Ddo_contratante_telefone_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratante_telefone_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV73TFContratante_Telefone = Ddo_contratante_telefone_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratante_Telefone", AV73TFContratante_Telefone);
            AV74TFContratante_Telefone_Sel = Ddo_contratante_telefone_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratante_Telefone_Sel", AV74TFContratante_Telefone_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E160T2( )
      {
         /* Ddo_areatrabalho_validaosfm_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_areatrabalho_validaosfm_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_validaosfm_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_validaosfm_Internalname, "SortedStatus", Ddo_areatrabalho_validaosfm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_validaosfm_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_validaosfm_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_validaosfm_Internalname, "SortedStatus", Ddo_areatrabalho_validaosfm_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_validaosfm_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV77TFAreaTrabalho_ValidaOSFM_Sel = (short)(NumberUtil.Val( Ddo_areatrabalho_validaosfm_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAreaTrabalho_ValidaOSFM_Sel", StringUtil.Str( (decimal)(AV77TFAreaTrabalho_ValidaOSFM_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E170T2( )
      {
         /* Ddo_areatrabalho_diasparapagar_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_areatrabalho_diasparapagar_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_diasparapagar_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_diasparapagar_Internalname, "SortedStatus", Ddo_areatrabalho_diasparapagar_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_diasparapagar_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_diasparapagar_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_diasparapagar_Internalname, "SortedStatus", Ddo_areatrabalho_diasparapagar_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_diasparapagar_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV80TFAreaTrabalho_DiasParaPagar = (short)(NumberUtil.Val( Ddo_areatrabalho_diasparapagar_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFAreaTrabalho_DiasParaPagar), 4, 0)));
            AV81TFAreaTrabalho_DiasParaPagar_To = (short)(NumberUtil.Val( Ddo_areatrabalho_diasparapagar_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFAreaTrabalho_DiasParaPagar_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFAreaTrabalho_DiasParaPagar_To), 4, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E180T2( )
      {
         /* Ddo_estado_uf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_uf_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_estado_uf_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV134TFEstado_UF = Ddo_estado_uf_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134TFEstado_UF", AV134TFEstado_UF);
            AV135TFEstado_UF_Sel = Ddo_estado_uf_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135TFEstado_UF_Sel", AV135TFEstado_UF_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E190T2( )
      {
         /* Ddo_municipio_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_municipio_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_municipio_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_municipio_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV118TFMunicipio_Nome = Ddo_municipio_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118TFMunicipio_Nome", AV118TFMunicipio_Nome);
            AV119TFMunicipio_Nome_Sel = Ddo_municipio_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119TFMunicipio_Nome_Sel", AV119TFMunicipio_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E200T2( )
      {
         /* Ddo_areatrabalho_calculopfinal_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_areatrabalho_calculopfinal_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_calculopfinal_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_calculopfinal_Internalname, "SortedStatus", Ddo_areatrabalho_calculopfinal_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_calculopfinal_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_calculopfinal_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_calculopfinal_Internalname, "SortedStatus", Ddo_areatrabalho_calculopfinal_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_calculopfinal_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV122TFAreaTrabalho_CalculoPFinal_SelsJson = Ddo_areatrabalho_calculopfinal_Selectedvalue_get;
            AV123TFAreaTrabalho_CalculoPFinal_Sels.FromJSonString(AV122TFAreaTrabalho_CalculoPFinal_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV123TFAreaTrabalho_CalculoPFinal_Sels", AV123TFAreaTrabalho_CalculoPFinal_Sels);
      }

      protected void E210T2( )
      {
         /* Ddo_areatrabalho_verta_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_areatrabalho_verta_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_verta_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_verta_Internalname, "SortedStatus", Ddo_areatrabalho_verta_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_verta_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_verta_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_verta_Internalname, "SortedStatus", Ddo_areatrabalho_verta_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_verta_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV143TFAreaTrabalho_VerTA_SelsJson = Ddo_areatrabalho_verta_Selectedvalue_get;
            AV144TFAreaTrabalho_VerTA_Sels.FromJSonString(StringUtil.StringReplace( AV143TFAreaTrabalho_VerTA_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV144TFAreaTrabalho_VerTA_Sels", AV144TFAreaTrabalho_VerTA_Sels);
      }

      protected void E220T2( )
      {
         /* Ddo_areatrabalho_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_areatrabalho_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_ativo_Internalname, "SortedStatus", Ddo_areatrabalho_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_areatrabalho_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_ativo_Internalname, "SortedStatus", Ddo_areatrabalho_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_areatrabalho_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV84TFAreaTrabalho_Ativo_Sel = (short)(NumberUtil.Val( Ddo_areatrabalho_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAreaTrabalho_Ativo_Sel", StringUtil.Str( (decimal)(AV84TFAreaTrabalho_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E360T2( )
      {
         /* Grid_Load Routine */
         AV29Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV29Update);
         AV184Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("areatrabalho.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A5AreaTrabalho_Codigo);
         AV28Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV28Delete);
         AV185Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("areatrabalho.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A5AreaTrabalho_Codigo);
         AV94Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV94Display);
         AV186Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewareatrabalho.aspx") + "?" + UrlEncode("" +A5AreaTrabalho_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         AV141Selecionar = context.GetImagePath( "7bc9610c-623c-457f-85be-06b4b73e31a2", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelecionar_Internalname, AV141Selecionar);
         AV187Selecionar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7bc9610c-623c-457f-85be-06b4b73e31a2", "", context.GetTheme( )));
         edtavSelecionar_Tooltiptext = "Selecionar �rea de trabalho";
         edtContratante_RazaoSocial_Link = formatLink("viewcontratante.aspx") + "?" + UrlEncode("" +A29Contratante_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtMunicipio_Nome_Link = formatLink("viewmunicipio.aspx") + "?" + UrlEncode("" +A25Municipio_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtavSelecionar_Visible = ((A5AreaTrabalho_Codigo!=AV6WWPContext.gxTpr_Areatrabalho_codigo) ? 1 : 0);
         if ( ( NumberUtil.Val( StringUtil.Substring( AV6WWPContext.gxTpr_Validationkey, 1, 2), ".") <= Convert.ToDecimal( subGrid_Recordcount( ) )) && ( StringUtil.StrCmp(StringUtil.Substring( AV6WWPContext.gxTpr_Validationkey, 1, 2), "00") != 0 ) )
         {
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
            imgInsert_Tooltiptext = "Limite da sua vers�o atingido!";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Tooltiptext", imgInsert_Tooltiptext);
         }
         edtavUpdate_Visible = (A72AreaTrabalho_Ativo||AV6WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         edtavDelete_Visible = (A72AreaTrabalho_Ativo||AV6WWPContext.gxTpr_Userehadministradorgam ? 1 : 0);
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Display ? 1 : 0);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 94;
         }
         sendrow_942( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_94_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(94, GridRow);
         }
      }

      protected void E230T2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E290T2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E240T2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV90Organizacao_Nome1, AV129AreaTrabalho_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21AreaTrabalho_Descricao2, AV92Organizacao_Nome2, AV130AreaTrabalho_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25AreaTrabalho_Descricao3, AV131Organizacao_Nome3, AV132AreaTrabalho_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV61TFAreaTrabalho_Descricao, AV62TFAreaTrabalho_Descricao_Sel, AV65TFContratante_RazaoSocial, AV66TFContratante_RazaoSocial_Sel, AV69TFContratante_CNPJ, AV70TFContratante_CNPJ_Sel, AV73TFContratante_Telefone, AV74TFContratante_Telefone_Sel, AV77TFAreaTrabalho_ValidaOSFM_Sel, AV80TFAreaTrabalho_DiasParaPagar, AV81TFAreaTrabalho_DiasParaPagar_To, AV134TFEstado_UF, AV135TFEstado_UF_Sel, AV118TFMunicipio_Nome, AV119TFMunicipio_Nome_Sel, AV84TFAreaTrabalho_Ativo_Sel, AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV71ddo_Contratante_CNPJTitleControlIdToReplace, AV75ddo_Contratante_TelefoneTitleControlIdToReplace, AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace, AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace, AV136ddo_Estado_UFTitleControlIdToReplace, AV120ddo_Municipio_NomeTitleControlIdToReplace, AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace, AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace, AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace, AV123TFAreaTrabalho_CalculoPFinal_Sels, AV144TFAreaTrabalho_VerTA_Sels, AV6WWPContext, AV188Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A5AreaTrabalho_Codigo, A29Contratante_Codigo, A25Municipio_Codigo, A72AreaTrabalho_Ativo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavAreatrabalho_ativo1.CurrentValue = StringUtil.RTrim( AV129AreaTrabalho_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo1_Internalname, "Values", cmbavAreatrabalho_ativo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavAreatrabalho_ativo2.CurrentValue = StringUtil.RTrim( AV130AreaTrabalho_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo2_Internalname, "Values", cmbavAreatrabalho_ativo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavAreatrabalho_ativo3.CurrentValue = StringUtil.RTrim( AV132AreaTrabalho_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo3_Internalname, "Values", cmbavAreatrabalho_ativo3.ToJavascriptSource());
      }

      protected void E300T2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E310T2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E250T2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV90Organizacao_Nome1, AV129AreaTrabalho_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21AreaTrabalho_Descricao2, AV92Organizacao_Nome2, AV130AreaTrabalho_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25AreaTrabalho_Descricao3, AV131Organizacao_Nome3, AV132AreaTrabalho_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV61TFAreaTrabalho_Descricao, AV62TFAreaTrabalho_Descricao_Sel, AV65TFContratante_RazaoSocial, AV66TFContratante_RazaoSocial_Sel, AV69TFContratante_CNPJ, AV70TFContratante_CNPJ_Sel, AV73TFContratante_Telefone, AV74TFContratante_Telefone_Sel, AV77TFAreaTrabalho_ValidaOSFM_Sel, AV80TFAreaTrabalho_DiasParaPagar, AV81TFAreaTrabalho_DiasParaPagar_To, AV134TFEstado_UF, AV135TFEstado_UF_Sel, AV118TFMunicipio_Nome, AV119TFMunicipio_Nome_Sel, AV84TFAreaTrabalho_Ativo_Sel, AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV71ddo_Contratante_CNPJTitleControlIdToReplace, AV75ddo_Contratante_TelefoneTitleControlIdToReplace, AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace, AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace, AV136ddo_Estado_UFTitleControlIdToReplace, AV120ddo_Municipio_NomeTitleControlIdToReplace, AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace, AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace, AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace, AV123TFAreaTrabalho_CalculoPFinal_Sels, AV144TFAreaTrabalho_VerTA_Sels, AV6WWPContext, AV188Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A5AreaTrabalho_Codigo, A29Contratante_Codigo, A25Municipio_Codigo, A72AreaTrabalho_Ativo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavAreatrabalho_ativo1.CurrentValue = StringUtil.RTrim( AV129AreaTrabalho_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo1_Internalname, "Values", cmbavAreatrabalho_ativo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavAreatrabalho_ativo2.CurrentValue = StringUtil.RTrim( AV130AreaTrabalho_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo2_Internalname, "Values", cmbavAreatrabalho_ativo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavAreatrabalho_ativo3.CurrentValue = StringUtil.RTrim( AV132AreaTrabalho_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo3_Internalname, "Values", cmbavAreatrabalho_ativo3.ToJavascriptSource());
      }

      protected void E320T2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E260T2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17AreaTrabalho_Descricao1, AV90Organizacao_Nome1, AV129AreaTrabalho_Ativo1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21AreaTrabalho_Descricao2, AV92Organizacao_Nome2, AV130AreaTrabalho_Ativo2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25AreaTrabalho_Descricao3, AV131Organizacao_Nome3, AV132AreaTrabalho_Ativo3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV61TFAreaTrabalho_Descricao, AV62TFAreaTrabalho_Descricao_Sel, AV65TFContratante_RazaoSocial, AV66TFContratante_RazaoSocial_Sel, AV69TFContratante_CNPJ, AV70TFContratante_CNPJ_Sel, AV73TFContratante_Telefone, AV74TFContratante_Telefone_Sel, AV77TFAreaTrabalho_ValidaOSFM_Sel, AV80TFAreaTrabalho_DiasParaPagar, AV81TFAreaTrabalho_DiasParaPagar_To, AV134TFEstado_UF, AV135TFEstado_UF_Sel, AV118TFMunicipio_Nome, AV119TFMunicipio_Nome_Sel, AV84TFAreaTrabalho_Ativo_Sel, AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace, AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace, AV71ddo_Contratante_CNPJTitleControlIdToReplace, AV75ddo_Contratante_TelefoneTitleControlIdToReplace, AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace, AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace, AV136ddo_Estado_UFTitleControlIdToReplace, AV120ddo_Municipio_NomeTitleControlIdToReplace, AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace, AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace, AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace, AV123TFAreaTrabalho_CalculoPFinal_Sels, AV144TFAreaTrabalho_VerTA_Sels, AV6WWPContext, AV188Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A5AreaTrabalho_Codigo, A29Contratante_Codigo, A25Municipio_Codigo, A72AreaTrabalho_Ativo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavAreatrabalho_ativo1.CurrentValue = StringUtil.RTrim( AV129AreaTrabalho_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo1_Internalname, "Values", cmbavAreatrabalho_ativo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavAreatrabalho_ativo2.CurrentValue = StringUtil.RTrim( AV130AreaTrabalho_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo2_Internalname, "Values", cmbavAreatrabalho_ativo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavAreatrabalho_ativo3.CurrentValue = StringUtil.RTrim( AV132AreaTrabalho_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo3_Internalname, "Values", cmbavAreatrabalho_ativo3.ToJavascriptSource());
      }

      protected void E330T2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E270T2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV123TFAreaTrabalho_CalculoPFinal_Sels", AV123TFAreaTrabalho_CalculoPFinal_Sels);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV144TFAreaTrabalho_VerTA_Sels", AV144TFAreaTrabalho_VerTA_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavAreatrabalho_ativo1.CurrentValue = StringUtil.RTrim( AV129AreaTrabalho_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo1_Internalname, "Values", cmbavAreatrabalho_ativo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavAreatrabalho_ativo2.CurrentValue = StringUtil.RTrim( AV130AreaTrabalho_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo2_Internalname, "Values", cmbavAreatrabalho_ativo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavAreatrabalho_ativo3.CurrentValue = StringUtil.RTrim( AV132AreaTrabalho_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo3_Internalname, "Values", cmbavAreatrabalho_ativo3.ToJavascriptSource());
      }

      protected void E280T2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("areatrabalho.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S192( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_areatrabalho_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_descricao_Internalname, "SortedStatus", Ddo_areatrabalho_descricao_Sortedstatus);
         Ddo_contratante_razaosocial_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SortedStatus", Ddo_contratante_razaosocial_Sortedstatus);
         Ddo_contratante_cnpj_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SortedStatus", Ddo_contratante_cnpj_Sortedstatus);
         Ddo_contratante_telefone_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SortedStatus", Ddo_contratante_telefone_Sortedstatus);
         Ddo_areatrabalho_validaosfm_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_validaosfm_Internalname, "SortedStatus", Ddo_areatrabalho_validaosfm_Sortedstatus);
         Ddo_areatrabalho_diasparapagar_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_diasparapagar_Internalname, "SortedStatus", Ddo_areatrabalho_diasparapagar_Sortedstatus);
         Ddo_estado_uf_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
         Ddo_municipio_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
         Ddo_areatrabalho_calculopfinal_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_calculopfinal_Internalname, "SortedStatus", Ddo_areatrabalho_calculopfinal_Sortedstatus);
         Ddo_areatrabalho_verta_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_verta_Internalname, "SortedStatus", Ddo_areatrabalho_verta_Sortedstatus);
         Ddo_areatrabalho_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_ativo_Internalname, "SortedStatus", Ddo_areatrabalho_ativo_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_areatrabalho_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_descricao_Internalname, "SortedStatus", Ddo_areatrabalho_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratante_razaosocial_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SortedStatus", Ddo_contratante_razaosocial_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratante_cnpj_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SortedStatus", Ddo_contratante_cnpj_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_contratante_telefone_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SortedStatus", Ddo_contratante_telefone_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_areatrabalho_validaosfm_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_validaosfm_Internalname, "SortedStatus", Ddo_areatrabalho_validaosfm_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_areatrabalho_diasparapagar_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_diasparapagar_Internalname, "SortedStatus", Ddo_areatrabalho_diasparapagar_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_estado_uf_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_municipio_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SortedStatus", Ddo_municipio_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_areatrabalho_calculopfinal_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_calculopfinal_Internalname, "SortedStatus", Ddo_areatrabalho_calculopfinal_Sortedstatus);
         }
         else if ( AV13OrderedBy == 10 )
         {
            Ddo_areatrabalho_verta_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_verta_Internalname, "SortedStatus", Ddo_areatrabalho_verta_Sortedstatus);
         }
         else if ( AV13OrderedBy == 11 )
         {
            Ddo_areatrabalho_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_ativo_Internalname, "SortedStatus", Ddo_areatrabalho_ativo_Sortedstatus);
         }
      }

      protected void S172( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavAreatrabalho_descricao1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao1_Visible), 5, 0)));
         edtavOrganizacao_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrganizacao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrganizacao_nome1_Visible), 5, 0)));
         cmbavAreatrabalho_ativo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAreatrabalho_ativo1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_DESCRICAO") == 0 )
         {
            edtavAreatrabalho_descricao1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 )
         {
            edtavOrganizacao_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrganizacao_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrganizacao_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_ATIVO") == 0 )
         {
            cmbavAreatrabalho_ativo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAreatrabalho_ativo1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavAreatrabalho_descricao2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao2_Visible), 5, 0)));
         edtavOrganizacao_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrganizacao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrganizacao_nome2_Visible), 5, 0)));
         cmbavAreatrabalho_ativo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAreatrabalho_ativo2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AREATRABALHO_DESCRICAO") == 0 )
         {
            edtavAreatrabalho_descricao2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ORGANIZACAO_NOME") == 0 )
         {
            edtavOrganizacao_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrganizacao_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrganizacao_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AREATRABALHO_ATIVO") == 0 )
         {
            cmbavAreatrabalho_ativo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAreatrabalho_ativo2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavAreatrabalho_descricao3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao3_Visible), 5, 0)));
         edtavOrganizacao_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrganizacao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrganizacao_nome3_Visible), 5, 0)));
         cmbavAreatrabalho_ativo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAreatrabalho_ativo3.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AREATRABALHO_DESCRICAO") == 0 )
         {
            edtavAreatrabalho_descricao3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ORGANIZACAO_NOME") == 0 )
         {
            edtavOrganizacao_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrganizacao_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrganizacao_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AREATRABALHO_ATIVO") == 0 )
         {
            cmbavAreatrabalho_ativo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAreatrabalho_ativo3.Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "AREATRABALHO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV21AreaTrabalho_Descricao2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AreaTrabalho_Descricao2", AV21AreaTrabalho_Descricao2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "AREATRABALHO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV25AreaTrabalho_Descricao3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AreaTrabalho_Descricao3", AV25AreaTrabalho_Descricao3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV61TFAreaTrabalho_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFAreaTrabalho_Descricao", AV61TFAreaTrabalho_Descricao);
         Ddo_areatrabalho_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_descricao_Internalname, "FilteredText_set", Ddo_areatrabalho_descricao_Filteredtext_set);
         AV62TFAreaTrabalho_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFAreaTrabalho_Descricao_Sel", AV62TFAreaTrabalho_Descricao_Sel);
         Ddo_areatrabalho_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_descricao_Internalname, "SelectedValue_set", Ddo_areatrabalho_descricao_Selectedvalue_set);
         AV65TFContratante_RazaoSocial = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratante_RazaoSocial", AV65TFContratante_RazaoSocial);
         Ddo_contratante_razaosocial_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "FilteredText_set", Ddo_contratante_razaosocial_Filteredtext_set);
         AV66TFContratante_RazaoSocial_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratante_RazaoSocial_Sel", AV66TFContratante_RazaoSocial_Sel);
         Ddo_contratante_razaosocial_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SelectedValue_set", Ddo_contratante_razaosocial_Selectedvalue_set);
         AV69TFContratante_CNPJ = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratante_CNPJ", AV69TFContratante_CNPJ);
         Ddo_contratante_cnpj_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "FilteredText_set", Ddo_contratante_cnpj_Filteredtext_set);
         AV70TFContratante_CNPJ_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratante_CNPJ_Sel", AV70TFContratante_CNPJ_Sel);
         Ddo_contratante_cnpj_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SelectedValue_set", Ddo_contratante_cnpj_Selectedvalue_set);
         AV73TFContratante_Telefone = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratante_Telefone", AV73TFContratante_Telefone);
         Ddo_contratante_telefone_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "FilteredText_set", Ddo_contratante_telefone_Filteredtext_set);
         AV74TFContratante_Telefone_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratante_Telefone_Sel", AV74TFContratante_Telefone_Sel);
         Ddo_contratante_telefone_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SelectedValue_set", Ddo_contratante_telefone_Selectedvalue_set);
         AV77TFAreaTrabalho_ValidaOSFM_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAreaTrabalho_ValidaOSFM_Sel", StringUtil.Str( (decimal)(AV77TFAreaTrabalho_ValidaOSFM_Sel), 1, 0));
         Ddo_areatrabalho_validaosfm_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_validaosfm_Internalname, "SelectedValue_set", Ddo_areatrabalho_validaosfm_Selectedvalue_set);
         AV80TFAreaTrabalho_DiasParaPagar = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFAreaTrabalho_DiasParaPagar), 4, 0)));
         Ddo_areatrabalho_diasparapagar_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_diasparapagar_Internalname, "FilteredText_set", Ddo_areatrabalho_diasparapagar_Filteredtext_set);
         AV81TFAreaTrabalho_DiasParaPagar_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFAreaTrabalho_DiasParaPagar_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFAreaTrabalho_DiasParaPagar_To), 4, 0)));
         Ddo_areatrabalho_diasparapagar_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_diasparapagar_Internalname, "FilteredTextTo_set", Ddo_areatrabalho_diasparapagar_Filteredtextto_set);
         AV134TFEstado_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134TFEstado_UF", AV134TFEstado_UF);
         Ddo_estado_uf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "FilteredText_set", Ddo_estado_uf_Filteredtext_set);
         AV135TFEstado_UF_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135TFEstado_UF_Sel", AV135TFEstado_UF_Sel);
         Ddo_estado_uf_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SelectedValue_set", Ddo_estado_uf_Selectedvalue_set);
         AV118TFMunicipio_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118TFMunicipio_Nome", AV118TFMunicipio_Nome);
         Ddo_municipio_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "FilteredText_set", Ddo_municipio_nome_Filteredtext_set);
         AV119TFMunicipio_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119TFMunicipio_Nome_Sel", AV119TFMunicipio_Nome_Sel);
         Ddo_municipio_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SelectedValue_set", Ddo_municipio_nome_Selectedvalue_set);
         AV123TFAreaTrabalho_CalculoPFinal_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_areatrabalho_calculopfinal_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_calculopfinal_Internalname, "SelectedValue_set", Ddo_areatrabalho_calculopfinal_Selectedvalue_set);
         AV144TFAreaTrabalho_VerTA_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_areatrabalho_verta_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_verta_Internalname, "SelectedValue_set", Ddo_areatrabalho_verta_Selectedvalue_set);
         AV84TFAreaTrabalho_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAreaTrabalho_Ativo_Sel", StringUtil.Str( (decimal)(AV84TFAreaTrabalho_Ativo_Sel), 1, 0));
         Ddo_areatrabalho_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_ativo_Internalname, "SelectedValue_set", Ddo_areatrabalho_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "AREATRABALHO_DESCRICAO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17AreaTrabalho_Descricao1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV32Session.Get(AV188Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV188Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV32Session.Get(AV188Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S252( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV189GXV1 = 1;
         while ( AV189GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV189GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_DESCRICAO") == 0 )
            {
               AV61TFAreaTrabalho_Descricao = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61TFAreaTrabalho_Descricao", AV61TFAreaTrabalho_Descricao);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFAreaTrabalho_Descricao)) )
               {
                  Ddo_areatrabalho_descricao_Filteredtext_set = AV61TFAreaTrabalho_Descricao;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_descricao_Internalname, "FilteredText_set", Ddo_areatrabalho_descricao_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_DESCRICAO_SEL") == 0 )
            {
               AV62TFAreaTrabalho_Descricao_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62TFAreaTrabalho_Descricao_Sel", AV62TFAreaTrabalho_Descricao_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFAreaTrabalho_Descricao_Sel)) )
               {
                  Ddo_areatrabalho_descricao_Selectedvalue_set = AV62TFAreaTrabalho_Descricao_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_descricao_Internalname, "SelectedValue_set", Ddo_areatrabalho_descricao_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAZAOSOCIAL") == 0 )
            {
               AV65TFContratante_RazaoSocial = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFContratante_RazaoSocial", AV65TFContratante_RazaoSocial);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratante_RazaoSocial)) )
               {
                  Ddo_contratante_razaosocial_Filteredtext_set = AV65TFContratante_RazaoSocial;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "FilteredText_set", Ddo_contratante_razaosocial_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_RAZAOSOCIAL_SEL") == 0 )
            {
               AV66TFContratante_RazaoSocial_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66TFContratante_RazaoSocial_Sel", AV66TFContratante_RazaoSocial_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContratante_RazaoSocial_Sel)) )
               {
                  Ddo_contratante_razaosocial_Selectedvalue_set = AV66TFContratante_RazaoSocial_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_razaosocial_Internalname, "SelectedValue_set", Ddo_contratante_razaosocial_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_CNPJ") == 0 )
            {
               AV69TFContratante_CNPJ = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFContratante_CNPJ", AV69TFContratante_CNPJ);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69TFContratante_CNPJ)) )
               {
                  Ddo_contratante_cnpj_Filteredtext_set = AV69TFContratante_CNPJ;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "FilteredText_set", Ddo_contratante_cnpj_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_CNPJ_SEL") == 0 )
            {
               AV70TFContratante_CNPJ_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70TFContratante_CNPJ_Sel", AV70TFContratante_CNPJ_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFContratante_CNPJ_Sel)) )
               {
                  Ddo_contratante_cnpj_Selectedvalue_set = AV70TFContratante_CNPJ_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_cnpj_Internalname, "SelectedValue_set", Ddo_contratante_cnpj_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_TELEFONE") == 0 )
            {
               AV73TFContratante_Telefone = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV73TFContratante_Telefone", AV73TFContratante_Telefone);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFContratante_Telefone)) )
               {
                  Ddo_contratante_telefone_Filteredtext_set = AV73TFContratante_Telefone;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "FilteredText_set", Ddo_contratante_telefone_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFCONTRATANTE_TELEFONE_SEL") == 0 )
            {
               AV74TFContratante_Telefone_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFContratante_Telefone_Sel", AV74TFContratante_Telefone_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFContratante_Telefone_Sel)) )
               {
                  Ddo_contratante_telefone_Selectedvalue_set = AV74TFContratante_Telefone_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratante_telefone_Internalname, "SelectedValue_set", Ddo_contratante_telefone_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_VALIDAOSFM_SEL") == 0 )
            {
               AV77TFAreaTrabalho_ValidaOSFM_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFAreaTrabalho_ValidaOSFM_Sel", StringUtil.Str( (decimal)(AV77TFAreaTrabalho_ValidaOSFM_Sel), 1, 0));
               if ( ! (0==AV77TFAreaTrabalho_ValidaOSFM_Sel) )
               {
                  Ddo_areatrabalho_validaosfm_Selectedvalue_set = StringUtil.Str( (decimal)(AV77TFAreaTrabalho_ValidaOSFM_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_validaosfm_Internalname, "SelectedValue_set", Ddo_areatrabalho_validaosfm_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_DIASPARAPAGAR") == 0 )
            {
               AV80TFAreaTrabalho_DiasParaPagar = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFAreaTrabalho_DiasParaPagar", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80TFAreaTrabalho_DiasParaPagar), 4, 0)));
               AV81TFAreaTrabalho_DiasParaPagar_To = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81TFAreaTrabalho_DiasParaPagar_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81TFAreaTrabalho_DiasParaPagar_To), 4, 0)));
               if ( ! (0==AV80TFAreaTrabalho_DiasParaPagar) )
               {
                  Ddo_areatrabalho_diasparapagar_Filteredtext_set = StringUtil.Str( (decimal)(AV80TFAreaTrabalho_DiasParaPagar), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_diasparapagar_Internalname, "FilteredText_set", Ddo_areatrabalho_diasparapagar_Filteredtext_set);
               }
               if ( ! (0==AV81TFAreaTrabalho_DiasParaPagar_To) )
               {
                  Ddo_areatrabalho_diasparapagar_Filteredtextto_set = StringUtil.Str( (decimal)(AV81TFAreaTrabalho_DiasParaPagar_To), 4, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_diasparapagar_Internalname, "FilteredTextTo_set", Ddo_areatrabalho_diasparapagar_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFESTADO_UF") == 0 )
            {
               AV134TFEstado_UF = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV134TFEstado_UF", AV134TFEstado_UF);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134TFEstado_UF)) )
               {
                  Ddo_estado_uf_Filteredtext_set = AV134TFEstado_UF;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "FilteredText_set", Ddo_estado_uf_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFESTADO_UF_SEL") == 0 )
            {
               AV135TFEstado_UF_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV135TFEstado_UF_Sel", AV135TFEstado_UF_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV135TFEstado_UF_Sel)) )
               {
                  Ddo_estado_uf_Selectedvalue_set = AV135TFEstado_UF_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_estado_uf_Internalname, "SelectedValue_set", Ddo_estado_uf_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME") == 0 )
            {
               AV118TFMunicipio_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118TFMunicipio_Nome", AV118TFMunicipio_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118TFMunicipio_Nome)) )
               {
                  Ddo_municipio_nome_Filteredtext_set = AV118TFMunicipio_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "FilteredText_set", Ddo_municipio_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFMUNICIPIO_NOME_SEL") == 0 )
            {
               AV119TFMunicipio_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119TFMunicipio_Nome_Sel", AV119TFMunicipio_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119TFMunicipio_Nome_Sel)) )
               {
                  Ddo_municipio_nome_Selectedvalue_set = AV119TFMunicipio_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_municipio_nome_Internalname, "SelectedValue_set", Ddo_municipio_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_CALCULOPFINAL_SEL") == 0 )
            {
               AV122TFAreaTrabalho_CalculoPFinal_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV123TFAreaTrabalho_CalculoPFinal_Sels.FromJSonString(AV122TFAreaTrabalho_CalculoPFinal_SelsJson);
               if ( ! ( AV123TFAreaTrabalho_CalculoPFinal_Sels.Count == 0 ) )
               {
                  Ddo_areatrabalho_calculopfinal_Selectedvalue_set = AV122TFAreaTrabalho_CalculoPFinal_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_calculopfinal_Internalname, "SelectedValue_set", Ddo_areatrabalho_calculopfinal_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_VERTA_SEL") == 0 )
            {
               AV143TFAreaTrabalho_VerTA_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV144TFAreaTrabalho_VerTA_Sels.FromJSonString(AV143TFAreaTrabalho_VerTA_SelsJson);
               if ( ! ( AV144TFAreaTrabalho_VerTA_Sels.Count == 0 ) )
               {
                  Ddo_areatrabalho_verta_Selectedvalue_set = AV143TFAreaTrabalho_VerTA_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_verta_Internalname, "SelectedValue_set", Ddo_areatrabalho_verta_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFAREATRABALHO_ATIVO_SEL") == 0 )
            {
               AV84TFAreaTrabalho_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84TFAreaTrabalho_Ativo_Sel", StringUtil.Str( (decimal)(AV84TFAreaTrabalho_Ativo_Sel), 1, 0));
               if ( ! (0==AV84TFAreaTrabalho_Ativo_Sel) )
               {
                  Ddo_areatrabalho_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV84TFAreaTrabalho_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_areatrabalho_ativo_Internalname, "SelectedValue_set", Ddo_areatrabalho_ativo_Selectedvalue_set);
               }
            }
            AV189GXV1 = (int)(AV189GXV1+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_DESCRICAO") == 0 )
            {
               AV17AreaTrabalho_Descricao1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17AreaTrabalho_Descricao1", AV17AreaTrabalho_Descricao1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV90Organizacao_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90Organizacao_Nome1", AV90Organizacao_Nome1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_ATIVO") == 0 )
            {
               AV129AreaTrabalho_Ativo1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV129AreaTrabalho_Ativo1", AV129AreaTrabalho_Ativo1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AREATRABALHO_DESCRICAO") == 0 )
               {
                  AV21AreaTrabalho_Descricao2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21AreaTrabalho_Descricao2", AV21AreaTrabalho_Descricao2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ORGANIZACAO_NOME") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV92Organizacao_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV92Organizacao_Nome2", AV92Organizacao_Nome2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AREATRABALHO_ATIVO") == 0 )
               {
                  AV130AreaTrabalho_Ativo2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV130AreaTrabalho_Ativo2", AV130AreaTrabalho_Ativo2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AREATRABALHO_DESCRICAO") == 0 )
                  {
                     AV25AreaTrabalho_Descricao3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25AreaTrabalho_Descricao3", AV25AreaTrabalho_Descricao3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ORGANIZACAO_NOME") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV131Organizacao_Nome3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV131Organizacao_Nome3", AV131Organizacao_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AREATRABALHO_ATIVO") == 0 )
                  {
                     AV132AreaTrabalho_Ativo3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV132AreaTrabalho_Ativo3", AV132AreaTrabalho_Ativo3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV32Session.Get(AV188Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61TFAreaTrabalho_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAREATRABALHO_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV61TFAreaTrabalho_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62TFAreaTrabalho_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAREATRABALHO_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV62TFAreaTrabalho_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65TFContratante_RazaoSocial)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_RAZAOSOCIAL";
            AV11GridStateFilterValue.gxTpr_Value = AV65TFContratante_RazaoSocial;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66TFContratante_RazaoSocial_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_RAZAOSOCIAL_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV66TFContratante_RazaoSocial_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69TFContratante_CNPJ)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_CNPJ";
            AV11GridStateFilterValue.gxTpr_Value = AV69TFContratante_CNPJ;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70TFContratante_CNPJ_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_CNPJ_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV70TFContratante_CNPJ_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73TFContratante_Telefone)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_TELEFONE";
            AV11GridStateFilterValue.gxTpr_Value = AV73TFContratante_Telefone;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74TFContratante_Telefone_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATANTE_TELEFONE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV74TFContratante_Telefone_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV77TFAreaTrabalho_ValidaOSFM_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAREATRABALHO_VALIDAOSFM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV77TFAreaTrabalho_ValidaOSFM_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV80TFAreaTrabalho_DiasParaPagar) && (0==AV81TFAreaTrabalho_DiasParaPagar_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAREATRABALHO_DIASPARAPAGAR";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV80TFAreaTrabalho_DiasParaPagar), 4, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV81TFAreaTrabalho_DiasParaPagar_To), 4, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV134TFEstado_UF)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFESTADO_UF";
            AV11GridStateFilterValue.gxTpr_Value = AV134TFEstado_UF;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV135TFEstado_UF_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFESTADO_UF_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV135TFEstado_UF_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118TFMunicipio_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMUNICIPIO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV118TFMunicipio_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119TFMunicipio_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFMUNICIPIO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV119TFMunicipio_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV123TFAreaTrabalho_CalculoPFinal_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAREATRABALHO_CALCULOPFINAL_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV123TFAreaTrabalho_CalculoPFinal_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV144TFAreaTrabalho_VerTA_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAREATRABALHO_VERTA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV144TFAreaTrabalho_VerTA_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV84TFAreaTrabalho_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFAREATRABALHO_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV84TFAreaTrabalho_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV188Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17AreaTrabalho_Descricao1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17AreaTrabalho_Descricao1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV90Organizacao_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV90Organizacao_Nome1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "AREATRABALHO_ATIVO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV129AreaTrabalho_Ativo1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV129AreaTrabalho_Ativo1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AREATRABALHO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21AreaTrabalho_Descricao2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21AreaTrabalho_Descricao2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "ORGANIZACAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV92Organizacao_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV92Organizacao_Nome2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "AREATRABALHO_ATIVO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV130AreaTrabalho_Ativo2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV130AreaTrabalho_Ativo2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AREATRABALHO_DESCRICAO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25AreaTrabalho_Descricao3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25AreaTrabalho_Descricao3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "ORGANIZACAO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV131Organizacao_Nome3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV131Organizacao_Nome3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "AREATRABALHO_ATIVO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV132AreaTrabalho_Ativo3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV132AreaTrabalho_Ativo3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV188Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "AreaTrabalho";
         AV32Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void E370T2( )
      {
         /* Selecionar_Click Routine */
         /* Execute user subroutine: 'SELECIONARAREAINATIVA' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.wjLoc = formatLink("wwareatrabalho.aspx") ;
         context.wjLocDisableFrm = 1;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
      }

      protected void S242( )
      {
         /* 'SELECIONARAREAINATIVA' Routine */
         AV6WWPContext.gxTpr_Areatrabalho_codigo = A5AreaTrabalho_Codigo;
         AV6WWPContext.gxTpr_Areatrabalho_descricao = A6AreaTrabalho_Descricao+(A72AreaTrabalho_Ativo ? "" : " (Inativa)");
         AV6WWPContext.gxTpr_Contratante_razaosocial = A9Contratante_RazaoSocial;
         AV6WWPContext.gxTpr_Contratante_nomefantasia = A10Contratante_NomeFantasia;
         AV6WWPContext.gxTpr_Contratante_telefone = A31Contratante_Telefone;
         AV6WWPContext.gxTpr_Servicopadrao = (short)(A830AreaTrabalho_ServicoPadrao);
         AV6WWPContext.gxTpr_Calculopfinal = A642AreaTrabalho_CalculoPFinal;
         AV6WWPContext.gxTpr_Insert = A72AreaTrabalho_Ativo;
         AV6WWPContext.gxTpr_Update = A72AreaTrabalho_Ativo;
         AV6WWPContext.gxTpr_Delete = A72AreaTrabalho_Ativo;
         AV56Contratante_Codigo = A29Contratante_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56Contratante_Codigo), 6, 0)));
         /* Execute user subroutine: 'CONTRATANTEOUCONTRATADA' */
         S262 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.setwwpcontext(context ).execute(  AV6WWPContext) ;
      }

      protected void S262( )
      {
         /* 'CONTRATANTEOUCONTRATADA' Routine */
         AV6WWPContext.gxTpr_Contratada_codigo = 0;
         AV6WWPContext.gxTpr_Contratada_pessoacod = 0;
         AV6WWPContext.gxTpr_Contratada_pessoanom = "";
         AV6WWPContext.gxTpr_Contratante_codigo = 0;
         AV6WWPContext.gxTpr_Contratante_razaosocial = "";
         AV6WWPContext.gxTpr_Userehcontratante = false;
         AV6WWPContext.gxTpr_Userehcontratada = false;
         AV190GXLvl1289 = 0;
         /* Using cursor H000T4 */
         pr_default.execute(2, new Object[] {AV56Contratante_Codigo, AV6WWPContext.gxTpr_Userid});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A340ContratanteUsuario_ContratantePesCod = H000T4_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = H000T4_n340ContratanteUsuario_ContratantePesCod[0];
            A60ContratanteUsuario_UsuarioCod = H000T4_A60ContratanteUsuario_UsuarioCod[0];
            A63ContratanteUsuario_ContratanteCod = H000T4_A63ContratanteUsuario_ContratanteCod[0];
            A64ContratanteUsuario_ContratanteRaz = H000T4_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = H000T4_n64ContratanteUsuario_ContratanteRaz[0];
            A65ContratanteUsuario_ContratanteFan = H000T4_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = H000T4_n65ContratanteUsuario_ContratanteFan[0];
            A31Contratante_Telefone = H000T4_A31Contratante_Telefone[0];
            n31Contratante_Telefone = H000T4_n31Contratante_Telefone[0];
            A340ContratanteUsuario_ContratantePesCod = H000T4_A340ContratanteUsuario_ContratantePesCod[0];
            n340ContratanteUsuario_ContratantePesCod = H000T4_n340ContratanteUsuario_ContratantePesCod[0];
            A65ContratanteUsuario_ContratanteFan = H000T4_A65ContratanteUsuario_ContratanteFan[0];
            n65ContratanteUsuario_ContratanteFan = H000T4_n65ContratanteUsuario_ContratanteFan[0];
            A31Contratante_Telefone = H000T4_A31Contratante_Telefone[0];
            n31Contratante_Telefone = H000T4_n31Contratante_Telefone[0];
            A64ContratanteUsuario_ContratanteRaz = H000T4_A64ContratanteUsuario_ContratanteRaz[0];
            n64ContratanteUsuario_ContratanteRaz = H000T4_n64ContratanteUsuario_ContratanteRaz[0];
            AV190GXLvl1289 = 1;
            AV6WWPContext.gxTpr_Userehcontratante = true;
            AV6WWPContext.gxTpr_Contratante_codigo = A63ContratanteUsuario_ContratanteCod;
            AV6WWPContext.gxTpr_Contratante_razaosocial = A64ContratanteUsuario_ContratanteRaz;
            AV6WWPContext.gxTpr_Contratante_nomefantasia = A65ContratanteUsuario_ContratanteFan;
            AV6WWPContext.gxTpr_Contratante_telefone = A31Contratante_Telefone;
            AV6WWPContext.gxTpr_Userehgestor = true;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         if ( AV190GXLvl1289 == 0 )
         {
            /* Using cursor H000T5 */
            pr_default.execute(3, new Object[] {AV6WWPContext.gxTpr_Userid, AV6WWPContext.gxTpr_Areatrabalho_codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A69ContratadaUsuario_UsuarioCod = H000T5_A69ContratadaUsuario_UsuarioCod[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H000T5_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H000T5_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A66ContratadaUsuario_ContratadaCod = H000T5_A66ContratadaUsuario_ContratadaCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = H000T5_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H000T5_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A68ContratadaUsuario_ContratadaPessoaNom = H000T5_A68ContratadaUsuario_ContratadaPessoaNom[0];
               n68ContratadaUsuario_ContratadaPessoaNom = H000T5_n68ContratadaUsuario_ContratadaPessoaNom[0];
               A1228ContratadaUsuario_AreaTrabalhoCod = H000T5_A1228ContratadaUsuario_AreaTrabalhoCod[0];
               n1228ContratadaUsuario_AreaTrabalhoCod = H000T5_n1228ContratadaUsuario_AreaTrabalhoCod[0];
               A67ContratadaUsuario_ContratadaPessoaCod = H000T5_A67ContratadaUsuario_ContratadaPessoaCod[0];
               n67ContratadaUsuario_ContratadaPessoaCod = H000T5_n67ContratadaUsuario_ContratadaPessoaCod[0];
               A68ContratadaUsuario_ContratadaPessoaNom = H000T5_A68ContratadaUsuario_ContratadaPessoaNom[0];
               n68ContratadaUsuario_ContratadaPessoaNom = H000T5_n68ContratadaUsuario_ContratadaPessoaNom[0];
               AV6WWPContext.gxTpr_Userehcontratada = true;
               AV6WWPContext.gxTpr_Contratada_codigo = A66ContratadaUsuario_ContratadaCod;
               AV6WWPContext.gxTpr_Contratada_pessoacod = A67ContratadaUsuario_ContratadaPessoaCod;
               AV6WWPContext.gxTpr_Contratada_pessoanom = A68ContratadaUsuario_ContratadaPessoaNom;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(3);
            }
            pr_default.close(3);
         }
         /* Execute user subroutine: 'SERVICOSGERIDOS' */
         S272 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S272( )
      {
         /* 'SERVICOSGERIDOS' Routine */
         AV6WWPContext.gxTpr_Servicosgeridos = "";
         if ( AV6WWPContext.gxTpr_Userehcontratada )
         {
            /* Using cursor H000T6 */
            pr_default.execute(4, new Object[] {AV6WWPContext.gxTpr_Userid, AV6WWPContext.gxTpr_Contratada_codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A1078ContratoGestor_ContratoCod = H000T6_A1078ContratoGestor_ContratoCod[0];
               A1079ContratoGestor_UsuarioCod = H000T6_A1079ContratoGestor_UsuarioCod[0];
               A1136ContratoGestor_ContratadaCod = H000T6_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = H000T6_n1136ContratoGestor_ContratadaCod[0];
               A1136ContratoGestor_ContratadaCod = H000T6_A1136ContratoGestor_ContratadaCod[0];
               n1136ContratoGestor_ContratadaCod = H000T6_n1136ContratoGestor_ContratadaCod[0];
               AV6WWPContext.gxTpr_Userehgestor = true;
               /* Using cursor H000T7 */
               pr_default.execute(5, new Object[] {A1078ContratoGestor_ContratoCod});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A74Contrato_Codigo = H000T7_A74Contrato_Codigo[0];
                  A155Servico_Codigo = H000T7_A155Servico_Codigo[0];
                  AV6WWPContext.gxTpr_Servicosgeridos = AV6WWPContext.gxTpr_Servicosgeridos+StringUtil.Trim( StringUtil.Str( (decimal)(A155Servico_Codigo), 6, 0))+",";
                  pr_default.readNext(5);
               }
               pr_default.close(5);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(4);
            }
            pr_default.close(4);
         }
      }

      protected void wb_table1_2_0T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_0T2( true) ;
         }
         else
         {
            wb_table2_8_0T2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_0T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_88_0T2( true) ;
         }
         else
         {
            wb_table3_88_0T2( false) ;
         }
         return  ;
      }

      protected void wb_table3_88_0T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0T2e( true) ;
         }
         else
         {
            wb_table1_2_0T2e( false) ;
         }
      }

      protected void wb_table3_88_0T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_91_0T2( true) ;
         }
         else
         {
            wb_table4_91_0T2( false) ;
         }
         return  ;
      }

      protected void wb_table4_91_0T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_88_0T2e( true) ;
         }
         else
         {
            wb_table3_88_0T2e( false) ;
         }
      }

      protected void wb_table4_91_0T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"94\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAreaTrabalho_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtAreaTrabalho_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAreaTrabalho_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_RazaoSocial_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_RazaoSocial_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_RazaoSocial_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_CNPJ_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_CNPJ_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_CNPJ_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(110), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratante_Telefone_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratante_Telefone_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratante_Telefone_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbAreaTrabalho_ValidaOSFM_Titleformat == 0 )
               {
                  context.SendWebValue( cmbAreaTrabalho_ValidaOSFM.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbAreaTrabalho_ValidaOSFM.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(93), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAreaTrabalho_DiasParaPagar_Titleformat == 0 )
               {
                  context.SendWebValue( edtAreaTrabalho_DiasParaPagar_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAreaTrabalho_DiasParaPagar_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Organiza��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtEstado_UF_Titleformat == 0 )
               {
                  context.SendWebValue( edtEstado_UF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtEstado_UF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtMunicipio_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtMunicipio_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtMunicipio_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbAreaTrabalho_CalculoPFinal_Titleformat == 0 )
               {
                  context.SendWebValue( cmbAreaTrabalho_CalculoPFinal.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbAreaTrabalho_CalculoPFinal.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbAreaTrabalho_VerTA_Titleformat == 0 )
               {
                  context.SendWebValue( cmbAreaTrabalho_VerTA.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbAreaTrabalho_VerTA.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkAreaTrabalho_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkAreaTrabalho_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkAreaTrabalho_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavSelecionar_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Nome Fantasia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Servi�o padr�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV94Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A6AreaTrabalho_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAreaTrabalho_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAreaTrabalho_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A9Contratante_RazaoSocial));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_RazaoSocial_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_RazaoSocial_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtContratante_RazaoSocial_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A12Contratante_CNPJ);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_CNPJ_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_CNPJ_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A31Contratante_Telefone));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratante_Telefone_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratante_Telefone_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbAreaTrabalho_ValidaOSFM.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAreaTrabalho_ValidaOSFM_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAreaTrabalho_DiasParaPagar_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAreaTrabalho_DiasParaPagar_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A23Estado_UF));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtEstado_UF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtEstado_UF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A26Municipio_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtMunicipio_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtMunicipio_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtMunicipio_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbAreaTrabalho_CalculoPFinal.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAreaTrabalho_CalculoPFinal_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2081AreaTrabalho_VerTA), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbAreaTrabalho_VerTA.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAreaTrabalho_VerTA_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A72AreaTrabalho_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkAreaTrabalho_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkAreaTrabalho_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV141Selecionar));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelecionar_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavSelecionar_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A10Contratante_NomeFantasia));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 94 )
         {
            wbEnd = 0;
            nRC_GXsfl_94 = (short)(nGXsfl_94_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_91_0T2e( true) ;
         }
         else
         {
            wb_table4_91_0T2e( false) ;
         }
      }

      protected void wb_table2_8_0T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_0T2( true) ;
         }
         else
         {
            wb_table5_11_0T2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_0T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_23_0T2( true) ;
         }
         else
         {
            wb_table6_23_0T2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_0T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_0T2e( true) ;
         }
         else
         {
            wb_table2_8_0T2e( false) ;
         }
      }

      protected void wb_table6_23_0T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_0T2( true) ;
         }
         else
         {
            wb_table7_28_0T2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_0T2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_0T2e( true) ;
         }
         else
         {
            wb_table6_23_0T2e( false) ;
         }
      }

      protected void wb_table7_28_0T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWAreaTrabalho.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_37_0T2( true) ;
         }
         else
         {
            wb_table8_37_0T2( false) ;
         }
         return  ;
      }

      protected void wb_table8_37_0T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAreaTrabalho.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_WWAreaTrabalho.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_56_0T2( true) ;
         }
         else
         {
            wb_table9_56_0T2( false) ;
         }
         return  ;
      }

      protected void wb_table9_56_0T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAreaTrabalho.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_WWAreaTrabalho.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_75_0T2( true) ;
         }
         else
         {
            wb_table10_75_0T2( false) ;
         }
         return  ;
      }

      protected void wb_table10_75_0T2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_0T2e( true) ;
         }
         else
         {
            wb_table7_28_0T2e( false) ;
         }
      }

      protected void wb_table10_75_0T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "", true, "HLP_WWAreaTrabalho.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_descricao3_Internalname, AV25AreaTrabalho_Descricao3, StringUtil.RTrim( context.localUtil.Format( AV25AreaTrabalho_Descricao3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,80);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_descricao3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAreatrabalho_descricao3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrganizacao_nome3_Internalname, StringUtil.RTrim( AV131Organizacao_Nome3), StringUtil.RTrim( context.localUtil.Format( AV131Organizacao_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrganizacao_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavOrganizacao_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAreatrabalho_ativo3, cmbavAreatrabalho_ativo3_Internalname, StringUtil.RTrim( AV132AreaTrabalho_Ativo3), 1, cmbavAreatrabalho_ativo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavAreatrabalho_ativo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", "", true, "HLP_WWAreaTrabalho.htm");
            cmbavAreatrabalho_ativo3.CurrentValue = StringUtil.RTrim( AV132AreaTrabalho_Ativo3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo3_Internalname, "Values", (String)(cmbavAreatrabalho_ativo3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_75_0T2e( true) ;
         }
         else
         {
            wb_table10_75_0T2e( false) ;
         }
      }

      protected void wb_table9_56_0T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_WWAreaTrabalho.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_descricao2_Internalname, AV21AreaTrabalho_Descricao2, StringUtil.RTrim( context.localUtil.Format( AV21AreaTrabalho_Descricao2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_descricao2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAreatrabalho_descricao2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrganizacao_nome2_Internalname, StringUtil.RTrim( AV92Organizacao_Nome2), StringUtil.RTrim( context.localUtil.Format( AV92Organizacao_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrganizacao_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavOrganizacao_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAreatrabalho_ativo2, cmbavAreatrabalho_ativo2_Internalname, StringUtil.RTrim( AV130AreaTrabalho_Ativo2), 1, cmbavAreatrabalho_ativo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavAreatrabalho_ativo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "", true, "HLP_WWAreaTrabalho.htm");
            cmbavAreatrabalho_ativo2.CurrentValue = StringUtil.RTrim( AV130AreaTrabalho_Ativo2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo2_Internalname, "Values", (String)(cmbavAreatrabalho_ativo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_56_0T2e( true) ;
         }
         else
         {
            wb_table9_56_0T2e( false) ;
         }
      }

      protected void wb_table8_37_0T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWAreaTrabalho.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_descricao1_Internalname, AV17AreaTrabalho_Descricao1, StringUtil.RTrim( context.localUtil.Format( AV17AreaTrabalho_Descricao1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_descricao1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavAreatrabalho_descricao1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrganizacao_nome1_Internalname, StringUtil.RTrim( AV90Organizacao_Nome1), StringUtil.RTrim( context.localUtil.Format( AV90Organizacao_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrganizacao_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavOrganizacao_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWAreaTrabalho.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAreatrabalho_ativo1, cmbavAreatrabalho_ativo1_Internalname, StringUtil.RTrim( AV129AreaTrabalho_Ativo1), 1, cmbavAreatrabalho_ativo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavAreatrabalho_ativo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_WWAreaTrabalho.htm");
            cmbavAreatrabalho_ativo1.CurrentValue = StringUtil.RTrim( AV129AreaTrabalho_Ativo1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAreatrabalho_ativo1_Internalname, "Values", (String)(cmbavAreatrabalho_ativo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_37_0T2e( true) ;
         }
         else
         {
            wb_table8_37_0T2e( false) ;
         }
      }

      protected void wb_table5_11_0T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAreatrabalhotitle_Internalname, "�reas de Trabalho", "", "", lblAreatrabalhotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", imgInsert_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_94_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWAreaTrabalho.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_94_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWAreaTrabalho.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_0T2e( true) ;
         }
         else
         {
            wb_table5_11_0T2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0T2( ) ;
         WS0T2( ) ;
         WE0T2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020312049498");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwareatrabalho.js", "?2020312049498");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_942( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_94_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_94_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_94_idx;
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO_"+sGXsfl_94_idx;
         edtContratante_Codigo_Internalname = "CONTRATANTE_CODIGO_"+sGXsfl_94_idx;
         edtAreaTrabalho_Descricao_Internalname = "AREATRABALHO_DESCRICAO_"+sGXsfl_94_idx;
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL_"+sGXsfl_94_idx;
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ_"+sGXsfl_94_idx;
         edtContratante_Telefone_Internalname = "CONTRATANTE_TELEFONE_"+sGXsfl_94_idx;
         cmbAreaTrabalho_ValidaOSFM_Internalname = "AREATRABALHO_VALIDAOSFM_"+sGXsfl_94_idx;
         edtAreaTrabalho_DiasParaPagar_Internalname = "AREATRABALHO_DIASPARAPAGAR_"+sGXsfl_94_idx;
         edtAreaTrabalho_OrganizacaoCod_Internalname = "AREATRABALHO_ORGANIZACAOCOD_"+sGXsfl_94_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_94_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_94_idx;
         cmbAreaTrabalho_CalculoPFinal_Internalname = "AREATRABALHO_CALCULOPFINAL_"+sGXsfl_94_idx;
         cmbAreaTrabalho_VerTA_Internalname = "AREATRABALHO_VERTA_"+sGXsfl_94_idx;
         chkAreaTrabalho_Ativo_Internalname = "AREATRABALHO_ATIVO_"+sGXsfl_94_idx;
         edtavSelecionar_Internalname = "vSELECIONAR_"+sGXsfl_94_idx;
         edtContratante_NomeFantasia_Internalname = "CONTRATANTE_NOMEFANTASIA_"+sGXsfl_94_idx;
         edtAreaTrabalho_ServicoPadrao_Internalname = "AREATRABALHO_SERVICOPADRAO_"+sGXsfl_94_idx;
      }

      protected void SubsflControlProps_fel_942( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_94_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_94_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_94_fel_idx;
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO_"+sGXsfl_94_fel_idx;
         edtContratante_Codigo_Internalname = "CONTRATANTE_CODIGO_"+sGXsfl_94_fel_idx;
         edtAreaTrabalho_Descricao_Internalname = "AREATRABALHO_DESCRICAO_"+sGXsfl_94_fel_idx;
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL_"+sGXsfl_94_fel_idx;
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ_"+sGXsfl_94_fel_idx;
         edtContratante_Telefone_Internalname = "CONTRATANTE_TELEFONE_"+sGXsfl_94_fel_idx;
         cmbAreaTrabalho_ValidaOSFM_Internalname = "AREATRABALHO_VALIDAOSFM_"+sGXsfl_94_fel_idx;
         edtAreaTrabalho_DiasParaPagar_Internalname = "AREATRABALHO_DIASPARAPAGAR_"+sGXsfl_94_fel_idx;
         edtAreaTrabalho_OrganizacaoCod_Internalname = "AREATRABALHO_ORGANIZACAOCOD_"+sGXsfl_94_fel_idx;
         edtEstado_UF_Internalname = "ESTADO_UF_"+sGXsfl_94_fel_idx;
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME_"+sGXsfl_94_fel_idx;
         cmbAreaTrabalho_CalculoPFinal_Internalname = "AREATRABALHO_CALCULOPFINAL_"+sGXsfl_94_fel_idx;
         cmbAreaTrabalho_VerTA_Internalname = "AREATRABALHO_VERTA_"+sGXsfl_94_fel_idx;
         chkAreaTrabalho_Ativo_Internalname = "AREATRABALHO_ATIVO_"+sGXsfl_94_fel_idx;
         edtavSelecionar_Internalname = "vSELECIONAR_"+sGXsfl_94_fel_idx;
         edtContratante_NomeFantasia_Internalname = "CONTRATANTE_NOMEFANTASIA_"+sGXsfl_94_fel_idx;
         edtAreaTrabalho_ServicoPadrao_Internalname = "AREATRABALHO_SERVICOPADRAO_"+sGXsfl_94_fel_idx;
      }

      protected void sendrow_942( )
      {
         SubsflControlProps_942( ) ;
         WB0T0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_94_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_94_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_94_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV184Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV184Update_GXI : context.PathToRelativeUrl( AV29Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV185Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)) ? AV185Delete_GXI : context.PathToRelativeUrl( AV28Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV94Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV94Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV186Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV94Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV94Display)) ? AV186Display_GXI : context.PathToRelativeUrl( AV94Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV94Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Contratante_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_Descricao_Internalname,(String)A6AreaTrabalho_Descricao,StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Descricao",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_RazaoSocial_Internalname,StringUtil.RTrim( A9Contratante_RazaoSocial),StringUtil.RTrim( context.localUtil.Format( A9Contratante_RazaoSocial, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtContratante_RazaoSocial_Link,(String)"",(String)"",(String)"",(String)edtContratante_RazaoSocial_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_CNPJ_Internalname,(String)A12Contratante_CNPJ,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_CNPJ_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Docto",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            if ( context.isSmartDevice( ) )
            {
               gxphoneLink = "tel:" + StringUtil.RTrim( A31Contratante_Telefone);
            }
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_Telefone_Internalname,StringUtil.RTrim( A31Contratante_Telefone),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)gxphoneLink,(String)"",(String)"",(String)"",(String)edtContratante_Telefone_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"tel",(String)"",(short)110,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Phone",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_94_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "AREATRABALHO_VALIDAOSFM_" + sGXsfl_94_idx;
               cmbAreaTrabalho_ValidaOSFM.Name = GXCCtl;
               cmbAreaTrabalho_ValidaOSFM.WebTags = "";
               cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( true), "Sim", 0);
               cmbAreaTrabalho_ValidaOSFM.addItem(StringUtil.BoolToStr( false), "N�o", 0);
               if ( cmbAreaTrabalho_ValidaOSFM.ItemCount > 0 )
               {
                  A834AreaTrabalho_ValidaOSFM = StringUtil.StrToBool( cmbAreaTrabalho_ValidaOSFM.getValidValue(StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM)));
                  n834AreaTrabalho_ValidaOSFM = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAreaTrabalho_ValidaOSFM,(String)cmbAreaTrabalho_ValidaOSFM_Internalname,StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM),(short)1,(String)cmbAreaTrabalho_ValidaOSFM_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"boolean",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbAreaTrabalho_ValidaOSFM.CurrentValue = StringUtil.BoolToStr( A834AreaTrabalho_ValidaOSFM);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_ValidaOSFM_Internalname, "Values", (String)(cmbAreaTrabalho_ValidaOSFM.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_DiasParaPagar_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A855AreaTrabalho_DiasParaPagar), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_DiasParaPagar_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)93,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_OrganizacaoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1216AreaTrabalho_OrganizacaoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_OrganizacaoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_UF_Internalname,StringUtil.RTrim( A23Estado_UF),StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEstado_UF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"UF",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtMunicipio_Nome_Internalname,StringUtil.RTrim( A26Municipio_Nome),StringUtil.RTrim( context.localUtil.Format( A26Municipio_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtMunicipio_Nome_Link,(String)"",(String)"",(String)"",(String)edtMunicipio_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_94_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "AREATRABALHO_CALCULOPFINAL_" + sGXsfl_94_idx;
               cmbAreaTrabalho_CalculoPFinal.Name = GXCCtl;
               cmbAreaTrabalho_CalculoPFinal.WebTags = "";
               cmbAreaTrabalho_CalculoPFinal.addItem("MB", "Menor PFB", 0);
               cmbAreaTrabalho_CalculoPFinal.addItem("BM", "PFB FM", 0);
               if ( cmbAreaTrabalho_CalculoPFinal.ItemCount > 0 )
               {
                  A642AreaTrabalho_CalculoPFinal = cmbAreaTrabalho_CalculoPFinal.getValidValue(A642AreaTrabalho_CalculoPFinal);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAreaTrabalho_CalculoPFinal,(String)cmbAreaTrabalho_CalculoPFinal_Internalname,StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal),(short)1,(String)cmbAreaTrabalho_CalculoPFinal_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbAreaTrabalho_CalculoPFinal.CurrentValue = StringUtil.RTrim( A642AreaTrabalho_CalculoPFinal);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_CalculoPFinal_Internalname, "Values", (String)(cmbAreaTrabalho_CalculoPFinal.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_94_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "AREATRABALHO_VERTA_" + sGXsfl_94_idx;
               cmbAreaTrabalho_VerTA.Name = GXCCtl;
               cmbAreaTrabalho_VerTA.WebTags = "";
               cmbAreaTrabalho_VerTA.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "Padr�o", 0);
               cmbAreaTrabalho_VerTA.addItem("1", "Vers�o 1", 0);
               if ( cmbAreaTrabalho_VerTA.ItemCount > 0 )
               {
                  A2081AreaTrabalho_VerTA = (short)(NumberUtil.Val( cmbAreaTrabalho_VerTA.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0))), "."));
                  n2081AreaTrabalho_VerTA = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAreaTrabalho_VerTA,(String)cmbAreaTrabalho_VerTA_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0)),(short)1,(String)cmbAreaTrabalho_VerTA_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbAreaTrabalho_VerTA.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2081AreaTrabalho_VerTA), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAreaTrabalho_VerTA_Internalname, "Values", (String)(cmbAreaTrabalho_VerTA.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAreaTrabalho_Ativo_Internalname,StringUtil.BoolToStr( A72AreaTrabalho_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavSelecionar_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelecionar_Enabled!=0)&&(edtavSelecionar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 112,'',false,'',94)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV141Selecionar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV141Selecionar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV187Selecionar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV141Selecionar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelecionar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV141Selecionar)) ? AV187Selecionar_GXI : context.PathToRelativeUrl( AV141Selecionar)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavSelecionar_Visible,(short)1,(String)"",(String)edtavSelecionar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelecionar_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVSELECIONAR.CLICK."+sGXsfl_94_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV141Selecionar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratante_NomeFantasia_Internalname,StringUtil.RTrim( A10Contratante_NomeFantasia),StringUtil.RTrim( context.localUtil.Format( A10Contratante_NomeFantasia, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratante_NomeFantasia_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_ServicoPadrao_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A830AreaTrabalho_ServicoPadrao), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A830AreaTrabalho_ServicoPadrao), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_ServicoPadrao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)94,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CODIGO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATANTE_CODIGO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A29Contratante_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_DESCRICAO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A6AreaTrabalho_Descricao, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_VALIDAOSFM"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, A834AreaTrabalho_ValidaOSFM));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_DIASPARAPAGAR"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A855AreaTrabalho_DiasParaPagar), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_ORGANIZACAOCOD"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A1216AreaTrabalho_OrganizacaoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CALCULOPFINAL"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, StringUtil.RTrim( context.localUtil.Format( A642AreaTrabalho_CalculoPFinal, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_VERTA"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A2081AreaTrabalho_VerTA), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_ATIVO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, A72AreaTrabalho_Ativo));
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_SERVICOPADRAO"+"_"+sGXsfl_94_idx, GetSecureSignedToken( sGXsfl_94_idx, context.localUtil.Format( (decimal)(A830AreaTrabalho_ServicoPadrao), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_94_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_94_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_94_idx+1));
            sGXsfl_94_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_94_idx), 4, 0)), 4, "0");
            SubsflControlProps_942( ) ;
         }
         /* End function sendrow_942 */
      }

      protected void init_default_properties( )
      {
         lblAreatrabalhotitle_Internalname = "AREATRABALHOTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavAreatrabalho_descricao1_Internalname = "vAREATRABALHO_DESCRICAO1";
         edtavOrganizacao_nome1_Internalname = "vORGANIZACAO_NOME1";
         cmbavAreatrabalho_ativo1_Internalname = "vAREATRABALHO_ATIVO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavAreatrabalho_descricao2_Internalname = "vAREATRABALHO_DESCRICAO2";
         edtavOrganizacao_nome2_Internalname = "vORGANIZACAO_NOME2";
         cmbavAreatrabalho_ativo2_Internalname = "vAREATRABALHO_ATIVO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavAreatrabalho_descricao3_Internalname = "vAREATRABALHO_DESCRICAO3";
         edtavOrganizacao_nome3_Internalname = "vORGANIZACAO_NOME3";
         cmbavAreatrabalho_ativo3_Internalname = "vAREATRABALHO_ATIVO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO";
         edtContratante_Codigo_Internalname = "CONTRATANTE_CODIGO";
         edtAreaTrabalho_Descricao_Internalname = "AREATRABALHO_DESCRICAO";
         edtContratante_RazaoSocial_Internalname = "CONTRATANTE_RAZAOSOCIAL";
         edtContratante_CNPJ_Internalname = "CONTRATANTE_CNPJ";
         edtContratante_Telefone_Internalname = "CONTRATANTE_TELEFONE";
         cmbAreaTrabalho_ValidaOSFM_Internalname = "AREATRABALHO_VALIDAOSFM";
         edtAreaTrabalho_DiasParaPagar_Internalname = "AREATRABALHO_DIASPARAPAGAR";
         edtAreaTrabalho_OrganizacaoCod_Internalname = "AREATRABALHO_ORGANIZACAOCOD";
         edtEstado_UF_Internalname = "ESTADO_UF";
         edtMunicipio_Nome_Internalname = "MUNICIPIO_NOME";
         cmbAreaTrabalho_CalculoPFinal_Internalname = "AREATRABALHO_CALCULOPFINAL";
         cmbAreaTrabalho_VerTA_Internalname = "AREATRABALHO_VERTA";
         chkAreaTrabalho_Ativo_Internalname = "AREATRABALHO_ATIVO";
         edtavSelecionar_Internalname = "vSELECIONAR";
         edtContratante_NomeFantasia_Internalname = "CONTRATANTE_NOMEFANTASIA";
         edtAreaTrabalho_ServicoPadrao_Internalname = "AREATRABALHO_SERVICOPADRAO";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfareatrabalho_descricao_Internalname = "vTFAREATRABALHO_DESCRICAO";
         edtavTfareatrabalho_descricao_sel_Internalname = "vTFAREATRABALHO_DESCRICAO_SEL";
         edtavTfcontratante_razaosocial_Internalname = "vTFCONTRATANTE_RAZAOSOCIAL";
         edtavTfcontratante_razaosocial_sel_Internalname = "vTFCONTRATANTE_RAZAOSOCIAL_SEL";
         edtavTfcontratante_cnpj_Internalname = "vTFCONTRATANTE_CNPJ";
         edtavTfcontratante_cnpj_sel_Internalname = "vTFCONTRATANTE_CNPJ_SEL";
         edtavTfcontratante_telefone_Internalname = "vTFCONTRATANTE_TELEFONE";
         edtavTfcontratante_telefone_sel_Internalname = "vTFCONTRATANTE_TELEFONE_SEL";
         edtavTfareatrabalho_validaosfm_sel_Internalname = "vTFAREATRABALHO_VALIDAOSFM_SEL";
         edtavTfareatrabalho_diasparapagar_Internalname = "vTFAREATRABALHO_DIASPARAPAGAR";
         edtavTfareatrabalho_diasparapagar_to_Internalname = "vTFAREATRABALHO_DIASPARAPAGAR_TO";
         edtavTfestado_uf_Internalname = "vTFESTADO_UF";
         edtavTfestado_uf_sel_Internalname = "vTFESTADO_UF_SEL";
         edtavTfmunicipio_nome_Internalname = "vTFMUNICIPIO_NOME";
         edtavTfmunicipio_nome_sel_Internalname = "vTFMUNICIPIO_NOME_SEL";
         edtavTfareatrabalho_ativo_sel_Internalname = "vTFAREATRABALHO_ATIVO_SEL";
         Ddo_areatrabalho_descricao_Internalname = "DDO_AREATRABALHO_DESCRICAO";
         edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Internalname = "vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_contratante_razaosocial_Internalname = "DDO_CONTRATANTE_RAZAOSOCIAL";
         edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE";
         Ddo_contratante_cnpj_Internalname = "DDO_CONTRATANTE_CNPJ";
         edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE";
         Ddo_contratante_telefone_Internalname = "DDO_CONTRATANTE_TELEFONE";
         edtavDdo_contratante_telefonetitlecontrolidtoreplace_Internalname = "vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE";
         Ddo_areatrabalho_validaosfm_Internalname = "DDO_AREATRABALHO_VALIDAOSFM";
         edtavDdo_areatrabalho_validaosfmtitlecontrolidtoreplace_Internalname = "vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE";
         Ddo_areatrabalho_diasparapagar_Internalname = "DDO_AREATRABALHO_DIASPARAPAGAR";
         edtavDdo_areatrabalho_diasparapagartitlecontrolidtoreplace_Internalname = "vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE";
         Ddo_estado_uf_Internalname = "DDO_ESTADO_UF";
         edtavDdo_estado_uftitlecontrolidtoreplace_Internalname = "vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE";
         Ddo_municipio_nome_Internalname = "DDO_MUNICIPIO_NOME";
         edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname = "vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_areatrabalho_calculopfinal_Internalname = "DDO_AREATRABALHO_CALCULOPFINAL";
         edtavDdo_areatrabalho_calculopfinaltitlecontrolidtoreplace_Internalname = "vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE";
         Ddo_areatrabalho_verta_Internalname = "DDO_AREATRABALHO_VERTA";
         edtavDdo_areatrabalho_vertatitlecontrolidtoreplace_Internalname = "vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE";
         Ddo_areatrabalho_ativo_Internalname = "DDO_AREATRABALHO_ATIVO";
         edtavDdo_areatrabalho_ativotitlecontrolidtoreplace_Internalname = "vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtAreaTrabalho_ServicoPadrao_Jsonclick = "";
         edtContratante_NomeFantasia_Jsonclick = "";
         edtavSelecionar_Jsonclick = "";
         edtavSelecionar_Enabled = 1;
         cmbAreaTrabalho_VerTA_Jsonclick = "";
         cmbAreaTrabalho_CalculoPFinal_Jsonclick = "";
         edtMunicipio_Nome_Jsonclick = "";
         edtEstado_UF_Jsonclick = "";
         edtAreaTrabalho_OrganizacaoCod_Jsonclick = "";
         edtAreaTrabalho_DiasParaPagar_Jsonclick = "";
         cmbAreaTrabalho_ValidaOSFM_Jsonclick = "";
         edtContratante_Telefone_Jsonclick = "";
         edtContratante_CNPJ_Jsonclick = "";
         edtContratante_RazaoSocial_Jsonclick = "";
         edtAreaTrabalho_Descricao_Jsonclick = "";
         edtContratante_Codigo_Jsonclick = "";
         edtAreaTrabalho_Codigo_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         cmbavAreatrabalho_ativo1_Jsonclick = "";
         edtavOrganizacao_nome1_Jsonclick = "";
         edtavAreatrabalho_descricao1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         cmbavAreatrabalho_ativo2_Jsonclick = "";
         edtavOrganizacao_nome2_Jsonclick = "";
         edtavAreatrabalho_descricao2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavAreatrabalho_ativo3_Jsonclick = "";
         edtavOrganizacao_nome3_Jsonclick = "";
         edtavAreatrabalho_descricao3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelecionar_Tooltiptext = "Selecionar �rea de trabalho";
         edtMunicipio_Nome_Link = "";
         edtContratante_RazaoSocial_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavSelecionar_Visible = -1;
         chkAreaTrabalho_Ativo_Titleformat = 0;
         cmbAreaTrabalho_VerTA_Titleformat = 0;
         cmbAreaTrabalho_CalculoPFinal_Titleformat = 0;
         edtMunicipio_Nome_Titleformat = 0;
         edtEstado_UF_Titleformat = 0;
         edtAreaTrabalho_DiasParaPagar_Titleformat = 0;
         cmbAreaTrabalho_ValidaOSFM_Titleformat = 0;
         edtContratante_Telefone_Titleformat = 0;
         edtContratante_CNPJ_Titleformat = 0;
         edtContratante_RazaoSocial_Titleformat = 0;
         edtAreaTrabalho_Descricao_Titleformat = 0;
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         cmbavAreatrabalho_ativo3.Visible = 1;
         edtavOrganizacao_nome3_Visible = 1;
         edtavAreatrabalho_descricao3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         cmbavAreatrabalho_ativo2.Visible = 1;
         edtavOrganizacao_nome2_Visible = 1;
         edtavAreatrabalho_descricao2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         cmbavAreatrabalho_ativo1.Visible = 1;
         edtavOrganizacao_nome1_Visible = 1;
         edtavAreatrabalho_descricao1_Visible = 1;
         imgInsert_Tooltiptext = "Inserir";
         chkAreaTrabalho_Ativo.Title.Text = "Ativo?";
         cmbAreaTrabalho_VerTA.Title.Text = "Vers�o do TA";
         cmbAreaTrabalho_CalculoPFinal.Title.Text = "Calculo P. Final";
         edtMunicipio_Nome_Title = "Munic�pio";
         edtEstado_UF_Title = "Estado";
         edtAreaTrabalho_DiasParaPagar_Title = "Dias para pagar";
         cmbAreaTrabalho_ValidaOSFM.Title.Text = "Valida OS FM";
         edtContratante_Telefone_Title = "Telefone";
         edtContratante_CNPJ_Title = "CNPJ";
         edtContratante_RazaoSocial_Title = "Contratante";
         edtAreaTrabalho_Descricao_Title = "Descri��o";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkAreaTrabalho_Ativo.Caption = "";
         edtavDdo_areatrabalho_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_areatrabalho_vertatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_areatrabalho_calculopfinaltitlecontrolidtoreplace_Visible = 1;
         edtavDdo_municipio_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_estado_uftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_areatrabalho_diasparapagartitlecontrolidtoreplace_Visible = 1;
         edtavDdo_areatrabalho_validaosfmtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratante_telefonetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavTfareatrabalho_ativo_sel_Jsonclick = "";
         edtavTfareatrabalho_ativo_sel_Visible = 1;
         edtavTfmunicipio_nome_sel_Jsonclick = "";
         edtavTfmunicipio_nome_sel_Visible = 1;
         edtavTfmunicipio_nome_Jsonclick = "";
         edtavTfmunicipio_nome_Visible = 1;
         edtavTfestado_uf_sel_Jsonclick = "";
         edtavTfestado_uf_sel_Visible = 1;
         edtavTfestado_uf_Jsonclick = "";
         edtavTfestado_uf_Visible = 1;
         edtavTfareatrabalho_diasparapagar_to_Jsonclick = "";
         edtavTfareatrabalho_diasparapagar_to_Visible = 1;
         edtavTfareatrabalho_diasparapagar_Jsonclick = "";
         edtavTfareatrabalho_diasparapagar_Visible = 1;
         edtavTfareatrabalho_validaosfm_sel_Jsonclick = "";
         edtavTfareatrabalho_validaosfm_sel_Visible = 1;
         edtavTfcontratante_telefone_sel_Jsonclick = "";
         edtavTfcontratante_telefone_sel_Visible = 1;
         edtavTfcontratante_telefone_Jsonclick = "";
         edtavTfcontratante_telefone_Visible = 1;
         edtavTfcontratante_cnpj_sel_Jsonclick = "";
         edtavTfcontratante_cnpj_sel_Visible = 1;
         edtavTfcontratante_cnpj_Jsonclick = "";
         edtavTfcontratante_cnpj_Visible = 1;
         edtavTfcontratante_razaosocial_sel_Jsonclick = "";
         edtavTfcontratante_razaosocial_sel_Visible = 1;
         edtavTfcontratante_razaosocial_Jsonclick = "";
         edtavTfcontratante_razaosocial_Visible = 1;
         edtavTfareatrabalho_descricao_sel_Jsonclick = "";
         edtavTfareatrabalho_descricao_sel_Visible = 1;
         edtavTfareatrabalho_descricao_Jsonclick = "";
         edtavTfareatrabalho_descricao_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_areatrabalho_ativo_Searchbuttontext = "Pesquisar";
         Ddo_areatrabalho_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_areatrabalho_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_areatrabalho_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_areatrabalho_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_areatrabalho_ativo_Datalisttype = "FixedValues";
         Ddo_areatrabalho_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_areatrabalho_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_areatrabalho_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_ativo_Titlecontrolidtoreplace = "";
         Ddo_areatrabalho_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_areatrabalho_ativo_Cls = "ColumnSettings";
         Ddo_areatrabalho_ativo_Tooltip = "Op��es";
         Ddo_areatrabalho_ativo_Caption = "";
         Ddo_areatrabalho_verta_Searchbuttontext = "Filtrar Selecionados";
         Ddo_areatrabalho_verta_Cleanfilter = "Limpar pesquisa";
         Ddo_areatrabalho_verta_Sortdsc = "Ordenar de Z � A";
         Ddo_areatrabalho_verta_Sortasc = "Ordenar de A � Z";
         Ddo_areatrabalho_verta_Datalistfixedvalues = "1:Vers�o 1";
         Ddo_areatrabalho_verta_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_areatrabalho_verta_Datalisttype = "FixedValues";
         Ddo_areatrabalho_verta_Includedatalist = Convert.ToBoolean( -1);
         Ddo_areatrabalho_verta_Includefilter = Convert.ToBoolean( 0);
         Ddo_areatrabalho_verta_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_verta_Includesortasc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_verta_Titlecontrolidtoreplace = "";
         Ddo_areatrabalho_verta_Dropdownoptionstype = "GridTitleSettings";
         Ddo_areatrabalho_verta_Cls = "ColumnSettings";
         Ddo_areatrabalho_verta_Tooltip = "Op��es";
         Ddo_areatrabalho_verta_Caption = "";
         Ddo_areatrabalho_calculopfinal_Searchbuttontext = "Filtrar Selecionados";
         Ddo_areatrabalho_calculopfinal_Cleanfilter = "Limpar pesquisa";
         Ddo_areatrabalho_calculopfinal_Sortdsc = "Ordenar de Z � A";
         Ddo_areatrabalho_calculopfinal_Sortasc = "Ordenar de A � Z";
         Ddo_areatrabalho_calculopfinal_Datalistfixedvalues = "MB:Menor PFB,BM:PFB FM";
         Ddo_areatrabalho_calculopfinal_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_areatrabalho_calculopfinal_Datalisttype = "FixedValues";
         Ddo_areatrabalho_calculopfinal_Includedatalist = Convert.ToBoolean( -1);
         Ddo_areatrabalho_calculopfinal_Includefilter = Convert.ToBoolean( 0);
         Ddo_areatrabalho_calculopfinal_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_calculopfinal_Includesortasc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_calculopfinal_Titlecontrolidtoreplace = "";
         Ddo_areatrabalho_calculopfinal_Dropdownoptionstype = "GridTitleSettings";
         Ddo_areatrabalho_calculopfinal_Cls = "ColumnSettings";
         Ddo_areatrabalho_calculopfinal_Tooltip = "Op��es";
         Ddo_areatrabalho_calculopfinal_Caption = "";
         Ddo_municipio_nome_Searchbuttontext = "Pesquisar";
         Ddo_municipio_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_municipio_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_municipio_nome_Loadingdata = "Carregando dados...";
         Ddo_municipio_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_municipio_nome_Sortasc = "Ordenar de A � Z";
         Ddo_municipio_nome_Datalistupdateminimumcharacters = 0;
         Ddo_municipio_nome_Datalistproc = "GetWWAreaTrabalhoFilterData";
         Ddo_municipio_nome_Datalisttype = "Dynamic";
         Ddo_municipio_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_municipio_nome_Filtertype = "Character";
         Ddo_municipio_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_municipio_nome_Titlecontrolidtoreplace = "";
         Ddo_municipio_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_municipio_nome_Cls = "ColumnSettings";
         Ddo_municipio_nome_Tooltip = "Op��es";
         Ddo_municipio_nome_Caption = "";
         Ddo_estado_uf_Searchbuttontext = "Pesquisar";
         Ddo_estado_uf_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_estado_uf_Cleanfilter = "Limpar pesquisa";
         Ddo_estado_uf_Loadingdata = "Carregando dados...";
         Ddo_estado_uf_Sortdsc = "Ordenar de Z � A";
         Ddo_estado_uf_Sortasc = "Ordenar de A � Z";
         Ddo_estado_uf_Datalistupdateminimumcharacters = 0;
         Ddo_estado_uf_Datalistproc = "GetWWAreaTrabalhoFilterData";
         Ddo_estado_uf_Datalisttype = "Dynamic";
         Ddo_estado_uf_Includedatalist = Convert.ToBoolean( -1);
         Ddo_estado_uf_Filterisrange = Convert.ToBoolean( 0);
         Ddo_estado_uf_Filtertype = "Character";
         Ddo_estado_uf_Includefilter = Convert.ToBoolean( -1);
         Ddo_estado_uf_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_estado_uf_Includesortasc = Convert.ToBoolean( -1);
         Ddo_estado_uf_Titlecontrolidtoreplace = "";
         Ddo_estado_uf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_estado_uf_Cls = "ColumnSettings";
         Ddo_estado_uf_Tooltip = "Op��es";
         Ddo_estado_uf_Caption = "";
         Ddo_areatrabalho_diasparapagar_Searchbuttontext = "Pesquisar";
         Ddo_areatrabalho_diasparapagar_Rangefilterto = "At�";
         Ddo_areatrabalho_diasparapagar_Rangefilterfrom = "Desde";
         Ddo_areatrabalho_diasparapagar_Cleanfilter = "Limpar pesquisa";
         Ddo_areatrabalho_diasparapagar_Sortdsc = "Ordenar de Z � A";
         Ddo_areatrabalho_diasparapagar_Sortasc = "Ordenar de A � Z";
         Ddo_areatrabalho_diasparapagar_Includedatalist = Convert.ToBoolean( 0);
         Ddo_areatrabalho_diasparapagar_Filterisrange = Convert.ToBoolean( -1);
         Ddo_areatrabalho_diasparapagar_Filtertype = "Numeric";
         Ddo_areatrabalho_diasparapagar_Includefilter = Convert.ToBoolean( -1);
         Ddo_areatrabalho_diasparapagar_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_diasparapagar_Includesortasc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_diasparapagar_Titlecontrolidtoreplace = "";
         Ddo_areatrabalho_diasparapagar_Dropdownoptionstype = "GridTitleSettings";
         Ddo_areatrabalho_diasparapagar_Cls = "ColumnSettings";
         Ddo_areatrabalho_diasparapagar_Tooltip = "Op��es";
         Ddo_areatrabalho_diasparapagar_Caption = "";
         Ddo_areatrabalho_validaosfm_Searchbuttontext = "Pesquisar";
         Ddo_areatrabalho_validaosfm_Cleanfilter = "Limpar pesquisa";
         Ddo_areatrabalho_validaosfm_Sortdsc = "Ordenar de Z � A";
         Ddo_areatrabalho_validaosfm_Sortasc = "Ordenar de A � Z";
         Ddo_areatrabalho_validaosfm_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_areatrabalho_validaosfm_Datalisttype = "FixedValues";
         Ddo_areatrabalho_validaosfm_Includedatalist = Convert.ToBoolean( -1);
         Ddo_areatrabalho_validaosfm_Includefilter = Convert.ToBoolean( 0);
         Ddo_areatrabalho_validaosfm_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_validaosfm_Includesortasc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_validaosfm_Titlecontrolidtoreplace = "";
         Ddo_areatrabalho_validaosfm_Dropdownoptionstype = "GridTitleSettings";
         Ddo_areatrabalho_validaosfm_Cls = "ColumnSettings";
         Ddo_areatrabalho_validaosfm_Tooltip = "Op��es";
         Ddo_areatrabalho_validaosfm_Caption = "";
         Ddo_contratante_telefone_Searchbuttontext = "Pesquisar";
         Ddo_contratante_telefone_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratante_telefone_Cleanfilter = "Limpar pesquisa";
         Ddo_contratante_telefone_Loadingdata = "Carregando dados...";
         Ddo_contratante_telefone_Sortdsc = "Ordenar de Z � A";
         Ddo_contratante_telefone_Sortasc = "Ordenar de A � Z";
         Ddo_contratante_telefone_Datalistupdateminimumcharacters = 0;
         Ddo_contratante_telefone_Datalistproc = "GetWWAreaTrabalhoFilterData";
         Ddo_contratante_telefone_Datalisttype = "Dynamic";
         Ddo_contratante_telefone_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratante_telefone_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratante_telefone_Filtertype = "Character";
         Ddo_contratante_telefone_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratante_telefone_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratante_telefone_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratante_telefone_Titlecontrolidtoreplace = "";
         Ddo_contratante_telefone_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratante_telefone_Cls = "ColumnSettings";
         Ddo_contratante_telefone_Tooltip = "Op��es";
         Ddo_contratante_telefone_Caption = "";
         Ddo_contratante_cnpj_Searchbuttontext = "Pesquisar";
         Ddo_contratante_cnpj_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratante_cnpj_Cleanfilter = "Limpar pesquisa";
         Ddo_contratante_cnpj_Loadingdata = "Carregando dados...";
         Ddo_contratante_cnpj_Sortdsc = "Ordenar de Z � A";
         Ddo_contratante_cnpj_Sortasc = "Ordenar de A � Z";
         Ddo_contratante_cnpj_Datalistupdateminimumcharacters = 0;
         Ddo_contratante_cnpj_Datalistproc = "GetWWAreaTrabalhoFilterData";
         Ddo_contratante_cnpj_Datalisttype = "Dynamic";
         Ddo_contratante_cnpj_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratante_cnpj_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratante_cnpj_Filtertype = "Character";
         Ddo_contratante_cnpj_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratante_cnpj_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratante_cnpj_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratante_cnpj_Titlecontrolidtoreplace = "";
         Ddo_contratante_cnpj_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratante_cnpj_Cls = "ColumnSettings";
         Ddo_contratante_cnpj_Tooltip = "Op��es";
         Ddo_contratante_cnpj_Caption = "";
         Ddo_contratante_razaosocial_Searchbuttontext = "Pesquisar";
         Ddo_contratante_razaosocial_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratante_razaosocial_Cleanfilter = "Limpar pesquisa";
         Ddo_contratante_razaosocial_Loadingdata = "Carregando dados...";
         Ddo_contratante_razaosocial_Sortdsc = "Ordenar de Z � A";
         Ddo_contratante_razaosocial_Sortasc = "Ordenar de A � Z";
         Ddo_contratante_razaosocial_Datalistupdateminimumcharacters = 0;
         Ddo_contratante_razaosocial_Datalistproc = "GetWWAreaTrabalhoFilterData";
         Ddo_contratante_razaosocial_Datalisttype = "Dynamic";
         Ddo_contratante_razaosocial_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratante_razaosocial_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratante_razaosocial_Filtertype = "Character";
         Ddo_contratante_razaosocial_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratante_razaosocial_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratante_razaosocial_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratante_razaosocial_Titlecontrolidtoreplace = "";
         Ddo_contratante_razaosocial_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratante_razaosocial_Cls = "ColumnSettings";
         Ddo_contratante_razaosocial_Tooltip = "Op��es";
         Ddo_contratante_razaosocial_Caption = "";
         Ddo_areatrabalho_descricao_Searchbuttontext = "Pesquisar";
         Ddo_areatrabalho_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_areatrabalho_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_areatrabalho_descricao_Loadingdata = "Carregando dados...";
         Ddo_areatrabalho_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_areatrabalho_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_areatrabalho_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_areatrabalho_descricao_Datalistproc = "GetWWAreaTrabalhoFilterData";
         Ddo_areatrabalho_descricao_Datalisttype = "Dynamic";
         Ddo_areatrabalho_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_areatrabalho_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_areatrabalho_descricao_Filtertype = "Character";
         Ddo_areatrabalho_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_areatrabalho_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_areatrabalho_descricao_Titlecontrolidtoreplace = "";
         Ddo_areatrabalho_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_areatrabalho_descricao_Cls = "ColumnSettings";
         Ddo_areatrabalho_descricao_Tooltip = "Op��es";
         Ddo_areatrabalho_descricao_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " �rea de Trabalho";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV60AreaTrabalho_DescricaoTitleFilterData',fld:'vAREATRABALHO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV64Contratante_RazaoSocialTitleFilterData',fld:'vCONTRATANTE_RAZAOSOCIALTITLEFILTERDATA',pic:'',nv:null},{av:'AV68Contratante_CNPJTitleFilterData',fld:'vCONTRATANTE_CNPJTITLEFILTERDATA',pic:'',nv:null},{av:'AV72Contratante_TelefoneTitleFilterData',fld:'vCONTRATANTE_TELEFONETITLEFILTERDATA',pic:'',nv:null},{av:'AV76AreaTrabalho_ValidaOSFMTitleFilterData',fld:'vAREATRABALHO_VALIDAOSFMTITLEFILTERDATA',pic:'',nv:null},{av:'AV79AreaTrabalho_DiasParaPagarTitleFilterData',fld:'vAREATRABALHO_DIASPARAPAGARTITLEFILTERDATA',pic:'',nv:null},{av:'AV133Estado_UFTitleFilterData',fld:'vESTADO_UFTITLEFILTERDATA',pic:'',nv:null},{av:'AV117Municipio_NomeTitleFilterData',fld:'vMUNICIPIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV121AreaTrabalho_CalculoPFinalTitleFilterData',fld:'vAREATRABALHO_CALCULOPFINALTITLEFILTERDATA',pic:'',nv:null},{av:'AV142AreaTrabalho_VerTATitleFilterData',fld:'vAREATRABALHO_VERTATITLEFILTERDATA',pic:'',nv:null},{av:'AV83AreaTrabalho_AtivoTitleFilterData',fld:'vAREATRABALHO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtAreaTrabalho_Descricao_Titleformat',ctrl:'AREATRABALHO_DESCRICAO',prop:'Titleformat'},{av:'edtAreaTrabalho_Descricao_Title',ctrl:'AREATRABALHO_DESCRICAO',prop:'Title'},{av:'edtContratante_RazaoSocial_Titleformat',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Titleformat'},{av:'edtContratante_RazaoSocial_Title',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Title'},{av:'edtContratante_CNPJ_Titleformat',ctrl:'CONTRATANTE_CNPJ',prop:'Titleformat'},{av:'edtContratante_CNPJ_Title',ctrl:'CONTRATANTE_CNPJ',prop:'Title'},{av:'edtContratante_Telefone_Titleformat',ctrl:'CONTRATANTE_TELEFONE',prop:'Titleformat'},{av:'edtContratante_Telefone_Title',ctrl:'CONTRATANTE_TELEFONE',prop:'Title'},{av:'cmbAreaTrabalho_ValidaOSFM'},{av:'edtAreaTrabalho_DiasParaPagar_Titleformat',ctrl:'AREATRABALHO_DIASPARAPAGAR',prop:'Titleformat'},{av:'edtAreaTrabalho_DiasParaPagar_Title',ctrl:'AREATRABALHO_DIASPARAPAGAR',prop:'Title'},{av:'edtEstado_UF_Titleformat',ctrl:'ESTADO_UF',prop:'Titleformat'},{av:'edtEstado_UF_Title',ctrl:'ESTADO_UF',prop:'Title'},{av:'edtMunicipio_Nome_Titleformat',ctrl:'MUNICIPIO_NOME',prop:'Titleformat'},{av:'edtMunicipio_Nome_Title',ctrl:'MUNICIPIO_NOME',prop:'Title'},{av:'cmbAreaTrabalho_CalculoPFinal'},{av:'cmbAreaTrabalho_VerTA'},{av:'chkAreaTrabalho_Ativo_Titleformat',ctrl:'AREATRABALHO_ATIVO',prop:'Titleformat'},{av:'chkAreaTrabalho_Ativo.Title.Text',ctrl:'AREATRABALHO_ATIVO',prop:'Title'},{av:'AV88GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV89GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E110T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_AREATRABALHO_DESCRICAO.ONOPTIONCLICKED","{handler:'E120T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_areatrabalho_descricao_Activeeventkey',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_areatrabalho_descricao_Filteredtext_get',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_areatrabalho_descricao_Selectedvalue_get',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_areatrabalho_validaosfm_Sortedstatus',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SortedStatus'},{av:'Ddo_areatrabalho_diasparapagar_Sortedstatus',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_areatrabalho_calculopfinal_Sortedstatus',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SortedStatus'},{av:'Ddo_areatrabalho_verta_Sortedstatus',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SortedStatus'},{av:'Ddo_areatrabalho_ativo_Sortedstatus',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTE_RAZAOSOCIAL.ONOPTIONCLICKED","{handler:'E130T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_contratante_razaosocial_Activeeventkey',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'ActiveEventKey'},{av:'Ddo_contratante_razaosocial_Filteredtext_get',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'FilteredText_get'},{av:'Ddo_contratante_razaosocial_Selectedvalue_get',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_areatrabalho_validaosfm_Sortedstatus',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SortedStatus'},{av:'Ddo_areatrabalho_diasparapagar_Sortedstatus',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_areatrabalho_calculopfinal_Sortedstatus',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SortedStatus'},{av:'Ddo_areatrabalho_verta_Sortedstatus',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SortedStatus'},{av:'Ddo_areatrabalho_ativo_Sortedstatus',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTE_CNPJ.ONOPTIONCLICKED","{handler:'E140T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_contratante_cnpj_Activeeventkey',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'ActiveEventKey'},{av:'Ddo_contratante_cnpj_Filteredtext_get',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'FilteredText_get'},{av:'Ddo_contratante_cnpj_Selectedvalue_get',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_areatrabalho_validaosfm_Sortedstatus',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SortedStatus'},{av:'Ddo_areatrabalho_diasparapagar_Sortedstatus',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_areatrabalho_calculopfinal_Sortedstatus',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SortedStatus'},{av:'Ddo_areatrabalho_verta_Sortedstatus',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SortedStatus'},{av:'Ddo_areatrabalho_ativo_Sortedstatus',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATANTE_TELEFONE.ONOPTIONCLICKED","{handler:'E150T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_contratante_telefone_Activeeventkey',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'ActiveEventKey'},{av:'Ddo_contratante_telefone_Filteredtext_get',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'FilteredText_get'},{av:'Ddo_contratante_telefone_Selectedvalue_get',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_areatrabalho_validaosfm_Sortedstatus',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SortedStatus'},{av:'Ddo_areatrabalho_diasparapagar_Sortedstatus',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_areatrabalho_calculopfinal_Sortedstatus',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SortedStatus'},{av:'Ddo_areatrabalho_verta_Sortedstatus',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SortedStatus'},{av:'Ddo_areatrabalho_ativo_Sortedstatus',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AREATRABALHO_VALIDAOSFM.ONOPTIONCLICKED","{handler:'E160T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_areatrabalho_validaosfm_Activeeventkey',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'ActiveEventKey'},{av:'Ddo_areatrabalho_validaosfm_Selectedvalue_get',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_areatrabalho_validaosfm_Sortedstatus',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SortedStatus'},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_areatrabalho_diasparapagar_Sortedstatus',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_areatrabalho_calculopfinal_Sortedstatus',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SortedStatus'},{av:'Ddo_areatrabalho_verta_Sortedstatus',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SortedStatus'},{av:'Ddo_areatrabalho_ativo_Sortedstatus',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AREATRABALHO_DIASPARAPAGAR.ONOPTIONCLICKED","{handler:'E170T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_areatrabalho_diasparapagar_Activeeventkey',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'ActiveEventKey'},{av:'Ddo_areatrabalho_diasparapagar_Filteredtext_get',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'FilteredText_get'},{av:'Ddo_areatrabalho_diasparapagar_Filteredtextto_get',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_areatrabalho_diasparapagar_Sortedstatus',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'SortedStatus'},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_areatrabalho_validaosfm_Sortedstatus',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_areatrabalho_calculopfinal_Sortedstatus',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SortedStatus'},{av:'Ddo_areatrabalho_verta_Sortedstatus',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SortedStatus'},{av:'Ddo_areatrabalho_ativo_Sortedstatus',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ESTADO_UF.ONOPTIONCLICKED","{handler:'E180T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_estado_uf_Activeeventkey',ctrl:'DDO_ESTADO_UF',prop:'ActiveEventKey'},{av:'Ddo_estado_uf_Filteredtext_get',ctrl:'DDO_ESTADO_UF',prop:'FilteredText_get'},{av:'Ddo_estado_uf_Selectedvalue_get',ctrl:'DDO_ESTADO_UF',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_areatrabalho_validaosfm_Sortedstatus',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SortedStatus'},{av:'Ddo_areatrabalho_diasparapagar_Sortedstatus',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_areatrabalho_calculopfinal_Sortedstatus',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SortedStatus'},{av:'Ddo_areatrabalho_verta_Sortedstatus',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SortedStatus'},{av:'Ddo_areatrabalho_ativo_Sortedstatus',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_MUNICIPIO_NOME.ONOPTIONCLICKED","{handler:'E190T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_municipio_nome_Activeeventkey',ctrl:'DDO_MUNICIPIO_NOME',prop:'ActiveEventKey'},{av:'Ddo_municipio_nome_Filteredtext_get',ctrl:'DDO_MUNICIPIO_NOME',prop:'FilteredText_get'},{av:'Ddo_municipio_nome_Selectedvalue_get',ctrl:'DDO_MUNICIPIO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_areatrabalho_validaosfm_Sortedstatus',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SortedStatus'},{av:'Ddo_areatrabalho_diasparapagar_Sortedstatus',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_areatrabalho_calculopfinal_Sortedstatus',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SortedStatus'},{av:'Ddo_areatrabalho_verta_Sortedstatus',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SortedStatus'},{av:'Ddo_areatrabalho_ativo_Sortedstatus',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AREATRABALHO_CALCULOPFINAL.ONOPTIONCLICKED","{handler:'E200T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_areatrabalho_calculopfinal_Activeeventkey',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'ActiveEventKey'},{av:'Ddo_areatrabalho_calculopfinal_Selectedvalue_get',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_areatrabalho_calculopfinal_Sortedstatus',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SortedStatus'},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_areatrabalho_validaosfm_Sortedstatus',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SortedStatus'},{av:'Ddo_areatrabalho_diasparapagar_Sortedstatus',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_areatrabalho_verta_Sortedstatus',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SortedStatus'},{av:'Ddo_areatrabalho_ativo_Sortedstatus',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AREATRABALHO_VERTA.ONOPTIONCLICKED","{handler:'E210T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_areatrabalho_verta_Activeeventkey',ctrl:'DDO_AREATRABALHO_VERTA',prop:'ActiveEventKey'},{av:'Ddo_areatrabalho_verta_Selectedvalue_get',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_areatrabalho_verta_Sortedstatus',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SortedStatus'},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_areatrabalho_validaosfm_Sortedstatus',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SortedStatus'},{av:'Ddo_areatrabalho_diasparapagar_Sortedstatus',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_areatrabalho_calculopfinal_Sortedstatus',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SortedStatus'},{av:'Ddo_areatrabalho_ativo_Sortedstatus',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AREATRABALHO_ATIVO.ONOPTIONCLICKED","{handler:'E220T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_areatrabalho_ativo_Activeeventkey',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_areatrabalho_ativo_Selectedvalue_get',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_areatrabalho_ativo_Sortedstatus',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SortedStatus'},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_areatrabalho_descricao_Sortedstatus',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_contratante_razaosocial_Sortedstatus',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SortedStatus'},{av:'Ddo_contratante_cnpj_Sortedstatus',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SortedStatus'},{av:'Ddo_contratante_telefone_Sortedstatus',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SortedStatus'},{av:'Ddo_areatrabalho_validaosfm_Sortedstatus',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SortedStatus'},{av:'Ddo_areatrabalho_diasparapagar_Sortedstatus',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_municipio_nome_Sortedstatus',ctrl:'DDO_MUNICIPIO_NOME',prop:'SortedStatus'},{av:'Ddo_areatrabalho_calculopfinal_Sortedstatus',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SortedStatus'},{av:'Ddo_areatrabalho_verta_Sortedstatus',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E360T2',iparms:[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV29Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV28Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV94Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV141Selecionar',fld:'vSELECIONAR',pic:'',nv:''},{av:'edtavSelecionar_Tooltiptext',ctrl:'vSELECIONAR',prop:'Tooltiptext'},{av:'edtContratante_RazaoSocial_Link',ctrl:'CONTRATANTE_RAZAOSOCIAL',prop:'Link'},{av:'edtMunicipio_Nome_Link',ctrl:'MUNICIPIO_NOME',prop:'Link'},{av:'edtavSelecionar_Visible',ctrl:'vSELECIONAR',prop:'Visible'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'imgInsert_Tooltiptext',ctrl:'INSERT',prop:'Tooltiptext'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E230T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E290T2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E240T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'edtavAreatrabalho_descricao2_Visible',ctrl:'vAREATRABALHO_DESCRICAO2',prop:'Visible'},{av:'edtavOrganizacao_nome2_Visible',ctrl:'vORGANIZACAO_NOME2',prop:'Visible'},{av:'cmbavAreatrabalho_ativo2'},{av:'edtavAreatrabalho_descricao3_Visible',ctrl:'vAREATRABALHO_DESCRICAO3',prop:'Visible'},{av:'edtavOrganizacao_nome3_Visible',ctrl:'vORGANIZACAO_NOME3',prop:'Visible'},{av:'cmbavAreatrabalho_ativo3'},{av:'edtavAreatrabalho_descricao1_Visible',ctrl:'vAREATRABALHO_DESCRICAO1',prop:'Visible'},{av:'edtavOrganizacao_nome1_Visible',ctrl:'vORGANIZACAO_NOME1',prop:'Visible'},{av:'cmbavAreatrabalho_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E300T2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavAreatrabalho_descricao1_Visible',ctrl:'vAREATRABALHO_DESCRICAO1',prop:'Visible'},{av:'edtavOrganizacao_nome1_Visible',ctrl:'vORGANIZACAO_NOME1',prop:'Visible'},{av:'cmbavAreatrabalho_ativo1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E310T2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E250T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'edtavAreatrabalho_descricao2_Visible',ctrl:'vAREATRABALHO_DESCRICAO2',prop:'Visible'},{av:'edtavOrganizacao_nome2_Visible',ctrl:'vORGANIZACAO_NOME2',prop:'Visible'},{av:'cmbavAreatrabalho_ativo2'},{av:'edtavAreatrabalho_descricao3_Visible',ctrl:'vAREATRABALHO_DESCRICAO3',prop:'Visible'},{av:'edtavOrganizacao_nome3_Visible',ctrl:'vORGANIZACAO_NOME3',prop:'Visible'},{av:'cmbavAreatrabalho_ativo3'},{av:'edtavAreatrabalho_descricao1_Visible',ctrl:'vAREATRABALHO_DESCRICAO1',prop:'Visible'},{av:'edtavOrganizacao_nome1_Visible',ctrl:'vORGANIZACAO_NOME1',prop:'Visible'},{av:'cmbavAreatrabalho_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E320T2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavAreatrabalho_descricao2_Visible',ctrl:'vAREATRABALHO_DESCRICAO2',prop:'Visible'},{av:'edtavOrganizacao_nome2_Visible',ctrl:'vORGANIZACAO_NOME2',prop:'Visible'},{av:'cmbavAreatrabalho_ativo2'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E260T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'edtavAreatrabalho_descricao2_Visible',ctrl:'vAREATRABALHO_DESCRICAO2',prop:'Visible'},{av:'edtavOrganizacao_nome2_Visible',ctrl:'vORGANIZACAO_NOME2',prop:'Visible'},{av:'cmbavAreatrabalho_ativo2'},{av:'edtavAreatrabalho_descricao3_Visible',ctrl:'vAREATRABALHO_DESCRICAO3',prop:'Visible'},{av:'edtavOrganizacao_nome3_Visible',ctrl:'vORGANIZACAO_NOME3',prop:'Visible'},{av:'cmbavAreatrabalho_ativo3'},{av:'edtavAreatrabalho_descricao1_Visible',ctrl:'vAREATRABALHO_DESCRICAO1',prop:'Visible'},{av:'edtavOrganizacao_nome1_Visible',ctrl:'vORGANIZACAO_NOME1',prop:'Visible'},{av:'cmbavAreatrabalho_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E330T2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavAreatrabalho_descricao3_Visible',ctrl:'vAREATRABALHO_DESCRICAO3',prop:'Visible'},{av:'edtavOrganizacao_nome3_Visible',ctrl:'vORGANIZACAO_NOME3',prop:'Visible'},{av:'cmbavAreatrabalho_ativo3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E270T2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_RAZAOSOCIALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV71ddo_Contratante_CNPJTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_CNPJTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_Contratante_TelefoneTitleControlIdToReplace',fld:'vDDO_CONTRATANTE_TELEFONETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VALIDAOSFMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_DIASPARAPAGARTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV136ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV120ddo_Municipio_NomeTitleControlIdToReplace',fld:'vDDO_MUNICIPIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_CALCULOPFINALTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace',fld:'vDDO_AREATRABALHO_VERTATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace',fld:'vDDO_AREATRABALHO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV188Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A25Municipio_Codigo',fld:'MUNICIPIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV61TFAreaTrabalho_Descricao',fld:'vTFAREATRABALHO_DESCRICAO',pic:'@!',nv:''},{av:'Ddo_areatrabalho_descricao_Filteredtext_set',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'FilteredText_set'},{av:'AV62TFAreaTrabalho_Descricao_Sel',fld:'vTFAREATRABALHO_DESCRICAO_SEL',pic:'@!',nv:''},{av:'Ddo_areatrabalho_descricao_Selectedvalue_set',ctrl:'DDO_AREATRABALHO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV65TFContratante_RazaoSocial',fld:'vTFCONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'Ddo_contratante_razaosocial_Filteredtext_set',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'FilteredText_set'},{av:'AV66TFContratante_RazaoSocial_Sel',fld:'vTFCONTRATANTE_RAZAOSOCIAL_SEL',pic:'@!',nv:''},{av:'Ddo_contratante_razaosocial_Selectedvalue_set',ctrl:'DDO_CONTRATANTE_RAZAOSOCIAL',prop:'SelectedValue_set'},{av:'AV69TFContratante_CNPJ',fld:'vTFCONTRATANTE_CNPJ',pic:'',nv:''},{av:'Ddo_contratante_cnpj_Filteredtext_set',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'FilteredText_set'},{av:'AV70TFContratante_CNPJ_Sel',fld:'vTFCONTRATANTE_CNPJ_SEL',pic:'',nv:''},{av:'Ddo_contratante_cnpj_Selectedvalue_set',ctrl:'DDO_CONTRATANTE_CNPJ',prop:'SelectedValue_set'},{av:'AV73TFContratante_Telefone',fld:'vTFCONTRATANTE_TELEFONE',pic:'',nv:''},{av:'Ddo_contratante_telefone_Filteredtext_set',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'FilteredText_set'},{av:'AV74TFContratante_Telefone_Sel',fld:'vTFCONTRATANTE_TELEFONE_SEL',pic:'',nv:''},{av:'Ddo_contratante_telefone_Selectedvalue_set',ctrl:'DDO_CONTRATANTE_TELEFONE',prop:'SelectedValue_set'},{av:'AV77TFAreaTrabalho_ValidaOSFM_Sel',fld:'vTFAREATRABALHO_VALIDAOSFM_SEL',pic:'9',nv:0},{av:'Ddo_areatrabalho_validaosfm_Selectedvalue_set',ctrl:'DDO_AREATRABALHO_VALIDAOSFM',prop:'SelectedValue_set'},{av:'AV80TFAreaTrabalho_DiasParaPagar',fld:'vTFAREATRABALHO_DIASPARAPAGAR',pic:'ZZZ9',nv:0},{av:'Ddo_areatrabalho_diasparapagar_Filteredtext_set',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'FilteredText_set'},{av:'AV81TFAreaTrabalho_DiasParaPagar_To',fld:'vTFAREATRABALHO_DIASPARAPAGAR_TO',pic:'ZZZ9',nv:0},{av:'Ddo_areatrabalho_diasparapagar_Filteredtextto_set',ctrl:'DDO_AREATRABALHO_DIASPARAPAGAR',prop:'FilteredTextTo_set'},{av:'AV134TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'Ddo_estado_uf_Filteredtext_set',ctrl:'DDO_ESTADO_UF',prop:'FilteredText_set'},{av:'AV135TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'Ddo_estado_uf_Selectedvalue_set',ctrl:'DDO_ESTADO_UF',prop:'SelectedValue_set'},{av:'AV118TFMunicipio_Nome',fld:'vTFMUNICIPIO_NOME',pic:'@!',nv:''},{av:'Ddo_municipio_nome_Filteredtext_set',ctrl:'DDO_MUNICIPIO_NOME',prop:'FilteredText_set'},{av:'AV119TFMunicipio_Nome_Sel',fld:'vTFMUNICIPIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_municipio_nome_Selectedvalue_set',ctrl:'DDO_MUNICIPIO_NOME',prop:'SelectedValue_set'},{av:'AV123TFAreaTrabalho_CalculoPFinal_Sels',fld:'vTFAREATRABALHO_CALCULOPFINAL_SELS',pic:'',nv:null},{av:'Ddo_areatrabalho_calculopfinal_Selectedvalue_set',ctrl:'DDO_AREATRABALHO_CALCULOPFINAL',prop:'SelectedValue_set'},{av:'AV144TFAreaTrabalho_VerTA_Sels',fld:'vTFAREATRABALHO_VERTA_SELS',pic:'',nv:null},{av:'Ddo_areatrabalho_verta_Selectedvalue_set',ctrl:'DDO_AREATRABALHO_VERTA',prop:'SelectedValue_set'},{av:'AV84TFAreaTrabalho_Ativo_Sel',fld:'vTFAREATRABALHO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_areatrabalho_ativo_Selectedvalue_set',ctrl:'DDO_AREATRABALHO_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17AreaTrabalho_Descricao1',fld:'vAREATRABALHO_DESCRICAO1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavAreatrabalho_descricao1_Visible',ctrl:'vAREATRABALHO_DESCRICAO1',prop:'Visible'},{av:'edtavOrganizacao_nome1_Visible',ctrl:'vORGANIZACAO_NOME1',prop:'Visible'},{av:'cmbavAreatrabalho_ativo1'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21AreaTrabalho_Descricao2',fld:'vAREATRABALHO_DESCRICAO2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25AreaTrabalho_Descricao3',fld:'vAREATRABALHO_DESCRICAO3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'cmbavDynamicfiltersoperator3'},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV90Organizacao_Nome1',fld:'vORGANIZACAO_NOME1',pic:'@!',nv:''},{av:'AV129AreaTrabalho_Ativo1',fld:'vAREATRABALHO_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV92Organizacao_Nome2',fld:'vORGANIZACAO_NOME2',pic:'@!',nv:''},{av:'AV130AreaTrabalho_Ativo2',fld:'vAREATRABALHO_ATIVO2',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV131Organizacao_Nome3',fld:'vORGANIZACAO_NOME3',pic:'@!',nv:''},{av:'AV132AreaTrabalho_Ativo3',fld:'vAREATRABALHO_ATIVO3',pic:'',nv:''},{av:'edtavAreatrabalho_descricao2_Visible',ctrl:'vAREATRABALHO_DESCRICAO2',prop:'Visible'},{av:'edtavOrganizacao_nome2_Visible',ctrl:'vORGANIZACAO_NOME2',prop:'Visible'},{av:'cmbavAreatrabalho_ativo2'},{av:'edtavAreatrabalho_descricao3_Visible',ctrl:'vAREATRABALHO_DESCRICAO3',prop:'Visible'},{av:'edtavOrganizacao_nome3_Visible',ctrl:'vORGANIZACAO_NOME3',prop:'Visible'},{av:'cmbavAreatrabalho_ativo3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E280T2',iparms:[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("VSELECIONAR.CLICK","{handler:'E370T2',iparms:[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A6AreaTrabalho_Descricao',fld:'AREATRABALHO_DESCRICAO',pic:'@!',hsh:true,nv:''},{av:'A72AreaTrabalho_Ativo',fld:'AREATRABALHO_ATIVO',pic:'',hsh:true,nv:false},{av:'A9Contratante_RazaoSocial',fld:'CONTRATANTE_RAZAOSOCIAL',pic:'@!',nv:''},{av:'A10Contratante_NomeFantasia',fld:'CONTRATANTE_NOMEFANTASIA',pic:'@!',nv:''},{av:'A31Contratante_Telefone',fld:'CONTRATANTE_TELEFONE',pic:'',nv:''},{av:'A830AreaTrabalho_ServicoPadrao',fld:'AREATRABALHO_SERVICOPADRAO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A642AreaTrabalho_CalculoPFinal',fld:'AREATRABALHO_CALCULOPFINAL',pic:'',hsh:true,nv:''},{av:'A29Contratante_Codigo',fld:'CONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV56Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A64ContratanteUsuario_ContratanteRaz',fld:'CONTRATANTEUSUARIO_CONTRATANTERAZ',pic:'@!',nv:''},{av:'A65ContratanteUsuario_ContratanteFan',fld:'CONTRATANTEUSUARIO_CONTRATANTEFAN',pic:'@!',nv:''},{av:'A1228ContratadaUsuario_AreaTrabalhoCod',fld:'CONTRATADAUSUARIO_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'A69ContratadaUsuario_UsuarioCod',fld:'CONTRATADAUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A66ContratadaUsuario_ContratadaCod',fld:'CONTRATADAUSUARIO_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A67ContratadaUsuario_ContratadaPessoaCod',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOACOD',pic:'ZZZZZ9',nv:0},{av:'A68ContratadaUsuario_ContratadaPessoaNom',fld:'CONTRATADAUSUARIO_CONTRATADAPESSOANOM',pic:'@!',nv:''},{av:'A1136ContratoGestor_ContratadaCod',fld:'CONTRATOGESTOR_CONTRATADACOD',pic:'ZZZZZ9',nv:0},{av:'A1079ContratoGestor_UsuarioCod',fld:'CONTRATOGESTOR_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1078ContratoGestor_ContratoCod',fld:'CONTRATOGESTOR_CONTRATOCOD',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV56Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_areatrabalho_descricao_Activeeventkey = "";
         Ddo_areatrabalho_descricao_Filteredtext_get = "";
         Ddo_areatrabalho_descricao_Selectedvalue_get = "";
         Ddo_contratante_razaosocial_Activeeventkey = "";
         Ddo_contratante_razaosocial_Filteredtext_get = "";
         Ddo_contratante_razaosocial_Selectedvalue_get = "";
         Ddo_contratante_cnpj_Activeeventkey = "";
         Ddo_contratante_cnpj_Filteredtext_get = "";
         Ddo_contratante_cnpj_Selectedvalue_get = "";
         Ddo_contratante_telefone_Activeeventkey = "";
         Ddo_contratante_telefone_Filteredtext_get = "";
         Ddo_contratante_telefone_Selectedvalue_get = "";
         Ddo_areatrabalho_validaosfm_Activeeventkey = "";
         Ddo_areatrabalho_validaosfm_Selectedvalue_get = "";
         Ddo_areatrabalho_diasparapagar_Activeeventkey = "";
         Ddo_areatrabalho_diasparapagar_Filteredtext_get = "";
         Ddo_areatrabalho_diasparapagar_Filteredtextto_get = "";
         Ddo_estado_uf_Activeeventkey = "";
         Ddo_estado_uf_Filteredtext_get = "";
         Ddo_estado_uf_Selectedvalue_get = "";
         Ddo_municipio_nome_Activeeventkey = "";
         Ddo_municipio_nome_Filteredtext_get = "";
         Ddo_municipio_nome_Selectedvalue_get = "";
         Ddo_areatrabalho_calculopfinal_Activeeventkey = "";
         Ddo_areatrabalho_calculopfinal_Selectedvalue_get = "";
         Ddo_areatrabalho_verta_Activeeventkey = "";
         Ddo_areatrabalho_verta_Selectedvalue_get = "";
         Ddo_areatrabalho_ativo_Activeeventkey = "";
         Ddo_areatrabalho_ativo_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17AreaTrabalho_Descricao1 = "";
         AV90Organizacao_Nome1 = "";
         AV129AreaTrabalho_Ativo1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21AreaTrabalho_Descricao2 = "";
         AV92Organizacao_Nome2 = "";
         AV130AreaTrabalho_Ativo2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25AreaTrabalho_Descricao3 = "";
         AV131Organizacao_Nome3 = "";
         AV132AreaTrabalho_Ativo3 = "";
         AV61TFAreaTrabalho_Descricao = "";
         AV62TFAreaTrabalho_Descricao_Sel = "";
         AV65TFContratante_RazaoSocial = "";
         AV66TFContratante_RazaoSocial_Sel = "";
         AV69TFContratante_CNPJ = "";
         AV70TFContratante_CNPJ_Sel = "";
         AV73TFContratante_Telefone = "";
         AV74TFContratante_Telefone_Sel = "";
         AV134TFEstado_UF = "";
         AV135TFEstado_UF_Sel = "";
         AV118TFMunicipio_Nome = "";
         AV119TFMunicipio_Nome_Sel = "";
         AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace = "";
         AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace = "";
         AV71ddo_Contratante_CNPJTitleControlIdToReplace = "";
         AV75ddo_Contratante_TelefoneTitleControlIdToReplace = "";
         AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace = "";
         AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace = "";
         AV136ddo_Estado_UFTitleControlIdToReplace = "";
         AV120ddo_Municipio_NomeTitleControlIdToReplace = "";
         AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace = "";
         AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace = "";
         AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace = "";
         AV123TFAreaTrabalho_CalculoPFinal_Sels = new GxSimpleCollection();
         AV144TFAreaTrabalho_VerTA_Sels = new GxSimpleCollection();
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV188Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV86DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV60AreaTrabalho_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV64Contratante_RazaoSocialTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV68Contratante_CNPJTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV72Contratante_TelefoneTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76AreaTrabalho_ValidaOSFMTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79AreaTrabalho_DiasParaPagarTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV133Estado_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV117Municipio_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV121AreaTrabalho_CalculoPFinalTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV142AreaTrabalho_VerTATitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV83AreaTrabalho_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         A64ContratanteUsuario_ContratanteRaz = "";
         A65ContratanteUsuario_ContratanteFan = "";
         A68ContratadaUsuario_ContratadaPessoaNom = "";
         Ddo_areatrabalho_descricao_Filteredtext_set = "";
         Ddo_areatrabalho_descricao_Selectedvalue_set = "";
         Ddo_areatrabalho_descricao_Sortedstatus = "";
         Ddo_contratante_razaosocial_Filteredtext_set = "";
         Ddo_contratante_razaosocial_Selectedvalue_set = "";
         Ddo_contratante_razaosocial_Sortedstatus = "";
         Ddo_contratante_cnpj_Filteredtext_set = "";
         Ddo_contratante_cnpj_Selectedvalue_set = "";
         Ddo_contratante_cnpj_Sortedstatus = "";
         Ddo_contratante_telefone_Filteredtext_set = "";
         Ddo_contratante_telefone_Selectedvalue_set = "";
         Ddo_contratante_telefone_Sortedstatus = "";
         Ddo_areatrabalho_validaosfm_Selectedvalue_set = "";
         Ddo_areatrabalho_validaosfm_Sortedstatus = "";
         Ddo_areatrabalho_diasparapagar_Filteredtext_set = "";
         Ddo_areatrabalho_diasparapagar_Filteredtextto_set = "";
         Ddo_areatrabalho_diasparapagar_Sortedstatus = "";
         Ddo_estado_uf_Filteredtext_set = "";
         Ddo_estado_uf_Selectedvalue_set = "";
         Ddo_estado_uf_Sortedstatus = "";
         Ddo_municipio_nome_Filteredtext_set = "";
         Ddo_municipio_nome_Selectedvalue_set = "";
         Ddo_municipio_nome_Sortedstatus = "";
         Ddo_areatrabalho_calculopfinal_Selectedvalue_set = "";
         Ddo_areatrabalho_calculopfinal_Sortedstatus = "";
         Ddo_areatrabalho_verta_Selectedvalue_set = "";
         Ddo_areatrabalho_verta_Sortedstatus = "";
         Ddo_areatrabalho_ativo_Selectedvalue_set = "";
         Ddo_areatrabalho_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV29Update = "";
         AV184Update_GXI = "";
         AV28Delete = "";
         AV185Delete_GXI = "";
         AV94Display = "";
         AV186Display_GXI = "";
         A6AreaTrabalho_Descricao = "";
         A9Contratante_RazaoSocial = "";
         A12Contratante_CNPJ = "";
         A31Contratante_Telefone = "";
         A23Estado_UF = "";
         A26Municipio_Nome = "";
         A642AreaTrabalho_CalculoPFinal = "";
         AV141Selecionar = "";
         AV187Selecionar_GXI = "";
         A10Contratante_NomeFantasia = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels = new GxSimpleCollection();
         AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 = "";
         lV152WWAreaTrabalhoDS_5_Organizacao_nome1 = "";
         lV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 = "";
         lV158WWAreaTrabalhoDS_11_Organizacao_nome2 = "";
         lV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 = "";
         lV164WWAreaTrabalhoDS_17_Organizacao_nome3 = "";
         lV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao = "";
         lV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial = "";
         lV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj = "";
         lV172WWAreaTrabalhoDS_25_Tfcontratante_telefone = "";
         lV177WWAreaTrabalhoDS_30_Tfestado_uf = "";
         lV179WWAreaTrabalhoDS_32_Tfmunicipio_nome = "";
         AV148Udparg1 = new GxSimpleCollection();
         AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 = "";
         AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 = "";
         AV152WWAreaTrabalhoDS_5_Organizacao_nome1 = "";
         AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 = "";
         AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 = "";
         AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 = "";
         AV158WWAreaTrabalhoDS_11_Organizacao_nome2 = "";
         AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 = "";
         AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 = "";
         AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 = "";
         AV164WWAreaTrabalhoDS_17_Organizacao_nome3 = "";
         AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 = "";
         AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel = "";
         AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao = "";
         AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel = "";
         AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial = "";
         AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel = "";
         AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj = "";
         AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel = "";
         AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone = "";
         AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel = "";
         AV177WWAreaTrabalhoDS_30_Tfestado_uf = "";
         AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel = "";
         AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome = "";
         A1214Organizacao_Nome = "";
         H000T2_A335Contratante_PessoaCod = new int[1] ;
         H000T2_A1214Organizacao_Nome = new String[] {""} ;
         H000T2_n1214Organizacao_Nome = new bool[] {false} ;
         H000T2_A25Municipio_Codigo = new int[1] ;
         H000T2_n25Municipio_Codigo = new bool[] {false} ;
         H000T2_A830AreaTrabalho_ServicoPadrao = new int[1] ;
         H000T2_n830AreaTrabalho_ServicoPadrao = new bool[] {false} ;
         H000T2_A10Contratante_NomeFantasia = new String[] {""} ;
         H000T2_A72AreaTrabalho_Ativo = new bool[] {false} ;
         H000T2_A2081AreaTrabalho_VerTA = new short[1] ;
         H000T2_n2081AreaTrabalho_VerTA = new bool[] {false} ;
         H000T2_A642AreaTrabalho_CalculoPFinal = new String[] {""} ;
         H000T2_A26Municipio_Nome = new String[] {""} ;
         H000T2_A23Estado_UF = new String[] {""} ;
         H000T2_A1216AreaTrabalho_OrganizacaoCod = new int[1] ;
         H000T2_n1216AreaTrabalho_OrganizacaoCod = new bool[] {false} ;
         H000T2_A855AreaTrabalho_DiasParaPagar = new short[1] ;
         H000T2_n855AreaTrabalho_DiasParaPagar = new bool[] {false} ;
         H000T2_A834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         H000T2_n834AreaTrabalho_ValidaOSFM = new bool[] {false} ;
         H000T2_A31Contratante_Telefone = new String[] {""} ;
         H000T2_n31Contratante_Telefone = new bool[] {false} ;
         H000T2_A12Contratante_CNPJ = new String[] {""} ;
         H000T2_n12Contratante_CNPJ = new bool[] {false} ;
         H000T2_A9Contratante_RazaoSocial = new String[] {""} ;
         H000T2_n9Contratante_RazaoSocial = new bool[] {false} ;
         H000T2_A6AreaTrabalho_Descricao = new String[] {""} ;
         H000T2_A29Contratante_Codigo = new int[1] ;
         H000T2_n29Contratante_Codigo = new bool[] {false} ;
         H000T2_A5AreaTrabalho_Codigo = new int[1] ;
         H000T3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV122TFAreaTrabalho_CalculoPFinal_SelsJson = "";
         AV143TFAreaTrabalho_VerTA_SelsJson = "";
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV32Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         H000T4_A340ContratanteUsuario_ContratantePesCod = new int[1] ;
         H000T4_n340ContratanteUsuario_ContratantePesCod = new bool[] {false} ;
         H000T4_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H000T4_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H000T4_A64ContratanteUsuario_ContratanteRaz = new String[] {""} ;
         H000T4_n64ContratanteUsuario_ContratanteRaz = new bool[] {false} ;
         H000T4_A65ContratanteUsuario_ContratanteFan = new String[] {""} ;
         H000T4_n65ContratanteUsuario_ContratanteFan = new bool[] {false} ;
         H000T4_A31Contratante_Telefone = new String[] {""} ;
         H000T4_n31Contratante_Telefone = new bool[] {false} ;
         H000T5_A69ContratadaUsuario_UsuarioCod = new int[1] ;
         H000T5_A1228ContratadaUsuario_AreaTrabalhoCod = new int[1] ;
         H000T5_n1228ContratadaUsuario_AreaTrabalhoCod = new bool[] {false} ;
         H000T5_A66ContratadaUsuario_ContratadaCod = new int[1] ;
         H000T5_A67ContratadaUsuario_ContratadaPessoaCod = new int[1] ;
         H000T5_n67ContratadaUsuario_ContratadaPessoaCod = new bool[] {false} ;
         H000T5_A68ContratadaUsuario_ContratadaPessoaNom = new String[] {""} ;
         H000T5_n68ContratadaUsuario_ContratadaPessoaNom = new bool[] {false} ;
         H000T6_A1078ContratoGestor_ContratoCod = new int[1] ;
         H000T6_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H000T6_A1136ContratoGestor_ContratadaCod = new int[1] ;
         H000T6_n1136ContratoGestor_ContratadaCod = new bool[] {false} ;
         H000T7_A160ContratoServicos_Codigo = new int[1] ;
         H000T7_A74Contrato_Codigo = new int[1] ;
         H000T7_A155Servico_Codigo = new int[1] ;
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblAreatrabalhotitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         gxphoneLink = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwareatrabalho__default(),
            new Object[][] {
                new Object[] {
               H000T2_A335Contratante_PessoaCod, H000T2_A1214Organizacao_Nome, H000T2_n1214Organizacao_Nome, H000T2_A25Municipio_Codigo, H000T2_n25Municipio_Codigo, H000T2_A830AreaTrabalho_ServicoPadrao, H000T2_n830AreaTrabalho_ServicoPadrao, H000T2_A10Contratante_NomeFantasia, H000T2_A72AreaTrabalho_Ativo, H000T2_A2081AreaTrabalho_VerTA,
               H000T2_n2081AreaTrabalho_VerTA, H000T2_A642AreaTrabalho_CalculoPFinal, H000T2_A26Municipio_Nome, H000T2_A23Estado_UF, H000T2_A1216AreaTrabalho_OrganizacaoCod, H000T2_n1216AreaTrabalho_OrganizacaoCod, H000T2_A855AreaTrabalho_DiasParaPagar, H000T2_n855AreaTrabalho_DiasParaPagar, H000T2_A834AreaTrabalho_ValidaOSFM, H000T2_n834AreaTrabalho_ValidaOSFM,
               H000T2_A31Contratante_Telefone, H000T2_A12Contratante_CNPJ, H000T2_n12Contratante_CNPJ, H000T2_A9Contratante_RazaoSocial, H000T2_n9Contratante_RazaoSocial, H000T2_A6AreaTrabalho_Descricao, H000T2_A29Contratante_Codigo, H000T2_n29Contratante_Codigo, H000T2_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H000T3_AGRID_nRecordCount
               }
               , new Object[] {
               H000T4_A340ContratanteUsuario_ContratantePesCod, H000T4_n340ContratanteUsuario_ContratantePesCod, H000T4_A60ContratanteUsuario_UsuarioCod, H000T4_A63ContratanteUsuario_ContratanteCod, H000T4_A64ContratanteUsuario_ContratanteRaz, H000T4_n64ContratanteUsuario_ContratanteRaz, H000T4_A65ContratanteUsuario_ContratanteFan, H000T4_n65ContratanteUsuario_ContratanteFan, H000T4_A31Contratante_Telefone, H000T4_n31Contratante_Telefone
               }
               , new Object[] {
               H000T5_A69ContratadaUsuario_UsuarioCod, H000T5_A1228ContratadaUsuario_AreaTrabalhoCod, H000T5_n1228ContratadaUsuario_AreaTrabalhoCod, H000T5_A66ContratadaUsuario_ContratadaCod, H000T5_A67ContratadaUsuario_ContratadaPessoaCod, H000T5_n67ContratadaUsuario_ContratadaPessoaCod, H000T5_A68ContratadaUsuario_ContratadaPessoaNom, H000T5_n68ContratadaUsuario_ContratadaPessoaNom
               }
               , new Object[] {
               H000T6_A1078ContratoGestor_ContratoCod, H000T6_A1079ContratoGestor_UsuarioCod, H000T6_A1136ContratoGestor_ContratadaCod, H000T6_n1136ContratoGestor_ContratadaCod
               }
               , new Object[] {
               H000T7_A160ContratoServicos_Codigo, H000T7_A74Contrato_Codigo, H000T7_A155Servico_Codigo
               }
            }
         );
         AV188Pgmname = "WWAreaTrabalho";
         /* GeneXus formulas. */
         AV188Pgmname = "WWAreaTrabalho";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_94 ;
      private short nGXsfl_94_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV77TFAreaTrabalho_ValidaOSFM_Sel ;
      private short AV80TFAreaTrabalho_DiasParaPagar ;
      private short AV81TFAreaTrabalho_DiasParaPagar_To ;
      private short AV84TFAreaTrabalho_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A855AreaTrabalho_DiasParaPagar ;
      private short A2081AreaTrabalho_VerTA ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_94_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 ;
      private short AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 ;
      private short AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 ;
      private short AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel ;
      private short AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar ;
      private short AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to ;
      private short AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel ;
      private short edtAreaTrabalho_Descricao_Titleformat ;
      private short edtContratante_RazaoSocial_Titleformat ;
      private short edtContratante_CNPJ_Titleformat ;
      private short edtContratante_Telefone_Titleformat ;
      private short cmbAreaTrabalho_ValidaOSFM_Titleformat ;
      private short edtAreaTrabalho_DiasParaPagar_Titleformat ;
      private short edtEstado_UF_Titleformat ;
      private short edtMunicipio_Nome_Titleformat ;
      private short cmbAreaTrabalho_CalculoPFinal_Titleformat ;
      private short cmbAreaTrabalho_VerTA_Titleformat ;
      private short chkAreaTrabalho_Ativo_Titleformat ;
      private short AV190GXLvl1289 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A5AreaTrabalho_Codigo ;
      private int A29Contratante_Codigo ;
      private int A25Municipio_Codigo ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int AV56Contratante_Codigo ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A1228ContratadaUsuario_AreaTrabalhoCod ;
      private int A69ContratadaUsuario_UsuarioCod ;
      private int A66ContratadaUsuario_ContratadaCod ;
      private int A67ContratadaUsuario_ContratadaPessoaCod ;
      private int A1136ContratoGestor_ContratadaCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A74Contrato_Codigo ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A155Servico_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_areatrabalho_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_contratante_razaosocial_Datalistupdateminimumcharacters ;
      private int Ddo_contratante_cnpj_Datalistupdateminimumcharacters ;
      private int Ddo_contratante_telefone_Datalistupdateminimumcharacters ;
      private int Ddo_estado_uf_Datalistupdateminimumcharacters ;
      private int Ddo_municipio_nome_Datalistupdateminimumcharacters ;
      private int edtavTfareatrabalho_descricao_Visible ;
      private int edtavTfareatrabalho_descricao_sel_Visible ;
      private int edtavTfcontratante_razaosocial_Visible ;
      private int edtavTfcontratante_razaosocial_sel_Visible ;
      private int edtavTfcontratante_cnpj_Visible ;
      private int edtavTfcontratante_cnpj_sel_Visible ;
      private int edtavTfcontratante_telefone_Visible ;
      private int edtavTfcontratante_telefone_sel_Visible ;
      private int edtavTfareatrabalho_validaosfm_sel_Visible ;
      private int edtavTfareatrabalho_diasparapagar_Visible ;
      private int edtavTfareatrabalho_diasparapagar_to_Visible ;
      private int edtavTfestado_uf_Visible ;
      private int edtavTfestado_uf_sel_Visible ;
      private int edtavTfmunicipio_nome_Visible ;
      private int edtavTfmunicipio_nome_sel_Visible ;
      private int edtavTfareatrabalho_ativo_sel_Visible ;
      private int edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratante_telefonetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_areatrabalho_validaosfmtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_areatrabalho_diasparapagartitlecontrolidtoreplace_Visible ;
      private int edtavDdo_estado_uftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_municipio_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_areatrabalho_calculopfinaltitlecontrolidtoreplace_Visible ;
      private int edtavDdo_areatrabalho_vertatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_areatrabalho_ativotitlecontrolidtoreplace_Visible ;
      private int A1216AreaTrabalho_OrganizacaoCod ;
      private int A830AreaTrabalho_ServicoPadrao ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels_Count ;
      private int AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels_Count ;
      private int AV6WWPContext_gxTpr_Contratante_codigo ;
      private int AV6WWPContext_gxTpr_Contratada_codigo ;
      private int A335Contratante_PessoaCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV87PageToGo ;
      private int edtavSelecionar_Visible ;
      private int imgInsert_Enabled ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavAreatrabalho_descricao1_Visible ;
      private int edtavOrganizacao_nome1_Visible ;
      private int edtavAreatrabalho_descricao2_Visible ;
      private int edtavOrganizacao_nome2_Visible ;
      private int edtavAreatrabalho_descricao3_Visible ;
      private int edtavOrganizacao_nome3_Visible ;
      private int AV189GXV1 ;
      private int A340ContratanteUsuario_ContratantePesCod ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelecionar_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long AV88GridCurrentPage ;
      private long AV89GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_areatrabalho_descricao_Activeeventkey ;
      private String Ddo_areatrabalho_descricao_Filteredtext_get ;
      private String Ddo_areatrabalho_descricao_Selectedvalue_get ;
      private String Ddo_contratante_razaosocial_Activeeventkey ;
      private String Ddo_contratante_razaosocial_Filteredtext_get ;
      private String Ddo_contratante_razaosocial_Selectedvalue_get ;
      private String Ddo_contratante_cnpj_Activeeventkey ;
      private String Ddo_contratante_cnpj_Filteredtext_get ;
      private String Ddo_contratante_cnpj_Selectedvalue_get ;
      private String Ddo_contratante_telefone_Activeeventkey ;
      private String Ddo_contratante_telefone_Filteredtext_get ;
      private String Ddo_contratante_telefone_Selectedvalue_get ;
      private String Ddo_areatrabalho_validaosfm_Activeeventkey ;
      private String Ddo_areatrabalho_validaosfm_Selectedvalue_get ;
      private String Ddo_areatrabalho_diasparapagar_Activeeventkey ;
      private String Ddo_areatrabalho_diasparapagar_Filteredtext_get ;
      private String Ddo_areatrabalho_diasparapagar_Filteredtextto_get ;
      private String Ddo_estado_uf_Activeeventkey ;
      private String Ddo_estado_uf_Filteredtext_get ;
      private String Ddo_estado_uf_Selectedvalue_get ;
      private String Ddo_municipio_nome_Activeeventkey ;
      private String Ddo_municipio_nome_Filteredtext_get ;
      private String Ddo_municipio_nome_Selectedvalue_get ;
      private String Ddo_areatrabalho_calculopfinal_Activeeventkey ;
      private String Ddo_areatrabalho_calculopfinal_Selectedvalue_get ;
      private String Ddo_areatrabalho_verta_Activeeventkey ;
      private String Ddo_areatrabalho_verta_Selectedvalue_get ;
      private String Ddo_areatrabalho_ativo_Activeeventkey ;
      private String Ddo_areatrabalho_ativo_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_94_idx="0001" ;
      private String AV90Organizacao_Nome1 ;
      private String AV129AreaTrabalho_Ativo1 ;
      private String AV92Organizacao_Nome2 ;
      private String AV130AreaTrabalho_Ativo2 ;
      private String AV131Organizacao_Nome3 ;
      private String AV132AreaTrabalho_Ativo3 ;
      private String AV65TFContratante_RazaoSocial ;
      private String AV66TFContratante_RazaoSocial_Sel ;
      private String AV73TFContratante_Telefone ;
      private String AV74TFContratante_Telefone_Sel ;
      private String AV134TFEstado_UF ;
      private String AV135TFEstado_UF_Sel ;
      private String AV118TFMunicipio_Nome ;
      private String AV119TFMunicipio_Nome_Sel ;
      private String AV188Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A64ContratanteUsuario_ContratanteRaz ;
      private String A65ContratanteUsuario_ContratanteFan ;
      private String A68ContratadaUsuario_ContratadaPessoaNom ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_areatrabalho_descricao_Caption ;
      private String Ddo_areatrabalho_descricao_Tooltip ;
      private String Ddo_areatrabalho_descricao_Cls ;
      private String Ddo_areatrabalho_descricao_Filteredtext_set ;
      private String Ddo_areatrabalho_descricao_Selectedvalue_set ;
      private String Ddo_areatrabalho_descricao_Dropdownoptionstype ;
      private String Ddo_areatrabalho_descricao_Titlecontrolidtoreplace ;
      private String Ddo_areatrabalho_descricao_Sortedstatus ;
      private String Ddo_areatrabalho_descricao_Filtertype ;
      private String Ddo_areatrabalho_descricao_Datalisttype ;
      private String Ddo_areatrabalho_descricao_Datalistproc ;
      private String Ddo_areatrabalho_descricao_Sortasc ;
      private String Ddo_areatrabalho_descricao_Sortdsc ;
      private String Ddo_areatrabalho_descricao_Loadingdata ;
      private String Ddo_areatrabalho_descricao_Cleanfilter ;
      private String Ddo_areatrabalho_descricao_Noresultsfound ;
      private String Ddo_areatrabalho_descricao_Searchbuttontext ;
      private String Ddo_contratante_razaosocial_Caption ;
      private String Ddo_contratante_razaosocial_Tooltip ;
      private String Ddo_contratante_razaosocial_Cls ;
      private String Ddo_contratante_razaosocial_Filteredtext_set ;
      private String Ddo_contratante_razaosocial_Selectedvalue_set ;
      private String Ddo_contratante_razaosocial_Dropdownoptionstype ;
      private String Ddo_contratante_razaosocial_Titlecontrolidtoreplace ;
      private String Ddo_contratante_razaosocial_Sortedstatus ;
      private String Ddo_contratante_razaosocial_Filtertype ;
      private String Ddo_contratante_razaosocial_Datalisttype ;
      private String Ddo_contratante_razaosocial_Datalistproc ;
      private String Ddo_contratante_razaosocial_Sortasc ;
      private String Ddo_contratante_razaosocial_Sortdsc ;
      private String Ddo_contratante_razaosocial_Loadingdata ;
      private String Ddo_contratante_razaosocial_Cleanfilter ;
      private String Ddo_contratante_razaosocial_Noresultsfound ;
      private String Ddo_contratante_razaosocial_Searchbuttontext ;
      private String Ddo_contratante_cnpj_Caption ;
      private String Ddo_contratante_cnpj_Tooltip ;
      private String Ddo_contratante_cnpj_Cls ;
      private String Ddo_contratante_cnpj_Filteredtext_set ;
      private String Ddo_contratante_cnpj_Selectedvalue_set ;
      private String Ddo_contratante_cnpj_Dropdownoptionstype ;
      private String Ddo_contratante_cnpj_Titlecontrolidtoreplace ;
      private String Ddo_contratante_cnpj_Sortedstatus ;
      private String Ddo_contratante_cnpj_Filtertype ;
      private String Ddo_contratante_cnpj_Datalisttype ;
      private String Ddo_contratante_cnpj_Datalistproc ;
      private String Ddo_contratante_cnpj_Sortasc ;
      private String Ddo_contratante_cnpj_Sortdsc ;
      private String Ddo_contratante_cnpj_Loadingdata ;
      private String Ddo_contratante_cnpj_Cleanfilter ;
      private String Ddo_contratante_cnpj_Noresultsfound ;
      private String Ddo_contratante_cnpj_Searchbuttontext ;
      private String Ddo_contratante_telefone_Caption ;
      private String Ddo_contratante_telefone_Tooltip ;
      private String Ddo_contratante_telefone_Cls ;
      private String Ddo_contratante_telefone_Filteredtext_set ;
      private String Ddo_contratante_telefone_Selectedvalue_set ;
      private String Ddo_contratante_telefone_Dropdownoptionstype ;
      private String Ddo_contratante_telefone_Titlecontrolidtoreplace ;
      private String Ddo_contratante_telefone_Sortedstatus ;
      private String Ddo_contratante_telefone_Filtertype ;
      private String Ddo_contratante_telefone_Datalisttype ;
      private String Ddo_contratante_telefone_Datalistproc ;
      private String Ddo_contratante_telefone_Sortasc ;
      private String Ddo_contratante_telefone_Sortdsc ;
      private String Ddo_contratante_telefone_Loadingdata ;
      private String Ddo_contratante_telefone_Cleanfilter ;
      private String Ddo_contratante_telefone_Noresultsfound ;
      private String Ddo_contratante_telefone_Searchbuttontext ;
      private String Ddo_areatrabalho_validaosfm_Caption ;
      private String Ddo_areatrabalho_validaosfm_Tooltip ;
      private String Ddo_areatrabalho_validaosfm_Cls ;
      private String Ddo_areatrabalho_validaosfm_Selectedvalue_set ;
      private String Ddo_areatrabalho_validaosfm_Dropdownoptionstype ;
      private String Ddo_areatrabalho_validaosfm_Titlecontrolidtoreplace ;
      private String Ddo_areatrabalho_validaosfm_Sortedstatus ;
      private String Ddo_areatrabalho_validaosfm_Datalisttype ;
      private String Ddo_areatrabalho_validaosfm_Datalistfixedvalues ;
      private String Ddo_areatrabalho_validaosfm_Sortasc ;
      private String Ddo_areatrabalho_validaosfm_Sortdsc ;
      private String Ddo_areatrabalho_validaosfm_Cleanfilter ;
      private String Ddo_areatrabalho_validaosfm_Searchbuttontext ;
      private String Ddo_areatrabalho_diasparapagar_Caption ;
      private String Ddo_areatrabalho_diasparapagar_Tooltip ;
      private String Ddo_areatrabalho_diasparapagar_Cls ;
      private String Ddo_areatrabalho_diasparapagar_Filteredtext_set ;
      private String Ddo_areatrabalho_diasparapagar_Filteredtextto_set ;
      private String Ddo_areatrabalho_diasparapagar_Dropdownoptionstype ;
      private String Ddo_areatrabalho_diasparapagar_Titlecontrolidtoreplace ;
      private String Ddo_areatrabalho_diasparapagar_Sortedstatus ;
      private String Ddo_areatrabalho_diasparapagar_Filtertype ;
      private String Ddo_areatrabalho_diasparapagar_Sortasc ;
      private String Ddo_areatrabalho_diasparapagar_Sortdsc ;
      private String Ddo_areatrabalho_diasparapagar_Cleanfilter ;
      private String Ddo_areatrabalho_diasparapagar_Rangefilterfrom ;
      private String Ddo_areatrabalho_diasparapagar_Rangefilterto ;
      private String Ddo_areatrabalho_diasparapagar_Searchbuttontext ;
      private String Ddo_estado_uf_Caption ;
      private String Ddo_estado_uf_Tooltip ;
      private String Ddo_estado_uf_Cls ;
      private String Ddo_estado_uf_Filteredtext_set ;
      private String Ddo_estado_uf_Selectedvalue_set ;
      private String Ddo_estado_uf_Dropdownoptionstype ;
      private String Ddo_estado_uf_Titlecontrolidtoreplace ;
      private String Ddo_estado_uf_Sortedstatus ;
      private String Ddo_estado_uf_Filtertype ;
      private String Ddo_estado_uf_Datalisttype ;
      private String Ddo_estado_uf_Datalistproc ;
      private String Ddo_estado_uf_Sortasc ;
      private String Ddo_estado_uf_Sortdsc ;
      private String Ddo_estado_uf_Loadingdata ;
      private String Ddo_estado_uf_Cleanfilter ;
      private String Ddo_estado_uf_Noresultsfound ;
      private String Ddo_estado_uf_Searchbuttontext ;
      private String Ddo_municipio_nome_Caption ;
      private String Ddo_municipio_nome_Tooltip ;
      private String Ddo_municipio_nome_Cls ;
      private String Ddo_municipio_nome_Filteredtext_set ;
      private String Ddo_municipio_nome_Selectedvalue_set ;
      private String Ddo_municipio_nome_Dropdownoptionstype ;
      private String Ddo_municipio_nome_Titlecontrolidtoreplace ;
      private String Ddo_municipio_nome_Sortedstatus ;
      private String Ddo_municipio_nome_Filtertype ;
      private String Ddo_municipio_nome_Datalisttype ;
      private String Ddo_municipio_nome_Datalistproc ;
      private String Ddo_municipio_nome_Sortasc ;
      private String Ddo_municipio_nome_Sortdsc ;
      private String Ddo_municipio_nome_Loadingdata ;
      private String Ddo_municipio_nome_Cleanfilter ;
      private String Ddo_municipio_nome_Noresultsfound ;
      private String Ddo_municipio_nome_Searchbuttontext ;
      private String Ddo_areatrabalho_calculopfinal_Caption ;
      private String Ddo_areatrabalho_calculopfinal_Tooltip ;
      private String Ddo_areatrabalho_calculopfinal_Cls ;
      private String Ddo_areatrabalho_calculopfinal_Selectedvalue_set ;
      private String Ddo_areatrabalho_calculopfinal_Dropdownoptionstype ;
      private String Ddo_areatrabalho_calculopfinal_Titlecontrolidtoreplace ;
      private String Ddo_areatrabalho_calculopfinal_Sortedstatus ;
      private String Ddo_areatrabalho_calculopfinal_Datalisttype ;
      private String Ddo_areatrabalho_calculopfinal_Datalistfixedvalues ;
      private String Ddo_areatrabalho_calculopfinal_Sortasc ;
      private String Ddo_areatrabalho_calculopfinal_Sortdsc ;
      private String Ddo_areatrabalho_calculopfinal_Cleanfilter ;
      private String Ddo_areatrabalho_calculopfinal_Searchbuttontext ;
      private String Ddo_areatrabalho_verta_Caption ;
      private String Ddo_areatrabalho_verta_Tooltip ;
      private String Ddo_areatrabalho_verta_Cls ;
      private String Ddo_areatrabalho_verta_Selectedvalue_set ;
      private String Ddo_areatrabalho_verta_Dropdownoptionstype ;
      private String Ddo_areatrabalho_verta_Titlecontrolidtoreplace ;
      private String Ddo_areatrabalho_verta_Sortedstatus ;
      private String Ddo_areatrabalho_verta_Datalisttype ;
      private String Ddo_areatrabalho_verta_Datalistfixedvalues ;
      private String Ddo_areatrabalho_verta_Sortasc ;
      private String Ddo_areatrabalho_verta_Sortdsc ;
      private String Ddo_areatrabalho_verta_Cleanfilter ;
      private String Ddo_areatrabalho_verta_Searchbuttontext ;
      private String Ddo_areatrabalho_ativo_Caption ;
      private String Ddo_areatrabalho_ativo_Tooltip ;
      private String Ddo_areatrabalho_ativo_Cls ;
      private String Ddo_areatrabalho_ativo_Selectedvalue_set ;
      private String Ddo_areatrabalho_ativo_Dropdownoptionstype ;
      private String Ddo_areatrabalho_ativo_Titlecontrolidtoreplace ;
      private String Ddo_areatrabalho_ativo_Sortedstatus ;
      private String Ddo_areatrabalho_ativo_Datalisttype ;
      private String Ddo_areatrabalho_ativo_Datalistfixedvalues ;
      private String Ddo_areatrabalho_ativo_Sortasc ;
      private String Ddo_areatrabalho_ativo_Sortdsc ;
      private String Ddo_areatrabalho_ativo_Cleanfilter ;
      private String Ddo_areatrabalho_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfareatrabalho_descricao_Internalname ;
      private String edtavTfareatrabalho_descricao_Jsonclick ;
      private String edtavTfareatrabalho_descricao_sel_Internalname ;
      private String edtavTfareatrabalho_descricao_sel_Jsonclick ;
      private String edtavTfcontratante_razaosocial_Internalname ;
      private String edtavTfcontratante_razaosocial_Jsonclick ;
      private String edtavTfcontratante_razaosocial_sel_Internalname ;
      private String edtavTfcontratante_razaosocial_sel_Jsonclick ;
      private String edtavTfcontratante_cnpj_Internalname ;
      private String edtavTfcontratante_cnpj_Jsonclick ;
      private String edtavTfcontratante_cnpj_sel_Internalname ;
      private String edtavTfcontratante_cnpj_sel_Jsonclick ;
      private String edtavTfcontratante_telefone_Internalname ;
      private String edtavTfcontratante_telefone_Jsonclick ;
      private String edtavTfcontratante_telefone_sel_Internalname ;
      private String edtavTfcontratante_telefone_sel_Jsonclick ;
      private String edtavTfareatrabalho_validaosfm_sel_Internalname ;
      private String edtavTfareatrabalho_validaosfm_sel_Jsonclick ;
      private String edtavTfareatrabalho_diasparapagar_Internalname ;
      private String edtavTfareatrabalho_diasparapagar_Jsonclick ;
      private String edtavTfareatrabalho_diasparapagar_to_Internalname ;
      private String edtavTfareatrabalho_diasparapagar_to_Jsonclick ;
      private String edtavTfestado_uf_Internalname ;
      private String edtavTfestado_uf_Jsonclick ;
      private String edtavTfestado_uf_sel_Internalname ;
      private String edtavTfestado_uf_sel_Jsonclick ;
      private String edtavTfmunicipio_nome_Internalname ;
      private String edtavTfmunicipio_nome_Jsonclick ;
      private String edtavTfmunicipio_nome_sel_Internalname ;
      private String edtavTfmunicipio_nome_sel_Jsonclick ;
      private String edtavTfareatrabalho_ativo_sel_Internalname ;
      private String edtavTfareatrabalho_ativo_sel_Jsonclick ;
      private String edtavDdo_areatrabalho_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratante_razaosocialtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratante_cnpjtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratante_telefonetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_areatrabalho_validaosfmtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_areatrabalho_diasparapagartitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_estado_uftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_municipio_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_areatrabalho_calculopfinaltitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_areatrabalho_vertatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_areatrabalho_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtAreaTrabalho_Codigo_Internalname ;
      private String edtContratante_Codigo_Internalname ;
      private String edtAreaTrabalho_Descricao_Internalname ;
      private String A9Contratante_RazaoSocial ;
      private String edtContratante_RazaoSocial_Internalname ;
      private String edtContratante_CNPJ_Internalname ;
      private String A31Contratante_Telefone ;
      private String edtContratante_Telefone_Internalname ;
      private String cmbAreaTrabalho_ValidaOSFM_Internalname ;
      private String edtAreaTrabalho_DiasParaPagar_Internalname ;
      private String edtAreaTrabalho_OrganizacaoCod_Internalname ;
      private String A23Estado_UF ;
      private String edtEstado_UF_Internalname ;
      private String A26Municipio_Nome ;
      private String edtMunicipio_Nome_Internalname ;
      private String cmbAreaTrabalho_CalculoPFinal_Internalname ;
      private String A642AreaTrabalho_CalculoPFinal ;
      private String cmbAreaTrabalho_VerTA_Internalname ;
      private String chkAreaTrabalho_Ativo_Internalname ;
      private String edtavSelecionar_Internalname ;
      private String A10Contratante_NomeFantasia ;
      private String edtContratante_NomeFantasia_Internalname ;
      private String edtAreaTrabalho_ServicoPadrao_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV152WWAreaTrabalhoDS_5_Organizacao_nome1 ;
      private String lV158WWAreaTrabalhoDS_11_Organizacao_nome2 ;
      private String lV164WWAreaTrabalhoDS_17_Organizacao_nome3 ;
      private String lV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial ;
      private String lV172WWAreaTrabalhoDS_25_Tfcontratante_telefone ;
      private String lV177WWAreaTrabalhoDS_30_Tfestado_uf ;
      private String lV179WWAreaTrabalhoDS_32_Tfmunicipio_nome ;
      private String AV152WWAreaTrabalhoDS_5_Organizacao_nome1 ;
      private String AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 ;
      private String AV158WWAreaTrabalhoDS_11_Organizacao_nome2 ;
      private String AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 ;
      private String AV164WWAreaTrabalhoDS_17_Organizacao_nome3 ;
      private String AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 ;
      private String AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel ;
      private String AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial ;
      private String AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel ;
      private String AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone ;
      private String AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel ;
      private String AV177WWAreaTrabalhoDS_30_Tfestado_uf ;
      private String AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel ;
      private String AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome ;
      private String A1214Organizacao_Nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavAreatrabalho_descricao1_Internalname ;
      private String edtavOrganizacao_nome1_Internalname ;
      private String cmbavAreatrabalho_ativo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavAreatrabalho_descricao2_Internalname ;
      private String edtavOrganizacao_nome2_Internalname ;
      private String cmbavAreatrabalho_ativo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavAreatrabalho_descricao3_Internalname ;
      private String edtavOrganizacao_nome3_Internalname ;
      private String cmbavAreatrabalho_ativo3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_areatrabalho_descricao_Internalname ;
      private String Ddo_contratante_razaosocial_Internalname ;
      private String Ddo_contratante_cnpj_Internalname ;
      private String Ddo_contratante_telefone_Internalname ;
      private String Ddo_areatrabalho_validaosfm_Internalname ;
      private String Ddo_areatrabalho_diasparapagar_Internalname ;
      private String Ddo_estado_uf_Internalname ;
      private String Ddo_municipio_nome_Internalname ;
      private String Ddo_areatrabalho_calculopfinal_Internalname ;
      private String Ddo_areatrabalho_verta_Internalname ;
      private String Ddo_areatrabalho_ativo_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtAreaTrabalho_Descricao_Title ;
      private String edtContratante_RazaoSocial_Title ;
      private String edtContratante_CNPJ_Title ;
      private String edtContratante_Telefone_Title ;
      private String edtAreaTrabalho_DiasParaPagar_Title ;
      private String edtEstado_UF_Title ;
      private String edtMunicipio_Nome_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavSelecionar_Tooltiptext ;
      private String edtContratante_RazaoSocial_Link ;
      private String edtMunicipio_Nome_Link ;
      private String imgInsert_Internalname ;
      private String imgInsert_Tooltiptext ;
      private String imgInsert_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavAreatrabalho_descricao3_Jsonclick ;
      private String edtavOrganizacao_nome3_Jsonclick ;
      private String cmbavAreatrabalho_ativo3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavAreatrabalho_descricao2_Jsonclick ;
      private String edtavOrganizacao_nome2_Jsonclick ;
      private String cmbavAreatrabalho_ativo2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavAreatrabalho_descricao1_Jsonclick ;
      private String edtavOrganizacao_nome1_Jsonclick ;
      private String cmbavAreatrabalho_ativo1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblAreatrabalhotitle_Internalname ;
      private String lblAreatrabalhotitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_94_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAreaTrabalho_Codigo_Jsonclick ;
      private String edtContratante_Codigo_Jsonclick ;
      private String edtAreaTrabalho_Descricao_Jsonclick ;
      private String edtContratante_RazaoSocial_Jsonclick ;
      private String edtContratante_CNPJ_Jsonclick ;
      private String gxphoneLink ;
      private String edtContratante_Telefone_Jsonclick ;
      private String cmbAreaTrabalho_ValidaOSFM_Jsonclick ;
      private String edtAreaTrabalho_DiasParaPagar_Jsonclick ;
      private String edtAreaTrabalho_OrganizacaoCod_Jsonclick ;
      private String edtEstado_UF_Jsonclick ;
      private String edtMunicipio_Nome_Jsonclick ;
      private String cmbAreaTrabalho_CalculoPFinal_Jsonclick ;
      private String cmbAreaTrabalho_VerTA_Jsonclick ;
      private String edtavSelecionar_Jsonclick ;
      private String edtContratante_NomeFantasia_Jsonclick ;
      private String edtAreaTrabalho_ServicoPadrao_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool n29Contratante_Codigo ;
      private bool n25Municipio_Codigo ;
      private bool A72AreaTrabalho_Ativo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_areatrabalho_descricao_Includesortasc ;
      private bool Ddo_areatrabalho_descricao_Includesortdsc ;
      private bool Ddo_areatrabalho_descricao_Includefilter ;
      private bool Ddo_areatrabalho_descricao_Filterisrange ;
      private bool Ddo_areatrabalho_descricao_Includedatalist ;
      private bool Ddo_contratante_razaosocial_Includesortasc ;
      private bool Ddo_contratante_razaosocial_Includesortdsc ;
      private bool Ddo_contratante_razaosocial_Includefilter ;
      private bool Ddo_contratante_razaosocial_Filterisrange ;
      private bool Ddo_contratante_razaosocial_Includedatalist ;
      private bool Ddo_contratante_cnpj_Includesortasc ;
      private bool Ddo_contratante_cnpj_Includesortdsc ;
      private bool Ddo_contratante_cnpj_Includefilter ;
      private bool Ddo_contratante_cnpj_Filterisrange ;
      private bool Ddo_contratante_cnpj_Includedatalist ;
      private bool Ddo_contratante_telefone_Includesortasc ;
      private bool Ddo_contratante_telefone_Includesortdsc ;
      private bool Ddo_contratante_telefone_Includefilter ;
      private bool Ddo_contratante_telefone_Filterisrange ;
      private bool Ddo_contratante_telefone_Includedatalist ;
      private bool Ddo_areatrabalho_validaosfm_Includesortasc ;
      private bool Ddo_areatrabalho_validaosfm_Includesortdsc ;
      private bool Ddo_areatrabalho_validaosfm_Includefilter ;
      private bool Ddo_areatrabalho_validaosfm_Includedatalist ;
      private bool Ddo_areatrabalho_diasparapagar_Includesortasc ;
      private bool Ddo_areatrabalho_diasparapagar_Includesortdsc ;
      private bool Ddo_areatrabalho_diasparapagar_Includefilter ;
      private bool Ddo_areatrabalho_diasparapagar_Filterisrange ;
      private bool Ddo_areatrabalho_diasparapagar_Includedatalist ;
      private bool Ddo_estado_uf_Includesortasc ;
      private bool Ddo_estado_uf_Includesortdsc ;
      private bool Ddo_estado_uf_Includefilter ;
      private bool Ddo_estado_uf_Filterisrange ;
      private bool Ddo_estado_uf_Includedatalist ;
      private bool Ddo_municipio_nome_Includesortasc ;
      private bool Ddo_municipio_nome_Includesortdsc ;
      private bool Ddo_municipio_nome_Includefilter ;
      private bool Ddo_municipio_nome_Filterisrange ;
      private bool Ddo_municipio_nome_Includedatalist ;
      private bool Ddo_areatrabalho_calculopfinal_Includesortasc ;
      private bool Ddo_areatrabalho_calculopfinal_Includesortdsc ;
      private bool Ddo_areatrabalho_calculopfinal_Includefilter ;
      private bool Ddo_areatrabalho_calculopfinal_Includedatalist ;
      private bool Ddo_areatrabalho_calculopfinal_Allowmultipleselection ;
      private bool Ddo_areatrabalho_verta_Includesortasc ;
      private bool Ddo_areatrabalho_verta_Includesortdsc ;
      private bool Ddo_areatrabalho_verta_Includefilter ;
      private bool Ddo_areatrabalho_verta_Includedatalist ;
      private bool Ddo_areatrabalho_verta_Allowmultipleselection ;
      private bool Ddo_areatrabalho_ativo_Includesortasc ;
      private bool Ddo_areatrabalho_ativo_Includesortdsc ;
      private bool Ddo_areatrabalho_ativo_Includefilter ;
      private bool Ddo_areatrabalho_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n9Contratante_RazaoSocial ;
      private bool n12Contratante_CNPJ ;
      private bool n31Contratante_Telefone ;
      private bool A834AreaTrabalho_ValidaOSFM ;
      private bool n834AreaTrabalho_ValidaOSFM ;
      private bool n855AreaTrabalho_DiasParaPagar ;
      private bool n1216AreaTrabalho_OrganizacaoCod ;
      private bool n2081AreaTrabalho_VerTA ;
      private bool n830AreaTrabalho_ServicoPadrao ;
      private bool AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 ;
      private bool AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 ;
      private bool n1214Organizacao_Nome ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool n340ContratanteUsuario_ContratantePesCod ;
      private bool n64ContratanteUsuario_ContratanteRaz ;
      private bool n65ContratanteUsuario_ContratanteFan ;
      private bool n1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool n67ContratadaUsuario_ContratadaPessoaCod ;
      private bool n68ContratadaUsuario_ContratadaPessoaNom ;
      private bool n1136ContratoGestor_ContratadaCod ;
      private bool AV29Update_IsBlob ;
      private bool AV28Delete_IsBlob ;
      private bool AV94Display_IsBlob ;
      private bool AV141Selecionar_IsBlob ;
      private String AV122TFAreaTrabalho_CalculoPFinal_SelsJson ;
      private String AV143TFAreaTrabalho_VerTA_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV17AreaTrabalho_Descricao1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV21AreaTrabalho_Descricao2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV25AreaTrabalho_Descricao3 ;
      private String AV61TFAreaTrabalho_Descricao ;
      private String AV62TFAreaTrabalho_Descricao_Sel ;
      private String AV69TFContratante_CNPJ ;
      private String AV70TFContratante_CNPJ_Sel ;
      private String AV63ddo_AreaTrabalho_DescricaoTitleControlIdToReplace ;
      private String AV67ddo_Contratante_RazaoSocialTitleControlIdToReplace ;
      private String AV71ddo_Contratante_CNPJTitleControlIdToReplace ;
      private String AV75ddo_Contratante_TelefoneTitleControlIdToReplace ;
      private String AV78ddo_AreaTrabalho_ValidaOSFMTitleControlIdToReplace ;
      private String AV82ddo_AreaTrabalho_DiasParaPagarTitleControlIdToReplace ;
      private String AV136ddo_Estado_UFTitleControlIdToReplace ;
      private String AV120ddo_Municipio_NomeTitleControlIdToReplace ;
      private String AV124ddo_AreaTrabalho_CalculoPFinalTitleControlIdToReplace ;
      private String AV145ddo_AreaTrabalho_VerTATitleControlIdToReplace ;
      private String AV85ddo_AreaTrabalho_AtivoTitleControlIdToReplace ;
      private String AV184Update_GXI ;
      private String AV185Delete_GXI ;
      private String AV186Display_GXI ;
      private String A6AreaTrabalho_Descricao ;
      private String A12Contratante_CNPJ ;
      private String AV187Selecionar_GXI ;
      private String lV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 ;
      private String lV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 ;
      private String lV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 ;
      private String lV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao ;
      private String lV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj ;
      private String AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 ;
      private String AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 ;
      private String AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 ;
      private String AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 ;
      private String AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 ;
      private String AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 ;
      private String AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel ;
      private String AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao ;
      private String AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel ;
      private String AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj ;
      private String AV29Update ;
      private String AV28Delete ;
      private String AV94Display ;
      private String AV141Selecionar ;
      private String imgInsert_Bitmap ;
      private IGxSession AV32Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavAreatrabalho_ativo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavAreatrabalho_ativo2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCombobox cmbavAreatrabalho_ativo3 ;
      private GXCombobox cmbAreaTrabalho_ValidaOSFM ;
      private GXCombobox cmbAreaTrabalho_CalculoPFinal ;
      private GXCombobox cmbAreaTrabalho_VerTA ;
      private GXCheckbox chkAreaTrabalho_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H000T2_A335Contratante_PessoaCod ;
      private String[] H000T2_A1214Organizacao_Nome ;
      private bool[] H000T2_n1214Organizacao_Nome ;
      private int[] H000T2_A25Municipio_Codigo ;
      private bool[] H000T2_n25Municipio_Codigo ;
      private int[] H000T2_A830AreaTrabalho_ServicoPadrao ;
      private bool[] H000T2_n830AreaTrabalho_ServicoPadrao ;
      private String[] H000T2_A10Contratante_NomeFantasia ;
      private bool[] H000T2_A72AreaTrabalho_Ativo ;
      private short[] H000T2_A2081AreaTrabalho_VerTA ;
      private bool[] H000T2_n2081AreaTrabalho_VerTA ;
      private String[] H000T2_A642AreaTrabalho_CalculoPFinal ;
      private String[] H000T2_A26Municipio_Nome ;
      private String[] H000T2_A23Estado_UF ;
      private int[] H000T2_A1216AreaTrabalho_OrganizacaoCod ;
      private bool[] H000T2_n1216AreaTrabalho_OrganizacaoCod ;
      private short[] H000T2_A855AreaTrabalho_DiasParaPagar ;
      private bool[] H000T2_n855AreaTrabalho_DiasParaPagar ;
      private bool[] H000T2_A834AreaTrabalho_ValidaOSFM ;
      private bool[] H000T2_n834AreaTrabalho_ValidaOSFM ;
      private String[] H000T2_A31Contratante_Telefone ;
      private bool[] H000T2_n31Contratante_Telefone ;
      private String[] H000T2_A12Contratante_CNPJ ;
      private bool[] H000T2_n12Contratante_CNPJ ;
      private String[] H000T2_A9Contratante_RazaoSocial ;
      private bool[] H000T2_n9Contratante_RazaoSocial ;
      private String[] H000T2_A6AreaTrabalho_Descricao ;
      private int[] H000T2_A29Contratante_Codigo ;
      private bool[] H000T2_n29Contratante_Codigo ;
      private int[] H000T2_A5AreaTrabalho_Codigo ;
      private long[] H000T3_AGRID_nRecordCount ;
      private int[] H000T4_A340ContratanteUsuario_ContratantePesCod ;
      private bool[] H000T4_n340ContratanteUsuario_ContratantePesCod ;
      private int[] H000T4_A60ContratanteUsuario_UsuarioCod ;
      private int[] H000T4_A63ContratanteUsuario_ContratanteCod ;
      private String[] H000T4_A64ContratanteUsuario_ContratanteRaz ;
      private bool[] H000T4_n64ContratanteUsuario_ContratanteRaz ;
      private String[] H000T4_A65ContratanteUsuario_ContratanteFan ;
      private bool[] H000T4_n65ContratanteUsuario_ContratanteFan ;
      private String[] H000T4_A31Contratante_Telefone ;
      private bool[] H000T4_n31Contratante_Telefone ;
      private int[] H000T5_A69ContratadaUsuario_UsuarioCod ;
      private int[] H000T5_A1228ContratadaUsuario_AreaTrabalhoCod ;
      private bool[] H000T5_n1228ContratadaUsuario_AreaTrabalhoCod ;
      private int[] H000T5_A66ContratadaUsuario_ContratadaCod ;
      private int[] H000T5_A67ContratadaUsuario_ContratadaPessoaCod ;
      private bool[] H000T5_n67ContratadaUsuario_ContratadaPessoaCod ;
      private String[] H000T5_A68ContratadaUsuario_ContratadaPessoaNom ;
      private bool[] H000T5_n68ContratadaUsuario_ContratadaPessoaNom ;
      private int[] H000T6_A1078ContratoGestor_ContratoCod ;
      private int[] H000T6_A1079ContratoGestor_UsuarioCod ;
      private int[] H000T6_A1136ContratoGestor_ContratadaCod ;
      private bool[] H000T6_n1136ContratoGestor_ContratadaCod ;
      private int[] H000T7_A160ContratoServicos_Codigo ;
      private int[] H000T7_A74Contrato_Codigo ;
      private int[] H000T7_A155Servico_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV144TFAreaTrabalho_VerTA_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV148Udparg1 ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV123TFAreaTrabalho_CalculoPFinal_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV60AreaTrabalho_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV64Contratante_RazaoSocialTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV68Contratante_CNPJTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV72Contratante_TelefoneTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV76AreaTrabalho_ValidaOSFMTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV79AreaTrabalho_DiasParaPagarTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV133Estado_UFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV117Municipio_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV121AreaTrabalho_CalculoPFinalTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV142AreaTrabalho_VerTATitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV83AreaTrabalho_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV86DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wwareatrabalho__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H000T2( IGxContext context ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV148Udparg1 ,
                                             String A642AreaTrabalho_CalculoPFinal ,
                                             IGxCollection AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels ,
                                             short A2081AreaTrabalho_VerTA ,
                                             IGxCollection AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels ,
                                             String AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 ,
                                             String AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 ,
                                             short AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 ,
                                             String AV152WWAreaTrabalhoDS_5_Organizacao_nome1 ,
                                             String AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 ,
                                             bool AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 ,
                                             String AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 ,
                                             String AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 ,
                                             short AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 ,
                                             String AV158WWAreaTrabalhoDS_11_Organizacao_nome2 ,
                                             String AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 ,
                                             bool AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 ,
                                             String AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 ,
                                             String AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 ,
                                             short AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 ,
                                             String AV164WWAreaTrabalhoDS_17_Organizacao_nome3 ,
                                             String AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 ,
                                             String AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel ,
                                             String AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao ,
                                             String AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel ,
                                             String AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial ,
                                             String AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel ,
                                             String AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj ,
                                             String AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel ,
                                             String AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone ,
                                             short AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel ,
                                             short AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar ,
                                             short AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to ,
                                             String AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel ,
                                             String AV177WWAreaTrabalhoDS_30_Tfestado_uf ,
                                             String AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel ,
                                             String AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome ,
                                             int AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels_Count ,
                                             int AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels_Count ,
                                             short AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel ,
                                             String A6AreaTrabalho_Descricao ,
                                             String A1214Organizacao_Nome ,
                                             bool A72AreaTrabalho_Ativo ,
                                             String A9Contratante_RazaoSocial ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             bool A834AreaTrabalho_ValidaOSFM ,
                                             short A855AreaTrabalho_DiasParaPagar ,
                                             String A23Estado_UF ,
                                             String A26Municipio_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int AV6WWPContext_gxTpr_Contratante_codigo ,
                                             int AV6WWPContext_gxTpr_Contratada_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [30] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[Contratante_PessoaCod] AS Contratante_PessoaCod, T2.[Organizacao_Nome], T3.[Municipio_Codigo], T1.[AreaTrabalho_ServicoPadrao], T3.[Contratante_NomeFantasia], T1.[AreaTrabalho_Ativo], T1.[AreaTrabalho_VerTA], T1.[AreaTrabalho_CalculoPFinal], T5.[Municipio_Nome], T5.[Estado_UF], T1.[AreaTrabalho_OrganizacaoCod] AS AreaTrabalho_OrganizacaoCod, T1.[AreaTrabalho_DiasParaPagar], T1.[AreaTrabalho_ValidaOSFM], T3.[Contratante_Telefone], T4.[Pessoa_Docto] AS Contratante_CNPJ, T4.[Pessoa_Nome] AS Contratante_RazaoSocial, T1.[AreaTrabalho_Descricao], T1.[Contratante_Codigo], T1.[AreaTrabalho_Codigo]";
         sFromString = " FROM (((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Organizacao] T2 WITH (NOLOCK) ON T2.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod]) LEFT JOIN [Contratante] T3 WITH (NOLOCK) ON T3.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T5 WITH (NOLOCK) ON T5.[Municipio_Codigo] = T3.[Municipio_Codigo])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (Not ( ( @AV6WWPCo_2Contratante_codigo + @AV6WWPCo_1Contratada_codigo > 0)) or ( " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV148Udparg1, "T1.[AreaTrabalho_Codigo] IN (", ")") + "))";
         if ( ( StringUtil.StrCmp(AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWAreaTrabalhoDS_5_Organizacao_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Organizacao_Nome] like @lV152WWAreaTrabalhoDS_5_Organizacao_nome1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWAreaTrabalhoDS_5_Organizacao_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Organizacao_Nome] like '%' + @lV152WWAreaTrabalhoDS_5_Organizacao_nome1)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWAreaTrabalhoDS_11_Organizacao_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Organizacao_Nome] like @lV158WWAreaTrabalhoDS_11_Organizacao_nome2)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWAreaTrabalhoDS_11_Organizacao_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Organizacao_Nome] like '%' + @lV158WWAreaTrabalhoDS_11_Organizacao_nome2)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV164WWAreaTrabalhoDS_17_Organizacao_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Organizacao_Nome] like @lV164WWAreaTrabalhoDS_17_Organizacao_nome3)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV164WWAreaTrabalhoDS_17_Organizacao_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Organizacao_Nome] like '%' + @lV164WWAreaTrabalhoDS_17_Organizacao_nome3)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like @lV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] = @AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Contratante_Telefone] like @lV172WWAreaTrabalhoDS_25_Tfcontratante_telefone)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Contratante_Telefone] = @AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 1)";
         }
         if ( AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 0)";
         }
         if ( ! (0==AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] >= @AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (0==AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] <= @AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV177WWAreaTrabalhoDS_30_Tfestado_uf)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Estado_UF] like @lV177WWAreaTrabalhoDS_30_Tfestado_uf)";
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Estado_UF] = @AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel)";
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Municipio_Nome] like @lV179WWAreaTrabalhoDS_32_Tfmunicipio_nome)";
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T5.[Municipio_Nome] = @AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel)";
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
         }
         if ( AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
         }
         if ( AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_Descricao]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Docto] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contratante_Telefone]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Contratante_Telefone] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_ValidaOSFM]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_ValidaOSFM] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_DiasParaPagar]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_DiasParaPagar] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Estado_UF]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Estado_UF] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Municipio_Nome]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T5.[Municipio_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_CalculoPFinal]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_CalculoPFinal] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_VerTA]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_VerTA] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_Ativo]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AreaTrabalho_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H000T3( IGxContext context ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV148Udparg1 ,
                                             String A642AreaTrabalho_CalculoPFinal ,
                                             IGxCollection AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels ,
                                             short A2081AreaTrabalho_VerTA ,
                                             IGxCollection AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels ,
                                             String AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1 ,
                                             String AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1 ,
                                             short AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 ,
                                             String AV152WWAreaTrabalhoDS_5_Organizacao_nome1 ,
                                             String AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1 ,
                                             bool AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 ,
                                             String AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2 ,
                                             String AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2 ,
                                             short AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 ,
                                             String AV158WWAreaTrabalhoDS_11_Organizacao_nome2 ,
                                             String AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2 ,
                                             bool AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 ,
                                             String AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3 ,
                                             String AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3 ,
                                             short AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 ,
                                             String AV164WWAreaTrabalhoDS_17_Organizacao_nome3 ,
                                             String AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3 ,
                                             String AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel ,
                                             String AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao ,
                                             String AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel ,
                                             String AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial ,
                                             String AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel ,
                                             String AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj ,
                                             String AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel ,
                                             String AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone ,
                                             short AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel ,
                                             short AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar ,
                                             short AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to ,
                                             String AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel ,
                                             String AV177WWAreaTrabalhoDS_30_Tfestado_uf ,
                                             String AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel ,
                                             String AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome ,
                                             int AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels_Count ,
                                             int AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels_Count ,
                                             short AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel ,
                                             String A6AreaTrabalho_Descricao ,
                                             String A1214Organizacao_Nome ,
                                             bool A72AreaTrabalho_Ativo ,
                                             String A9Contratante_RazaoSocial ,
                                             String A12Contratante_CNPJ ,
                                             String A31Contratante_Telefone ,
                                             bool A834AreaTrabalho_ValidaOSFM ,
                                             short A855AreaTrabalho_DiasParaPagar ,
                                             String A23Estado_UF ,
                                             String A26Municipio_Nome ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int AV6WWPContext_gxTpr_Contratante_codigo ,
                                             int AV6WWPContext_gxTpr_Contratada_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [25] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (((([AreaTrabalho] T1 WITH (NOLOCK) LEFT JOIN [Organizacao] T5 WITH (NOLOCK) ON T5.[Organizacao_Codigo] = T1.[AreaTrabalho_OrganizacaoCod]) LEFT JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[Contratante_Codigo]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) LEFT JOIN [Municipio] T3 WITH (NOLOCK) ON T3.[Municipio_Codigo] = T2.[Municipio_Codigo])";
         scmdbuf = scmdbuf + " WHERE (Not ( ( @AV6WWPCo_2Contratante_codigo + @AV6WWPCo_1Contratada_codigo > 0)) or ( " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV148Udparg1, "T1.[AreaTrabalho_Codigo] IN (", ")") + "))";
         if ( ( StringUtil.StrCmp(AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWAreaTrabalhoDS_5_Organizacao_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV152WWAreaTrabalhoDS_5_Organizacao_nome1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( AV150WWAreaTrabalhoDS_3_Dynamicfiltersoperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV152WWAreaTrabalhoDS_5_Organizacao_nome1)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV152WWAreaTrabalhoDS_5_Organizacao_nome1)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ( StringUtil.StrCmp(AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( ( StringUtil.StrCmp(AV149WWAreaTrabalhoDS_2_Dynamicfiltersselector1, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV153WWAreaTrabalhoDS_6_Areatrabalho_ativo1, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWAreaTrabalhoDS_11_Organizacao_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV158WWAreaTrabalhoDS_11_Organizacao_nome2)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( AV156WWAreaTrabalhoDS_9_Dynamicfiltersoperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV158WWAreaTrabalhoDS_11_Organizacao_nome2)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV158WWAreaTrabalhoDS_11_Organizacao_nome2)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( AV154WWAreaTrabalhoDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV155WWAreaTrabalhoDS_8_Dynamicfiltersselector2, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV159WWAreaTrabalhoDS_12_Areatrabalho_ativo2, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3, "AREATRABALHO_DESCRICAO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like '%' + @lV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV164WWAreaTrabalhoDS_17_Organizacao_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like @lV164WWAreaTrabalhoDS_17_Organizacao_nome3)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( AV162WWAreaTrabalhoDS_15_Dynamicfiltersoperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV164WWAreaTrabalhoDS_17_Organizacao_nome3)) ) )
         {
            sWhereString = sWhereString + " and (T5.[Organizacao_Nome] like '%' + @lV164WWAreaTrabalhoDS_17_Organizacao_nome3)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3, "S") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( AV160WWAreaTrabalhoDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV161WWAreaTrabalhoDS_14_Dynamicfiltersselector3, "AREATRABALHO_ATIVO") == 0 ) && ( ( StringUtil.StrCmp(AV165WWAreaTrabalhoDS_18_Areatrabalho_ativo3, "N") == 0 ) ) )
         {
            sWhereString = sWhereString + " and (Not T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao)) ) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] like @lV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel)) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Descricao] = @AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] like @lV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Pessoa_Docto] = @AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV172WWAreaTrabalhoDS_25_Tfcontratante_telefone)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] like @lV172WWAreaTrabalhoDS_25_Tfcontratante_telefone)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Contratante_Telefone] = @AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 1)";
         }
         if ( AV174WWAreaTrabalhoDS_27_Tfareatrabalho_validaosfm_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_ValidaOSFM] = 0)";
         }
         if ( ! (0==AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] >= @AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (0==AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to) )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_DiasParaPagar] <= @AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV177WWAreaTrabalhoDS_30_Tfestado_uf)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] like @lV177WWAreaTrabalhoDS_30_Tfestado_uf)";
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Estado_UF] = @AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel)";
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV179WWAreaTrabalhoDS_32_Tfmunicipio_nome)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] like @lV179WWAreaTrabalhoDS_32_Tfmunicipio_nome)";
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Municipio_Nome] = @AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel)";
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV181WWAreaTrabalhoDS_34_Tfareatrabalho_calculopfinal_sels, "T1.[AreaTrabalho_CalculoPFinal] IN (", ")") + ")";
         }
         if ( AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV182WWAreaTrabalhoDS_35_Tfareatrabalho_verta_sels, "T1.[AreaTrabalho_VerTA] IN (", ")") + ")";
         }
         if ( AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 1)";
         }
         if ( AV183WWAreaTrabalhoDS_36_Tfareatrabalho_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[AreaTrabalho_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H000T2(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (short)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (short)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (bool)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (bool)dynConstraints[47] , (short)dynConstraints[48] , (String)dynConstraints[49] , (String)dynConstraints[50] , (short)dynConstraints[51] , (bool)dynConstraints[52] , (int)dynConstraints[53] , (int)dynConstraints[54] );
               case 1 :
                     return conditional_H000T3(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (short)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (short)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (short)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (short)dynConstraints[31] , (short)dynConstraints[32] , (short)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (String)dynConstraints[37] , (int)dynConstraints[38] , (int)dynConstraints[39] , (short)dynConstraints[40] , (String)dynConstraints[41] , (String)dynConstraints[42] , (bool)dynConstraints[43] , (String)dynConstraints[44] , (String)dynConstraints[45] , (String)dynConstraints[46] , (bool)dynConstraints[47] , (short)dynConstraints[48] , (String)dynConstraints[49] , (String)dynConstraints[50] , (short)dynConstraints[51] , (bool)dynConstraints[52] , (int)dynConstraints[53] , (int)dynConstraints[54] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000T4 ;
          prmH000T4 = new Object[] {
          new Object[] {"@AV56Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH000T5 ;
          prmH000T5 = new Object[] {
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV6WWPCo_3Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000T6 ;
          prmH000T6 = new Object[] {
          new Object[] {"@AV6WWPContext__Userid",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV6WWPCo_1Contratada_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000T7 ;
          prmH000T7 = new Object[] {
          new Object[] {"@ContratoGestor_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH000T2 ;
          prmH000T2 = new Object[] {
          new Object[] {"@AV6WWPCo_2Contratante_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV152WWAreaTrabalhoDS_5_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV152WWAreaTrabalhoDS_5_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV158WWAreaTrabalhoDS_11_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV158WWAreaTrabalhoDS_11_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV164WWAreaTrabalhoDS_17_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV164WWAreaTrabalhoDS_17_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV172WWAreaTrabalhoDS_25_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV177WWAreaTrabalhoDS_30_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV179WWAreaTrabalhoDS_32_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH000T3 ;
          prmH000T3 = new Object[] {
          new Object[] {"@AV6WWPCo_2Contratante_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV6WWPCo_1Contratada_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV151WWAreaTrabalhoDS_4_Areatrabalho_descricao1",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV152WWAreaTrabalhoDS_5_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV152WWAreaTrabalhoDS_5_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV157WWAreaTrabalhoDS_10_Areatrabalho_descricao2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV158WWAreaTrabalhoDS_11_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV158WWAreaTrabalhoDS_11_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV163WWAreaTrabalhoDS_16_Areatrabalho_descricao3",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV164WWAreaTrabalhoDS_17_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV164WWAreaTrabalhoDS_17_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV166WWAreaTrabalhoDS_19_Tfareatrabalho_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV167WWAreaTrabalhoDS_20_Tfareatrabalho_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@lV168WWAreaTrabalhoDS_21_Tfcontratante_razaosocial",SqlDbType.Char,100,0} ,
          new Object[] {"@AV169WWAreaTrabalhoDS_22_Tfcontratante_razaosocial_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV170WWAreaTrabalhoDS_23_Tfcontratante_cnpj",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV171WWAreaTrabalhoDS_24_Tfcontratante_cnpj_sel",SqlDbType.VarChar,15,0} ,
          new Object[] {"@lV172WWAreaTrabalhoDS_25_Tfcontratante_telefone",SqlDbType.Char,20,0} ,
          new Object[] {"@AV173WWAreaTrabalhoDS_26_Tfcontratante_telefone_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV175WWAreaTrabalhoDS_28_Tfareatrabalho_diasparapagar",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV176WWAreaTrabalhoDS_29_Tfareatrabalho_diasparapagar_to",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV177WWAreaTrabalhoDS_30_Tfestado_uf",SqlDbType.Char,2,0} ,
          new Object[] {"@AV178WWAreaTrabalhoDS_31_Tfestado_uf_sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV179WWAreaTrabalhoDS_32_Tfmunicipio_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV180WWAreaTrabalhoDS_33_Tfmunicipio_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000T2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000T2,11,0,true,false )
             ,new CursorDef("H000T3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000T3,1,0,true,false )
             ,new CursorDef("H000T4", "SELECT TOP 1 T2.[Contratante_PessoaCod] AS ContratanteUsuario_ContratantePesCod, T1.[ContratanteUsuario_UsuarioCod], T1.[ContratanteUsuario_ContratanteCod] AS ContratanteUsuario_ContratanteCod, T3.[Pessoa_Nome] AS ContratanteUsuario_ContratanteRaz, T2.[Contratante_NomeFantasia] AS ContratanteUsuario_ContratanteFan, T2.[Contratante_Telefone] FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratante] T2 WITH (NOLOCK) ON T2.[Contratante_Codigo] = T1.[ContratanteUsuario_ContratanteCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratante_PessoaCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV56Contratante_Codigo and T1.[ContratanteUsuario_UsuarioCod] = @AV6WWPContext__Userid ORDER BY T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000T4,1,0,false,true )
             ,new CursorDef("H000T5", "SELECT TOP 1 T1.[ContratadaUsuario_UsuarioCod], T2.[Contratada_AreaTrabalhoCod] AS ContratadaUsuario_AreaTrabalhoCod, T1.[ContratadaUsuario_ContratadaCod] AS ContratadaUsuario_ContratadaCod, T2.[Contratada_PessoaCod] AS ContratadaUsuario_ContratadaPessoaCod, T3.[Pessoa_Nome] AS ContratadaUsuario_ContratadaPe FROM (([ContratadaUsuario] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContratadaUsuario_ContratadaCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Contratada_PessoaCod]) WHERE (T1.[ContratadaUsuario_UsuarioCod] = @AV6WWPContext__Userid) AND (T2.[Contratada_AreaTrabalhoCod] = @AV6WWPCo_3Areatrabalho_codigo) ORDER BY T1.[ContratadaUsuario_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000T5,1,0,false,true )
             ,new CursorDef("H000T6", "SELECT TOP 1 T1.[ContratoGestor_ContratoCod] AS ContratoGestor_ContratoCod, T1.[ContratoGestor_UsuarioCod], T2.[Contratada_Codigo] AS ContratoGestor_ContratadaCod FROM ([ContratoGestor] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[ContratoGestor_ContratoCod]) WHERE (T1.[ContratoGestor_UsuarioCod] = @AV6WWPContext__Userid) AND (T2.[Contratada_Codigo] = @AV6WWPCo_1Contratada_codigo) ORDER BY T1.[ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000T6,1,0,true,true )
             ,new CursorDef("H000T7", "SELECT [ContratoServicos_Codigo], [Contrato_Codigo], [Servico_Codigo] FROM [ContratoServicos] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoGestor_ContratoCod ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000T7,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[8])[0] = rslt.getBool(6) ;
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getString(8, 2) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 50) ;
                ((String[]) buf[13])[0] = rslt.getString(10, 2) ;
                ((int[]) buf[14])[0] = rslt.getInt(11) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(11);
                ((short[]) buf[16])[0] = rslt.getShort(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((bool[]) buf[18])[0] = rslt.getBool(13) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(13);
                ((String[]) buf[20])[0] = rslt.getString(14, 20) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(15);
                ((String[]) buf[23])[0] = rslt.getString(16, 100) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(16);
                ((String[]) buf[25])[0] = rslt.getVarchar(17) ;
                ((int[]) buf[26])[0] = rslt.getInt(18) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(18);
                ((int[]) buf[28])[0] = rslt.getInt(19) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 20) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
