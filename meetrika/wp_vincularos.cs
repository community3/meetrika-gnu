/*
               File: WP_VincularOs
        Description: Vincular
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/24/2020 23:46:36.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_vincularos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_vincularos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_vincularos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           out bool aP1_Confirmado )
      {
         this.AV13ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV26Confirmado = false ;
         executePrivate();
         aP0_ContagemResultado_Codigo=this.AV13ContagemResultado_Codigo;
         aP1_Confirmado=this.AV26Confirmado;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContagemresultado_demanda2 = new GXCombobox();
         chkavSelected = new GXCheckbox();
         cmbRequisito_Prioridade = new GXCombobox();
         cmbRequisito_Status = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTAGEMRESULTADO_DEMANDA2") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV29WWPContext);
               AV27Filtro = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Filtro", AV27Filtro);
               AV32Filtro2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Filtro2", AV32Filtro2);
               AV13ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContagemResultado_Codigo), 6, 0)));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTAGEMRESULTADO_DEMANDA2FS2( AV29WWPContext, AV27Filtro, AV32Filtro2, AV13ContagemResultado_Codigo) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridrequisitos") == 0 )
            {
               nRC_GXsfl_90 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_90_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_90_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridrequisitos_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridrequisitos") == 0 )
            {
               AV5ContagemResultado_Demanda2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Demanda2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0)));
               AV34Agrupador = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV34Agrupador);
               A1912SolicServicoReqNeg_Agrupador = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1912SolicServicoReqNeg_Agrupador", A1912SolicServicoReqNeg_Agrupador);
               A1914SolicServicoReqNeg_Situacao = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1914SolicServicoReqNeg_Situacao", StringUtil.LTrim( StringUtil.Str( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0)));
               A1895SolicServicoReqNeg_Codigo = (long)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
               AV39OSVisible = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39OSVisible", AV39OSVisible);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridrequisitos_refresh( AV5ContagemResultado_Demanda2, AV34Agrupador, A1912SolicServicoReqNeg_Agrupador, A1914SolicServicoReqNeg_Situacao, A1895SolicServicoReqNeg_Codigo, AV39OSVisible) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV13ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContagemResultado_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV26Confirmado = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Confirmado", AV26Confirmado);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAFS2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTFS2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202032423463630");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_vincularos.aspx") + "?" + UrlEncode("" +AV13ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV26Confirmado))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vCONTAGEMRESULTADO_DEMANDA2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5ContagemResultado_Demanda2), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_90", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_90), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SOLICSERVICOREQNEG_AGRUPADOR", A1912SolicServicoReqNeg_Agrupador);
         GxWebStd.gx_hidden_field( context, "SOLICSERVICOREQNEG_SITUACAO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1914SolicServicoReqNeg_Situacao), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SOLICSERVICOREQNEG_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vOSVISIBLE", AV39OSVisible);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_CONTRATADASIGLA", StringUtil.RTrim( A803ContagemResultado_ContratadaSigla));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A684ContagemResultado_PFBFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFSULTIMA", StringUtil.LTrim( StringUtil.NToC( A685ContagemResultado_PFLFSUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFBFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A682ContagemResultado_PFBFMUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_PFLFMULTIMA", StringUtil.LTrim( StringUtil.NToC( A683ContagemResultado_PFLFMUltima, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATADMN", context.localUtil.DToC( A471ContagemResultado_DataDmn, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_SERVICOSIGLA", StringUtil.RTrim( A801ContagemResultado_ServicoSigla));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREQUISITOS", AV42Requisitos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREQUISITOS", AV42Requisitos);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vCONFIRMADO", AV26Confirmado);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV29WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV29WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_DEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18ContagemResultado_Demanda, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( "", AV17ContagemResultado_DataDmn));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CONTRATADASIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30ContagemResultado_ContratadaSigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_PFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV20ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_PFLFS", GetSecureSignedToken( "", context.localUtil.Format( AV22ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_PFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV19ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_PFLFM", GetSecureSignedToken( "", context.localUtil.Format( AV21ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEFS2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTFS2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_vincularos.aspx") + "?" + UrlEncode("" +AV13ContagemResultado_Codigo) + "," + UrlEncode(StringUtil.BoolToStr(AV26Confirmado)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_VincularOs" ;
      }

      public override String GetPgmdesc( )
      {
         return "Vincular" ;
      }

      protected void WBFS0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            context.WriteHtmlText( "<p>") ;
            wb_table1_3_FS2( true) ;
         }
         else
         {
            wb_table1_3_FS2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_FS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_VincularOs.htm");
         }
         wbLoad = true;
      }

      protected void STARTFS2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Vincular", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPFS0( ) ;
      }

      protected void WSFS2( )
      {
         STARTFS2( ) ;
         EVTFS2( ) ;
      }

      protected void EVTFS2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTAGEMRESULTADO_DEMANDA2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11FS2 */
                              E11FS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ENTER'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12FS2 */
                              E12FS2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 22), "GRIDREQUISITOS.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "GRIDREQUISITOS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                           {
                              nGXsfl_90_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
                              SubsflControlProps_902( ) ;
                              AV35Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV35Selected);
                              A1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( edtRequisito_Codigo_Internalname), ",", "."));
                              AV34Agrupador = cgiGet( edtavAgrupador_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV34Agrupador);
                              A2001Requisito_Identificador = cgiGet( edtRequisito_Identificador_Internalname);
                              n2001Requisito_Identificador = false;
                              A1927Requisito_Titulo = cgiGet( edtRequisito_Titulo_Internalname);
                              n1927Requisito_Titulo = false;
                              cmbRequisito_Prioridade.Name = cmbRequisito_Prioridade_Internalname;
                              cmbRequisito_Prioridade.CurrentValue = cgiGet( cmbRequisito_Prioridade_Internalname);
                              A2002Requisito_Prioridade = (short)(NumberUtil.Val( cgiGet( cmbRequisito_Prioridade_Internalname), "."));
                              n2002Requisito_Prioridade = false;
                              cmbRequisito_Status.Name = cmbRequisito_Status_Internalname;
                              cmbRequisito_Status.CurrentValue = cgiGet( cmbRequisito_Status_Internalname);
                              A1934Requisito_Status = (short)(NumberUtil.Val( cgiGet( cmbRequisito_Status_Internalname), "."));
                              AV33OS = cgiGet( edtavOs_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavOs_Internalname, AV33OS);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13FS2 */
                                    E13FS2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDREQUISITOS.REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14FS2 */
                                    E14FS2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDREQUISITOS.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15FS2 */
                                    E15FS2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Contagemresultado_demanda2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTAGEMRESULTADO_DEMANDA2"), ",", ".") != Convert.ToDecimal( AV5ContagemResultado_Demanda2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                          /* Execute user event: E12FS2 */
                                          E12FS2 ();
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEFS2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAFS2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContagemresultado_demanda2.Name = "vCONTAGEMRESULTADO_DEMANDA2";
            dynavContagemresultado_demanda2.WebTags = "";
            GXCCtl = "vSELECTED_" + sGXsfl_90_idx;
            chkavSelected.Name = GXCCtl;
            chkavSelected.WebTags = "";
            chkavSelected.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelected_Internalname, "TitleCaption", chkavSelected.Caption);
            chkavSelected.CheckedValue = "false";
            GXCCtl = "REQUISITO_PRIORIDADE_" + sGXsfl_90_idx;
            cmbRequisito_Prioridade.Name = GXCCtl;
            cmbRequisito_Prioridade.WebTags = "";
            cmbRequisito_Prioridade.addItem("1", "Alta", 0);
            cmbRequisito_Prioridade.addItem("2", "Alta M�dia", 0);
            cmbRequisito_Prioridade.addItem("3", "Alta Baixa", 0);
            cmbRequisito_Prioridade.addItem("4", "M�dia Alta", 0);
            cmbRequisito_Prioridade.addItem("5", "M�dia M�dia", 0);
            cmbRequisito_Prioridade.addItem("6", "M�dia Baixa", 0);
            cmbRequisito_Prioridade.addItem("7", "Baixa Alta", 0);
            cmbRequisito_Prioridade.addItem("8", "Baixa M�dia", 0);
            cmbRequisito_Prioridade.addItem("9", "Baixa Baixa", 0);
            if ( cmbRequisito_Prioridade.ItemCount > 0 )
            {
               A2002Requisito_Prioridade = (short)(NumberUtil.Val( cmbRequisito_Prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2002Requisito_Prioridade), 2, 0))), "."));
               n2002Requisito_Prioridade = false;
            }
            GXCCtl = "REQUISITO_STATUS_" + sGXsfl_90_idx;
            cmbRequisito_Status.Name = GXCCtl;
            cmbRequisito_Status.WebTags = "";
            cmbRequisito_Status.addItem("0", "Rascunho", 0);
            cmbRequisito_Status.addItem("1", "Solicitado", 0);
            cmbRequisito_Status.addItem("2", "Aprovado", 0);
            cmbRequisito_Status.addItem("3", "N�o Aprovado", 0);
            cmbRequisito_Status.addItem("4", "Pausado", 0);
            cmbRequisito_Status.addItem("5", "Cancelado", 0);
            if ( cmbRequisito_Status.ItemCount > 0 )
            {
               A1934Requisito_Status = (short)(NumberUtil.Val( cmbRequisito_Status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0))), "."));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContagemresultado_demanda_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         GXVvCONTAGEMRESULTADO_DEMANDA2_htmlFS2( AV29WWPContext, AV27Filtro, AV32Filtro2, AV13ContagemResultado_Codigo) ;
         GXVvCONTAGEMRESULTADO_DEMANDA2_htmlFS2( AV29WWPContext, AV27Filtro, AV32Filtro2, AV13ContagemResultado_Codigo) ;
         GXVvCONTAGEMRESULTADO_DEMANDA2_htmlFS2( AV29WWPContext, AV27Filtro, AV32Filtro2, AV13ContagemResultado_Codigo) ;
         GXVvCONTAGEMRESULTADO_DEMANDA2_htmlFS2( AV29WWPContext, AV27Filtro, AV32Filtro2, AV13ContagemResultado_Codigo) ;
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTAGEMRESULTADO_DEMANDA2FS2( wwpbaseobjects.SdtWWPContext AV29WWPContext ,
                                                          String AV27Filtro ,
                                                          String AV32Filtro2 ,
                                                          int AV13ContagemResultado_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTAGEMRESULTADO_DEMANDA2_dataFS2( AV29WWPContext, AV27Filtro, AV32Filtro2, AV13ContagemResultado_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTAGEMRESULTADO_DEMANDA2_htmlFS2( wwpbaseobjects.SdtWWPContext AV29WWPContext ,
                                                             String AV27Filtro ,
                                                             String AV32Filtro2 ,
                                                             int AV13ContagemResultado_Codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTAGEMRESULTADO_DEMANDA2_dataFS2( AV29WWPContext, AV27Filtro, AV32Filtro2, AV13ContagemResultado_Codigo) ;
         gxdynajaxindex = 1;
         dynavContagemresultado_demanda2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContagemresultado_demanda2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContagemresultado_demanda2.ItemCount > 0 )
         {
            AV5ContagemResultado_Demanda2 = (int)(NumberUtil.Val( dynavContagemresultado_demanda2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Demanda2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0)));
         }
      }

      protected void GXDLVvCONTAGEMRESULTADO_DEMANDA2_dataFS2( wwpbaseobjects.SdtWWPContext AV29WWPContext ,
                                                               String AV27Filtro ,
                                                               String AV32Filtro2 ,
                                                               int AV13ContagemResultado_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor H00FS2 */
         pr_default.execute(0, new Object[] {AV27Filtro, AV32Filtro2, AV13ContagemResultado_Codigo, AV29WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00FS2_A456ContagemResultado_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H00FS2_A1815ContagemResultado_DmnSrvPrst[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGridrequisitos_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_902( ) ;
         while ( nGXsfl_90_idx <= nRC_GXsfl_90 )
         {
            sendrow_902( ) ;
            nGXsfl_90_idx = (short)(((subGridrequisitos_Islastpage==1)&&(nGXsfl_90_idx+1>subGridrequisitos_Recordsperpage( )) ? 1 : nGXsfl_90_idx+1));
            sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
            SubsflControlProps_902( ) ;
         }
         context.GX_webresponse.AddString(GridrequisitosContainer.ToJavascriptSource());
         /* End function gxnrGridrequisitos_newrow */
      }

      protected void gxgrGridrequisitos_refresh( int AV5ContagemResultado_Demanda2 ,
                                                 String AV34Agrupador ,
                                                 String A1912SolicServicoReqNeg_Agrupador ,
                                                 short A1914SolicServicoReqNeg_Situacao ,
                                                 long A1895SolicServicoReqNeg_Codigo ,
                                                 bool AV39OSVisible )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRIDREQUISITOS_nCurrentRecord = 0;
         RFFS2( ) ;
         /* End function gxgrGridrequisitos_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1919Requisito_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "REQUISITO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContagemresultado_demanda2.ItemCount > 0 )
         {
            AV5ContagemResultado_Demanda2 = (int)(NumberUtil.Val( dynavContagemresultado_demanda2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Demanda2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFFS2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_demanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda_Enabled), 5, 0)));
         edtavContagemresultado_datadmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datadmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datadmn_Enabled), 5, 0)));
         edtavContagemresultado_contratadasigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contratadasigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contratadasigla_Enabled), 5, 0)));
         edtavContagemresultado_pfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfs_Enabled), 5, 0)));
         edtavContagemresultado_pflfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfs_Enabled), 5, 0)));
         edtavContagemresultado_pfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfm_Enabled), 5, 0)));
         edtavContagemresultado_pflfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfm_Enabled), 5, 0)));
         edtavContagemresultado_datadmn2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datadmn2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datadmn2_Enabled), 5, 0)));
         edtavContagemresultado_contratadasigla2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contratadasigla2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contratadasigla2_Enabled), 5, 0)));
         edtavContagemresultado_pflfs2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfs2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfs2_Enabled), 5, 0)));
         edtavContagemresultado_pfbfs2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfs2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfs2_Enabled), 5, 0)));
         edtavContagemresultado_pfbfm2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfm2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfm2_Enabled), 5, 0)));
         edtavContagemresultado_pflfm2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfm2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfm2_Enabled), 5, 0)));
         edtavServico_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla_Enabled), 5, 0)));
         edtavAgrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgrupador_Enabled), 5, 0)));
         edtavOs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOs_Enabled), 5, 0)));
      }

      protected void RFFS2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridrequisitosContainer.ClearRows();
         }
         wbStart = 90;
         /* Execute user event: E14FS2 */
         E14FS2 ();
         nGXsfl_90_idx = 1;
         sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
         SubsflControlProps_902( ) ;
         nGXsfl_90_Refreshing = 1;
         GridrequisitosContainer.AddObjectProperty("GridName", "Gridrequisitos");
         GridrequisitosContainer.AddObjectProperty("CmpContext", "");
         GridrequisitosContainer.AddObjectProperty("InMasterPage", "false");
         GridrequisitosContainer.AddObjectProperty("Class", "Grid");
         GridrequisitosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridrequisitosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridrequisitosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Backcolorstyle), 1, 0, ".", "")));
         GridrequisitosContainer.AddObjectProperty("Backcoloreven", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Backcoloreven), 9, 0, ".", "")));
         GridrequisitosContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Titleforecolor), 9, 0, ".", "")));
         GridrequisitosContainer.PageSize = subGridrequisitos_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_902( ) ;
            /* Using cursor H00FS5 */
            pr_default.execute(1, new Object[] {AV5ContagemResultado_Demanda2});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A490ContagemResultado_ContratadaCod = H00FS5_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00FS5_n490ContagemResultado_ContratadaCod[0];
               A1553ContagemResultado_CntSrvCod = H00FS5_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00FS5_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = H00FS5_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00FS5_n601ContagemResultado_Servico[0];
               A803ContagemResultado_ContratadaSigla = H00FS5_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00FS5_n803ContagemResultado_ContratadaSigla[0];
               A471ContagemResultado_DataDmn = H00FS5_A471ContagemResultado_DataDmn[0];
               A801ContagemResultado_ServicoSigla = H00FS5_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00FS5_n801ContagemResultado_ServicoSigla[0];
               A456ContagemResultado_Codigo = H00FS5_A456ContagemResultado_Codigo[0];
               A457ContagemResultado_Demanda = H00FS5_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00FS5_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = H00FS5_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00FS5_n493ContagemResultado_DemandaFM[0];
               A52Contratada_AreaTrabalhoCod = H00FS5_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00FS5_n52Contratada_AreaTrabalhoCod[0];
               A1912SolicServicoReqNeg_Agrupador = H00FS5_A1912SolicServicoReqNeg_Agrupador[0];
               A1914SolicServicoReqNeg_Situacao = H00FS5_A1914SolicServicoReqNeg_Situacao[0];
               A1895SolicServicoReqNeg_Codigo = H00FS5_A1895SolicServicoReqNeg_Codigo[0];
               A1934Requisito_Status = H00FS5_A1934Requisito_Status[0];
               A2002Requisito_Prioridade = H00FS5_A2002Requisito_Prioridade[0];
               n2002Requisito_Prioridade = H00FS5_n2002Requisito_Prioridade[0];
               A1927Requisito_Titulo = H00FS5_A1927Requisito_Titulo[0];
               n1927Requisito_Titulo = H00FS5_n1927Requisito_Titulo[0];
               A2001Requisito_Identificador = H00FS5_A2001Requisito_Identificador[0];
               n2001Requisito_Identificador = H00FS5_n2001Requisito_Identificador[0];
               A1919Requisito_Codigo = H00FS5_A1919Requisito_Codigo[0];
               A684ContagemResultado_PFBFSUltima = H00FS5_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00FS5_A685ContagemResultado_PFLFSUltima[0];
               A682ContagemResultado_PFBFMUltima = H00FS5_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00FS5_A683ContagemResultado_PFLFMUltima[0];
               A456ContagemResultado_Codigo = H00FS5_A456ContagemResultado_Codigo[0];
               A1912SolicServicoReqNeg_Agrupador = H00FS5_A1912SolicServicoReqNeg_Agrupador[0];
               A1914SolicServicoReqNeg_Situacao = H00FS5_A1914SolicServicoReqNeg_Situacao[0];
               A490ContagemResultado_ContratadaCod = H00FS5_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = H00FS5_n490ContagemResultado_ContratadaCod[0];
               A1553ContagemResultado_CntSrvCod = H00FS5_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = H00FS5_n1553ContagemResultado_CntSrvCod[0];
               A471ContagemResultado_DataDmn = H00FS5_A471ContagemResultado_DataDmn[0];
               A457ContagemResultado_Demanda = H00FS5_A457ContagemResultado_Demanda[0];
               n457ContagemResultado_Demanda = H00FS5_n457ContagemResultado_Demanda[0];
               A493ContagemResultado_DemandaFM = H00FS5_A493ContagemResultado_DemandaFM[0];
               n493ContagemResultado_DemandaFM = H00FS5_n493ContagemResultado_DemandaFM[0];
               A803ContagemResultado_ContratadaSigla = H00FS5_A803ContagemResultado_ContratadaSigla[0];
               n803ContagemResultado_ContratadaSigla = H00FS5_n803ContagemResultado_ContratadaSigla[0];
               A52Contratada_AreaTrabalhoCod = H00FS5_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = H00FS5_n52Contratada_AreaTrabalhoCod[0];
               A601ContagemResultado_Servico = H00FS5_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = H00FS5_n601ContagemResultado_Servico[0];
               A801ContagemResultado_ServicoSigla = H00FS5_A801ContagemResultado_ServicoSigla[0];
               n801ContagemResultado_ServicoSigla = H00FS5_n801ContagemResultado_ServicoSigla[0];
               A684ContagemResultado_PFBFSUltima = H00FS5_A684ContagemResultado_PFBFSUltima[0];
               A685ContagemResultado_PFLFSUltima = H00FS5_A685ContagemResultado_PFLFSUltima[0];
               A682ContagemResultado_PFBFMUltima = H00FS5_A682ContagemResultado_PFBFMUltima[0];
               A683ContagemResultado_PFLFMUltima = H00FS5_A683ContagemResultado_PFLFMUltima[0];
               A1934Requisito_Status = H00FS5_A1934Requisito_Status[0];
               A2002Requisito_Prioridade = H00FS5_A2002Requisito_Prioridade[0];
               n2002Requisito_Prioridade = H00FS5_n2002Requisito_Prioridade[0];
               A1927Requisito_Titulo = H00FS5_A1927Requisito_Titulo[0];
               n1927Requisito_Titulo = H00FS5_n1927Requisito_Titulo[0];
               A2001Requisito_Identificador = H00FS5_A2001Requisito_Identificador[0];
               n2001Requisito_Identificador = H00FS5_n2001Requisito_Identificador[0];
               /* Execute user event: E15FS2 */
               E15FS2 ();
               pr_default.readNext(1);
            }
            pr_default.close(1);
            wbEnd = 90;
            WBFS0( ) ;
         }
         nGXsfl_90_Refreshing = 0;
      }

      protected int subGridrequisitos_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridrequisitos_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridrequisitos_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridrequisitos_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUPFS0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_demanda_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_demanda_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_demanda_Enabled), 5, 0)));
         edtavContagemresultado_datadmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datadmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datadmn_Enabled), 5, 0)));
         edtavContagemresultado_contratadasigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contratadasigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contratadasigla_Enabled), 5, 0)));
         edtavContagemresultado_pfbfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfs_Enabled), 5, 0)));
         edtavContagemresultado_pflfs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfs_Enabled), 5, 0)));
         edtavContagemresultado_pfbfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfm_Enabled), 5, 0)));
         edtavContagemresultado_pflfm_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfm_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfm_Enabled), 5, 0)));
         edtavContagemresultado_datadmn2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_datadmn2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_datadmn2_Enabled), 5, 0)));
         edtavContagemresultado_contratadasigla2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_contratadasigla2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_contratadasigla2_Enabled), 5, 0)));
         edtavContagemresultado_pflfs2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfs2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfs2_Enabled), 5, 0)));
         edtavContagemresultado_pfbfs2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfs2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfs2_Enabled), 5, 0)));
         edtavContagemresultado_pfbfm2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pfbfm2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pfbfm2_Enabled), 5, 0)));
         edtavContagemresultado_pflfm2_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContagemresultado_pflfm2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContagemresultado_pflfm2_Enabled), 5, 0)));
         edtavServico_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavServico_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavServico_sigla_Enabled), 5, 0)));
         edtavAgrupador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAgrupador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAgrupador_Enabled), 5, 0)));
         edtavOs_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOs_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOs_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13FS2 */
         E13FS2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV18ContagemResultado_Demanda = StringUtil.Upper( cgiGet( edtavContagemresultado_demanda_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_Demanda", AV18ContagemResultado_Demanda);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18ContagemResultado_Demanda, "@!"))));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data da Demanda"}), 1, "vCONTAGEMRESULTADO_DATADMN");
               GX_FocusControl = edtavContagemresultado_datadmn_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContagemResultado_DataDmn = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_DataDmn", context.localUtil.Format(AV17ContagemResultado_DataDmn, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( "", AV17ContagemResultado_DataDmn));
            }
            else
            {
               AV17ContagemResultado_DataDmn = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_DataDmn", context.localUtil.Format(AV17ContagemResultado_DataDmn, "99/99/99"));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( "", AV17ContagemResultado_DataDmn));
            }
            AV30ContagemResultado_ContratadaSigla = StringUtil.Upper( cgiGet( edtavContagemresultado_contratadasigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_ContratadaSigla", AV30ContagemResultado_ContratadaSigla);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTRATADASIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30ContagemResultado_ContratadaSigla, "@!"))));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFBFS");
               GX_FocusControl = edtavContagemresultado_pfbfs_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20ContagemResultado_PFBFS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV20ContagemResultado_PFBFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV20ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV20ContagemResultado_PFBFS = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV20ContagemResultado_PFBFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV20ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFLFS");
               GX_FocusControl = edtavContagemresultado_pflfs_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22ContagemResultado_PFLFS = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV22ContagemResultado_PFLFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFLFS", GetSecureSignedToken( "", context.localUtil.Format( AV22ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV22ContagemResultado_PFLFS = context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV22ContagemResultado_PFLFS, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFLFS", GetSecureSignedToken( "", context.localUtil.Format( AV22ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfm_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFBFM");
               GX_FocusControl = edtavContagemresultado_pfbfm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19ContagemResultado_PFBFM = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV19ContagemResultado_PFBFM, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV19ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV19ContagemResultado_PFBFM = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV19ContagemResultado_PFBFM, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV19ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfm_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfm_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFLFM");
               GX_FocusControl = edtavContagemresultado_pflfm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21ContagemResultado_PFLFM = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV21ContagemResultado_PFLFM, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFLFM", GetSecureSignedToken( "", context.localUtil.Format( AV21ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
            }
            else
            {
               AV21ContagemResultado_PFLFM = context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV21ContagemResultado_PFLFM, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFLFM", GetSecureSignedToken( "", context.localUtil.Format( AV21ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
            }
            AV27Filtro = cgiGet( edtavFiltro_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Filtro", AV27Filtro);
            AV32Filtro2 = cgiGet( edtavFiltro2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Filtro2", AV32Filtro2);
            dynavContagemresultado_demanda2.Name = dynavContagemresultado_demanda2_Internalname;
            dynavContagemresultado_demanda2.CurrentValue = cgiGet( dynavContagemresultado_demanda2_Internalname);
            AV5ContagemResultado_Demanda2 = (int)(NumberUtil.Val( cgiGet( dynavContagemresultado_demanda2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Demanda2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0)));
            if ( context.localUtil.VCDateTime( cgiGet( edtavContagemresultado_datadmn2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data da Demanda"}), 1, "vCONTAGEMRESULTADO_DATADMN2");
               GX_FocusControl = edtavContagemresultado_datadmn2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11ContagemResultado_DataDmn2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_DataDmn2", context.localUtil.Format(AV11ContagemResultado_DataDmn2, "99/99/99"));
            }
            else
            {
               AV11ContagemResultado_DataDmn2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavContagemresultado_datadmn2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_DataDmn2", context.localUtil.Format(AV11ContagemResultado_DataDmn2, "99/99/99"));
            }
            AV31ContagemResultado_ContratadaSigla2 = StringUtil.Upper( cgiGet( edtavContagemresultado_contratadasigla2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_ContratadaSigla2", AV31ContagemResultado_ContratadaSigla2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs2_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFLFS2");
               GX_FocusControl = edtavContagemresultado_pflfs2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8ContagemResultado_PFLFS2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_PFLFS2", StringUtil.LTrim( StringUtil.Str( AV8ContagemResultado_PFLFS2, 14, 5)));
            }
            else
            {
               AV8ContagemResultado_PFLFS2 = context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfs2_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_PFLFS2", StringUtil.LTrim( StringUtil.Str( AV8ContagemResultado_PFLFS2, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs2_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFBFS2");
               GX_FocusControl = edtavContagemresultado_pfbfs2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7ContagemResultado_PFBFS2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_PFBFS2", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_PFBFS2, 14, 5)));
            }
            else
            {
               AV7ContagemResultado_PFBFS2 = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfs2_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_PFBFS2", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_PFBFS2, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfm2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfm2_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFBFM2");
               GX_FocusControl = edtavContagemresultado_pfbfm2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV9ContagemResultado_PFBFM2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_PFBFM2", StringUtil.LTrim( StringUtil.Str( AV9ContagemResultado_PFBFM2, 14, 5)));
            }
            else
            {
               AV9ContagemResultado_PFBFM2 = context.localUtil.CToN( cgiGet( edtavContagemresultado_pfbfm2_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_PFBFM2", StringUtil.LTrim( StringUtil.Str( AV9ContagemResultado_PFBFM2, 14, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfm2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfm2_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTAGEMRESULTADO_PFLFM2");
               GX_FocusControl = edtavContagemresultado_pflfm2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV10ContagemResultado_PFLFM2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_PFLFM2", StringUtil.LTrim( StringUtil.Str( AV10ContagemResultado_PFLFM2, 14, 5)));
            }
            else
            {
               AV10ContagemResultado_PFLFM2 = context.localUtil.CToN( cgiGet( edtavContagemresultado_pflfm2_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_PFLFM2", StringUtil.LTrim( StringUtil.Str( AV10ContagemResultado_PFLFM2, 14, 5)));
            }
            AV28Servico_Sigla = StringUtil.Upper( cgiGet( edtavServico_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Servico_Sigla", AV28Servico_Sigla);
            /* Read saved values. */
            nRC_GXsfl_90 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_90"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13FS2 */
         E13FS2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13FS2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV29WWPContext) ;
         bttConfirmar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttConfirmar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttConfirmar_Enabled), 5, 0)));
         bttConfirmar_Tooltiptext = "Indicar demanda v�lida para vincular";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttConfirmar_Internalname, "Tooltiptext", bttConfirmar_Tooltiptext);
         /* Using cursor H00FS7 */
         pr_default.execute(2, new Object[] {AV13ContagemResultado_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A490ContagemResultado_ContratadaCod = H00FS7_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00FS7_n490ContagemResultado_ContratadaCod[0];
            A456ContagemResultado_Codigo = H00FS7_A456ContagemResultado_Codigo[0];
            A803ContagemResultado_ContratadaSigla = H00FS7_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = H00FS7_n803ContagemResultado_ContratadaSigla[0];
            A493ContagemResultado_DemandaFM = H00FS7_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00FS7_n493ContagemResultado_DemandaFM[0];
            A471ContagemResultado_DataDmn = H00FS7_A471ContagemResultado_DataDmn[0];
            A805ContagemResultado_ContratadaOrigemCod = H00FS7_A805ContagemResultado_ContratadaOrigemCod[0];
            n805ContagemResultado_ContratadaOrigemCod = H00FS7_n805ContagemResultado_ContratadaOrigemCod[0];
            A684ContagemResultado_PFBFSUltima = H00FS7_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00FS7_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = H00FS7_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = H00FS7_A683ContagemResultado_PFLFMUltima[0];
            A803ContagemResultado_ContratadaSigla = H00FS7_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = H00FS7_n803ContagemResultado_ContratadaSigla[0];
            A684ContagemResultado_PFBFSUltima = H00FS7_A684ContagemResultado_PFBFSUltima[0];
            A685ContagemResultado_PFLFSUltima = H00FS7_A685ContagemResultado_PFLFSUltima[0];
            A682ContagemResultado_PFBFMUltima = H00FS7_A682ContagemResultado_PFBFMUltima[0];
            A683ContagemResultado_PFLFMUltima = H00FS7_A683ContagemResultado_PFLFMUltima[0];
            AV30ContagemResultado_ContratadaSigla = A803ContagemResultado_ContratadaSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30ContagemResultado_ContratadaSigla", AV30ContagemResultado_ContratadaSigla);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CONTRATADASIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30ContagemResultado_ContratadaSigla, "@!"))));
            AV18ContagemResultado_Demanda = A493ContagemResultado_DemandaFM;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ContagemResultado_Demanda", AV18ContagemResultado_Demanda);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DEMANDA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV18ContagemResultado_Demanda, "@!"))));
            AV20ContagemResultado_PFBFS = A684ContagemResultado_PFBFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20ContagemResultado_PFBFS", StringUtil.LTrim( StringUtil.Str( AV20ContagemResultado_PFBFS, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFBFS", GetSecureSignedToken( "", context.localUtil.Format( AV20ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")));
            AV22ContagemResultado_PFLFS = A685ContagemResultado_PFLFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22ContagemResultado_PFLFS", StringUtil.LTrim( StringUtil.Str( AV22ContagemResultado_PFLFS, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFLFS", GetSecureSignedToken( "", context.localUtil.Format( AV22ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")));
            AV19ContagemResultado_PFBFM = A682ContagemResultado_PFBFMUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ContagemResultado_PFBFM", StringUtil.LTrim( StringUtil.Str( AV19ContagemResultado_PFBFM, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFBFM", GetSecureSignedToken( "", context.localUtil.Format( AV19ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")));
            AV21ContagemResultado_PFLFM = A683ContagemResultado_PFLFMUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContagemResultado_PFLFM", StringUtil.LTrim( StringUtil.Str( AV21ContagemResultado_PFLFM, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_PFLFM", GetSecureSignedToken( "", context.localUtil.Format( AV21ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")));
            AV17ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContagemResultado_DataDmn", context.localUtil.Format(AV17ContagemResultado_DataDmn, "99/99/99"));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_DATADMN", GetSecureSignedToken( "", AV17ContagemResultado_DataDmn));
            AV12Contratada_Codigo = A805ContagemResultado_ContratadaOrigemCod;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void E11FS2( )
      {
         /* Contagemresultado_demanda2_Click Routine */
         AV46GXLvl26 = 0;
         /* Using cursor H00FS8 */
         pr_default.execute(3, new Object[] {AV5ContagemResultado_Demanda2});
         while ( (pr_default.getStatus(3) != 101) )
         {
            AV46GXLvl26 = 1;
            AV31ContagemResultado_ContratadaSigla2 = A803ContagemResultado_ContratadaSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ContagemResultado_ContratadaSigla2", AV31ContagemResultado_ContratadaSigla2);
            AV7ContagemResultado_PFBFS2 = A684ContagemResultado_PFBFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContagemResultado_PFBFS2", StringUtil.LTrim( StringUtil.Str( AV7ContagemResultado_PFBFS2, 14, 5)));
            AV8ContagemResultado_PFLFS2 = A685ContagemResultado_PFLFSUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ContagemResultado_PFLFS2", StringUtil.LTrim( StringUtil.Str( AV8ContagemResultado_PFLFS2, 14, 5)));
            AV9ContagemResultado_PFBFM2 = A682ContagemResultado_PFBFMUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ContagemResultado_PFBFM2", StringUtil.LTrim( StringUtil.Str( AV9ContagemResultado_PFBFM2, 14, 5)));
            AV10ContagemResultado_PFLFM2 = A683ContagemResultado_PFLFMUltima;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ContagemResultado_PFLFM2", StringUtil.LTrim( StringUtil.Str( AV10ContagemResultado_PFLFM2, 14, 5)));
            AV11ContagemResultado_DataDmn2 = A471ContagemResultado_DataDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContagemResultado_DataDmn2", context.localUtil.Format(AV11ContagemResultado_DataDmn2, "99/99/99"));
            AV28Servico_Sigla = A801ContagemResultado_ServicoSigla;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Servico_Sigla", AV28Servico_Sigla);
            bttConfirmar_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttConfirmar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttConfirmar_Enabled), 5, 0)));
            bttConfirmar_Caption = "Vincular demandas";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttConfirmar_Internalname, "Caption", bttConfirmar_Caption);
            bttConfirmar_Tooltiptext = "Vincular demandas";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttConfirmar_Internalname, "Tooltiptext", bttConfirmar_Tooltiptext);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
         if ( AV46GXLvl26 == 0 )
         {
            bttConfirmar_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttConfirmar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttConfirmar_Enabled), 5, 0)));
            bttConfirmar_Tooltiptext = "Vincular demandas";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttConfirmar_Internalname, "Tooltiptext", bttConfirmar_Tooltiptext);
            GX_msglist.addItem("Indique uma demanda v�lida para vincular");
         }
      }

      protected void E12FS2( )
      {
         /* 'Enter' Routine */
         /* Start For Each Line */
         nRC_GXsfl_90 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_90"), ",", "."));
         nGXsfl_90_fel_idx = 0;
         while ( nGXsfl_90_fel_idx < nRC_GXsfl_90 )
         {
            nGXsfl_90_fel_idx = (short)(((subGridrequisitos_Islastpage==1)&&(nGXsfl_90_fel_idx+1>subGridrequisitos_Recordsperpage( )) ? 1 : nGXsfl_90_fel_idx+1));
            sGXsfl_90_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_902( ) ;
            AV35Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
            A1919Requisito_Codigo = (int)(context.localUtil.CToN( cgiGet( edtRequisito_Codigo_Internalname), ",", "."));
            AV34Agrupador = cgiGet( edtavAgrupador_Internalname);
            A2001Requisito_Identificador = cgiGet( edtRequisito_Identificador_Internalname);
            n2001Requisito_Identificador = false;
            A1927Requisito_Titulo = cgiGet( edtRequisito_Titulo_Internalname);
            n1927Requisito_Titulo = false;
            cmbRequisito_Prioridade.Name = cmbRequisito_Prioridade_Internalname;
            cmbRequisito_Prioridade.CurrentValue = cgiGet( cmbRequisito_Prioridade_Internalname);
            A2002Requisito_Prioridade = (short)(NumberUtil.Val( cgiGet( cmbRequisito_Prioridade_Internalname), "."));
            n2002Requisito_Prioridade = false;
            cmbRequisito_Status.Name = cmbRequisito_Status_Internalname;
            cmbRequisito_Status.CurrentValue = cgiGet( cmbRequisito_Status_Internalname);
            A1934Requisito_Status = (short)(NumberUtil.Val( cgiGet( cmbRequisito_Status_Internalname), "."));
            AV33OS = cgiGet( edtavOs_Internalname);
            if ( AV35Selected )
            {
               AV42Requisitos.Add(A1919Requisito_Codigo, 0);
            }
            /* End For Each Line */
         }
         if ( nGXsfl_90_fel_idx == 0 )
         {
            nGXsfl_90_idx = 1;
            sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
            SubsflControlProps_902( ) ;
         }
         nGXsfl_90_fel_idx = 1;
         AV38WebSession.Set("Requisitos", AV42Requisitos.ToXml(false, true, "Collection", ""));
         new prc_vincularos(context ).execute( ref  AV13ContagemResultado_Codigo,  AV5ContagemResultado_Demanda2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5ContagemResultado_Demanda2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)AV13ContagemResultado_Codigo,(bool)AV26Confirmado});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42Requisitos", AV42Requisitos);
      }

      protected void E14FS2( )
      {
         /* Gridrequisitos_Refresh Routine */
         AV34Agrupador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV34Agrupador);
         AV39OSVisible = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39OSVisible", AV39OSVisible);
         tblTblrequisitos_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblrequisitos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblrequisitos_Visible), 5, 0)));
      }

      private void E15FS2( )
      {
         /* Gridrequisitos_Load Routine */
         if ( StringUtil.StrCmp(AV34Agrupador, A1912SolicServicoReqNeg_Agrupador) == 0 )
         {
            edtavAgrupador_Forecolor = GXUtil.RGB( 255, 255, 255);
         }
         else
         {
            edtavAgrupador_Forecolor = GXUtil.RGB( 0, 0, 0);
            AV34Agrupador = A1912SolicServicoReqNeg_Agrupador;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavAgrupador_Internalname, AV34Agrupador);
         }
         if ( A1914SolicServicoReqNeg_Situacao == 1 )
         {
            AV33OS = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavOs_Internalname, AV33OS);
         }
         else
         {
            new prc_requisitoatendidopor(context ).execute( ref  A1895SolicServicoReqNeg_Codigo, out  AV33OS, out  AV41Tooltiptext, ref  AV39OSVisible) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1895SolicServicoReqNeg_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1895SolicServicoReqNeg_Codigo), 10, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavOs_Internalname, AV33OS);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39OSVisible", AV39OSVisible);
            edtavOs_Tooltiptext = AV41Tooltiptext;
         }
         edtavOs_Visible = (AV39OSVisible ? 1 : 0);
         tblTblrequisitos_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTblrequisitos_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTblrequisitos_Visible), 5, 0)));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 90;
         }
         sendrow_902( ) ;
         if ( isFullAjaxMode( ) && ( nGXsfl_90_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(90, GridrequisitosRow);
         }
      }

      protected void wb_table1_3_FS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(785), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"8\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:27px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Demanda:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_demanda_Internalname, AV18ContagemResultado_Demanda, StringUtil.RTrim( context.localUtil.Format( AV18ContagemResultado_Demanda, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_demanda_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_demanda_Enabled, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock10_Internalname, "Data:", "", "", lblTextblock10_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'" + sGXsfl_90_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn_Internalname, context.localUtil.Format(AV17ContagemResultado_DataDmn, "99/99/99"), context.localUtil.Format( AV17ContagemResultado_DataDmn, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,17);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_datadmn_Enabled, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_VincularOs.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_datadmn_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_VincularOs.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:26px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "da Contratada:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_contratadasigla_Internalname, StringUtil.RTrim( AV30ContagemResultado_ContratadaSigla), StringUtil.RTrim( context.localUtil.Format( AV30ContagemResultado_ContratadaSigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,25);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_contratadasigla_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_contratadasigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "PF FS:", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pfbfs_Internalname, StringUtil.LTrim( StringUtil.NToC( AV20ContagemResultado_PFBFS, 14, 5, ",", "")), ((edtavContagemresultado_pfbfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV20ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV20ContagemResultado_PFBFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pfbfs_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pfbfs_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_VincularOs.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pflfs_Internalname, StringUtil.LTrim( StringUtil.NToC( AV22ContagemResultado_PFLFS, 14, 5, ",", "")), ((edtavContagemresultado_pflfs_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV22ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV22ContagemResultado_PFLFS, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pflfs_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pflfs_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock7_Internalname, "PF FM:", "", "", lblTextblock7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pfbfm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV19ContagemResultado_PFBFM, 14, 5, ",", "")), ((edtavContagemresultado_pfbfm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV19ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV19ContagemResultado_PFBFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pfbfm_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pfbfm_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_VincularOs.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pflfm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV21ContagemResultado_PFLFM, 14, 5, ",", "")), ((edtavContagemresultado_pflfm_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV21ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV21ContagemResultado_PFLFM, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pflfm_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pflfm_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock9_Internalname, "Com", "", "", lblTextblock9_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp; ") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltro_Internalname, AV27Filtro, StringUtil.RTrim( context.localUtil.Format( AV27Filtro, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "N� OS", edtavFiltro_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"6\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFiltro2_Internalname, AV32Filtro2, StringUtil.RTrim( context.localUtil.Format( AV32Filtro2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "N� OS Refer�ncia", edtavFiltro2_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Demanda:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_90_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContagemresultado_demanda2, dynavContagemresultado_demanda2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0)), 1, dynavContagemresultado_demanda2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTAGEMRESULTADO_DEMANDA2.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "", true, "HLP_WP_VincularOs.htm");
            dynavContagemresultado_demanda2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContagemresultado_demanda2_Internalname, "Values", (String)(dynavContagemresultado_demanda2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock11_Internalname, "Data:", "", "", lblTextblock11_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_90_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavContagemresultado_datadmn2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_datadmn2_Internalname, context.localUtil.Format(AV11ContagemResultado_DataDmn2, "99/99/99"), context.localUtil.Format( AV11ContagemResultado_DataDmn2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_datadmn2_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_datadmn2_Enabled, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_VincularOs.htm");
            GxWebStd.gx_bitmap( context, edtavContagemresultado_datadmn2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavContagemresultado_datadmn2_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_VincularOs.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "da Contratada:", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_contratadasigla2_Internalname, StringUtil.RTrim( AV31ContagemResultado_ContratadaSigla2), StringUtil.RTrim( context.localUtil.Format( AV31ContagemResultado_ContratadaSigla2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_contratadasigla2_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_contratadasigla2_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "PF FS:", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pflfs2_Internalname, StringUtil.LTrim( StringUtil.NToC( AV8ContagemResultado_PFLFS2, 14, 5, ",", "")), ((edtavContagemresultado_pflfs2_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV8ContagemResultado_PFLFS2, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV8ContagemResultado_PFLFS2, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pflfs2_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pflfs2_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_VincularOs.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pfbfs2_Internalname, StringUtil.LTrim( StringUtil.NToC( AV7ContagemResultado_PFBFS2, 14, 5, ",", "")), ((edtavContagemresultado_pfbfs2_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV7ContagemResultado_PFBFS2, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV7ContagemResultado_PFBFS2, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pfbfs2_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pfbfs2_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock8_Internalname, "PF FM:", "", "", lblTextblock8_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pfbfm2_Internalname, StringUtil.LTrim( StringUtil.NToC( AV9ContagemResultado_PFBFM2, 14, 5, ",", "")), ((edtavContagemresultado_pfbfm2_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV9ContagemResultado_PFBFM2, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV9ContagemResultado_PFBFM2, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pfbfm2_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pfbfm2_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_VincularOs.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContagemresultado_pflfm2_Internalname, StringUtil.LTrim( StringUtil.NToC( AV10ContagemResultado_PFLFM2, 14, 5, ",", "")), ((edtavContagemresultado_pflfm2_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV10ContagemResultado_PFLFM2, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV10ContagemResultado_PFLFM2, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContagemresultado_pflfm2_Jsonclick, 0, "Attribute", "", "", "", 1, edtavContagemresultado_pflfm2_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock12_Internalname, "Servi�o:", "", "", lblTextblock12_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_90_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavServico_sigla_Internalname, StringUtil.RTrim( AV28Servico_Sigla), StringUtil.RTrim( context.localUtil.Format( AV28Servico_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavServico_sigla_Jsonclick, 0, "Attribute", "", "", "", 1, edtavServico_sigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"9\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"9\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table2_84_FS2( true) ;
         }
         else
         {
            wb_table2_84_FS2( false) ;
         }
         return  ;
      }

      protected void wb_table2_84_FS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"9\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:middle;height:100px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttConfirmar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(90), 2, 0)+","+"null"+");", bttConfirmar_Caption, bttConfirmar_Jsonclick, 5, bttConfirmar_Tooltiptext, "", StyleString, ClassString, 1, bttConfirmar_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ENTER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_VincularOs.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(90), 2, 0)+","+"null"+");", "Fechar", bttButton2_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_FS2e( true) ;
         }
         else
         {
            wb_table1_3_FS2e( false) ;
         }
      }

      protected void wb_table2_84_FS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTblrequisitos_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTblrequisitos_Internalname, tblTblrequisitos_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock13_Internalname, "Trazendo os Requisitos:", "", "", lblTextblock13_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_VincularOs.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridrequisitosContainer.SetWrapped(nGXWrapped);
            if ( GridrequisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridrequisitosContainer"+"DivS\" data-gxgridid=\"90\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridrequisitos_Internalname, subGridrequisitos_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridrequisitos_Backcolorstyle == 0 )
               {
                  subGridrequisitos_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridrequisitos_Class) > 0 )
                  {
                     subGridrequisitos_Linesclass = subGridrequisitos_Class+"Title";
                  }
               }
               else
               {
                  subGridrequisitos_Titlebackstyle = 1;
                  if ( subGridrequisitos_Backcolorstyle == 1 )
                  {
                     subGridrequisitos_Titlebackcolor = subGridrequisitos_Allbackcolor;
                     if ( StringUtil.Len( subGridrequisitos_Class) > 0 )
                     {
                        subGridrequisitos_Linesclass = subGridrequisitos_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridrequisitos_Class) > 0 )
                     {
                        subGridrequisitos_Linesclass = subGridrequisitos_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Incluir") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+"display:none;"+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "C�digo do Requisito") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Agrupador") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Identificador") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "T�tulo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Prioridade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+""+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "Status") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridrequisitos_Linesclass+"\" "+" style=\""+((edtavOs_Visible==0) ? "display:none;" : "")+"color:#000000;"+"\" "+">") ;
               context.SendWebValue( "OS") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridrequisitosContainer.AddObjectProperty("GridName", "Gridrequisitos");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridrequisitosContainer = new GXWebGrid( context);
               }
               else
               {
                  GridrequisitosContainer.Clear();
               }
               GridrequisitosContainer.SetWrapped(nGXWrapped);
               GridrequisitosContainer.AddObjectProperty("GridName", "Gridrequisitos");
               GridrequisitosContainer.AddObjectProperty("Class", "Grid");
               GridrequisitosContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Backcolorstyle), 1, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Backcoloreven", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Backcoloreven), 9, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Titleforecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Titleforecolor), 9, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("CmpContext", "");
               GridrequisitosContainer.AddObjectProperty("InMasterPage", "false");
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV35Selected));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ".", "")));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Value", AV34Agrupador);
               GridrequisitosColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAgrupador_Forecolor), 9, 0, ".", "")));
               GridrequisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAgrupador_Enabled), 5, 0, ".", "")));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Value", A2001Requisito_Identificador);
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Value", A1927Requisito_Titulo);
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2002Requisito_Prioridade), 2, 0, ".", "")));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1934Requisito_Status), 4, 0, ".", "")));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridrequisitosColumn.AddObjectProperty("Value", AV33OS);
               GridrequisitosColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavOs_Enabled), 5, 0, ".", "")));
               GridrequisitosColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavOs_Tooltiptext));
               GridrequisitosColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavOs_Visible), 5, 0, ".", "")));
               GridrequisitosContainer.AddColumnProperties(GridrequisitosColumn);
               GridrequisitosContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Allowselection), 1, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Selectioncolor), 9, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Allowhovering), 1, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Hoveringcolor), 9, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Allowcollapsing), 1, 0, ".", "")));
               GridrequisitosContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrequisitos_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 90 )
         {
            wbEnd = 0;
            nRC_GXsfl_90 = (short)(nGXsfl_90_idx-1);
            if ( GridrequisitosContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridrequisitosContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridrequisitos", GridrequisitosContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridrequisitosContainerData", GridrequisitosContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridrequisitosContainerData"+"V", GridrequisitosContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridrequisitosContainerData"+"V"+"\" value='"+GridrequisitosContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_84_FS2e( true) ;
         }
         else
         {
            wb_table2_84_FS2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV13ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContagemResultado_Codigo), 6, 0)));
         AV26Confirmado = (bool)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Confirmado", AV26Confirmado);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAFS2( ) ;
         WSFS2( ) ;
         WEFS2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202032423463755");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_vincularos.js", "?202032423463755");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_902( )
      {
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_90_idx;
         edtRequisito_Codigo_Internalname = "REQUISITO_CODIGO_"+sGXsfl_90_idx;
         edtavAgrupador_Internalname = "vAGRUPADOR_"+sGXsfl_90_idx;
         edtRequisito_Identificador_Internalname = "REQUISITO_IDENTIFICADOR_"+sGXsfl_90_idx;
         edtRequisito_Titulo_Internalname = "REQUISITO_TITULO_"+sGXsfl_90_idx;
         cmbRequisito_Prioridade_Internalname = "REQUISITO_PRIORIDADE_"+sGXsfl_90_idx;
         cmbRequisito_Status_Internalname = "REQUISITO_STATUS_"+sGXsfl_90_idx;
         edtavOs_Internalname = "vOS_"+sGXsfl_90_idx;
      }

      protected void SubsflControlProps_fel_902( )
      {
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_90_fel_idx;
         edtRequisito_Codigo_Internalname = "REQUISITO_CODIGO_"+sGXsfl_90_fel_idx;
         edtavAgrupador_Internalname = "vAGRUPADOR_"+sGXsfl_90_fel_idx;
         edtRequisito_Identificador_Internalname = "REQUISITO_IDENTIFICADOR_"+sGXsfl_90_fel_idx;
         edtRequisito_Titulo_Internalname = "REQUISITO_TITULO_"+sGXsfl_90_fel_idx;
         cmbRequisito_Prioridade_Internalname = "REQUISITO_PRIORIDADE_"+sGXsfl_90_fel_idx;
         cmbRequisito_Status_Internalname = "REQUISITO_STATUS_"+sGXsfl_90_fel_idx;
         edtavOs_Internalname = "vOS_"+sGXsfl_90_fel_idx;
      }

      protected void sendrow_902( )
      {
         SubsflControlProps_902( ) ;
         WBFS0( ) ;
         GridrequisitosRow = GXWebRow.GetNew(context,GridrequisitosContainer);
         if ( subGridrequisitos_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridrequisitos_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridrequisitos_Class, "") != 0 )
            {
               subGridrequisitos_Linesclass = subGridrequisitos_Class+"Odd";
            }
         }
         else if ( subGridrequisitos_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridrequisitos_Backstyle = 0;
            subGridrequisitos_Backcolor = subGridrequisitos_Allbackcolor;
            if ( StringUtil.StrCmp(subGridrequisitos_Class, "") != 0 )
            {
               subGridrequisitos_Linesclass = subGridrequisitos_Class+"Uniform";
            }
         }
         else if ( subGridrequisitos_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridrequisitos_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridrequisitos_Class, "") != 0 )
            {
               subGridrequisitos_Linesclass = subGridrequisitos_Class+"Odd";
            }
            subGridrequisitos_Backcolor = (int)(0xF0F0F0);
         }
         else if ( subGridrequisitos_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridrequisitos_Backstyle = 1;
            if ( ((int)((nGXsfl_90_idx) % (2))) == 0 )
            {
               subGridrequisitos_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subGridrequisitos_Class, "") != 0 )
               {
                  subGridrequisitos_Linesclass = subGridrequisitos_Class+"Even";
               }
            }
            else
            {
               subGridrequisitos_Backcolor = (int)(0xF0F0F0);
               if ( StringUtil.StrCmp(subGridrequisitos_Class, "") != 0 )
               {
                  subGridrequisitos_Linesclass = subGridrequisitos_Class+"Odd";
               }
            }
         }
         if ( GridrequisitosContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subGridrequisitos_Linesclass+"\" style=\""+((subGridrequisitos_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGridrequisitos_Backcolor)+";")+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_90_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridrequisitosContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Check box */
         TempTags = " " + ((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 91,'',false,'"+sGXsfl_90_idx+"',90)\"" : " ");
         ClassString = "Attribute";
         StyleString = ((subGridrequisitos_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGridrequisitos_Backcolor)+";");
         GridrequisitosRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavSelected_Internalname,StringUtil.BoolToStr( AV35Selected),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(91, this, 'true', 'false');gx.evt.onchange(this);\" " : " ")+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,91);\"" : " ")});
         /* Subfile cell */
         if ( GridrequisitosContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridrequisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1919Requisito_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1919Requisito_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Codigo_Jsonclick,(short)0,(String)"Attribute",((subGridrequisitos_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGridrequisitos_Backcolor)+";"),(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridrequisitosContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavAgrupador_Enabled!=0)&&(edtavAgrupador_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 93,'',false,'"+sGXsfl_90_idx+"',90)\"" : " ");
         ROClassString = "Attribute";
         GridrequisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavAgrupador_Internalname,(String)AV34Agrupador,(String)"",TempTags+((edtavAgrupador_Enabled!=0)&&(edtavAgrupador_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavAgrupador_Enabled!=0)&&(edtavAgrupador_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,93);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavAgrupador_Jsonclick,(short)0,(String)"Attribute","color:"+context.BuildHTMLColor( edtavAgrupador_Forecolor)+";"+((subGridrequisitos_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGridrequisitos_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(int)edtavAgrupador_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)25,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridrequisitosContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridrequisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Identificador_Internalname,(String)A2001Requisito_Identificador,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Identificador_Jsonclick,(short)0,(String)"Attribute",((subGridrequisitos_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGridrequisitos_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"ReqIdentificador",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridrequisitosContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute";
         GridrequisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRequisito_Titulo_Internalname,(String)A1927Requisito_Titulo,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRequisito_Titulo_Jsonclick,(short)0,(String)"Attribute",((subGridrequisitos_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGridrequisitos_Backcolor)+";"),(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)250,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"ReqNome",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridrequisitosContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         GXCCtl = "REQUISITO_PRIORIDADE_" + sGXsfl_90_idx;
         cmbRequisito_Prioridade.Name = GXCCtl;
         cmbRequisito_Prioridade.WebTags = "";
         cmbRequisito_Prioridade.addItem("1", "Alta", 0);
         cmbRequisito_Prioridade.addItem("2", "Alta M�dia", 0);
         cmbRequisito_Prioridade.addItem("3", "Alta Baixa", 0);
         cmbRequisito_Prioridade.addItem("4", "M�dia Alta", 0);
         cmbRequisito_Prioridade.addItem("5", "M�dia M�dia", 0);
         cmbRequisito_Prioridade.addItem("6", "M�dia Baixa", 0);
         cmbRequisito_Prioridade.addItem("7", "Baixa Alta", 0);
         cmbRequisito_Prioridade.addItem("8", "Baixa M�dia", 0);
         cmbRequisito_Prioridade.addItem("9", "Baixa Baixa", 0);
         if ( cmbRequisito_Prioridade.ItemCount > 0 )
         {
            A2002Requisito_Prioridade = (short)(NumberUtil.Val( cmbRequisito_Prioridade.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2002Requisito_Prioridade), 2, 0))), "."));
            n2002Requisito_Prioridade = false;
         }
         /* ComboBox */
         GridrequisitosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbRequisito_Prioridade,(String)cmbRequisito_Prioridade_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A2002Requisito_Prioridade), 2, 0)),(short)1,(String)cmbRequisito_Prioridade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",((subGridrequisitos_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGridrequisitos_Backcolor)+";"),(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
         cmbRequisito_Prioridade.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2002Requisito_Prioridade), 2, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRequisito_Prioridade_Internalname, "Values", (String)(cmbRequisito_Prioridade.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridrequisitosContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         GXCCtl = "REQUISITO_STATUS_" + sGXsfl_90_idx;
         cmbRequisito_Status.Name = GXCCtl;
         cmbRequisito_Status.WebTags = "";
         cmbRequisito_Status.addItem("0", "Rascunho", 0);
         cmbRequisito_Status.addItem("1", "Solicitado", 0);
         cmbRequisito_Status.addItem("2", "Aprovado", 0);
         cmbRequisito_Status.addItem("3", "N�o Aprovado", 0);
         cmbRequisito_Status.addItem("4", "Pausado", 0);
         cmbRequisito_Status.addItem("5", "Cancelado", 0);
         if ( cmbRequisito_Status.ItemCount > 0 )
         {
            A1934Requisito_Status = (short)(NumberUtil.Val( cmbRequisito_Status.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0))), "."));
         }
         /* ComboBox */
         GridrequisitosRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbRequisito_Status,(String)cmbRequisito_Status_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0)),(short)1,(String)cmbRequisito_Status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",((subGridrequisitos_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGridrequisitos_Backcolor)+";"),(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
         cmbRequisito_Status.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1934Requisito_Status), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRequisito_Status_Internalname, "Values", (String)(cmbRequisito_Status.ToJavascriptSource()));
         /* Subfile cell */
         if ( GridrequisitosContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtavOs_Visible==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavOs_Enabled!=0)&&(edtavOs_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 98,'',false,'"+sGXsfl_90_idx+"',90)\"" : " ");
         ROClassString = "Attribute";
         GridrequisitosRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavOs_Internalname,(String)AV33OS,(String)"",TempTags+((edtavOs_Enabled!=0)&&(edtavOs_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavOs_Enabled!=0)&&(edtavOs_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,98);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)edtavOs_Tooltiptext,(String)"",(String)edtavOs_Jsonclick,(short)0,(String)"Attribute",((subGridrequisitos_Backcolor==-1) ? "" : "background-color:"+context.BuildHTMLColor( subGridrequisitos_Backcolor)+";"),(String)ROClassString,(String)"",(int)edtavOs_Visible,(int)edtavOs_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)90,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         GxWebStd.gx_hidden_field( context, "gxhash_REQUISITO_CODIGO"+"_"+sGXsfl_90_idx, GetSecureSignedToken( sGXsfl_90_idx, context.localUtil.Format( (decimal)(A1919Requisito_Codigo), "ZZZZZ9")));
         GridrequisitosContainer.AddRow(GridrequisitosRow);
         nGXsfl_90_idx = (short)(((subGridrequisitos_Islastpage==1)&&(nGXsfl_90_idx+1>subGridrequisitos_Recordsperpage( )) ? 1 : nGXsfl_90_idx+1));
         sGXsfl_90_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_90_idx), 4, 0)), 4, "0");
         SubsflControlProps_902( ) ;
         /* End function sendrow_902 */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavContagemresultado_demanda_Internalname = "vCONTAGEMRESULTADO_DEMANDA";
         lblTextblock10_Internalname = "TEXTBLOCK10";
         edtavContagemresultado_datadmn_Internalname = "vCONTAGEMRESULTADO_DATADMN";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavContagemresultado_contratadasigla_Internalname = "vCONTAGEMRESULTADO_CONTRATADASIGLA";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavContagemresultado_pfbfs_Internalname = "vCONTAGEMRESULTADO_PFBFS";
         edtavContagemresultado_pflfs_Internalname = "vCONTAGEMRESULTADO_PFLFS";
         lblTextblock7_Internalname = "TEXTBLOCK7";
         edtavContagemresultado_pfbfm_Internalname = "vCONTAGEMRESULTADO_PFBFM";
         edtavContagemresultado_pflfm_Internalname = "vCONTAGEMRESULTADO_PFLFM";
         lblTextblock9_Internalname = "TEXTBLOCK9";
         edtavFiltro_Internalname = "vFILTRO";
         edtavFiltro2_Internalname = "vFILTRO2";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         dynavContagemresultado_demanda2_Internalname = "vCONTAGEMRESULTADO_DEMANDA2";
         lblTextblock11_Internalname = "TEXTBLOCK11";
         edtavContagemresultado_datadmn2_Internalname = "vCONTAGEMRESULTADO_DATADMN2";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         edtavContagemresultado_contratadasigla2_Internalname = "vCONTAGEMRESULTADO_CONTRATADASIGLA2";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         edtavContagemresultado_pflfs2_Internalname = "vCONTAGEMRESULTADO_PFLFS2";
         edtavContagemresultado_pfbfs2_Internalname = "vCONTAGEMRESULTADO_PFBFS2";
         lblTextblock8_Internalname = "TEXTBLOCK8";
         edtavContagemresultado_pfbfm2_Internalname = "vCONTAGEMRESULTADO_PFBFM2";
         edtavContagemresultado_pflfm2_Internalname = "vCONTAGEMRESULTADO_PFLFM2";
         lblTextblock12_Internalname = "TEXTBLOCK12";
         edtavServico_sigla_Internalname = "vSERVICO_SIGLA";
         lblTextblock13_Internalname = "TEXTBLOCK13";
         chkavSelected_Internalname = "vSELECTED";
         edtRequisito_Codigo_Internalname = "REQUISITO_CODIGO";
         edtavAgrupador_Internalname = "vAGRUPADOR";
         edtRequisito_Identificador_Internalname = "REQUISITO_IDENTIFICADOR";
         edtRequisito_Titulo_Internalname = "REQUISITO_TITULO";
         cmbRequisito_Prioridade_Internalname = "REQUISITO_PRIORIDADE";
         cmbRequisito_Status_Internalname = "REQUISITO_STATUS";
         edtavOs_Internalname = "vOS";
         tblTblrequisitos_Internalname = "TBLREQUISITOS";
         bttConfirmar_Internalname = "CONFIRMAR";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
         subGridrequisitos_Internalname = "GRIDREQUISITOS";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavOs_Jsonclick = "";
         cmbRequisito_Status_Jsonclick = "";
         cmbRequisito_Prioridade_Jsonclick = "";
         edtRequisito_Titulo_Jsonclick = "";
         edtRequisito_Identificador_Jsonclick = "";
         edtavAgrupador_Jsonclick = "";
         edtavAgrupador_Visible = -1;
         edtRequisito_Codigo_Jsonclick = "";
         chkavSelected.Visible = -1;
         chkavSelected.Enabled = 1;
         subGridrequisitos_Allowcollapsing = 0;
         subGridrequisitos_Allowselection = 0;
         edtavOs_Tooltiptext = "";
         edtavOs_Enabled = 1;
         edtavAgrupador_Enabled = 1;
         edtavAgrupador_Forecolor = (int)(0x000000);
         edtavOs_Visible = -1;
         subGridrequisitos_Class = "Grid";
         bttConfirmar_Enabled = 1;
         edtavServico_sigla_Jsonclick = "";
         edtavServico_sigla_Enabled = 1;
         edtavContagemresultado_pflfm2_Jsonclick = "";
         edtavContagemresultado_pflfm2_Enabled = 1;
         edtavContagemresultado_pfbfm2_Jsonclick = "";
         edtavContagemresultado_pfbfm2_Enabled = 1;
         edtavContagemresultado_pfbfs2_Jsonclick = "";
         edtavContagemresultado_pfbfs2_Enabled = 1;
         edtavContagemresultado_pflfs2_Jsonclick = "";
         edtavContagemresultado_pflfs2_Enabled = 1;
         edtavContagemresultado_contratadasigla2_Jsonclick = "";
         edtavContagemresultado_contratadasigla2_Enabled = 1;
         edtavContagemresultado_datadmn2_Jsonclick = "";
         edtavContagemresultado_datadmn2_Enabled = 1;
         dynavContagemresultado_demanda2_Jsonclick = "";
         edtavFiltro2_Jsonclick = "";
         edtavFiltro_Jsonclick = "";
         edtavContagemresultado_pflfm_Jsonclick = "";
         edtavContagemresultado_pflfm_Enabled = 1;
         edtavContagemresultado_pfbfm_Jsonclick = "";
         edtavContagemresultado_pfbfm_Enabled = 1;
         edtavContagemresultado_pflfs_Jsonclick = "";
         edtavContagemresultado_pflfs_Enabled = 1;
         edtavContagemresultado_pfbfs_Jsonclick = "";
         edtavContagemresultado_pfbfs_Enabled = 1;
         edtavContagemresultado_contratadasigla_Jsonclick = "";
         edtavContagemresultado_contratadasigla_Enabled = 1;
         edtavContagemresultado_datadmn_Jsonclick = "";
         edtavContagemresultado_datadmn_Enabled = 1;
         edtavContagemresultado_demanda_Jsonclick = "";
         edtavContagemresultado_demanda_Enabled = 1;
         tblTblrequisitos_Visible = 1;
         bttConfirmar_Caption = "Vincular demanda";
         bttConfirmar_Tooltiptext = "Vincular demanda";
         subGridrequisitos_Titleforecolor = (int)(0x000000);
         subGridrequisitos_Backcoloreven = (int)(0xFFFFFF);
         subGridrequisitos_Backcolorstyle = 3;
         chkavSelected.Caption = "";
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Vincular";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public void Validv_Filtro( wwpbaseobjects.SdtWWPContext GX_Parm1 ,
                                 String GX_Parm2 ,
                                 String GX_Parm3 ,
                                 int GX_Parm4 ,
                                 GXCombobox dynGX_Parm5 )
      {
         AV29WWPContext = GX_Parm1;
         AV27Filtro = GX_Parm2;
         AV32Filtro2 = GX_Parm3;
         AV13ContagemResultado_Codigo = GX_Parm4;
         dynavContagemresultado_demanda2 = dynGX_Parm5;
         AV5ContagemResultado_Demanda2 = (int)(NumberUtil.Val( dynavContagemresultado_demanda2.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_DEMANDA2_htmlFS2( AV29WWPContext, AV27Filtro, AV32Filtro2, AV13ContagemResultado_Codigo) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_demanda2.ItemCount > 0 )
         {
            AV5ContagemResultado_Demanda2 = (int)(NumberUtil.Val( dynavContagemresultado_demanda2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0))), "."));
         }
         dynavContagemresultado_demanda2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0));
         isValidOutput.Add(dynavContagemresultado_demanda2);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Validv_Filtro2( wwpbaseobjects.SdtWWPContext GX_Parm1 ,
                                  String GX_Parm2 ,
                                  String GX_Parm3 ,
                                  int GX_Parm4 ,
                                  GXCombobox dynGX_Parm5 )
      {
         AV29WWPContext = GX_Parm1;
         AV27Filtro = GX_Parm2;
         AV32Filtro2 = GX_Parm3;
         AV13ContagemResultado_Codigo = GX_Parm4;
         dynavContagemresultado_demanda2 = dynGX_Parm5;
         AV5ContagemResultado_Demanda2 = (int)(NumberUtil.Val( dynavContagemresultado_demanda2.CurrentValue, "."));
         GXVvCONTAGEMRESULTADO_DEMANDA2_htmlFS2( AV29WWPContext, AV27Filtro, AV32Filtro2, AV13ContagemResultado_Codigo) ;
         dynload_actions( ) ;
         if ( dynavContagemresultado_demanda2.ItemCount > 0 )
         {
            AV5ContagemResultado_Demanda2 = (int)(NumberUtil.Val( dynavContagemresultado_demanda2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0))), "."));
         }
         dynavContagemresultado_demanda2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5ContagemResultado_Demanda2), 6, 0));
         isValidOutput.Add(dynavContagemresultado_demanda2);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDREQUISITOS_nFirstRecordOnPage',nv:0},{av:'GRIDREQUISITOS_nEOF',nv:0},{av:'AV5ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'ZZZZZ9',nv:0},{av:'AV34Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'A1912SolicServicoReqNeg_Agrupador',fld:'SOLICSERVICOREQNEG_AGRUPADOR',pic:'',nv:''},{av:'A1914SolicServicoReqNeg_Situacao',fld:'SOLICSERVICOREQNEG_SITUACAO',pic:'Z9',nv:0},{av:'A1895SolicServicoReqNeg_Codigo',fld:'SOLICSERVICOREQNEG_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV39OSVisible',fld:'vOSVISIBLE',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("VCONTAGEMRESULTADO_DEMANDA2.CLICK","{handler:'E11FS2',iparms:[{av:'A456ContagemResultado_Codigo',fld:'CONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'ZZZZZ9',nv:0},{av:'A803ContagemResultado_ContratadaSigla',fld:'CONTAGEMRESULTADO_CONTRATADASIGLA',pic:'@!',nv:''},{av:'A684ContagemResultado_PFBFSUltima',fld:'CONTAGEMRESULTADO_PFBFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A685ContagemResultado_PFLFSUltima',fld:'CONTAGEMRESULTADO_PFLFSULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A682ContagemResultado_PFBFMUltima',fld:'CONTAGEMRESULTADO_PFBFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A683ContagemResultado_PFLFMUltima',fld:'CONTAGEMRESULTADO_PFLFMULTIMA',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'A471ContagemResultado_DataDmn',fld:'CONTAGEMRESULTADO_DATADMN',pic:'',nv:''},{av:'A801ContagemResultado_ServicoSigla',fld:'CONTAGEMRESULTADO_SERVICOSIGLA',pic:'@!',nv:''}],oparms:[{av:'AV31ContagemResultado_ContratadaSigla2',fld:'vCONTAGEMRESULTADO_CONTRATADASIGLA2',pic:'@!',nv:''},{av:'AV7ContagemResultado_PFBFS2',fld:'vCONTAGEMRESULTADO_PFBFS2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV8ContagemResultado_PFLFS2',fld:'vCONTAGEMRESULTADO_PFLFS2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV9ContagemResultado_PFBFM2',fld:'vCONTAGEMRESULTADO_PFBFM2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV10ContagemResultado_PFLFM2',fld:'vCONTAGEMRESULTADO_PFLFM2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV11ContagemResultado_DataDmn2',fld:'vCONTAGEMRESULTADO_DATADMN2',pic:'',nv:''},{av:'AV28Servico_Sigla',fld:'vSERVICO_SIGLA',pic:'@!',nv:''},{ctrl:'CONFIRMAR',prop:'Enabled'},{ctrl:'CONFIRMAR',prop:'Caption'},{ctrl:'CONFIRMAR',prop:'Tooltiptext'}]}");
         setEventMetadata("'ENTER'","{handler:'E12FS2',iparms:[{av:'AV35Selected',fld:'vSELECTED',grid:90,pic:'',nv:false},{av:'A1919Requisito_Codigo',fld:'REQUISITO_CODIGO',grid:90,pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV42Requisitos',fld:'vREQUISITOS',pic:'',nv:null},{av:'AV13ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5ContagemResultado_Demanda2',fld:'vCONTAGEMRESULTADO_DEMANDA2',pic:'ZZZZZ9',nv:0},{av:'AV26Confirmado',fld:'vCONFIRMADO',pic:'',nv:false}],oparms:[{av:'AV42Requisitos',fld:'vREQUISITOS',pic:'',nv:null},{av:'AV13ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRIDREQUISITOS.REFRESH","{handler:'E14FS2',iparms:[],oparms:[{av:'AV34Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'AV39OSVisible',fld:'vOSVISIBLE',pic:'',nv:false},{av:'tblTblrequisitos_Visible',ctrl:'TBLREQUISITOS',prop:'Visible'}]}");
         setEventMetadata("GRIDREQUISITOS.LOAD","{handler:'E15FS2',iparms:[{av:'AV34Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'A1912SolicServicoReqNeg_Agrupador',fld:'SOLICSERVICOREQNEG_AGRUPADOR',pic:'',nv:''},{av:'A1914SolicServicoReqNeg_Situacao',fld:'SOLICSERVICOREQNEG_SITUACAO',pic:'Z9',nv:0},{av:'A1895SolicServicoReqNeg_Codigo',fld:'SOLICSERVICOREQNEG_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'AV39OSVisible',fld:'vOSVISIBLE',pic:'',nv:false}],oparms:[{av:'AV34Agrupador',fld:'vAGRUPADOR',pic:'',nv:''},{av:'edtavAgrupador_Forecolor',ctrl:'vAGRUPADOR',prop:'Forecolor'},{av:'AV39OSVisible',fld:'vOSVISIBLE',pic:'',nv:false},{av:'A1895SolicServicoReqNeg_Codigo',fld:'SOLICSERVICOREQNEG_CODIGO',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavOs_Tooltiptext',ctrl:'vOS',prop:'Tooltiptext'},{av:'AV33OS',fld:'vOS',pic:'',nv:''},{av:'edtavOs_Visible',ctrl:'vOS',prop:'Visible'},{av:'tblTblrequisitos_Visible',ctrl:'TBLREQUISITOS',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV29WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Filtro = "";
         AV32Filtro2 = "";
         AV34Agrupador = "";
         A1912SolicServicoReqNeg_Agrupador = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A803ContagemResultado_ContratadaSigla = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         A801ContagemResultado_ServicoSigla = "";
         AV42Requisitos = new GxSimpleCollection();
         AV18ContagemResultado_Demanda = "";
         AV17ContagemResultado_DataDmn = DateTime.MinValue;
         AV30ContagemResultado_ContratadaSigla = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A2001Requisito_Identificador = "";
         A1927Requisito_Titulo = "";
         AV33OS = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00FS2_A601ContagemResultado_Servico = new int[1] ;
         H00FS2_n601ContagemResultado_Servico = new bool[] {false} ;
         H00FS2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00FS2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00FS2_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00FS2_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00FS2_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00FS2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00FS2_A456ContagemResultado_Codigo = new int[1] ;
         H00FS2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00FS2_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00FS2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00FS2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00FS2_A457ContagemResultado_Demanda = new String[] {""} ;
         H00FS2_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00FS2_A1815ContagemResultado_DmnSrvPrst = new String[] {""} ;
         GridrequisitosContainer = new GXWebGrid( context);
         H00FS5_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00FS5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00FS5_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00FS5_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00FS5_A601ContagemResultado_Servico = new int[1] ;
         H00FS5_n601ContagemResultado_Servico = new bool[] {false} ;
         H00FS5_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00FS5_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         H00FS5_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00FS5_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         H00FS5_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         H00FS5_A456ContagemResultado_Codigo = new int[1] ;
         H00FS5_A457ContagemResultado_Demanda = new String[] {""} ;
         H00FS5_n457ContagemResultado_Demanda = new bool[] {false} ;
         H00FS5_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00FS5_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00FS5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00FS5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         H00FS5_A1912SolicServicoReqNeg_Agrupador = new String[] {""} ;
         H00FS5_A1914SolicServicoReqNeg_Situacao = new short[1] ;
         H00FS5_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         H00FS5_A1934Requisito_Status = new short[1] ;
         H00FS5_A2002Requisito_Prioridade = new short[1] ;
         H00FS5_n2002Requisito_Prioridade = new bool[] {false} ;
         H00FS5_A1927Requisito_Titulo = new String[] {""} ;
         H00FS5_n1927Requisito_Titulo = new bool[] {false} ;
         H00FS5_A2001Requisito_Identificador = new String[] {""} ;
         H00FS5_n2001Requisito_Identificador = new bool[] {false} ;
         H00FS5_A1919Requisito_Codigo = new int[1] ;
         H00FS5_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00FS5_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         H00FS5_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00FS5_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         A457ContagemResultado_Demanda = "";
         A493ContagemResultado_DemandaFM = "";
         AV11ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV31ContagemResultado_ContratadaSigla2 = "";
         AV28Servico_Sigla = "";
         H00FS7_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00FS7_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00FS7_A456ContagemResultado_Codigo = new int[1] ;
         H00FS7_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         H00FS7_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         H00FS7_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00FS7_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00FS7_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         H00FS7_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         H00FS7_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         H00FS7_A684ContagemResultado_PFBFSUltima = new decimal[1] ;
         H00FS7_A685ContagemResultado_PFLFSUltima = new decimal[1] ;
         H00FS7_A682ContagemResultado_PFBFMUltima = new decimal[1] ;
         H00FS7_A683ContagemResultado_PFLFMUltima = new decimal[1] ;
         H00FS8_A456ContagemResultado_Codigo = new int[1] ;
         AV38WebSession = context.GetSession();
         AV41Tooltiptext = "";
         GridrequisitosRow = new GXWebRow();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock1_Jsonclick = "";
         TempTags = "";
         lblTextblock10_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock7_Jsonclick = "";
         lblTextblock9_Jsonclick = "";
         lblTextblock3_Jsonclick = "";
         lblTextblock11_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         lblTextblock8_Jsonclick = "";
         lblTextblock12_Jsonclick = "";
         bttConfirmar_Jsonclick = "";
         bttButton2_Jsonclick = "";
         lblTextblock13_Jsonclick = "";
         subGridrequisitos_Linesclass = "";
         GridrequisitosColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_vincularos__default(),
            new Object[][] {
                new Object[] {
               H00FS2_A601ContagemResultado_Servico, H00FS2_n601ContagemResultado_Servico, H00FS2_A1553ContagemResultado_CntSrvCod, H00FS2_n1553ContagemResultado_CntSrvCod, H00FS2_A805ContagemResultado_ContratadaOrigemCod, H00FS2_n805ContagemResultado_ContratadaOrigemCod, H00FS2_A490ContagemResultado_ContratadaCod, H00FS2_n490ContagemResultado_ContratadaCod, H00FS2_A456ContagemResultado_Codigo, H00FS2_A52Contratada_AreaTrabalhoCod,
               H00FS2_n52Contratada_AreaTrabalhoCod, H00FS2_A493ContagemResultado_DemandaFM, H00FS2_n493ContagemResultado_DemandaFM, H00FS2_A457ContagemResultado_Demanda, H00FS2_n457ContagemResultado_Demanda, H00FS2_A1815ContagemResultado_DmnSrvPrst
               }
               , new Object[] {
               H00FS5_A490ContagemResultado_ContratadaCod, H00FS5_n490ContagemResultado_ContratadaCod, H00FS5_A1553ContagemResultado_CntSrvCod, H00FS5_n1553ContagemResultado_CntSrvCod, H00FS5_A601ContagemResultado_Servico, H00FS5_n601ContagemResultado_Servico, H00FS5_A803ContagemResultado_ContratadaSigla, H00FS5_n803ContagemResultado_ContratadaSigla, H00FS5_A471ContagemResultado_DataDmn, H00FS5_A801ContagemResultado_ServicoSigla,
               H00FS5_n801ContagemResultado_ServicoSigla, H00FS5_A456ContagemResultado_Codigo, H00FS5_A457ContagemResultado_Demanda, H00FS5_n457ContagemResultado_Demanda, H00FS5_A493ContagemResultado_DemandaFM, H00FS5_n493ContagemResultado_DemandaFM, H00FS5_A52Contratada_AreaTrabalhoCod, H00FS5_n52Contratada_AreaTrabalhoCod, H00FS5_A1912SolicServicoReqNeg_Agrupador, H00FS5_A1914SolicServicoReqNeg_Situacao,
               H00FS5_A1895SolicServicoReqNeg_Codigo, H00FS5_A1934Requisito_Status, H00FS5_A2002Requisito_Prioridade, H00FS5_n2002Requisito_Prioridade, H00FS5_A1927Requisito_Titulo, H00FS5_n1927Requisito_Titulo, H00FS5_A2001Requisito_Identificador, H00FS5_n2001Requisito_Identificador, H00FS5_A1919Requisito_Codigo, H00FS5_A684ContagemResultado_PFBFSUltima,
               H00FS5_A685ContagemResultado_PFLFSUltima, H00FS5_A682ContagemResultado_PFBFMUltima, H00FS5_A683ContagemResultado_PFLFMUltima
               }
               , new Object[] {
               H00FS7_A490ContagemResultado_ContratadaCod, H00FS7_n490ContagemResultado_ContratadaCod, H00FS7_A456ContagemResultado_Codigo, H00FS7_A803ContagemResultado_ContratadaSigla, H00FS7_n803ContagemResultado_ContratadaSigla, H00FS7_A493ContagemResultado_DemandaFM, H00FS7_n493ContagemResultado_DemandaFM, H00FS7_A471ContagemResultado_DataDmn, H00FS7_A805ContagemResultado_ContratadaOrigemCod, H00FS7_n805ContagemResultado_ContratadaOrigemCod,
               H00FS7_A684ContagemResultado_PFBFSUltima, H00FS7_A685ContagemResultado_PFLFSUltima, H00FS7_A682ContagemResultado_PFBFMUltima, H00FS7_A683ContagemResultado_PFLFMUltima
               }
               , new Object[] {
               H00FS8_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavContagemresultado_demanda_Enabled = 0;
         edtavContagemresultado_datadmn_Enabled = 0;
         edtavContagemresultado_contratadasigla_Enabled = 0;
         edtavContagemresultado_pfbfs_Enabled = 0;
         edtavContagemresultado_pflfs_Enabled = 0;
         edtavContagemresultado_pfbfm_Enabled = 0;
         edtavContagemresultado_pflfm_Enabled = 0;
         edtavContagemresultado_datadmn2_Enabled = 0;
         edtavContagemresultado_contratadasigla2_Enabled = 0;
         edtavContagemresultado_pflfs2_Enabled = 0;
         edtavContagemresultado_pfbfs2_Enabled = 0;
         edtavContagemresultado_pfbfm2_Enabled = 0;
         edtavContagemresultado_pflfm2_Enabled = 0;
         edtavServico_sigla_Enabled = 0;
         edtavAgrupador_Enabled = 0;
         edtavOs_Enabled = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_90 ;
      private short nGXsfl_90_idx=1 ;
      private short A1914SolicServicoReqNeg_Situacao ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short A2002Requisito_Prioridade ;
      private short A1934Requisito_Status ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_90_Refreshing=0 ;
      private short subGridrequisitos_Backcolorstyle ;
      private short AV46GXLvl26 ;
      private short nGXsfl_90_fel_idx=1 ;
      private short subGridrequisitos_Titlebackstyle ;
      private short subGridrequisitos_Allowselection ;
      private short subGridrequisitos_Allowhovering ;
      private short subGridrequisitos_Allowcollapsing ;
      private short subGridrequisitos_Collapsed ;
      private short nGXWrapped ;
      private short subGridrequisitos_Backstyle ;
      private short wbTemp ;
      private short GRIDREQUISITOS_nEOF ;
      private int AV13ContagemResultado_Codigo ;
      private int wcpOAV13ContagemResultado_Codigo ;
      private int AV5ContagemResultado_Demanda2 ;
      private int A456ContagemResultado_Codigo ;
      private int lblTbjava_Visible ;
      private int A1919Requisito_Codigo ;
      private int gxdynajaxindex ;
      private int subGridrequisitos_Islastpage ;
      private int edtavContagemresultado_demanda_Enabled ;
      private int edtavContagemresultado_datadmn_Enabled ;
      private int edtavContagemresultado_contratadasigla_Enabled ;
      private int edtavContagemresultado_pfbfs_Enabled ;
      private int edtavContagemresultado_pflfs_Enabled ;
      private int edtavContagemresultado_pfbfm_Enabled ;
      private int edtavContagemresultado_pflfm_Enabled ;
      private int edtavContagemresultado_datadmn2_Enabled ;
      private int edtavContagemresultado_contratadasigla2_Enabled ;
      private int edtavContagemresultado_pflfs2_Enabled ;
      private int edtavContagemresultado_pfbfs2_Enabled ;
      private int edtavContagemresultado_pfbfm2_Enabled ;
      private int edtavContagemresultado_pflfm2_Enabled ;
      private int edtavServico_sigla_Enabled ;
      private int edtavAgrupador_Enabled ;
      private int edtavOs_Enabled ;
      private int subGridrequisitos_Backcoloreven ;
      private int subGridrequisitos_Titleforecolor ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int bttConfirmar_Enabled ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int AV12Contratada_Codigo ;
      private int tblTblrequisitos_Visible ;
      private int edtavAgrupador_Forecolor ;
      private int edtavOs_Visible ;
      private int subGridrequisitos_Titlebackcolor ;
      private int subGridrequisitos_Allbackcolor ;
      private int subGridrequisitos_Selectioncolor ;
      private int subGridrequisitos_Hoveringcolor ;
      private int idxLst ;
      private int subGridrequisitos_Backcolor ;
      private int edtavAgrupador_Visible ;
      private long A1895SolicServicoReqNeg_Codigo ;
      private long GRIDREQUISITOS_nCurrentRecord ;
      private long GRIDREQUISITOS_nFirstRecordOnPage ;
      private decimal A684ContagemResultado_PFBFSUltima ;
      private decimal A685ContagemResultado_PFLFSUltima ;
      private decimal A682ContagemResultado_PFBFMUltima ;
      private decimal A683ContagemResultado_PFLFMUltima ;
      private decimal AV20ContagemResultado_PFBFS ;
      private decimal AV22ContagemResultado_PFLFS ;
      private decimal AV19ContagemResultado_PFBFM ;
      private decimal AV21ContagemResultado_PFLFM ;
      private decimal AV8ContagemResultado_PFLFS2 ;
      private decimal AV7ContagemResultado_PFBFS2 ;
      private decimal AV9ContagemResultado_PFBFM2 ;
      private decimal AV10ContagemResultado_PFLFM2 ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_90_idx="0001" ;
      private String edtavAgrupador_Internalname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private String AV30ContagemResultado_ContratadaSigla ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavSelected_Internalname ;
      private String edtRequisito_Codigo_Internalname ;
      private String edtRequisito_Identificador_Internalname ;
      private String edtRequisito_Titulo_Internalname ;
      private String cmbRequisito_Prioridade_Internalname ;
      private String cmbRequisito_Status_Internalname ;
      private String edtavOs_Internalname ;
      private String GXCCtl ;
      private String edtavContagemresultado_demanda_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavContagemresultado_datadmn_Internalname ;
      private String edtavContagemresultado_contratadasigla_Internalname ;
      private String edtavContagemresultado_pfbfs_Internalname ;
      private String edtavContagemresultado_pflfs_Internalname ;
      private String edtavContagemresultado_pfbfm_Internalname ;
      private String edtavContagemresultado_pflfm_Internalname ;
      private String edtavContagemresultado_datadmn2_Internalname ;
      private String edtavContagemresultado_contratadasigla2_Internalname ;
      private String edtavContagemresultado_pflfs2_Internalname ;
      private String edtavContagemresultado_pfbfs2_Internalname ;
      private String edtavContagemresultado_pfbfm2_Internalname ;
      private String edtavContagemresultado_pflfm2_Internalname ;
      private String edtavServico_sigla_Internalname ;
      private String edtavFiltro_Internalname ;
      private String edtavFiltro2_Internalname ;
      private String dynavContagemresultado_demanda2_Internalname ;
      private String AV31ContagemResultado_ContratadaSigla2 ;
      private String AV28Servico_Sigla ;
      private String bttConfirmar_Internalname ;
      private String bttConfirmar_Tooltiptext ;
      private String bttConfirmar_Caption ;
      private String sGXsfl_90_fel_idx="0001" ;
      private String tblTblrequisitos_Internalname ;
      private String AV41Tooltiptext ;
      private String edtavOs_Tooltiptext ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String TempTags ;
      private String edtavContagemresultado_demanda_Jsonclick ;
      private String lblTextblock10_Internalname ;
      private String lblTextblock10_Jsonclick ;
      private String edtavContagemresultado_datadmn_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavContagemresultado_contratadasigla_Jsonclick ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtavContagemresultado_pfbfs_Jsonclick ;
      private String edtavContagemresultado_pflfs_Jsonclick ;
      private String lblTextblock7_Internalname ;
      private String lblTextblock7_Jsonclick ;
      private String edtavContagemresultado_pfbfm_Jsonclick ;
      private String edtavContagemresultado_pflfm_Jsonclick ;
      private String lblTextblock9_Internalname ;
      private String lblTextblock9_Jsonclick ;
      private String edtavFiltro_Jsonclick ;
      private String edtavFiltro2_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String dynavContagemresultado_demanda2_Jsonclick ;
      private String lblTextblock11_Internalname ;
      private String lblTextblock11_Jsonclick ;
      private String edtavContagemresultado_datadmn2_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String edtavContagemresultado_contratadasigla2_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String edtavContagemresultado_pflfs2_Jsonclick ;
      private String edtavContagemresultado_pfbfs2_Jsonclick ;
      private String lblTextblock8_Internalname ;
      private String lblTextblock8_Jsonclick ;
      private String edtavContagemresultado_pfbfm2_Jsonclick ;
      private String edtavContagemresultado_pflfm2_Jsonclick ;
      private String lblTextblock12_Internalname ;
      private String lblTextblock12_Jsonclick ;
      private String edtavServico_sigla_Jsonclick ;
      private String bttConfirmar_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private String lblTextblock13_Internalname ;
      private String lblTextblock13_Jsonclick ;
      private String subGridrequisitos_Internalname ;
      private String subGridrequisitos_Class ;
      private String subGridrequisitos_Linesclass ;
      private String ROClassString ;
      private String edtRequisito_Codigo_Jsonclick ;
      private String edtavAgrupador_Jsonclick ;
      private String edtRequisito_Identificador_Jsonclick ;
      private String edtRequisito_Titulo_Jsonclick ;
      private String cmbRequisito_Prioridade_Jsonclick ;
      private String cmbRequisito_Status_Jsonclick ;
      private String edtavOs_Jsonclick ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime AV17ContagemResultado_DataDmn ;
      private DateTime AV11ContagemResultado_DataDmn2 ;
      private bool entryPointCalled ;
      private bool AV39OSVisible ;
      private bool AV26Confirmado ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV35Selected ;
      private bool n2001Requisito_Identificador ;
      private bool n1927Requisito_Titulo ;
      private bool n2002Requisito_Prioridade ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n457ContagemResultado_Demanda ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool returnInSub ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private String AV27Filtro ;
      private String AV32Filtro2 ;
      private String AV34Agrupador ;
      private String A1912SolicServicoReqNeg_Agrupador ;
      private String AV18ContagemResultado_Demanda ;
      private String A2001Requisito_Identificador ;
      private String A1927Requisito_Titulo ;
      private String AV33OS ;
      private String A457ContagemResultado_Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid GridrequisitosContainer ;
      private GXWebRow GridrequisitosRow ;
      private GXWebColumn GridrequisitosColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private GXCombobox dynavContagemresultado_demanda2 ;
      private GXCheckbox chkavSelected ;
      private GXCombobox cmbRequisito_Prioridade ;
      private GXCombobox cmbRequisito_Status ;
      private IDataStoreProvider pr_default ;
      private int[] H00FS2_A601ContagemResultado_Servico ;
      private bool[] H00FS2_n601ContagemResultado_Servico ;
      private int[] H00FS2_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00FS2_n1553ContagemResultado_CntSrvCod ;
      private int[] H00FS2_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00FS2_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] H00FS2_A490ContagemResultado_ContratadaCod ;
      private bool[] H00FS2_n490ContagemResultado_ContratadaCod ;
      private int[] H00FS2_A456ContagemResultado_Codigo ;
      private int[] H00FS2_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00FS2_n52Contratada_AreaTrabalhoCod ;
      private String[] H00FS2_A493ContagemResultado_DemandaFM ;
      private bool[] H00FS2_n493ContagemResultado_DemandaFM ;
      private String[] H00FS2_A457ContagemResultado_Demanda ;
      private bool[] H00FS2_n457ContagemResultado_Demanda ;
      private String[] H00FS2_A1815ContagemResultado_DmnSrvPrst ;
      private int[] H00FS5_A490ContagemResultado_ContratadaCod ;
      private bool[] H00FS5_n490ContagemResultado_ContratadaCod ;
      private int[] H00FS5_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00FS5_n1553ContagemResultado_CntSrvCod ;
      private int[] H00FS5_A601ContagemResultado_Servico ;
      private bool[] H00FS5_n601ContagemResultado_Servico ;
      private String[] H00FS5_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00FS5_n803ContagemResultado_ContratadaSigla ;
      private DateTime[] H00FS5_A471ContagemResultado_DataDmn ;
      private String[] H00FS5_A801ContagemResultado_ServicoSigla ;
      private bool[] H00FS5_n801ContagemResultado_ServicoSigla ;
      private int[] H00FS5_A456ContagemResultado_Codigo ;
      private String[] H00FS5_A457ContagemResultado_Demanda ;
      private bool[] H00FS5_n457ContagemResultado_Demanda ;
      private String[] H00FS5_A493ContagemResultado_DemandaFM ;
      private bool[] H00FS5_n493ContagemResultado_DemandaFM ;
      private int[] H00FS5_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00FS5_n52Contratada_AreaTrabalhoCod ;
      private String[] H00FS5_A1912SolicServicoReqNeg_Agrupador ;
      private short[] H00FS5_A1914SolicServicoReqNeg_Situacao ;
      private long[] H00FS5_A1895SolicServicoReqNeg_Codigo ;
      private short[] H00FS5_A1934Requisito_Status ;
      private short[] H00FS5_A2002Requisito_Prioridade ;
      private bool[] H00FS5_n2002Requisito_Prioridade ;
      private String[] H00FS5_A1927Requisito_Titulo ;
      private bool[] H00FS5_n1927Requisito_Titulo ;
      private String[] H00FS5_A2001Requisito_Identificador ;
      private bool[] H00FS5_n2001Requisito_Identificador ;
      private int[] H00FS5_A1919Requisito_Codigo ;
      private decimal[] H00FS5_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00FS5_A685ContagemResultado_PFLFSUltima ;
      private decimal[] H00FS5_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00FS5_A683ContagemResultado_PFLFMUltima ;
      private int[] H00FS7_A490ContagemResultado_ContratadaCod ;
      private bool[] H00FS7_n490ContagemResultado_ContratadaCod ;
      private int[] H00FS7_A456ContagemResultado_Codigo ;
      private String[] H00FS7_A803ContagemResultado_ContratadaSigla ;
      private bool[] H00FS7_n803ContagemResultado_ContratadaSigla ;
      private String[] H00FS7_A493ContagemResultado_DemandaFM ;
      private bool[] H00FS7_n493ContagemResultado_DemandaFM ;
      private DateTime[] H00FS7_A471ContagemResultado_DataDmn ;
      private int[] H00FS7_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] H00FS7_n805ContagemResultado_ContratadaOrigemCod ;
      private decimal[] H00FS7_A684ContagemResultado_PFBFSUltima ;
      private decimal[] H00FS7_A685ContagemResultado_PFLFSUltima ;
      private decimal[] H00FS7_A682ContagemResultado_PFBFMUltima ;
      private decimal[] H00FS7_A683ContagemResultado_PFLFMUltima ;
      private int[] H00FS8_A456ContagemResultado_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private bool aP1_Confirmado ;
      private IGxSession AV38WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV42Requisitos ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV29WWPContext ;
   }

   public class wp_vincularos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00FS2 ;
          prmH00FS2 = new Object[] {
          new Object[] {"@AV27Filtro",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV32Filtro2",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV13ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00FS5 ;
          prmH00FS5 = new Object[] {
          new Object[] {"@AV5ContagemResultado_Demanda2",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00FS5 ;
          cmdBufferH00FS5=" SELECT T3.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T3.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T5.[Servico_Codigo] AS ContagemResultado_Servico, T4.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T3.[ContagemResultado_DataDmn], T6.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T2.[ContagemResultado_Codigo], T3.[ContagemResultado_Demanda], T3.[ContagemResultado_DemandaFM], T4.[Contratada_AreaTrabalhoCod], T2.[SolicServicoReqNeg_Agrupador], T2.[SolicServicoReqNeg_Situacao], T1.[SolicServicoReqNeg_Codigo], T8.[Requisito_Status], T8.[Requisito_Prioridade], T8.[Requisito_Titulo], T8.[Requisito_Identificador], T1.[Requisito_Codigo], COALESCE( T7.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T7.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T7.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T7.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima FROM ((((((([ReqNegReqTec] T1 WITH (NOLOCK) INNER JOIN [SolicitacaoServicoReqNegocio] T2 WITH (NOLOCK) ON T2.[SolicServicoReqNeg_Codigo] = T1.[SolicServicoReqNeg_Codigo]) INNER JOIN [ContagemResultado] T3 WITH (NOLOCK) ON T3.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) LEFT JOIN [Contratada] T4 WITH (NOLOCK) ON T4.[Contratada_Codigo] = T3.[ContagemResultado_ContratadaCod]) LEFT JOIN [ContratoServicos] T5 WITH (NOLOCK) ON T5.[ContratoServicos_Codigo] = T3.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T6 WITH (NOLOCK) ON T6.[Servico_Codigo] = T5.[Servico_Codigo]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) "
          + " AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T7 ON T7.[ContagemResultado_Codigo] = T2.[ContagemResultado_Codigo]) INNER JOIN [Requisito] T8 WITH (NOLOCK) ON T8.[Requisito_Codigo] = T1.[Requisito_Codigo]) WHERE T2.[ContagemResultado_Codigo] = @AV5ContagemResultado_Demanda2 ORDER BY T1.[SolicServicoReqNeg_Codigo], T1.[Requisito_Codigo]" ;
          Object[] prmH00FS7 ;
          prmH00FS7 = new Object[] {
          new Object[] {"@AV13ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00FS8 ;
          prmH00FS8 = new Object[] {
          new Object[] {"@AV5ContagemResultado_Demanda2",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00FS2", "SELECT T5.[Servico_Codigo] AS ContagemResultado_Servico, T4.[ContratoServicos_Codigo] AS ContagemResultado_CntSrvCod, T3.[Contratada_Codigo] AS ContagemResultado_ContratadaOr, T2.[Contratada_Codigo] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], RTRIM(LTRIM(COALESCE( T1.[ContagemResultado_DemandaFM], ''))) + ' ' + RTRIM(LTRIM(COALESCE( T5.[Servico_Sigla], ''))) + ' (' + COALESCE( T2.[Contratada_Sigla], '') + ')' AS ContagemResultado_DmnSrvPrst FROM (((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaOrigemCod]) LEFT JOIN [ContratoServicos] T4 WITH (NOLOCK) ON T4.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T5 WITH (NOLOCK) ON T5.[Servico_Codigo] = T4.[Servico_Codigo]) WHERE (( T1.[ContagemResultado_DemandaFM] = @AV27Filtro and Not (@AV27Filtro = '')) or ( T1.[ContagemResultado_Demanda] = @AV32Filtro2 and Not (@AV32Filtro2 = ''))) AND (T1.[ContagemResultado_Codigo] <> @AV13ContagemResultado_Codigo) AND (T2.[Contratada_AreaTrabalhoCod] = @AV29WWPC_1Areatrabalho_codigo) ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FS2,0,0,true,false )
             ,new CursorDef("H00FS5", cmdBufferH00FS5,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FS5,11,0,true,false )
             ,new CursorDef("H00FS7", "SELECT TOP 1 T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_DataDmn], T1.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, COALESCE( T3.[ContagemResultado_PFBFSUltima], 0) AS ContagemResultado_PFBFSUltima, COALESCE( T3.[ContagemResultado_PFLFSUltima], 0) AS ContagemResultado_PFLFSUltima, COALESCE( T3.[ContagemResultado_PFBFMUltima], 0) AS ContagemResultado_PFBFMUltima, COALESCE( T3.[ContagemResultado_PFLFMUltima], 0) AS ContagemResultado_PFLFMUltima FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_PFBFS]) AS ContagemResultado_PFBFSUltima, [ContagemResultado_Codigo], MIN([ContagemResultado_PFLFS]) AS ContagemResultado_PFLFSUltima, MIN([ContagemResultado_PFBFM]) AS ContagemResultado_PFBFMUltima, MIN([ContagemResultado_PFLFM]) AS ContagemResultado_PFLFMUltima FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T3 ON T3.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV13ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FS7,1,0,false,true )
             ,new CursorDef("H00FS8", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @AV5ContagemResultado_Demanda2 ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FS8,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 15) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((String[]) buf[18])[0] = rslt.getVarchar(11) ;
                ((short[]) buf[19])[0] = rslt.getShort(12) ;
                ((long[]) buf[20])[0] = rslt.getLong(13) ;
                ((short[]) buf[21])[0] = rslt.getShort(14) ;
                ((short[]) buf[22])[0] = rslt.getShort(15) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(15);
                ((String[]) buf[24])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[25])[0] = rslt.wasNull(16);
                ((String[]) buf[26])[0] = rslt.getVarchar(17) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(17);
                ((int[]) buf[28])[0] = rslt.getInt(18) ;
                ((decimal[]) buf[29])[0] = rslt.getDecimal(19) ;
                ((decimal[]) buf[30])[0] = rslt.getDecimal(20) ;
                ((decimal[]) buf[31])[0] = rslt.getDecimal(21) ;
                ((decimal[]) buf[32])[0] = rslt.getDecimal(22) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(5) ;
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[11])[0] = rslt.getDecimal(8) ;
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((decimal[]) buf[13])[0] = rslt.getDecimal(10) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
