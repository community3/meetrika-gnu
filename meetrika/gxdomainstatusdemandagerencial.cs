/*
               File: StatusDemandaGerencial
        Description: StatusDemandaGerencial
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/21/2019 0:17:37.91
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gxdomainstatusdemandagerencial
   {
      private static Hashtable domain = new Hashtable();
      static gxdomainstatusdemandagerencial ()
      {
         domain["Y"] = "Demandas em Aguardo";
         domain["X"] = "Demandas em Aberto";
         domain["Z"] = "Demandas Finalizadas";
         domain["P"] = "A Pagar";
         domain["O"] = "Aceite";
         domain["I"] = "An�lise Planejamento";
         domain["X"] = "Cancelada";
         domain["C"] = "Conferida";
         domain["E"] = "Em An�lise";
         domain["A"] = "Em Execu��o";
         domain["H"] = "Homologada";
         domain["L"] = "Liquidada";
         domain["N"] = "N�o Faturada";
         domain["J"] = "Planejamento";
         domain["U"] = "Rascunho";
         domain["D"] = "Rejeitada";
         domain["R"] = "Resolvida";
         domain["S"] = "Solicitada";
         domain["B"] = "Stand by";
         domain["M"] = "Valida��o Mensura��o";
         domain["Q"] = "Validacao Qualidade";
         domain["T"] = "Validacao T�cnica";
         domain["G"] = "Em Homologa��o";
      }

      public static string getDescription( IGxContext context ,
                                           String key )
      {
         string rtkey ;
         rtkey = StringUtil.Trim( (String)(key));
         return (string)domain[rtkey] ;
      }

      public static GxSimpleCollection getValues( )
      {
         GxSimpleCollection value = new GxSimpleCollection();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (String key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

   }

}
