/*
               File: WP_Alerta
        Description: Alerta
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:7:5.57
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_alerta : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_alerta( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_alerta( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Alerta_Codigo ,
                           int aP1_UserId )
      {
         this.A1881Alerta_Codigo = aP0_Alerta_Codigo;
         this.AV5UserId = aP1_UserId;
         executePrivate();
         aP0_Alerta_Codigo=this.A1881Alerta_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbAlerta_ToGroup = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A1881Alerta_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV5UserId = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5UserId), 6, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAOS2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTOS2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031197559");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_alerta.aspx") + "?" + UrlEncode("" +A1881Alerta_Codigo) + "," + UrlEncode("" +AV5UserId)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "ALERTA_DESCRICAO", A1893Alerta_Descricao);
         GxWebStd.gx_hidden_field( context, "ALERTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1881Alerta_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV5UserId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_ASSUNTO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1892Alerta_Assunto, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vDE_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV10De_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAREATRABALHO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6AreaTrabalho_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_ALERTA_TOGROUP", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPARA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8Para_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "ALERTA_DESCRICAO_Enabled", StringUtil.BoolToStr( Alerta_descricao_Enabled));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEOS2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTOS2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_alerta.aspx") + "?" + UrlEncode("" +A1881Alerta_Codigo) + "," + UrlEncode("" +AV5UserId) ;
      }

      public override String GetPgmname( )
      {
         return "WP_Alerta" ;
      }

      public override String GetPgmdesc( )
      {
         return "Alerta" ;
      }

      protected void WBOS0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_OS2( true) ;
         }
         else
         {
            wb_table1_2_OS2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_OS2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_Alerta.htm");
         }
         wbLoad = true;
      }

      protected void STARTOS2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Alerta", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPOS0( ) ;
      }

      protected void WSOS2( )
      {
         STARTOS2( ) ;
         EVTOS2( ) ;
      }

      protected void EVTOS2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11OS2 */
                              E11OS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12OS2 */
                              E12OS2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E13OS2 */
                                    E13OS2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEOS2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAOS2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbAlerta_ToGroup.Name = "ALERTA_TOGROUP";
            cmbAlerta_ToGroup.WebTags = "";
            cmbAlerta_ToGroup.addItem("0", "(Nenhum)", 0);
            cmbAlerta_ToGroup.addItem("1", "Todos os usu�rios", 0);
            cmbAlerta_ToGroup.addItem("2", "Todos os usu�rios da Contratante", 0);
            cmbAlerta_ToGroup.addItem("3", "Todos os usu�rios da Contratada", 0);
            cmbAlerta_ToGroup.addItem("4", "Todos os gestores", 0);
            cmbAlerta_ToGroup.addItem("5", "Todos os gestores da Contratante", 0);
            cmbAlerta_ToGroup.addItem("6", "Todos os gestores da Contratada", 0);
            if ( cmbAlerta_ToGroup.ItemCount > 0 )
            {
               A1884Alerta_ToGroup = (short)(NumberUtil.Val( cmbAlerta_ToGroup.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0))), "."));
               n1884Alerta_ToGroup = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_ALERTA_TOGROUP", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavDe_nome_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbAlerta_ToGroup.ItemCount > 0 )
         {
            A1884Alerta_ToGroup = (short)(NumberUtil.Val( cmbAlerta_ToGroup.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0))), "."));
            n1884Alerta_ToGroup = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_ALERTA_TOGROUP", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFOS2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavDe_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDe_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDe_nome_Enabled), 5, 0)));
         edtavAreatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0)));
         edtavPara_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPara_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPara_nome_Enabled), 5, 0)));
      }

      protected void RFOS2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00OS5 */
            pr_default.execute(0, new Object[] {A1881Alerta_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1887Alerta_Owner = H00OS5_A1887Alerta_Owner[0];
               A1882Alerta_ToAreaTrabalho = H00OS5_A1882Alerta_ToAreaTrabalho[0];
               n1882Alerta_ToAreaTrabalho = H00OS5_n1882Alerta_ToAreaTrabalho[0];
               A1883Alerta_ToUser = H00OS5_A1883Alerta_ToUser[0];
               n1883Alerta_ToUser = H00OS5_n1883Alerta_ToUser[0];
               A1884Alerta_ToGroup = H00OS5_A1884Alerta_ToGroup[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_ALERTA_TOGROUP", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
               n1884Alerta_ToGroup = H00OS5_n1884Alerta_ToGroup[0];
               A1893Alerta_Descricao = H00OS5_A1893Alerta_Descricao[0];
               A1892Alerta_Assunto = H00OS5_A1892Alerta_Assunto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1892Alerta_Assunto", A1892Alerta_Assunto);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_ALERTA_ASSUNTO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1892Alerta_Assunto, ""))));
               A40000AreaTrabalho_Descricao = H00OS5_A40000AreaTrabalho_Descricao[0];
               n40000AreaTrabalho_Descricao = H00OS5_n40000AreaTrabalho_Descricao[0];
               A40001Usuario_PessoaNom = H00OS5_A40001Usuario_PessoaNom[0];
               n40001Usuario_PessoaNom = H00OS5_n40001Usuario_PessoaNom[0];
               A40002Usuario_PessoaNom = H00OS5_A40002Usuario_PessoaNom[0];
               n40002Usuario_PessoaNom = H00OS5_n40002Usuario_PessoaNom[0];
               A40001Usuario_PessoaNom = H00OS5_A40001Usuario_PessoaNom[0];
               n40001Usuario_PessoaNom = H00OS5_n40001Usuario_PessoaNom[0];
               A40000AreaTrabalho_Descricao = H00OS5_A40000AreaTrabalho_Descricao[0];
               n40000AreaTrabalho_Descricao = H00OS5_n40000AreaTrabalho_Descricao[0];
               A40002Usuario_PessoaNom = H00OS5_A40002Usuario_PessoaNom[0];
               n40002Usuario_PessoaNom = H00OS5_n40002Usuario_PessoaNom[0];
               /* Execute user event: E12OS2 */
               E12OS2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBOS0( ) ;
         }
      }

      protected void STRUPOS0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavDe_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDe_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDe_nome_Enabled), 5, 0)));
         edtavAreatrabalho_descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAreatrabalho_descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAreatrabalho_descricao_Enabled), 5, 0)));
         edtavPara_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPara_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPara_nome_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11OS2 */
         E11OS2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1892Alerta_Assunto = cgiGet( edtAlerta_Assunto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1892Alerta_Assunto", A1892Alerta_Assunto);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_ALERTA_ASSUNTO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A1892Alerta_Assunto, ""))));
            AV10De_Nome = StringUtil.Upper( cgiGet( edtavDe_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10De_Nome", AV10De_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDE_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV10De_Nome, "@!"))));
            AV6AreaTrabalho_Descricao = StringUtil.Upper( cgiGet( edtavAreatrabalho_descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AreaTrabalho_Descricao", AV6AreaTrabalho_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6AreaTrabalho_Descricao, "@!"))));
            cmbAlerta_ToGroup.CurrentValue = cgiGet( cmbAlerta_ToGroup_Internalname);
            A1884Alerta_ToGroup = (short)(NumberUtil.Val( cgiGet( cmbAlerta_ToGroup_Internalname), "."));
            n1884Alerta_ToGroup = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_ALERTA_TOGROUP", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1884Alerta_ToGroup), "ZZZ9")));
            AV8Para_Nome = StringUtil.Upper( cgiGet( edtavPara_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Para_Nome", AV8Para_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8Para_Nome, "@!"))));
            /* Read saved values. */
            A1893Alerta_Descricao = cgiGet( "ALERTA_DESCRICAO");
            Alerta_descricao_Enabled = StringUtil.StrToBool( cgiGet( "ALERTA_DESCRICAO_Enabled"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11OS2 */
         E11OS2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11OS2( )
      {
         /* Start Routine */
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void nextLoad( )
      {
      }

      protected void E12OS2( )
      {
         /* Load Routine */
         if ( A1882Alerta_ToAreaTrabalho > 0 )
         {
            AV6AreaTrabalho_Descricao = A40000AreaTrabalho_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AreaTrabalho_Descricao", AV6AreaTrabalho_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6AreaTrabalho_Descricao, "@!"))));
         }
         else
         {
            AV6AreaTrabalho_Descricao = "(Indiferente)";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AreaTrabalho_Descricao", AV6AreaTrabalho_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAREATRABALHO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV6AreaTrabalho_Descricao, "@!"))));
         }
         AV10De_Nome = A40001Usuario_PessoaNom;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10De_Nome", AV10De_Nome);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vDE_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV10De_Nome, "@!"))));
         if ( A1883Alerta_ToUser > 0 )
         {
            AV8Para_Nome = A40002Usuario_PessoaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Para_Nome", AV8Para_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8Para_Nome, "@!"))));
         }
         else
         {
            AV8Para_Nome = "Todos";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Para_Nome", AV8Para_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPARA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV8Para_Nome, "@!"))));
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E13OS2 */
         E13OS2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13OS2( )
      {
         /* Enter Routine */
         new prc_alertalida(context ).execute(  A1881Alerta_Codigo, ref  AV5UserId) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5UserId), 6, 0)));
         context.setWebReturnParms(new Object[] {(int)A1881Alerta_Codigo});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void wb_table1_2_OS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Assunto:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAlerta_Assunto_Internalname, A1892Alerta_Assunto, StringUtil.RTrim( context.localUtil.Format( A1892Alerta_Assunto, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlerta_Assunto_Jsonclick, 0, "Attribute", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Descri��o:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"ALERTA_DESCRICAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock7_Internalname, "De:", "", "", lblTextblock7_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDe_nome_Internalname, StringUtil.RTrim( AV10De_Nome), StringUtil.RTrim( context.localUtil.Format( AV10De_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,17);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDe_nome_Jsonclick, 0, "Attribute", "", "", "", 1, edtavDe_nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Para:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            context.WriteHtmlText( "<p></p>") ;
            context.WriteHtmlText( "<p>") ;
            wb_table2_22_OS2( true) ;
         }
         else
         {
            wb_table2_22_OS2( false) ;
         }
         return  ;
      }

      protected void wb_table2_22_OS2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Confirmar leitura", bttButton1_Jsonclick, 5, "Confirmar leitura", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_OS2e( true) ;
         }
         else
         {
            wb_table1_2_OS2e( false) ;
         }
      }

      protected void wb_table2_22_OS2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock5_Internalname, "�rea", "", "", lblTextblock5_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_descricao_Internalname, AV6AreaTrabalho_Descricao, StringUtil.RTrim( context.localUtil.Format( AV6AreaTrabalho_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_descricao_Jsonclick, 0, "Attribute", "", "", "", 1, edtavAreatrabalho_descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock4_Internalname, "Grupo", "", "", lblTextblock4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbAlerta_ToGroup, cmbAlerta_ToGroup_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)), 1, cmbAlerta_ToGroup_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", "", true, "HLP_WP_Alerta.htm");
            cmbAlerta_ToGroup.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAlerta_ToGroup_Internalname, "Values", (String)(cmbAlerta_ToGroup.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock6_Internalname, "Usu�rio", "", "", lblTextblock6_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPara_nome_Internalname, StringUtil.RTrim( AV8Para_Nome), StringUtil.RTrim( context.localUtil.Format( AV8Para_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPara_nome_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPara_nome_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_WP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_22_OS2e( true) ;
         }
         else
         {
            wb_table2_22_OS2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1881Alerta_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
         AV5UserId = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5UserId), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAOS2( ) ;
         WSOS2( ) ;
         WEOS2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031197588");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_alerta.js", "?202031197588");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtAlerta_Assunto_Internalname = "ALERTA_ASSUNTO";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         Alerta_descricao_Internalname = "ALERTA_DESCRICAO";
         lblTextblock7_Internalname = "TEXTBLOCK7";
         edtavDe_nome_Internalname = "vDE_NOME";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         lblTextblock5_Internalname = "TEXTBLOCK5";
         edtavAreatrabalho_descricao_Internalname = "vAREATRABALHO_DESCRICAO";
         lblTextblock4_Internalname = "TEXTBLOCK4";
         cmbAlerta_ToGroup_Internalname = "ALERTA_TOGROUP";
         lblTextblock6_Internalname = "TEXTBLOCK6";
         edtavPara_nome_Internalname = "vPARA_NOME";
         tblTable2_Internalname = "TABLE2";
         bttButton1_Internalname = "BUTTON1";
         tblTable1_Internalname = "TABLE1";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavPara_nome_Jsonclick = "";
         edtavPara_nome_Enabled = 1;
         cmbAlerta_ToGroup_Jsonclick = "";
         edtavAreatrabalho_descricao_Jsonclick = "";
         edtavAreatrabalho_descricao_Enabled = 1;
         edtavDe_nome_Jsonclick = "";
         edtavDe_nome_Enabled = 1;
         Alerta_descricao_Enabled = Convert.ToBoolean( 0);
         edtAlerta_Assunto_Jsonclick = "";
         lblTbjava_Caption = "Script";
         lblTbjava_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Alerta";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E13OS2',iparms:[{av:'A1881Alerta_Codigo',fld:'ALERTA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5UserId',fld:'vUSERID',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV5UserId',fld:'vUSERID',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A1893Alerta_Descricao = "";
         A1892Alerta_Assunto = "";
         AV10De_Nome = "";
         AV6AreaTrabalho_Descricao = "";
         AV8Para_Nome = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00OS5_A1887Alerta_Owner = new int[1] ;
         H00OS5_A1881Alerta_Codigo = new int[1] ;
         H00OS5_A1882Alerta_ToAreaTrabalho = new int[1] ;
         H00OS5_n1882Alerta_ToAreaTrabalho = new bool[] {false} ;
         H00OS5_A1883Alerta_ToUser = new int[1] ;
         H00OS5_n1883Alerta_ToUser = new bool[] {false} ;
         H00OS5_A1884Alerta_ToGroup = new short[1] ;
         H00OS5_n1884Alerta_ToGroup = new bool[] {false} ;
         H00OS5_A1893Alerta_Descricao = new String[] {""} ;
         H00OS5_A1892Alerta_Assunto = new String[] {""} ;
         H00OS5_A40000AreaTrabalho_Descricao = new String[] {""} ;
         H00OS5_n40000AreaTrabalho_Descricao = new bool[] {false} ;
         H00OS5_A40001Usuario_PessoaNom = new String[] {""} ;
         H00OS5_n40001Usuario_PessoaNom = new bool[] {false} ;
         H00OS5_A40002Usuario_PessoaNom = new String[] {""} ;
         H00OS5_n40002Usuario_PessoaNom = new bool[] {false} ;
         A40000AreaTrabalho_Descricao = "";
         A40001Usuario_PessoaNom = "";
         A40002Usuario_PessoaNom = "";
         sStyleString = "";
         lblTextblock1_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         lblTextblock7_Jsonclick = "";
         TempTags = "";
         lblTextblock3_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttButton1_Jsonclick = "";
         lblTextblock5_Jsonclick = "";
         lblTextblock4_Jsonclick = "";
         lblTextblock6_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_alerta__default(),
            new Object[][] {
                new Object[] {
               H00OS5_A1887Alerta_Owner, H00OS5_A1881Alerta_Codigo, H00OS5_A1882Alerta_ToAreaTrabalho, H00OS5_n1882Alerta_ToAreaTrabalho, H00OS5_A1883Alerta_ToUser, H00OS5_n1883Alerta_ToUser, H00OS5_A1884Alerta_ToGroup, H00OS5_n1884Alerta_ToGroup, H00OS5_A1893Alerta_Descricao, H00OS5_A1892Alerta_Assunto,
               H00OS5_A40000AreaTrabalho_Descricao, H00OS5_n40000AreaTrabalho_Descricao, H00OS5_A40001Usuario_PessoaNom, H00OS5_n40001Usuario_PessoaNom, H00OS5_A40002Usuario_PessoaNom, H00OS5_n40002Usuario_PessoaNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavDe_nome_Enabled = 0;
         edtavAreatrabalho_descricao_Enabled = 0;
         edtavPara_nome_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short A1884Alerta_ToGroup ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1881Alerta_Codigo ;
      private int AV5UserId ;
      private int wcpOA1881Alerta_Codigo ;
      private int wcpOAV5UserId ;
      private int lblTbjava_Visible ;
      private int edtavDe_nome_Enabled ;
      private int edtavAreatrabalho_descricao_Enabled ;
      private int edtavPara_nome_Enabled ;
      private int A1887Alerta_Owner ;
      private int A1882Alerta_ToAreaTrabalho ;
      private int A1883Alerta_ToUser ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV10De_Nome ;
      private String AV8Para_Nome ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDe_nome_Internalname ;
      private String edtavAreatrabalho_descricao_Internalname ;
      private String edtavPara_nome_Internalname ;
      private String scmdbuf ;
      private String A40001Usuario_PessoaNom ;
      private String A40002Usuario_PessoaNom ;
      private String edtAlerta_Assunto_Internalname ;
      private String cmbAlerta_ToGroup_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtAlerta_Assunto_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String lblTextblock7_Internalname ;
      private String lblTextblock7_Jsonclick ;
      private String TempTags ;
      private String edtavDe_nome_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblock5_Internalname ;
      private String lblTextblock5_Jsonclick ;
      private String edtavAreatrabalho_descricao_Jsonclick ;
      private String lblTextblock4_Internalname ;
      private String lblTextblock4_Jsonclick ;
      private String cmbAlerta_ToGroup_Jsonclick ;
      private String lblTextblock6_Internalname ;
      private String lblTextblock6_Jsonclick ;
      private String edtavPara_nome_Jsonclick ;
      private String Alerta_descricao_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Alerta_descricao_Enabled ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1884Alerta_ToGroup ;
      private bool n1882Alerta_ToAreaTrabalho ;
      private bool n1883Alerta_ToUser ;
      private bool n40000AreaTrabalho_Descricao ;
      private bool n40001Usuario_PessoaNom ;
      private bool n40002Usuario_PessoaNom ;
      private bool returnInSub ;
      private String A1893Alerta_Descricao ;
      private String A1892Alerta_Assunto ;
      private String AV6AreaTrabalho_Descricao ;
      private String A40000AreaTrabalho_Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Alerta_Codigo ;
      private GXCombobox cmbAlerta_ToGroup ;
      private IDataStoreProvider pr_default ;
      private int[] H00OS5_A1887Alerta_Owner ;
      private int[] H00OS5_A1881Alerta_Codigo ;
      private int[] H00OS5_A1882Alerta_ToAreaTrabalho ;
      private bool[] H00OS5_n1882Alerta_ToAreaTrabalho ;
      private int[] H00OS5_A1883Alerta_ToUser ;
      private bool[] H00OS5_n1883Alerta_ToUser ;
      private short[] H00OS5_A1884Alerta_ToGroup ;
      private bool[] H00OS5_n1884Alerta_ToGroup ;
      private String[] H00OS5_A1893Alerta_Descricao ;
      private String[] H00OS5_A1892Alerta_Assunto ;
      private String[] H00OS5_A40000AreaTrabalho_Descricao ;
      private bool[] H00OS5_n40000AreaTrabalho_Descricao ;
      private String[] H00OS5_A40001Usuario_PessoaNom ;
      private bool[] H00OS5_n40001Usuario_PessoaNom ;
      private String[] H00OS5_A40002Usuario_PessoaNom ;
      private bool[] H00OS5_n40002Usuario_PessoaNom ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
   }

   public class wp_alerta__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00OS5 ;
          prmH00OS5 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00OS5", "SELECT T1.[Alerta_Owner], T1.[Alerta_Codigo], T1.[Alerta_ToAreaTrabalho], T1.[Alerta_ToUser], T1.[Alerta_ToGroup], T1.[Alerta_Descricao], T1.[Alerta_Assunto], COALESCE( T3.[AreaTrabalho_Descricao], '') AS AreaTrabalho_Descricao, COALESCE( T2.[Usuario_PessoaNom], '') AS Usuario_PessoaNom, COALESCE( T4.[Usuario_PessoaNom], '') AS Usuario_PessoaNom FROM ((([Alerta] T1 WITH (NOLOCK) LEFT JOIN (SELECT T6.[Pessoa_Nome] AS Usuario_PessoaNom, T5.[Usuario_Codigo] FROM ([Usuario] T5 WITH (NOLOCK) INNER JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) ) T2 ON T2.[Usuario_Codigo] = T1.[Alerta_Owner]) LEFT JOIN (SELECT [AreaTrabalho_Descricao] AS AreaTrabalho_Descricao, [AreaTrabalho_Codigo] FROM [AreaTrabalho] WITH (NOLOCK) ) T3 ON T3.[AreaTrabalho_Codigo] = T1.[Alerta_ToAreaTrabalho]) LEFT JOIN (SELECT T6.[Pessoa_Nome] AS Usuario_PessoaNom, T5.[Usuario_Codigo] FROM ([Usuario] T5 WITH (NOLOCK) INNER JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) ) T4 ON T4.[Usuario_Codigo] = T1.[Alerta_ToUser]) WHERE T1.[Alerta_Codigo] = @Alerta_Codigo ORDER BY T1.[Alerta_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OS5,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getLongVarchar(6) ;
                ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
                ((String[]) buf[10])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((String[]) buf[12])[0] = rslt.getString(9, 100) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((String[]) buf[14])[0] = rslt.getString(10, 100) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
