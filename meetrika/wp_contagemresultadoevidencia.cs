/*
               File: WP_ContagemResultadoEvidencia
        Description: Anexar arquivo:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:43:56.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_contagemresultadoevidencia : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_contagemresultadoevidencia( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_contagemresultadoevidencia( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo )
      {
         this.AV6ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         executePrivate();
         aP0_ContagemResultado_Codigo=this.AV6ContagemResultado_Codigo;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavTipodocumento_codigo = new GXCombobox();
         cmbavArtefatos_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vTIPODOCUMENTO_CODIGO") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvTIPODOCUMENTO_CODIGOC52( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV6ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAC52( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTC52( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117435690");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_contagemresultadoevidencia.aspx") + "?" + UrlEncode("" +AV6ContagemResultado_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_ARQUIVOEVIDENCIA", AV16Sdt_ArquivoEvidencia);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_ARQUIVOEVIDENCIA", AV16Sdt_ArquivoEvidencia);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV19WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV19WWPContext);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV15Sdt_ContagemResultadoEvidencias);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSDT_CONTAGEMRESULTADOEVIDENCIAS", AV15Sdt_ContagemResultadoEvidencias);
         }
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6ContagemResultado_Codigo), 6, 0, ",", "")));
         GXCCtlgxBlob = "vBLOB" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, AV5Blob);
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEC52( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTC52( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_contagemresultadoevidencia.aspx") + "?" + UrlEncode("" +AV6ContagemResultado_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_ContagemResultadoEvidencia" ;
      }

      public override String GetPgmdesc( )
      {
         return "Anexar arquivo:" ;
      }

      protected void WBC50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_C52( true) ;
         }
         else
         {
            wb_table1_2_C52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_C52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_ContagemResultadoEvidencia.htm");
         }
         wbLoad = true;
      }

      protected void STARTC52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Anexar arquivo:", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPC50( ) ;
      }

      protected void WSC52( )
      {
         STARTC52( ) ;
         EVTC52( ) ;
      }

      protected void EVTC52( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11C52 */
                              E11C52 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12C52 */
                                    E12C52 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13C52 */
                              E13C52 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEC52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAC52( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavTipodocumento_codigo.Name = "vTIPODOCUMENTO_CODIGO";
            dynavTipodocumento_codigo.WebTags = "";
            cmbavArtefatos_codigo.Name = "vARTEFATOS_CODIGO";
            cmbavArtefatos_codigo.WebTags = "";
            cmbavArtefatos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavArtefatos_codigo.ItemCount > 0 )
            {
               AV20Artefatos_Codigo = (int)(NumberUtil.Val( cmbavArtefatos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20Artefatos_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Artefatos_Codigo), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavBlob_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvTIPODOCUMENTO_CODIGOC52( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvTIPODOCUMENTO_CODIGO_dataC52( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvTIPODOCUMENTO_CODIGO_htmlC52( )
      {
         int gxdynajaxvalue ;
         GXDLVvTIPODOCUMENTO_CODIGO_dataC52( ) ;
         gxdynajaxindex = 1;
         dynavTipodocumento_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavTipodocumento_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavTipodocumento_codigo.ItemCount > 0 )
         {
            AV11TipoDocumento_Codigo = (int)(NumberUtil.Val( dynavTipodocumento_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11TipoDocumento_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11TipoDocumento_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvTIPODOCUMENTO_CODIGO_dataC52( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00C52 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00C52_A645TipoDocumento_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00C52_A646TipoDocumento_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavTipodocumento_codigo.ItemCount > 0 )
         {
            AV11TipoDocumento_Codigo = (int)(NumberUtil.Val( dynavTipodocumento_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11TipoDocumento_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11TipoDocumento_Codigo), 6, 0)));
         }
         if ( cmbavArtefatos_codigo.ItemCount > 0 )
         {
            AV20Artefatos_Codigo = (int)(NumberUtil.Val( cmbavArtefatos_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20Artefatos_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Artefatos_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFC52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFC52( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E13C52 */
            E13C52 ();
            WBC50( ) ;
         }
      }

      protected void STRUPC50( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         GXVvTIPODOCUMENTO_CODIGO_htmlC52( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11C52 */
         E11C52 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5Blob = cgiGet( edtavBlob_Internalname);
            AV18Link = cgiGet( edtavLink_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Link", AV18Link);
            dynavTipodocumento_codigo.CurrentValue = cgiGet( dynavTipodocumento_codigo_Internalname);
            AV11TipoDocumento_Codigo = (int)(NumberUtil.Val( cgiGet( dynavTipodocumento_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11TipoDocumento_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11TipoDocumento_Codigo), 6, 0)));
            cmbavArtefatos_codigo.CurrentValue = cgiGet( cmbavArtefatos_codigo_Internalname);
            AV20Artefatos_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavArtefatos_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Artefatos_Codigo), 6, 0)));
            AV10Descricao = cgiGet( edtavDescricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Descricao", AV10Descricao);
            AV9FileName = cgiGet( edtavFilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9FileName", AV9FileName);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) )
            {
               GXCCtlgxBlob = "vBLOB" + "_gxBlob";
               AV5Blob = cgiGet( GXCCtlgxBlob);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvTIPODOCUMENTO_CODIGO_htmlC52( ) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11C52 */
         E11C52 ();
         if (returnInSub) return;
      }

      protected void E11C52( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV19WWPContext) ;
         Form.Jscriptsrc.Add("http://code.jquery.com/jquery-latest.js\">") ;
         Form.Headerrawhtml = "<script type=\"text/javascript\">";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(document).ready(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vBLOB\").blur(function(){";
         Form.Headerrawhtml = Form.Headerrawhtml+"var nome = $(\"#vBLOB\").val();";
         Form.Headerrawhtml = Form.Headerrawhtml+"$(\"#vFILENAME\").val(nome);";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"});";
         Form.Headerrawhtml = Form.Headerrawhtml+"</script>";
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
         edtavFilename_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFilename_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFilename_Visible), 5, 0)));
         /* Execute user subroutine: 'ARTEFATOS' */
         S112 ();
         if (returnInSub) return;
      }

      public void GXEnter( )
      {
         /* Execute user event: E12C52 */
         E12C52 ();
         if (returnInSub) return;
      }

      protected void E12C52( )
      {
         /* Enter Routine */
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) && String.IsNullOrEmpty(StringUtil.RTrim( AV18Link)) )
         {
            GX_msglist.addItem("Selecione o Arquivo ou informe o Link que deseja anexar!");
         }
         else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Link)) )
         {
            GX_msglist.addItem("Arquivo e Link n�o podem ser anexados na mesma a��o!");
         }
         else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Link)) && (0==AV11TipoDocumento_Codigo) && (0==AV20Artefatos_Codigo) )
         {
            if ( cmbavArtefatos_codigo.ItemCount > 1 )
            {
               GX_msglist.addItem("Informe o Tipo de Documento ou Artefato associado ao Link a ser anexado!");
            }
            else
            {
               GX_msglist.addItem("Informe o Tipo de Documento associado ao Link a ser anexado!");
            }
         }
         else if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) && (0==AV11TipoDocumento_Codigo) && (0==AV20Artefatos_Codigo) )
         {
            if ( cmbavArtefatos_codigo.ItemCount > 1 )
            {
               GX_msglist.addItem("Informe o Tipo de Documento ou Artefato associado ao arquivo a ser anexado!");
            }
            else
            {
               GX_msglist.addItem("Informe o Tipo de Documento associado ao arquivo a ser anexado!");
            }
         }
         else
         {
            AV12Arquivo = AV5Blob;
            AV9FileName = StringUtil.Substring( AV9FileName, StringUtil.StringSearchRev( AV9FileName, "\\", -1)+1, 255);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9FileName", AV9FileName);
            AV8Ext = StringUtil.Trim( StringUtil.Substring( AV9FileName, StringUtil.StringSearchRev( AV9FileName, ".", -1)+1, 4));
            AV9FileName = StringUtil.Substring( AV9FileName, 1, StringUtil.StringSearchRev( AV9FileName, ".", -1)-1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9FileName", AV9FileName);
            AV16Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_data = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV16Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_arquivo = AV12Arquivo;
            AV16Sdt_ArquivoEvidencia.gxTpr_Tipodocumento_codigo = AV11TipoDocumento_Codigo;
            AV16Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_nomearq = AV9FileName;
            AV16Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_tipoarq = AV8Ext;
            AV16Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_descricao = AV10Descricao;
            AV16Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_link = AV18Link;
            AV16Sdt_ArquivoEvidencia.gxTpr_Contagemresultadoevidencia_owner = AV19WWPContext.gxTpr_Userid;
            AV16Sdt_ArquivoEvidencia.gxTpr_Artefatos_codigo = AV20Artefatos_Codigo;
            AV15Sdt_ContagemResultadoEvidencias.Add(AV16Sdt_ArquivoEvidencia, 0);
            AV17WebSession.Set("ArquivosEvd", AV15Sdt_ContagemResultadoEvidencias.ToXml(false, true, "SDT_ContagemResultadoEvidencias", "GxEv3Up14_Meetrika"));
            new prc_newevidenciademanda(context ).execute( ref  AV6ContagemResultado_Codigo) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
            context.setWebReturnParms(new Object[] {(int)AV6ContagemResultado_Codigo});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV16Sdt_ArquivoEvidencia", AV16Sdt_ArquivoEvidencia);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV15Sdt_ContagemResultadoEvidencias", AV15Sdt_ContagemResultadoEvidencias);
      }

      protected void S112( )
      {
         /* 'ARTEFATOS' Routine */
         /* Using cursor H00C53 */
         pr_default.execute(1, new Object[] {AV6ContagemResultado_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1772ContagemResultadoArtefato_OSCod = H00C53_A1772ContagemResultadoArtefato_OSCod[0];
            A1773ContagemResultadoArtefato_AnxCod = H00C53_A1773ContagemResultadoArtefato_AnxCod[0];
            n1773ContagemResultadoArtefato_AnxCod = H00C53_n1773ContagemResultadoArtefato_AnxCod[0];
            A1770ContagemResultadoArtefato_EvdCod = H00C53_A1770ContagemResultadoArtefato_EvdCod[0];
            n1770ContagemResultadoArtefato_EvdCod = H00C53_n1770ContagemResultadoArtefato_EvdCod[0];
            A1751Artefatos_Descricao = H00C53_A1751Artefatos_Descricao[0];
            n1751Artefatos_Descricao = H00C53_n1751Artefatos_Descricao[0];
            A1771ContagemResultadoArtefato_ArtefatoCod = H00C53_A1771ContagemResultadoArtefato_ArtefatoCod[0];
            A1751Artefatos_Descricao = H00C53_A1751Artefatos_Descricao[0];
            n1751Artefatos_Descricao = H00C53_n1751Artefatos_Descricao[0];
            AV21Anexado = (bool)((A1770ContagemResultadoArtefato_EvdCod>0)||(A1773ContagemResultadoArtefato_AnxCod>0));
            cmbavArtefatos_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A1771ContagemResultadoArtefato_ArtefatoCod), 6, 0)), A1751Artefatos_Descricao+(AV21Anexado ? " -" : " ?"), 0);
            pr_default.readNext(1);
         }
         pr_default.close(1);
         lblTbartefato_Visible = ((cmbavArtefatos_codigo.ItemCount>1) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbartefato_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbartefato_Visible), 5, 0)));
         cmbavArtefatos_codigo.Visible = ((cmbavArtefatos_codigo.ItemCount>1) ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavArtefatos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavArtefatos_codigo.Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E13C52( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_C52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "left", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\">") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='DataContentCell'>") ;
            ClassString = "ImageAttribute";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'',0)\"";
            edtavBlob_Filetype = "tmp";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV5Blob)) )
            {
               gxblobfileaux.Source = AV5Blob;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtavBlob_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtavBlob_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  AV5Blob = gxblobfileaux.GetAbsoluteName();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV5Blob));
                  edtavBlob_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "Filetype", edtavBlob_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBlob_Internalname, "URL", context.PathToRelativeUrl( AV5Blob));
            }
            GxWebStd.gx_blob_field( context, edtavBlob_Internalname, StringUtil.RTrim( AV5Blob), context.PathToRelativeUrl( AV5Blob), (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtavBlob_Filetype)) ? AV5Blob : edtavBlob_Filetype)) : edtavBlob_Contenttype), false, "", edtavBlob_Parameters, 0, 1, 1, "", "", 0, 0, 0, "px", 18, "px", 0, 0, 0, edtavBlob_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", StyleString, ClassString, "", ""+TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,8);\"", "", "", "HLP_WP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:19px")+"\" class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "Link:", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLink_Internalname, AV18Link, AV18Link, TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLink_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 80, "chr", 1, "row", 2097152, 0, 1, 0, 1, 0, -1, true, "", "left", false, "HLP_WP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Tipo Documento:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellLogin'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavTipodocumento_codigo, dynavTipodocumento_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV11TipoDocumento_Codigo), 6, 0)), 1, dynavTipodocumento_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WP_ContagemResultadoEvidencia.htm");
            dynavTipodocumento_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV11TipoDocumento_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavTipodocumento_codigo_Internalname, "Values", (String)(dynavTipodocumento_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbartefato_Internalname, "Artefato:", "", "", lblTbartefato_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "font-family:'Microsoft Sans Serif'; font-size:10.0pt; font-weight:normal; font-style:normal;", "TextBlock", 0, "", lblTbartefato_Visible, 1, 0, "HLP_WP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavArtefatos_codigo, cmbavArtefatos_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20Artefatos_Codigo), 6, 0)), 1, cmbavArtefatos_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavArtefatos_codigo.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "", true, "HLP_WP_ContagemResultadoEvidencia.htm");
            cmbavArtefatos_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20Artefatos_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavArtefatos_codigo_Internalname, "Values", (String)(cmbavArtefatos_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Descri��o:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDescricao_Internalname, AV10Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", 0, 1, 1, 0, 80, "chr", 2, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_WP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='DataContentCell'>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  style=\""+CSSHelper.Prettify( "height:25px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFilename_Internalname, AV9FileName, StringUtil.RTrim( context.localUtil.Format( AV9FileName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFilename_Jsonclick, 0, "Attribute", "color:#000000;", "", "", edtavFilename_Visible, 1, 0, "text", "", 80, "chr", 1, "px", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<td colspan=\"4\"  style=\""+CSSHelper.Prettify( "height:36px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ContagemResultadoEvidencia.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ContagemResultadoEvidencia.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_C52e( true) ;
         }
         else
         {
            wb_table1_2_C52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV6ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6ContagemResultado_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAC52( ) ;
         WSC52( ) ;
         WEC52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117435721");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_contagemresultadoevidencia.js", "?20203117435721");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         edtavBlob_Internalname = "vBLOB";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         edtavLink_Internalname = "vLINK";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         dynavTipodocumento_codigo_Internalname = "vTIPODOCUMENTO_CODIGO";
         lblTbartefato_Internalname = "TBARTEFATO";
         cmbavArtefatos_codigo_Internalname = "vARTEFATOS_CODIGO";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavDescricao_Internalname = "vDESCRICAO";
         edtavFilename_Internalname = "vFILENAME";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         tblTable1_Internalname = "TABLE1";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavFilename_Jsonclick = "";
         cmbavArtefatos_codigo_Jsonclick = "";
         lblTbartefato_Visible = 1;
         dynavTipodocumento_codigo_Jsonclick = "";
         edtavLink_Jsonclick = "";
         edtavBlob_Jsonclick = "";
         edtavBlob_Parameters = "";
         edtavBlob_Contenttype = "";
         edtavBlob_Filetype = "";
         cmbavArtefatos_codigo.Visible = 1;
         edtavFilename_Visible = 1;
         lblTbjava_Caption = "Text Block";
         lblTbjava_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Anexar arquivo:";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12C52',iparms:[{av:'AV5Blob',fld:'vBLOB',pic:'',nv:''},{av:'AV18Link',fld:'vLINK',pic:'',nv:''},{av:'AV11TipoDocumento_Codigo',fld:'vTIPODOCUMENTO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV16Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV10Descricao',fld:'vDESCRICAO',pic:'',nv:''},{av:'AV19WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV15Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null},{av:'AV6ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV9FileName',fld:'vFILENAME',pic:'',nv:''},{av:'AV16Sdt_ArquivoEvidencia',fld:'vSDT_ARQUIVOEVIDENCIA',pic:'',nv:null},{av:'AV15Sdt_ContagemResultadoEvidencias',fld:'vSDT_CONTAGEMRESULTADOEVIDENCIAS',pic:'',nv:null},{av:'AV6ContagemResultado_Codigo',fld:'vCONTAGEMRESULTADO_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV16Sdt_ArquivoEvidencia = new SdtSDT_ContagemResultadoEvidencias_Arquivo(context);
         AV19WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV15Sdt_ContagemResultadoEvidencias = new GxObjectCollection( context, "SDT_ContagemResultadoEvidencias.Arquivo", "GxEv3Up14_Meetrika", "SdtSDT_ContagemResultadoEvidencias_Arquivo", "GeneXus.Programs");
         GXCCtlgxBlob = "";
         AV5Blob = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00C52_A645TipoDocumento_Codigo = new int[1] ;
         H00C52_A646TipoDocumento_Nome = new String[] {""} ;
         H00C52_A647TipoDocumento_Ativo = new bool[] {false} ;
         AV18Link = "";
         AV10Descricao = "";
         AV9FileName = "";
         AV12Arquivo = "";
         AV8Ext = "";
         AV17WebSession = context.GetSession();
         H00C53_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         H00C53_A1772ContagemResultadoArtefato_OSCod = new int[1] ;
         H00C53_A1773ContagemResultadoArtefato_AnxCod = new int[1] ;
         H00C53_n1773ContagemResultadoArtefato_AnxCod = new bool[] {false} ;
         H00C53_A1770ContagemResultadoArtefato_EvdCod = new int[1] ;
         H00C53_n1770ContagemResultadoArtefato_EvdCod = new bool[] {false} ;
         H00C53_A1751Artefatos_Descricao = new String[] {""} ;
         H00C53_n1751Artefatos_Descricao = new bool[] {false} ;
         H00C53_A1771ContagemResultadoArtefato_ArtefatoCod = new int[1] ;
         A1751Artefatos_Descricao = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         lblTextblock3_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTbartefato_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_contagemresultadoevidencia__default(),
            new Object[][] {
                new Object[] {
               H00C52_A645TipoDocumento_Codigo, H00C52_A646TipoDocumento_Nome, H00C52_A647TipoDocumento_Ativo
               }
               , new Object[] {
               H00C53_A1769ContagemResultadoArtefato_Codigo, H00C53_A1772ContagemResultadoArtefato_OSCod, H00C53_A1773ContagemResultadoArtefato_AnxCod, H00C53_n1773ContagemResultadoArtefato_AnxCod, H00C53_A1770ContagemResultadoArtefato_EvdCod, H00C53_n1770ContagemResultadoArtefato_EvdCod, H00C53_A1751Artefatos_Descricao, H00C53_n1751Artefatos_Descricao, H00C53_A1771ContagemResultadoArtefato_ArtefatoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int AV6ContagemResultado_Codigo ;
      private int wcpOAV6ContagemResultado_Codigo ;
      private int lblTbjava_Visible ;
      private int AV20Artefatos_Codigo ;
      private int gxdynajaxindex ;
      private int AV11TipoDocumento_Codigo ;
      private int edtavFilename_Visible ;
      private int A1772ContagemResultadoArtefato_OSCod ;
      private int A1773ContagemResultadoArtefato_AnxCod ;
      private int A1770ContagemResultadoArtefato_EvdCod ;
      private int A1771ContagemResultadoArtefato_ArtefatoCod ;
      private int lblTbartefato_Visible ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavBlob_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String edtavLink_Internalname ;
      private String dynavTipodocumento_codigo_Internalname ;
      private String cmbavArtefatos_codigo_Internalname ;
      private String edtavDescricao_Internalname ;
      private String edtavFilename_Internalname ;
      private String AV12Arquivo ;
      private String AV8Ext ;
      private String lblTbartefato_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String edtavBlob_Filetype ;
      private String edtavBlob_Contenttype ;
      private String edtavBlob_Parameters ;
      private String edtavBlob_Jsonclick ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String edtavLink_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String dynavTipodocumento_codigo_Jsonclick ;
      private String lblTbartefato_Jsonclick ;
      private String cmbavArtefatos_codigo_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavFilename_Jsonclick ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1773ContagemResultadoArtefato_AnxCod ;
      private bool n1770ContagemResultadoArtefato_EvdCod ;
      private bool n1751Artefatos_Descricao ;
      private bool AV21Anexado ;
      private String AV18Link ;
      private String AV10Descricao ;
      private String AV9FileName ;
      private String A1751Artefatos_Descricao ;
      private String AV5Blob ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxFile gxblobfileaux ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private GXCombobox dynavTipodocumento_codigo ;
      private GXCombobox cmbavArtefatos_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00C52_A645TipoDocumento_Codigo ;
      private String[] H00C52_A646TipoDocumento_Nome ;
      private bool[] H00C52_A647TipoDocumento_Ativo ;
      private int[] H00C53_A1769ContagemResultadoArtefato_Codigo ;
      private int[] H00C53_A1772ContagemResultadoArtefato_OSCod ;
      private int[] H00C53_A1773ContagemResultadoArtefato_AnxCod ;
      private bool[] H00C53_n1773ContagemResultadoArtefato_AnxCod ;
      private int[] H00C53_A1770ContagemResultadoArtefato_EvdCod ;
      private bool[] H00C53_n1770ContagemResultadoArtefato_EvdCod ;
      private String[] H00C53_A1751Artefatos_Descricao ;
      private bool[] H00C53_n1751Artefatos_Descricao ;
      private int[] H00C53_A1771ContagemResultadoArtefato_ArtefatoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV17WebSession ;
      [ObjectCollection(ItemType=typeof( SdtSDT_ContagemResultadoEvidencias_Arquivo ))]
      private IGxCollection AV15Sdt_ContagemResultadoEvidencias ;
      private GXWebForm Form ;
      private SdtSDT_ContagemResultadoEvidencias_Arquivo AV16Sdt_ArquivoEvidencia ;
      private wwpbaseobjects.SdtWWPContext AV19WWPContext ;
   }

   public class wp_contagemresultadoevidencia__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00C52 ;
          prmH00C52 = new Object[] {
          } ;
          Object[] prmH00C53 ;
          prmH00C53 = new Object[] {
          new Object[] {"@AV6ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00C52", "SELECT [TipoDocumento_Codigo], [TipoDocumento_Nome], [TipoDocumento_Ativo] FROM [TipoDocumento] WITH (NOLOCK) WHERE [TipoDocumento_Ativo] = 1 ORDER BY [TipoDocumento_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C52,0,0,true,false )
             ,new CursorDef("H00C53", "SELECT T1.[ContagemResultadoArtefato_Codigo], T1.[ContagemResultadoArtefato_OSCod], T1.[ContagemResultadoArtefato_AnxCod], T1.[ContagemResultadoArtefato_EvdCod], T2.[Artefatos_Descricao], T1.[ContagemResultadoArtefato_ArtefatoCod] AS ContagemResultadoArtefato_ArtefatoCod FROM ([ContagemResultadoArtefato] T1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = T1.[ContagemResultadoArtefato_ArtefatoCod]) WHERE T1.[ContagemResultadoArtefato_OSCod] = @AV6ContagemResultado_Codigo ORDER BY T1.[ContagemResultadoArtefato_OSCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00C53,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
