/*
               File: WP_FuncoesTRN
        Description: Fun��es de Transa��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:55:53.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_funcoestrn : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_funcoestrn( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_funcoestrn( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_FuncaoAPF_SistemaCod ,
                           ref String aP1_Sistema_Sigla )
      {
         this.A360FuncaoAPF_SistemaCod = aP0_FuncaoAPF_SistemaCod;
         this.AV128Sistema_Sigla = aP1_Sistema_Sigla;
         executePrivate();
         aP0_FuncaoAPF_SistemaCod=this.A360FuncaoAPF_SistemaCod;
         aP1_Sistema_Sigla=this.AV128Sistema_Sigla;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavFuncaoapf_tipo1 = new GXCombobox();
         cmbavFuncaoapf_complexidade1 = new GXCombobox();
         cmbavFuncaoapf_ativo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavFuncaoapf_tipo2 = new GXCombobox();
         cmbavFuncaoapf_complexidade2 = new GXCombobox();
         cmbavFuncaoapf_ativo2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavFuncaoapf_tipo3 = new GXCombobox();
         cmbavFuncaoapf_complexidade3 = new GXCombobox();
         cmbavFuncaoapf_ativo3 = new GXCombobox();
         cmbavDynamicfiltersselector4 = new GXCombobox();
         cmbavFuncaoapf_tipo4 = new GXCombobox();
         cmbavFuncaoapf_complexidade4 = new GXCombobox();
         cmbavFuncaoapf_ativo4 = new GXCombobox();
         cmbavSorepetidas = new GXCombobox();
         chkavSelectall = new GXCheckbox();
         chkavReverseall = new GXCheckbox();
         chkavEmcascata = new GXCheckbox();
         chkavSelected = new GXCheckbox();
         cmbavFuncaoapf_tipo = new GXCombobox();
         cmbavFuncaoapf_complexidade = new GXCombobox();
         cmbavFuncaoapf_status = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
         chkavDynamicfiltersenabled4 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_124 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_124_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_124_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV127Rows = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Rows", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Rows), 4, 0)));
               AV59OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OrderedBy), 4, 0)));
               AV61OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61OrderedDsc", AV61OrderedDsc);
               AV136Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV50GridState);
               AV19DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersIgnoreFirst", AV19DynamicFiltersIgnoreFirst);
               AV24DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector1", AV24DynamicFiltersSelector1);
               AV39FuncaoAPF_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39FuncaoAPF_Nome1", AV39FuncaoAPF_Nome1);
               AV123FuncaoAPF_Tipo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123FuncaoAPF_Tipo1", AV123FuncaoAPF_Tipo1);
               AV119FuncaoAPF_Complexidade1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119FuncaoAPF_Complexidade1", AV119FuncaoAPF_Complexidade1);
               AV115FuncaoAPF_Ativo1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115FuncaoAPF_Ativo1", AV115FuncaoAPF_Ativo1);
               AV23DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
               AV17DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV25DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector2", AV25DynamicFiltersSelector2);
               AV40FuncaoAPF_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40FuncaoAPF_Nome2", AV40FuncaoAPF_Nome2);
               AV124FuncaoAPF_Tipo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124FuncaoAPF_Tipo2", AV124FuncaoAPF_Tipo2);
               AV120FuncaoAPF_Complexidade2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120FuncaoAPF_Complexidade2", AV120FuncaoAPF_Complexidade2);
               AV116FuncaoAPF_Ativo2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116FuncaoAPF_Ativo2", AV116FuncaoAPF_Ativo2);
               AV18DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled3", AV18DynamicFiltersEnabled3);
               AV26DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
               AV41FuncaoAPF_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoAPF_Nome3", AV41FuncaoAPF_Nome3);
               AV125FuncaoAPF_Tipo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125FuncaoAPF_Tipo3", AV125FuncaoAPF_Tipo3);
               AV121FuncaoAPF_Complexidade3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121FuncaoAPF_Complexidade3", AV121FuncaoAPF_Complexidade3);
               AV117FuncaoAPF_Ativo3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117FuncaoAPF_Ativo3", AV117FuncaoAPF_Ativo3);
               AV101DynamicFiltersEnabled4 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101DynamicFiltersEnabled4", AV101DynamicFiltersEnabled4);
               AV102DynamicFiltersSelector4 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102DynamicFiltersSelector4", AV102DynamicFiltersSelector4);
               AV109FuncaoAPF_Nome4 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109FuncaoAPF_Nome4", AV109FuncaoAPF_Nome4);
               AV126FuncaoAPF_Tipo4 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126FuncaoAPF_Tipo4", AV126FuncaoAPF_Tipo4);
               AV122FuncaoAPF_Complexidade4 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122FuncaoAPF_Complexidade4", AV122FuncaoAPF_Complexidade4);
               AV118FuncaoAPF_Ativo4 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118FuncaoAPF_Ativo4", AV118FuncaoAPF_Ativo4);
               AV66SoRepetidas = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66SoRepetidas", AV66SoRepetidas);
               A166FuncaoAPF_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               A184FuncaoAPF_Tipo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
               A183FuncaoAPF_Ativo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A183FuncaoAPF_Ativo", A183FuncaoAPF_Ativo);
               A185FuncaoAPF_Complexidade = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
               A387FuncaoAPF_AR = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
               A388FuncaoAPF_TD = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
               A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A363FuncaoAPF_FunAPFPaiNom = GetNextPar( );
               n363FuncaoAPF_FunAPFPaiNom = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A363FuncaoAPF_FunAPFPaiNom", A363FuncaoAPF_FunAPFPaiNom);
               AV42FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A358FuncaoAPF_FunAPFPaiCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               n358FuncaoAPF_FunAPFPaiCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A358FuncaoAPF_FunAPFPaiCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0)));
               A386FuncaoAPF_PF = NumberUtil.Val( GetNextPar( ), ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
               AV38FuncaoAPF_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38FuncaoAPF_Nome", AV38FuncaoAPF_Nome);
               AV48FuncaoAPF_Tipo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_tipo_Internalname, AV48FuncaoAPF_Tipo);
               AV27FuncaoAPF_AR = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_ar_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV27FuncaoAPF_AR), 4, 0)));
               AV47FuncaoAPF_TD = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_td_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV47FuncaoAPF_TD), 4, 0)));
               AV89FuncaoAPF_Ativo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89FuncaoAPF_Ativo", AV89FuncaoAPF_Ativo);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV85Processadas);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6Codigos);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               A360FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               n360FuncaoAPF_SistemaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV128Sistema_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128Sistema_Sigla", AV128Sistema_Sigla);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAHA2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTHA2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118555431");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_funcoestrn.aspx") + "?" + UrlEncode("" +A360FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(AV128Sistema_Sigla))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_124", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_124), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vROWS", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV127Rows), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV136Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV50GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV50GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV19DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV23DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_NOME", A166FuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_TIPO", StringUtil.RTrim( A184FuncaoAPF_Tipo));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_ATIVO", StringUtil.RTrim( A183FuncaoAPF_Ativo));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_FUNAPFPAINOM", A363FuncaoAPF_FunAPFPaiNom);
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_FUNAPFPAICOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A358FuncaoAPF_FunAPFPaiCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPF_NOME", AV38FuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, "vFUNCAOAPF_ATIVO", StringUtil.RTrim( AV89FuncaoAPF_Ativo));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROCESSADAS", AV85Processadas);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROCESSADAS", AV85Processadas);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV6Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV6Codigos);
         }
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_SIGLA", StringUtil.RTrim( AV128Sistema_Sigla));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_PF", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_AR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_TD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "FUNCAOAPF_COMPLEXIDADE", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEHA2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTHA2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_funcoestrn.aspx") + "?" + UrlEncode("" +A360FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(AV128Sistema_Sigla)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_FuncoesTRN" ;
      }

      public override String GetPgmdesc( )
      {
         return "Fun��es de Transa��o" ;
      }

      protected void WBHA0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_HA2( true) ;
         }
         else
         {
            wb_table1_2_HA2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_HA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'" + sGXsfl_124_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV17DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(148, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,148);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'" + sGXsfl_124_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(149, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,149);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 150,'',false,'" + sGXsfl_124_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled4_Internalname, StringUtil.BoolToStr( AV101DynamicFiltersEnabled4), "", "", chkavDynamicfiltersenabled4.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(150, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,150);\"");
         }
         wbLoad = true;
      }

      protected void STARTHA2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Fun��es de Transa��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPHA0( ) ;
      }

      protected void WSHA2( )
      {
         STARTHA2( ) ;
         EVTHA2( ) ;
      }

      protected void EVTHA2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11HA2 */
                              E11HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12HA2 */
                              E12HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13HA2 */
                              E13HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14HA2 */
                              E14HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS4'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15HA2 */
                              E15HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16HA2 */
                              E16HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOATUALIZAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17HA2 */
                              E17HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOAPAGARSELECIONADAS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18HA2 */
                              E18HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSELECTALL.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19HA2 */
                              E19HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VREVERSEALL.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20HA2 */
                              E20HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21HA2 */
                              E21HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22HA2 */
                              E22HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23HA2 */
                              E23HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24HA2 */
                              E24HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25HA2 */
                              E25HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26HA2 */
                              E26HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR4.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27HA2 */
                              E27HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSOREPETIDAS.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E28HA2 */
                              E28HA2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VSELECTED.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VSELECTED.CLICK") == 0 ) )
                           {
                              nGXsfl_124_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_124_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_124_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1242( ) ;
                              AV71Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV71Update)) ? AV133Update_GXI : context.convertURL( context.PathToRelativeUrl( AV71Update))));
                              AV15Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)) ? AV134Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV15Delete))));
                              AV16Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV16Display)) ? AV135Display_GXI : context.convertURL( context.PathToRelativeUrl( AV16Display))));
                              AV64Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV64Selected);
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_CODIGO");
                                 GX_FocusControl = edtavFuncaoapf_codigo_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV30FuncaoAPF_Codigo = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV30FuncaoAPF_Codigo), 6, 0)));
                              }
                              else
                              {
                                 AV30FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV30FuncaoAPF_Codigo), 6, 0)));
                              }
                              AV80Funcao_Nome = cgiGet( edtavFuncao_nome_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncao_nome_Internalname, AV80Funcao_Nome);
                              cmbavFuncaoapf_tipo.Name = cmbavFuncaoapf_tipo_Internalname;
                              cmbavFuncaoapf_tipo.CurrentValue = cgiGet( cmbavFuncaoapf_tipo_Internalname);
                              AV48FuncaoAPF_Tipo = cgiGet( cmbavFuncaoapf_tipo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_tipo_Internalname, AV48FuncaoAPF_Tipo);
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_TD");
                                 GX_FocusControl = edtavFuncaoapf_td_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV47FuncaoAPF_TD = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_td_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV47FuncaoAPF_TD), 4, 0)));
                              }
                              else
                              {
                                 AV47FuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_td_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV47FuncaoAPF_TD), 4, 0)));
                              }
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_AR");
                                 GX_FocusControl = edtavFuncaoapf_ar_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV27FuncaoAPF_AR = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_ar_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV27FuncaoAPF_AR), 4, 0)));
                              }
                              else
                              {
                                 AV27FuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", "."));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_ar_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV27FuncaoAPF_AR), 4, 0)));
                              }
                              cmbavFuncaoapf_complexidade.Name = cmbavFuncaoapf_complexidade_Internalname;
                              cmbavFuncaoapf_complexidade.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade_Internalname);
                              AV77FuncaoAPF_Complexidade = cgiGet( cmbavFuncaoapf_complexidade_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_complexidade_Internalname, AV77FuncaoAPF_Complexidade);
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_PF");
                                 GX_FocusControl = edtavFuncaoapf_pf_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV78FuncaoAPF_PF = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_pf_Internalname, StringUtil.LTrim( StringUtil.Str( AV78FuncaoAPF_PF, 14, 5)));
                              }
                              else
                              {
                                 AV78FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".");
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_pf_Internalname, StringUtil.LTrim( StringUtil.Str( AV78FuncaoAPF_PF, 14, 5)));
                              }
                              AV79FunAPFPaiNom = cgiGet( edtavFunapfpainom_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFunapfpainom_Internalname, AV79FunAPFPaiNom);
                              AV79FunAPFPaiNom = cgiGet( edtavFunapfpainom_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFunapfpainom_Internalname, AV79FunAPFPaiNom);
                              cmbavFuncaoapf_status.Name = cmbavFuncaoapf_status_Internalname;
                              cmbavFuncaoapf_status.CurrentValue = cgiGet( cmbavFuncaoapf_status_Internalname);
                              AV81FuncaoAPF_Status = cgiGet( cmbavFuncaoapf_status_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_status_Internalname, AV81FuncaoAPF_Status);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29HA2 */
                                    E29HA2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E30HA2 */
                                    E30HA2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E31HA2 */
                                    E31HA2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VSELECTED.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E32HA2 */
                                    E32HA2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEHA2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAHA2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV59OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("0", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("1", "Tipo", 0);
            cmbavDynamicfiltersselector1.addItem("2", "Complexidade", 0);
            cmbavDynamicfiltersselector1.addItem("3", "Status", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV24DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV24DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector1", AV24DynamicFiltersSelector1);
            }
            cmbavFuncaoapf_tipo1.Name = "vFUNCAOAPF_TIPO1";
            cmbavFuncaoapf_tipo1.WebTags = "";
            cmbavFuncaoapf_tipo1.addItem("", "Tudo", 0);
            cmbavFuncaoapf_tipo1.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo1.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo1.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo1.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo1.ItemCount > 0 )
            {
               AV123FuncaoAPF_Tipo1 = cmbavFuncaoapf_tipo1.getValidValue(AV123FuncaoAPF_Tipo1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123FuncaoAPF_Tipo1", AV123FuncaoAPF_Tipo1);
            }
            cmbavFuncaoapf_complexidade1.Name = "vFUNCAOAPF_COMPLEXIDADE1";
            cmbavFuncaoapf_complexidade1.WebTags = "";
            cmbavFuncaoapf_complexidade1.addItem("", "Tudo", 0);
            cmbavFuncaoapf_complexidade1.addItem("E", "", 0);
            cmbavFuncaoapf_complexidade1.addItem("B", "Baixa", 0);
            cmbavFuncaoapf_complexidade1.addItem("M", "M�dia", 0);
            cmbavFuncaoapf_complexidade1.addItem("A", "Alta", 0);
            if ( cmbavFuncaoapf_complexidade1.ItemCount > 0 )
            {
               AV119FuncaoAPF_Complexidade1 = cmbavFuncaoapf_complexidade1.getValidValue(AV119FuncaoAPF_Complexidade1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119FuncaoAPF_Complexidade1", AV119FuncaoAPF_Complexidade1);
            }
            cmbavFuncaoapf_ativo1.Name = "vFUNCAOAPF_ATIVO1";
            cmbavFuncaoapf_ativo1.WebTags = "";
            cmbavFuncaoapf_ativo1.addItem("", "Tudo", 0);
            cmbavFuncaoapf_ativo1.addItem("A", "Ativa", 0);
            cmbavFuncaoapf_ativo1.addItem("E", "Exclu�da", 0);
            cmbavFuncaoapf_ativo1.addItem("R", "Rejeitada", 0);
            if ( cmbavFuncaoapf_ativo1.ItemCount > 0 )
            {
               AV115FuncaoAPF_Ativo1 = cmbavFuncaoapf_ativo1.getValidValue(AV115FuncaoAPF_Ativo1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115FuncaoAPF_Ativo1", AV115FuncaoAPF_Ativo1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV25DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector2", AV25DynamicFiltersSelector2);
            }
            cmbavFuncaoapf_tipo2.Name = "vFUNCAOAPF_TIPO2";
            cmbavFuncaoapf_tipo2.WebTags = "";
            cmbavFuncaoapf_tipo2.addItem("", "Tudo", 0);
            cmbavFuncaoapf_tipo2.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo2.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo2.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo2.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo2.ItemCount > 0 )
            {
               AV124FuncaoAPF_Tipo2 = cmbavFuncaoapf_tipo2.getValidValue(AV124FuncaoAPF_Tipo2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124FuncaoAPF_Tipo2", AV124FuncaoAPF_Tipo2);
            }
            cmbavFuncaoapf_complexidade2.Name = "vFUNCAOAPF_COMPLEXIDADE2";
            cmbavFuncaoapf_complexidade2.WebTags = "";
            cmbavFuncaoapf_complexidade2.addItem("", "Tudo", 0);
            cmbavFuncaoapf_complexidade2.addItem("E", "", 0);
            cmbavFuncaoapf_complexidade2.addItem("B", "Baixa", 0);
            cmbavFuncaoapf_complexidade2.addItem("M", "M�dia", 0);
            cmbavFuncaoapf_complexidade2.addItem("A", "Alta", 0);
            if ( cmbavFuncaoapf_complexidade2.ItemCount > 0 )
            {
               AV120FuncaoAPF_Complexidade2 = cmbavFuncaoapf_complexidade2.getValidValue(AV120FuncaoAPF_Complexidade2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120FuncaoAPF_Complexidade2", AV120FuncaoAPF_Complexidade2);
            }
            cmbavFuncaoapf_ativo2.Name = "vFUNCAOAPF_ATIVO2";
            cmbavFuncaoapf_ativo2.WebTags = "";
            cmbavFuncaoapf_ativo2.addItem("", "Tudo", 0);
            cmbavFuncaoapf_ativo2.addItem("A", "Ativa", 0);
            cmbavFuncaoapf_ativo2.addItem("E", "Exclu�da", 0);
            cmbavFuncaoapf_ativo2.addItem("R", "Rejeitada", 0);
            if ( cmbavFuncaoapf_ativo2.ItemCount > 0 )
            {
               AV116FuncaoAPF_Ativo2 = cmbavFuncaoapf_ativo2.getValidValue(AV116FuncaoAPF_Ativo2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116FuncaoAPF_Ativo2", AV116FuncaoAPF_Ativo2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            }
            cmbavFuncaoapf_tipo3.Name = "vFUNCAOAPF_TIPO3";
            cmbavFuncaoapf_tipo3.WebTags = "";
            cmbavFuncaoapf_tipo3.addItem("", "Tudo", 0);
            cmbavFuncaoapf_tipo3.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo3.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo3.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo3.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo3.ItemCount > 0 )
            {
               AV125FuncaoAPF_Tipo3 = cmbavFuncaoapf_tipo3.getValidValue(AV125FuncaoAPF_Tipo3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125FuncaoAPF_Tipo3", AV125FuncaoAPF_Tipo3);
            }
            cmbavFuncaoapf_complexidade3.Name = "vFUNCAOAPF_COMPLEXIDADE3";
            cmbavFuncaoapf_complexidade3.WebTags = "";
            cmbavFuncaoapf_complexidade3.addItem("", "Tudo", 0);
            cmbavFuncaoapf_complexidade3.addItem("E", "", 0);
            cmbavFuncaoapf_complexidade3.addItem("B", "Baixa", 0);
            cmbavFuncaoapf_complexidade3.addItem("M", "M�dia", 0);
            cmbavFuncaoapf_complexidade3.addItem("A", "Alta", 0);
            if ( cmbavFuncaoapf_complexidade3.ItemCount > 0 )
            {
               AV121FuncaoAPF_Complexidade3 = cmbavFuncaoapf_complexidade3.getValidValue(AV121FuncaoAPF_Complexidade3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121FuncaoAPF_Complexidade3", AV121FuncaoAPF_Complexidade3);
            }
            cmbavFuncaoapf_ativo3.Name = "vFUNCAOAPF_ATIVO3";
            cmbavFuncaoapf_ativo3.WebTags = "";
            cmbavFuncaoapf_ativo3.addItem("", "Tudo", 0);
            cmbavFuncaoapf_ativo3.addItem("A", "Ativa", 0);
            cmbavFuncaoapf_ativo3.addItem("E", "Exclu�da", 0);
            cmbavFuncaoapf_ativo3.addItem("R", "Rejeitada", 0);
            if ( cmbavFuncaoapf_ativo3.ItemCount > 0 )
            {
               AV117FuncaoAPF_Ativo3 = cmbavFuncaoapf_ativo3.getValidValue(AV117FuncaoAPF_Ativo3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117FuncaoAPF_Ativo3", AV117FuncaoAPF_Ativo3);
            }
            cmbavDynamicfiltersselector4.Name = "vDYNAMICFILTERSSELECTOR4";
            cmbavDynamicfiltersselector4.WebTags = "";
            if ( cmbavDynamicfiltersselector4.ItemCount > 0 )
            {
               AV102DynamicFiltersSelector4 = cmbavDynamicfiltersselector4.getValidValue(AV102DynamicFiltersSelector4);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102DynamicFiltersSelector4", AV102DynamicFiltersSelector4);
            }
            cmbavFuncaoapf_tipo4.Name = "vFUNCAOAPF_TIPO4";
            cmbavFuncaoapf_tipo4.WebTags = "";
            cmbavFuncaoapf_tipo4.addItem("", "Tudo", 0);
            cmbavFuncaoapf_tipo4.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo4.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo4.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo4.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo4.ItemCount > 0 )
            {
               AV126FuncaoAPF_Tipo4 = cmbavFuncaoapf_tipo4.getValidValue(AV126FuncaoAPF_Tipo4);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126FuncaoAPF_Tipo4", AV126FuncaoAPF_Tipo4);
            }
            cmbavFuncaoapf_complexidade4.Name = "vFUNCAOAPF_COMPLEXIDADE4";
            cmbavFuncaoapf_complexidade4.WebTags = "";
            cmbavFuncaoapf_complexidade4.addItem("", "Tudo", 0);
            cmbavFuncaoapf_complexidade4.addItem("E", "", 0);
            cmbavFuncaoapf_complexidade4.addItem("B", "Baixa", 0);
            cmbavFuncaoapf_complexidade4.addItem("M", "M�dia", 0);
            cmbavFuncaoapf_complexidade4.addItem("A", "Alta", 0);
            if ( cmbavFuncaoapf_complexidade4.ItemCount > 0 )
            {
               AV122FuncaoAPF_Complexidade4 = cmbavFuncaoapf_complexidade4.getValidValue(AV122FuncaoAPF_Complexidade4);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122FuncaoAPF_Complexidade4", AV122FuncaoAPF_Complexidade4);
            }
            cmbavFuncaoapf_ativo4.Name = "vFUNCAOAPF_ATIVO4";
            cmbavFuncaoapf_ativo4.WebTags = "";
            cmbavFuncaoapf_ativo4.addItem("", "Tudo", 0);
            cmbavFuncaoapf_ativo4.addItem("A", "Ativa", 0);
            cmbavFuncaoapf_ativo4.addItem("E", "Exclu�da", 0);
            cmbavFuncaoapf_ativo4.addItem("R", "Rejeitada", 0);
            if ( cmbavFuncaoapf_ativo4.ItemCount > 0 )
            {
               AV118FuncaoAPF_Ativo4 = cmbavFuncaoapf_ativo4.getValidValue(AV118FuncaoAPF_Ativo4);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118FuncaoAPF_Ativo4", AV118FuncaoAPF_Ativo4);
            }
            cmbavSorepetidas.Name = "vSOREPETIDAS";
            cmbavSorepetidas.WebTags = "";
            cmbavSorepetidas.addItem(StringUtil.BoolToStr( false), "Todas", 0);
            cmbavSorepetidas.addItem(StringUtil.BoolToStr( true), "S� repetidas", 0);
            if ( cmbavSorepetidas.ItemCount > 0 )
            {
               AV66SoRepetidas = StringUtil.StrToBool( cmbavSorepetidas.getValidValue(StringUtil.BoolToStr( AV66SoRepetidas)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66SoRepetidas", AV66SoRepetidas);
            }
            chkavSelectall.Name = "vSELECTALL";
            chkavSelectall.WebTags = "";
            chkavSelectall.Caption = "�Todas seleccionadas�";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelectall_Internalname, "TitleCaption", chkavSelectall.Caption);
            chkavSelectall.CheckedValue = "false";
            chkavReverseall.Name = "vREVERSEALL";
            chkavReverseall.WebTags = "";
            chkavReverseall.Caption = "�Inverter sele��o�";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavReverseall_Internalname, "TitleCaption", chkavReverseall.Caption);
            chkavReverseall.CheckedValue = "false";
            chkavEmcascata.Name = "vEMCASCATA";
            chkavEmcascata.WebTags = "";
            chkavEmcascata.Caption = "�Apagar em cascata�";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavEmcascata_Internalname, "TitleCaption", chkavEmcascata.Caption);
            chkavEmcascata.CheckedValue = "false";
            GXCCtl = "vSELECTED_" + sGXsfl_124_idx;
            chkavSelected.Name = GXCCtl;
            chkavSelected.WebTags = "";
            chkavSelected.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelected_Internalname, "TitleCaption", chkavSelected.Caption);
            chkavSelected.CheckedValue = "false";
            GXCCtl = "vFUNCAOAPF_TIPO_" + sGXsfl_124_idx;
            cmbavFuncaoapf_tipo.Name = GXCCtl;
            cmbavFuncaoapf_tipo.WebTags = "";
            cmbavFuncaoapf_tipo.addItem("", "(Nenhum)", 0);
            cmbavFuncaoapf_tipo.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo.ItemCount > 0 )
            {
               AV48FuncaoAPF_Tipo = cmbavFuncaoapf_tipo.getValidValue(AV48FuncaoAPF_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_tipo_Internalname, AV48FuncaoAPF_Tipo);
            }
            GXCCtl = "vFUNCAOAPF_COMPLEXIDADE_" + sGXsfl_124_idx;
            cmbavFuncaoapf_complexidade.Name = GXCCtl;
            cmbavFuncaoapf_complexidade.WebTags = "";
            cmbavFuncaoapf_complexidade.addItem("E", "", 0);
            cmbavFuncaoapf_complexidade.addItem("B", "Baixa", 0);
            cmbavFuncaoapf_complexidade.addItem("M", "M�dia", 0);
            cmbavFuncaoapf_complexidade.addItem("A", "Alta", 0);
            if ( cmbavFuncaoapf_complexidade.ItemCount > 0 )
            {
               AV77FuncaoAPF_Complexidade = cmbavFuncaoapf_complexidade.getValidValue(AV77FuncaoAPF_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_complexidade_Internalname, AV77FuncaoAPF_Complexidade);
            }
            GXCCtl = "vFUNCAOAPF_STATUS_" + sGXsfl_124_idx;
            cmbavFuncaoapf_status.Name = GXCCtl;
            cmbavFuncaoapf_status.WebTags = "";
            cmbavFuncaoapf_status.addItem("A", "Ativa", 0);
            cmbavFuncaoapf_status.addItem("E", "Exclu�da", 0);
            cmbavFuncaoapf_status.addItem("R", "Rejeitada", 0);
            if ( cmbavFuncaoapf_status.ItemCount > 0 )
            {
               AV81FuncaoAPF_Status = cmbavFuncaoapf_status.getValidValue(AV81FuncaoAPF_Status);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_status_Internalname, AV81FuncaoAPF_Status);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            chkavDynamicfiltersenabled4.Name = "vDYNAMICFILTERSENABLED4";
            chkavDynamicfiltersenabled4.WebTags = "";
            chkavDynamicfiltersenabled4.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled4_Internalname, "TitleCaption", chkavDynamicfiltersenabled4.Caption);
            chkavDynamicfiltersenabled4.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1242( ) ;
         while ( nGXsfl_124_idx <= nRC_GXsfl_124 )
         {
            sendrow_1242( ) ;
            nGXsfl_124_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_124_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_124_idx+1));
            sGXsfl_124_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_124_idx), 4, 0)), 4, "0");
            SubsflControlProps_1242( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV127Rows ,
                                       short AV59OrderedBy ,
                                       bool AV61OrderedDsc ,
                                       String AV136Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV50GridState ,
                                       bool AV19DynamicFiltersIgnoreFirst ,
                                       String AV24DynamicFiltersSelector1 ,
                                       String AV39FuncaoAPF_Nome1 ,
                                       String AV123FuncaoAPF_Tipo1 ,
                                       String AV119FuncaoAPF_Complexidade1 ,
                                       String AV115FuncaoAPF_Ativo1 ,
                                       bool AV23DynamicFiltersRemoving ,
                                       bool AV17DynamicFiltersEnabled2 ,
                                       String AV25DynamicFiltersSelector2 ,
                                       String AV40FuncaoAPF_Nome2 ,
                                       String AV124FuncaoAPF_Tipo2 ,
                                       String AV120FuncaoAPF_Complexidade2 ,
                                       String AV116FuncaoAPF_Ativo2 ,
                                       bool AV18DynamicFiltersEnabled3 ,
                                       String AV26DynamicFiltersSelector3 ,
                                       String AV41FuncaoAPF_Nome3 ,
                                       String AV125FuncaoAPF_Tipo3 ,
                                       String AV121FuncaoAPF_Complexidade3 ,
                                       String AV117FuncaoAPF_Ativo3 ,
                                       bool AV101DynamicFiltersEnabled4 ,
                                       String AV102DynamicFiltersSelector4 ,
                                       String AV109FuncaoAPF_Nome4 ,
                                       String AV126FuncaoAPF_Tipo4 ,
                                       String AV122FuncaoAPF_Complexidade4 ,
                                       String AV118FuncaoAPF_Ativo4 ,
                                       bool AV66SoRepetidas ,
                                       String A166FuncaoAPF_Nome ,
                                       String A184FuncaoAPF_Tipo ,
                                       String A183FuncaoAPF_Ativo ,
                                       String A185FuncaoAPF_Complexidade ,
                                       short A387FuncaoAPF_AR ,
                                       short A388FuncaoAPF_TD ,
                                       int A165FuncaoAPF_Codigo ,
                                       String A363FuncaoAPF_FunAPFPaiNom ,
                                       int AV42FuncaoAPF_SistemaCod ,
                                       int A358FuncaoAPF_FunAPFPaiCod ,
                                       decimal A386FuncaoAPF_PF ,
                                       String AV38FuncaoAPF_Nome ,
                                       String AV48FuncaoAPF_Tipo ,
                                       short AV27FuncaoAPF_AR ,
                                       short AV47FuncaoAPF_TD ,
                                       String AV89FuncaoAPF_Ativo ,
                                       IGxCollection AV85Processadas ,
                                       IGxCollection AV6Codigos )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFHA2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV59OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV59OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV24DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV24DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector1", AV24DynamicFiltersSelector1);
         }
         if ( cmbavFuncaoapf_tipo1.ItemCount > 0 )
         {
            AV123FuncaoAPF_Tipo1 = cmbavFuncaoapf_tipo1.getValidValue(AV123FuncaoAPF_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123FuncaoAPF_Tipo1", AV123FuncaoAPF_Tipo1);
         }
         if ( cmbavFuncaoapf_complexidade1.ItemCount > 0 )
         {
            AV119FuncaoAPF_Complexidade1 = cmbavFuncaoapf_complexidade1.getValidValue(AV119FuncaoAPF_Complexidade1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119FuncaoAPF_Complexidade1", AV119FuncaoAPF_Complexidade1);
         }
         if ( cmbavFuncaoapf_ativo1.ItemCount > 0 )
         {
            AV115FuncaoAPF_Ativo1 = cmbavFuncaoapf_ativo1.getValidValue(AV115FuncaoAPF_Ativo1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115FuncaoAPF_Ativo1", AV115FuncaoAPF_Ativo1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV25DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector2", AV25DynamicFiltersSelector2);
         }
         if ( cmbavFuncaoapf_tipo2.ItemCount > 0 )
         {
            AV124FuncaoAPF_Tipo2 = cmbavFuncaoapf_tipo2.getValidValue(AV124FuncaoAPF_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124FuncaoAPF_Tipo2", AV124FuncaoAPF_Tipo2);
         }
         if ( cmbavFuncaoapf_complexidade2.ItemCount > 0 )
         {
            AV120FuncaoAPF_Complexidade2 = cmbavFuncaoapf_complexidade2.getValidValue(AV120FuncaoAPF_Complexidade2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120FuncaoAPF_Complexidade2", AV120FuncaoAPF_Complexidade2);
         }
         if ( cmbavFuncaoapf_ativo2.ItemCount > 0 )
         {
            AV116FuncaoAPF_Ativo2 = cmbavFuncaoapf_ativo2.getValidValue(AV116FuncaoAPF_Ativo2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116FuncaoAPF_Ativo2", AV116FuncaoAPF_Ativo2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         }
         if ( cmbavFuncaoapf_tipo3.ItemCount > 0 )
         {
            AV125FuncaoAPF_Tipo3 = cmbavFuncaoapf_tipo3.getValidValue(AV125FuncaoAPF_Tipo3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125FuncaoAPF_Tipo3", AV125FuncaoAPF_Tipo3);
         }
         if ( cmbavFuncaoapf_complexidade3.ItemCount > 0 )
         {
            AV121FuncaoAPF_Complexidade3 = cmbavFuncaoapf_complexidade3.getValidValue(AV121FuncaoAPF_Complexidade3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121FuncaoAPF_Complexidade3", AV121FuncaoAPF_Complexidade3);
         }
         if ( cmbavFuncaoapf_ativo3.ItemCount > 0 )
         {
            AV117FuncaoAPF_Ativo3 = cmbavFuncaoapf_ativo3.getValidValue(AV117FuncaoAPF_Ativo3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117FuncaoAPF_Ativo3", AV117FuncaoAPF_Ativo3);
         }
         if ( cmbavDynamicfiltersselector4.ItemCount > 0 )
         {
            AV102DynamicFiltersSelector4 = cmbavDynamicfiltersselector4.getValidValue(AV102DynamicFiltersSelector4);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102DynamicFiltersSelector4", AV102DynamicFiltersSelector4);
         }
         if ( cmbavFuncaoapf_tipo4.ItemCount > 0 )
         {
            AV126FuncaoAPF_Tipo4 = cmbavFuncaoapf_tipo4.getValidValue(AV126FuncaoAPF_Tipo4);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126FuncaoAPF_Tipo4", AV126FuncaoAPF_Tipo4);
         }
         if ( cmbavFuncaoapf_complexidade4.ItemCount > 0 )
         {
            AV122FuncaoAPF_Complexidade4 = cmbavFuncaoapf_complexidade4.getValidValue(AV122FuncaoAPF_Complexidade4);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122FuncaoAPF_Complexidade4", AV122FuncaoAPF_Complexidade4);
         }
         if ( cmbavFuncaoapf_ativo4.ItemCount > 0 )
         {
            AV118FuncaoAPF_Ativo4 = cmbavFuncaoapf_ativo4.getValidValue(AV118FuncaoAPF_Ativo4);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118FuncaoAPF_Ativo4", AV118FuncaoAPF_Ativo4);
         }
         if ( cmbavSorepetidas.ItemCount > 0 )
         {
            AV66SoRepetidas = StringUtil.StrToBool( cmbavSorepetidas.getValidValue(StringUtil.BoolToStr( AV66SoRepetidas)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66SoRepetidas", AV66SoRepetidas);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFHA2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV136Pgmname = "WP_FuncoesTRN";
         context.Gx_err = 0;
         edtavFuncaoapf_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_codigo_Enabled), 5, 0)));
         edtavFuncao_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_nome_Enabled), 5, 0)));
         cmbavFuncaoapf_tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo.Enabled), 5, 0)));
         edtavFuncaoapf_td_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_td_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_td_Enabled), 5, 0)));
         edtavFuncaoapf_ar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_ar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_ar_Enabled), 5, 0)));
         cmbavFuncaoapf_complexidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade.Enabled), 5, 0)));
         edtavFuncaoapf_pf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_pf_Enabled), 5, 0)));
         edtavFunapfpainom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFunapfpainom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFunapfpainom_Enabled), 5, 0)));
         cmbavFuncaoapf_status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_status.Enabled), 5, 0)));
         edtavQtde_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
      }

      protected void RFHA2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 124;
         /* Execute user event: E30HA2 */
         E30HA2 ();
         nGXsfl_124_idx = 1;
         sGXsfl_124_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_124_idx), 4, 0)), 4, "0");
         SubsflControlProps_1242( ) ;
         nGXsfl_124_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1242( ) ;
            /* Execute user event: E31HA2 */
            E31HA2 ();
            wbEnd = 124;
            WBHA0( ) ;
         }
         nGXsfl_124_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
         }
         return (int)(0) ;
      }

      protected void STRUPHA0( )
      {
         /* Before Start, stand alone formulas. */
         AV136Pgmname = "WP_FuncoesTRN";
         context.Gx_err = 0;
         edtavFuncaoapf_codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_codigo_Enabled), 5, 0)));
         edtavFuncao_nome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_nome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncao_nome_Enabled), 5, 0)));
         cmbavFuncaoapf_tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo.Enabled), 5, 0)));
         edtavFuncaoapf_td_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_td_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_td_Enabled), 5, 0)));
         edtavFuncaoapf_ar_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_ar_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_ar_Enabled), 5, 0)));
         cmbavFuncaoapf_complexidade.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade.Enabled), 5, 0)));
         edtavFuncaoapf_pf_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_pf_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_pf_Enabled), 5, 0)));
         edtavFunapfpainom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFunapfpainom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFunapfpainom_Enabled), 5, 0)));
         cmbavFuncaoapf_status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_status.Enabled), 5, 0)));
         edtavQtde_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQtde_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQtde_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E29HA2 */
         E29HA2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV59OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OrderedBy), 4, 0)));
            AV61OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61OrderedDsc", AV61OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV24DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector1", AV24DynamicFiltersSelector1);
            AV39FuncaoAPF_Nome1 = cgiGet( edtavFuncaoapf_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39FuncaoAPF_Nome1", AV39FuncaoAPF_Nome1);
            cmbavFuncaoapf_tipo1.Name = cmbavFuncaoapf_tipo1_Internalname;
            cmbavFuncaoapf_tipo1.CurrentValue = cgiGet( cmbavFuncaoapf_tipo1_Internalname);
            AV123FuncaoAPF_Tipo1 = cgiGet( cmbavFuncaoapf_tipo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123FuncaoAPF_Tipo1", AV123FuncaoAPF_Tipo1);
            cmbavFuncaoapf_complexidade1.Name = cmbavFuncaoapf_complexidade1_Internalname;
            cmbavFuncaoapf_complexidade1.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade1_Internalname);
            AV119FuncaoAPF_Complexidade1 = cgiGet( cmbavFuncaoapf_complexidade1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119FuncaoAPF_Complexidade1", AV119FuncaoAPF_Complexidade1);
            cmbavFuncaoapf_ativo1.Name = cmbavFuncaoapf_ativo1_Internalname;
            cmbavFuncaoapf_ativo1.CurrentValue = cgiGet( cmbavFuncaoapf_ativo1_Internalname);
            AV115FuncaoAPF_Ativo1 = cgiGet( cmbavFuncaoapf_ativo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115FuncaoAPF_Ativo1", AV115FuncaoAPF_Ativo1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV25DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector2", AV25DynamicFiltersSelector2);
            AV40FuncaoAPF_Nome2 = cgiGet( edtavFuncaoapf_nome2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40FuncaoAPF_Nome2", AV40FuncaoAPF_Nome2);
            cmbavFuncaoapf_tipo2.Name = cmbavFuncaoapf_tipo2_Internalname;
            cmbavFuncaoapf_tipo2.CurrentValue = cgiGet( cmbavFuncaoapf_tipo2_Internalname);
            AV124FuncaoAPF_Tipo2 = cgiGet( cmbavFuncaoapf_tipo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124FuncaoAPF_Tipo2", AV124FuncaoAPF_Tipo2);
            cmbavFuncaoapf_complexidade2.Name = cmbavFuncaoapf_complexidade2_Internalname;
            cmbavFuncaoapf_complexidade2.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade2_Internalname);
            AV120FuncaoAPF_Complexidade2 = cgiGet( cmbavFuncaoapf_complexidade2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120FuncaoAPF_Complexidade2", AV120FuncaoAPF_Complexidade2);
            cmbavFuncaoapf_ativo2.Name = cmbavFuncaoapf_ativo2_Internalname;
            cmbavFuncaoapf_ativo2.CurrentValue = cgiGet( cmbavFuncaoapf_ativo2_Internalname);
            AV116FuncaoAPF_Ativo2 = cgiGet( cmbavFuncaoapf_ativo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116FuncaoAPF_Ativo2", AV116FuncaoAPF_Ativo2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV26DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            AV41FuncaoAPF_Nome3 = cgiGet( edtavFuncaoapf_nome3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoAPF_Nome3", AV41FuncaoAPF_Nome3);
            cmbavFuncaoapf_tipo3.Name = cmbavFuncaoapf_tipo3_Internalname;
            cmbavFuncaoapf_tipo3.CurrentValue = cgiGet( cmbavFuncaoapf_tipo3_Internalname);
            AV125FuncaoAPF_Tipo3 = cgiGet( cmbavFuncaoapf_tipo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125FuncaoAPF_Tipo3", AV125FuncaoAPF_Tipo3);
            cmbavFuncaoapf_complexidade3.Name = cmbavFuncaoapf_complexidade3_Internalname;
            cmbavFuncaoapf_complexidade3.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade3_Internalname);
            AV121FuncaoAPF_Complexidade3 = cgiGet( cmbavFuncaoapf_complexidade3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121FuncaoAPF_Complexidade3", AV121FuncaoAPF_Complexidade3);
            cmbavFuncaoapf_ativo3.Name = cmbavFuncaoapf_ativo3_Internalname;
            cmbavFuncaoapf_ativo3.CurrentValue = cgiGet( cmbavFuncaoapf_ativo3_Internalname);
            AV117FuncaoAPF_Ativo3 = cgiGet( cmbavFuncaoapf_ativo3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117FuncaoAPF_Ativo3", AV117FuncaoAPF_Ativo3);
            cmbavDynamicfiltersselector4.Name = cmbavDynamicfiltersselector4_Internalname;
            cmbavDynamicfiltersselector4.CurrentValue = cgiGet( cmbavDynamicfiltersselector4_Internalname);
            AV102DynamicFiltersSelector4 = cgiGet( cmbavDynamicfiltersselector4_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102DynamicFiltersSelector4", AV102DynamicFiltersSelector4);
            AV109FuncaoAPF_Nome4 = cgiGet( edtavFuncaoapf_nome4_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109FuncaoAPF_Nome4", AV109FuncaoAPF_Nome4);
            cmbavFuncaoapf_tipo4.Name = cmbavFuncaoapf_tipo4_Internalname;
            cmbavFuncaoapf_tipo4.CurrentValue = cgiGet( cmbavFuncaoapf_tipo4_Internalname);
            AV126FuncaoAPF_Tipo4 = cgiGet( cmbavFuncaoapf_tipo4_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126FuncaoAPF_Tipo4", AV126FuncaoAPF_Tipo4);
            cmbavFuncaoapf_complexidade4.Name = cmbavFuncaoapf_complexidade4_Internalname;
            cmbavFuncaoapf_complexidade4.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade4_Internalname);
            AV122FuncaoAPF_Complexidade4 = cgiGet( cmbavFuncaoapf_complexidade4_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122FuncaoAPF_Complexidade4", AV122FuncaoAPF_Complexidade4);
            cmbavFuncaoapf_ativo4.Name = cmbavFuncaoapf_ativo4_Internalname;
            cmbavFuncaoapf_ativo4.CurrentValue = cgiGet( cmbavFuncaoapf_ativo4_Internalname);
            AV118FuncaoAPF_Ativo4 = cgiGet( cmbavFuncaoapf_ativo4_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118FuncaoAPF_Ativo4", AV118FuncaoAPF_Ativo4);
            cmbavSorepetidas.Name = cmbavSorepetidas_Internalname;
            cmbavSorepetidas.CurrentValue = cgiGet( cmbavSorepetidas_Internalname);
            AV66SoRepetidas = StringUtil.StrToBool( cgiGet( cmbavSorepetidas_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66SoRepetidas", AV66SoRepetidas);
            AV87SelectAll = StringUtil.StrToBool( cgiGet( chkavSelectall_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87SelectAll", AV87SelectAll);
            AV90ReverseAll = StringUtil.StrToBool( cgiGet( chkavReverseall_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ReverseAll", AV90ReverseAll);
            AV91EmCascata = StringUtil.StrToBool( cgiGet( chkavEmcascata_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91EmCascata", AV91EmCascata);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavQtde_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavQtde_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vQTDE");
               GX_FocusControl = edtavQtde_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV84Qtde = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84Qtde", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84Qtde), 4, 0)));
            }
            else
            {
               AV84Qtde = (short)(context.localUtil.CToN( cgiGet( edtavQtde_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84Qtde", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84Qtde), 4, 0)));
            }
            AV17DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
            AV18DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled3", AV18DynamicFiltersEnabled3);
            AV101DynamicFiltersEnabled4 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled4_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101DynamicFiltersEnabled4", AV101DynamicFiltersEnabled4);
            /* Read saved values. */
            nRC_GXsfl_124 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_124"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E29HA2 */
         E29HA2 ();
         if (returnInSub) return;
      }

      protected void E29HA2( )
      {
         /* Start Routine */
         AV127Rows = 50;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV127Rows", StringUtil.LTrim( StringUtil.Str( (decimal)(AV127Rows), 4, 0)));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         chkavDynamicfiltersenabled4.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled4.Visible), 5, 0)));
         cmbavFuncaoapf_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo1.Visible), 5, 0)));
         AV123FuncaoAPF_Tipo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123FuncaoAPF_Tipo1", AV123FuncaoAPF_Tipo1);
         cmbavFuncaoapf_complexidade1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade1.Visible), 5, 0)));
         AV119FuncaoAPF_Complexidade1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119FuncaoAPF_Complexidade1", AV119FuncaoAPF_Complexidade1);
         cmbavFuncaoapf_ativo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo1.Visible), 5, 0)));
         AV115FuncaoAPF_Ativo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115FuncaoAPF_Ativo1", AV115FuncaoAPF_Ativo1);
         cmbavFuncaoapf_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo2.Visible), 5, 0)));
         AV124FuncaoAPF_Tipo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124FuncaoAPF_Tipo2", AV124FuncaoAPF_Tipo2);
         cmbavFuncaoapf_complexidade2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade2.Visible), 5, 0)));
         AV120FuncaoAPF_Complexidade2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120FuncaoAPF_Complexidade2", AV120FuncaoAPF_Complexidade2);
         cmbavFuncaoapf_ativo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo2.Visible), 5, 0)));
         AV116FuncaoAPF_Ativo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116FuncaoAPF_Ativo2", AV116FuncaoAPF_Ativo2);
         cmbavFuncaoapf_tipo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo3.Visible), 5, 0)));
         AV125FuncaoAPF_Tipo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125FuncaoAPF_Tipo3", AV125FuncaoAPF_Tipo3);
         cmbavFuncaoapf_complexidade3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade3.Visible), 5, 0)));
         AV121FuncaoAPF_Complexidade3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121FuncaoAPF_Complexidade3", AV121FuncaoAPF_Complexidade3);
         cmbavFuncaoapf_ativo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo3.Visible), 5, 0)));
         AV117FuncaoAPF_Ativo3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117FuncaoAPF_Ativo3", AV117FuncaoAPF_Ativo3);
         cmbavFuncaoapf_tipo4.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo4.Visible), 5, 0)));
         AV126FuncaoAPF_Tipo4 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126FuncaoAPF_Tipo4", AV126FuncaoAPF_Tipo4);
         cmbavFuncaoapf_complexidade4.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade4.Visible), 5, 0)));
         AV122FuncaoAPF_Complexidade4 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122FuncaoAPF_Complexidade4", AV122FuncaoAPF_Complexidade4);
         cmbavFuncaoapf_ativo4.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo4.Visible), 5, 0)));
         AV118FuncaoAPF_Ativo4 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118FuncaoAPF_Ativo4", AV118FuncaoAPF_Ativo4);
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgAdddynamicfilters3_Jsonclick = "WWPDynFilterShow(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters3_Internalname, "Jsonclick", imgAdddynamicfilters3_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         imgRemovedynamicfilters4_Jsonclick = "WWPDynFilterHideLast(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters4_Internalname, "Jsonclick", imgRemovedynamicfilters4_Jsonclick);
         Form.Caption = " Fun��es de Transa��";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S112 ();
         if (returnInSub) return;
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Tipo", 0);
         cmbavOrderedby.addItem("3", "Vinculada com", 0);
         cmbavOrderedby.addItem("4", "Status", 0);
         if ( AV59OrderedBy < 1 )
         {
            AV59OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(4)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         lblFuncaoapftitle_Caption = "Fun��es de Transa��o de "+StringUtil.Upper( StringUtil.Trim( AV128Sistema_Sigla));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblFuncaoapftitle_Internalname, "Caption", lblFuncaoapftitle_Caption);
      }

      protected void E30HA2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV75WWPContext) ;
         subGrid_Rows = AV127Rows;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S122 ();
         if (returnInSub) return;
         edtavFuncao_nome_Titleformat = 2;
         edtavFuncao_nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV59OrderedBy==1) ? (AV61OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncao_nome_Internalname, "Title", edtavFuncao_nome_Title);
         cmbavFuncaoapf_tipo_Titleformat = 2;
         cmbavFuncaoapf_tipo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV59OrderedBy==2) ? (AV61OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tipo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo_Internalname, "Title", cmbavFuncaoapf_tipo.Title.Text);
         edtavFunapfpainom_Titleformat = 2;
         edtavFunapfpainom_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV59OrderedBy==3) ? (AV61OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Vinculada com", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFunapfpainom_Internalname, "Title", edtavFunapfpainom_Title);
         cmbavFuncaoapf_status_Titleformat = 2;
         cmbavFuncaoapf_status.Title.Text = StringUtil.Format( "<span c	lass='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV59OrderedBy==4) ? (AV61OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Status", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_status_Internalname, "Title", cmbavFuncaoapf_status.Title.Text);
         chkavSelected.Enabled = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelected_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavSelected.Enabled), 5, 0)));
         AV87SelectAll = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV87SelectAll", AV87SelectAll);
         AV90ReverseAll = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV90ReverseAll", AV90ReverseAll);
         AV91EmCascata = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91EmCascata", AV91EmCascata);
         cmbavFuncaoapf_complexidade1.removeItem("E");
         cmbavFuncaoapf_complexidade2.removeItem("E");
         cmbavFuncaoapf_complexidade3.removeItem("E");
         cmbavFuncaoapf_complexidade4.removeItem("E");
         cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV119FuncaoAPF_Complexidade1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", cmbavFuncaoapf_complexidade1.ToJavascriptSource());
         cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV120FuncaoAPF_Complexidade2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", cmbavFuncaoapf_complexidade2.ToJavascriptSource());
         cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV121FuncaoAPF_Complexidade3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", cmbavFuncaoapf_complexidade3.ToJavascriptSource());
         cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV122FuncaoAPF_Complexidade4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", cmbavFuncaoapf_complexidade4.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50GridState", AV50GridState);
      }

      private void E31HA2( )
      {
         /* Grid_Load Routine */
         AV84Qtde = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84Qtde", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84Qtde), 4, 0)));
         AV6Codigos.Clear();
         if ( AV66SoRepetidas )
         {
            AV85Processadas.Clear();
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV24DynamicFiltersSelector1 ,
                                                 AV39FuncaoAPF_Nome1 ,
                                                 AV123FuncaoAPF_Tipo1 ,
                                                 AV115FuncaoAPF_Ativo1 ,
                                                 AV17DynamicFiltersEnabled2 ,
                                                 AV25DynamicFiltersSelector2 ,
                                                 AV40FuncaoAPF_Nome2 ,
                                                 AV124FuncaoAPF_Tipo2 ,
                                                 AV116FuncaoAPF_Ativo2 ,
                                                 AV18DynamicFiltersEnabled3 ,
                                                 AV26DynamicFiltersSelector3 ,
                                                 AV41FuncaoAPF_Nome3 ,
                                                 AV125FuncaoAPF_Tipo3 ,
                                                 AV117FuncaoAPF_Ativo3 ,
                                                 AV101DynamicFiltersEnabled4 ,
                                                 AV102DynamicFiltersSelector4 ,
                                                 AV109FuncaoAPF_Nome4 ,
                                                 AV126FuncaoAPF_Tipo4 ,
                                                 AV118FuncaoAPF_Ativo4 ,
                                                 AV50GridState.gxTpr_Dynamicfilters.Count ,
                                                 A166FuncaoAPF_Nome ,
                                                 A184FuncaoAPF_Tipo ,
                                                 A183FuncaoAPF_Ativo ,
                                                 A165FuncaoAPF_Codigo ,
                                                 A360FuncaoAPF_SistemaCod ,
                                                 AV119FuncaoAPF_Complexidade1 ,
                                                 A185FuncaoAPF_Complexidade ,
                                                 AV120FuncaoAPF_Complexidade2 ,
                                                 AV121FuncaoAPF_Complexidade3 ,
                                                 AV122FuncaoAPF_Complexidade4 },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING
                                                 }
            });
            lV39FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV39FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39FuncaoAPF_Nome1", AV39FuncaoAPF_Nome1);
            lV40FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV40FuncaoAPF_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40FuncaoAPF_Nome2", AV40FuncaoAPF_Nome2);
            lV41FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV41FuncaoAPF_Nome3), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoAPF_Nome3", AV41FuncaoAPF_Nome3);
            lV109FuncaoAPF_Nome4 = StringUtil.Concat( StringUtil.RTrim( AV109FuncaoAPF_Nome4), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109FuncaoAPF_Nome4", AV109FuncaoAPF_Nome4);
            /* Using cursor H00HA2 */
            pr_default.execute(0, new Object[] {n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod, lV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV115FuncaoAPF_Ativo1, lV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV116FuncaoAPF_Ativo2, lV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV117FuncaoAPF_Ativo3, lV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV118FuncaoAPF_Ativo4});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A183FuncaoAPF_Ativo = H00HA2_A183FuncaoAPF_Ativo[0];
               A184FuncaoAPF_Tipo = H00HA2_A184FuncaoAPF_Tipo[0];
               A166FuncaoAPF_Nome = H00HA2_A166FuncaoAPF_Nome[0];
               A165FuncaoAPF_Codigo = H00HA2_A165FuncaoAPF_Codigo[0];
               GXt_int1 = A388FuncaoAPF_TD;
               new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A388FuncaoAPF_TD = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
               GXt_int1 = A387FuncaoAPF_AR;
               new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A387FuncaoAPF_AR = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
               GXt_char2 = A185FuncaoAPF_Complexidade;
               new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A185FuncaoAPF_Complexidade = GXt_char2;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
               if ( ! ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "2") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119FuncaoAPF_Complexidade1)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV119FuncaoAPF_Complexidade1) == 0 ) ) )
               {
                  if ( ! ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "2") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120FuncaoAPF_Complexidade2)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV120FuncaoAPF_Complexidade2) == 0 ) ) )
                  {
                     if ( ! ( AV18DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "2") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121FuncaoAPF_Complexidade3)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV121FuncaoAPF_Complexidade3) == 0 ) ) )
                     {
                        if ( ! ( AV101DynamicFiltersEnabled4 && ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "2") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122FuncaoAPF_Complexidade4)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV122FuncaoAPF_Complexidade4) == 0 ) ) )
                        {
                           AV27FuncaoAPF_AR = A387FuncaoAPF_AR;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_ar_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV27FuncaoAPF_AR), 4, 0)));
                           AV47FuncaoAPF_TD = A388FuncaoAPF_TD;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_td_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV47FuncaoAPF_TD), 4, 0)));
                           AV38FuncaoAPF_Nome = A166FuncaoAPF_Nome;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38FuncaoAPF_Nome", AV38FuncaoAPF_Nome);
                           AV48FuncaoAPF_Tipo = A184FuncaoAPF_Tipo;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_tipo_Internalname, AV48FuncaoAPF_Tipo);
                           AV89FuncaoAPF_Ativo = A183FuncaoAPF_Ativo;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV89FuncaoAPF_Ativo", AV89FuncaoAPF_Ativo);
                           AV85Processadas.Add(A165FuncaoAPF_Codigo, 0);
                           /* Execute user subroutine: 'NOMESREPETIDOS' */
                           S133 ();
                           if ( returnInSub )
                           {
                              pr_default.close(0);
                              returnInSub = true;
                              if (true) return;
                           }
                           if ( AV6Codigos.Count >= AV127Rows )
                           {
                              subGrid_Rows = AV6Codigos.Count;
                              GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
                              /* Exit For each command. Update data (if necessary), close cursors & exit. */
                              if (true) break;
                           }
                        }
                     }
                  }
               }
               pr_default.readNext(0);
            }
            pr_default.close(0);
         }
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A165FuncaoAPF_Codigo ,
                                              AV6Codigos ,
                                              AV24DynamicFiltersSelector1 ,
                                              AV39FuncaoAPF_Nome1 ,
                                              AV123FuncaoAPF_Tipo1 ,
                                              AV115FuncaoAPF_Ativo1 ,
                                              AV17DynamicFiltersEnabled2 ,
                                              AV25DynamicFiltersSelector2 ,
                                              AV40FuncaoAPF_Nome2 ,
                                              AV124FuncaoAPF_Tipo2 ,
                                              AV116FuncaoAPF_Ativo2 ,
                                              AV18DynamicFiltersEnabled3 ,
                                              AV26DynamicFiltersSelector3 ,
                                              AV41FuncaoAPF_Nome3 ,
                                              AV125FuncaoAPF_Tipo3 ,
                                              AV117FuncaoAPF_Ativo3 ,
                                              AV101DynamicFiltersEnabled4 ,
                                              AV102DynamicFiltersSelector4 ,
                                              AV109FuncaoAPF_Nome4 ,
                                              AV126FuncaoAPF_Tipo4 ,
                                              AV118FuncaoAPF_Ativo4 ,
                                              AV66SoRepetidas ,
                                              AV50GridState.gxTpr_Dynamicfilters.Count ,
                                              A166FuncaoAPF_Nome ,
                                              A184FuncaoAPF_Tipo ,
                                              A183FuncaoAPF_Ativo ,
                                              AV59OrderedBy ,
                                              A360FuncaoAPF_SistemaCod ,
                                              AV119FuncaoAPF_Complexidade1 ,
                                              A185FuncaoAPF_Complexidade ,
                                              AV120FuncaoAPF_Complexidade2 ,
                                              AV121FuncaoAPF_Complexidade3 ,
                                              AV122FuncaoAPF_Complexidade4 },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         lV39FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV39FuncaoAPF_Nome1), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39FuncaoAPF_Nome1", AV39FuncaoAPF_Nome1);
         lV40FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV40FuncaoAPF_Nome2), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40FuncaoAPF_Nome2", AV40FuncaoAPF_Nome2);
         lV41FuncaoAPF_Nome3 = StringUtil.Concat( StringUtil.RTrim( AV41FuncaoAPF_Nome3), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoAPF_Nome3", AV41FuncaoAPF_Nome3);
         lV109FuncaoAPF_Nome4 = StringUtil.Concat( StringUtil.RTrim( AV109FuncaoAPF_Nome4), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109FuncaoAPF_Nome4", AV109FuncaoAPF_Nome4);
         /* Using cursor H00HA3 */
         pr_default.execute(1, new Object[] {n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod, lV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV115FuncaoAPF_Ativo1, lV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV116FuncaoAPF_Ativo2, lV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV117FuncaoAPF_Ativo3, lV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV118FuncaoAPF_Ativo4});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A183FuncaoAPF_Ativo = H00HA3_A183FuncaoAPF_Ativo[0];
            A184FuncaoAPF_Tipo = H00HA3_A184FuncaoAPF_Tipo[0];
            A166FuncaoAPF_Nome = H00HA3_A166FuncaoAPF_Nome[0];
            A358FuncaoAPF_FunAPFPaiCod = H00HA3_A358FuncaoAPF_FunAPFPaiCod[0];
            n358FuncaoAPF_FunAPFPaiCod = H00HA3_n358FuncaoAPF_FunAPFPaiCod[0];
            A363FuncaoAPF_FunAPFPaiNom = H00HA3_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = H00HA3_n363FuncaoAPF_FunAPFPaiNom[0];
            A165FuncaoAPF_Codigo = H00HA3_A165FuncaoAPF_Codigo[0];
            A363FuncaoAPF_FunAPFPaiNom = H00HA3_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = H00HA3_n363FuncaoAPF_FunAPFPaiNom[0];
            GXt_decimal3 = A386FuncaoAPF_PF;
            new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A386FuncaoAPF_PF = GXt_decimal3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A386FuncaoAPF_PF", StringUtil.LTrim( StringUtil.Str( A386FuncaoAPF_PF, 14, 5)));
            GXt_int1 = A387FuncaoAPF_AR;
            new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A387FuncaoAPF_AR = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
            GXt_int1 = A388FuncaoAPF_TD;
            new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A388FuncaoAPF_TD = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
            GXt_char2 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char2) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A185FuncaoAPF_Complexidade = GXt_char2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            if ( ! ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "2") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV119FuncaoAPF_Complexidade1)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV119FuncaoAPF_Complexidade1) == 0 ) ) )
            {
               if ( ! ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "2") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV120FuncaoAPF_Complexidade2)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV120FuncaoAPF_Complexidade2) == 0 ) ) )
               {
                  if ( ! ( AV18DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "2") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV121FuncaoAPF_Complexidade3)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV121FuncaoAPF_Complexidade3) == 0 ) ) )
                  {
                     if ( ! ( AV101DynamicFiltersEnabled4 && ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "2") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV122FuncaoAPF_Complexidade4)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV122FuncaoAPF_Complexidade4) == 0 ) ) )
                     {
                        AV71Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV71Update);
                        AV133Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
                        edtavUpdate_Tooltiptext = "Modifica";
                        edtavUpdate_Link = formatLink("funcaoapf.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV42FuncaoAPF_SistemaCod);
                        AV15Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV15Delete);
                        AV134Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
                        edtavDelete_Tooltiptext = "Eliminar";
                        edtavDelete_Link = formatLink("funcaoapf.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV42FuncaoAPF_SistemaCod);
                        AV16Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV16Display);
                        AV135Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
                        edtavDisplay_Tooltiptext = "Mostrar";
                        edtavDisplay_Link = formatLink("viewfuncaoapf.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV42FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
                        edtavFuncao_nome_Link = formatLink("viewfuncaoapf.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV42FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
                        edtavFunapfpainom_Link = formatLink("viewfuncaoapf.aspx") + "?" + UrlEncode("" +A358FuncaoAPF_FunAPFPaiCod) + "," + UrlEncode("" +AV42FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
                        AV30FuncaoAPF_Codigo = A165FuncaoAPF_Codigo;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_codigo_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV30FuncaoAPF_Codigo), 6, 0)));
                        AV80Funcao_Nome = A166FuncaoAPF_Nome;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncao_nome_Internalname, AV80Funcao_Nome);
                        AV48FuncaoAPF_Tipo = A184FuncaoAPF_Tipo;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_tipo_Internalname, AV48FuncaoAPF_Tipo);
                        AV47FuncaoAPF_TD = A388FuncaoAPF_TD;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_td_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV47FuncaoAPF_TD), 4, 0)));
                        AV27FuncaoAPF_AR = A387FuncaoAPF_AR;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_ar_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV27FuncaoAPF_AR), 4, 0)));
                        AV78FuncaoAPF_PF = A386FuncaoAPF_PF;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFuncaoapf_pf_Internalname, StringUtil.LTrim( StringUtil.Str( AV78FuncaoAPF_PF, 14, 5)));
                        AV79FunAPFPaiNom = A363FuncaoAPF_FunAPFPaiNom;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavFunapfpainom_Internalname, AV79FunAPFPaiNom);
                        AV81FuncaoAPF_Status = A183FuncaoAPF_Ativo;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_status_Internalname, AV81FuncaoAPF_Status);
                        AV77FuncaoAPF_Complexidade = A185FuncaoAPF_Complexidade;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_complexidade_Internalname, AV77FuncaoAPF_Complexidade);
                        /* Load Method */
                        if ( wbStart != -1 )
                        {
                           wbStart = 124;
                        }
                        if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
                        {
                           sendrow_1242( ) ;
                           GRID_nEOF = 1;
                           GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                           if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
                           {
                              GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
                           }
                        }
                        if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
                        {
                           GRID_nEOF = 0;
                           GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
                        }
                        GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
                        if ( isFullAjaxMode( ) && ( nGXsfl_124_Refreshing == 0 ) )
                        {
                           context.DoAjaxLoad(124, GridRow);
                        }
                        AV84Qtde = (short)(AV84Qtde+1);
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84Qtde", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84Qtde), 4, 0)));
                     }
                  }
               }
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6Codigos", AV6Codigos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV85Processadas", AV85Processadas);
         cmbavFuncaoapf_tipo.CurrentValue = StringUtil.RTrim( AV48FuncaoAPF_Tipo);
         cmbavFuncaoapf_status.CurrentValue = StringUtil.RTrim( AV81FuncaoAPF_Status);
         cmbavFuncaoapf_complexidade.CurrentValue = StringUtil.RTrim( AV77FuncaoAPF_Complexidade);
      }

      protected void E11HA2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E21HA2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV17DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E12HA2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV19DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersIgnoreFirst", AV19DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if (returnInSub) return;
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV19DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersIgnoreFirst", AV19DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50GridState", AV50GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV102DynamicFiltersSelector4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", cmbavDynamicfiltersselector4.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV123FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV119FuncaoAPF_Complexidade1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", cmbavFuncaoapf_complexidade1.ToJavascriptSource());
         cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV115FuncaoAPF_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", cmbavFuncaoapf_ativo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV124FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV120FuncaoAPF_Complexidade2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", cmbavFuncaoapf_complexidade2.ToJavascriptSource());
         cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV116FuncaoAPF_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", cmbavFuncaoapf_ativo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV125FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
         cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV121FuncaoAPF_Complexidade3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", cmbavFuncaoapf_complexidade3.ToJavascriptSource());
         cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV117FuncaoAPF_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", cmbavFuncaoapf_ativo3.ToJavascriptSource());
         cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV126FuncaoAPF_Tipo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", cmbavFuncaoapf_tipo4.ToJavascriptSource());
         cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV122FuncaoAPF_Complexidade4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", cmbavFuncaoapf_complexidade4.ToJavascriptSource());
         cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV118FuncaoAPF_Ativo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", cmbavFuncaoapf_ativo4.ToJavascriptSource());
      }

      protected void E22HA2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S172 ();
         if (returnInSub) return;
      }

      protected void E23HA2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV18DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled3", AV18DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E13HA2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if (returnInSub) return;
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50GridState", AV50GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV102DynamicFiltersSelector4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", cmbavDynamicfiltersselector4.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV123FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV119FuncaoAPF_Complexidade1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", cmbavFuncaoapf_complexidade1.ToJavascriptSource());
         cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV115FuncaoAPF_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", cmbavFuncaoapf_ativo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV124FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV120FuncaoAPF_Complexidade2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", cmbavFuncaoapf_complexidade2.ToJavascriptSource());
         cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV116FuncaoAPF_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", cmbavFuncaoapf_ativo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV125FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
         cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV121FuncaoAPF_Complexidade3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", cmbavFuncaoapf_complexidade3.ToJavascriptSource());
         cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV117FuncaoAPF_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", cmbavFuncaoapf_ativo3.ToJavascriptSource());
         cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV126FuncaoAPF_Tipo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", cmbavFuncaoapf_tipo4.ToJavascriptSource());
         cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV122FuncaoAPF_Complexidade4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", cmbavFuncaoapf_complexidade4.ToJavascriptSource());
         cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV118FuncaoAPF_Ativo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", cmbavFuncaoapf_ativo4.ToJavascriptSource());
      }

      protected void E24HA2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S182 ();
         if (returnInSub) return;
      }

      protected void E25HA2( )
      {
         /* 'AddDynamicFilters3' Routine */
         AV101DynamicFiltersEnabled4 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101DynamicFiltersEnabled4", AV101DynamicFiltersEnabled4);
         imgAdddynamicfilters3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters3_Visible), 5, 0)));
         imgRemovedynamicfilters3_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters3_Visible), 5, 0)));
      }

      protected void E14HA2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled3", AV18DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if (returnInSub) return;
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50GridState", AV50GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV102DynamicFiltersSelector4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", cmbavDynamicfiltersselector4.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV123FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV119FuncaoAPF_Complexidade1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", cmbavFuncaoapf_complexidade1.ToJavascriptSource());
         cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV115FuncaoAPF_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", cmbavFuncaoapf_ativo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV124FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV120FuncaoAPF_Complexidade2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", cmbavFuncaoapf_complexidade2.ToJavascriptSource());
         cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV116FuncaoAPF_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", cmbavFuncaoapf_ativo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV125FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
         cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV121FuncaoAPF_Complexidade3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", cmbavFuncaoapf_complexidade3.ToJavascriptSource());
         cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV117FuncaoAPF_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", cmbavFuncaoapf_ativo3.ToJavascriptSource());
         cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV126FuncaoAPF_Tipo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", cmbavFuncaoapf_tipo4.ToJavascriptSource());
         cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV122FuncaoAPF_Complexidade4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", cmbavFuncaoapf_complexidade4.ToJavascriptSource());
         cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV118FuncaoAPF_Ativo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", cmbavFuncaoapf_ativo4.ToJavascriptSource());
      }

      protected void E26HA2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S192 ();
         if (returnInSub) return;
      }

      protected void E15HA2( )
      {
         /* 'RemoveDynamicFilters4' Routine */
         AV23DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         AV101DynamicFiltersEnabled4 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101DynamicFiltersEnabled4", AV101DynamicFiltersEnabled4);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if (returnInSub) return;
         AV23DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersRemoving", AV23DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50GridState", AV50GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV102DynamicFiltersSelector4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", cmbavDynamicfiltersselector4.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV123FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV119FuncaoAPF_Complexidade1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", cmbavFuncaoapf_complexidade1.ToJavascriptSource());
         cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV115FuncaoAPF_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", cmbavFuncaoapf_ativo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV124FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV120FuncaoAPF_Complexidade2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", cmbavFuncaoapf_complexidade2.ToJavascriptSource());
         cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV116FuncaoAPF_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", cmbavFuncaoapf_ativo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV125FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
         cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV121FuncaoAPF_Complexidade3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", cmbavFuncaoapf_complexidade3.ToJavascriptSource());
         cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV117FuncaoAPF_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", cmbavFuncaoapf_ativo3.ToJavascriptSource());
         cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV126FuncaoAPF_Tipo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", cmbavFuncaoapf_tipo4.ToJavascriptSource());
         cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV122FuncaoAPF_Complexidade4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", cmbavFuncaoapf_complexidade4.ToJavascriptSource());
         cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV118FuncaoAPF_Ativo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", cmbavFuncaoapf_ativo4.ToJavascriptSource());
      }

      protected void E27HA2( )
      {
         /* Dynamicfiltersselector4_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS4' */
         S202 ();
         if (returnInSub) return;
      }

      protected void E16HA2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if (returnInSub) return;
         gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50GridState", AV50GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV102DynamicFiltersSelector4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", cmbavDynamicfiltersselector4.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV123FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV119FuncaoAPF_Complexidade1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", cmbavFuncaoapf_complexidade1.ToJavascriptSource());
         cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV115FuncaoAPF_Ativo1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", cmbavFuncaoapf_ativo1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV124FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV120FuncaoAPF_Complexidade2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", cmbavFuncaoapf_complexidade2.ToJavascriptSource());
         cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV116FuncaoAPF_Ativo2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", cmbavFuncaoapf_ativo2.ToJavascriptSource());
         cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV125FuncaoAPF_Tipo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", cmbavFuncaoapf_tipo3.ToJavascriptSource());
         cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV121FuncaoAPF_Complexidade3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", cmbavFuncaoapf_complexidade3.ToJavascriptSource());
         cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV117FuncaoAPF_Ativo3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", cmbavFuncaoapf_ativo3.ToJavascriptSource());
         cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV126FuncaoAPF_Tipo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", cmbavFuncaoapf_tipo4.ToJavascriptSource());
         cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV122FuncaoAPF_Complexidade4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", cmbavFuncaoapf_complexidade4.ToJavascriptSource());
         cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV118FuncaoAPF_Ativo4);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", cmbavFuncaoapf_ativo4.ToJavascriptSource());
      }

      protected void E17HA2( )
      {
         /* 'DoAtualizar' Routine */
         context.DoAjaxRefresh();
      }

      protected void E18HA2( )
      {
         /* 'DoApagarSelecionadas' Routine */
         AV86WebSession.Set("Codigos", AV6Codigos.ToXml(false, true, "Collection", ""));
         new prc_funcaoestrnrepetidasdlt(context ).execute( ref  AV91EmCascata) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV91EmCascata", AV91EmCascata);
         AV86WebSession.Remove("Codigos");
         gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
      }

      protected void S172( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaoapf_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
         cmbavFuncaoapf_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo1.Visible), 5, 0)));
         cmbavFuncaoapf_complexidade1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade1.Visible), 5, 0)));
         cmbavFuncaoapf_ativo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "0") == 0 )
         {
            edtavFuncaoapf_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "1") == 0 )
         {
            cmbavFuncaoapf_tipo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "2") == 0 )
         {
            cmbavFuncaoapf_complexidade1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "3") == 0 )
         {
            cmbavFuncaoapf_ativo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo1.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavFuncaoapf_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
         cmbavFuncaoapf_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo2.Visible), 5, 0)));
         cmbavFuncaoapf_complexidade2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade2.Visible), 5, 0)));
         cmbavFuncaoapf_ativo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "0") == 0 )
         {
            edtavFuncaoapf_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "1") == 0 )
         {
            cmbavFuncaoapf_tipo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "2") == 0 )
         {
            cmbavFuncaoapf_complexidade2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "3") == 0 )
         {
            cmbavFuncaoapf_ativo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo2.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavFuncaoapf_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
         cmbavFuncaoapf_tipo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo3.Visible), 5, 0)));
         cmbavFuncaoapf_complexidade3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade3.Visible), 5, 0)));
         cmbavFuncaoapf_ativo3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "0") == 0 )
         {
            edtavFuncaoapf_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "1") == 0 )
         {
            cmbavFuncaoapf_tipo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "2") == 0 )
         {
            cmbavFuncaoapf_complexidade3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "3") == 0 )
         {
            cmbavFuncaoapf_ativo3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo3.Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'ENABLEDYNAMICFILTERS4' Routine */
         edtavFuncaoapf_nome4_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome4_Visible), 5, 0)));
         cmbavFuncaoapf_tipo4.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo4.Visible), 5, 0)));
         cmbavFuncaoapf_complexidade4.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade4.Visible), 5, 0)));
         cmbavFuncaoapf_ativo4.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo4.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "0") == 0 )
         {
            edtavFuncaoapf_nome4_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavFuncaoapf_nome4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome4_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "1") == 0 )
         {
            cmbavFuncaoapf_tipo4.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo4.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "2") == 0 )
         {
            cmbavFuncaoapf_complexidade4.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_complexidade4.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "3") == 0 )
         {
            cmbavFuncaoapf_ativo4.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_ativo4.Visible), 5, 0)));
         }
      }

      protected void S152( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV17DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
         AV25DynamicFiltersSelector2 = "0";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector2", AV25DynamicFiltersSelector2);
         AV40FuncaoAPF_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40FuncaoAPF_Nome2", AV40FuncaoAPF_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S182 ();
         if (returnInSub) return;
         AV18DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled3", AV18DynamicFiltersEnabled3);
         AV26DynamicFiltersSelector3 = "0";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         AV41FuncaoAPF_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoAPF_Nome3", AV41FuncaoAPF_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S192 ();
         if (returnInSub) return;
         AV101DynamicFiltersEnabled4 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101DynamicFiltersEnabled4", AV101DynamicFiltersEnabled4);
         AV102DynamicFiltersSelector4 = "0";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102DynamicFiltersSelector4", AV102DynamicFiltersSelector4);
         AV109FuncaoAPF_Nome4 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109FuncaoAPF_Nome4", AV109FuncaoAPF_Nome4);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS4' */
         S202 ();
         if (returnInSub) return;
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV24DynamicFiltersSelector1 = "0";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector1", AV24DynamicFiltersSelector1);
         AV39FuncaoAPF_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39FuncaoAPF_Nome1", AV39FuncaoAPF_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S172 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if (returnInSub) return;
         AV50GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if (returnInSub) return;
      }

      protected void S112( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV65Session.Get(AV136Pgmname+"GridState"), "") == 0 )
         {
            AV50GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV136Pgmname+"GridState"), "");
         }
         else
         {
            AV50GridState.FromXml(AV65Session.Get(AV136Pgmname+"GridState"), "");
         }
         AV59OrderedBy = AV50GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59OrderedBy), 4, 0)));
         AV61OrderedDsc = AV50GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61OrderedDsc", AV61OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if (returnInSub) return;
      }

      protected void S162( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         imgAdddynamicfilters3_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters3_Visible), 5, 0)));
         imgRemovedynamicfilters3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters3_Visible), 5, 0)));
         if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(1));
            AV24DynamicFiltersSelector1 = AV51GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersSelector1", AV24DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "0") == 0 )
            {
               AV39FuncaoAPF_Nome1 = AV51GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39FuncaoAPF_Nome1", AV39FuncaoAPF_Nome1);
            }
            else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "1") == 0 )
            {
               AV123FuncaoAPF_Tipo1 = AV51GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV123FuncaoAPF_Tipo1", AV123FuncaoAPF_Tipo1);
            }
            else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "2") == 0 )
            {
               AV119FuncaoAPF_Complexidade1 = AV51GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV119FuncaoAPF_Complexidade1", AV119FuncaoAPF_Complexidade1);
            }
            else if ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "3") == 0 )
            {
               AV115FuncaoAPF_Ativo1 = AV51GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV115FuncaoAPF_Ativo1", AV115FuncaoAPF_Ativo1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S172 ();
            if (returnInSub) return;
            if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV17DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersEnabled2", AV17DynamicFiltersEnabled2);
               AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(2));
               AV25DynamicFiltersSelector2 = AV51GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersSelector2", AV25DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "0") == 0 )
               {
                  AV40FuncaoAPF_Nome2 = AV51GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40FuncaoAPF_Nome2", AV40FuncaoAPF_Nome2);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "1") == 0 )
               {
                  AV124FuncaoAPF_Tipo2 = AV51GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV124FuncaoAPF_Tipo2", AV124FuncaoAPF_Tipo2);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "2") == 0 )
               {
                  AV120FuncaoAPF_Complexidade2 = AV51GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV120FuncaoAPF_Complexidade2", AV120FuncaoAPF_Complexidade2);
               }
               else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "3") == 0 )
               {
                  AV116FuncaoAPF_Ativo2 = AV51GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV116FuncaoAPF_Ativo2", AV116FuncaoAPF_Ativo2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S182 ();
               if (returnInSub) return;
               if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV18DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled3", AV18DynamicFiltersEnabled3);
                  AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(3));
                  AV26DynamicFiltersSelector3 = AV51GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "0") == 0 )
                  {
                     AV41FuncaoAPF_Nome3 = AV51GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41FuncaoAPF_Nome3", AV41FuncaoAPF_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "1") == 0 )
                  {
                     AV125FuncaoAPF_Tipo3 = AV51GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV125FuncaoAPF_Tipo3", AV125FuncaoAPF_Tipo3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "2") == 0 )
                  {
                     AV121FuncaoAPF_Complexidade3 = AV51GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV121FuncaoAPF_Complexidade3", AV121FuncaoAPF_Complexidade3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "3") == 0 )
                  {
                     AV117FuncaoAPF_Ativo3 = AV51GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV117FuncaoAPF_Ativo3", AV117FuncaoAPF_Ativo3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S192 ();
                  if (returnInSub) return;
                  if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(4);";
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                     imgAdddynamicfilters3_Visible = 0;
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters3_Visible), 5, 0)));
                     imgRemovedynamicfilters3_Visible = 1;
                     context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters3_Visible), 5, 0)));
                     AV101DynamicFiltersEnabled4 = true;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV101DynamicFiltersEnabled4", AV101DynamicFiltersEnabled4);
                     AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(4));
                     AV102DynamicFiltersSelector4 = AV51GridStateDynamicFilter.gxTpr_Selected;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV102DynamicFiltersSelector4", AV102DynamicFiltersSelector4);
                     if ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "0") == 0 )
                     {
                        AV109FuncaoAPF_Nome4 = AV51GridStateDynamicFilter.gxTpr_Value;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV109FuncaoAPF_Nome4", AV109FuncaoAPF_Nome4);
                     }
                     else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "1") == 0 )
                     {
                        AV126FuncaoAPF_Tipo4 = AV51GridStateDynamicFilter.gxTpr_Value;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV126FuncaoAPF_Tipo4", AV126FuncaoAPF_Tipo4);
                     }
                     else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "2") == 0 )
                     {
                        AV122FuncaoAPF_Complexidade4 = AV51GridStateDynamicFilter.gxTpr_Value;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV122FuncaoAPF_Complexidade4", AV122FuncaoAPF_Complexidade4);
                     }
                     else if ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "3") == 0 )
                     {
                        AV118FuncaoAPF_Ativo4 = AV51GridStateDynamicFilter.gxTpr_Value;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV118FuncaoAPF_Ativo4", AV118FuncaoAPF_Ativo4);
                     }
                     /* Execute user subroutine: 'ENABLEDYNAMICFILTERS4' */
                     S202 ();
                     if (returnInSub) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV23DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S122( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV50GridState.FromXml(AV65Session.Get(AV136Pgmname+"GridState"), "");
         AV50GridState.gxTpr_Orderedby = AV59OrderedBy;
         AV50GridState.gxTpr_Ordereddsc = AV61OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if (returnInSub) return;
         new wwpbaseobjects.savegridstate(context ).execute(  AV136Pgmname+"GridState",  AV50GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S142( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV50GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV19DynamicFiltersIgnoreFirst )
         {
            AV51GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV51GridStateDynamicFilter.gxTpr_Selected = AV24DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "0") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV39FuncaoAPF_Nome1)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV39FuncaoAPF_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "1") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV123FuncaoAPF_Tipo1)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV123FuncaoAPF_Tipo1;
            }
            else if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "2") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV119FuncaoAPF_Complexidade1)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV119FuncaoAPF_Complexidade1;
            }
            else if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "3") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV115FuncaoAPF_Ativo1)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV115FuncaoAPF_Ativo1;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV51GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV50GridState.gxTpr_Dynamicfilters.Add(AV51GridStateDynamicFilter, 0);
            }
         }
         if ( AV17DynamicFiltersEnabled2 )
         {
            AV51GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV51GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "0") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV40FuncaoAPF_Nome2)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV40FuncaoAPF_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "1") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV124FuncaoAPF_Tipo2)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV124FuncaoAPF_Tipo2;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "2") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV120FuncaoAPF_Complexidade2)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV120FuncaoAPF_Complexidade2;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "3") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV116FuncaoAPF_Ativo2)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV116FuncaoAPF_Ativo2;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV51GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV50GridState.gxTpr_Dynamicfilters.Add(AV51GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled3 )
         {
            AV51GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV51GridStateDynamicFilter.gxTpr_Selected = AV26DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "0") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV41FuncaoAPF_Nome3)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV41FuncaoAPF_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "1") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV125FuncaoAPF_Tipo3)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV125FuncaoAPF_Tipo3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "2") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV121FuncaoAPF_Complexidade3)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV121FuncaoAPF_Complexidade3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "3") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV117FuncaoAPF_Ativo3)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV117FuncaoAPF_Ativo3;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV51GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV50GridState.gxTpr_Dynamicfilters.Add(AV51GridStateDynamicFilter, 0);
            }
         }
         if ( AV101DynamicFiltersEnabled4 )
         {
            AV51GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV51GridStateDynamicFilter.gxTpr_Selected = AV102DynamicFiltersSelector4;
            if ( ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "0") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV109FuncaoAPF_Nome4)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV109FuncaoAPF_Nome4;
            }
            else if ( ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "1") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV126FuncaoAPF_Tipo4)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV126FuncaoAPF_Tipo4;
            }
            else if ( ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "2") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV122FuncaoAPF_Complexidade4)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV122FuncaoAPF_Complexidade4;
            }
            else if ( ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "3") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV118FuncaoAPF_Ativo4)) )
            {
               AV51GridStateDynamicFilter.gxTpr_Value = AV118FuncaoAPF_Ativo4;
            }
            if ( AV23DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV51GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV50GridState.gxTpr_Dynamicfilters.Add(AV51GridStateDynamicFilter, 0);
            }
         }
      }

      protected void E32HA2( )
      {
         /* Selected_Click Routine */
         if ( AV64Selected )
         {
            if ( AV6Codigos.IndexOf(AV30FuncaoAPF_Codigo) == 0 )
            {
               AV6Codigos.Add(AV30FuncaoAPF_Codigo, 0);
            }
         }
         else
         {
            AV6Codigos.RemoveItem(AV6Codigos.IndexOf(AV30FuncaoAPF_Codigo));
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6Codigos", AV6Codigos);
      }

      protected void E19HA2( )
      {
         /* Selectall_Click Routine */
         if ( AV87SelectAll )
         {
            /* Start For Each Line */
            nRC_GXsfl_124 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_124"), ",", "."));
            nGXsfl_124_fel_idx = 0;
            while ( nGXsfl_124_fel_idx < nRC_GXsfl_124 )
            {
               nGXsfl_124_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_124_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_124_fel_idx+1));
               sGXsfl_124_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_124_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_1242( ) ;
               AV71Update = cgiGet( edtavUpdate_Internalname);
               AV15Delete = cgiGet( edtavDelete_Internalname);
               AV16Display = cgiGet( edtavDisplay_Internalname);
               AV64Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
               if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_CODIGO");
                  GX_FocusControl = edtavFuncaoapf_codigo_Internalname;
                  wbErr = true;
                  AV30FuncaoAPF_Codigo = 0;
               }
               else
               {
                  AV30FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", "."));
               }
               AV80Funcao_Nome = cgiGet( edtavFuncao_nome_Internalname);
               cmbavFuncaoapf_tipo.Name = cmbavFuncaoapf_tipo_Internalname;
               cmbavFuncaoapf_tipo.CurrentValue = cgiGet( cmbavFuncaoapf_tipo_Internalname);
               AV48FuncaoAPF_Tipo = cgiGet( cmbavFuncaoapf_tipo_Internalname);
               if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_TD");
                  GX_FocusControl = edtavFuncaoapf_td_Internalname;
                  wbErr = true;
                  AV47FuncaoAPF_TD = 0;
               }
               else
               {
                  AV47FuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", "."));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_AR");
                  GX_FocusControl = edtavFuncaoapf_ar_Internalname;
                  wbErr = true;
                  AV27FuncaoAPF_AR = 0;
               }
               else
               {
                  AV27FuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", "."));
               }
               cmbavFuncaoapf_complexidade.Name = cmbavFuncaoapf_complexidade_Internalname;
               cmbavFuncaoapf_complexidade.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade_Internalname);
               AV77FuncaoAPF_Complexidade = cgiGet( cmbavFuncaoapf_complexidade_Internalname);
               if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_PF");
                  GX_FocusControl = edtavFuncaoapf_pf_Internalname;
                  wbErr = true;
                  AV78FuncaoAPF_PF = 0;
               }
               else
               {
                  AV78FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".");
               }
               AV79FunAPFPaiNom = cgiGet( edtavFunapfpainom_Internalname);
               AV79FunAPFPaiNom = cgiGet( edtavFunapfpainom_Internalname);
               cmbavFuncaoapf_status.Name = cmbavFuncaoapf_status_Internalname;
               cmbavFuncaoapf_status.CurrentValue = cgiGet( cmbavFuncaoapf_status_Internalname);
               AV81FuncaoAPF_Status = cgiGet( cmbavFuncaoapf_status_Internalname);
               if ( ! AV64Selected )
               {
                  AV64Selected = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV64Selected);
                  if ( AV6Codigos.IndexOf(AV30FuncaoAPF_Codigo) == 0 )
                  {
                     AV6Codigos.Add(AV30FuncaoAPF_Codigo, 0);
                  }
               }
               /* End For Each Line */
            }
            if ( nGXsfl_124_fel_idx == 0 )
            {
               nGXsfl_124_idx = 1;
               sGXsfl_124_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_124_idx), 4, 0)), 4, "0");
               SubsflControlProps_1242( ) ;
            }
            nGXsfl_124_fel_idx = 1;
         }
         else
         {
            /* Start For Each Line */
            nRC_GXsfl_124 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_124"), ",", "."));
            nGXsfl_124_fel_idx = 0;
            while ( nGXsfl_124_fel_idx < nRC_GXsfl_124 )
            {
               nGXsfl_124_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_124_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_124_fel_idx+1));
               sGXsfl_124_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_124_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_1242( ) ;
               AV71Update = cgiGet( edtavUpdate_Internalname);
               AV15Delete = cgiGet( edtavDelete_Internalname);
               AV16Display = cgiGet( edtavDisplay_Internalname);
               AV64Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
               if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_CODIGO");
                  GX_FocusControl = edtavFuncaoapf_codigo_Internalname;
                  wbErr = true;
                  AV30FuncaoAPF_Codigo = 0;
               }
               else
               {
                  AV30FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", "."));
               }
               AV80Funcao_Nome = cgiGet( edtavFuncao_nome_Internalname);
               cmbavFuncaoapf_tipo.Name = cmbavFuncaoapf_tipo_Internalname;
               cmbavFuncaoapf_tipo.CurrentValue = cgiGet( cmbavFuncaoapf_tipo_Internalname);
               AV48FuncaoAPF_Tipo = cgiGet( cmbavFuncaoapf_tipo_Internalname);
               if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_TD");
                  GX_FocusControl = edtavFuncaoapf_td_Internalname;
                  wbErr = true;
                  AV47FuncaoAPF_TD = 0;
               }
               else
               {
                  AV47FuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", "."));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_AR");
                  GX_FocusControl = edtavFuncaoapf_ar_Internalname;
                  wbErr = true;
                  AV27FuncaoAPF_AR = 0;
               }
               else
               {
                  AV27FuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", "."));
               }
               cmbavFuncaoapf_complexidade.Name = cmbavFuncaoapf_complexidade_Internalname;
               cmbavFuncaoapf_complexidade.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade_Internalname);
               AV77FuncaoAPF_Complexidade = cgiGet( cmbavFuncaoapf_complexidade_Internalname);
               if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_PF");
                  GX_FocusControl = edtavFuncaoapf_pf_Internalname;
                  wbErr = true;
                  AV78FuncaoAPF_PF = 0;
               }
               else
               {
                  AV78FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".");
               }
               AV79FunAPFPaiNom = cgiGet( edtavFunapfpainom_Internalname);
               AV79FunAPFPaiNom = cgiGet( edtavFunapfpainom_Internalname);
               cmbavFuncaoapf_status.Name = cmbavFuncaoapf_status_Internalname;
               cmbavFuncaoapf_status.CurrentValue = cgiGet( cmbavFuncaoapf_status_Internalname);
               AV81FuncaoAPF_Status = cgiGet( cmbavFuncaoapf_status_Internalname);
               if ( AV64Selected )
               {
                  AV64Selected = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV64Selected);
                  AV6Codigos.RemoveItem(AV6Codigos.IndexOf(AV30FuncaoAPF_Codigo));
               }
               /* End For Each Line */
            }
            if ( nGXsfl_124_fel_idx == 0 )
            {
               nGXsfl_124_idx = 1;
               sGXsfl_124_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_124_idx), 4, 0)), 4, "0");
               SubsflControlProps_1242( ) ;
            }
            nGXsfl_124_fel_idx = 1;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6Codigos", AV6Codigos);
      }

      protected void E20HA2( )
      {
         /* Reverseall_Click Routine */
         /* Start For Each Line */
         nRC_GXsfl_124 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_124"), ",", "."));
         nGXsfl_124_fel_idx = 0;
         while ( nGXsfl_124_fel_idx < nRC_GXsfl_124 )
         {
            nGXsfl_124_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_124_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_124_fel_idx+1));
            sGXsfl_124_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_124_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_1242( ) ;
            AV71Update = cgiGet( edtavUpdate_Internalname);
            AV15Delete = cgiGet( edtavDelete_Internalname);
            AV16Display = cgiGet( edtavDisplay_Internalname);
            AV64Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_CODIGO");
               GX_FocusControl = edtavFuncaoapf_codigo_Internalname;
               wbErr = true;
               AV30FuncaoAPF_Codigo = 0;
            }
            else
            {
               AV30FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_codigo_Internalname), ",", "."));
            }
            AV80Funcao_Nome = cgiGet( edtavFuncao_nome_Internalname);
            cmbavFuncaoapf_tipo.Name = cmbavFuncaoapf_tipo_Internalname;
            cmbavFuncaoapf_tipo.CurrentValue = cgiGet( cmbavFuncaoapf_tipo_Internalname);
            AV48FuncaoAPF_Tipo = cgiGet( cmbavFuncaoapf_tipo_Internalname);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_TD");
               GX_FocusControl = edtavFuncaoapf_td_Internalname;
               wbErr = true;
               AV47FuncaoAPF_TD = 0;
            }
            else
            {
               AV47FuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_td_Internalname), ",", "."));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_AR");
               GX_FocusControl = edtavFuncaoapf_ar_Internalname;
               wbErr = true;
               AV27FuncaoAPF_AR = 0;
            }
            else
            {
               AV27FuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtavFuncaoapf_ar_Internalname), ",", "."));
            }
            cmbavFuncaoapf_complexidade.Name = cmbavFuncaoapf_complexidade_Internalname;
            cmbavFuncaoapf_complexidade.CurrentValue = cgiGet( cmbavFuncaoapf_complexidade_Internalname);
            AV77FuncaoAPF_Complexidade = cgiGet( cmbavFuncaoapf_complexidade_Internalname);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_PF");
               GX_FocusControl = edtavFuncaoapf_pf_Internalname;
               wbErr = true;
               AV78FuncaoAPF_PF = 0;
            }
            else
            {
               AV78FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf_Internalname), ",", ".");
            }
            AV79FunAPFPaiNom = cgiGet( edtavFunapfpainom_Internalname);
            AV79FunAPFPaiNom = cgiGet( edtavFunapfpainom_Internalname);
            cmbavFuncaoapf_status.Name = cmbavFuncaoapf_status_Internalname;
            cmbavFuncaoapf_status.CurrentValue = cgiGet( cmbavFuncaoapf_status_Internalname);
            AV81FuncaoAPF_Status = cgiGet( cmbavFuncaoapf_status_Internalname);
            AV64Selected = (bool)(!AV64Selected);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV64Selected);
            if ( AV64Selected && ( AV6Codigos.IndexOf(AV30FuncaoAPF_Codigo) == 0 ) )
            {
               AV6Codigos.Add(AV30FuncaoAPF_Codigo, 0);
            }
            else
            {
               AV6Codigos.RemoveItem(AV6Codigos.IndexOf(AV30FuncaoAPF_Codigo));
            }
            /* End For Each Line */
         }
         if ( nGXsfl_124_fel_idx == 0 )
         {
            nGXsfl_124_idx = 1;
            sGXsfl_124_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_124_idx), 4, 0)), 4, "0");
            SubsflControlProps_1242( ) ;
         }
         nGXsfl_124_fel_idx = 1;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6Codigos", AV6Codigos);
      }

      protected void E28HA2( )
      {
         /* Sorepetidas_Click Routine */
         gxgrGrid_refresh( subGrid_Rows, AV127Rows, AV59OrderedBy, AV61OrderedDsc, AV136Pgmname, AV50GridState, AV19DynamicFiltersIgnoreFirst, AV24DynamicFiltersSelector1, AV39FuncaoAPF_Nome1, AV123FuncaoAPF_Tipo1, AV119FuncaoAPF_Complexidade1, AV115FuncaoAPF_Ativo1, AV23DynamicFiltersRemoving, AV17DynamicFiltersEnabled2, AV25DynamicFiltersSelector2, AV40FuncaoAPF_Nome2, AV124FuncaoAPF_Tipo2, AV120FuncaoAPF_Complexidade2, AV116FuncaoAPF_Ativo2, AV18DynamicFiltersEnabled3, AV26DynamicFiltersSelector3, AV41FuncaoAPF_Nome3, AV125FuncaoAPF_Tipo3, AV121FuncaoAPF_Complexidade3, AV117FuncaoAPF_Ativo3, AV101DynamicFiltersEnabled4, AV102DynamicFiltersSelector4, AV109FuncaoAPF_Nome4, AV126FuncaoAPF_Tipo4, AV122FuncaoAPF_Complexidade4, AV118FuncaoAPF_Ativo4, AV66SoRepetidas, A166FuncaoAPF_Nome, A184FuncaoAPF_Tipo, A183FuncaoAPF_Ativo, A185FuncaoAPF_Complexidade, A387FuncaoAPF_AR, A388FuncaoAPF_TD, A165FuncaoAPF_Codigo, A363FuncaoAPF_FunAPFPaiNom, AV42FuncaoAPF_SistemaCod, A358FuncaoAPF_FunAPFPaiCod, A386FuncaoAPF_PF, AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV27FuncaoAPF_AR, AV47FuncaoAPF_TD, AV89FuncaoAPF_Ativo, AV85Processadas, AV6Codigos) ;
      }

      protected void S133( )
      {
         /* 'NOMESREPETIDOS' Routine */
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A165FuncaoAPF_Codigo ,
                                              AV85Processadas ,
                                              A360FuncaoAPF_SistemaCod ,
                                              A387FuncaoAPF_AR ,
                                              AV27FuncaoAPF_AR ,
                                              A388FuncaoAPF_TD ,
                                              AV47FuncaoAPF_TD ,
                                              AV38FuncaoAPF_Nome ,
                                              AV48FuncaoAPF_Tipo ,
                                              AV89FuncaoAPF_Ativo ,
                                              A166FuncaoAPF_Nome ,
                                              A184FuncaoAPF_Tipo ,
                                              A183FuncaoAPF_Ativo },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING
                                              }
         });
         /* Using cursor H00HA4 */
         pr_default.execute(2, new Object[] {AV38FuncaoAPF_Nome, AV48FuncaoAPF_Tipo, AV89FuncaoAPF_Ativo, n360FuncaoAPF_SistemaCod, A360FuncaoAPF_SistemaCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A166FuncaoAPF_Nome = H00HA4_A166FuncaoAPF_Nome[0];
            A184FuncaoAPF_Tipo = H00HA4_A184FuncaoAPF_Tipo[0];
            A183FuncaoAPF_Ativo = H00HA4_A183FuncaoAPF_Ativo[0];
            A165FuncaoAPF_Codigo = H00HA4_A165FuncaoAPF_Codigo[0];
            GXt_int1 = A388FuncaoAPF_TD;
            new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            A388FuncaoAPF_TD = GXt_int1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A388FuncaoAPF_TD", StringUtil.LTrim( StringUtil.Str( (decimal)(A388FuncaoAPF_TD), 4, 0)));
            if ( A388FuncaoAPF_TD == AV47FuncaoAPF_TD )
            {
               GXt_int1 = A387FuncaoAPF_AR;
               new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               A387FuncaoAPF_AR = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A387FuncaoAPF_AR", StringUtil.LTrim( StringUtil.Str( (decimal)(A387FuncaoAPF_AR), 4, 0)));
               if ( A387FuncaoAPF_AR == AV27FuncaoAPF_AR )
               {
                  AV6Codigos.Add(A165FuncaoAPF_Codigo, 0);
                  AV85Processadas.Add(A165FuncaoAPF_Codigo, 0);
               }
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void wb_table1_2_HA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_HA2( true) ;
         }
         else
         {
            wb_table2_8_HA2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_HA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_121_HA2( true) ;
         }
         else
         {
            wb_table3_121_HA2( false) ;
         }
         return  ;
      }

      protected void wb_table3_121_HA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\">") ;
            wb_table4_141_HA2( true) ;
         }
         else
         {
            wb_table4_141_HA2( false) ;
         }
         return  ;
      }

      protected void wb_table4_141_HA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_HA2e( true) ;
         }
         else
         {
            wb_table1_2_HA2e( false) ;
         }
      }

      protected void wb_table4_141_HA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTbltotais_Internalname, tblTbltotais_Internalname, "", "Table", 0, "", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_pflfstotal_Internalname, "Quantidade: ", "", "", lblTextblockcontagemresultado_pflfstotal_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'" + sGXsfl_124_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavQtde_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84Qtde), 4, 0, ",", "")), ((edtavQtde_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV84Qtde), "ZZZ9")) : context.localUtil.Format( (decimal)(AV84Qtde), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,146);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavQtde_Jsonclick, 0, "BootstrapAttribute", "font-family:'Trebuchet MS'; font-size:10.0pt; font-weight:bold; font-style:normal;", "", "", 1, edtavQtde_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_141_HA2e( true) ;
         }
         else
         {
            wb_table4_141_HA2e( false) ;
         }
      }

      protected void wb_table3_121_HA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"124\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o de Transa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtavFuncao_nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtavFuncao_nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtavFuncao_nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbavFuncaoapf_tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbavFuncaoapf_tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbavFuncaoapf_tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "DER") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "ALR") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Complexidade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               if ( edtavFunapfpainom_Titleformat == 0 )
               {
                  context.SendWebValue( edtavFunapfpainom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtavFunapfpainom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               if ( edtavFunapfpainom_Titleformat == 0 )
               {
                  context.SendWebValue( edtavFunapfpainom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtavFunapfpainom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbavFuncaoapf_status_Titleformat == 0 )
               {
                  context.SendWebValue( cmbavFuncaoapf_status.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbavFuncaoapf_status.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV71Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV16Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV64Selected));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavSelected.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncaoapf_codigo_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV80Funcao_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavFuncao_nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncao_nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncao_nome_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavFuncao_nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV48FuncaoAPF_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbavFuncaoapf_tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavFuncaoapf_tipo_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavFuncaoapf_tipo.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47FuncaoAPF_TD), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncaoapf_td_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27FuncaoAPF_AR), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncaoapf_ar_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV77FuncaoAPF_Complexidade));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavFuncaoapf_complexidade.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( AV78FuncaoAPF_PF, 14, 5, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFuncaoapf_pf_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV79FunAPFPaiNom);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavFunapfpainom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFunapfpainom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFunapfpainom_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavFunapfpainom_Link));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavFunapfpainom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFunapfpainom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFunapfpainom_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavFunapfpainom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV79FunAPFPaiNom);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavFunapfpainom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFunapfpainom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFunapfpainom_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavFunapfpainom_Link));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavFunapfpainom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFunapfpainom_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavFunapfpainom_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavFunapfpainom_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV81FuncaoAPF_Status));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbavFuncaoapf_status.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavFuncaoapf_status_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavFuncaoapf_status.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 124 )
         {
            wbEnd = 0;
            nRC_GXsfl_124 = (short)(nGXsfl_124_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_121_HA2e( true) ;
         }
         else
         {
            wb_table3_121_HA2e( false) ;
         }
      }

      protected void wb_table2_8_HA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFuncaoapftitle_Internalname, lblFuncaoapftitle_Caption, "", "", lblFuncaoapftitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenar por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV59OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV59OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'" + sGXsfl_124_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV61OrderedDsc), StringUtil.BoolToStr( AV61OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_19_HA2( true) ;
         }
         else
         {
            wb_table5_19_HA2( false) ;
         }
         return  ;
      }

      protected void wb_table5_19_HA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:1px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:1px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"20\" >") ;
            wb_table6_104_HA2( true) ;
         }
         else
         {
            wb_table6_104_HA2( false) ;
         }
         return  ;
      }

      protected void wb_table6_104_HA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_HA2e( true) ;
         }
         else
         {
            wb_table2_8_HA2e( false) ;
         }
      }

      protected void wb_table6_104_HA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocksorepetidas_Internalname, "Mostrar", "", "", lblTextblocksorepetidas_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavSorepetidas, cmbavSorepetidas_Internalname, StringUtil.BoolToStr( AV66SoRepetidas), 1, cmbavSorepetidas_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVSOREPETIDAS.CLICK."+"'", "boolean", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavSorepetidas.CurrentValue = StringUtil.BoolToStr( AV66SoRepetidas);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavSorepetidas_Internalname, "Values", (String)(cmbavSorepetidas.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table7_111_HA2( true) ;
         }
         else
         {
            wb_table7_111_HA2( false) ;
         }
         return  ;
      }

      protected void wb_table7_111_HA2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_104_HA2e( true) ;
         }
         else
         {
            wb_table6_104_HA2e( false) ;
         }
      }

      protected void wb_table7_111_HA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblsorepetidas_Internalname, tblTblsorepetidas_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtnapagarselecionadas_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Apagar todas as Fun��es selecionadas", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtnapagarselecionadas_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOAPAGARSELECIONADAS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_124_idx + "',0)\"";
            ClassString = "AttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavSelectall_Internalname, StringUtil.BoolToStr( AV87SelectAll), "", "", 1, 1, "true", "�Todas seleccionadas�", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(116, this, 'true', 'false');gx.ajax.executeCliEvent('e19ha2_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"");
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_124_idx + "',0)\"";
            ClassString = "AttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavReverseall_Internalname, StringUtil.BoolToStr( AV90ReverseAll), "", "", 1, 1, "true", "�Inverter sele��o�", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(117, this, 'true', 'false');gx.ajax.executeCliEvent('e20ha2_client',this, event);gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"");
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_124_idx + "',0)\"";
            ClassString = "AttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavEmcascata_Internalname, StringUtil.BoolToStr( AV91EmCascata), "", "", 1, 1, "true", "�Apagar em cascata�", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(118, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_111_HA2e( true) ;
         }
         else
         {
            wb_table7_111_HA2e( false) ;
         }
      }

      protected void wb_table5_19_HA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_24_HA2( true) ;
         }
         else
         {
            wb_table8_24_HA2( false) ;
         }
         return  ;
      }

      protected void wb_table8_24_HA2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnatualizar_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(124), 3, 0)+","+"null"+");", "Procurar", bttBtnatualizar_Jsonclick, 5, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOATUALIZAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_19_HA2e( true) ;
         }
         else
         {
            wb_table5_19_HA2e( false) ;
         }
      }

      protected void wb_table8_24_HA2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Procurar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV24DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV24DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_124_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_nome1_Internalname, AV39FuncaoAPF_Nome1, StringUtil.RTrim( context.localUtil.Format( AV39FuncaoAPF_Nome1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_nome1_Visible, 1, 0, "text", "", 200, "px", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_FuncoesTRN.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo1, cmbavFuncaoapf_tipo1_Internalname, StringUtil.RTrim( AV123FuncaoAPF_Tipo1), 1, cmbavFuncaoapf_tipo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV123FuncaoAPF_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo1_Internalname, "Values", (String)(cmbavFuncaoapf_tipo1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_complexidade1, cmbavFuncaoapf_complexidade1_Internalname, StringUtil.RTrim( AV119FuncaoAPF_Complexidade1), 1, cmbavFuncaoapf_complexidade1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_complexidade1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_complexidade1.CurrentValue = StringUtil.RTrim( AV119FuncaoAPF_Complexidade1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade1_Internalname, "Values", (String)(cmbavFuncaoapf_complexidade1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_ativo1, cmbavFuncaoapf_ativo1_Internalname, StringUtil.RTrim( AV115FuncaoAPF_Ativo1), 1, cmbavFuncaoapf_ativo1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_ativo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_ativo1.CurrentValue = StringUtil.RTrim( AV115FuncaoAPF_Ativo1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo1_Internalname, "Values", (String)(cmbavFuncaoapf_ativo1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Add filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_FuncoesTRN.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remove filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Procurar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_124_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_nome2_Internalname, AV40FuncaoAPF_Nome2, StringUtil.RTrim( context.localUtil.Format( AV40FuncaoAPF_Nome2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_nome2_Visible, 1, 0, "text", "", 200, "px", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_FuncoesTRN.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo2, cmbavFuncaoapf_tipo2_Internalname, StringUtil.RTrim( AV124FuncaoAPF_Tipo2), 1, cmbavFuncaoapf_tipo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV124FuncaoAPF_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo2_Internalname, "Values", (String)(cmbavFuncaoapf_tipo2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_complexidade2, cmbavFuncaoapf_complexidade2_Internalname, StringUtil.RTrim( AV120FuncaoAPF_Complexidade2), 1, cmbavFuncaoapf_complexidade2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_complexidade2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_complexidade2.CurrentValue = StringUtil.RTrim( AV120FuncaoAPF_Complexidade2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade2_Internalname, "Values", (String)(cmbavFuncaoapf_complexidade2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_ativo2, cmbavFuncaoapf_ativo2_Internalname, StringUtil.RTrim( AV116FuncaoAPF_Ativo2), 1, cmbavFuncaoapf_ativo2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_ativo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_ativo2.CurrentValue = StringUtil.RTrim( AV116FuncaoAPF_Ativo2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo2_Internalname, "Values", (String)(cmbavFuncaoapf_ativo2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Add filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_FuncoesTRN.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remove filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Procurar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV26DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'" + sGXsfl_124_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_nome3_Internalname, AV41FuncaoAPF_Nome3, StringUtil.RTrim( context.localUtil.Format( AV41FuncaoAPF_Nome3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_nome3_Visible, 1, 0, "text", "", 200, "px", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_FuncoesTRN.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo3, cmbavFuncaoapf_tipo3_Internalname, StringUtil.RTrim( AV125FuncaoAPF_Tipo3), 1, cmbavFuncaoapf_tipo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_tipo3.CurrentValue = StringUtil.RTrim( AV125FuncaoAPF_Tipo3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo3_Internalname, "Values", (String)(cmbavFuncaoapf_tipo3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_complexidade3, cmbavFuncaoapf_complexidade3_Internalname, StringUtil.RTrim( AV121FuncaoAPF_Complexidade3), 1, cmbavFuncaoapf_complexidade3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_complexidade3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_complexidade3.CurrentValue = StringUtil.RTrim( AV121FuncaoAPF_Complexidade3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade3_Internalname, "Values", (String)(cmbavFuncaoapf_complexidade3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_ativo3, cmbavFuncaoapf_ativo3_Internalname, StringUtil.RTrim( AV117FuncaoAPF_Ativo3), 1, cmbavFuncaoapf_ativo3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_ativo3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_ativo3.CurrentValue = StringUtil.RTrim( AV117FuncaoAPF_Ativo3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo3_Internalname, "Values", (String)(cmbavFuncaoapf_ativo3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters3_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters3_Visible, 1, "", "Add filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_FuncoesTRN.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters3_Visible, 1, "", "Remove filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow4\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix4_Internalname, "Procurar por", "", "", lblDynamicfiltersprefix4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector4, cmbavDynamicfiltersselector4_Internalname, StringUtil.RTrim( AV102DynamicFiltersSelector4), 1, cmbavDynamicfiltersselector4_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR4.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavDynamicfiltersselector4.CurrentValue = StringUtil.RTrim( AV102DynamicFiltersSelector4);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector4_Internalname, "Values", (String)(cmbavDynamicfiltersselector4.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle4_Internalname, "valor", "", "", lblDynamicfiltersmiddle4_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_124_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_nome4_Internalname, AV109FuncaoAPF_Nome4, StringUtil.RTrim( context.localUtil.Format( AV109FuncaoAPF_Nome4, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_nome4_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_nome4_Visible, 1, 0, "text", "", 200, "px", 1, "row", 200, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_FuncoesTRN.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo4, cmbavFuncaoapf_tipo4_Internalname, StringUtil.RTrim( AV126FuncaoAPF_Tipo4), 1, cmbavFuncaoapf_tipo4_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo4.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_tipo4.CurrentValue = StringUtil.RTrim( AV126FuncaoAPF_Tipo4);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo4_Internalname, "Values", (String)(cmbavFuncaoapf_tipo4.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_complexidade4, cmbavFuncaoapf_complexidade4_Internalname, StringUtil.RTrim( AV122FuncaoAPF_Complexidade4), 1, cmbavFuncaoapf_complexidade4_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_complexidade4.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_complexidade4.CurrentValue = StringUtil.RTrim( AV122FuncaoAPF_Complexidade4);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade4_Internalname, "Values", (String)(cmbavFuncaoapf_complexidade4.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_124_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_ativo4, cmbavFuncaoapf_ativo4_Internalname, StringUtil.RTrim( AV118FuncaoAPF_Ativo4), 1, cmbavFuncaoapf_ativo4_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_ativo4.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", "", true, "HLP_WP_FuncoesTRN.htm");
            cmbavFuncaoapf_ativo4.CurrentValue = StringUtil.RTrim( AV118FuncaoAPF_Ativo4);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_ativo4_Internalname, "Values", (String)(cmbavFuncaoapf_ativo4.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters4_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remove filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters4_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS4\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_FuncoesTRN.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_24_HA2e( true) ;
         }
         else
         {
            wb_table8_24_HA2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A360FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,0));
         n360FuncaoAPF_SistemaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A360FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0)));
         AV128Sistema_Sigla = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV128Sistema_Sigla", AV128Sistema_Sigla);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAHA2( ) ;
         WSHA2( ) ;
         WEHA2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118555878");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wp_funcoestrn.js", "?20203118555878");
            context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1242( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_124_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_124_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_124_idx;
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_124_idx;
         edtavFuncaoapf_codigo_Internalname = "vFUNCAOAPF_CODIGO_"+sGXsfl_124_idx;
         edtavFuncao_nome_Internalname = "vFUNCAO_NOME_"+sGXsfl_124_idx;
         cmbavFuncaoapf_tipo_Internalname = "vFUNCAOAPF_TIPO_"+sGXsfl_124_idx;
         edtavFuncaoapf_td_Internalname = "vFUNCAOAPF_TD_"+sGXsfl_124_idx;
         edtavFuncaoapf_ar_Internalname = "vFUNCAOAPF_AR_"+sGXsfl_124_idx;
         cmbavFuncaoapf_complexidade_Internalname = "vFUNCAOAPF_COMPLEXIDADE_"+sGXsfl_124_idx;
         edtavFuncaoapf_pf_Internalname = "vFUNCAOAPF_PF_"+sGXsfl_124_idx;
         edtavFunapfpainom_Internalname = "vFUNAPFPAINOM_"+sGXsfl_124_idx;
         edtavFunapfpainom_Internalname = "vFUNAPFPAINOM_"+sGXsfl_124_idx;
         cmbavFuncaoapf_status_Internalname = "vFUNCAOAPF_STATUS_"+sGXsfl_124_idx;
      }

      protected void SubsflControlProps_fel_1242( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_124_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_124_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_124_fel_idx;
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_124_fel_idx;
         edtavFuncaoapf_codigo_Internalname = "vFUNCAOAPF_CODIGO_"+sGXsfl_124_fel_idx;
         edtavFuncao_nome_Internalname = "vFUNCAO_NOME_"+sGXsfl_124_fel_idx;
         cmbavFuncaoapf_tipo_Internalname = "vFUNCAOAPF_TIPO_"+sGXsfl_124_fel_idx;
         edtavFuncaoapf_td_Internalname = "vFUNCAOAPF_TD_"+sGXsfl_124_fel_idx;
         edtavFuncaoapf_ar_Internalname = "vFUNCAOAPF_AR_"+sGXsfl_124_fel_idx;
         cmbavFuncaoapf_complexidade_Internalname = "vFUNCAOAPF_COMPLEXIDADE_"+sGXsfl_124_fel_idx;
         edtavFuncaoapf_pf_Internalname = "vFUNCAOAPF_PF_"+sGXsfl_124_fel_idx;
         edtavFunapfpainom_Internalname = "vFUNAPFPAINOM_"+sGXsfl_124_fel_idx;
         edtavFunapfpainom_Internalname = "vFUNAPFPAINOM_"+sGXsfl_124_fel_idx;
         cmbavFuncaoapf_status_Internalname = "vFUNCAOAPF_STATUS_"+sGXsfl_124_fel_idx;
      }

      protected void sendrow_1242( )
      {
         SubsflControlProps_1242( ) ;
         WBHA0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_124_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_124_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_124_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV71Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV71Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV133Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV71Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV71Update)) ? AV133Update_GXI : context.PathToRelativeUrl( AV71Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV71Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV134Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)) ? AV134Delete_GXI : context.PathToRelativeUrl( AV15Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV16Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV16Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV135Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV16Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV16Display)) ? AV135Display_GXI : context.PathToRelativeUrl( AV16Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV16Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 128,'',false,'"+sGXsfl_124_idx+"',124)\"" : " ");
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavSelected_Internalname,StringUtil.BoolToStr( AV64Selected),(String)"",(String)"",(short)-1,chkavSelected.Enabled,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(128, this, 'true', 'false');gx.ajax.executeCliEvent('e32ha2_client',this, event);gx.evt.onchange(this);\" " : " ")+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,128);\"" : " ")});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavFuncaoapf_codigo_Enabled!=0)&&(edtavFuncaoapf_codigo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 129,'',false,'"+sGXsfl_124_idx+"',124)\"" : " ");
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncaoapf_codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30FuncaoAPF_Codigo), 6, 0, ",", "")),((edtavFuncaoapf_codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV30FuncaoAPF_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV30FuncaoAPF_Codigo), "ZZZZZ9")),TempTags+((edtavFuncaoapf_codigo_Enabled!=0)&&(edtavFuncaoapf_codigo_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFuncaoapf_codigo_Enabled!=0)&&(edtavFuncaoapf_codigo_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,129);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFuncaoapf_codigo_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavFuncaoapf_codigo_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)124,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavFuncao_nome_Enabled!=0)&&(edtavFuncao_nome_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 130,'',false,'"+sGXsfl_124_idx+"',124)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncao_nome_Internalname,(String)AV80Funcao_Nome,(String)"",TempTags+((edtavFuncao_nome_Enabled!=0)&&(edtavFuncao_nome_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFuncao_nome_Enabled!=0)&&(edtavFuncao_nome_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,130);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtavFuncao_nome_Link,(String)"",(String)"",(String)"",(String)edtavFuncao_nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavFuncao_nome_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)124,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            TempTags = " " + ((cmbavFuncaoapf_tipo.Enabled!=0)&&(cmbavFuncaoapf_tipo.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 131,'',false,'"+sGXsfl_124_idx+"',124)\"" : " ");
            if ( ( nGXsfl_124_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vFUNCAOAPF_TIPO_" + sGXsfl_124_idx;
               cmbavFuncaoapf_tipo.Name = GXCCtl;
               cmbavFuncaoapf_tipo.WebTags = "";
               cmbavFuncaoapf_tipo.addItem("", "(Nenhum)", 0);
               cmbavFuncaoapf_tipo.addItem("EE", "EE", 0);
               cmbavFuncaoapf_tipo.addItem("SE", "SE", 0);
               cmbavFuncaoapf_tipo.addItem("CE", "CE", 0);
               cmbavFuncaoapf_tipo.addItem("NM", "NM", 0);
               if ( cmbavFuncaoapf_tipo.ItemCount > 0 )
               {
                  AV48FuncaoAPF_Tipo = cmbavFuncaoapf_tipo.getValidValue(AV48FuncaoAPF_Tipo);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_tipo_Internalname, AV48FuncaoAPF_Tipo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavFuncaoapf_tipo,(String)cmbavFuncaoapf_tipo_Internalname,StringUtil.RTrim( AV48FuncaoAPF_Tipo),(short)1,(String)cmbavFuncaoapf_tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavFuncaoapf_tipo.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",TempTags+((cmbavFuncaoapf_tipo.Enabled!=0)&&(cmbavFuncaoapf_tipo.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavFuncaoapf_tipo.Enabled!=0)&&(cmbavFuncaoapf_tipo.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,131);\"" : " "),(String)"",(bool)true});
            cmbavFuncaoapf_tipo.CurrentValue = StringUtil.RTrim( AV48FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_tipo_Internalname, "Values", (String)(cmbavFuncaoapf_tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavFuncaoapf_td_Enabled!=0)&&(edtavFuncaoapf_td_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 132,'',false,'"+sGXsfl_124_idx+"',124)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncaoapf_td_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV47FuncaoAPF_TD), 4, 0, ",", "")),((edtavFuncaoapf_td_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV47FuncaoAPF_TD), "ZZZ9")) : context.localUtil.Format( (decimal)(AV47FuncaoAPF_TD), "ZZZ9")),TempTags+((edtavFuncaoapf_td_Enabled!=0)&&(edtavFuncaoapf_td_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFuncaoapf_td_Enabled!=0)&&(edtavFuncaoapf_td_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,132);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFuncaoapf_td_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavFuncaoapf_td_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)124,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavFuncaoapf_ar_Enabled!=0)&&(edtavFuncaoapf_ar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 133,'',false,'"+sGXsfl_124_idx+"',124)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncaoapf_ar_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27FuncaoAPF_AR), 4, 0, ",", "")),((edtavFuncaoapf_ar_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV27FuncaoAPF_AR), "ZZZ9")) : context.localUtil.Format( (decimal)(AV27FuncaoAPF_AR), "ZZZ9")),TempTags+((edtavFuncaoapf_ar_Enabled!=0)&&(edtavFuncaoapf_ar_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFuncaoapf_ar_Enabled!=0)&&(edtavFuncaoapf_ar_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,133);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFuncaoapf_ar_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavFuncaoapf_ar_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)124,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            TempTags = " " + ((cmbavFuncaoapf_complexidade.Enabled!=0)&&(cmbavFuncaoapf_complexidade.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 134,'',false,'"+sGXsfl_124_idx+"',124)\"" : " ");
            if ( ( nGXsfl_124_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vFUNCAOAPF_COMPLEXIDADE_" + sGXsfl_124_idx;
               cmbavFuncaoapf_complexidade.Name = GXCCtl;
               cmbavFuncaoapf_complexidade.WebTags = "";
               cmbavFuncaoapf_complexidade.addItem("E", "", 0);
               cmbavFuncaoapf_complexidade.addItem("B", "Baixa", 0);
               cmbavFuncaoapf_complexidade.addItem("M", "M�dia", 0);
               cmbavFuncaoapf_complexidade.addItem("A", "Alta", 0);
               if ( cmbavFuncaoapf_complexidade.ItemCount > 0 )
               {
                  AV77FuncaoAPF_Complexidade = cmbavFuncaoapf_complexidade.getValidValue(AV77FuncaoAPF_Complexidade);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_complexidade_Internalname, AV77FuncaoAPF_Complexidade);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavFuncaoapf_complexidade,(String)cmbavFuncaoapf_complexidade_Internalname,StringUtil.RTrim( AV77FuncaoAPF_Complexidade),(short)1,(String)cmbavFuncaoapf_complexidade_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavFuncaoapf_complexidade.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",TempTags+((cmbavFuncaoapf_complexidade.Enabled!=0)&&(cmbavFuncaoapf_complexidade.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavFuncaoapf_complexidade.Enabled!=0)&&(cmbavFuncaoapf_complexidade.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,134);\"" : " "),(String)"",(bool)true});
            cmbavFuncaoapf_complexidade.CurrentValue = StringUtil.RTrim( AV77FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_complexidade_Internalname, "Values", (String)(cmbavFuncaoapf_complexidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavFuncaoapf_pf_Enabled!=0)&&(edtavFuncaoapf_pf_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 135,'',false,'"+sGXsfl_124_idx+"',124)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncaoapf_pf_Internalname,StringUtil.LTrim( StringUtil.NToC( AV78FuncaoAPF_PF, 14, 5, ",", "")),((edtavFuncaoapf_pf_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV78FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( AV78FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")),TempTags+((edtavFuncaoapf_pf_Enabled!=0)&&(edtavFuncaoapf_pf_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFuncaoapf_pf_Enabled!=0)&&(edtavFuncaoapf_pf_Visible!=0) ? " onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,135);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavFuncaoapf_pf_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavFuncaoapf_pf_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)124,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavFunapfpainom_Enabled!=0)&&(edtavFunapfpainom_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 136,'',false,'"+sGXsfl_124_idx+"',124)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFunapfpainom_Internalname,(String)AV79FunAPFPaiNom,(String)"",TempTags+((edtavFunapfpainom_Enabled!=0)&&(edtavFunapfpainom_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFunapfpainom_Enabled!=0)&&(edtavFunapfpainom_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,136);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtavFunapfpainom_Link,(String)"",(String)"",(String)"",(String)edtavFunapfpainom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavFunapfpainom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)124,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavFunapfpainom_Enabled!=0)&&(edtavFunapfpainom_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 136,'',false,'"+sGXsfl_124_idx+"',124)\"" : " ");
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavFunapfpainom_Internalname,(String)AV79FunAPFPaiNom,(String)"",TempTags+((edtavFunapfpainom_Enabled!=0)&&(edtavFunapfpainom_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavFunapfpainom_Enabled!=0)&&(edtavFunapfpainom_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,136);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtavFunapfpainom_Link,(String)"",(String)"",(String)"",(String)edtavFunapfpainom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavFunapfpainom_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)124,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            TempTags = " " + ((cmbavFuncaoapf_status.Enabled!=0)&&(cmbavFuncaoapf_status.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 138,'',false,'"+sGXsfl_124_idx+"',124)\"" : " ");
            if ( ( nGXsfl_124_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vFUNCAOAPF_STATUS_" + sGXsfl_124_idx;
               cmbavFuncaoapf_status.Name = GXCCtl;
               cmbavFuncaoapf_status.WebTags = "";
               cmbavFuncaoapf_status.addItem("A", "Ativa", 0);
               cmbavFuncaoapf_status.addItem("E", "Exclu�da", 0);
               cmbavFuncaoapf_status.addItem("R", "Rejeitada", 0);
               if ( cmbavFuncaoapf_status.ItemCount > 0 )
               {
                  AV81FuncaoAPF_Status = cmbavFuncaoapf_status.getValidValue(AV81FuncaoAPF_Status);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavFuncaoapf_status_Internalname, AV81FuncaoAPF_Status);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavFuncaoapf_status,(String)cmbavFuncaoapf_status_Internalname,StringUtil.RTrim( AV81FuncaoAPF_Status),(short)1,(String)cmbavFuncaoapf_status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavFuncaoapf_status.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",TempTags+((cmbavFuncaoapf_status.Enabled!=0)&&(cmbavFuncaoapf_status.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavFuncaoapf_status.Enabled!=0)&&(cmbavFuncaoapf_status.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,138);\"" : " "),(String)"",(bool)true});
            cmbavFuncaoapf_status.CurrentValue = StringUtil.RTrim( AV81FuncaoAPF_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFuncaoapf_status_Internalname, "Values", (String)(cmbavFuncaoapf_status.ToJavascriptSource()));
            GridContainer.AddRow(GridRow);
            nGXsfl_124_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_124_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_124_idx+1));
            sGXsfl_124_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_124_idx), 4, 0)), 4, "0");
            SubsflControlProps_1242( ) ;
         }
         /* End function sendrow_1242 */
      }

      protected void init_default_properties( )
      {
         lblFuncaoapftitle_Internalname = "FUNCAOAPFTITLE";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavFuncaoapf_nome1_Internalname = "vFUNCAOAPF_NOME1";
         cmbavFuncaoapf_tipo1_Internalname = "vFUNCAOAPF_TIPO1";
         cmbavFuncaoapf_complexidade1_Internalname = "vFUNCAOAPF_COMPLEXIDADE1";
         cmbavFuncaoapf_ativo1_Internalname = "vFUNCAOAPF_ATIVO1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavFuncaoapf_nome2_Internalname = "vFUNCAOAPF_NOME2";
         cmbavFuncaoapf_tipo2_Internalname = "vFUNCAOAPF_TIPO2";
         cmbavFuncaoapf_complexidade2_Internalname = "vFUNCAOAPF_COMPLEXIDADE2";
         cmbavFuncaoapf_ativo2_Internalname = "vFUNCAOAPF_ATIVO2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavFuncaoapf_nome3_Internalname = "vFUNCAOAPF_NOME3";
         cmbavFuncaoapf_tipo3_Internalname = "vFUNCAOAPF_TIPO3";
         cmbavFuncaoapf_complexidade3_Internalname = "vFUNCAOAPF_COMPLEXIDADE3";
         cmbavFuncaoapf_ativo3_Internalname = "vFUNCAOAPF_ATIVO3";
         imgAdddynamicfilters3_Internalname = "ADDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         lblDynamicfiltersprefix4_Internalname = "DYNAMICFILTERSPREFIX4";
         cmbavDynamicfiltersselector4_Internalname = "vDYNAMICFILTERSSELECTOR4";
         lblDynamicfiltersmiddle4_Internalname = "DYNAMICFILTERSMIDDLE4";
         edtavFuncaoapf_nome4_Internalname = "vFUNCAOAPF_NOME4";
         cmbavFuncaoapf_tipo4_Internalname = "vFUNCAOAPF_TIPO4";
         cmbavFuncaoapf_complexidade4_Internalname = "vFUNCAOAPF_COMPLEXIDADE4";
         cmbavFuncaoapf_ativo4_Internalname = "vFUNCAOAPF_ATIVO4";
         imgRemovedynamicfilters4_Internalname = "REMOVEDYNAMICFILTERS4";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         bttBtnatualizar_Internalname = "BTNATUALIZAR";
         tblTablefilters_Internalname = "TABLEFILTERS";
         lblTextblocksorepetidas_Internalname = "TEXTBLOCKSOREPETIDAS";
         cmbavSorepetidas_Internalname = "vSOREPETIDAS";
         imgBtnapagarselecionadas_Internalname = "BTNAPAGARSELECIONADAS";
         chkavSelectall_Internalname = "vSELECTALL";
         chkavReverseall_Internalname = "vREVERSEALL";
         chkavEmcascata_Internalname = "vEMCASCATA";
         tblTblsorepetidas_Internalname = "TBLSOREPETIDAS";
         tblTable1_Internalname = "TABLE1";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtavDisplay_Internalname = "vDISPLAY";
         chkavSelected_Internalname = "vSELECTED";
         edtavFuncaoapf_codigo_Internalname = "vFUNCAOAPF_CODIGO";
         edtavFuncao_nome_Internalname = "vFUNCAO_NOME";
         cmbavFuncaoapf_tipo_Internalname = "vFUNCAOAPF_TIPO";
         edtavFuncaoapf_td_Internalname = "vFUNCAOAPF_TD";
         edtavFuncaoapf_ar_Internalname = "vFUNCAOAPF_AR";
         cmbavFuncaoapf_complexidade_Internalname = "vFUNCAOAPF_COMPLEXIDADE";
         edtavFuncaoapf_pf_Internalname = "vFUNCAOAPF_PF";
         edtavFunapfpainom_Internalname = "vFUNAPFPAINOM";
         edtavFunapfpainom_Internalname = "vFUNAPFPAINOM";
         cmbavFuncaoapf_status_Internalname = "vFUNCAOAPF_STATUS";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         lblTextblockcontagemresultado_pflfstotal_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PFLFSTOTAL";
         edtavQtde_Internalname = "vQTDE";
         tblTbltotais_Internalname = "TBLTOTAIS";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         chkavDynamicfiltersenabled4_Internalname = "vDYNAMICFILTERSENABLED4";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavFuncaoapf_status_Jsonclick = "";
         cmbavFuncaoapf_status.Visible = -1;
         edtavFunapfpainom_Jsonclick = "";
         edtavFunapfpainom_Visible = 0;
         edtavFuncaoapf_pf_Jsonclick = "";
         edtavFuncaoapf_pf_Visible = -1;
         cmbavFuncaoapf_complexidade_Jsonclick = "";
         cmbavFuncaoapf_complexidade.Visible = -1;
         edtavFuncaoapf_ar_Jsonclick = "";
         edtavFuncaoapf_ar_Visible = -1;
         edtavFuncaoapf_td_Jsonclick = "";
         edtavFuncaoapf_td_Visible = -1;
         cmbavFuncaoapf_tipo_Jsonclick = "";
         cmbavFuncaoapf_tipo.Visible = -1;
         edtavFuncao_nome_Jsonclick = "";
         edtavFuncao_nome_Visible = -1;
         edtavFuncaoapf_codigo_Jsonclick = "";
         edtavFuncaoapf_codigo_Visible = 0;
         chkavSelected.Visible = -1;
         cmbavFuncaoapf_ativo4_Jsonclick = "";
         cmbavFuncaoapf_complexidade4_Jsonclick = "";
         cmbavFuncaoapf_tipo4_Jsonclick = "";
         edtavFuncaoapf_nome4_Jsonclick = "";
         cmbavDynamicfiltersselector4_Jsonclick = "";
         imgRemovedynamicfilters3_Visible = 1;
         imgAdddynamicfilters3_Visible = 1;
         cmbavFuncaoapf_ativo3_Jsonclick = "";
         cmbavFuncaoapf_complexidade3_Jsonclick = "";
         cmbavFuncaoapf_tipo3_Jsonclick = "";
         edtavFuncaoapf_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavFuncaoapf_ativo2_Jsonclick = "";
         cmbavFuncaoapf_complexidade2_Jsonclick = "";
         cmbavFuncaoapf_tipo2_Jsonclick = "";
         edtavFuncaoapf_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavFuncaoapf_ativo1_Jsonclick = "";
         cmbavFuncaoapf_complexidade1_Jsonclick = "";
         cmbavFuncaoapf_tipo1_Jsonclick = "";
         edtavFuncaoapf_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         cmbavSorepetidas_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         cmbavFuncaoapf_status.Enabled = 1;
         edtavFunapfpainom_Link = "";
         edtavFunapfpainom_Enabled = 1;
         edtavFuncaoapf_pf_Enabled = 1;
         cmbavFuncaoapf_complexidade.Enabled = 1;
         edtavFuncaoapf_ar_Enabled = 1;
         edtavFuncaoapf_td_Enabled = 1;
         cmbavFuncaoapf_tipo.Enabled = 1;
         edtavFuncao_nome_Link = "";
         edtavFuncao_nome_Enabled = 1;
         edtavFuncaoapf_codigo_Enabled = 1;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         cmbavFuncaoapf_status_Titleformat = 0;
         edtavFunapfpainom_Titleformat = 0;
         cmbavFuncaoapf_tipo_Titleformat = 0;
         edtavFuncao_nome_Titleformat = 0;
         subGrid_Class = "WorkWith";
         edtavQtde_Jsonclick = "";
         edtavQtde_Enabled = 1;
         edtavFuncaoapf_nome4_Visible = 1;
         edtavFuncaoapf_nome3_Visible = 1;
         edtavFuncaoapf_nome2_Visible = 1;
         edtavFuncaoapf_nome1_Visible = 1;
         chkavSelected.Enabled = 1;
         cmbavFuncaoapf_status.Title.Text = "Status";
         edtavFunapfpainom_Title = "C�digo";
         cmbavFuncaoapf_tipo.Title.Text = "Tipo";
         edtavFuncao_nome_Title = "Nome";
         lblFuncaoapftitle_Caption = "Fun��es de Transa��o";
         edtavOrdereddsc_Visible = 1;
         cmbavFuncaoapf_ativo4.Visible = 1;
         cmbavFuncaoapf_complexidade4.Visible = 1;
         cmbavFuncaoapf_tipo4.Visible = 1;
         cmbavFuncaoapf_ativo3.Visible = 1;
         cmbavFuncaoapf_complexidade3.Visible = 1;
         cmbavFuncaoapf_tipo3.Visible = 1;
         cmbavFuncaoapf_ativo2.Visible = 1;
         cmbavFuncaoapf_complexidade2.Visible = 1;
         cmbavFuncaoapf_tipo2.Visible = 1;
         cmbavFuncaoapf_ativo1.Visible = 1;
         cmbavFuncaoapf_complexidade1.Visible = 1;
         cmbavFuncaoapf_tipo1.Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled4.Caption = "";
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavSelected.Caption = "";
         chkavEmcascata.Caption = "";
         chkavReverseall.Caption = "";
         chkavSelectall.Caption = "";
         chkavDynamicfiltersenabled4.Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Fun��es de Transa��o";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''}],oparms:[{av:'subGrid_Rows',ctrl:'GRID',prop:'Rows'},{av:'edtavFuncao_nome_Titleformat',ctrl:'vFUNCAO_NOME',prop:'Titleformat'},{av:'edtavFuncao_nome_Title',ctrl:'vFUNCAO_NOME',prop:'Title'},{av:'cmbavFuncaoapf_tipo'},{av:'edtavFunapfpainom_Titleformat',ctrl:'vFUNAPFPAINOM',prop:'Titleformat'},{av:'edtavFunapfpainom_Title',ctrl:'vFUNAPFPAINOM',prop:'Title'},{av:'cmbavFuncaoapf_status'},{av:'chkavSelected.Enabled',ctrl:'vSELECTED',prop:'Enabled'},{av:'AV87SelectAll',fld:'vSELECTALL',pic:'',nv:false},{av:'AV90ReverseAll',fld:'vREVERSEALL',pic:'',nv:false},{av:'AV91EmCascata',fld:'vEMCASCATA',pic:'',nv:false},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID.LOAD","{handler:'E31HA2',iparms:[{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null}],oparms:[{av:'AV84Qtde',fld:'vQTDE',pic:'ZZZ9',nv:0},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'subGrid_Rows',ctrl:'GRID',prop:'Rows'},{av:'AV71Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV15Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV16Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtavFuncao_nome_Link',ctrl:'vFUNCAO_NOME',prop:'Link'},{av:'edtavFunapfpainom_Link',ctrl:'vFUNAPFPAINOM',prop:'Link'},{av:'AV30FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV80Funcao_Nome',fld:'vFUNCAO_NOME',pic:'',nv:''},{av:'AV78FuncaoAPF_PF',fld:'vFUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV79FunAPFPaiNom',fld:'vFUNAPFPAINOM',pic:'',nv:''},{av:'AV81FuncaoAPF_Status',fld:'vFUNCAOAPF_STATUS',pic:'',nv:''},{av:'AV77FuncaoAPF_Complexidade',fld:'vFUNCAOAPF_COMPLEXIDADE',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E11HA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21HA2',iparms:[],oparms:[{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E12HA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'},{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22HA2',iparms:[{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E23HA2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E13HA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'},{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E24HA2',iparms:[{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS3'","{handler:'E25HA2',iparms:[],oparms:[{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E14HA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'},{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26HA2',iparms:[{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS4'","{handler:'E15HA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null}],oparms:[{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'},{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR4.CLICK","{handler:'E27HA2',iparms:[{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E16HA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null}],oparms:[{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'cmbavFuncaoapf_complexidade1'},{av:'cmbavFuncaoapf_ativo1'},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'imgAdddynamicfilters3_Visible',ctrl:'ADDDYNAMICFILTERS3',prop:'Visible'},{av:'imgRemovedynamicfilters3_Visible',ctrl:'REMOVEDYNAMICFILTERS3',prop:'Visible'},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'cmbavFuncaoapf_complexidade2'},{av:'cmbavFuncaoapf_ativo2'},{av:'edtavFuncaoapf_nome3_Visible',ctrl:'vFUNCAOAPF_NOME3',prop:'Visible'},{av:'cmbavFuncaoapf_tipo3'},{av:'cmbavFuncaoapf_complexidade3'},{av:'cmbavFuncaoapf_ativo3'},{av:'edtavFuncaoapf_nome4_Visible',ctrl:'vFUNCAOAPF_NOME4',prop:'Visible'},{av:'cmbavFuncaoapf_tipo4'},{av:'cmbavFuncaoapf_complexidade4'},{av:'cmbavFuncaoapf_ativo4'}]}");
         setEventMetadata("'DOATUALIZAR'","{handler:'E17HA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("'DOAPAGARSELECIONADAS'","{handler:'E18HA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV91EmCascata',fld:'vEMCASCATA',pic:'',nv:false}],oparms:[{av:'AV91EmCascata',fld:'vEMCASCATA',pic:'',nv:false}]}");
         setEventMetadata("VSELECTED.CLICK","{handler:'E32HA2',iparms:[{av:'AV64Selected',fld:'vSELECTED',pic:'',nv:false},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV30FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null}]}");
         setEventMetadata("VSELECTALL.CLICK","{handler:'E19HA2',iparms:[{av:'AV87SelectAll',fld:'vSELECTALL',pic:'',nv:false},{av:'AV64Selected',fld:'vSELECTED',grid:124,pic:'',nv:false},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV30FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',grid:124,pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV64Selected',fld:'vSELECTED',pic:'',nv:false},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null}]}");
         setEventMetadata("VREVERSEALL.CLICK","{handler:'E20HA2',iparms:[{av:'AV64Selected',fld:'vSELECTED',grid:124,pic:'',nv:false},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV30FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',grid:124,pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV64Selected',fld:'vSELECTED',pic:'',nv:false},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null}]}");
         setEventMetadata("VSOREPETIDAS.CLICK","{handler:'E28HA2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null}],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''}],oparms:[{av:'subGrid_Rows',ctrl:'GRID',prop:'Rows'},{av:'edtavFuncao_nome_Titleformat',ctrl:'vFUNCAO_NOME',prop:'Titleformat'},{av:'edtavFuncao_nome_Title',ctrl:'vFUNCAO_NOME',prop:'Title'},{av:'cmbavFuncaoapf_tipo'},{av:'edtavFunapfpainom_Titleformat',ctrl:'vFUNAPFPAINOM',prop:'Titleformat'},{av:'edtavFunapfpainom_Title',ctrl:'vFUNAPFPAINOM',prop:'Title'},{av:'cmbavFuncaoapf_status'},{av:'chkavSelected.Enabled',ctrl:'vSELECTED',prop:'Enabled'},{av:'AV87SelectAll',fld:'vSELECTALL',pic:'',nv:false},{av:'AV90ReverseAll',fld:'vREVERSEALL',pic:'',nv:false},{av:'AV91EmCascata',fld:'vEMCASCATA',pic:'',nv:false},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''}],oparms:[{av:'subGrid_Rows',ctrl:'GRID',prop:'Rows'},{av:'edtavFuncao_nome_Titleformat',ctrl:'vFUNCAO_NOME',prop:'Titleformat'},{av:'edtavFuncao_nome_Title',ctrl:'vFUNCAO_NOME',prop:'Title'},{av:'cmbavFuncaoapf_tipo'},{av:'edtavFunapfpainom_Titleformat',ctrl:'vFUNAPFPAINOM',prop:'Titleformat'},{av:'edtavFunapfpainom_Title',ctrl:'vFUNAPFPAINOM',prop:'Title'},{av:'cmbavFuncaoapf_status'},{av:'chkavSelected.Enabled',ctrl:'vSELECTED',prop:'Enabled'},{av:'AV87SelectAll',fld:'vSELECTALL',pic:'',nv:false},{av:'AV90ReverseAll',fld:'vREVERSEALL',pic:'',nv:false},{av:'AV91EmCascata',fld:'vEMCASCATA',pic:'',nv:false},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''}],oparms:[{av:'subGrid_Rows',ctrl:'GRID',prop:'Rows'},{av:'edtavFuncao_nome_Titleformat',ctrl:'vFUNCAO_NOME',prop:'Titleformat'},{av:'edtavFuncao_nome_Title',ctrl:'vFUNCAO_NOME',prop:'Title'},{av:'cmbavFuncaoapf_tipo'},{av:'edtavFunapfpainom_Titleformat',ctrl:'vFUNAPFPAINOM',prop:'Titleformat'},{av:'edtavFunapfpainom_Title',ctrl:'vFUNAPFPAINOM',prop:'Title'},{av:'cmbavFuncaoapf_status'},{av:'chkavSelected.Enabled',ctrl:'vSELECTED',prop:'Enabled'},{av:'AV87SelectAll',fld:'vSELECTALL',pic:'',nv:false},{av:'AV90ReverseAll',fld:'vREVERSEALL',pic:'',nv:false},{av:'AV91EmCascata',fld:'vEMCASCATA',pic:'',nv:false},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV66SoRepetidas',fld:'vSOREPETIDAS',pic:'',nv:false},{av:'A166FuncaoAPF_Nome',fld:'FUNCAOAPF_NOME',pic:'',nv:''},{av:'A184FuncaoAPF_Tipo',fld:'FUNCAOAPF_TIPO',pic:'',nv:''},{av:'A183FuncaoAPF_Ativo',fld:'FUNCAOAPF_ATIVO',pic:'',nv:''},{av:'A185FuncaoAPF_Complexidade',fld:'FUNCAOAPF_COMPLEXIDADE',pic:'',nv:''},{av:'A387FuncaoAPF_AR',fld:'FUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'A388FuncaoAPF_TD',fld:'FUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A363FuncaoAPF_FunAPFPaiNom',fld:'FUNCAOAPF_FUNAPFPAINOM',pic:'',nv:''},{av:'AV42FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A358FuncaoAPF_FunAPFPaiCod',fld:'FUNCAOAPF_FUNAPFPAICOD',pic:'ZZZZZ9',nv:0},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV38FuncaoAPF_Nome',fld:'vFUNCAOAPF_NOME',pic:'',nv:''},{av:'AV48FuncaoAPF_Tipo',fld:'vFUNCAOAPF_TIPO',pic:'',nv:''},{av:'AV27FuncaoAPF_AR',fld:'vFUNCAOAPF_AR',pic:'ZZZ9',nv:0},{av:'AV47FuncaoAPF_TD',fld:'vFUNCAOAPF_TD',pic:'ZZZ9',nv:0},{av:'AV89FuncaoAPF_Ativo',fld:'vFUNCAOAPF_ATIVO',pic:'',nv:''},{av:'AV85Processadas',fld:'vPROCESSADAS',pic:'',nv:null},{av:'AV6Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV127Rows',fld:'vROWS',pic:'ZZZ9',nv:0},{av:'AV59OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV136Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV24DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV39FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV123FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV115FuncaoAPF_Ativo1',fld:'vFUNCAOAPF_ATIVO1',pic:'',nv:''},{av:'AV23DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV17DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV40FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV124FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV116FuncaoAPF_Ativo2',fld:'vFUNCAOAPF_ATIVO2',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV41FuncaoAPF_Nome3',fld:'vFUNCAOAPF_NOME3',pic:'',nv:''},{av:'AV125FuncaoAPF_Tipo3',fld:'vFUNCAOAPF_TIPO3',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV117FuncaoAPF_Ativo3',fld:'vFUNCAOAPF_ATIVO3',pic:'',nv:''},{av:'AV101DynamicFiltersEnabled4',fld:'vDYNAMICFILTERSENABLED4',pic:'',nv:false},{av:'AV102DynamicFiltersSelector4',fld:'vDYNAMICFILTERSSELECTOR4',pic:'',nv:''},{av:'AV109FuncaoAPF_Nome4',fld:'vFUNCAOAPF_NOME4',pic:'',nv:''},{av:'AV126FuncaoAPF_Tipo4',fld:'vFUNCAOAPF_TIPO4',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV118FuncaoAPF_Ativo4',fld:'vFUNCAOAPF_ATIVO4',pic:'',nv:''}],oparms:[{av:'subGrid_Rows',ctrl:'GRID',prop:'Rows'},{av:'edtavFuncao_nome_Titleformat',ctrl:'vFUNCAO_NOME',prop:'Titleformat'},{av:'edtavFuncao_nome_Title',ctrl:'vFUNCAO_NOME',prop:'Title'},{av:'cmbavFuncaoapf_tipo'},{av:'edtavFunapfpainom_Titleformat',ctrl:'vFUNAPFPAINOM',prop:'Titleformat'},{av:'edtavFunapfpainom_Title',ctrl:'vFUNAPFPAINOM',prop:'Title'},{av:'cmbavFuncaoapf_status'},{av:'chkavSelected.Enabled',ctrl:'vSELECTED',prop:'Enabled'},{av:'AV87SelectAll',fld:'vSELECTALL',pic:'',nv:false},{av:'AV90ReverseAll',fld:'vREVERSEALL',pic:'',nv:false},{av:'AV91EmCascata',fld:'vEMCASCATA',pic:'',nv:false},{av:'AV119FuncaoAPF_Complexidade1',fld:'vFUNCAOAPF_COMPLEXIDADE1',pic:'',nv:''},{av:'AV120FuncaoAPF_Complexidade2',fld:'vFUNCAOAPF_COMPLEXIDADE2',pic:'',nv:''},{av:'AV121FuncaoAPF_Complexidade3',fld:'vFUNCAOAPF_COMPLEXIDADE3',pic:'',nv:''},{av:'AV122FuncaoAPF_Complexidade4',fld:'vFUNCAOAPF_COMPLEXIDADE4',pic:'',nv:''},{av:'AV50GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV128Sistema_Sigla = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV136Pgmname = "";
         AV50GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV24DynamicFiltersSelector1 = "";
         AV39FuncaoAPF_Nome1 = "";
         AV123FuncaoAPF_Tipo1 = "";
         AV119FuncaoAPF_Complexidade1 = "";
         AV115FuncaoAPF_Ativo1 = "A";
         AV25DynamicFiltersSelector2 = "";
         AV40FuncaoAPF_Nome2 = "";
         AV124FuncaoAPF_Tipo2 = "";
         AV120FuncaoAPF_Complexidade2 = "";
         AV116FuncaoAPF_Ativo2 = "A";
         AV26DynamicFiltersSelector3 = "";
         AV41FuncaoAPF_Nome3 = "";
         AV125FuncaoAPF_Tipo3 = "";
         AV121FuncaoAPF_Complexidade3 = "";
         AV117FuncaoAPF_Ativo3 = "A";
         AV102DynamicFiltersSelector4 = "";
         AV109FuncaoAPF_Nome4 = "";
         AV126FuncaoAPF_Tipo4 = "";
         AV122FuncaoAPF_Complexidade4 = "";
         AV118FuncaoAPF_Ativo4 = "A";
         A166FuncaoAPF_Nome = "";
         A184FuncaoAPF_Tipo = "";
         A183FuncaoAPF_Ativo = "";
         A185FuncaoAPF_Complexidade = "";
         A363FuncaoAPF_FunAPFPaiNom = "";
         AV38FuncaoAPF_Nome = "";
         AV48FuncaoAPF_Tipo = "";
         AV89FuncaoAPF_Ativo = "A";
         AV85Processadas = new GxSimpleCollection();
         AV6Codigos = new GxSimpleCollection();
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV71Update = "";
         AV133Update_GXI = "";
         AV15Delete = "";
         AV134Delete_GXI = "";
         AV16Display = "";
         AV135Display_GXI = "";
         AV80Funcao_Nome = "";
         AV77FuncaoAPF_Complexidade = "";
         AV79FunAPFPaiNom = "";
         AV81FuncaoAPF_Status = "A";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgAdddynamicfilters3_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgRemovedynamicfilters4_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         AV75WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         scmdbuf = "";
         lV39FuncaoAPF_Nome1 = "";
         lV40FuncaoAPF_Nome2 = "";
         lV41FuncaoAPF_Nome3 = "";
         lV109FuncaoAPF_Nome4 = "";
         H00HA2_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00HA2_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00HA2_A183FuncaoAPF_Ativo = new String[] {""} ;
         H00HA2_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00HA2_A166FuncaoAPF_Nome = new String[] {""} ;
         H00HA2_A165FuncaoAPF_Codigo = new int[1] ;
         H00HA3_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00HA3_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00HA3_A183FuncaoAPF_Ativo = new String[] {""} ;
         H00HA3_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00HA3_A166FuncaoAPF_Nome = new String[] {""} ;
         H00HA3_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         H00HA3_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         H00HA3_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         H00HA3_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         H00HA3_A165FuncaoAPF_Codigo = new int[1] ;
         GXt_char2 = "";
         GridRow = new GXWebRow();
         AV86WebSession = context.GetSession();
         AV65Session = context.GetSession();
         AV51GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         H00HA4_A360FuncaoAPF_SistemaCod = new int[1] ;
         H00HA4_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H00HA4_A166FuncaoAPF_Nome = new String[] {""} ;
         H00HA4_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00HA4_A183FuncaoAPF_Ativo = new String[] {""} ;
         H00HA4_A165FuncaoAPF_Codigo = new int[1] ;
         sStyleString = "";
         lblTextblockcontagemresultado_pflfstotal_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFuncaoapftitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblTextblocksorepetidas_Jsonclick = "";
         imgBtnapagarselecionadas_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         bttBtnatualizar_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfiltersprefix4_Jsonclick = "";
         lblDynamicfiltersmiddle4_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_funcoestrn__default(),
            new Object[][] {
                new Object[] {
               H00HA2_A360FuncaoAPF_SistemaCod, H00HA2_n360FuncaoAPF_SistemaCod, H00HA2_A183FuncaoAPF_Ativo, H00HA2_A184FuncaoAPF_Tipo, H00HA2_A166FuncaoAPF_Nome, H00HA2_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               H00HA3_A360FuncaoAPF_SistemaCod, H00HA3_n360FuncaoAPF_SistemaCod, H00HA3_A183FuncaoAPF_Ativo, H00HA3_A184FuncaoAPF_Tipo, H00HA3_A166FuncaoAPF_Nome, H00HA3_A358FuncaoAPF_FunAPFPaiCod, H00HA3_n358FuncaoAPF_FunAPFPaiCod, H00HA3_A363FuncaoAPF_FunAPFPaiNom, H00HA3_n363FuncaoAPF_FunAPFPaiNom, H00HA3_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               H00HA4_A360FuncaoAPF_SistemaCod, H00HA4_n360FuncaoAPF_SistemaCod, H00HA4_A166FuncaoAPF_Nome, H00HA4_A184FuncaoAPF_Tipo, H00HA4_A183FuncaoAPF_Ativo, H00HA4_A165FuncaoAPF_Codigo
               }
            }
         );
         AV136Pgmname = "WP_FuncoesTRN";
         /* GeneXus formulas. */
         AV136Pgmname = "WP_FuncoesTRN";
         context.Gx_err = 0;
         edtavFuncaoapf_codigo_Enabled = 0;
         edtavFuncao_nome_Enabled = 0;
         cmbavFuncaoapf_tipo.Enabled = 0;
         edtavFuncaoapf_td_Enabled = 0;
         edtavFuncaoapf_ar_Enabled = 0;
         cmbavFuncaoapf_complexidade.Enabled = 0;
         edtavFuncaoapf_pf_Enabled = 0;
         edtavFunapfpainom_Enabled = 0;
         cmbavFuncaoapf_status.Enabled = 0;
         edtavQtde_Enabled = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_124 ;
      private short nGXsfl_124_idx=1 ;
      private short AV127Rows ;
      private short AV59OrderedBy ;
      private short A387FuncaoAPF_AR ;
      private short A388FuncaoAPF_TD ;
      private short AV27FuncaoAPF_AR ;
      private short AV47FuncaoAPF_TD ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_124_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV84Qtde ;
      private short edtavFuncao_nome_Titleformat ;
      private short cmbavFuncaoapf_tipo_Titleformat ;
      private short edtavFunapfpainom_Titleformat ;
      private short cmbavFuncaoapf_status_Titleformat ;
      private short nGXsfl_124_fel_idx=1 ;
      private short GXt_int1 ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int A360FuncaoAPF_SistemaCod ;
      private int wcpOA360FuncaoAPF_SistemaCod ;
      private int subGrid_Rows ;
      private int A165FuncaoAPF_Codigo ;
      private int AV42FuncaoAPF_SistemaCod ;
      private int A358FuncaoAPF_FunAPFPaiCod ;
      private int AV30FuncaoAPF_Codigo ;
      private int subGrid_Islastpage ;
      private int edtavFuncaoapf_codigo_Enabled ;
      private int edtavFuncao_nome_Enabled ;
      private int edtavFuncaoapf_td_Enabled ;
      private int edtavFuncaoapf_ar_Enabled ;
      private int edtavFuncaoapf_pf_Enabled ;
      private int edtavFunapfpainom_Enabled ;
      private int edtavQtde_Enabled ;
      private int edtavOrdereddsc_Visible ;
      private int AV50GridState_gxTpr_Dynamicfilters_Count ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgAdddynamicfilters3_Visible ;
      private int imgRemovedynamicfilters3_Visible ;
      private int edtavFuncaoapf_nome1_Visible ;
      private int edtavFuncaoapf_nome2_Visible ;
      private int edtavFuncaoapf_nome3_Visible ;
      private int edtavFuncaoapf_nome4_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavFuncaoapf_codigo_Visible ;
      private int edtavFuncao_nome_Visible ;
      private int edtavFuncaoapf_td_Visible ;
      private int edtavFuncaoapf_ar_Visible ;
      private int edtavFuncaoapf_pf_Visible ;
      private int edtavFunapfpainom_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A386FuncaoAPF_PF ;
      private decimal AV78FuncaoAPF_PF ;
      private decimal GXt_decimal3 ;
      private String AV128Sistema_Sigla ;
      private String wcpOAV128Sistema_Sigla ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_124_idx="0001" ;
      private String AV136Pgmname ;
      private String AV123FuncaoAPF_Tipo1 ;
      private String AV119FuncaoAPF_Complexidade1 ;
      private String AV115FuncaoAPF_Ativo1 ;
      private String AV124FuncaoAPF_Tipo2 ;
      private String AV120FuncaoAPF_Complexidade2 ;
      private String AV116FuncaoAPF_Ativo2 ;
      private String AV125FuncaoAPF_Tipo3 ;
      private String AV121FuncaoAPF_Complexidade3 ;
      private String AV117FuncaoAPF_Ativo3 ;
      private String AV126FuncaoAPF_Tipo4 ;
      private String AV122FuncaoAPF_Complexidade4 ;
      private String AV118FuncaoAPF_Ativo4 ;
      private String A184FuncaoAPF_Tipo ;
      private String A183FuncaoAPF_Ativo ;
      private String A185FuncaoAPF_Complexidade ;
      private String AV48FuncaoAPF_Tipo ;
      private String cmbavFuncaoapf_tipo_Internalname ;
      private String edtavFuncaoapf_ar_Internalname ;
      private String edtavFuncaoapf_td_Internalname ;
      private String AV89FuncaoAPF_Ativo ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String chkavDynamicfiltersenabled4_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String chkavSelected_Internalname ;
      private String edtavFuncaoapf_codigo_Internalname ;
      private String edtavFuncao_nome_Internalname ;
      private String cmbavFuncaoapf_complexidade_Internalname ;
      private String AV77FuncaoAPF_Complexidade ;
      private String edtavFuncaoapf_pf_Internalname ;
      private String edtavFunapfpainom_Internalname ;
      private String cmbavFuncaoapf_status_Internalname ;
      private String AV81FuncaoAPF_Status ;
      private String chkavSelectall_Internalname ;
      private String chkavReverseall_Internalname ;
      private String chkavEmcascata_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String edtavQtde_Internalname ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavFuncaoapf_nome1_Internalname ;
      private String cmbavFuncaoapf_tipo1_Internalname ;
      private String cmbavFuncaoapf_complexidade1_Internalname ;
      private String cmbavFuncaoapf_ativo1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavFuncaoapf_nome2_Internalname ;
      private String cmbavFuncaoapf_tipo2_Internalname ;
      private String cmbavFuncaoapf_complexidade2_Internalname ;
      private String cmbavFuncaoapf_ativo2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavFuncaoapf_nome3_Internalname ;
      private String cmbavFuncaoapf_tipo3_Internalname ;
      private String cmbavFuncaoapf_complexidade3_Internalname ;
      private String cmbavFuncaoapf_ativo3_Internalname ;
      private String cmbavDynamicfiltersselector4_Internalname ;
      private String edtavFuncaoapf_nome4_Internalname ;
      private String cmbavFuncaoapf_tipo4_Internalname ;
      private String cmbavFuncaoapf_complexidade4_Internalname ;
      private String cmbavFuncaoapf_ativo4_Internalname ;
      private String cmbavSorepetidas_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgAdddynamicfilters3_Jsonclick ;
      private String imgAdddynamicfilters3_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgRemovedynamicfilters4_Jsonclick ;
      private String imgRemovedynamicfilters4_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String lblFuncaoapftitle_Caption ;
      private String lblFuncaoapftitle_Internalname ;
      private String edtavFuncao_nome_Title ;
      private String edtavFunapfpainom_Title ;
      private String scmdbuf ;
      private String GXt_char2 ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavFuncao_nome_Link ;
      private String edtavFunapfpainom_Link ;
      private String sGXsfl_124_fel_idx="0001" ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTbltotais_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Internalname ;
      private String lblTextblockcontagemresultado_pflfstotal_Jsonclick ;
      private String edtavQtde_Jsonclick ;
      private String tblTablegridheader_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblFuncaoapftitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTable1_Internalname ;
      private String lblTextblocksorepetidas_Internalname ;
      private String lblTextblocksorepetidas_Jsonclick ;
      private String cmbavSorepetidas_Jsonclick ;
      private String tblTblsorepetidas_Internalname ;
      private String imgBtnapagarselecionadas_Internalname ;
      private String imgBtnapagarselecionadas_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String bttBtnatualizar_Internalname ;
      private String bttBtnatualizar_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavFuncaoapf_nome1_Jsonclick ;
      private String cmbavFuncaoapf_tipo1_Jsonclick ;
      private String cmbavFuncaoapf_complexidade1_Jsonclick ;
      private String cmbavFuncaoapf_ativo1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavFuncaoapf_nome2_Jsonclick ;
      private String cmbavFuncaoapf_tipo2_Jsonclick ;
      private String cmbavFuncaoapf_complexidade2_Jsonclick ;
      private String cmbavFuncaoapf_ativo2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavFuncaoapf_nome3_Jsonclick ;
      private String cmbavFuncaoapf_tipo3_Jsonclick ;
      private String cmbavFuncaoapf_complexidade3_Jsonclick ;
      private String cmbavFuncaoapf_ativo3_Jsonclick ;
      private String lblDynamicfiltersprefix4_Internalname ;
      private String lblDynamicfiltersprefix4_Jsonclick ;
      private String cmbavDynamicfiltersselector4_Jsonclick ;
      private String lblDynamicfiltersmiddle4_Internalname ;
      private String lblDynamicfiltersmiddle4_Jsonclick ;
      private String edtavFuncaoapf_nome4_Jsonclick ;
      private String cmbavFuncaoapf_tipo4_Jsonclick ;
      private String cmbavFuncaoapf_complexidade4_Jsonclick ;
      private String cmbavFuncaoapf_ativo4_Jsonclick ;
      private String ROClassString ;
      private String edtavFuncaoapf_codigo_Jsonclick ;
      private String edtavFuncao_nome_Jsonclick ;
      private String cmbavFuncaoapf_tipo_Jsonclick ;
      private String edtavFuncaoapf_td_Jsonclick ;
      private String edtavFuncaoapf_ar_Jsonclick ;
      private String cmbavFuncaoapf_complexidade_Jsonclick ;
      private String edtavFuncaoapf_pf_Jsonclick ;
      private String edtavFunapfpainom_Jsonclick ;
      private String cmbavFuncaoapf_status_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV61OrderedDsc ;
      private bool AV19DynamicFiltersIgnoreFirst ;
      private bool AV23DynamicFiltersRemoving ;
      private bool AV17DynamicFiltersEnabled2 ;
      private bool AV18DynamicFiltersEnabled3 ;
      private bool AV101DynamicFiltersEnabled4 ;
      private bool AV66SoRepetidas ;
      private bool n363FuncaoAPF_FunAPFPaiNom ;
      private bool n358FuncaoAPF_FunAPFPaiCod ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV64Selected ;
      private bool AV87SelectAll ;
      private bool AV90ReverseAll ;
      private bool AV91EmCascata ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV71Update_IsBlob ;
      private bool AV15Delete_IsBlob ;
      private bool AV16Display_IsBlob ;
      private String AV24DynamicFiltersSelector1 ;
      private String AV39FuncaoAPF_Nome1 ;
      private String AV25DynamicFiltersSelector2 ;
      private String AV40FuncaoAPF_Nome2 ;
      private String AV26DynamicFiltersSelector3 ;
      private String AV41FuncaoAPF_Nome3 ;
      private String AV102DynamicFiltersSelector4 ;
      private String AV109FuncaoAPF_Nome4 ;
      private String A166FuncaoAPF_Nome ;
      private String A363FuncaoAPF_FunAPFPaiNom ;
      private String AV38FuncaoAPF_Nome ;
      private String AV133Update_GXI ;
      private String AV134Delete_GXI ;
      private String AV135Display_GXI ;
      private String AV80Funcao_Nome ;
      private String AV79FunAPFPaiNom ;
      private String lV39FuncaoAPF_Nome1 ;
      private String lV40FuncaoAPF_Nome2 ;
      private String lV41FuncaoAPF_Nome3 ;
      private String lV109FuncaoAPF_Nome4 ;
      private String AV71Update ;
      private String AV15Delete ;
      private String AV16Display ;
      private IGxSession AV65Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_FuncaoAPF_SistemaCod ;
      private String aP1_Sistema_Sigla ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavFuncaoapf_tipo1 ;
      private GXCombobox cmbavFuncaoapf_complexidade1 ;
      private GXCombobox cmbavFuncaoapf_ativo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavFuncaoapf_tipo2 ;
      private GXCombobox cmbavFuncaoapf_complexidade2 ;
      private GXCombobox cmbavFuncaoapf_ativo2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavFuncaoapf_tipo3 ;
      private GXCombobox cmbavFuncaoapf_complexidade3 ;
      private GXCombobox cmbavFuncaoapf_ativo3 ;
      private GXCombobox cmbavDynamicfiltersselector4 ;
      private GXCombobox cmbavFuncaoapf_tipo4 ;
      private GXCombobox cmbavFuncaoapf_complexidade4 ;
      private GXCombobox cmbavFuncaoapf_ativo4 ;
      private GXCombobox cmbavSorepetidas ;
      private GXCheckbox chkavSelectall ;
      private GXCheckbox chkavReverseall ;
      private GXCheckbox chkavEmcascata ;
      private GXCheckbox chkavSelected ;
      private GXCombobox cmbavFuncaoapf_tipo ;
      private GXCombobox cmbavFuncaoapf_complexidade ;
      private GXCombobox cmbavFuncaoapf_status ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private GXCheckbox chkavDynamicfiltersenabled4 ;
      private IDataStoreProvider pr_default ;
      private int[] H00HA2_A360FuncaoAPF_SistemaCod ;
      private bool[] H00HA2_n360FuncaoAPF_SistemaCod ;
      private String[] H00HA2_A183FuncaoAPF_Ativo ;
      private String[] H00HA2_A184FuncaoAPF_Tipo ;
      private String[] H00HA2_A166FuncaoAPF_Nome ;
      private int[] H00HA2_A165FuncaoAPF_Codigo ;
      private int[] H00HA3_A360FuncaoAPF_SistemaCod ;
      private bool[] H00HA3_n360FuncaoAPF_SistemaCod ;
      private String[] H00HA3_A183FuncaoAPF_Ativo ;
      private String[] H00HA3_A184FuncaoAPF_Tipo ;
      private String[] H00HA3_A166FuncaoAPF_Nome ;
      private int[] H00HA3_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] H00HA3_n358FuncaoAPF_FunAPFPaiCod ;
      private String[] H00HA3_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] H00HA3_n363FuncaoAPF_FunAPFPaiNom ;
      private int[] H00HA3_A165FuncaoAPF_Codigo ;
      private int[] H00HA4_A360FuncaoAPF_SistemaCod ;
      private bool[] H00HA4_n360FuncaoAPF_SistemaCod ;
      private String[] H00HA4_A166FuncaoAPF_Nome ;
      private String[] H00HA4_A184FuncaoAPF_Tipo ;
      private String[] H00HA4_A183FuncaoAPF_Ativo ;
      private int[] H00HA4_A165FuncaoAPF_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV86WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV85Processadas ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV6Codigos ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPGridState AV50GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV51GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV75WWPContext ;
   }

   public class wp_funcoestrn__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00HA2( IGxContext context ,
                                             String AV24DynamicFiltersSelector1 ,
                                             String AV39FuncaoAPF_Nome1 ,
                                             String AV123FuncaoAPF_Tipo1 ,
                                             String AV115FuncaoAPF_Ativo1 ,
                                             bool AV17DynamicFiltersEnabled2 ,
                                             String AV25DynamicFiltersSelector2 ,
                                             String AV40FuncaoAPF_Nome2 ,
                                             String AV124FuncaoAPF_Tipo2 ,
                                             String AV116FuncaoAPF_Ativo2 ,
                                             bool AV18DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             String AV41FuncaoAPF_Nome3 ,
                                             String AV125FuncaoAPF_Tipo3 ,
                                             String AV117FuncaoAPF_Ativo3 ,
                                             bool AV101DynamicFiltersEnabled4 ,
                                             String AV102DynamicFiltersSelector4 ,
                                             String AV109FuncaoAPF_Nome4 ,
                                             String AV126FuncaoAPF_Tipo4 ,
                                             String AV118FuncaoAPF_Ativo4 ,
                                             int AV50GridState_gxTpr_Dynamicfilters_Count ,
                                             String A166FuncaoAPF_Nome ,
                                             String A184FuncaoAPF_Tipo ,
                                             String A183FuncaoAPF_Ativo ,
                                             int A165FuncaoAPF_Codigo ,
                                             int A360FuncaoAPF_SistemaCod ,
                                             String AV119FuncaoAPF_Complexidade1 ,
                                             String A185FuncaoAPF_Complexidade ,
                                             String AV120FuncaoAPF_Complexidade2 ,
                                             String AV121FuncaoAPF_Complexidade3 ,
                                             String AV122FuncaoAPF_Complexidade4 )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [13] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoAPF_SistemaCod], [FuncaoAPF_Ativo], [FuncaoAPF_Tipo], [FuncaoAPF_Nome], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([FuncaoAPF_SistemaCod] = @FuncaoAPF_SistemaCod)";
         if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "0") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39FuncaoAPF_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Nome] like @lV39FuncaoAPF_Nome1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "1") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123FuncaoAPF_Tipo1)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Tipo] = @AV123FuncaoAPF_Tipo1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "3") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115FuncaoAPF_Ativo1)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Ativo] = @AV115FuncaoAPF_Ativo1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "0") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40FuncaoAPF_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Nome] like @lV40FuncaoAPF_Nome2 + '%')";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "1") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124FuncaoAPF_Tipo2)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Tipo] = @AV124FuncaoAPF_Tipo2)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "3") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116FuncaoAPF_Ativo2)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Ativo] = @AV116FuncaoAPF_Ativo2)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "0") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41FuncaoAPF_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Nome] like @lV41FuncaoAPF_Nome3 + '%')";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV18DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "1") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125FuncaoAPF_Tipo3)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Tipo] = @AV125FuncaoAPF_Tipo3)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV18DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "3") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117FuncaoAPF_Ativo3)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Ativo] = @AV117FuncaoAPF_Ativo3)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV101DynamicFiltersEnabled4 && ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "0") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109FuncaoAPF_Nome4)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Nome] like @lV109FuncaoAPF_Nome4 + '%')";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV101DynamicFiltersEnabled4 && ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "1") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126FuncaoAPF_Tipo4)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Tipo] = @AV126FuncaoAPF_Tipo4)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV101DynamicFiltersEnabled4 && ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "3") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118FuncaoAPF_Ativo4)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Ativo] = @AV118FuncaoAPF_Ativo4)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV50GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_Nome], [FuncaoAPF_Tipo], [FuncaoAPF_Ativo]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H00HA3( IGxContext context ,
                                             int A165FuncaoAPF_Codigo ,
                                             IGxCollection AV6Codigos ,
                                             String AV24DynamicFiltersSelector1 ,
                                             String AV39FuncaoAPF_Nome1 ,
                                             String AV123FuncaoAPF_Tipo1 ,
                                             String AV115FuncaoAPF_Ativo1 ,
                                             bool AV17DynamicFiltersEnabled2 ,
                                             String AV25DynamicFiltersSelector2 ,
                                             String AV40FuncaoAPF_Nome2 ,
                                             String AV124FuncaoAPF_Tipo2 ,
                                             String AV116FuncaoAPF_Ativo2 ,
                                             bool AV18DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             String AV41FuncaoAPF_Nome3 ,
                                             String AV125FuncaoAPF_Tipo3 ,
                                             String AV117FuncaoAPF_Ativo3 ,
                                             bool AV101DynamicFiltersEnabled4 ,
                                             String AV102DynamicFiltersSelector4 ,
                                             String AV109FuncaoAPF_Nome4 ,
                                             String AV126FuncaoAPF_Tipo4 ,
                                             String AV118FuncaoAPF_Ativo4 ,
                                             bool AV66SoRepetidas ,
                                             int AV50GridState_gxTpr_Dynamicfilters_Count ,
                                             String A166FuncaoAPF_Nome ,
                                             String A184FuncaoAPF_Tipo ,
                                             String A183FuncaoAPF_Ativo ,
                                             short AV59OrderedBy ,
                                             int A360FuncaoAPF_SistemaCod ,
                                             String AV119FuncaoAPF_Complexidade1 ,
                                             String A185FuncaoAPF_Complexidade ,
                                             String AV120FuncaoAPF_Complexidade2 ,
                                             String AV121FuncaoAPF_Complexidade3 ,
                                             String AV122FuncaoAPF_Complexidade4 )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [13] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPF_SistemaCod], T1.[FuncaoAPF_Ativo], T1.[FuncaoAPF_Tipo], T1.[FuncaoAPF_Nome], T1.[FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, T2.[FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom, T1.[FuncaoAPF_Codigo] FROM ([FuncoesAPF] T1 WITH (NOLOCK) LEFT JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_FunAPFPaiCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoAPF_SistemaCod] = @FuncaoAPF_SistemaCod)";
         if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "0") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39FuncaoAPF_Nome1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV39FuncaoAPF_Nome1 + '%')";
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "1") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV123FuncaoAPF_Tipo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV123FuncaoAPF_Tipo1)";
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV24DynamicFiltersSelector1, "3") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115FuncaoAPF_Ativo1)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV115FuncaoAPF_Ativo1)";
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "0") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40FuncaoAPF_Nome2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV40FuncaoAPF_Nome2 + '%')";
         }
         else
         {
            GXv_int6[4] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "1") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124FuncaoAPF_Tipo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV124FuncaoAPF_Tipo2)";
         }
         else
         {
            GXv_int6[5] = 1;
         }
         if ( AV17DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV25DynamicFiltersSelector2, "3") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV116FuncaoAPF_Ativo2)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV116FuncaoAPF_Ativo2)";
         }
         else
         {
            GXv_int6[6] = 1;
         }
         if ( AV18DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "0") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41FuncaoAPF_Nome3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV41FuncaoAPF_Nome3 + '%')";
         }
         else
         {
            GXv_int6[7] = 1;
         }
         if ( AV18DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "1") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125FuncaoAPF_Tipo3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV125FuncaoAPF_Tipo3)";
         }
         else
         {
            GXv_int6[8] = 1;
         }
         if ( AV18DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "3") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV117FuncaoAPF_Ativo3)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV117FuncaoAPF_Ativo3)";
         }
         else
         {
            GXv_int6[9] = 1;
         }
         if ( AV101DynamicFiltersEnabled4 && ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "0") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV109FuncaoAPF_Nome4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV109FuncaoAPF_Nome4 + '%')";
         }
         else
         {
            GXv_int6[10] = 1;
         }
         if ( AV101DynamicFiltersEnabled4 && ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "1") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV126FuncaoAPF_Tipo4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV126FuncaoAPF_Tipo4)";
         }
         else
         {
            GXv_int6[11] = 1;
         }
         if ( AV101DynamicFiltersEnabled4 && ( StringUtil.StrCmp(AV102DynamicFiltersSelector4, "3") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV118FuncaoAPF_Ativo4)) ) )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV118FuncaoAPF_Ativo4)";
         }
         else
         {
            GXv_int6[12] = 1;
         }
         if ( AV66SoRepetidas )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV6Codigos, "T1.[FuncaoAPF_Codigo] IN (", ")") + ")";
         }
         if ( AV50GridState_gxTpr_Dynamicfilters_Count < 1 )
         {
            sWhereString = sWhereString + " and (T1.[FuncaoAPF_Codigo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV59OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Nome]";
         }
         else if ( AV59OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Tipo]";
         }
         else if ( AV59OrderedBy == 3 )
         {
            scmdbuf = scmdbuf + " ORDER BY T2.[FuncaoAPF_Nome]";
         }
         else if ( AV59OrderedBy == 4 )
         {
            scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Ativo]";
         }
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      protected Object[] conditional_H00HA4( IGxContext context ,
                                             int A165FuncaoAPF_Codigo ,
                                             IGxCollection AV85Processadas ,
                                             int A360FuncaoAPF_SistemaCod ,
                                             short A387FuncaoAPF_AR ,
                                             short AV27FuncaoAPF_AR ,
                                             short A388FuncaoAPF_TD ,
                                             short AV47FuncaoAPF_TD ,
                                             String AV38FuncaoAPF_Nome ,
                                             String AV48FuncaoAPF_Tipo ,
                                             String AV89FuncaoAPF_Ativo ,
                                             String A166FuncaoAPF_Nome ,
                                             String A184FuncaoAPF_Tipo ,
                                             String A183FuncaoAPF_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int8 ;
         GXv_int8 = new short [4] ;
         Object[] GXv_Object9 ;
         GXv_Object9 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoAPF_SistemaCod], [FuncaoAPF_Nome], [FuncaoAPF_Tipo], [FuncaoAPF_Ativo], [FuncaoAPF_Codigo] FROM [FuncoesAPF] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([FuncaoAPF_Nome] = @AV38FuncaoAPF_Nome and [FuncaoAPF_Tipo] = @AV48FuncaoAPF_Tipo and [FuncaoAPF_Ativo] = @AV89FuncaoAPF_Ativo)";
         scmdbuf = scmdbuf + " and ([FuncaoAPF_SistemaCod] = @FuncaoAPF_SistemaCod)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV85Processadas, "[FuncaoAPF_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_Nome], [FuncaoAPF_Tipo], [FuncaoAPF_Ativo]";
         GXv_Object9[0] = scmdbuf;
         GXv_Object9[1] = GXv_int8;
         return GXv_Object9 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00HA2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (bool)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (int)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] );
               case 1 :
                     return conditional_H00HA3(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (bool)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] );
               case 2 :
                     return conditional_H00HA4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (short)dynConstraints[3] , (short)dynConstraints[4] , (short)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00HA2 ;
          prmH00HA2 = new Object[] {
          new Object[] {"@FuncaoAPF_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV123FuncaoAPF_Tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@AV115FuncaoAPF_Ativo1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV40FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV124FuncaoAPF_Tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@AV116FuncaoAPF_Ativo2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV41FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV125FuncaoAPF_Tipo3",SqlDbType.Char,3,0} ,
          new Object[] {"@AV117FuncaoAPF_Ativo3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV109FuncaoAPF_Nome4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV126FuncaoAPF_Tipo4",SqlDbType.Char,3,0} ,
          new Object[] {"@AV118FuncaoAPF_Ativo4",SqlDbType.Char,1,0}
          } ;
          Object[] prmH00HA3 ;
          prmH00HA3 = new Object[] {
          new Object[] {"@FuncaoAPF_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV39FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV123FuncaoAPF_Tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@AV115FuncaoAPF_Ativo1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV40FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV124FuncaoAPF_Tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@AV116FuncaoAPF_Ativo2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV41FuncaoAPF_Nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV125FuncaoAPF_Tipo3",SqlDbType.Char,3,0} ,
          new Object[] {"@AV117FuncaoAPF_Ativo3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV109FuncaoAPF_Nome4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV126FuncaoAPF_Tipo4",SqlDbType.Char,3,0} ,
          new Object[] {"@AV118FuncaoAPF_Ativo4",SqlDbType.Char,1,0}
          } ;
          Object[] prmH00HA4 ;
          prmH00HA4 = new Object[] {
          new Object[] {"@AV38FuncaoAPF_Nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV48FuncaoAPF_Tipo",SqlDbType.Char,3,0} ,
          new Object[] {"@AV89FuncaoAPF_Ativo",SqlDbType.Char,1,0} ,
          new Object[] {"@FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00HA2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HA2,100,0,true,false )
             ,new CursorDef("H00HA3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HA3,100,0,true,false )
             ,new CursorDef("H00HA4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00HA4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 1) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[13] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Int );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (int)parms[14]);
                   }
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[13] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Int );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (int)parms[14]);
                   }
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   if ( (bool)parms[7] )
                   {
                      stmt.setNull( sIdx , SqlDbType.Int );
                   }
                   else
                   {
                      stmt.SetParameter(sIdx, (int)parms[8]);
                   }
                }
                return;
       }
    }

 }

}
