/*
               File: GetWWNotaEmpenhoFilterData
        Description: Get WWNota Empenho Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:44.75
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwnotaempenhofilterdata : GXProcedure
   {
      public getwwnotaempenhofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwnotaempenhofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV29DDOName = aP0_DDOName;
         this.AV27SearchTxt = aP1_SearchTxt;
         this.AV28SearchTxtTo = aP2_SearchTxtTo;
         this.AV33OptionsJson = "" ;
         this.AV36OptionsDescJson = "" ;
         this.AV38OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV33OptionsJson;
         aP4_OptionsDescJson=this.AV36OptionsDescJson;
         aP5_OptionIndexesJson=this.AV38OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV29DDOName = aP0_DDOName;
         this.AV27SearchTxt = aP1_SearchTxt;
         this.AV28SearchTxtTo = aP2_SearchTxtTo;
         this.AV33OptionsJson = "" ;
         this.AV36OptionsDescJson = "" ;
         this.AV38OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV33OptionsJson;
         aP4_OptionsDescJson=this.AV36OptionsDescJson;
         aP5_OptionIndexesJson=this.AV38OptionIndexesJson;
         return AV38OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwnotaempenhofilterdata objgetwwnotaempenhofilterdata;
         objgetwwnotaempenhofilterdata = new getwwnotaempenhofilterdata();
         objgetwwnotaempenhofilterdata.AV29DDOName = aP0_DDOName;
         objgetwwnotaempenhofilterdata.AV27SearchTxt = aP1_SearchTxt;
         objgetwwnotaempenhofilterdata.AV28SearchTxtTo = aP2_SearchTxtTo;
         objgetwwnotaempenhofilterdata.AV33OptionsJson = "" ;
         objgetwwnotaempenhofilterdata.AV36OptionsDescJson = "" ;
         objgetwwnotaempenhofilterdata.AV38OptionIndexesJson = "" ;
         objgetwwnotaempenhofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwnotaempenhofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwnotaempenhofilterdata);
         aP3_OptionsJson=this.AV33OptionsJson;
         aP4_OptionsDescJson=this.AV36OptionsDescJson;
         aP5_OptionIndexesJson=this.AV38OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwnotaempenhofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV32Options = (IGxCollection)(new GxSimpleCollection());
         AV35OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV37OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV29DDOName), "DDO_NOTAEMPENHO_ITENTIFICADOR") == 0 )
         {
            /* Execute user subroutine: 'LOADNOTAEMPENHO_ITENTIFICADOROPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV33OptionsJson = AV32Options.ToJSonString(false);
         AV36OptionsDescJson = AV35OptionsDesc.ToJSonString(false);
         AV38OptionIndexesJson = AV37OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV40Session.Get("WWNotaEmpenhoGridState"), "") == 0 )
         {
            AV42GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWNotaEmpenhoGridState"), "");
         }
         else
         {
            AV42GridState.FromXml(AV40Session.Get("WWNotaEmpenhoGridState"), "");
         }
         AV58GXV1 = 1;
         while ( AV58GXV1 <= AV42GridState.gxTpr_Filtervalues.Count )
         {
            AV43GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV42GridState.gxTpr_Filtervalues.Item(AV58GXV1));
            if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_CODIGO") == 0 )
            {
               AV10TFNotaEmpenho_Codigo = (int)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
               AV11TFNotaEmpenho_Codigo_To = (int)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_CODIGO") == 0 )
            {
               AV12TFSaldoContrato_Codigo = (int)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
               AV13TFSaldoContrato_Codigo_To = (int)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ITENTIFICADOR") == 0 )
            {
               AV14TFNotaEmpenho_Itentificador = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ITENTIFICADOR_SEL") == 0 )
            {
               AV15TFNotaEmpenho_Itentificador_Sel = AV43GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_DEMISSAO") == 0 )
            {
               AV16TFNotaEmpenho_DEmissao = context.localUtil.CToT( AV43GridStateFilterValue.gxTpr_Value, 2);
               AV17TFNotaEmpenho_DEmissao_To = context.localUtil.CToT( AV43GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_VALOR") == 0 )
            {
               AV18TFNotaEmpenho_Valor = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, ".");
               AV19TFNotaEmpenho_Valor_To = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_QTD") == 0 )
            {
               AV20TFNotaEmpenho_Qtd = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, ".");
               AV21TFNotaEmpenho_Qtd_To = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_SALDOANT") == 0 )
            {
               AV22TFNotaEmpenho_SaldoAnt = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, ".");
               AV23TFNotaEmpenho_SaldoAnt_To = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_SALDOPOS") == 0 )
            {
               AV24TFNotaEmpenho_SaldoPos = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, ".");
               AV25TFNotaEmpenho_SaldoPos_To = NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV43GridStateFilterValue.gxTpr_Name, "TFNOTAEMPENHO_ATIVO_SEL") == 0 )
            {
               AV26TFNotaEmpenho_Ativo_Sel = (short)(NumberUtil.Val( AV43GridStateFilterValue.gxTpr_Value, "."));
            }
            AV58GXV1 = (int)(AV58GXV1+1);
         }
         if ( AV42GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV44GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV42GridState.gxTpr_Dynamicfilters.Item(1));
            AV45DynamicFiltersSelector1 = AV44GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 )
            {
               AV46DynamicFiltersOperator1 = AV44GridStateDynamicFilter.gxTpr_Operator;
               AV47SaldoContrato_Codigo1 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV42GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV48DynamicFiltersEnabled2 = true;
               AV44GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV42GridState.gxTpr_Dynamicfilters.Item(2));
               AV49DynamicFiltersSelector2 = AV44GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 )
               {
                  AV50DynamicFiltersOperator2 = AV44GridStateDynamicFilter.gxTpr_Operator;
                  AV51SaldoContrato_Codigo2 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV42GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV52DynamicFiltersEnabled3 = true;
                  AV44GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV42GridState.gxTpr_Dynamicfilters.Item(3));
                  AV53DynamicFiltersSelector3 = AV44GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 )
                  {
                     AV54DynamicFiltersOperator3 = AV44GridStateDynamicFilter.gxTpr_Operator;
                     AV55SaldoContrato_Codigo3 = (int)(NumberUtil.Val( AV44GridStateDynamicFilter.gxTpr_Value, "."));
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADNOTAEMPENHO_ITENTIFICADOROPTIONS' Routine */
         AV14TFNotaEmpenho_Itentificador = AV27SearchTxt;
         AV15TFNotaEmpenho_Itentificador_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV45DynamicFiltersSelector1 ,
                                              AV46DynamicFiltersOperator1 ,
                                              AV47SaldoContrato_Codigo1 ,
                                              AV48DynamicFiltersEnabled2 ,
                                              AV49DynamicFiltersSelector2 ,
                                              AV50DynamicFiltersOperator2 ,
                                              AV51SaldoContrato_Codigo2 ,
                                              AV52DynamicFiltersEnabled3 ,
                                              AV53DynamicFiltersSelector3 ,
                                              AV54DynamicFiltersOperator3 ,
                                              AV55SaldoContrato_Codigo3 ,
                                              AV10TFNotaEmpenho_Codigo ,
                                              AV11TFNotaEmpenho_Codigo_To ,
                                              AV12TFSaldoContrato_Codigo ,
                                              AV13TFSaldoContrato_Codigo_To ,
                                              AV15TFNotaEmpenho_Itentificador_Sel ,
                                              AV14TFNotaEmpenho_Itentificador ,
                                              AV16TFNotaEmpenho_DEmissao ,
                                              AV17TFNotaEmpenho_DEmissao_To ,
                                              AV18TFNotaEmpenho_Valor ,
                                              AV19TFNotaEmpenho_Valor_To ,
                                              AV20TFNotaEmpenho_Qtd ,
                                              AV21TFNotaEmpenho_Qtd_To ,
                                              AV22TFNotaEmpenho_SaldoAnt ,
                                              AV23TFNotaEmpenho_SaldoAnt_To ,
                                              AV24TFNotaEmpenho_SaldoPos ,
                                              AV25TFNotaEmpenho_SaldoPos_To ,
                                              AV26TFNotaEmpenho_Ativo_Sel ,
                                              A1561SaldoContrato_Codigo ,
                                              A1560NotaEmpenho_Codigo ,
                                              A1564NotaEmpenho_Itentificador ,
                                              A1565NotaEmpenho_DEmissao ,
                                              A1566NotaEmpenho_Valor ,
                                              A1567NotaEmpenho_Qtd ,
                                              A1568NotaEmpenho_SaldoAnt ,
                                              A1569NotaEmpenho_SaldoPos ,
                                              A1570NotaEmpenho_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV14TFNotaEmpenho_Itentificador = StringUtil.PadR( StringUtil.RTrim( AV14TFNotaEmpenho_Itentificador), 15, "%");
         /* Using cursor P00R72 */
         pr_default.execute(0, new Object[] {AV47SaldoContrato_Codigo1, AV47SaldoContrato_Codigo1, AV47SaldoContrato_Codigo1, AV51SaldoContrato_Codigo2, AV51SaldoContrato_Codigo2, AV51SaldoContrato_Codigo2, AV55SaldoContrato_Codigo3, AV55SaldoContrato_Codigo3, AV55SaldoContrato_Codigo3, AV10TFNotaEmpenho_Codigo, AV11TFNotaEmpenho_Codigo_To, AV12TFSaldoContrato_Codigo, AV13TFSaldoContrato_Codigo_To, lV14TFNotaEmpenho_Itentificador, AV15TFNotaEmpenho_Itentificador_Sel, AV16TFNotaEmpenho_DEmissao, AV17TFNotaEmpenho_DEmissao_To, AV18TFNotaEmpenho_Valor, AV19TFNotaEmpenho_Valor_To, AV20TFNotaEmpenho_Qtd, AV21TFNotaEmpenho_Qtd_To, AV22TFNotaEmpenho_SaldoAnt, AV23TFNotaEmpenho_SaldoAnt_To, AV24TFNotaEmpenho_SaldoPos, AV25TFNotaEmpenho_SaldoPos_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKR72 = false;
            A1564NotaEmpenho_Itentificador = P00R72_A1564NotaEmpenho_Itentificador[0];
            n1564NotaEmpenho_Itentificador = P00R72_n1564NotaEmpenho_Itentificador[0];
            A1570NotaEmpenho_Ativo = P00R72_A1570NotaEmpenho_Ativo[0];
            n1570NotaEmpenho_Ativo = P00R72_n1570NotaEmpenho_Ativo[0];
            A1569NotaEmpenho_SaldoPos = P00R72_A1569NotaEmpenho_SaldoPos[0];
            n1569NotaEmpenho_SaldoPos = P00R72_n1569NotaEmpenho_SaldoPos[0];
            A1568NotaEmpenho_SaldoAnt = P00R72_A1568NotaEmpenho_SaldoAnt[0];
            n1568NotaEmpenho_SaldoAnt = P00R72_n1568NotaEmpenho_SaldoAnt[0];
            A1567NotaEmpenho_Qtd = P00R72_A1567NotaEmpenho_Qtd[0];
            n1567NotaEmpenho_Qtd = P00R72_n1567NotaEmpenho_Qtd[0];
            A1566NotaEmpenho_Valor = P00R72_A1566NotaEmpenho_Valor[0];
            n1566NotaEmpenho_Valor = P00R72_n1566NotaEmpenho_Valor[0];
            A1565NotaEmpenho_DEmissao = P00R72_A1565NotaEmpenho_DEmissao[0];
            n1565NotaEmpenho_DEmissao = P00R72_n1565NotaEmpenho_DEmissao[0];
            A1560NotaEmpenho_Codigo = P00R72_A1560NotaEmpenho_Codigo[0];
            A1561SaldoContrato_Codigo = P00R72_A1561SaldoContrato_Codigo[0];
            AV39count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00R72_A1564NotaEmpenho_Itentificador[0], A1564NotaEmpenho_Itentificador) == 0 ) )
            {
               BRKR72 = false;
               A1560NotaEmpenho_Codigo = P00R72_A1560NotaEmpenho_Codigo[0];
               AV39count = (long)(AV39count+1);
               BRKR72 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1564NotaEmpenho_Itentificador)) )
            {
               AV31Option = A1564NotaEmpenho_Itentificador;
               AV32Options.Add(AV31Option, 0);
               AV37OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV39count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV32Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKR72 )
            {
               BRKR72 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV32Options = new GxSimpleCollection();
         AV35OptionsDesc = new GxSimpleCollection();
         AV37OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV40Session = context.GetSession();
         AV42GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV43GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFNotaEmpenho_Itentificador = "";
         AV15TFNotaEmpenho_Itentificador_Sel = "";
         AV16TFNotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         AV17TFNotaEmpenho_DEmissao_To = (DateTime)(DateTime.MinValue);
         AV44GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV45DynamicFiltersSelector1 = "";
         AV49DynamicFiltersSelector2 = "";
         AV53DynamicFiltersSelector3 = "";
         scmdbuf = "";
         lV14TFNotaEmpenho_Itentificador = "";
         A1564NotaEmpenho_Itentificador = "";
         A1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         P00R72_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         P00R72_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         P00R72_A1570NotaEmpenho_Ativo = new bool[] {false} ;
         P00R72_n1570NotaEmpenho_Ativo = new bool[] {false} ;
         P00R72_A1569NotaEmpenho_SaldoPos = new decimal[1] ;
         P00R72_n1569NotaEmpenho_SaldoPos = new bool[] {false} ;
         P00R72_A1568NotaEmpenho_SaldoAnt = new decimal[1] ;
         P00R72_n1568NotaEmpenho_SaldoAnt = new bool[] {false} ;
         P00R72_A1567NotaEmpenho_Qtd = new decimal[1] ;
         P00R72_n1567NotaEmpenho_Qtd = new bool[] {false} ;
         P00R72_A1566NotaEmpenho_Valor = new decimal[1] ;
         P00R72_n1566NotaEmpenho_Valor = new bool[] {false} ;
         P00R72_A1565NotaEmpenho_DEmissao = new DateTime[] {DateTime.MinValue} ;
         P00R72_n1565NotaEmpenho_DEmissao = new bool[] {false} ;
         P00R72_A1560NotaEmpenho_Codigo = new int[1] ;
         P00R72_A1561SaldoContrato_Codigo = new int[1] ;
         AV31Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwnotaempenhofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00R72_A1564NotaEmpenho_Itentificador, P00R72_n1564NotaEmpenho_Itentificador, P00R72_A1570NotaEmpenho_Ativo, P00R72_n1570NotaEmpenho_Ativo, P00R72_A1569NotaEmpenho_SaldoPos, P00R72_n1569NotaEmpenho_SaldoPos, P00R72_A1568NotaEmpenho_SaldoAnt, P00R72_n1568NotaEmpenho_SaldoAnt, P00R72_A1567NotaEmpenho_Qtd, P00R72_n1567NotaEmpenho_Qtd,
               P00R72_A1566NotaEmpenho_Valor, P00R72_n1566NotaEmpenho_Valor, P00R72_A1565NotaEmpenho_DEmissao, P00R72_n1565NotaEmpenho_DEmissao, P00R72_A1560NotaEmpenho_Codigo, P00R72_A1561SaldoContrato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV26TFNotaEmpenho_Ativo_Sel ;
      private short AV46DynamicFiltersOperator1 ;
      private short AV50DynamicFiltersOperator2 ;
      private short AV54DynamicFiltersOperator3 ;
      private int AV58GXV1 ;
      private int AV10TFNotaEmpenho_Codigo ;
      private int AV11TFNotaEmpenho_Codigo_To ;
      private int AV12TFSaldoContrato_Codigo ;
      private int AV13TFSaldoContrato_Codigo_To ;
      private int AV47SaldoContrato_Codigo1 ;
      private int AV51SaldoContrato_Codigo2 ;
      private int AV55SaldoContrato_Codigo3 ;
      private int A1561SaldoContrato_Codigo ;
      private int A1560NotaEmpenho_Codigo ;
      private long AV39count ;
      private decimal AV18TFNotaEmpenho_Valor ;
      private decimal AV19TFNotaEmpenho_Valor_To ;
      private decimal AV20TFNotaEmpenho_Qtd ;
      private decimal AV21TFNotaEmpenho_Qtd_To ;
      private decimal AV22TFNotaEmpenho_SaldoAnt ;
      private decimal AV23TFNotaEmpenho_SaldoAnt_To ;
      private decimal AV24TFNotaEmpenho_SaldoPos ;
      private decimal AV25TFNotaEmpenho_SaldoPos_To ;
      private decimal A1566NotaEmpenho_Valor ;
      private decimal A1567NotaEmpenho_Qtd ;
      private decimal A1568NotaEmpenho_SaldoAnt ;
      private decimal A1569NotaEmpenho_SaldoPos ;
      private String AV14TFNotaEmpenho_Itentificador ;
      private String AV15TFNotaEmpenho_Itentificador_Sel ;
      private String scmdbuf ;
      private String lV14TFNotaEmpenho_Itentificador ;
      private String A1564NotaEmpenho_Itentificador ;
      private DateTime AV16TFNotaEmpenho_DEmissao ;
      private DateTime AV17TFNotaEmpenho_DEmissao_To ;
      private DateTime A1565NotaEmpenho_DEmissao ;
      private bool returnInSub ;
      private bool AV48DynamicFiltersEnabled2 ;
      private bool AV52DynamicFiltersEnabled3 ;
      private bool A1570NotaEmpenho_Ativo ;
      private bool BRKR72 ;
      private bool n1564NotaEmpenho_Itentificador ;
      private bool n1570NotaEmpenho_Ativo ;
      private bool n1569NotaEmpenho_SaldoPos ;
      private bool n1568NotaEmpenho_SaldoAnt ;
      private bool n1567NotaEmpenho_Qtd ;
      private bool n1566NotaEmpenho_Valor ;
      private bool n1565NotaEmpenho_DEmissao ;
      private String AV38OptionIndexesJson ;
      private String AV33OptionsJson ;
      private String AV36OptionsDescJson ;
      private String AV29DDOName ;
      private String AV27SearchTxt ;
      private String AV28SearchTxtTo ;
      private String AV45DynamicFiltersSelector1 ;
      private String AV49DynamicFiltersSelector2 ;
      private String AV53DynamicFiltersSelector3 ;
      private String AV31Option ;
      private IGxSession AV40Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00R72_A1564NotaEmpenho_Itentificador ;
      private bool[] P00R72_n1564NotaEmpenho_Itentificador ;
      private bool[] P00R72_A1570NotaEmpenho_Ativo ;
      private bool[] P00R72_n1570NotaEmpenho_Ativo ;
      private decimal[] P00R72_A1569NotaEmpenho_SaldoPos ;
      private bool[] P00R72_n1569NotaEmpenho_SaldoPos ;
      private decimal[] P00R72_A1568NotaEmpenho_SaldoAnt ;
      private bool[] P00R72_n1568NotaEmpenho_SaldoAnt ;
      private decimal[] P00R72_A1567NotaEmpenho_Qtd ;
      private bool[] P00R72_n1567NotaEmpenho_Qtd ;
      private decimal[] P00R72_A1566NotaEmpenho_Valor ;
      private bool[] P00R72_n1566NotaEmpenho_Valor ;
      private DateTime[] P00R72_A1565NotaEmpenho_DEmissao ;
      private bool[] P00R72_n1565NotaEmpenho_DEmissao ;
      private int[] P00R72_A1560NotaEmpenho_Codigo ;
      private int[] P00R72_A1561SaldoContrato_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV35OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV37OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV42GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV43GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV44GridStateDynamicFilter ;
   }

   public class getwwnotaempenhofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00R72( IGxContext context ,
                                             String AV45DynamicFiltersSelector1 ,
                                             short AV46DynamicFiltersOperator1 ,
                                             int AV47SaldoContrato_Codigo1 ,
                                             bool AV48DynamicFiltersEnabled2 ,
                                             String AV49DynamicFiltersSelector2 ,
                                             short AV50DynamicFiltersOperator2 ,
                                             int AV51SaldoContrato_Codigo2 ,
                                             bool AV52DynamicFiltersEnabled3 ,
                                             String AV53DynamicFiltersSelector3 ,
                                             short AV54DynamicFiltersOperator3 ,
                                             int AV55SaldoContrato_Codigo3 ,
                                             int AV10TFNotaEmpenho_Codigo ,
                                             int AV11TFNotaEmpenho_Codigo_To ,
                                             int AV12TFSaldoContrato_Codigo ,
                                             int AV13TFSaldoContrato_Codigo_To ,
                                             String AV15TFNotaEmpenho_Itentificador_Sel ,
                                             String AV14TFNotaEmpenho_Itentificador ,
                                             DateTime AV16TFNotaEmpenho_DEmissao ,
                                             DateTime AV17TFNotaEmpenho_DEmissao_To ,
                                             decimal AV18TFNotaEmpenho_Valor ,
                                             decimal AV19TFNotaEmpenho_Valor_To ,
                                             decimal AV20TFNotaEmpenho_Qtd ,
                                             decimal AV21TFNotaEmpenho_Qtd_To ,
                                             decimal AV22TFNotaEmpenho_SaldoAnt ,
                                             decimal AV23TFNotaEmpenho_SaldoAnt_To ,
                                             decimal AV24TFNotaEmpenho_SaldoPos ,
                                             decimal AV25TFNotaEmpenho_SaldoPos_To ,
                                             short AV26TFNotaEmpenho_Ativo_Sel ,
                                             int A1561SaldoContrato_Codigo ,
                                             int A1560NotaEmpenho_Codigo ,
                                             String A1564NotaEmpenho_Itentificador ,
                                             DateTime A1565NotaEmpenho_DEmissao ,
                                             decimal A1566NotaEmpenho_Valor ,
                                             decimal A1567NotaEmpenho_Qtd ,
                                             decimal A1568NotaEmpenho_SaldoAnt ,
                                             decimal A1569NotaEmpenho_SaldoPos ,
                                             bool A1570NotaEmpenho_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [25] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [NotaEmpenho_Itentificador], [NotaEmpenho_Ativo], [NotaEmpenho_SaldoPos], [NotaEmpenho_SaldoAnt], [NotaEmpenho_Qtd], [NotaEmpenho_Valor], [NotaEmpenho_DEmissao], [NotaEmpenho_Codigo], [SaldoContrato_Codigo] FROM [NotaEmpenho] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV46DynamicFiltersOperator1 == 0 ) && ( ! (0==AV47SaldoContrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] < @AV47SaldoContrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] < @AV47SaldoContrato_Codigo1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV46DynamicFiltersOperator1 == 1 ) && ( ! (0==AV47SaldoContrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] = @AV47SaldoContrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] = @AV47SaldoContrato_Codigo1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV45DynamicFiltersSelector1, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV46DynamicFiltersOperator1 == 2 ) && ( ! (0==AV47SaldoContrato_Codigo1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] > @AV47SaldoContrato_Codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] > @AV47SaldoContrato_Codigo1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV50DynamicFiltersOperator2 == 0 ) && ( ! (0==AV51SaldoContrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] < @AV51SaldoContrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] < @AV51SaldoContrato_Codigo2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV50DynamicFiltersOperator2 == 1 ) && ( ! (0==AV51SaldoContrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] = @AV51SaldoContrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] = @AV51SaldoContrato_Codigo2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV48DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV49DynamicFiltersSelector2, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV50DynamicFiltersOperator2 == 2 ) && ( ! (0==AV51SaldoContrato_Codigo2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] > @AV51SaldoContrato_Codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] > @AV51SaldoContrato_Codigo2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV52DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV54DynamicFiltersOperator3 == 0 ) && ( ! (0==AV55SaldoContrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] < @AV55SaldoContrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] < @AV55SaldoContrato_Codigo3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV52DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV54DynamicFiltersOperator3 == 1 ) && ( ! (0==AV55SaldoContrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] = @AV55SaldoContrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] = @AV55SaldoContrato_Codigo3)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV52DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV53DynamicFiltersSelector3, "SALDOCONTRATO_CODIGO") == 0 ) && ( AV54DynamicFiltersOperator3 == 2 ) && ( ! (0==AV55SaldoContrato_Codigo3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] > @AV55SaldoContrato_Codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] > @AV55SaldoContrato_Codigo3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV10TFNotaEmpenho_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Codigo] >= @AV10TFNotaEmpenho_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Codigo] >= @AV10TFNotaEmpenho_Codigo)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (0==AV11TFNotaEmpenho_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Codigo] <= @AV11TFNotaEmpenho_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Codigo] <= @AV11TFNotaEmpenho_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (0==AV12TFSaldoContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] >= @AV12TFSaldoContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] >= @AV12TFSaldoContrato_Codigo)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV13TFSaldoContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([SaldoContrato_Codigo] <= @AV13TFSaldoContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([SaldoContrato_Codigo] <= @AV13TFSaldoContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFNotaEmpenho_Itentificador_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFNotaEmpenho_Itentificador)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] like @lV14TFNotaEmpenho_Itentificador)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Itentificador] like @lV14TFNotaEmpenho_Itentificador)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFNotaEmpenho_Itentificador_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Itentificador] = @AV15TFNotaEmpenho_Itentificador_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Itentificador] = @AV15TFNotaEmpenho_Itentificador_Sel)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV16TFNotaEmpenho_DEmissao) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_DEmissao] >= @AV16TFNotaEmpenho_DEmissao)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_DEmissao] >= @AV16TFNotaEmpenho_DEmissao)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV17TFNotaEmpenho_DEmissao_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_DEmissao] <= @AV17TFNotaEmpenho_DEmissao_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_DEmissao] <= @AV17TFNotaEmpenho_DEmissao_To)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFNotaEmpenho_Valor) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Valor] >= @AV18TFNotaEmpenho_Valor)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Valor] >= @AV18TFNotaEmpenho_Valor)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFNotaEmpenho_Valor_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Valor] <= @AV19TFNotaEmpenho_Valor_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Valor] <= @AV19TFNotaEmpenho_Valor_To)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV20TFNotaEmpenho_Qtd) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Qtd] >= @AV20TFNotaEmpenho_Qtd)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Qtd] >= @AV20TFNotaEmpenho_Qtd)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFNotaEmpenho_Qtd_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Qtd] <= @AV21TFNotaEmpenho_Qtd_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Qtd] <= @AV21TFNotaEmpenho_Qtd_To)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFNotaEmpenho_SaldoAnt) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoAnt] >= @AV22TFNotaEmpenho_SaldoAnt)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoAnt] >= @AV22TFNotaEmpenho_SaldoAnt)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFNotaEmpenho_SaldoAnt_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoAnt] <= @AV23TFNotaEmpenho_SaldoAnt_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoAnt] <= @AV23TFNotaEmpenho_SaldoAnt_To)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV24TFNotaEmpenho_SaldoPos) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoPos] >= @AV24TFNotaEmpenho_SaldoPos)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoPos] >= @AV24TFNotaEmpenho_SaldoPos)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFNotaEmpenho_SaldoPos_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_SaldoPos] <= @AV25TFNotaEmpenho_SaldoPos_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_SaldoPos] <= @AV25TFNotaEmpenho_SaldoPos_To)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( AV26TFNotaEmpenho_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Ativo] = 1)";
            }
         }
         if ( AV26TFNotaEmpenho_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([NotaEmpenho_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([NotaEmpenho_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [NotaEmpenho_Itentificador]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00R72(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (int)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (int)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (int)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (decimal)dynConstraints[21] , (decimal)dynConstraints[22] , (decimal)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (short)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (decimal)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (bool)dynConstraints[36] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00R72 ;
          prmP00R72 = new Object[] {
          new Object[] {"@AV47SaldoContrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47SaldoContrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47SaldoContrato_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@AV51SaldoContrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV51SaldoContrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV51SaldoContrato_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55SaldoContrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55SaldoContrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55SaldoContrato_Codigo3",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFNotaEmpenho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFNotaEmpenho_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFSaldoContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFSaldoContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFNotaEmpenho_Itentificador",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFNotaEmpenho_Itentificador_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV16TFNotaEmpenho_DEmissao",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV17TFNotaEmpenho_DEmissao_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV18TFNotaEmpenho_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV19TFNotaEmpenho_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV20TFNotaEmpenho_Qtd",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV21TFNotaEmpenho_Qtd_To",SqlDbType.Decimal,14,5} ,
          new Object[] {"@AV22TFNotaEmpenho_SaldoAnt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV23TFNotaEmpenho_SaldoAnt_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV24TFNotaEmpenho_SaldoPos",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV25TFNotaEmpenho_SaldoPos_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00R72", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00R72,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[12])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((int[]) buf[14])[0] = rslt.getInt(8) ;
                ((int[]) buf[15])[0] = rslt.getInt(9) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[49]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwnotaempenhofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwnotaempenhofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwnotaempenhofilterdata") )
          {
             return  ;
          }
          getwwnotaempenhofilterdata worker = new getwwnotaempenhofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
