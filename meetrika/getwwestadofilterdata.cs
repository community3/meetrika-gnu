/*
               File: GetWWEstadoFilterData
        Description: Get WWEstado Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:10:15.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwestadofilterdata : GXProcedure
   {
      public getwwestadofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwestadofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
         return AV26OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwestadofilterdata objgetwwestadofilterdata;
         objgetwwestadofilterdata = new getwwestadofilterdata();
         objgetwwestadofilterdata.AV17DDOName = aP0_DDOName;
         objgetwwestadofilterdata.AV15SearchTxt = aP1_SearchTxt;
         objgetwwestadofilterdata.AV16SearchTxtTo = aP2_SearchTxtTo;
         objgetwwestadofilterdata.AV21OptionsJson = "" ;
         objgetwwestadofilterdata.AV24OptionsDescJson = "" ;
         objgetwwestadofilterdata.AV26OptionIndexesJson = "" ;
         objgetwwestadofilterdata.context.SetSubmitInitialConfig(context);
         objgetwwestadofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwestadofilterdata);
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwestadofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV20Options = (IGxCollection)(new GxSimpleCollection());
         AV23OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV25OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV17DDOName), "DDO_ESTADO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADESTADO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV21OptionsJson = AV20Options.ToJSonString(false);
         AV24OptionsDescJson = AV23OptionsDesc.ToJSonString(false);
         AV26OptionIndexesJson = AV25OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV28Session.Get("WWEstadoGridState"), "") == 0 )
         {
            AV30GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWEstadoGridState"), "");
         }
         else
         {
            AV30GridState.FromXml(AV28Session.Get("WWEstadoGridState"), "");
         }
         AV46GXV1 = 1;
         while ( AV46GXV1 <= AV30GridState.gxTpr_Filtervalues.Count )
         {
            AV31GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV30GridState.gxTpr_Filtervalues.Item(AV46GXV1));
            if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFESTADO_NOME") == 0 )
            {
               AV12TFEstado_Nome = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFESTADO_NOME_SEL") == 0 )
            {
               AV13TFEstado_Nome_Sel = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFESTADO_ATIVO_SEL") == 0 )
            {
               AV14TFEstado_Ativo_Sel = (short)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            AV46GXV1 = (int)(AV46GXV1+1);
         }
         if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(1));
            AV33DynamicFiltersSelector1 = AV32GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "ESTADO_UF") == 0 )
            {
               AV34Estado_UF1 = AV32GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "ESTADO_NOME") == 0 )
            {
               AV35Estado_Nome1 = AV32GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV36DynamicFiltersEnabled2 = true;
               AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(2));
               AV37DynamicFiltersSelector2 = AV32GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "ESTADO_UF") == 0 )
               {
                  AV38Estado_UF2 = AV32GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV37DynamicFiltersSelector2, "ESTADO_NOME") == 0 )
               {
                  AV39Estado_Nome2 = AV32GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV40DynamicFiltersEnabled3 = true;
                  AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(3));
                  AV41DynamicFiltersSelector3 = AV32GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "ESTADO_UF") == 0 )
                  {
                     AV42Estado_UF3 = AV32GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector3, "ESTADO_NOME") == 0 )
                  {
                     AV43Estado_Nome3 = AV32GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADESTADO_NOMEOPTIONS' Routine */
         AV12TFEstado_Nome = AV15SearchTxt;
         AV13TFEstado_Nome_Sel = "";
         AV48WWEstadoDS_1_Dynamicfiltersselector1 = AV33DynamicFiltersSelector1;
         AV49WWEstadoDS_2_Estado_uf1 = AV34Estado_UF1;
         AV50WWEstadoDS_3_Estado_nome1 = AV35Estado_Nome1;
         AV51WWEstadoDS_4_Dynamicfiltersenabled2 = AV36DynamicFiltersEnabled2;
         AV52WWEstadoDS_5_Dynamicfiltersselector2 = AV37DynamicFiltersSelector2;
         AV53WWEstadoDS_6_Estado_uf2 = AV38Estado_UF2;
         AV54WWEstadoDS_7_Estado_nome2 = AV39Estado_Nome2;
         AV55WWEstadoDS_8_Dynamicfiltersenabled3 = AV40DynamicFiltersEnabled3;
         AV56WWEstadoDS_9_Dynamicfiltersselector3 = AV41DynamicFiltersSelector3;
         AV57WWEstadoDS_10_Estado_uf3 = AV42Estado_UF3;
         AV58WWEstadoDS_11_Estado_nome3 = AV43Estado_Nome3;
         AV59WWEstadoDS_12_Tfestado_nome = AV12TFEstado_Nome;
         AV60WWEstadoDS_13_Tfestado_nome_sel = AV13TFEstado_Nome_Sel;
         AV61WWEstadoDS_14_Tfestado_ativo_sel = AV14TFEstado_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV48WWEstadoDS_1_Dynamicfiltersselector1 ,
                                              AV49WWEstadoDS_2_Estado_uf1 ,
                                              AV50WWEstadoDS_3_Estado_nome1 ,
                                              AV51WWEstadoDS_4_Dynamicfiltersenabled2 ,
                                              AV52WWEstadoDS_5_Dynamicfiltersselector2 ,
                                              AV53WWEstadoDS_6_Estado_uf2 ,
                                              AV54WWEstadoDS_7_Estado_nome2 ,
                                              AV55WWEstadoDS_8_Dynamicfiltersenabled3 ,
                                              AV56WWEstadoDS_9_Dynamicfiltersselector3 ,
                                              AV57WWEstadoDS_10_Estado_uf3 ,
                                              AV58WWEstadoDS_11_Estado_nome3 ,
                                              AV60WWEstadoDS_13_Tfestado_nome_sel ,
                                              AV59WWEstadoDS_12_Tfestado_nome ,
                                              AV61WWEstadoDS_14_Tfestado_ativo_sel ,
                                              A23Estado_UF ,
                                              A24Estado_Nome ,
                                              A634Estado_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV49WWEstadoDS_2_Estado_uf1 = StringUtil.PadR( StringUtil.RTrim( AV49WWEstadoDS_2_Estado_uf1), 2, "%");
         lV50WWEstadoDS_3_Estado_nome1 = StringUtil.PadR( StringUtil.RTrim( AV50WWEstadoDS_3_Estado_nome1), 50, "%");
         lV53WWEstadoDS_6_Estado_uf2 = StringUtil.PadR( StringUtil.RTrim( AV53WWEstadoDS_6_Estado_uf2), 2, "%");
         lV54WWEstadoDS_7_Estado_nome2 = StringUtil.PadR( StringUtil.RTrim( AV54WWEstadoDS_7_Estado_nome2), 50, "%");
         lV57WWEstadoDS_10_Estado_uf3 = StringUtil.PadR( StringUtil.RTrim( AV57WWEstadoDS_10_Estado_uf3), 2, "%");
         lV58WWEstadoDS_11_Estado_nome3 = StringUtil.PadR( StringUtil.RTrim( AV58WWEstadoDS_11_Estado_nome3), 50, "%");
         lV59WWEstadoDS_12_Tfestado_nome = StringUtil.PadR( StringUtil.RTrim( AV59WWEstadoDS_12_Tfestado_nome), 50, "%");
         /* Using cursor P00EM2 */
         pr_default.execute(0, new Object[] {lV49WWEstadoDS_2_Estado_uf1, lV50WWEstadoDS_3_Estado_nome1, lV53WWEstadoDS_6_Estado_uf2, lV54WWEstadoDS_7_Estado_nome2, lV57WWEstadoDS_10_Estado_uf3, lV58WWEstadoDS_11_Estado_nome3, lV59WWEstadoDS_12_Tfestado_nome, AV60WWEstadoDS_13_Tfestado_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKEM2 = false;
            A24Estado_Nome = P00EM2_A24Estado_Nome[0];
            A634Estado_Ativo = P00EM2_A634Estado_Ativo[0];
            A23Estado_UF = P00EM2_A23Estado_UF[0];
            AV27count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00EM2_A24Estado_Nome[0], A24Estado_Nome) == 0 ) )
            {
               BRKEM2 = false;
               A23Estado_UF = P00EM2_A23Estado_UF[0];
               AV27count = (long)(AV27count+1);
               BRKEM2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A24Estado_Nome)) )
            {
               AV19Option = A24Estado_Nome;
               AV20Options.Add(AV19Option, 0);
               AV25OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV27count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV20Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKEM2 )
            {
               BRKEM2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV20Options = new GxSimpleCollection();
         AV23OptionsDesc = new GxSimpleCollection();
         AV25OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV28Session = context.GetSession();
         AV30GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV31GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFEstado_Nome = "";
         AV13TFEstado_Nome_Sel = "";
         AV32GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV33DynamicFiltersSelector1 = "";
         AV34Estado_UF1 = "";
         AV35Estado_Nome1 = "";
         AV37DynamicFiltersSelector2 = "";
         AV38Estado_UF2 = "";
         AV39Estado_Nome2 = "";
         AV41DynamicFiltersSelector3 = "";
         AV42Estado_UF3 = "";
         AV43Estado_Nome3 = "";
         AV48WWEstadoDS_1_Dynamicfiltersselector1 = "";
         AV49WWEstadoDS_2_Estado_uf1 = "";
         AV50WWEstadoDS_3_Estado_nome1 = "";
         AV52WWEstadoDS_5_Dynamicfiltersselector2 = "";
         AV53WWEstadoDS_6_Estado_uf2 = "";
         AV54WWEstadoDS_7_Estado_nome2 = "";
         AV56WWEstadoDS_9_Dynamicfiltersselector3 = "";
         AV57WWEstadoDS_10_Estado_uf3 = "";
         AV58WWEstadoDS_11_Estado_nome3 = "";
         AV59WWEstadoDS_12_Tfestado_nome = "";
         AV60WWEstadoDS_13_Tfestado_nome_sel = "";
         scmdbuf = "";
         lV49WWEstadoDS_2_Estado_uf1 = "";
         lV50WWEstadoDS_3_Estado_nome1 = "";
         lV53WWEstadoDS_6_Estado_uf2 = "";
         lV54WWEstadoDS_7_Estado_nome2 = "";
         lV57WWEstadoDS_10_Estado_uf3 = "";
         lV58WWEstadoDS_11_Estado_nome3 = "";
         lV59WWEstadoDS_12_Tfestado_nome = "";
         A23Estado_UF = "";
         A24Estado_Nome = "";
         P00EM2_A24Estado_Nome = new String[] {""} ;
         P00EM2_A634Estado_Ativo = new bool[] {false} ;
         P00EM2_A23Estado_UF = new String[] {""} ;
         AV19Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwestadofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00EM2_A24Estado_Nome, P00EM2_A634Estado_Ativo, P00EM2_A23Estado_UF
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFEstado_Ativo_Sel ;
      private short AV61WWEstadoDS_14_Tfestado_ativo_sel ;
      private int AV46GXV1 ;
      private long AV27count ;
      private String AV12TFEstado_Nome ;
      private String AV13TFEstado_Nome_Sel ;
      private String AV34Estado_UF1 ;
      private String AV35Estado_Nome1 ;
      private String AV38Estado_UF2 ;
      private String AV39Estado_Nome2 ;
      private String AV42Estado_UF3 ;
      private String AV43Estado_Nome3 ;
      private String AV49WWEstadoDS_2_Estado_uf1 ;
      private String AV50WWEstadoDS_3_Estado_nome1 ;
      private String AV53WWEstadoDS_6_Estado_uf2 ;
      private String AV54WWEstadoDS_7_Estado_nome2 ;
      private String AV57WWEstadoDS_10_Estado_uf3 ;
      private String AV58WWEstadoDS_11_Estado_nome3 ;
      private String AV59WWEstadoDS_12_Tfestado_nome ;
      private String AV60WWEstadoDS_13_Tfestado_nome_sel ;
      private String scmdbuf ;
      private String lV49WWEstadoDS_2_Estado_uf1 ;
      private String lV50WWEstadoDS_3_Estado_nome1 ;
      private String lV53WWEstadoDS_6_Estado_uf2 ;
      private String lV54WWEstadoDS_7_Estado_nome2 ;
      private String lV57WWEstadoDS_10_Estado_uf3 ;
      private String lV58WWEstadoDS_11_Estado_nome3 ;
      private String lV59WWEstadoDS_12_Tfestado_nome ;
      private String A23Estado_UF ;
      private String A24Estado_Nome ;
      private bool returnInSub ;
      private bool AV36DynamicFiltersEnabled2 ;
      private bool AV40DynamicFiltersEnabled3 ;
      private bool AV51WWEstadoDS_4_Dynamicfiltersenabled2 ;
      private bool AV55WWEstadoDS_8_Dynamicfiltersenabled3 ;
      private bool A634Estado_Ativo ;
      private bool BRKEM2 ;
      private String AV26OptionIndexesJson ;
      private String AV21OptionsJson ;
      private String AV24OptionsDescJson ;
      private String AV17DDOName ;
      private String AV15SearchTxt ;
      private String AV16SearchTxtTo ;
      private String AV33DynamicFiltersSelector1 ;
      private String AV37DynamicFiltersSelector2 ;
      private String AV41DynamicFiltersSelector3 ;
      private String AV48WWEstadoDS_1_Dynamicfiltersselector1 ;
      private String AV52WWEstadoDS_5_Dynamicfiltersselector2 ;
      private String AV56WWEstadoDS_9_Dynamicfiltersselector3 ;
      private String AV19Option ;
      private IGxSession AV28Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00EM2_A24Estado_Nome ;
      private bool[] P00EM2_A634Estado_Ativo ;
      private String[] P00EM2_A23Estado_UF ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV30GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV31GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV32GridStateDynamicFilter ;
   }

   public class getwwestadofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00EM2( IGxContext context ,
                                             String AV48WWEstadoDS_1_Dynamicfiltersselector1 ,
                                             String AV49WWEstadoDS_2_Estado_uf1 ,
                                             String AV50WWEstadoDS_3_Estado_nome1 ,
                                             bool AV51WWEstadoDS_4_Dynamicfiltersenabled2 ,
                                             String AV52WWEstadoDS_5_Dynamicfiltersselector2 ,
                                             String AV53WWEstadoDS_6_Estado_uf2 ,
                                             String AV54WWEstadoDS_7_Estado_nome2 ,
                                             bool AV55WWEstadoDS_8_Dynamicfiltersenabled3 ,
                                             String AV56WWEstadoDS_9_Dynamicfiltersselector3 ,
                                             String AV57WWEstadoDS_10_Estado_uf3 ,
                                             String AV58WWEstadoDS_11_Estado_nome3 ,
                                             String AV60WWEstadoDS_13_Tfestado_nome_sel ,
                                             String AV59WWEstadoDS_12_Tfestado_nome ,
                                             short AV61WWEstadoDS_14_Tfestado_ativo_sel ,
                                             String A23Estado_UF ,
                                             String A24Estado_Nome ,
                                             bool A634Estado_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Estado_Nome], [Estado_Ativo], [Estado_UF] FROM [Estado] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV48WWEstadoDS_1_Dynamicfiltersselector1, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49WWEstadoDS_2_Estado_uf1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_UF] like '%' + @lV49WWEstadoDS_2_Estado_uf1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_UF] like '%' + @lV49WWEstadoDS_2_Estado_uf1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV48WWEstadoDS_1_Dynamicfiltersselector1, "ESTADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWEstadoDS_3_Estado_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like '%' + @lV50WWEstadoDS_3_Estado_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like '%' + @lV50WWEstadoDS_3_Estado_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV51WWEstadoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWEstadoDS_5_Dynamicfiltersselector2, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWEstadoDS_6_Estado_uf2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_UF] like '%' + @lV53WWEstadoDS_6_Estado_uf2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_UF] like '%' + @lV53WWEstadoDS_6_Estado_uf2 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV51WWEstadoDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV52WWEstadoDS_5_Dynamicfiltersselector2, "ESTADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWEstadoDS_7_Estado_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like '%' + @lV54WWEstadoDS_7_Estado_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like '%' + @lV54WWEstadoDS_7_Estado_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV55WWEstadoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV56WWEstadoDS_9_Dynamicfiltersselector3, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWEstadoDS_10_Estado_uf3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_UF] like '%' + @lV57WWEstadoDS_10_Estado_uf3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_UF] like '%' + @lV57WWEstadoDS_10_Estado_uf3 + '%')";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV55WWEstadoDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV56WWEstadoDS_9_Dynamicfiltersselector3, "ESTADO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWEstadoDS_11_Estado_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like '%' + @lV58WWEstadoDS_11_Estado_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like '%' + @lV58WWEstadoDS_11_Estado_nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60WWEstadoDS_13_Tfestado_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWEstadoDS_12_Tfestado_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] like @lV59WWEstadoDS_12_Tfestado_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] like @lV59WWEstadoDS_12_Tfestado_nome)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWEstadoDS_13_Tfestado_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Nome] = @AV60WWEstadoDS_13_Tfestado_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Nome] = @AV60WWEstadoDS_13_Tfestado_nome_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV61WWEstadoDS_14_Tfestado_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Ativo] = 1)";
            }
         }
         if ( AV61WWEstadoDS_14_Tfestado_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Estado_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([Estado_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Estado_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00EM2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (bool)dynConstraints[16] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00EM2 ;
          prmP00EM2 = new Object[] {
          new Object[] {"@lV49WWEstadoDS_2_Estado_uf1",SqlDbType.Char,2,0} ,
          new Object[] {"@lV50WWEstadoDS_3_Estado_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWEstadoDS_6_Estado_uf2",SqlDbType.Char,2,0} ,
          new Object[] {"@lV54WWEstadoDS_7_Estado_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWEstadoDS_10_Estado_uf3",SqlDbType.Char,2,0} ,
          new Object[] {"@lV58WWEstadoDS_11_Estado_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV59WWEstadoDS_12_Tfestado_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV60WWEstadoDS_13_Tfestado_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00EM2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00EM2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwestadofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwestadofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwestadofilterdata") )
          {
             return  ;
          }
          getwwestadofilterdata worker = new getwwestadofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
