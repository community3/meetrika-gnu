/*
               File: WWContratoServicosIndicadorFaixas
        Description:  Contrato Servicos Indicador Faixas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 18:58:26.74
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwcontratoservicosindicadorfaixas : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwcontratoservicosindicadorfaixas( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwcontratoservicosindicadorfaixas( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_85 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_85_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_85_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoServicosIndicadorFaixa_Numero1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicadorFaixa_Numero1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicadorFaixa_Numero1), 4, 0)));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21ContratoServicosIndicadorFaixa_Numero2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicadorFaixa_Numero2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosIndicadorFaixa_Numero2), 4, 0)));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25ContratoServicosIndicadorFaixa_Numero3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicadorFaixa_Numero3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosIndicadorFaixa_Numero3), 4, 0)));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV46Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicadorFaixa_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicadorFaixa_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicadorFaixa_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV46Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAJP2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTJP2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203118582688");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwcontratoservicosindicadorfaixas.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17ContratoServicosIndicadorFaixa_Numero1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21ContratoServicosIndicadorFaixa_Numero2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25ContratoServicosIndicadorFaixa_Numero3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_85", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_85), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV46Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEJP2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTJP2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwcontratoservicosindicadorfaixas.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWContratoServicosIndicadorFaixas" ;
      }

      public override String GetPgmdesc( )
      {
         return " Contrato Servicos Indicador Faixas" ;
      }

      protected void WBJP0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_JP2( true) ;
         }
         else
         {
            wb_table1_2_JP2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_JP2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'" + sGXsfl_85_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(96, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_85_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(97, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"");
         }
         wbLoad = true;
      }

      protected void STARTJP2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Contrato Servicos Indicador Faixas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPJP0( ) ;
      }

      protected void WSJP2( )
      {
         STARTJP2( ) ;
         EVTJP2( ) ;
      }

      protected void EVTJP2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11JP2 */
                              E11JP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12JP2 */
                              E12JP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13JP2 */
                              E13JP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14JP2 */
                              E14JP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15JP2 */
                              E15JP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16JP2 */
                              E16JP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17JP2 */
                              E17JP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18JP2 */
                              E18JP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19JP2 */
                              E19JP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20JP2 */
                              E20JP2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
                              AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
                              AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 = AV17ContratoServicosIndicadorFaixa_Numero1;
                              AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
                              AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
                              AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
                              AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 = AV21ContratoServicosIndicadorFaixa_Numero2;
                              AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
                              AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
                              AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
                              AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 = AV25ContratoServicosIndicadorFaixa_Numero3;
                              sEvt = cgiGet( "GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_85_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
                              SubsflControlProps_852( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV44Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV45Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A1269ContratoServicosIndicador_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicador_Codigo_Internalname), ",", "."));
                              A1299ContratoServicosIndicadorFaixa_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Codigo_Internalname), ",", "."));
                              A1300ContratoServicosIndicadorFaixa_Numero = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Numero_Internalname), ",", "."));
                              A1301ContratoServicosIndicadorFaixa_Desde = context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Desde_Internalname), ",", ".");
                              A1302ContratoServicosIndicadorFaixa_Ate = context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Ate_Internalname), ",", ".");
                              A1303ContratoServicosIndicadorFaixa_Reduz = context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Reduz_Internalname), ",", ".");
                              A1304ContratoServicosIndicadorFaixa_Und = (short)(context.localUtil.CToN( cgiGet( edtContratoServicosIndicadorFaixa_Und_Internalname), ",", "."));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E21JP2 */
                                    E21JP2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E22JP2 */
                                    E22JP2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E23JP2 */
                                    E23JP2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoservicosindicadorfaixa_numero1 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1"), ",", ".") != Convert.ToDecimal( AV17ContratoServicosIndicadorFaixa_Numero1 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoservicosindicadorfaixa_numero2 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2"), ",", ".") != Convert.ToDecimal( AV21ContratoServicosIndicadorFaixa_Numero2 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Contratoservicosindicadorfaixa_numero3 Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3"), ",", ".") != Convert.ToDecimal( AV25ContratoServicosIndicadorFaixa_Numero3 )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEJP2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAJP2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("CONTRATOSERVICOSINDICADORFAIXA_NUMERO", "N�mero", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("CONTRATOSERVICOSINDICADORFAIXA_NUMERO", "N�mero", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("CONTRATOSERVICOSINDICADORFAIXA_NUMERO", "N�mero", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator3.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_852( ) ;
         while ( nGXsfl_85_idx <= nRC_GXsfl_85 )
         {
            sendrow_852( ) ;
            nGXsfl_85_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_85_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_85_idx+1));
            sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
            SubsflControlProps_852( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       short AV17ContratoServicosIndicadorFaixa_Numero1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       short AV21ContratoServicosIndicadorFaixa_Numero2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       short AV25ContratoServicosIndicadorFaixa_Numero3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV46Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFJP2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADOR_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_NUMERO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_DESDE", GetSecureSignedToken( "", context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_DESDE", StringUtil.LTrim( StringUtil.NToC( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_ATE", GetSecureSignedToken( "", context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_ATE", StringUtil.LTrim( StringUtil.NToC( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_REDUZ", GetSecureSignedToken( "", context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_REDUZ", StringUtil.LTrim( StringUtil.NToC( A1303ContratoServicosIndicadorFaixa_Reduz, 6, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_UND", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOSINDICADORFAIXA_UND", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJP2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV46Pgmname = "WWContratoServicosIndicadorFaixas";
         context.Gx_err = 0;
      }

      protected void RFJP2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 85;
         /* Execute user event: E22JP2 */
         E22JP2 ();
         nGXsfl_85_idx = 1;
         sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
         SubsflControlProps_852( ) ;
         nGXsfl_85_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_852( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 ,
                                                 AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 ,
                                                 AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 ,
                                                 AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 ,
                                                 AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 ,
                                                 AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 ,
                                                 AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 ,
                                                 AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 ,
                                                 AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 ,
                                                 AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 ,
                                                 AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 ,
                                                 A1300ContratoServicosIndicadorFaixa_Numero ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00JP2 */
            pr_default.execute(0, new Object[] {AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1, AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1, AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1, AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2, AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2, AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2, AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3, AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3, AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_85_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1304ContratoServicosIndicadorFaixa_Und = H00JP2_A1304ContratoServicosIndicadorFaixa_Und[0];
               A1303ContratoServicosIndicadorFaixa_Reduz = H00JP2_A1303ContratoServicosIndicadorFaixa_Reduz[0];
               A1302ContratoServicosIndicadorFaixa_Ate = H00JP2_A1302ContratoServicosIndicadorFaixa_Ate[0];
               A1301ContratoServicosIndicadorFaixa_Desde = H00JP2_A1301ContratoServicosIndicadorFaixa_Desde[0];
               A1300ContratoServicosIndicadorFaixa_Numero = H00JP2_A1300ContratoServicosIndicadorFaixa_Numero[0];
               A1299ContratoServicosIndicadorFaixa_Codigo = H00JP2_A1299ContratoServicosIndicadorFaixa_Codigo[0];
               A1269ContratoServicosIndicador_Codigo = H00JP2_A1269ContratoServicosIndicador_Codigo[0];
               /* Execute user event: E23JP2 */
               E23JP2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 85;
            WBJP0( ) ;
         }
         nGXsfl_85_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 = AV17ContratoServicosIndicadorFaixa_Numero1;
         AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 = AV21ContratoServicosIndicadorFaixa_Numero2;
         AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 = AV25ContratoServicosIndicadorFaixa_Numero3;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 ,
                                              AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 ,
                                              AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 ,
                                              AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 ,
                                              AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 ,
                                              AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 ,
                                              AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 ,
                                              AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 ,
                                              AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 ,
                                              AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 ,
                                              AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 ,
                                              A1300ContratoServicosIndicadorFaixa_Numero ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00JP3 */
         pr_default.execute(1, new Object[] {AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1, AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1, AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1, AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2, AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2, AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2, AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3, AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3, AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3});
         GRID_nRecordCount = H00JP3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 = AV17ContratoServicosIndicadorFaixa_Numero1;
         AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 = AV21ContratoServicosIndicadorFaixa_Numero2;
         AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 = AV25ContratoServicosIndicadorFaixa_Numero3;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicadorFaixa_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicadorFaixa_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicadorFaixa_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV46Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 = AV17ContratoServicosIndicadorFaixa_Numero1;
         AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 = AV21ContratoServicosIndicadorFaixa_Numero2;
         AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 = AV25ContratoServicosIndicadorFaixa_Numero3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicadorFaixa_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicadorFaixa_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicadorFaixa_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV46Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 = AV17ContratoServicosIndicadorFaixa_Numero1;
         AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 = AV21ContratoServicosIndicadorFaixa_Numero2;
         AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 = AV25ContratoServicosIndicadorFaixa_Numero3;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicadorFaixa_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicadorFaixa_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicadorFaixa_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV46Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 = AV17ContratoServicosIndicadorFaixa_Numero1;
         AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 = AV21ContratoServicosIndicadorFaixa_Numero2;
         AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 = AV25ContratoServicosIndicadorFaixa_Numero3;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicadorFaixa_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicadorFaixa_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicadorFaixa_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV46Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 = AV17ContratoServicosIndicadorFaixa_Numero1;
         AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 = AV21ContratoServicosIndicadorFaixa_Numero2;
         AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 = AV25ContratoServicosIndicadorFaixa_Numero3;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicadorFaixa_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicadorFaixa_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicadorFaixa_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV46Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPJP0( )
      {
         /* Before Start, stand alone formulas. */
         AV46Pgmname = "WWContratoServicosIndicadorFaixas";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E21JP2 */
         E21JP2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosindicadorfaixa_numero1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosindicadorfaixa_numero1_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1");
               GX_FocusControl = edtavContratoservicosindicadorfaixa_numero1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17ContratoServicosIndicadorFaixa_Numero1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicadorFaixa_Numero1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicadorFaixa_Numero1), 4, 0)));
            }
            else
            {
               AV17ContratoServicosIndicadorFaixa_Numero1 = (short)(context.localUtil.CToN( cgiGet( edtavContratoservicosindicadorfaixa_numero1_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicadorFaixa_Numero1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicadorFaixa_Numero1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosindicadorfaixa_numero2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosindicadorfaixa_numero2_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2");
               GX_FocusControl = edtavContratoservicosindicadorfaixa_numero2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21ContratoServicosIndicadorFaixa_Numero2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicadorFaixa_Numero2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosIndicadorFaixa_Numero2), 4, 0)));
            }
            else
            {
               AV21ContratoServicosIndicadorFaixa_Numero2 = (short)(context.localUtil.CToN( cgiGet( edtavContratoservicosindicadorfaixa_numero2_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicadorFaixa_Numero2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosIndicadorFaixa_Numero2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosindicadorfaixa_numero3_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicosindicadorfaixa_numero3_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3");
               GX_FocusControl = edtavContratoservicosindicadorfaixa_numero3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25ContratoServicosIndicadorFaixa_Numero3 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicadorFaixa_Numero3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosIndicadorFaixa_Numero3), 4, 0)));
            }
            else
            {
               AV25ContratoServicosIndicadorFaixa_Numero3 = (short)(context.localUtil.CToN( cgiGet( edtavContratoservicosindicadorfaixa_numero3_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicadorFaixa_Numero3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosIndicadorFaixa_Numero3), 4, 0)));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            /* Read saved values. */
            nRC_GXsfl_85 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_85"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1"), ",", ".") != Convert.ToDecimal( AV17ContratoServicosIndicadorFaixa_Numero1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2"), ",", ".") != Convert.ToDecimal( AV21ContratoServicosIndicadorFaixa_Numero2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3"), ",", ".") != Convert.ToDecimal( AV25ContratoServicosIndicadorFaixa_Numero3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E21JP2 */
         E21JP2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21JP2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         Form.Caption = " Contrato Servicos Indicador Faixas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "N�mero", 0);
         cmbavOrderedby.addItem("2", "Inicio", 0);
         cmbavOrderedby.addItem("3", "Fim", 0);
         cmbavOrderedby.addItem("4", "Reduz", 0);
         cmbavOrderedby.addItem("5", "", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
      }

      protected void E22JP2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicosIndicadorFaixa_Numero_Titleformat = 2;
         edtContratoServicosIndicadorFaixa_Numero_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV13OrderedBy==1) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "N�mero", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Numero_Internalname, "Title", edtContratoServicosIndicadorFaixa_Numero_Title);
         edtContratoServicosIndicadorFaixa_Desde_Titleformat = 2;
         edtContratoServicosIndicadorFaixa_Desde_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Inicio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Desde_Internalname, "Title", edtContratoServicosIndicadorFaixa_Desde_Title);
         edtContratoServicosIndicadorFaixa_Ate_Titleformat = 2;
         edtContratoServicosIndicadorFaixa_Ate_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Fim", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Ate_Internalname, "Title", edtContratoServicosIndicadorFaixa_Ate_Title);
         edtContratoServicosIndicadorFaixa_Reduz_Titleformat = 2;
         edtContratoServicosIndicadorFaixa_Reduz_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Reduz", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Reduz_Internalname, "Title", edtContratoServicosIndicadorFaixa_Reduz_Title);
         edtContratoServicosIndicadorFaixa_Und_Titleformat = 2;
         edtContratoServicosIndicadorFaixa_Und_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV13OrderedBy==5) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoServicosIndicadorFaixa_Und_Internalname, "Title", edtContratoServicosIndicadorFaixa_Und_Title);
         AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 = AV16DynamicFiltersOperator1;
         AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 = AV17ContratoServicosIndicadorFaixa_Numero1;
         AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 = AV20DynamicFiltersOperator2;
         AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 = AV21ContratoServicosIndicadorFaixa_Numero2;
         AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 = AV24DynamicFiltersOperator3;
         AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 = AV25ContratoServicosIndicadorFaixa_Numero3;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      private void E23JP2( )
      {
         /* Grid_Load Routine */
         AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
         AV44Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
         AV45Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 85;
         }
         sendrow_852( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_85_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(85, GridRow);
         }
      }

      protected void E11JP2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E16JP2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E12JP2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicadorFaixa_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicadorFaixa_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicadorFaixa_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV46Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E17JP2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18JP2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E13JP2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicadorFaixa_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicadorFaixa_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicadorFaixa_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV46Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E19JP2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14JP2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicadorFaixa_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicadorFaixa_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicadorFaixa_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV46Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E20JP2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15JP2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17ContratoServicosIndicadorFaixa_Numero1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21ContratoServicosIndicadorFaixa_Numero2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25ContratoServicosIndicadorFaixa_Numero3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV46Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavContratoservicosindicadorfaixa_numero1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicadorfaixa_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicadorfaixa_numero1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 )
         {
            edtavContratoservicosindicadorfaixa_numero1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicadorfaixa_numero1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicadorfaixa_numero1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavContratoservicosindicadorfaixa_numero2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicadorfaixa_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicadorfaixa_numero2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 )
         {
            edtavContratoservicosindicadorfaixa_numero2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicadorfaixa_numero2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicadorfaixa_numero2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavContratoservicosindicadorfaixa_numero3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicadorfaixa_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicadorfaixa_numero3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 )
         {
            edtavContratoservicosindicadorfaixa_numero3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicosindicadorfaixa_numero3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicosindicadorfaixa_numero3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S152( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "CONTRATOSERVICOSINDICADORFAIXA_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21ContratoServicosIndicadorFaixa_Numero2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicadorFaixa_Numero2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosIndicadorFaixa_Numero2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "CONTRATOSERVICOSINDICADORFAIXA_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25ContratoServicosIndicadorFaixa_Numero3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicadorFaixa_Numero3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosIndicadorFaixa_Numero3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV15DynamicFiltersSelector1 = "CONTRATOSERVICOSINDICADORFAIXA_NUMERO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17ContratoServicosIndicadorFaixa_Numero1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicadorFaixa_Numero1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicadorFaixa_Numero1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV46Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV46Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV46Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S162( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17ContratoServicosIndicadorFaixa_Numero1 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ContratoServicosIndicadorFaixa_Numero1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17ContratoServicosIndicadorFaixa_Numero1), 4, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21ContratoServicosIndicadorFaixa_Numero2 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21ContratoServicosIndicadorFaixa_Numero2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21ContratoServicosIndicadorFaixa_Numero2), 4, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S182 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25ContratoServicosIndicadorFaixa_Numero3 = (short)(NumberUtil.Val( AV12GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25ContratoServicosIndicadorFaixa_Numero3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25ContratoServicosIndicadorFaixa_Numero3), 4, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S192 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV46Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV46Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_Meetrika")) ;
      }

      protected void S142( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ! (0==AV17ContratoServicosIndicadorFaixa_Numero1) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV17ContratoServicosIndicadorFaixa_Numero1), 4, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ! (0==AV21ContratoServicosIndicadorFaixa_Numero2) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV21ContratoServicosIndicadorFaixa_Numero2), 4, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ! (0==AV25ContratoServicosIndicadorFaixa_Numero3) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV25ContratoServicosIndicadorFaixa_Numero3), 4, 0);
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV46Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContratoServicosIndicadorFaixas";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_JP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_JP2( true) ;
         }
         else
         {
            wb_table2_8_JP2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_JP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_82_JP2( true) ;
         }
         else
         {
            wb_table3_82_JP2( false) ;
         }
         return  ;
      }

      protected void wb_table3_82_JP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_JP2e( true) ;
         }
         else
         {
            wb_table1_2_JP2e( false) ;
         }
      }

      protected void wb_table3_82_JP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"85\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(46), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(51), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicadorFaixa_Numero_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicadorFaixa_Numero_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicadorFaixa_Numero_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(58), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicadorFaixa_Desde_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicadorFaixa_Desde_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicadorFaixa_Desde_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(58), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicadorFaixa_Ate_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicadorFaixa_Ate_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicadorFaixa_Ate_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(74), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicadorFaixa_Reduz_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicadorFaixa_Reduz_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicadorFaixa_Reduz_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosIndicadorFaixa_Und_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosIndicadorFaixa_Und_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosIndicadorFaixa_Und_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Numero_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Numero_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Desde_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Desde_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Ate_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Ate_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1303ContratoServicosIndicadorFaixa_Reduz, 8, 2, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Reduz_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Reduz_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosIndicadorFaixa_Und_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosIndicadorFaixa_Und_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 85 )
         {
            wbEnd = 0;
            nRC_GXsfl_85 = (short)(nGXsfl_85_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_82_JP2e( true) ;
         }
         else
         {
            wb_table3_82_JP2e( false) ;
         }
      }

      protected void wb_table2_8_JP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContratoservicosindicadorfaixastitle_Internalname, "Faixas", "", "", lblContratoservicosindicadorfaixastitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_13_JP2( true) ;
         }
         else
         {
            wb_table4_13_JP2( false) ;
         }
         return  ;
      }

      protected void wb_table4_13_JP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenar por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWContratoServicosIndicadorFaixas.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_23_JP2( true) ;
         }
         else
         {
            wb_table5_23_JP2( false) ;
         }
         return  ;
      }

      protected void wb_table5_23_JP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_JP2e( true) ;
         }
         else
         {
            wb_table2_8_JP2e( false) ;
         }
      }

      protected void wb_table5_23_JP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_28_JP2( true) ;
         }
         else
         {
            wb_table6_28_JP2( false) ;
         }
         return  ;
      }

      protected void wb_table6_28_JP2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_23_JP2e( true) ;
         }
         else
         {
            wb_table5_23_JP2e( false) ;
         }
      }

      protected void wb_table6_28_JP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Procurar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWContratoServicosIndicadorFaixas.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_37_JP2( true) ;
         }
         else
         {
            wb_table7_37_JP2( false) ;
         }
         return  ;
      }

      protected void wb_table7_37_JP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Add filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosIndicadorFaixas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remove filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Procurar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_WWContratoServicosIndicadorFaixas.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_54_JP2( true) ;
         }
         else
         {
            wb_table8_54_JP2( false) ;
         }
         return  ;
      }

      protected void wb_table8_54_JP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Add filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosIndicadorFaixas.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remove filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Procurar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_WWContratoServicosIndicadorFaixas.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_71_JP2( true) ;
         }
         else
         {
            wb_table9_71_JP2( false) ;
         }
         return  ;
      }

      protected void wb_table9_71_JP2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remove filter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_28_JP2e( true) ;
         }
         else
         {
            wb_table6_28_JP2e( false) ;
         }
      }

      protected void wb_table9_71_JP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_WWContratoServicosIndicadorFaixas.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosindicadorfaixa_numero3_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25ContratoServicosIndicadorFaixa_Numero3), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV25ContratoServicosIndicadorFaixa_Numero3), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosindicadorfaixa_numero3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosindicadorfaixa_numero3_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_71_JP2e( true) ;
         }
         else
         {
            wb_table9_71_JP2e( false) ;
         }
      }

      protected void wb_table8_54_JP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,57);\"", "", true, "HLP_WWContratoServicosIndicadorFaixas.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosindicadorfaixa_numero2_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21ContratoServicosIndicadorFaixa_Numero2), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV21ContratoServicosIndicadorFaixa_Numero2), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosindicadorfaixa_numero2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosindicadorfaixa_numero2_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_54_JP2e( true) ;
         }
         else
         {
            wb_table8_54_JP2e( false) ;
         }
      }

      protected void wb_table7_37_JP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_85_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWContratoServicosIndicadorFaixas.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicosindicadorfaixa_numero1_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17ContratoServicosIndicadorFaixa_Numero1), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV17ContratoServicosIndicadorFaixa_Numero1), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicosindicadorfaixa_numero1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavContratoservicosindicadorfaixa_numero1_Visible, 1, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_37_JP2e( true) ;
         }
         else
         {
            wb_table7_37_JP2e( false) ;
         }
      }

      protected void wb_table4_13_JP2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"e24jp1_client"+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWContratoServicosIndicadorFaixas.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_JP2e( true) ;
         }
         else
         {
            wb_table4_13_JP2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJP2( ) ;
         WSJP2( ) ;
         WEJP2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203118582852");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("wwcontratoservicosindicadorfaixas.js", "?20203118582852");
            context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_852( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_85_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_85_idx;
         edtContratoServicosIndicador_Codigo_Internalname = "CONTRATOSERVICOSINDICADOR_CODIGO_"+sGXsfl_85_idx;
         edtContratoServicosIndicadorFaixa_Codigo_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_CODIGO_"+sGXsfl_85_idx;
         edtContratoServicosIndicadorFaixa_Numero_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_NUMERO_"+sGXsfl_85_idx;
         edtContratoServicosIndicadorFaixa_Desde_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_85_idx;
         edtContratoServicosIndicadorFaixa_Ate_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_ATE_"+sGXsfl_85_idx;
         edtContratoServicosIndicadorFaixa_Reduz_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_REDUZ_"+sGXsfl_85_idx;
         edtContratoServicosIndicadorFaixa_Und_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_UND_"+sGXsfl_85_idx;
      }

      protected void SubsflControlProps_fel_852( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_85_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_85_fel_idx;
         edtContratoServicosIndicador_Codigo_Internalname = "CONTRATOSERVICOSINDICADOR_CODIGO_"+sGXsfl_85_fel_idx;
         edtContratoServicosIndicadorFaixa_Codigo_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_CODIGO_"+sGXsfl_85_fel_idx;
         edtContratoServicosIndicadorFaixa_Numero_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_NUMERO_"+sGXsfl_85_fel_idx;
         edtContratoServicosIndicadorFaixa_Desde_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_DESDE_"+sGXsfl_85_fel_idx;
         edtContratoServicosIndicadorFaixa_Ate_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_ATE_"+sGXsfl_85_fel_idx;
         edtContratoServicosIndicadorFaixa_Reduz_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_REDUZ_"+sGXsfl_85_fel_idx;
         edtContratoServicosIndicadorFaixa_Und_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_UND_"+sGXsfl_85_fel_idx;
      }

      protected void sendrow_852( )
      {
         SubsflControlProps_852( ) ;
         WBJP0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_85_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_85_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_85_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV44Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV44Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)0,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV45Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV45Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)0,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicador_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1269ContratoServicosIndicador_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicador_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Numero_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Numero_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)51,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Desde_Internalname,StringUtil.LTrim( StringUtil.NToC( A1301ContratoServicosIndicadorFaixa_Desde, 6, 2, ",", "")),context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Desde_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)58,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Ate_Internalname,StringUtil.LTrim( StringUtil.NToC( A1302ContratoServicosIndicadorFaixa_Ate, 6, 2, ",", "")),context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Ate_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)58,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Reduz_Internalname,StringUtil.LTrim( StringUtil.NToC( A1303ContratoServicosIndicadorFaixa_Reduz, 8, 2, ",", "")),context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Reduz_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)74,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"Percentual",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosIndicadorFaixa_Und_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), "ZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosIndicadorFaixa_Und_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADOR_CODIGO"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, context.localUtil.Format( (decimal)(A1269ContratoServicosIndicador_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_CODIGO"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, context.localUtil.Format( (decimal)(A1299ContratoServicosIndicadorFaixa_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_NUMERO"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, context.localUtil.Format( (decimal)(A1300ContratoServicosIndicadorFaixa_Numero), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_DESDE"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, context.localUtil.Format( A1301ContratoServicosIndicadorFaixa_Desde, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_ATE"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, context.localUtil.Format( A1302ContratoServicosIndicadorFaixa_Ate, "ZZ9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_REDUZ"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, context.localUtil.Format( A1303ContratoServicosIndicadorFaixa_Reduz, "ZZ9.99 %")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATOSERVICOSINDICADORFAIXA_UND"+"_"+sGXsfl_85_idx, GetSecureSignedToken( sGXsfl_85_idx, context.localUtil.Format( (decimal)(A1304ContratoServicosIndicadorFaixa_Und), "ZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_85_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_85_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_85_idx+1));
            sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
            SubsflControlProps_852( ) ;
         }
         /* End function sendrow_852 */
      }

      protected void init_default_properties( )
      {
         lblContratoservicosindicadorfaixastitle_Internalname = "CONTRATOSERVICOSINDICADORFAIXASTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavContratoservicosindicadorfaixa_numero1_Internalname = "vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavContratoservicosindicadorfaixa_numero2_Internalname = "vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavContratoservicosindicadorfaixa_numero3_Internalname = "vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtContratoServicosIndicador_Codigo_Internalname = "CONTRATOSERVICOSINDICADOR_CODIGO";
         edtContratoServicosIndicadorFaixa_Codigo_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_CODIGO";
         edtContratoServicosIndicadorFaixa_Numero_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_NUMERO";
         edtContratoServicosIndicadorFaixa_Desde_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_DESDE";
         edtContratoServicosIndicadorFaixa_Ate_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_ATE";
         edtContratoServicosIndicadorFaixa_Reduz_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_REDUZ";
         edtContratoServicosIndicadorFaixa_Und_Internalname = "CONTRATOSERVICOSINDICADORFAIXA_UND";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtContratoServicosIndicadorFaixa_Und_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Reduz_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Ate_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Desde_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Numero_Jsonclick = "";
         edtContratoServicosIndicadorFaixa_Codigo_Jsonclick = "";
         edtContratoServicosIndicador_Codigo_Jsonclick = "";
         edtavContratoservicosindicadorfaixa_numero1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavContratoservicosindicadorfaixa_numero2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavContratoservicosindicadorfaixa_numero3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavUpdate_Tooltiptext = "Modifica";
         edtContratoServicosIndicadorFaixa_Und_Titleformat = 0;
         edtContratoServicosIndicadorFaixa_Reduz_Titleformat = 0;
         edtContratoServicosIndicadorFaixa_Ate_Titleformat = 0;
         edtContratoServicosIndicadorFaixa_Desde_Titleformat = 0;
         edtContratoServicosIndicadorFaixa_Numero_Titleformat = 0;
         subGrid_Class = "WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavContratoservicosindicadorfaixa_numero3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavContratoservicosindicadorfaixa_numero2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavContratoservicosindicadorfaixa_numero1_Visible = 1;
         edtContratoServicosIndicadorFaixa_Und_Title = "";
         edtContratoServicosIndicadorFaixa_Reduz_Title = "Reduz";
         edtContratoServicosIndicadorFaixa_Ate_Title = "Fim";
         edtContratoServicosIndicadorFaixa_Desde_Title = "Inicio";
         edtContratoServicosIndicadorFaixa_Numero_Title = "N�mero";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Contrato Servicos Indicador Faixas";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'edtContratoServicosIndicadorFaixa_Numero_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Numero_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Desde_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Desde_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Ate_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Ate_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Und_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_UND',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Und_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_UND',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID.LOAD","{handler:'E23JP2',iparms:[],oparms:[{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E11JP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E16JP2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E12JP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosindicadorfaixa_numero2_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosindicadorfaixa_numero3_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosindicadorfaixa_numero1_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E17JP2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosindicadorfaixa_numero1_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E18JP2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E13JP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosindicadorfaixa_numero2_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosindicadorfaixa_numero3_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosindicadorfaixa_numero1_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E19JP2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosindicadorfaixa_numero2_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E14JP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosindicadorfaixa_numero2_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosindicadorfaixa_numero3_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavContratoservicosindicadorfaixa_numero1_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E20JP2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavContratoservicosindicadorfaixa_numero3_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E15JP2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavContratoservicosindicadorfaixa_numero1_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'edtavContratoservicosindicadorfaixa_numero2_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavContratoservicosindicadorfaixa_numero3_Visible',ctrl:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E24JP1',iparms:[],oparms:[]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'edtContratoServicosIndicadorFaixa_Numero_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Numero_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Desde_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Desde_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Ate_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Ate_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Und_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_UND',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Und_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_UND',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'edtContratoServicosIndicadorFaixa_Numero_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Numero_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Desde_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Desde_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Ate_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Ate_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Und_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_UND',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Und_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_UND',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'edtContratoServicosIndicadorFaixa_Numero_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Numero_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Desde_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Desde_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Ate_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Ate_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Und_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_UND',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Und_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_UND',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17ContratoServicosIndicadorFaixa_Numero1',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO1',pic:'ZZZ9',nv:0},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21ContratoServicosIndicadorFaixa_Numero2',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25ContratoServicosIndicadorFaixa_Numero3',fld:'vCONTRATOSERVICOSINDICADORFAIXA_NUMERO3',pic:'ZZZ9',nv:0},{av:'AV46Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'edtContratoServicosIndicadorFaixa_Numero_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Numero_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_NUMERO',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Desde_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Desde_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_DESDE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Ate_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Ate_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_ATE',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Reduz_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_REDUZ',prop:'Title'},{av:'edtContratoServicosIndicadorFaixa_Und_Titleformat',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_UND',prop:'Titleformat'},{av:'edtContratoServicosIndicadorFaixa_Und_Title',ctrl:'CONTRATOSERVICOSINDICADORFAIXA_UND',prop:'Title'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17ContratoServicosIndicadorFaixa_Numero1 = 0;
         AV19DynamicFiltersSelector2 = "";
         AV21ContratoServicosIndicadorFaixa_Numero2 = 0;
         AV23DynamicFiltersSelector3 = "";
         AV25ContratoServicosIndicadorFaixa_Numero3 = 0;
         AV46Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 = "";
         AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 = "";
         AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 = "";
         AV28Update = "";
         AV44Update_GXI = "";
         AV29Delete = "";
         AV45Delete_GXI = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00JP2_A1304ContratoServicosIndicadorFaixa_Und = new short[1] ;
         H00JP2_A1303ContratoServicosIndicadorFaixa_Reduz = new decimal[1] ;
         H00JP2_A1302ContratoServicosIndicadorFaixa_Ate = new decimal[1] ;
         H00JP2_A1301ContratoServicosIndicadorFaixa_Desde = new decimal[1] ;
         H00JP2_A1300ContratoServicosIndicadorFaixa_Numero = new short[1] ;
         H00JP2_A1299ContratoServicosIndicadorFaixa_Codigo = new int[1] ;
         H00JP2_A1269ContratoServicosIndicador_Codigo = new int[1] ;
         H00JP3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV30Session = context.GetSession();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblContratoservicosindicadorfaixastitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwcontratoservicosindicadorfaixas__default(),
            new Object[][] {
                new Object[] {
               H00JP2_A1304ContratoServicosIndicadorFaixa_Und, H00JP2_A1303ContratoServicosIndicadorFaixa_Reduz, H00JP2_A1302ContratoServicosIndicadorFaixa_Ate, H00JP2_A1301ContratoServicosIndicadorFaixa_Desde, H00JP2_A1300ContratoServicosIndicadorFaixa_Numero, H00JP2_A1299ContratoServicosIndicadorFaixa_Codigo, H00JP2_A1269ContratoServicosIndicador_Codigo
               }
               , new Object[] {
               H00JP3_AGRID_nRecordCount
               }
            }
         );
         AV46Pgmname = "WWContratoServicosIndicadorFaixas";
         /* GeneXus formulas. */
         AV46Pgmname = "WWContratoServicosIndicadorFaixas";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_85 ;
      private short nGXsfl_85_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV17ContratoServicosIndicadorFaixa_Numero1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV21ContratoServicosIndicadorFaixa_Numero2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short AV25ContratoServicosIndicadorFaixa_Numero3 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 ;
      private short AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 ;
      private short AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 ;
      private short AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 ;
      private short AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 ;
      private short AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 ;
      private short A1300ContratoServicosIndicadorFaixa_Numero ;
      private short A1304ContratoServicosIndicadorFaixa_Und ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_85_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicosIndicadorFaixa_Numero_Titleformat ;
      private short edtContratoServicosIndicadorFaixa_Desde_Titleformat ;
      private short edtContratoServicosIndicadorFaixa_Ate_Titleformat ;
      private short edtContratoServicosIndicadorFaixa_Reduz_Titleformat ;
      private short edtContratoServicosIndicadorFaixa_Und_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A1269ContratoServicosIndicador_Codigo ;
      private int A1299ContratoServicosIndicadorFaixa_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavContratoservicosindicadorfaixa_numero1_Visible ;
      private int edtavContratoservicosindicadorfaixa_numero2_Visible ;
      private int edtavContratoservicosindicadorfaixa_numero3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A1301ContratoServicosIndicadorFaixa_Desde ;
      private decimal A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal A1303ContratoServicosIndicadorFaixa_Reduz ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_85_idx="0001" ;
      private String AV46Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratoServicosIndicador_Codigo_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Codigo_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Numero_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Desde_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Ate_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Reduz_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Und_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavContratoservicosindicadorfaixa_numero1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavContratoservicosindicadorfaixa_numero2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavContratoservicosindicadorfaixa_numero3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtContratoServicosIndicadorFaixa_Numero_Title ;
      private String edtContratoServicosIndicadorFaixa_Desde_Title ;
      private String edtContratoServicosIndicadorFaixa_Ate_Title ;
      private String edtContratoServicosIndicadorFaixa_Reduz_Title ;
      private String edtContratoServicosIndicadorFaixa_Und_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavDelete_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblContratoservicosindicadorfaixastitle_Internalname ;
      private String lblContratoservicosindicadorfaixastitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavContratoservicosindicadorfaixa_numero3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavContratoservicosindicadorfaixa_numero2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavContratoservicosindicadorfaixa_numero1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_85_fel_idx="0001" ;
      private String ROClassString ;
      private String edtContratoServicosIndicador_Codigo_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Codigo_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Numero_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Desde_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Ate_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Reduz_Jsonclick ;
      private String edtContratoServicosIndicadorFaixa_Und_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 ;
      private bool AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 ;
      private String AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 ;
      private String AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 ;
      private String AV44Update_GXI ;
      private String AV45Delete_GXI ;
      private String AV28Update ;
      private String AV29Delete ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private short[] H00JP2_A1304ContratoServicosIndicadorFaixa_Und ;
      private decimal[] H00JP2_A1303ContratoServicosIndicadorFaixa_Reduz ;
      private decimal[] H00JP2_A1302ContratoServicosIndicadorFaixa_Ate ;
      private decimal[] H00JP2_A1301ContratoServicosIndicadorFaixa_Desde ;
      private short[] H00JP2_A1300ContratoServicosIndicadorFaixa_Numero ;
      private int[] H00JP2_A1299ContratoServicosIndicadorFaixa_Codigo ;
      private int[] H00JP2_A1269ContratoServicosIndicador_Codigo ;
      private long[] H00JP3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
   }

   public class wwcontratoservicosindicadorfaixas__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00JP2( IGxContext context ,
                                             String AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 ,
                                             short AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 ,
                                             short AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 ,
                                             bool AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 ,
                                             String AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 ,
                                             short AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 ,
                                             short AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 ,
                                             bool AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 ,
                                             String AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 ,
                                             short AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 ,
                                             short AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 ,
                                             short A1300ContratoServicosIndicadorFaixa_Numero ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [14] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ContratoServicosIndicadorFaixa_Und], [ContratoServicosIndicadorFaixa_Reduz], [ContratoServicosIndicadorFaixa_Ate], [ContratoServicosIndicadorFaixa_Desde], [ContratoServicosIndicadorFaixa_Numero], [ContratoServicosIndicadorFaixa_Codigo], [ContratoServicosIndicador_Codigo]";
         sFromString = " FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] < @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] < @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] = @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] = @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] > @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] > @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] < @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] < @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] = @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] = @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] > @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] > @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] < @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] < @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] = @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] = @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] > @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] > @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosIndicadorFaixa_Numero]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosIndicadorFaixa_Numero] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosIndicadorFaixa_Desde]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosIndicadorFaixa_Desde] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosIndicadorFaixa_Ate]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosIndicadorFaixa_Ate] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosIndicadorFaixa_Reduz]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosIndicadorFaixa_Reduz] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosIndicadorFaixa_Und]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosIndicadorFaixa_Und] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ContratoServicosIndicador_Codigo], [ContratoServicosIndicadorFaixa_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H00JP3( IGxContext context ,
                                             String AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1 ,
                                             short AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 ,
                                             short AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1 ,
                                             bool AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 ,
                                             String AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2 ,
                                             short AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 ,
                                             short AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2 ,
                                             bool AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 ,
                                             String AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3 ,
                                             short AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 ,
                                             short AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3 ,
                                             short A1300ContratoServicosIndicadorFaixa_Numero ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [9] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ContratoServicosIndicadorFaixas] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 == 0 ) && ( ! (0==AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] < @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] < @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 == 1 ) && ( ! (0==AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] = @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] = @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV33WWContratoServicosIndicadorFaixasDS_1_Dynamicfiltersselector1, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV34WWContratoServicosIndicadorFaixasDS_2_Dynamicfiltersoperator1 == 2 ) && ( ! (0==AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] > @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] > @AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 == 0 ) && ( ! (0==AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] < @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] < @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 == 1 ) && ( ! (0==AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] = @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] = @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV36WWContratoServicosIndicadorFaixasDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV37WWContratoServicosIndicadorFaixasDS_5_Dynamicfiltersselector2, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV38WWContratoServicosIndicadorFaixasDS_6_Dynamicfiltersoperator2 == 2 ) && ( ! (0==AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] > @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] > @AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 == 0 ) && ( ! (0==AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] < @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] < @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 == 1 ) && ( ! (0==AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] = @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] = @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV40WWContratoServicosIndicadorFaixasDS_8_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV41WWContratoServicosIndicadorFaixasDS_9_Dynamicfiltersselector3, "CONTRATOSERVICOSINDICADORFAIXA_NUMERO") == 0 ) && ( AV42WWContratoServicosIndicadorFaixasDS_10_Dynamicfiltersoperator3 == 2 ) && ( ! (0==AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ContratoServicosIndicadorFaixa_Numero] > @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
            else
            {
               sWhereString = sWhereString + " ([ContratoServicosIndicadorFaixa_Numero] > @AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00JP2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (short)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] );
               case 1 :
                     return conditional_H00JP3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (short)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (short)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (short)dynConstraints[10] , (short)dynConstraints[11] , (short)dynConstraints[12] , (bool)dynConstraints[13] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00JP2 ;
          prmH00JP2 = new Object[] {
          new Object[] {"@AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00JP3 ;
          prmH00JP3 = new Object[] {
          new Object[] {"@AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV35WWContratoServicosIndicadorFaixasDS_3_Contratoservicosindicadorfaixa_numero1",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV39WWContratoServicosIndicadorFaixasDS_7_Contratoservicosindicadorfaixa_numero2",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV43WWContratoServicosIndicadorFaixasDS_11_Contratoservicosindicadorfaixa_numero3",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00JP2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JP2,11,0,true,false )
             ,new CursorDef("H00JP3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00JP3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((int[]) buf[6])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[14]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[15]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[18]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[19]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[22]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[17]);
                }
                return;
       }
    }

 }

}
