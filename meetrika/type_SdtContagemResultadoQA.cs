/*
               File: type_SdtContagemResultadoQA
        Description: Contagem Resultado QA
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:12:11.54
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ContagemResultadoQA" )]
   [XmlType(TypeName =  "ContagemResultadoQA" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtContagemResultadoQA : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoQA( )
      {
         /* Constructor for serialization */
         gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm = "";
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_texto = "";
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom = "";
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom = "";
         gxTv_SdtContagemResultadoQA_Mode = "";
         gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z = "";
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z = "";
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z = "";
      }

      public SdtContagemResultadoQA( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV1984ContagemResultadoQA_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV1984ContagemResultadoQA_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContagemResultadoQA_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ContagemResultadoQA");
         metadata.Set("BT", "ContagemResultadoQA");
         metadata.Set("PK", "[ \"ContagemResultadoQA_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemResultadoQA_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContagemResultadoQA_Codigo\" ],\"FKMap\":[ \"ContagemResultadoQA_RespostaDe-ContagemResultadoQA_Codigo\" ] },{ \"FK\":[ \"ContagemResultado_Codigo\" ],\"FKMap\":[ \"ContagemResultadoQA_OSCod-ContagemResultado_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContagemResultadoQA_ParaCod-Usuario_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContagemResultadoQA_UserCod-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_oscod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demandafm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_datahora_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_usercod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_userpescod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_userpesnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_contratantedouser_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_paracod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_parapescod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_parapesnom_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_contratantedopara_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_respostade_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_resposta_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demandafm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_userpescod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_userpesnom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_parapescod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_parapesnom_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultadoqa_respostade_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtContagemResultadoQA deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtContagemResultadoQA)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtContagemResultadoQA obj ;
         obj = this;
         obj.gxTpr_Contagemresultadoqa_codigo = deserialized.gxTpr_Contagemresultadoqa_codigo;
         obj.gxTpr_Contagemresultadoqa_oscod = deserialized.gxTpr_Contagemresultadoqa_oscod;
         obj.gxTpr_Contagemresultado_demandafm = deserialized.gxTpr_Contagemresultado_demandafm;
         obj.gxTpr_Contagemresultadoqa_datahora = deserialized.gxTpr_Contagemresultadoqa_datahora;
         obj.gxTpr_Contagemresultadoqa_texto = deserialized.gxTpr_Contagemresultadoqa_texto;
         obj.gxTpr_Contagemresultadoqa_usercod = deserialized.gxTpr_Contagemresultadoqa_usercod;
         obj.gxTpr_Contagemresultadoqa_userpescod = deserialized.gxTpr_Contagemresultadoqa_userpescod;
         obj.gxTpr_Contagemresultadoqa_userpesnom = deserialized.gxTpr_Contagemresultadoqa_userpesnom;
         obj.gxTpr_Contagemresultadoqa_contratantedouser = deserialized.gxTpr_Contagemresultadoqa_contratantedouser;
         obj.gxTpr_Contagemresultadoqa_paracod = deserialized.gxTpr_Contagemresultadoqa_paracod;
         obj.gxTpr_Contagemresultadoqa_parapescod = deserialized.gxTpr_Contagemresultadoqa_parapescod;
         obj.gxTpr_Contagemresultadoqa_parapesnom = deserialized.gxTpr_Contagemresultadoqa_parapesnom;
         obj.gxTpr_Contagemresultadoqa_contratantedopara = deserialized.gxTpr_Contagemresultadoqa_contratantedopara;
         obj.gxTpr_Contagemresultadoqa_respostade = deserialized.gxTpr_Contagemresultadoqa_respostade;
         obj.gxTpr_Contagemresultadoqa_resposta = deserialized.gxTpr_Contagemresultadoqa_resposta;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemresultadoqa_codigo_Z = deserialized.gxTpr_Contagemresultadoqa_codigo_Z;
         obj.gxTpr_Contagemresultadoqa_oscod_Z = deserialized.gxTpr_Contagemresultadoqa_oscod_Z;
         obj.gxTpr_Contagemresultado_demandafm_Z = deserialized.gxTpr_Contagemresultado_demandafm_Z;
         obj.gxTpr_Contagemresultadoqa_datahora_Z = deserialized.gxTpr_Contagemresultadoqa_datahora_Z;
         obj.gxTpr_Contagemresultadoqa_usercod_Z = deserialized.gxTpr_Contagemresultadoqa_usercod_Z;
         obj.gxTpr_Contagemresultadoqa_userpescod_Z = deserialized.gxTpr_Contagemresultadoqa_userpescod_Z;
         obj.gxTpr_Contagemresultadoqa_userpesnom_Z = deserialized.gxTpr_Contagemresultadoqa_userpesnom_Z;
         obj.gxTpr_Contagemresultadoqa_contratantedouser_Z = deserialized.gxTpr_Contagemresultadoqa_contratantedouser_Z;
         obj.gxTpr_Contagemresultadoqa_paracod_Z = deserialized.gxTpr_Contagemresultadoqa_paracod_Z;
         obj.gxTpr_Contagemresultadoqa_parapescod_Z = deserialized.gxTpr_Contagemresultadoqa_parapescod_Z;
         obj.gxTpr_Contagemresultadoqa_parapesnom_Z = deserialized.gxTpr_Contagemresultadoqa_parapesnom_Z;
         obj.gxTpr_Contagemresultadoqa_contratantedopara_Z = deserialized.gxTpr_Contagemresultadoqa_contratantedopara_Z;
         obj.gxTpr_Contagemresultadoqa_respostade_Z = deserialized.gxTpr_Contagemresultadoqa_respostade_Z;
         obj.gxTpr_Contagemresultadoqa_resposta_Z = deserialized.gxTpr_Contagemresultadoqa_resposta_Z;
         obj.gxTpr_Contagemresultado_demandafm_N = deserialized.gxTpr_Contagemresultado_demandafm_N;
         obj.gxTpr_Contagemresultadoqa_userpescod_N = deserialized.gxTpr_Contagemresultadoqa_userpescod_N;
         obj.gxTpr_Contagemresultadoqa_userpesnom_N = deserialized.gxTpr_Contagemresultadoqa_userpesnom_N;
         obj.gxTpr_Contagemresultadoqa_parapescod_N = deserialized.gxTpr_Contagemresultadoqa_parapescod_N;
         obj.gxTpr_Contagemresultadoqa_parapesnom_N = deserialized.gxTpr_Contagemresultadoqa_parapesnom_N;
         obj.gxTpr_Contagemresultadoqa_respostade_N = deserialized.gxTpr_Contagemresultadoqa_respostade_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_Codigo") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_OSCod") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_DataHora") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_Texto") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_texto = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_UserCod") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_UserPesCod") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_UserPesNom") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ContratanteDoUser") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ParaCod") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ParaPesCod") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ParaPesNom") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ContratanteDoPara") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_RespostaDe") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_Resposta") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtContagemResultadoQA_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtContagemResultadoQA_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_Codigo_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_OSCod_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_DataHora_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_UserCod_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_UserPesCod_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_UserPesNom_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ContratanteDoUser_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ParaCod_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ParaPesCod_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ParaPesNom_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ContratanteDoPara_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_RespostaDe_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_Resposta_Z") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM_N") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_UserPesCod_N") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_UserPesNom_N") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ParaPesCod_N") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_ParaPesNom_N") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultadoQA_RespostaDe_N") )
               {
                  gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ContagemResultadoQA";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultadoQA_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoQA_OSCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultado_DemandaFM", StringUtil.RTrim( gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora) )
         {
            oWriter.WriteStartElement("ContagemResultadoQA_DataHora");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultadoQA_DataHora", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("ContagemResultadoQA_Texto", StringUtil.RTrim( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_texto));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoQA_UserCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoQA_UserPesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoQA_UserPesNom", StringUtil.RTrim( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoQA_ContratanteDoUser", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoQA_ParaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoQA_ParaPesCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoQA_ParaPesNom", StringUtil.RTrim( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoQA_ContratanteDoPara", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoQA_RespostaDe", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("ContagemResultadoQA_Resposta", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtContagemResultadoQA_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_OSCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultado_DemandaFM_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z) )
            {
               oWriter.WriteStartElement("ContagemResultadoQA_DataHora_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultadoQA_DataHora_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("ContagemResultadoQA_UserCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_UserPesCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_UserPesNom_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_ContratanteDoUser_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_ParaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_ParaPesCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_ParaPesNom_Z", StringUtil.RTrim( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_ContratanteDoPara_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_RespostaDe_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_Resposta_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultado_DemandaFM_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_UserPesCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_UserPesNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_ParaPesCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_ParaPesNom_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("ContagemResultadoQA_RespostaDe_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultadoQA_Codigo", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo, false);
         AddObjectProperty("ContagemResultadoQA_OSCod", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod, false);
         AddObjectProperty("ContagemResultado_DemandaFM", gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm, false);
         datetime_STZ = gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultadoQA_DataHora", sDateCnv, false);
         AddObjectProperty("ContagemResultadoQA_Texto", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_texto, false);
         AddObjectProperty("ContagemResultadoQA_UserCod", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod, false);
         AddObjectProperty("ContagemResultadoQA_UserPesCod", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod, false);
         AddObjectProperty("ContagemResultadoQA_UserPesNom", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom, false);
         AddObjectProperty("ContagemResultadoQA_ContratanteDoUser", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser, false);
         AddObjectProperty("ContagemResultadoQA_ParaCod", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod, false);
         AddObjectProperty("ContagemResultadoQA_ParaPesCod", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod, false);
         AddObjectProperty("ContagemResultadoQA_ParaPesNom", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom, false);
         AddObjectProperty("ContagemResultadoQA_ContratanteDoPara", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara, false);
         AddObjectProperty("ContagemResultadoQA_RespostaDe", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade, false);
         AddObjectProperty("ContagemResultadoQA_Resposta", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtContagemResultadoQA_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtContagemResultadoQA_Initialized, false);
            AddObjectProperty("ContagemResultadoQA_Codigo_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo_Z, false);
            AddObjectProperty("ContagemResultadoQA_OSCod_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod_Z, false);
            AddObjectProperty("ContagemResultado_DemandaFM_Z", gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z, false);
            datetime_STZ = gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultadoQA_DataHora_Z", sDateCnv, false);
            AddObjectProperty("ContagemResultadoQA_UserCod_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod_Z, false);
            AddObjectProperty("ContagemResultadoQA_UserPesCod_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_Z, false);
            AddObjectProperty("ContagemResultadoQA_UserPesNom_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z, false);
            AddObjectProperty("ContagemResultadoQA_ContratanteDoUser_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_Z, false);
            AddObjectProperty("ContagemResultadoQA_ParaCod_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod_Z, false);
            AddObjectProperty("ContagemResultadoQA_ParaPesCod_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_Z, false);
            AddObjectProperty("ContagemResultadoQA_ParaPesNom_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z, false);
            AddObjectProperty("ContagemResultadoQA_ContratanteDoPara_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_Z, false);
            AddObjectProperty("ContagemResultadoQA_RespostaDe_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_Z, false);
            AddObjectProperty("ContagemResultadoQA_Resposta_Z", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_Z, false);
            AddObjectProperty("ContagemResultado_DemandaFM_N", gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_N, false);
            AddObjectProperty("ContagemResultadoQA_UserPesCod_N", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_N, false);
            AddObjectProperty("ContagemResultadoQA_UserPesNom_N", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_N, false);
            AddObjectProperty("ContagemResultadoQA_ParaPesCod_N", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_N, false);
            AddObjectProperty("ContagemResultadoQA_ParaPesNom_N", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_N, false);
            AddObjectProperty("ContagemResultadoQA_RespostaDe_N", gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_Codigo"   )]
      public int gxTpr_Contagemresultadoqa_codigo
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo ;
         }

         set {
            if ( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo != value )
            {
               gxTv_SdtContagemResultadoQA_Mode = "INS";
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_Z_SetNull( );
               this.gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_Z_SetNull( );
            }
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoQA_OSCod" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_OSCod"   )]
      public int gxTpr_Contagemresultadoqa_oscod
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM"   )]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_N = 0;
            gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_N = 1;
         gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_DataHora" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_DataHora"  , IsNullable=true )]
      public string gxTpr_Contagemresultadoqa_datahora_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadoqa_datahora
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoQA_Texto" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_Texto"   )]
      public String gxTpr_Contagemresultadoqa_texto
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_texto ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_texto = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoQA_UserCod" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_UserCod"   )]
      public int gxTpr_Contagemresultadoqa_usercod
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoQA_UserPesCod" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_UserPesCod"   )]
      public int gxTpr_Contagemresultadoqa_userpescod
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_N = 0;
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_N = 1;
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_UserPesNom" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_UserPesNom"   )]
      public String gxTpr_Contagemresultadoqa_userpesnom
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_N = 0;
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_N = 1;
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ContratanteDoUser" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ContratanteDoUser"   )]
      public int gxTpr_Contagemresultadoqa_contratantedouser
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ParaCod" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ParaCod"   )]
      public int gxTpr_Contagemresultadoqa_paracod
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ParaPesCod" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ParaPesCod"   )]
      public int gxTpr_Contagemresultadoqa_parapescod
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_N = 0;
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_N = 1;
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ParaPesNom" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ParaPesNom"   )]
      public String gxTpr_Contagemresultadoqa_parapesnom
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_N = 0;
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_N = 1;
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ContratanteDoPara" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ContratanteDoPara"   )]
      public int gxTpr_Contagemresultadoqa_contratantedopara
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_RespostaDe" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_RespostaDe"   )]
      public int gxTpr_Contagemresultadoqa_respostade
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_N = 0;
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_N = 1;
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_Resposta" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_Resposta"   )]
      public int gxTpr_Contagemresultadoqa_resposta
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtContagemResultadoQA_Mode ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Mode = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Mode_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Mode = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtContagemResultadoQA_Initialized ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Initialized_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_Codigo_Z"   )]
      public int gxTpr_Contagemresultadoqa_codigo_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_OSCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_OSCod_Z"   )]
      public int gxTpr_Contagemresultadoqa_oscod_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM_Z"   )]
      public String gxTpr_Contagemresultado_demandafm_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_DataHora_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_DataHora_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultadoqa_datahora_Z_Nullable
      {
         get {
            if ( gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z = DateTime.MinValue;
            else
               gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultadoqa_datahora_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_UserCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_UserCod_Z"   )]
      public int gxTpr_Contagemresultadoqa_usercod_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_UserPesCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_UserPesCod_Z"   )]
      public int gxTpr_Contagemresultadoqa_userpescod_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_UserPesNom_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_UserPesNom_Z"   )]
      public String gxTpr_Contagemresultadoqa_userpesnom_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ContratanteDoUser_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ContratanteDoUser_Z"   )]
      public int gxTpr_Contagemresultadoqa_contratantedouser_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ParaCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ParaCod_Z"   )]
      public int gxTpr_Contagemresultadoqa_paracod_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ParaPesCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ParaPesCod_Z"   )]
      public int gxTpr_Contagemresultadoqa_parapescod_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ParaPesNom_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ParaPesNom_Z"   )]
      public String gxTpr_Contagemresultadoqa_parapesnom_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z = (String)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z = "";
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ContratanteDoPara_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ContratanteDoPara_Z"   )]
      public int gxTpr_Contagemresultadoqa_contratantedopara_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_RespostaDe_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_RespostaDe_Z"   )]
      public int gxTpr_Contagemresultadoqa_respostade_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_Resposta_Z" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_Resposta_Z"   )]
      public int gxTpr_Contagemresultadoqa_resposta_Z
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_Z ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_Z = (int)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_Z_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_Z = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM_N" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM_N"   )]
      public short gxTpr_Contagemresultado_demandafm_N
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_N ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_N_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_UserPesCod_N" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_UserPesCod_N"   )]
      public short gxTpr_Contagemresultadoqa_userpescod_N
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_N ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_UserPesNom_N" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_UserPesNom_N"   )]
      public short gxTpr_Contagemresultadoqa_userpesnom_N
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_N ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_N_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ParaPesCod_N" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ParaPesCod_N"   )]
      public short gxTpr_Contagemresultadoqa_parapescod_N
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_N ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_N_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_ParaPesNom_N" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_ParaPesNom_N"   )]
      public short gxTpr_Contagemresultadoqa_parapesnom_N
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_N ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_N_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultadoQA_RespostaDe_N" )]
      [  XmlElement( ElementName = "ContagemResultadoQA_RespostaDe_N"   )]
      public short gxTpr_Contagemresultadoqa_respostade_N
      {
         get {
            return gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_N ;
         }

         set {
            gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_N = (short)(value);
         }

      }

      public void gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_N_SetNull( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_N = 0;
         return  ;
      }

      public bool gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm = "";
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_texto = "";
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom = "";
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom = "";
         gxTv_SdtContagemResultadoQA_Mode = "";
         gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z = "";
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z = "";
         gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "contagemresultadoqa", "GeneXus.Programs.contagemresultadoqa_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtContagemResultadoQA_Initialized ;
      private short gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_N ;
      private short gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_N ;
      private short gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_N ;
      private short gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_N ;
      private short gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_N ;
      private short gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_codigo_Z ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_oscod_Z ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_usercod_Z ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpescod_Z ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedouser_Z ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_paracod_Z ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapescod_Z ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_contratantedopara_Z ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_respostade_Z ;
      private int gxTv_SdtContagemResultadoQA_Contagemresultadoqa_resposta_Z ;
      private String gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom ;
      private String gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom ;
      private String gxTv_SdtContagemResultadoQA_Mode ;
      private String gxTv_SdtContagemResultadoQA_Contagemresultadoqa_userpesnom_Z ;
      private String gxTv_SdtContagemResultadoQA_Contagemresultadoqa_parapesnom_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora ;
      private DateTime gxTv_SdtContagemResultadoQA_Contagemresultadoqa_datahora_Z ;
      private DateTime datetime_STZ ;
      private String gxTv_SdtContagemResultadoQA_Contagemresultadoqa_texto ;
      private String gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm ;
      private String gxTv_SdtContagemResultadoQA_Contagemresultado_demandafm_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"ContagemResultadoQA", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtContagemResultadoQA_RESTInterface : GxGenericCollectionItem<SdtContagemResultadoQA>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtContagemResultadoQA_RESTInterface( ) : base()
      {
      }

      public SdtContagemResultadoQA_RESTInterface( SdtContagemResultadoQA psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultadoQA_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoqa_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultadoqa_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_OSCod" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoqa_oscod
      {
         get {
            return sdt.gxTpr_Contagemresultadoqa_oscod ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_oscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_DemandaFM" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return sdt.gxTpr_Contagemresultado_demandafm ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demandafm = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_DataHora" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadoqa_datahora
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultadoqa_datahora) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_datahora = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultadoQA_Texto" , Order = 4 )]
      public String gxTpr_Contagemresultadoqa_texto
      {
         get {
            return sdt.gxTpr_Contagemresultadoqa_texto ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_texto = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_UserCod" , Order = 5 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoqa_usercod
      {
         get {
            return sdt.gxTpr_Contagemresultadoqa_usercod ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_usercod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_UserPesCod" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoqa_userpescod
      {
         get {
            return sdt.gxTpr_Contagemresultadoqa_userpescod ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_userpescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_UserPesNom" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadoqa_userpesnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadoqa_userpesnom) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_userpesnom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_ContratanteDoUser" , Order = 8 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoqa_contratantedouser
      {
         get {
            return sdt.gxTpr_Contagemresultadoqa_contratantedouser ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_contratantedouser = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_ParaCod" , Order = 9 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoqa_paracod
      {
         get {
            return sdt.gxTpr_Contagemresultadoqa_paracod ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_paracod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_ParaPesCod" , Order = 10 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoqa_parapescod
      {
         get {
            return sdt.gxTpr_Contagemresultadoqa_parapescod ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_parapescod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_ParaPesNom" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultadoqa_parapesnom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultadoqa_parapesnom) ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_parapesnom = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_ContratanteDoPara" , Order = 12 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoqa_contratantedopara
      {
         get {
            return sdt.gxTpr_Contagemresultadoqa_contratantedopara ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_contratantedopara = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_RespostaDe" , Order = 13 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoqa_respostade
      {
         get {
            return sdt.gxTpr_Contagemresultadoqa_respostade ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_respostade = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultadoQA_Resposta" , Order = 14 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultadoqa_resposta
      {
         get {
            return sdt.gxTpr_Contagemresultadoqa_resposta ;
         }

         set {
            sdt.gxTpr_Contagemresultadoqa_resposta = (int)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtContagemResultadoQA sdt
      {
         get {
            return (SdtContagemResultadoQA)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtContagemResultadoQA() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 37 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
