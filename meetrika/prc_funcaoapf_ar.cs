/*
               File: PRC_FuncaoAPF_AR
        Description: FuncaoAPF Arquivos Relacionados
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:7:42.84
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_funcaoapf_ar : GXProcedure
   {
      public prc_funcaoapf_ar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_funcaoapf_ar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_FuncaoAPF_Codigo ,
                           ref short aP1_FuncaoAPF_AR )
      {
         this.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV8FuncaoAPF_AR = aP1_FuncaoAPF_AR;
         initialize();
         executePrivate();
         aP0_FuncaoAPF_Codigo=this.A165FuncaoAPF_Codigo;
         aP1_FuncaoAPF_AR=this.AV8FuncaoAPF_AR;
      }

      public short executeUdp( ref int aP0_FuncaoAPF_Codigo )
      {
         this.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV8FuncaoAPF_AR = aP1_FuncaoAPF_AR;
         initialize();
         executePrivate();
         aP0_FuncaoAPF_Codigo=this.A165FuncaoAPF_Codigo;
         aP1_FuncaoAPF_AR=this.AV8FuncaoAPF_AR;
         return AV8FuncaoAPF_AR ;
      }

      public void executeSubmit( ref int aP0_FuncaoAPF_Codigo ,
                                 ref short aP1_FuncaoAPF_AR )
      {
         prc_funcaoapf_ar objprc_funcaoapf_ar;
         objprc_funcaoapf_ar = new prc_funcaoapf_ar();
         objprc_funcaoapf_ar.A165FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         objprc_funcaoapf_ar.AV8FuncaoAPF_AR = aP1_FuncaoAPF_AR;
         objprc_funcaoapf_ar.context.SetSubmitInitialConfig(context);
         objprc_funcaoapf_ar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_funcaoapf_ar);
         aP0_FuncaoAPF_Codigo=this.A165FuncaoAPF_Codigo;
         aP1_FuncaoAPF_AR=this.AV8FuncaoAPF_AR;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_funcaoapf_ar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8FuncaoAPF_AR = 0;
         /* Optimized group. */
         /* Using cursor P001Z2 */
         pr_default.execute(0, new Object[] {A165FuncaoAPF_Codigo});
         cV8FuncaoAPF_AR = P001Z2_AV8FuncaoAPF_AR[0];
         pr_default.close(0);
         AV8FuncaoAPF_AR = (short)(AV8FuncaoAPF_AR+cV8FuncaoAPF_AR*1);
         /* End optimized group. */
         if ( (0==AV8FuncaoAPF_AR) )
         {
            /* Using cursor P001Z3 */
            pr_default.execute(1, new Object[] {A165FuncaoAPF_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1026FuncaoAPF_RAImp = P001Z3_A1026FuncaoAPF_RAImp[0];
               n1026FuncaoAPF_RAImp = P001Z3_n1026FuncaoAPF_RAImp[0];
               AV8FuncaoAPF_AR = A1026FuncaoAPF_RAImp;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(1);
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001Z2_AV8FuncaoAPF_AR = new short[1] ;
         P001Z3_A165FuncaoAPF_Codigo = new int[1] ;
         P001Z3_A1026FuncaoAPF_RAImp = new short[1] ;
         P001Z3_n1026FuncaoAPF_RAImp = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_funcaoapf_ar__default(),
            new Object[][] {
                new Object[] {
               P001Z2_AV8FuncaoAPF_AR
               }
               , new Object[] {
               P001Z3_A165FuncaoAPF_Codigo, P001Z3_A1026FuncaoAPF_RAImp, P001Z3_n1026FuncaoAPF_RAImp
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV8FuncaoAPF_AR ;
      private short cV8FuncaoAPF_AR ;
      private short A1026FuncaoAPF_RAImp ;
      private int A165FuncaoAPF_Codigo ;
      private String scmdbuf ;
      private bool n1026FuncaoAPF_RAImp ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_FuncaoAPF_Codigo ;
      private short aP1_FuncaoAPF_AR ;
      private IDataStoreProvider pr_default ;
      private short[] P001Z2_AV8FuncaoAPF_AR ;
      private int[] P001Z3_A165FuncaoAPF_Codigo ;
      private short[] P001Z3_A1026FuncaoAPF_RAImp ;
      private bool[] P001Z3_n1026FuncaoAPF_RAImp ;
   }

   public class prc_funcaoapf_ar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001Z2 ;
          prmP001Z2 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP001Z3 ;
          prmP001Z3 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001Z2", "SELECT COUNT(*) FROM ( SELECT T1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod FROM ([FuncoesAPFAtributos] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoAPFAtributos_FuncaoDadosCod]) WHERE (T1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo) AND (T2.[FuncaoDados_Tipo] <> 'DC') GROUP BY T1.[FuncaoAPFAtributos_FuncaoDadosCod]) GroupByT ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001Z2,1,0,true,false )
             ,new CursorDef("P001Z3", "SELECT TOP 1 [FuncaoAPF_Codigo], [FuncaoAPF_RAImp] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_Codigo] = @FuncaoAPF_Codigo ORDER BY [FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001Z3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
