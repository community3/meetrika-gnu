/*
               File: GetPromptUnidadeMedicaoFilterData
        Description: Get Prompt Unidade Medicao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:11:37.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptunidademedicaofilterdata : GXProcedure
   {
      public getpromptunidademedicaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptunidademedicaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
         return AV26OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptunidademedicaofilterdata objgetpromptunidademedicaofilterdata;
         objgetpromptunidademedicaofilterdata = new getpromptunidademedicaofilterdata();
         objgetpromptunidademedicaofilterdata.AV17DDOName = aP0_DDOName;
         objgetpromptunidademedicaofilterdata.AV15SearchTxt = aP1_SearchTxt;
         objgetpromptunidademedicaofilterdata.AV16SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptunidademedicaofilterdata.AV21OptionsJson = "" ;
         objgetpromptunidademedicaofilterdata.AV24OptionsDescJson = "" ;
         objgetpromptunidademedicaofilterdata.AV26OptionIndexesJson = "" ;
         objgetpromptunidademedicaofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptunidademedicaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptunidademedicaofilterdata);
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptunidademedicaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV20Options = (IGxCollection)(new GxSimpleCollection());
         AV23OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV25OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV17DDOName), "DDO_UNIDADEMEDICAO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADUNIDADEMEDICAO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV17DDOName), "DDO_UNIDADEMEDICAO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADUNIDADEMEDICAO_SIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV21OptionsJson = AV20Options.ToJSonString(false);
         AV24OptionsDescJson = AV23OptionsDesc.ToJSonString(false);
         AV26OptionIndexesJson = AV25OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV28Session.Get("PromptUnidadeMedicaoGridState"), "") == 0 )
         {
            AV30GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptUnidadeMedicaoGridState"), "");
         }
         else
         {
            AV30GridState.FromXml(AV28Session.Get("PromptUnidadeMedicaoGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV30GridState.gxTpr_Filtervalues.Count )
         {
            AV31GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV30GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFUNIDADEMEDICAO_NOME") == 0 )
            {
               AV10TFUnidadeMedicao_Nome = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFUNIDADEMEDICAO_NOME_SEL") == 0 )
            {
               AV11TFUnidadeMedicao_Nome_Sel = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFUNIDADEMEDICAO_SIGLA") == 0 )
            {
               AV12TFUnidadeMedicao_Sigla = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFUNIDADEMEDICAO_SIGLA_SEL") == 0 )
            {
               AV13TFUnidadeMedicao_Sigla_Sel = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFUNIDADEMEDICAO_ATIVO_SEL") == 0 )
            {
               AV14TFUnidadeMedicao_Ativo_Sel = (short)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
         if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(1));
            AV33DynamicFiltersSelector1 = AV32GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "UNIDADEMEDICAO_NOME") == 0 )
            {
               AV34UnidadeMedicao_Nome1 = AV32GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV35DynamicFiltersEnabled2 = true;
               AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(2));
               AV36DynamicFiltersSelector2 = AV32GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "UNIDADEMEDICAO_NOME") == 0 )
               {
                  AV37UnidadeMedicao_Nome2 = AV32GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV38DynamicFiltersEnabled3 = true;
                  AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(3));
                  AV39DynamicFiltersSelector3 = AV32GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "UNIDADEMEDICAO_NOME") == 0 )
                  {
                     AV40UnidadeMedicao_Nome3 = AV32GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADUNIDADEMEDICAO_NOMEOPTIONS' Routine */
         AV10TFUnidadeMedicao_Nome = AV15SearchTxt;
         AV11TFUnidadeMedicao_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV33DynamicFiltersSelector1 ,
                                              AV34UnidadeMedicao_Nome1 ,
                                              AV35DynamicFiltersEnabled2 ,
                                              AV36DynamicFiltersSelector2 ,
                                              AV37UnidadeMedicao_Nome2 ,
                                              AV38DynamicFiltersEnabled3 ,
                                              AV39DynamicFiltersSelector3 ,
                                              AV40UnidadeMedicao_Nome3 ,
                                              AV11TFUnidadeMedicao_Nome_Sel ,
                                              AV10TFUnidadeMedicao_Nome ,
                                              AV13TFUnidadeMedicao_Sigla_Sel ,
                                              AV12TFUnidadeMedicao_Sigla ,
                                              AV14TFUnidadeMedicao_Ativo_Sel ,
                                              A1198UnidadeMedicao_Nome ,
                                              A1200UnidadeMedicao_Sigla ,
                                              A1199UnidadeMedicao_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV34UnidadeMedicao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV34UnidadeMedicao_Nome1), 50, "%");
         lV37UnidadeMedicao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV37UnidadeMedicao_Nome2), 50, "%");
         lV40UnidadeMedicao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV40UnidadeMedicao_Nome3), 50, "%");
         lV10TFUnidadeMedicao_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUnidadeMedicao_Nome), 50, "%");
         lV12TFUnidadeMedicao_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFUnidadeMedicao_Sigla), 15, "%");
         /* Using cursor P00Q42 */
         pr_default.execute(0, new Object[] {lV34UnidadeMedicao_Nome1, lV37UnidadeMedicao_Nome2, lV40UnidadeMedicao_Nome3, lV10TFUnidadeMedicao_Nome, AV11TFUnidadeMedicao_Nome_Sel, lV12TFUnidadeMedicao_Sigla, AV13TFUnidadeMedicao_Sigla_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQ42 = false;
            A1198UnidadeMedicao_Nome = P00Q42_A1198UnidadeMedicao_Nome[0];
            A1199UnidadeMedicao_Ativo = P00Q42_A1199UnidadeMedicao_Ativo[0];
            A1200UnidadeMedicao_Sigla = P00Q42_A1200UnidadeMedicao_Sigla[0];
            A1197UnidadeMedicao_Codigo = P00Q42_A1197UnidadeMedicao_Codigo[0];
            AV27count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00Q42_A1198UnidadeMedicao_Nome[0], A1198UnidadeMedicao_Nome) == 0 ) )
            {
               BRKQ42 = false;
               A1197UnidadeMedicao_Codigo = P00Q42_A1197UnidadeMedicao_Codigo[0];
               AV27count = (long)(AV27count+1);
               BRKQ42 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1198UnidadeMedicao_Nome)) )
            {
               AV19Option = A1198UnidadeMedicao_Nome;
               AV20Options.Add(AV19Option, 0);
               AV25OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV27count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV20Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQ42 )
            {
               BRKQ42 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADUNIDADEMEDICAO_SIGLAOPTIONS' Routine */
         AV12TFUnidadeMedicao_Sigla = AV15SearchTxt;
         AV13TFUnidadeMedicao_Sigla_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV33DynamicFiltersSelector1 ,
                                              AV34UnidadeMedicao_Nome1 ,
                                              AV35DynamicFiltersEnabled2 ,
                                              AV36DynamicFiltersSelector2 ,
                                              AV37UnidadeMedicao_Nome2 ,
                                              AV38DynamicFiltersEnabled3 ,
                                              AV39DynamicFiltersSelector3 ,
                                              AV40UnidadeMedicao_Nome3 ,
                                              AV11TFUnidadeMedicao_Nome_Sel ,
                                              AV10TFUnidadeMedicao_Nome ,
                                              AV13TFUnidadeMedicao_Sigla_Sel ,
                                              AV12TFUnidadeMedicao_Sigla ,
                                              AV14TFUnidadeMedicao_Ativo_Sel ,
                                              A1198UnidadeMedicao_Nome ,
                                              A1200UnidadeMedicao_Sigla ,
                                              A1199UnidadeMedicao_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV34UnidadeMedicao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV34UnidadeMedicao_Nome1), 50, "%");
         lV37UnidadeMedicao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV37UnidadeMedicao_Nome2), 50, "%");
         lV40UnidadeMedicao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV40UnidadeMedicao_Nome3), 50, "%");
         lV10TFUnidadeMedicao_Nome = StringUtil.PadR( StringUtil.RTrim( AV10TFUnidadeMedicao_Nome), 50, "%");
         lV12TFUnidadeMedicao_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFUnidadeMedicao_Sigla), 15, "%");
         /* Using cursor P00Q43 */
         pr_default.execute(1, new Object[] {lV34UnidadeMedicao_Nome1, lV37UnidadeMedicao_Nome2, lV40UnidadeMedicao_Nome3, lV10TFUnidadeMedicao_Nome, AV11TFUnidadeMedicao_Nome_Sel, lV12TFUnidadeMedicao_Sigla, AV13TFUnidadeMedicao_Sigla_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKQ44 = false;
            A1200UnidadeMedicao_Sigla = P00Q43_A1200UnidadeMedicao_Sigla[0];
            A1199UnidadeMedicao_Ativo = P00Q43_A1199UnidadeMedicao_Ativo[0];
            A1198UnidadeMedicao_Nome = P00Q43_A1198UnidadeMedicao_Nome[0];
            A1197UnidadeMedicao_Codigo = P00Q43_A1197UnidadeMedicao_Codigo[0];
            AV27count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00Q43_A1200UnidadeMedicao_Sigla[0], A1200UnidadeMedicao_Sigla) == 0 ) )
            {
               BRKQ44 = false;
               A1197UnidadeMedicao_Codigo = P00Q43_A1197UnidadeMedicao_Codigo[0];
               AV27count = (long)(AV27count+1);
               BRKQ44 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1200UnidadeMedicao_Sigla)) )
            {
               AV19Option = A1200UnidadeMedicao_Sigla;
               AV20Options.Add(AV19Option, 0);
               AV25OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV27count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV20Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQ44 )
            {
               BRKQ44 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV20Options = new GxSimpleCollection();
         AV23OptionsDesc = new GxSimpleCollection();
         AV25OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV28Session = context.GetSession();
         AV30GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV31GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFUnidadeMedicao_Nome = "";
         AV11TFUnidadeMedicao_Nome_Sel = "";
         AV12TFUnidadeMedicao_Sigla = "";
         AV13TFUnidadeMedicao_Sigla_Sel = "";
         AV32GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV33DynamicFiltersSelector1 = "";
         AV34UnidadeMedicao_Nome1 = "";
         AV36DynamicFiltersSelector2 = "";
         AV37UnidadeMedicao_Nome2 = "";
         AV39DynamicFiltersSelector3 = "";
         AV40UnidadeMedicao_Nome3 = "";
         scmdbuf = "";
         lV34UnidadeMedicao_Nome1 = "";
         lV37UnidadeMedicao_Nome2 = "";
         lV40UnidadeMedicao_Nome3 = "";
         lV10TFUnidadeMedicao_Nome = "";
         lV12TFUnidadeMedicao_Sigla = "";
         A1198UnidadeMedicao_Nome = "";
         A1200UnidadeMedicao_Sigla = "";
         P00Q42_A1198UnidadeMedicao_Nome = new String[] {""} ;
         P00Q42_A1199UnidadeMedicao_Ativo = new bool[] {false} ;
         P00Q42_A1200UnidadeMedicao_Sigla = new String[] {""} ;
         P00Q42_A1197UnidadeMedicao_Codigo = new int[1] ;
         AV19Option = "";
         P00Q43_A1200UnidadeMedicao_Sigla = new String[] {""} ;
         P00Q43_A1199UnidadeMedicao_Ativo = new bool[] {false} ;
         P00Q43_A1198UnidadeMedicao_Nome = new String[] {""} ;
         P00Q43_A1197UnidadeMedicao_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptunidademedicaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00Q42_A1198UnidadeMedicao_Nome, P00Q42_A1199UnidadeMedicao_Ativo, P00Q42_A1200UnidadeMedicao_Sigla, P00Q42_A1197UnidadeMedicao_Codigo
               }
               , new Object[] {
               P00Q43_A1200UnidadeMedicao_Sigla, P00Q43_A1199UnidadeMedicao_Ativo, P00Q43_A1198UnidadeMedicao_Nome, P00Q43_A1197UnidadeMedicao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFUnidadeMedicao_Ativo_Sel ;
      private int AV43GXV1 ;
      private int A1197UnidadeMedicao_Codigo ;
      private long AV27count ;
      private String AV10TFUnidadeMedicao_Nome ;
      private String AV11TFUnidadeMedicao_Nome_Sel ;
      private String AV12TFUnidadeMedicao_Sigla ;
      private String AV13TFUnidadeMedicao_Sigla_Sel ;
      private String AV34UnidadeMedicao_Nome1 ;
      private String AV37UnidadeMedicao_Nome2 ;
      private String AV40UnidadeMedicao_Nome3 ;
      private String scmdbuf ;
      private String lV34UnidadeMedicao_Nome1 ;
      private String lV37UnidadeMedicao_Nome2 ;
      private String lV40UnidadeMedicao_Nome3 ;
      private String lV10TFUnidadeMedicao_Nome ;
      private String lV12TFUnidadeMedicao_Sigla ;
      private String A1198UnidadeMedicao_Nome ;
      private String A1200UnidadeMedicao_Sigla ;
      private bool returnInSub ;
      private bool AV35DynamicFiltersEnabled2 ;
      private bool AV38DynamicFiltersEnabled3 ;
      private bool A1199UnidadeMedicao_Ativo ;
      private bool BRKQ42 ;
      private bool BRKQ44 ;
      private String AV26OptionIndexesJson ;
      private String AV21OptionsJson ;
      private String AV24OptionsDescJson ;
      private String AV17DDOName ;
      private String AV15SearchTxt ;
      private String AV16SearchTxtTo ;
      private String AV33DynamicFiltersSelector1 ;
      private String AV36DynamicFiltersSelector2 ;
      private String AV39DynamicFiltersSelector3 ;
      private String AV19Option ;
      private IGxSession AV28Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00Q42_A1198UnidadeMedicao_Nome ;
      private bool[] P00Q42_A1199UnidadeMedicao_Ativo ;
      private String[] P00Q42_A1200UnidadeMedicao_Sigla ;
      private int[] P00Q42_A1197UnidadeMedicao_Codigo ;
      private String[] P00Q43_A1200UnidadeMedicao_Sigla ;
      private bool[] P00Q43_A1199UnidadeMedicao_Ativo ;
      private String[] P00Q43_A1198UnidadeMedicao_Nome ;
      private int[] P00Q43_A1197UnidadeMedicao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV30GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV31GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV32GridStateDynamicFilter ;
   }

   public class getpromptunidademedicaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00Q42( IGxContext context ,
                                             String AV33DynamicFiltersSelector1 ,
                                             String AV34UnidadeMedicao_Nome1 ,
                                             bool AV35DynamicFiltersEnabled2 ,
                                             String AV36DynamicFiltersSelector2 ,
                                             String AV37UnidadeMedicao_Nome2 ,
                                             bool AV38DynamicFiltersEnabled3 ,
                                             String AV39DynamicFiltersSelector3 ,
                                             String AV40UnidadeMedicao_Nome3 ,
                                             String AV11TFUnidadeMedicao_Nome_Sel ,
                                             String AV10TFUnidadeMedicao_Nome ,
                                             String AV13TFUnidadeMedicao_Sigla_Sel ,
                                             String AV12TFUnidadeMedicao_Sigla ,
                                             short AV14TFUnidadeMedicao_Ativo_Sel ,
                                             String A1198UnidadeMedicao_Nome ,
                                             String A1200UnidadeMedicao_Sigla ,
                                             bool A1199UnidadeMedicao_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [7] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [UnidadeMedicao_Nome], [UnidadeMedicao_Ativo], [UnidadeMedicao_Sigla], [UnidadeMedicao_Codigo] FROM [UnidadeMedicao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34UnidadeMedicao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV34UnidadeMedicao_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV34UnidadeMedicao_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV35DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37UnidadeMedicao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV37UnidadeMedicao_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV37UnidadeMedicao_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV38DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40UnidadeMedicao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV40UnidadeMedicao_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV40UnidadeMedicao_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeMedicao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUnidadeMedicao_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like @lV10TFUnidadeMedicao_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like @lV10TFUnidadeMedicao_Nome)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeMedicao_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] = @AV11TFUnidadeMedicao_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] = @AV11TFUnidadeMedicao_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFUnidadeMedicao_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFUnidadeMedicao_Sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] like @lV12TFUnidadeMedicao_Sigla)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] like @lV12TFUnidadeMedicao_Sigla)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFUnidadeMedicao_Sigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] = @AV13TFUnidadeMedicao_Sigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] = @AV13TFUnidadeMedicao_Sigla_Sel)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV14TFUnidadeMedicao_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 1)";
            }
         }
         if ( AV14TFUnidadeMedicao_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [UnidadeMedicao_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00Q43( IGxContext context ,
                                             String AV33DynamicFiltersSelector1 ,
                                             String AV34UnidadeMedicao_Nome1 ,
                                             bool AV35DynamicFiltersEnabled2 ,
                                             String AV36DynamicFiltersSelector2 ,
                                             String AV37UnidadeMedicao_Nome2 ,
                                             bool AV38DynamicFiltersEnabled3 ,
                                             String AV39DynamicFiltersSelector3 ,
                                             String AV40UnidadeMedicao_Nome3 ,
                                             String AV11TFUnidadeMedicao_Nome_Sel ,
                                             String AV10TFUnidadeMedicao_Nome ,
                                             String AV13TFUnidadeMedicao_Sigla_Sel ,
                                             String AV12TFUnidadeMedicao_Sigla ,
                                             short AV14TFUnidadeMedicao_Ativo_Sel ,
                                             String A1198UnidadeMedicao_Nome ,
                                             String A1200UnidadeMedicao_Sigla ,
                                             bool A1199UnidadeMedicao_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [UnidadeMedicao_Sigla], [UnidadeMedicao_Ativo], [UnidadeMedicao_Nome], [UnidadeMedicao_Codigo] FROM [UnidadeMedicao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV34UnidadeMedicao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV34UnidadeMedicao_Nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV34UnidadeMedicao_Nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( AV35DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV37UnidadeMedicao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV37UnidadeMedicao_Nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV37UnidadeMedicao_Nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV38DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "UNIDADEMEDICAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40UnidadeMedicao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like '%' + @lV40UnidadeMedicao_Nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like '%' + @lV40UnidadeMedicao_Nome3 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeMedicao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFUnidadeMedicao_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] like @lV10TFUnidadeMedicao_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] like @lV10TFUnidadeMedicao_Nome)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFUnidadeMedicao_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Nome] = @AV11TFUnidadeMedicao_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Nome] = @AV11TFUnidadeMedicao_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFUnidadeMedicao_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFUnidadeMedicao_Sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] like @lV12TFUnidadeMedicao_Sigla)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] like @lV12TFUnidadeMedicao_Sigla)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFUnidadeMedicao_Sigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Sigla] = @AV13TFUnidadeMedicao_Sigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Sigla] = @AV13TFUnidadeMedicao_Sigla_Sel)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV14TFUnidadeMedicao_Ativo_Sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 1)";
            }
         }
         if ( AV14TFUnidadeMedicao_Ativo_Sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([UnidadeMedicao_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([UnidadeMedicao_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [UnidadeMedicao_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00Q42(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] );
               case 1 :
                     return conditional_P00Q43(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (short)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00Q42 ;
          prmP00Q42 = new Object[] {
          new Object[] {"@lV34UnidadeMedicao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37UnidadeMedicao_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40UnidadeMedicao_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFUnidadeMedicao_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUnidadeMedicao_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFUnidadeMedicao_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFUnidadeMedicao_Sigla_Sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00Q43 ;
          prmP00Q43 = new Object[] {
          new Object[] {"@lV34UnidadeMedicao_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV37UnidadeMedicao_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV40UnidadeMedicao_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV10TFUnidadeMedicao_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV11TFUnidadeMedicao_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV12TFUnidadeMedicao_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFUnidadeMedicao_Sigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00Q42", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Q42,100,0,true,false )
             ,new CursorDef("P00Q43", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Q43,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptunidademedicaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptunidademedicaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptunidademedicaofilterdata") )
          {
             return  ;
          }
          getpromptunidademedicaofilterdata worker = new getpromptunidademedicaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
