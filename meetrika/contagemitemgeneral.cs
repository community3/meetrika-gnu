/*
               File: ContagemItemGeneral
        Description: Contagem Item General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:19:5.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemitemgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contagemitemgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contagemitemgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemItem_Lancamento )
      {
         this.A224ContagemItem_Lancamento = aP0_ContagemItem_Lancamento;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbFuncaoAPF_Tipo = new GXCombobox();
         cmbContagemItem_TipoUnidade = new GXCombobox();
         cmbFuncaoAPF_Complexidade = new GXCombobox();
         cmbContagem_Tecnica = new GXCombobox();
         cmbContagem_Tipo = new GXCombobox();
         cmbContagemItem_ValidacaoStatusFinal = new GXCombobox();
         cmbContagemItem_FSValidacao = new GXCombobox();
         cmbContagemItem_FSTipoUnidade = new GXCombobox();
         cmbContagemItem_FMValidacao = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A224ContagemItem_Lancamento = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A224ContagemItem_Lancamento});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA552( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "ContagemItemGeneral";
               context.Gx_err = 0;
               /* Using cursor H00553 */
               pr_default.execute(0);
               if ( (pr_default.getStatus(0) != 101) )
               {
                  A232ContagemItem_FSDER = H00553_A232ContagemItem_FSDER[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A232ContagemItem_FSDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A232ContagemItem_FSDER), 3, 0)));
                  n232ContagemItem_FSDER = H00553_n232ContagemItem_FSDER[0];
               }
               else
               {
                  A232ContagemItem_FSDER = 0;
                  n232ContagemItem_FSDER = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A232ContagemItem_FSDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A232ContagemItem_FSDER), 3, 0)));
               }
               pr_default.close(0);
               /* Using cursor H00555 */
               pr_default.execute(1, new Object[] {A224ContagemItem_Lancamento});
               if ( (pr_default.getStatus(1) != 101) )
               {
                  A251ContagemItem_FMDER = H00555_A251ContagemItem_FMDER[0];
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
                  n251ContagemItem_FMDER = H00555_n251ContagemItem_FMDER[0];
               }
               else
               {
                  A251ContagemItem_FMDER = 0;
                  n251ContagemItem_FMDER = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
               }
               pr_default.close(1);
               GXt_int1 = A233ContagemItem_FSAR;
               new prc_calculaaruniquefs(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               A233ContagemItem_FSAR = (short)(GXt_int1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A233ContagemItem_FSAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A233ContagemItem_FSAR), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSAR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A233ContagemItem_FSAR), "ZZ9")));
               GXt_int1 = A252ContagemItem_FMAR;
               new prc_calculaaruniquefm(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
               A252ContagemItem_FMAR = (short)(GXt_int1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A252ContagemItem_FMAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A252ContagemItem_FMAR), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMAR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A252ContagemItem_FMAR), "ZZ9")));
               WS552( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contagem Item General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311719568");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemitemgeneral.aspx") + "?" + UrlEncode("" +A224ContagemItem_Lancamento)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA224ContagemItem_Lancamento), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_PFB", GetSecureSignedToken( sPrefix, context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_PFL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_TIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_EVIDENCIAS", GetSecureSignedToken( sPrefix, A953ContagemItem_Evidencias));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_QTDINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A954ContagemItem_QtdINM), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_CP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A955ContagemItem_CP, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_RA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_DER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FUNCAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A958ContagemItem_Funcao, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_SEQUENCIAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_VALIDACAOSTATUSFINAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A260ContagemItem_ValidacaoStatusFinal), "9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_REQUISITO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A229ContagemItem_Requisito), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_FABRICASOFTWAREPESSOANOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A209Contagem_FabricaSoftwarePessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSVALIDACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A267ContagemItem_FSValidacao), "9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSTIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A228ContagemItem_FSTipoUnidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSAR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A233ContagemItem_FSAR), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSQTDINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A234ContagemItem_FSQTDInm), "ZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSCP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A235ContagemItem_FSCP, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSPONTOFUNCAOBRUTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A236ContagemItem_FSPontoFuncaoBruto, "ZZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSPONTOFUNCAOLIQUIDO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A237ContagemItem_FSPontoFuncaoLiquido, "ZZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSEVIDENCIAS", GetSecureSignedToken( sPrefix, A238ContagemItem_FSEvidencias));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_USUARIOAUDITORPESSOANOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A218Contagem_UsuarioAuditorPessoaNom, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FMVALIDACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A268ContagemItem_FMValidacao), "9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FMAR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A252ContagemItem_FMAR), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FMQTDINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A239ContagemItem_FMQTDInm), "ZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FMCP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A240ContagemItem_FMCP, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FMPONTOFUNCAOBRUTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A241ContagemItem_FMPontoFuncaoBruto, "ZZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FMPONTOFUNCAOLIQUIDO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A242ContagemItem_FMPontoFuncaoLiquido, "ZZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FMREFERENCIATECNICACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_USUARIOAUDITORCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_USUARIOAUDITORPESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEMITEM_FSREFERENCIATECNICACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_FABRICASOFTWARECOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTAGEM_FABRICASOFTWAREPESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContagemItemGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contagemitemgeneral:[SendSecurityCheck value for]"+"ContagemItem_FMReferenciaTecnicaCod:"+context.localUtil.Format( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), "ZZZZZ9"));
         GXUtil.WriteLog("contagemitemgeneral:[SendSecurityCheck value for]"+"ContagemItem_FSReferenciaTecnicaCod:"+context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9"));
         GXUtil.WriteLog("contagemitemgeneral:[SendSecurityCheck value for]"+"FuncaoAPF_Codigo:"+context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm552( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contagemitemgeneral.js", "?2020311719585");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContagemItemGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Item General" ;
      }

      protected void WB550( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contagemitemgeneral.aspx");
            }
            wb_table1_2_552( true) ;
         }
         else
         {
            wb_table1_2_552( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_552e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMReferenciaTecnicaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMReferenciaTecnicaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagemItem_FMReferenciaTecnicaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioAuditorCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioAuditorCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_UsuarioAuditorCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemItemGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioAuditorPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioAuditorPessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_UsuarioAuditorPessoaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemItemGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSReferenciaTecnicaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSReferenciaTecnicaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagemItem_FSReferenciaTecnicaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_FabricaSoftwareCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_FabricaSoftwareCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_FabricaSoftwareCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemItemGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_FabricaSoftwarePessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_FabricaSoftwarePessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_FabricaSoftwarePessoaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemItemGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A213Contagem_UsuarioContadorCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_UsuarioContadorCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorPessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A214Contagem_UsuarioContadorPessoaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorPessoaCod_Jsonclick, 0, "Attribute", "", "", "", edtContagem_UsuarioContadorPessoaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtFuncaoAPF_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtFuncaoAPF_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtFuncaoAPF_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START552( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contagem Item General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP550( ) ;
            }
         }
      }

      protected void WS552( )
      {
         START552( ) ;
         EVT552( ) ;
      }

      protected void EVT552( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP550( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP550( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11552 */
                                    E11552 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP550( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12552 */
                                    E12552 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP550( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13552 */
                                    E13552 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP550( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14552 */
                                    E14552 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP550( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP550( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE552( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm552( ) ;
            }
         }
      }

      protected void PA552( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbFuncaoAPF_Tipo.Name = "FUNCAOAPF_TIPO";
            cmbFuncaoAPF_Tipo.WebTags = "";
            cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
            cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
            cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
            cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
            if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
            {
               A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
            }
            cmbContagemItem_TipoUnidade.Name = "CONTAGEMITEM_TIPOUNIDADE";
            cmbContagemItem_TipoUnidade.WebTags = "";
            if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
            {
               A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A952ContagemItem_TipoUnidade", A952ContagemItem_TipoUnidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_TIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
            }
            cmbFuncaoAPF_Complexidade.Name = "FUNCAOAPF_COMPLEXIDADE";
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
            }
            cmbContagem_Tecnica.Name = "CONTAGEM_TECNICA";
            cmbContagem_Tecnica.WebTags = "";
            cmbContagem_Tecnica.addItem("", "Nenhuma", 0);
            cmbContagem_Tecnica.addItem("1", "Indicativa", 0);
            cmbContagem_Tecnica.addItem("2", "Estimada", 0);
            cmbContagem_Tecnica.addItem("3", "Detalhada", 0);
            if ( cmbContagem_Tecnica.ItemCount > 0 )
            {
               A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
               n195Contagem_Tecnica = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
            }
            cmbContagem_Tipo.Name = "CONTAGEM_TIPO";
            cmbContagem_Tipo.WebTags = "";
            cmbContagem_Tipo.addItem("", "(Nenhum)", 0);
            cmbContagem_Tipo.addItem("D", "Projeto de Desenvolvimento", 0);
            cmbContagem_Tipo.addItem("M", "Projeto de Melhor�a", 0);
            cmbContagem_Tipo.addItem("C", "Projeto de Contagem", 0);
            cmbContagem_Tipo.addItem("A", "Aplica��o", 0);
            if ( cmbContagem_Tipo.ItemCount > 0 )
            {
               A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
               n196Contagem_Tipo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A196Contagem_Tipo", A196Contagem_Tipo);
            }
            cmbContagemItem_ValidacaoStatusFinal.Name = "CONTAGEMITEM_VALIDACAOSTATUSFINAL";
            cmbContagemItem_ValidacaoStatusFinal.WebTags = "";
            cmbContagemItem_ValidacaoStatusFinal.addItem("1", "Aprovado", 0);
            cmbContagemItem_ValidacaoStatusFinal.addItem("2", "N�o Aprovado", 0);
            if ( cmbContagemItem_ValidacaoStatusFinal.ItemCount > 0 )
            {
               A260ContagemItem_ValidacaoStatusFinal = (short)(NumberUtil.Val( cmbContagemItem_ValidacaoStatusFinal.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_VALIDACAOSTATUSFINAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A260ContagemItem_ValidacaoStatusFinal), "9")));
            }
            cmbContagemItem_FSValidacao.Name = "CONTAGEMITEM_FSVALIDACAO";
            cmbContagemItem_FSValidacao.WebTags = "";
            cmbContagemItem_FSValidacao.addItem("1", "Aprovado", 0);
            cmbContagemItem_FSValidacao.addItem("2", "N�o Aprovado", 0);
            if ( cmbContagemItem_FSValidacao.ItemCount > 0 )
            {
               A267ContagemItem_FSValidacao = (short)(NumberUtil.Val( cmbContagemItem_FSValidacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0))), "."));
               n267ContagemItem_FSValidacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A267ContagemItem_FSValidacao", StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSVALIDACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A267ContagemItem_FSValidacao), "9")));
            }
            cmbContagemItem_FSTipoUnidade.Name = "CONTAGEMITEM_FSTIPOUNIDADE";
            cmbContagemItem_FSTipoUnidade.WebTags = "";
            cmbContagemItem_FSTipoUnidade.addItem("EE", "Entrada Externa - EE", 0);
            cmbContagemItem_FSTipoUnidade.addItem("CE", "Consulta Externa - CE", 0);
            cmbContagemItem_FSTipoUnidade.addItem("SE", "Sa�da Externa - SE", 0);
            cmbContagemItem_FSTipoUnidade.addItem("ALI", "Arquivo L�gico Interno - ALI", 0);
            cmbContagemItem_FSTipoUnidade.addItem("aie", "Arquivo de Interface Externa - AIE", 0);
            cmbContagemItem_FSTipoUnidade.addItem("DC", "Dados de C�digo - DC", 0);
            if ( cmbContagemItem_FSTipoUnidade.ItemCount > 0 )
            {
               A228ContagemItem_FSTipoUnidade = cmbContagemItem_FSTipoUnidade.getValidValue(A228ContagemItem_FSTipoUnidade);
               n228ContagemItem_FSTipoUnidade = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A228ContagemItem_FSTipoUnidade", A228ContagemItem_FSTipoUnidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSTIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A228ContagemItem_FSTipoUnidade, ""))));
            }
            cmbContagemItem_FMValidacao.Name = "CONTAGEMITEM_FMVALIDACAO";
            cmbContagemItem_FMValidacao.WebTags = "";
            cmbContagemItem_FMValidacao.addItem("1", "Aprovado", 0);
            cmbContagemItem_FMValidacao.addItem("2", "N�o Aprovado", 0);
            if ( cmbContagemItem_FMValidacao.ItemCount > 0 )
            {
               A268ContagemItem_FMValidacao = (short)(NumberUtil.Val( cmbContagemItem_FMValidacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0))), "."));
               n268ContagemItem_FMValidacao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A268ContagemItem_FMValidacao", StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMVALIDACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A268ContagemItem_FMValidacao), "9")));
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
         {
            A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
         }
         if ( cmbContagemItem_TipoUnidade.ItemCount > 0 )
         {
            A952ContagemItem_TipoUnidade = cmbContagemItem_TipoUnidade.getValidValue(A952ContagemItem_TipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A952ContagemItem_TipoUnidade", A952ContagemItem_TipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_TIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
         }
         if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
         {
            A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         }
         if ( cmbContagem_Tecnica.ItemCount > 0 )
         {
            A195Contagem_Tecnica = cmbContagem_Tecnica.getValidValue(A195Contagem_Tecnica);
            n195Contagem_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
         }
         if ( cmbContagem_Tipo.ItemCount > 0 )
         {
            A196Contagem_Tipo = cmbContagem_Tipo.getValidValue(A196Contagem_Tipo);
            n196Contagem_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A196Contagem_Tipo", A196Contagem_Tipo);
         }
         if ( cmbContagemItem_ValidacaoStatusFinal.ItemCount > 0 )
         {
            A260ContagemItem_ValidacaoStatusFinal = (short)(NumberUtil.Val( cmbContagemItem_ValidacaoStatusFinal.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_VALIDACAOSTATUSFINAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A260ContagemItem_ValidacaoStatusFinal), "9")));
         }
         if ( cmbContagemItem_FSValidacao.ItemCount > 0 )
         {
            A267ContagemItem_FSValidacao = (short)(NumberUtil.Val( cmbContagemItem_FSValidacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0))), "."));
            n267ContagemItem_FSValidacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A267ContagemItem_FSValidacao", StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSVALIDACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A267ContagemItem_FSValidacao), "9")));
         }
         if ( cmbContagemItem_FSTipoUnidade.ItemCount > 0 )
         {
            A228ContagemItem_FSTipoUnidade = cmbContagemItem_FSTipoUnidade.getValidValue(A228ContagemItem_FSTipoUnidade);
            n228ContagemItem_FSTipoUnidade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A228ContagemItem_FSTipoUnidade", A228ContagemItem_FSTipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSTIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A228ContagemItem_FSTipoUnidade, ""))));
         }
         if ( cmbContagemItem_FMValidacao.ItemCount > 0 )
         {
            A268ContagemItem_FMValidacao = (short)(NumberUtil.Val( cmbContagemItem_FMValidacao.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0))), "."));
            n268ContagemItem_FMValidacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A268ContagemItem_FMValidacao", StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMVALIDACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A268ContagemItem_FMValidacao), "9")));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF552( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "ContagemItemGeneral";
         context.Gx_err = 0;
      }

      protected void RF552( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00558 */
            pr_default.execute(2, new Object[] {A224ContagemItem_Lancamento});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A214Contagem_UsuarioContadorPessoaCod = H00558_A214Contagem_UsuarioContadorPessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
               n214Contagem_UsuarioContadorPessoaCod = H00558_n214Contagem_UsuarioContadorPessoaCod[0];
               A213Contagem_UsuarioContadorCod = H00558_A213Contagem_UsuarioContadorCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
               n213Contagem_UsuarioContadorCod = H00558_n213Contagem_UsuarioContadorCod[0];
               A208Contagem_FabricaSoftwarePessoaCod = H00558_A208Contagem_FabricaSoftwarePessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A208Contagem_FabricaSoftwarePessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_FABRICASOFTWAREPESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9")));
               A207Contagem_FabricaSoftwareCod = H00558_A207Contagem_FabricaSoftwareCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A207Contagem_FabricaSoftwareCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_FABRICASOFTWARECOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9")));
               A226ContagemItem_FSReferenciaTecnicaCod = H00558_A226ContagemItem_FSReferenciaTecnicaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSREFERENCIATECNICACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9")));
               n226ContagemItem_FSReferenciaTecnicaCod = H00558_n226ContagemItem_FSReferenciaTecnicaCod[0];
               A217Contagem_UsuarioAuditorPessoaCod = H00558_A217Contagem_UsuarioAuditorPessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A217Contagem_UsuarioAuditorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_USUARIOAUDITORPESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9")));
               A216Contagem_UsuarioAuditorCod = H00558_A216Contagem_UsuarioAuditorCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A216Contagem_UsuarioAuditorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_USUARIOAUDITORCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9")));
               A255ContagemItem_FMReferenciaTecnicaCod = H00558_A255ContagemItem_FMReferenciaTecnicaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMREFERENCIATECNICACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), "ZZZZZ9")));
               n255ContagemItem_FMReferenciaTecnicaCod = H00558_n255ContagemItem_FMReferenciaTecnicaCod[0];
               A242ContagemItem_FMPontoFuncaoLiquido = H00558_A242ContagemItem_FMPontoFuncaoLiquido[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A242ContagemItem_FMPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A242ContagemItem_FMPontoFuncaoLiquido, 7, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMPONTOFUNCAOLIQUIDO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A242ContagemItem_FMPontoFuncaoLiquido, "ZZZ9.99")));
               n242ContagemItem_FMPontoFuncaoLiquido = H00558_n242ContagemItem_FMPontoFuncaoLiquido[0];
               A241ContagemItem_FMPontoFuncaoBruto = H00558_A241ContagemItem_FMPontoFuncaoBruto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A241ContagemItem_FMPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A241ContagemItem_FMPontoFuncaoBruto, 7, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMPONTOFUNCAOBRUTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A241ContagemItem_FMPontoFuncaoBruto, "ZZZ9.99")));
               n241ContagemItem_FMPontoFuncaoBruto = H00558_n241ContagemItem_FMPontoFuncaoBruto[0];
               A240ContagemItem_FMCP = H00558_A240ContagemItem_FMCP[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A240ContagemItem_FMCP", A240ContagemItem_FMCP);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMCP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A240ContagemItem_FMCP, ""))));
               n240ContagemItem_FMCP = H00558_n240ContagemItem_FMCP[0];
               A239ContagemItem_FMQTDInm = H00558_A239ContagemItem_FMQTDInm[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A239ContagemItem_FMQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A239ContagemItem_FMQTDInm), 9, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMQTDINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A239ContagemItem_FMQTDInm), "ZZZZZZZZ9")));
               n239ContagemItem_FMQTDInm = H00558_n239ContagemItem_FMQTDInm[0];
               A256ContagemItem_FMReferenciaTecnicaVal = H00558_A256ContagemItem_FMReferenciaTecnicaVal[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A256ContagemItem_FMReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5)));
               n256ContagemItem_FMReferenciaTecnicaVal = H00558_n256ContagemItem_FMReferenciaTecnicaVal[0];
               A257ContagemItem_FMReferenciaTecnicaNom = H00558_A257ContagemItem_FMReferenciaTecnicaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A257ContagemItem_FMReferenciaTecnicaNom", A257ContagemItem_FMReferenciaTecnicaNom);
               n257ContagemItem_FMReferenciaTecnicaNom = H00558_n257ContagemItem_FMReferenciaTecnicaNom[0];
               A218Contagem_UsuarioAuditorPessoaNom = H00558_A218Contagem_UsuarioAuditorPessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A218Contagem_UsuarioAuditorPessoaNom", A218Contagem_UsuarioAuditorPessoaNom);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_USUARIOAUDITORPESSOANOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A218Contagem_UsuarioAuditorPessoaNom, "@!"))));
               A238ContagemItem_FSEvidencias = H00558_A238ContagemItem_FSEvidencias[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A238ContagemItem_FSEvidencias", A238ContagemItem_FSEvidencias);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSEVIDENCIAS", GetSecureSignedToken( sPrefix, A238ContagemItem_FSEvidencias));
               n238ContagemItem_FSEvidencias = H00558_n238ContagemItem_FSEvidencias[0];
               A237ContagemItem_FSPontoFuncaoLiquido = H00558_A237ContagemItem_FSPontoFuncaoLiquido[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A237ContagemItem_FSPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A237ContagemItem_FSPontoFuncaoLiquido, 7, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSPONTOFUNCAOLIQUIDO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A237ContagemItem_FSPontoFuncaoLiquido, "ZZZ9.99")));
               n237ContagemItem_FSPontoFuncaoLiquido = H00558_n237ContagemItem_FSPontoFuncaoLiquido[0];
               A236ContagemItem_FSPontoFuncaoBruto = H00558_A236ContagemItem_FSPontoFuncaoBruto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A236ContagemItem_FSPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A236ContagemItem_FSPontoFuncaoBruto, 7, 2)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSPONTOFUNCAOBRUTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A236ContagemItem_FSPontoFuncaoBruto, "ZZZ9.99")));
               n236ContagemItem_FSPontoFuncaoBruto = H00558_n236ContagemItem_FSPontoFuncaoBruto[0];
               A235ContagemItem_FSCP = H00558_A235ContagemItem_FSCP[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A235ContagemItem_FSCP", A235ContagemItem_FSCP);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSCP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A235ContagemItem_FSCP, ""))));
               n235ContagemItem_FSCP = H00558_n235ContagemItem_FSCP[0];
               A234ContagemItem_FSQTDInm = H00558_A234ContagemItem_FSQTDInm[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A234ContagemItem_FSQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A234ContagemItem_FSQTDInm), 9, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSQTDINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A234ContagemItem_FSQTDInm), "ZZZZZZZZ9")));
               n234ContagemItem_FSQTDInm = H00558_n234ContagemItem_FSQTDInm[0];
               A227ContagemItem_FSReferenciaTecnicaVal = H00558_A227ContagemItem_FSReferenciaTecnicaVal[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A227ContagemItem_FSReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5)));
               n227ContagemItem_FSReferenciaTecnicaVal = H00558_n227ContagemItem_FSReferenciaTecnicaVal[0];
               A269ContagemItem_FSReferenciaTecnicaNom = H00558_A269ContagemItem_FSReferenciaTecnicaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A269ContagemItem_FSReferenciaTecnicaNom", A269ContagemItem_FSReferenciaTecnicaNom);
               n269ContagemItem_FSReferenciaTecnicaNom = H00558_n269ContagemItem_FSReferenciaTecnicaNom[0];
               A228ContagemItem_FSTipoUnidade = H00558_A228ContagemItem_FSTipoUnidade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A228ContagemItem_FSTipoUnidade", A228ContagemItem_FSTipoUnidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSTIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A228ContagemItem_FSTipoUnidade, ""))));
               n228ContagemItem_FSTipoUnidade = H00558_n228ContagemItem_FSTipoUnidade[0];
               A215Contagem_UsuarioContadorPessoaNom = H00558_A215Contagem_UsuarioContadorPessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
               n215Contagem_UsuarioContadorPessoaNom = H00558_n215Contagem_UsuarioContadorPessoaNom[0];
               A209Contagem_FabricaSoftwarePessoaNom = H00558_A209Contagem_FabricaSoftwarePessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A209Contagem_FabricaSoftwarePessoaNom", A209Contagem_FabricaSoftwarePessoaNom);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_FABRICASOFTWAREPESSOANOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A209Contagem_FabricaSoftwarePessoaNom, "@!"))));
               A166FuncaoAPF_Nome = H00558_A166FuncaoAPF_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               A229ContagemItem_Requisito = H00558_A229ContagemItem_Requisito[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A229ContagemItem_Requisito", StringUtil.LTrim( StringUtil.Str( (decimal)(A229ContagemItem_Requisito), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_REQUISITO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A229ContagemItem_Requisito), "ZZ9")));
               n229ContagemItem_Requisito = H00558_n229ContagemItem_Requisito[0];
               A225ContagemItem_Sequencial = H00558_A225ContagemItem_Sequencial[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A225ContagemItem_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A225ContagemItem_Sequencial), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_SEQUENCIAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
               n225ContagemItem_Sequencial = H00558_n225ContagemItem_Sequencial[0];
               A196Contagem_Tipo = H00558_A196Contagem_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A196Contagem_Tipo", A196Contagem_Tipo);
               n196Contagem_Tipo = H00558_n196Contagem_Tipo[0];
               A195Contagem_Tecnica = H00558_A195Contagem_Tecnica[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
               n195Contagem_Tecnica = H00558_n195Contagem_Tecnica[0];
               A192Contagem_Codigo = H00558_A192Contagem_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
               A958ContagemItem_Funcao = H00558_A958ContagemItem_Funcao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A958ContagemItem_Funcao", A958ContagemItem_Funcao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FUNCAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A958ContagemItem_Funcao, ""))));
               n958ContagemItem_Funcao = H00558_n958ContagemItem_Funcao[0];
               A957ContagemItem_DER = H00558_A957ContagemItem_DER[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A957ContagemItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A957ContagemItem_DER), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_DER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9")));
               n957ContagemItem_DER = H00558_n957ContagemItem_DER[0];
               A956ContagemItem_RA = H00558_A956ContagemItem_RA[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A956ContagemItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A956ContagemItem_RA), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_RA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9")));
               n956ContagemItem_RA = H00558_n956ContagemItem_RA[0];
               A955ContagemItem_CP = H00558_A955ContagemItem_CP[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A955ContagemItem_CP", A955ContagemItem_CP);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_CP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A955ContagemItem_CP, ""))));
               n955ContagemItem_CP = H00558_n955ContagemItem_CP[0];
               A954ContagemItem_QtdINM = H00558_A954ContagemItem_QtdINM[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A954ContagemItem_QtdINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A954ContagemItem_QtdINM), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_QTDINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A954ContagemItem_QtdINM), "ZZZ9")));
               n954ContagemItem_QtdINM = H00558_n954ContagemItem_QtdINM[0];
               A953ContagemItem_Evidencias = H00558_A953ContagemItem_Evidencias[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A953ContagemItem_Evidencias", A953ContagemItem_Evidencias);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_EVIDENCIAS", GetSecureSignedToken( sPrefix, A953ContagemItem_Evidencias));
               n953ContagemItem_Evidencias = H00558_n953ContagemItem_Evidencias[0];
               A952ContagemItem_TipoUnidade = H00558_A952ContagemItem_TipoUnidade[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A952ContagemItem_TipoUnidade", A952ContagemItem_TipoUnidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_TIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
               A184FuncaoAPF_Tipo = H00558_A184FuncaoAPF_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
               A951ContagemItem_PFL = H00558_A951ContagemItem_PFL[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A951ContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( A951ContagemItem_PFL, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_PFL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")));
               n951ContagemItem_PFL = H00558_n951ContagemItem_PFL[0];
               A950ContagemItem_PFB = H00558_A950ContagemItem_PFB[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A950ContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( A950ContagemItem_PFB, 14, 5)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_PFB", GetSecureSignedToken( sPrefix, context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")));
               n950ContagemItem_PFB = H00558_n950ContagemItem_PFB[0];
               A194Contagem_AreaTrabalhoDes = H00558_A194Contagem_AreaTrabalhoDes[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
               n194Contagem_AreaTrabalhoDes = H00558_n194Contagem_AreaTrabalhoDes[0];
               A193Contagem_AreaTrabalhoCod = H00558_A193Contagem_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
               A197Contagem_DataCriacao = H00558_A197Contagem_DataCriacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               A251ContagemItem_FMDER = H00558_A251ContagemItem_FMDER[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
               n251ContagemItem_FMDER = H00558_n251ContagemItem_FMDER[0];
               A232ContagemItem_FSDER = H00558_A232ContagemItem_FSDER[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A232ContagemItem_FSDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A232ContagemItem_FSDER), 3, 0)));
               n232ContagemItem_FSDER = H00558_n232ContagemItem_FSDER[0];
               A165FuncaoAPF_Codigo = H00558_A165FuncaoAPF_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
               n165FuncaoAPF_Codigo = H00558_n165FuncaoAPF_Codigo[0];
               A268ContagemItem_FMValidacao = H00558_A268ContagemItem_FMValidacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A268ContagemItem_FMValidacao", StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMVALIDACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A268ContagemItem_FMValidacao), "9")));
               n268ContagemItem_FMValidacao = H00558_n268ContagemItem_FMValidacao[0];
               A267ContagemItem_FSValidacao = H00558_A267ContagemItem_FSValidacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A267ContagemItem_FSValidacao", StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSVALIDACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A267ContagemItem_FSValidacao), "9")));
               n267ContagemItem_FSValidacao = H00558_n267ContagemItem_FSValidacao[0];
               A227ContagemItem_FSReferenciaTecnicaVal = H00558_A227ContagemItem_FSReferenciaTecnicaVal[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A227ContagemItem_FSReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5)));
               n227ContagemItem_FSReferenciaTecnicaVal = H00558_n227ContagemItem_FSReferenciaTecnicaVal[0];
               A269ContagemItem_FSReferenciaTecnicaNom = H00558_A269ContagemItem_FSReferenciaTecnicaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A269ContagemItem_FSReferenciaTecnicaNom", A269ContagemItem_FSReferenciaTecnicaNom);
               n269ContagemItem_FSReferenciaTecnicaNom = H00558_n269ContagemItem_FSReferenciaTecnicaNom[0];
               A256ContagemItem_FMReferenciaTecnicaVal = H00558_A256ContagemItem_FMReferenciaTecnicaVal[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A256ContagemItem_FMReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5)));
               n256ContagemItem_FMReferenciaTecnicaVal = H00558_n256ContagemItem_FMReferenciaTecnicaVal[0];
               A257ContagemItem_FMReferenciaTecnicaNom = H00558_A257ContagemItem_FMReferenciaTecnicaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A257ContagemItem_FMReferenciaTecnicaNom", A257ContagemItem_FMReferenciaTecnicaNom);
               n257ContagemItem_FMReferenciaTecnicaNom = H00558_n257ContagemItem_FMReferenciaTecnicaNom[0];
               A213Contagem_UsuarioContadorCod = H00558_A213Contagem_UsuarioContadorCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
               n213Contagem_UsuarioContadorCod = H00558_n213Contagem_UsuarioContadorCod[0];
               A196Contagem_Tipo = H00558_A196Contagem_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A196Contagem_Tipo", A196Contagem_Tipo);
               n196Contagem_Tipo = H00558_n196Contagem_Tipo[0];
               A195Contagem_Tecnica = H00558_A195Contagem_Tecnica[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
               n195Contagem_Tecnica = H00558_n195Contagem_Tecnica[0];
               A193Contagem_AreaTrabalhoCod = H00558_A193Contagem_AreaTrabalhoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
               A197Contagem_DataCriacao = H00558_A197Contagem_DataCriacao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
               A214Contagem_UsuarioContadorPessoaCod = H00558_A214Contagem_UsuarioContadorPessoaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
               n214Contagem_UsuarioContadorPessoaCod = H00558_n214Contagem_UsuarioContadorPessoaCod[0];
               A215Contagem_UsuarioContadorPessoaNom = H00558_A215Contagem_UsuarioContadorPessoaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
               n215Contagem_UsuarioContadorPessoaNom = H00558_n215Contagem_UsuarioContadorPessoaNom[0];
               A194Contagem_AreaTrabalhoDes = H00558_A194Contagem_AreaTrabalhoDes[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
               n194Contagem_AreaTrabalhoDes = H00558_n194Contagem_AreaTrabalhoDes[0];
               A166FuncaoAPF_Nome = H00558_A166FuncaoAPF_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
               A184FuncaoAPF_Tipo = H00558_A184FuncaoAPF_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
               A251ContagemItem_FMDER = H00558_A251ContagemItem_FMDER[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
               n251ContagemItem_FMDER = H00558_n251ContagemItem_FMDER[0];
               A232ContagemItem_FSDER = H00558_A232ContagemItem_FSDER[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A232ContagemItem_FSDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A232ContagemItem_FSDER), 3, 0)));
               n232ContagemItem_FSDER = H00558_n232ContagemItem_FSDER[0];
               if ( ( ( A267ContagemItem_FSValidacao == 1 ) ) && ( ( A268ContagemItem_FMValidacao == 1 ) ) )
               {
                  A260ContagemItem_ValidacaoStatusFinal = 1;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_VALIDACAOSTATUSFINAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A260ContagemItem_ValidacaoStatusFinal), "9")));
               }
               else
               {
                  A260ContagemItem_ValidacaoStatusFinal = 2;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_VALIDACAOSTATUSFINAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A260ContagemItem_ValidacaoStatusFinal), "9")));
               }
               GXt_char2 = A185FuncaoAPF_Complexidade;
               new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
               A185FuncaoAPF_Complexidade = GXt_char2;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
               /* Execute user event: E12552 */
               E12552 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(2);
            WB550( ) ;
         }
      }

      protected void STRUP550( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "ContagemItemGeneral";
         context.Gx_err = 0;
         /* Using cursor H005510 */
         pr_default.execute(3);
         if ( (pr_default.getStatus(3) != 101) )
         {
            A232ContagemItem_FSDER = H005510_A232ContagemItem_FSDER[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A232ContagemItem_FSDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A232ContagemItem_FSDER), 3, 0)));
            n232ContagemItem_FSDER = H005510_n232ContagemItem_FSDER[0];
         }
         else
         {
            A232ContagemItem_FSDER = 0;
            n232ContagemItem_FSDER = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A232ContagemItem_FSDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A232ContagemItem_FSDER), 3, 0)));
         }
         pr_default.close(3);
         /* Using cursor H005512 */
         pr_default.execute(4, new Object[] {A224ContagemItem_Lancamento});
         if ( (pr_default.getStatus(4) != 101) )
         {
            A251ContagemItem_FMDER = H005512_A251ContagemItem_FMDER[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
            n251ContagemItem_FMDER = H005512_n251ContagemItem_FMDER[0];
         }
         else
         {
            A251ContagemItem_FMDER = 0;
            n251ContagemItem_FMDER = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
         }
         pr_default.close(4);
         GXt_int1 = A233ContagemItem_FSAR;
         new prc_calculaaruniquefs(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         A233ContagemItem_FSAR = (short)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A233ContagemItem_FSAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A233ContagemItem_FSAR), 3, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSAR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A233ContagemItem_FSAR), "ZZ9")));
         GXt_int1 = A252ContagemItem_FMAR;
         new prc_calculaaruniquefm(context ).execute(  A224ContagemItem_Lancamento, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         A252ContagemItem_FMAR = (short)(GXt_int1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A252ContagemItem_FMAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A252ContagemItem_FMAR), 3, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMAR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A252ContagemItem_FMAR), "ZZ9")));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11552 */
         E11552 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A197Contagem_DataCriacao = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtContagem_DataCriacao_Internalname), 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A197Contagem_DataCriacao", context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"));
            A193Contagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_AreaTrabalhoCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A193Contagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0)));
            A194Contagem_AreaTrabalhoDes = StringUtil.Upper( cgiGet( edtContagem_AreaTrabalhoDes_Internalname));
            n194Contagem_AreaTrabalhoDes = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A194Contagem_AreaTrabalhoDes", A194Contagem_AreaTrabalhoDes);
            A950ContagemItem_PFB = context.localUtil.CToN( cgiGet( edtContagemItem_PFB_Internalname), ",", ".");
            n950ContagemItem_PFB = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A950ContagemItem_PFB", StringUtil.LTrim( StringUtil.Str( A950ContagemItem_PFB, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_PFB", GetSecureSignedToken( sPrefix, context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999")));
            A951ContagemItem_PFL = context.localUtil.CToN( cgiGet( edtContagemItem_PFL_Internalname), ",", ".");
            n951ContagemItem_PFL = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A951ContagemItem_PFL", StringUtil.LTrim( StringUtil.Str( A951ContagemItem_PFL, 14, 5)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_PFL", GetSecureSignedToken( sPrefix, context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999")));
            cmbFuncaoAPF_Tipo.CurrentValue = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
            A184FuncaoAPF_Tipo = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A184FuncaoAPF_Tipo", A184FuncaoAPF_Tipo);
            cmbContagemItem_TipoUnidade.CurrentValue = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
            A952ContagemItem_TipoUnidade = cgiGet( cmbContagemItem_TipoUnidade_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A952ContagemItem_TipoUnidade", A952ContagemItem_TipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_TIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A952ContagemItem_TipoUnidade, ""))));
            A953ContagemItem_Evidencias = cgiGet( edtContagemItem_Evidencias_Internalname);
            n953ContagemItem_Evidencias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A953ContagemItem_Evidencias", A953ContagemItem_Evidencias);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_EVIDENCIAS", GetSecureSignedToken( sPrefix, A953ContagemItem_Evidencias));
            A954ContagemItem_QtdINM = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_QtdINM_Internalname), ",", "."));
            n954ContagemItem_QtdINM = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A954ContagemItem_QtdINM", StringUtil.LTrim( StringUtil.Str( (decimal)(A954ContagemItem_QtdINM), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_QTDINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A954ContagemItem_QtdINM), "ZZZ9")));
            A955ContagemItem_CP = cgiGet( edtContagemItem_CP_Internalname);
            n955ContagemItem_CP = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A955ContagemItem_CP", A955ContagemItem_CP);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_CP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A955ContagemItem_CP, ""))));
            A956ContagemItem_RA = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_RA_Internalname), ",", "."));
            n956ContagemItem_RA = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A956ContagemItem_RA", StringUtil.LTrim( StringUtil.Str( (decimal)(A956ContagemItem_RA), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_RA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9")));
            A957ContagemItem_DER = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_DER_Internalname), ",", "."));
            n957ContagemItem_DER = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A957ContagemItem_DER", StringUtil.LTrim( StringUtil.Str( (decimal)(A957ContagemItem_DER), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_DER", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9")));
            A958ContagemItem_Funcao = cgiGet( edtContagemItem_Funcao_Internalname);
            n958ContagemItem_Funcao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A958ContagemItem_Funcao", A958ContagemItem_Funcao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FUNCAO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A958ContagemItem_Funcao, ""))));
            cmbFuncaoAPF_Complexidade.CurrentValue = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
            A185FuncaoAPF_Complexidade = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A185FuncaoAPF_Complexidade", A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
            A192Contagem_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagem_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A192Contagem_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A192Contagem_Codigo), 6, 0)));
            cmbContagem_Tecnica.CurrentValue = cgiGet( cmbContagem_Tecnica_Internalname);
            A195Contagem_Tecnica = cgiGet( cmbContagem_Tecnica_Internalname);
            n195Contagem_Tecnica = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A195Contagem_Tecnica", A195Contagem_Tecnica);
            cmbContagem_Tipo.CurrentValue = cgiGet( cmbContagem_Tipo_Internalname);
            A196Contagem_Tipo = cgiGet( cmbContagem_Tipo_Internalname);
            n196Contagem_Tipo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A196Contagem_Tipo", A196Contagem_Tipo);
            A225ContagemItem_Sequencial = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_Sequencial_Internalname), ",", "."));
            n225ContagemItem_Sequencial = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A225ContagemItem_Sequencial", StringUtil.LTrim( StringUtil.Str( (decimal)(A225ContagemItem_Sequencial), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_SEQUENCIAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9")));
            cmbContagemItem_ValidacaoStatusFinal.CurrentValue = cgiGet( cmbContagemItem_ValidacaoStatusFinal_Internalname);
            A260ContagemItem_ValidacaoStatusFinal = (short)(NumberUtil.Val( cgiGet( cmbContagemItem_ValidacaoStatusFinal_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A260ContagemItem_ValidacaoStatusFinal", StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_VALIDACAOSTATUSFINAL", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A260ContagemItem_ValidacaoStatusFinal), "9")));
            A229ContagemItem_Requisito = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_Requisito_Internalname), ",", "."));
            n229ContagemItem_Requisito = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A229ContagemItem_Requisito", StringUtil.LTrim( StringUtil.Str( (decimal)(A229ContagemItem_Requisito), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_REQUISITO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A229ContagemItem_Requisito), "ZZ9")));
            A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A166FuncaoAPF_Nome", A166FuncaoAPF_Nome);
            A209Contagem_FabricaSoftwarePessoaNom = StringUtil.Upper( cgiGet( edtContagem_FabricaSoftwarePessoaNom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A209Contagem_FabricaSoftwarePessoaNom", A209Contagem_FabricaSoftwarePessoaNom);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_FABRICASOFTWAREPESSOANOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A209Contagem_FabricaSoftwarePessoaNom, "@!"))));
            A215Contagem_UsuarioContadorPessoaNom = StringUtil.Upper( cgiGet( edtContagem_UsuarioContadorPessoaNom_Internalname));
            n215Contagem_UsuarioContadorPessoaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A215Contagem_UsuarioContadorPessoaNom", A215Contagem_UsuarioContadorPessoaNom);
            cmbContagemItem_FSValidacao.CurrentValue = cgiGet( cmbContagemItem_FSValidacao_Internalname);
            A267ContagemItem_FSValidacao = (short)(NumberUtil.Val( cgiGet( cmbContagemItem_FSValidacao_Internalname), "."));
            n267ContagemItem_FSValidacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A267ContagemItem_FSValidacao", StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSVALIDACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A267ContagemItem_FSValidacao), "9")));
            cmbContagemItem_FSTipoUnidade.CurrentValue = cgiGet( cmbContagemItem_FSTipoUnidade_Internalname);
            A228ContagemItem_FSTipoUnidade = cgiGet( cmbContagemItem_FSTipoUnidade_Internalname);
            n228ContagemItem_FSTipoUnidade = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A228ContagemItem_FSTipoUnidade", A228ContagemItem_FSTipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSTIPOUNIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A228ContagemItem_FSTipoUnidade, ""))));
            A269ContagemItem_FSReferenciaTecnicaNom = StringUtil.Upper( cgiGet( edtContagemItem_FSReferenciaTecnicaNom_Internalname));
            n269ContagemItem_FSReferenciaTecnicaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A269ContagemItem_FSReferenciaTecnicaNom", A269ContagemItem_FSReferenciaTecnicaNom);
            A227ContagemItem_FSReferenciaTecnicaVal = context.localUtil.CToN( cgiGet( edtContagemItem_FSReferenciaTecnicaVal_Internalname), ",", ".");
            n227ContagemItem_FSReferenciaTecnicaVal = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A227ContagemItem_FSReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5)));
            A232ContagemItem_FSDER = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_FSDER_Internalname), ",", "."));
            n232ContagemItem_FSDER = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A232ContagemItem_FSDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A232ContagemItem_FSDER), 3, 0)));
            A233ContagemItem_FSAR = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_FSAR_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A233ContagemItem_FSAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A233ContagemItem_FSAR), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSAR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A233ContagemItem_FSAR), "ZZ9")));
            A234ContagemItem_FSQTDInm = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FSQTDInm_Internalname), ",", "."));
            n234ContagemItem_FSQTDInm = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A234ContagemItem_FSQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A234ContagemItem_FSQTDInm), 9, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSQTDINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A234ContagemItem_FSQTDInm), "ZZZZZZZZ9")));
            A235ContagemItem_FSCP = cgiGet( edtContagemItem_FSCP_Internalname);
            n235ContagemItem_FSCP = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A235ContagemItem_FSCP", A235ContagemItem_FSCP);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSCP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A235ContagemItem_FSCP, ""))));
            A236ContagemItem_FSPontoFuncaoBruto = context.localUtil.CToN( cgiGet( edtContagemItem_FSPontoFuncaoBruto_Internalname), ",", ".");
            n236ContagemItem_FSPontoFuncaoBruto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A236ContagemItem_FSPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A236ContagemItem_FSPontoFuncaoBruto, 7, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSPONTOFUNCAOBRUTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A236ContagemItem_FSPontoFuncaoBruto, "ZZZ9.99")));
            A237ContagemItem_FSPontoFuncaoLiquido = context.localUtil.CToN( cgiGet( edtContagemItem_FSPontoFuncaoLiquido_Internalname), ",", ".");
            n237ContagemItem_FSPontoFuncaoLiquido = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A237ContagemItem_FSPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A237ContagemItem_FSPontoFuncaoLiquido, 7, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSPONTOFUNCAOLIQUIDO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A237ContagemItem_FSPontoFuncaoLiquido, "ZZZ9.99")));
            A238ContagemItem_FSEvidencias = cgiGet( edtContagemItem_FSEvidencias_Internalname);
            n238ContagemItem_FSEvidencias = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A238ContagemItem_FSEvidencias", A238ContagemItem_FSEvidencias);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSEVIDENCIAS", GetSecureSignedToken( sPrefix, A238ContagemItem_FSEvidencias));
            A218Contagem_UsuarioAuditorPessoaNom = StringUtil.Upper( cgiGet( edtContagem_UsuarioAuditorPessoaNom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A218Contagem_UsuarioAuditorPessoaNom", A218Contagem_UsuarioAuditorPessoaNom);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_USUARIOAUDITORPESSOANOM", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A218Contagem_UsuarioAuditorPessoaNom, "@!"))));
            cmbContagemItem_FMValidacao.CurrentValue = cgiGet( cmbContagemItem_FMValidacao_Internalname);
            A268ContagemItem_FMValidacao = (short)(NumberUtil.Val( cgiGet( cmbContagemItem_FMValidacao_Internalname), "."));
            n268ContagemItem_FMValidacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A268ContagemItem_FMValidacao", StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMVALIDACAO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A268ContagemItem_FMValidacao), "9")));
            A257ContagemItem_FMReferenciaTecnicaNom = StringUtil.Upper( cgiGet( edtContagemItem_FMReferenciaTecnicaNom_Internalname));
            n257ContagemItem_FMReferenciaTecnicaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A257ContagemItem_FMReferenciaTecnicaNom", A257ContagemItem_FMReferenciaTecnicaNom);
            A256ContagemItem_FMReferenciaTecnicaVal = context.localUtil.CToN( cgiGet( edtContagemItem_FMReferenciaTecnicaVal_Internalname), ",", ".");
            n256ContagemItem_FMReferenciaTecnicaVal = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A256ContagemItem_FMReferenciaTecnicaVal", StringUtil.LTrim( StringUtil.Str( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5)));
            A251ContagemItem_FMDER = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_FMDER_Internalname), ",", "."));
            n251ContagemItem_FMDER = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A251ContagemItem_FMDER", StringUtil.LTrim( StringUtil.Str( (decimal)(A251ContagemItem_FMDER), 3, 0)));
            A252ContagemItem_FMAR = (short)(context.localUtil.CToN( cgiGet( edtContagemItem_FMAR_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A252ContagemItem_FMAR", StringUtil.LTrim( StringUtil.Str( (decimal)(A252ContagemItem_FMAR), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMAR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A252ContagemItem_FMAR), "ZZ9")));
            A239ContagemItem_FMQTDInm = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FMQTDInm_Internalname), ",", "."));
            n239ContagemItem_FMQTDInm = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A239ContagemItem_FMQTDInm", StringUtil.LTrim( StringUtil.Str( (decimal)(A239ContagemItem_FMQTDInm), 9, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMQTDINM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A239ContagemItem_FMQTDInm), "ZZZZZZZZ9")));
            A240ContagemItem_FMCP = cgiGet( edtContagemItem_FMCP_Internalname);
            n240ContagemItem_FMCP = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A240ContagemItem_FMCP", A240ContagemItem_FMCP);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMCP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A240ContagemItem_FMCP, ""))));
            A241ContagemItem_FMPontoFuncaoBruto = context.localUtil.CToN( cgiGet( edtContagemItem_FMPontoFuncaoBruto_Internalname), ",", ".");
            n241ContagemItem_FMPontoFuncaoBruto = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A241ContagemItem_FMPontoFuncaoBruto", StringUtil.LTrim( StringUtil.Str( A241ContagemItem_FMPontoFuncaoBruto, 7, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMPONTOFUNCAOBRUTO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A241ContagemItem_FMPontoFuncaoBruto, "ZZZ9.99")));
            A242ContagemItem_FMPontoFuncaoLiquido = context.localUtil.CToN( cgiGet( edtContagemItem_FMPontoFuncaoLiquido_Internalname), ",", ".");
            n242ContagemItem_FMPontoFuncaoLiquido = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A242ContagemItem_FMPontoFuncaoLiquido", StringUtil.LTrim( StringUtil.Str( A242ContagemItem_FMPontoFuncaoLiquido, 7, 2)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMPONTOFUNCAOLIQUIDO", GetSecureSignedToken( sPrefix, context.localUtil.Format( A242ContagemItem_FMPontoFuncaoLiquido, "ZZZ9.99")));
            A255ContagemItem_FMReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FMReferenciaTecnicaCod_Internalname), ",", "."));
            n255ContagemItem_FMReferenciaTecnicaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMREFERENCIATECNICACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), "ZZZZZ9")));
            A216Contagem_UsuarioAuditorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A216Contagem_UsuarioAuditorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A216Contagem_UsuarioAuditorCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_USUARIOAUDITORCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A216Contagem_UsuarioAuditorCod), "ZZZZZ9")));
            A217Contagem_UsuarioAuditorPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioAuditorPessoaCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A217Contagem_UsuarioAuditorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_USUARIOAUDITORPESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A217Contagem_UsuarioAuditorPessoaCod), "ZZZZZ9")));
            A226ContagemItem_FSReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FSReferenciaTecnicaCod_Internalname), ",", "."));
            n226ContagemItem_FSReferenciaTecnicaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSREFERENCIATECNICACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9")));
            A207Contagem_FabricaSoftwareCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwareCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A207Contagem_FabricaSoftwareCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A207Contagem_FabricaSoftwareCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_FABRICASOFTWARECOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A207Contagem_FabricaSoftwareCod), "ZZZZZ9")));
            A208Contagem_FabricaSoftwarePessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_FabricaSoftwarePessoaCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A208Contagem_FabricaSoftwarePessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEM_FABRICASOFTWAREPESSOACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A208Contagem_FabricaSoftwarePessoaCod), "ZZZZZ9")));
            A213Contagem_UsuarioContadorCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorCod_Internalname), ",", "."));
            n213Contagem_UsuarioContadorCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A213Contagem_UsuarioContadorCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A213Contagem_UsuarioContadorCod), 6, 0)));
            A214Contagem_UsuarioContadorPessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagem_UsuarioContadorPessoaCod_Internalname), ",", "."));
            n214Contagem_UsuarioContadorPessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A214Contagem_UsuarioContadorPessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A214Contagem_UsuarioContadorPessoaCod), 6, 0)));
            A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
            n165FuncaoAPF_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA224ContagemItem_Lancamento"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContagemItemGeneral";
            A255ContagemItem_FMReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FMReferenciaTecnicaCod_Internalname), ",", "."));
            n255ContagemItem_FMReferenciaTecnicaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A255ContagemItem_FMReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FMREFERENCIATECNICACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), "ZZZZZ9");
            A226ContagemItem_FSReferenciaTecnicaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemItem_FSReferenciaTecnicaCod_Internalname), ",", "."));
            n226ContagemItem_FSReferenciaTecnicaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A226ContagemItem_FSReferenciaTecnicaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CONTAGEMITEM_FSREFERENCIATECNICACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9");
            A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
            n165FuncaoAPF_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A165FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A165FuncaoAPF_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contagemitemgeneral:[SecurityCheckFailed value for]"+"ContagemItem_FMReferenciaTecnicaCod:"+context.localUtil.Format( (decimal)(A255ContagemItem_FMReferenciaTecnicaCod), "ZZZZZ9"));
               GXUtil.WriteLog("contagemitemgeneral:[SecurityCheckFailed value for]"+"ContagemItem_FSReferenciaTecnicaCod:"+context.localUtil.Format( (decimal)(A226ContagemItem_FSReferenciaTecnicaCod), "ZZZZZ9"));
               GXUtil.WriteLog("contagemitemgeneral:[SecurityCheckFailed value for]"+"FuncaoAPF_Codigo:"+context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11552 */
         E11552 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11552( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12552( )
      {
         /* Load Routine */
         edtContagemItem_FMReferenciaTecnicaNom_Link = formatLink("viewreferenciatecnica.aspx") + "?" + UrlEncode("" +A255ContagemItem_FMReferenciaTecnicaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_FMReferenciaTecnicaNom_Internalname, "Link", edtContagemItem_FMReferenciaTecnicaNom_Link);
         edtContagemItem_FSReferenciaTecnicaNom_Link = formatLink("viewreferenciatecnica.aspx") + "?" + UrlEncode("" +A226ContagemItem_FSReferenciaTecnicaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_FSReferenciaTecnicaNom_Internalname, "Link", edtContagemItem_FSReferenciaTecnicaNom_Link);
         edtFuncaoAPF_Nome_Link = formatLink("viewfuncaoapf.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV12FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_Nome_Internalname, "Link", edtFuncaoAPF_Nome_Link);
         edtContagemItem_FMReferenciaTecnicaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_FMReferenciaTecnicaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FMReferenciaTecnicaCod_Visible), 5, 0)));
         edtContagem_UsuarioAuditorCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_UsuarioAuditorCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioAuditorCod_Visible), 5, 0)));
         edtContagem_UsuarioAuditorPessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_UsuarioAuditorPessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioAuditorPessoaCod_Visible), 5, 0)));
         edtContagemItem_FSReferenciaTecnicaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagemItem_FSReferenciaTecnicaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemItem_FSReferenciaTecnicaCod_Visible), 5, 0)));
         edtContagem_FabricaSoftwareCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_FabricaSoftwareCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_FabricaSoftwareCod_Visible), 5, 0)));
         edtContagem_FabricaSoftwarePessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_FabricaSoftwarePessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_FabricaSoftwarePessoaCod_Visible), 5, 0)));
         edtContagem_UsuarioContadorCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_UsuarioContadorCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorCod_Visible), 5, 0)));
         edtContagem_UsuarioContadorPessoaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContagem_UsuarioContadorPessoaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagem_UsuarioContadorPessoaCod_Visible), 5, 0)));
         edtFuncaoAPF_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_Codigo_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13552( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("contagemitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A224ContagemItem_Lancamento) + "," + UrlEncode("" +A192Contagem_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14552( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("contagemitem.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A224ContagemItem_Lancamento) + "," + UrlEncode("" +A192Contagem_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ContagemItem";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ContagemItem_Lancamento";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContagemItem_Lancamento), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_552( true) ;
         }
         else
         {
            wb_table2_8_552( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_284_552( true) ;
         }
         else
         {
            wb_table3_284_552( false) ;
         }
         return  ;
      }

      protected void wb_table3_284_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_552e( true) ;
         }
         else
         {
            wb_table1_2_552e( false) ;
         }
      }

      protected void wb_table3_284_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 287,'" + sPrefix + "',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 289,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_284_552e( true) ;
         }
         else
         {
            wb_table3_284_552e( false) ;
         }
      }

      protected void wb_table2_8_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_datacriacao_Internalname, "Data Cria��o", "", "", lblTextblockcontagem_datacriacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            context.WriteHtmlText( "<div id=\""+edtContagem_DataCriacao_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagem_DataCriacao_Internalname, context.localUtil.Format(A197Contagem_DataCriacao, "99/99/99"), context.localUtil.Format( A197Contagem_DataCriacao, "99/99/99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_DataCriacao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Data", "right", false, "HLP_ContagemItemGeneral.htm");
            GxWebStd.gx_bitmap( context, edtContagem_DataCriacao_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(0==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_areatrabalhocod_Internalname, "C�d. �rea de Trabalho", "", "", lblTextblockcontagem_areatrabalhocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_AreaTrabalhoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A193Contagem_AreaTrabalhoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A193Contagem_AreaTrabalhoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_AreaTrabalhoCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_areatrabalhodes_Internalname, "�rea de Trabalho", "", "", lblTextblockcontagem_areatrabalhodes_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_AreaTrabalhoDes_Internalname, A194Contagem_AreaTrabalhoDes, StringUtil.RTrim( context.localUtil.Format( A194Contagem_AreaTrabalhoDes, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_AreaTrabalhoDes_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_pfb_Internalname, "PFB", "", "", lblTextblockcontagemitem_pfb_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_PFB_Internalname, StringUtil.LTrim( StringUtil.NToC( A950ContagemItem_PFB, 14, 5, ",", "")), context.localUtil.Format( A950ContagemItem_PFB, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_PFB_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_pfl_Internalname, "PFL", "", "", lblTextblockcontagemitem_pfl_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_PFL_Internalname, StringUtil.LTrim( StringUtil.NToC( A951ContagemItem_PFL, 14, 5, ",", "")), context.localUtil.Format( A951ContagemItem_PFL, "ZZ,ZZZ,ZZ9.999"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_PFL_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_tipo_Internalname, "Tipo", "", "", lblTextblockfuncaoapf_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Tipo, cmbFuncaoAPF_Tipo_Internalname, StringUtil.RTrim( A184FuncaoAPF_Tipo), 1, cmbFuncaoAPF_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemItemGeneral.htm");
            cmbFuncaoAPF_Tipo.CurrentValue = StringUtil.RTrim( A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Tipo_Internalname, "Values", (String)(cmbFuncaoAPF_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_tipounidade_Internalname, "Tipo", "", "", lblTextblockcontagemitem_tipounidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemItem_TipoUnidade, cmbContagemItem_TipoUnidade_Internalname, StringUtil.RTrim( A952ContagemItem_TipoUnidade), 1, cmbContagemItem_TipoUnidade_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "svchar", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemItemGeneral.htm");
            cmbContagemItem_TipoUnidade.CurrentValue = StringUtil.RTrim( A952ContagemItem_TipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemItem_TipoUnidade_Internalname, "Values", (String)(cmbContagemItem_TipoUnidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_evidencias_Internalname, "Evidencias", "", "", lblTextblockcontagemitem_evidencias_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemItem_Evidencias_Internalname, A953ContagemItem_Evidencias, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "5000", -1, "", "", -1, true, "Evidencias", "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_qtdinm_Internalname, "INM", "", "", lblTextblockcontagemitem_qtdinm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_QtdINM_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A954ContagemItem_QtdINM), 4, 0, ",", "")), context.localUtil.Format( (decimal)(A954ContagemItem_QtdINM), "ZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_QtdINM_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_cp_Internalname, "CP", "", "", lblTextblockcontagemitem_cp_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_CP_Internalname, StringUtil.RTrim( A955ContagemItem_CP), StringUtil.RTrim( context.localUtil.Format( A955ContagemItem_CP, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_CP_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "CP", "left", true, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_ra_Internalname, "A", "", "", lblTextblockcontagemitem_ra_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_RA_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A956ContagemItem_RA), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A956ContagemItem_RA), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_RA_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_der_Internalname, "DER", "", "", lblTextblockcontagemitem_der_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_DER_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A957ContagemItem_DER), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A957ContagemItem_DER), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_DER_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_funcao_Internalname, "de Dados", "", "", lblTextblockcontagemitem_funcao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_Funcao_Internalname, A958ContagemItem_Funcao, StringUtil.RTrim( context.localUtil.Format( A958ContagemItem_Funcao, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_Funcao_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_complexidade_Internalname, "Complexidade", "", "", lblTextblockfuncaoapf_complexidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbFuncaoAPF_Complexidade, cmbFuncaoAPF_Complexidade_Internalname, StringUtil.RTrim( A185FuncaoAPF_Complexidade), 1, cmbFuncaoAPF_Complexidade_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemItemGeneral.htm");
            cmbFuncaoAPF_Complexidade.CurrentValue = StringUtil.RTrim( A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Complexidade_Internalname, "Values", (String)(cmbFuncaoAPF_Complexidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_81_552( true) ;
         }
         else
         {
            wb_table4_81_552( false) ;
         }
         return  ;
      }

      protected void wb_table4_81_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_552e( true) ;
         }
         else
         {
            wb_table2_8_552e( false) ;
         }
      }

      protected void wb_table4_81_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_84_552( true) ;
         }
         else
         {
            wb_table5_84_552( false) ;
         }
         return  ;
      }

      protected void wb_table5_84_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_126_552( true) ;
         }
         else
         {
            wb_table6_126_552( false) ;
         }
         return  ;
      }

      protected void wb_table6_126_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_81_552e( true) ;
         }
         else
         {
            wb_table4_81_552e( false) ;
         }
      }

      protected void wb_table6_126_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable3_Internalname, tblUnnamedtable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table7_129_552( true) ;
         }
         else
         {
            wb_table7_129_552( false) ;
         }
         return  ;
      }

      protected void wb_table7_129_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_212_552( true) ;
         }
         else
         {
            wb_table8_212_552( false) ;
         }
         return  ;
      }

      protected void wb_table8_212_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table9_216_552( true) ;
         }
         else
         {
            wb_table9_216_552( false) ;
         }
         return  ;
      }

      protected void wb_table9_216_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_126_552e( true) ;
         }
         else
         {
            wb_table6_126_552e( false) ;
         }
      }

      protected void wb_table9_216_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable6_Internalname, tblUnnamedtable6_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup8_Internalname, "F�brica de M�trica", 1, 0, "px", 0, "px", "Group", "", "HLP_ContagemItemGeneral.htm");
            wb_table10_220_552( true) ;
         }
         else
         {
            wb_table10_220_552( false) ;
         }
         return  ;
      }

      protected void wb_table10_220_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_216_552e( true) ;
         }
         else
         {
            wb_table9_216_552e( false) ;
         }
      }

      protected void wb_table10_220_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable7_Internalname, tblUnnamedtable7_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_usuarioauditorpessoanom_Internalname, "Auditor", "", "", lblTextblockcontagem_usuarioauditorpessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioAuditorPessoaNom_Internalname, StringUtil.RTrim( A218Contagem_UsuarioAuditorPessoaNom), StringUtil.RTrim( context.localUtil.Format( A218Contagem_UsuarioAuditorPessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioAuditorPessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmvalidacao_Internalname, "Valida��o", "", "", lblTextblockcontagemitem_fmvalidacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemItem_FMValidacao, cmbContagemItem_FMValidacao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0)), 1, cmbContagemItem_FMValidacao_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemItemGeneral.htm");
            cmbContagemItem_FMValidacao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A268ContagemItem_FMValidacao), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemItem_FMValidacao_Internalname, "Values", (String)(cmbContagemItem_FMValidacao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmreferenciatecnicanom_Internalname, "Refer�ncia", "", "", lblTextblockcontagemitem_fmreferenciatecnicanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMReferenciaTecnicaNom_Internalname, StringUtil.RTrim( A257ContagemItem_FMReferenciaTecnicaNom), StringUtil.RTrim( context.localUtil.Format( A257ContagemItem_FMReferenciaTecnicaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContagemItem_FMReferenciaTecnicaNom_Link, "", "", "", edtContagemItem_FMReferenciaTecnicaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmreferenciatecnicaval_Internalname, "Deflator / Inflator", "", "", lblTextblockcontagemitem_fmreferenciatecnicaval_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMReferenciaTecnicaVal_Internalname, StringUtil.LTrim( StringUtil.NToC( A256ContagemItem_FMReferenciaTecnicaVal, 18, 5, ",", "")), context.localUtil.Format( A256ContagemItem_FMReferenciaTecnicaVal, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMReferenciaTecnicaVal_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmder_Internalname, "DER", "", "", lblTextblockcontagemitem_fmder_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table11_245_552( true) ;
         }
         else
         {
            wb_table11_245_552( false) ;
         }
         return  ;
      }

      protected void wb_table11_245_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmqtdinm_Internalname, "Qtd. INM", "", "", lblTextblockcontagemitem_fmqtdinm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table12_261_552( true) ;
         }
         else
         {
            wb_table12_261_552( false) ;
         }
         return  ;
      }

      protected void wb_table12_261_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmpontofuncaobruto_Internalname, "PF Bruto", "", "", lblTextblockcontagemitem_fmpontofuncaobruto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table13_273_552( true) ;
         }
         else
         {
            wb_table13_273_552( false) ;
         }
         return  ;
      }

      protected void wb_table13_273_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_220_552e( true) ;
         }
         else
         {
            wb_table10_220_552e( false) ;
         }
      }

      protected void wb_table13_273_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fmpontofuncaobruto_Internalname, tblTablemergedcontagemitem_fmpontofuncaobruto_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMPontoFuncaoBruto_Internalname, StringUtil.LTrim( StringUtil.NToC( A241ContagemItem_FMPontoFuncaoBruto, 7, 2, ",", "")), context.localUtil.Format( A241ContagemItem_FMPontoFuncaoBruto, "ZZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMPontoFuncaoBruto_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "TotalPF", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmpontofuncaoliquido_Internalname, "PF L�quido", "", "", lblTextblockcontagemitem_fmpontofuncaoliquido_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMPontoFuncaoLiquido_Internalname, StringUtil.LTrim( StringUtil.NToC( A242ContagemItem_FMPontoFuncaoLiquido, 7, 2, ",", "")), context.localUtil.Format( A242ContagemItem_FMPontoFuncaoLiquido, "ZZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMPontoFuncaoLiquido_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "TotalPF", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_273_552e( true) ;
         }
         else
         {
            wb_table13_273_552e( false) ;
         }
      }

      protected void wb_table12_261_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fmqtdinm_Internalname, tblTablemergedcontagemitem_fmqtdinm_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMQTDInm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A239ContagemItem_FMQTDInm), 9, 0, ",", "")), context.localUtil.Format( (decimal)(A239ContagemItem_FMQTDInm), "ZZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMQTDInm_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "QuantidadeGrande", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmcp_Internalname, "Complexidade", "", "", lblTextblockcontagemitem_fmcp_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMCP_Internalname, StringUtil.RTrim( A240ContagemItem_FMCP), StringUtil.RTrim( context.localUtil.Format( A240ContagemItem_FMCP, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMCP_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "CP", "left", true, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_261_552e( true) ;
         }
         else
         {
            wb_table12_261_552e( false) ;
         }
      }

      protected void wb_table11_245_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fmder_Internalname, tblTablemergedcontagemitem_fmder_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMDER_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A251ContagemItem_FMDER), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A251ContagemItem_FMDER), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMDER_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemitem_fmder_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemitem_fmder_righttext_Internalname, "(Atributos)", "", "", lblContagemitem_fmder_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fmar_Internalname, "AR", "", "", lblTextblockcontagemitem_fmar_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FMAR_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A252ContagemItem_FMAR), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A252ContagemItem_FMAR), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FMAR_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemitem_fmar_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemitem_fmar_righttext_Internalname, "(Tabelas)", "", "", lblContagemitem_fmar_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_245_552e( true) ;
         }
         else
         {
            wb_table11_245_552e( false) ;
         }
      }

      protected void wb_table8_212_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable5_Internalname, tblUnnamedtable5_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_212_552e( true) ;
         }
         else
         {
            wb_table8_212_552e( false) ;
         }
      }

      protected void wb_table7_129_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable4_Internalname, tblUnnamedtable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Control Group */
            GxWebStd.gx_group_start( context, grpUnnamedgroup10_Internalname, "F�brica de Software", 1, 0, "px", 0, "px", "Group", "", "HLP_ContagemItemGeneral.htm");
            wb_table14_133_552( true) ;
         }
         else
         {
            wb_table14_133_552( false) ;
         }
         return  ;
      }

      protected void wb_table14_133_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_129_552e( true) ;
         }
         else
         {
            wb_table7_129_552e( false) ;
         }
      }

      protected void wb_table14_133_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable9_Internalname, tblUnnamedtable9_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_fabricasoftwarepessoanom_Internalname, "Contratada", "", "", lblTextblockcontagem_fabricasoftwarepessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_FabricaSoftwarePessoaNom_Internalname, StringUtil.RTrim( A209Contagem_FabricaSoftwarePessoaNom), StringUtil.RTrim( context.localUtil.Format( A209Contagem_FabricaSoftwarePessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_FabricaSoftwarePessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_usuariocontadorpessoanom_Internalname, "Contador", "", "", lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_UsuarioContadorPessoaNom_Internalname, StringUtil.RTrim( A215Contagem_UsuarioContadorPessoaNom), StringUtil.RTrim( context.localUtil.Format( A215Contagem_UsuarioContadorPessoaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_UsuarioContadorPessoaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsvalidacao_Internalname, "Valida��o", "", "", lblTextblockcontagemitem_fsvalidacao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table15_148_552( true) ;
         }
         else
         {
            wb_table15_148_552( false) ;
         }
         return  ;
      }

      protected void wb_table15_148_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsreferenciatecnicanom_Internalname, "Refer�ncia", "", "", lblTextblockcontagemitem_fsreferenciatecnicanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSReferenciaTecnicaNom_Internalname, StringUtil.RTrim( A269ContagemItem_FSReferenciaTecnicaNom), StringUtil.RTrim( context.localUtil.Format( A269ContagemItem_FSReferenciaTecnicaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtContagemItem_FSReferenciaTecnicaNom_Link, "", "", "", edtContagemItem_FSReferenciaTecnicaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsreferenciatecnicaval_Internalname, "Deflator/Inflator", "", "", lblTextblockcontagemitem_fsreferenciatecnicaval_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSReferenciaTecnicaVal_Internalname, StringUtil.LTrim( StringUtil.NToC( A227ContagemItem_FSReferenciaTecnicaVal, 18, 5, ",", "")), context.localUtil.Format( A227ContagemItem_FSReferenciaTecnicaVal, "ZZZ,ZZZ,ZZZ,ZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSReferenciaTecnicaVal_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "Valor", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsder_Internalname, "DER", "", "", lblTextblockcontagemitem_fsder_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table16_170_552( true) ;
         }
         else
         {
            wb_table16_170_552( false) ;
         }
         return  ;
      }

      protected void wb_table16_170_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsqtdinm_Internalname, "Qtd. INM", "", "", lblTextblockcontagemitem_fsqtdinm_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table17_186_552( true) ;
         }
         else
         {
            wb_table17_186_552( false) ;
         }
         return  ;
      }

      protected void wb_table17_186_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fspontofuncaobruto_Internalname, "PF Bruto", "", "", lblTextblockcontagemitem_fspontofuncaobruto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table18_198_552( true) ;
         }
         else
         {
            wb_table18_198_552( false) ;
         }
         return  ;
      }

      protected void wb_table18_198_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsevidencias_Internalname, "Evid�ncias", "", "", lblTextblockcontagemitem_fsevidencias_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemItem_FSEvidencias_Internalname, A238ContagemItem_FSEvidencias, "", "", 0, 1, 0, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "5000", -1, "", "", -1, true, "Evidencias", "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table14_133_552e( true) ;
         }
         else
         {
            wb_table14_133_552e( false) ;
         }
      }

      protected void wb_table18_198_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fspontofuncaobruto_Internalname, tblTablemergedcontagemitem_fspontofuncaobruto_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSPontoFuncaoBruto_Internalname, StringUtil.LTrim( StringUtil.NToC( A236ContagemItem_FSPontoFuncaoBruto, 7, 2, ",", "")), context.localUtil.Format( A236ContagemItem_FSPontoFuncaoBruto, "ZZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSPontoFuncaoBruto_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "TotalPF", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fspontofuncaoliquido_Internalname, "PF L�quido", "", "", lblTextblockcontagemitem_fspontofuncaoliquido_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSPontoFuncaoLiquido_Internalname, StringUtil.LTrim( StringUtil.NToC( A237ContagemItem_FSPontoFuncaoLiquido, 7, 2, ",", "")), context.localUtil.Format( A237ContagemItem_FSPontoFuncaoLiquido, "ZZZ9.99"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSPontoFuncaoLiquido_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 7, "chr", 1, "row", 7, 0, 0, 0, 1, -1, 0, true, "TotalPF", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table18_198_552e( true) ;
         }
         else
         {
            wb_table18_198_552e( false) ;
         }
      }

      protected void wb_table17_186_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fsqtdinm_Internalname, tblTablemergedcontagemitem_fsqtdinm_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSQTDInm_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A234ContagemItem_FSQTDInm), 9, 0, ",", "")), context.localUtil.Format( (decimal)(A234ContagemItem_FSQTDInm), "ZZZZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSQTDInm_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 9, "chr", 1, "row", 9, 0, 0, 0, 1, -1, 0, true, "QuantidadeGrande", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fscp_Internalname, "Complexidade", "", "", lblTextblockcontagemitem_fscp_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSCP_Internalname, StringUtil.RTrim( A235ContagemItem_FSCP), StringUtil.RTrim( context.localUtil.Format( A235ContagemItem_FSCP, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSCP_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "CP", "left", true, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table17_186_552e( true) ;
         }
         else
         {
            wb_table17_186_552e( false) ;
         }
      }

      protected void wb_table16_170_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fsder_Internalname, tblTablemergedcontagemitem_fsder_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSDER_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A232ContagemItem_FSDER), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A232ContagemItem_FSDER), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSDER_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemitem_fsder_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemitem_fsder_righttext_Internalname, "(Atributos)", "", "", lblContagemitem_fsder_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fsar_Internalname, "AR", "", "", lblTextblockcontagemitem_fsar_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_FSAR_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A233ContagemItem_FSAR), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A233ContagemItem_FSAR), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_FSAR_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Quantidade", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellContagemitem_fsar_righttext_cell_Internalname+"\"  class=''>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemitem_fsar_righttext_Internalname, "(Tabelas)", "", "", lblContagemitem_fsar_righttext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table16_170_552e( true) ;
         }
         else
         {
            wb_table16_170_552e( false) ;
         }
      }

      protected void wb_table15_148_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagemitem_fsvalidacao_Internalname, tblTablemergedcontagemitem_fsvalidacao_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemItem_FSValidacao, cmbContagemItem_FSValidacao_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0)), 1, cmbContagemItem_FSValidacao_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemItemGeneral.htm");
            cmbContagemItem_FSValidacao.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A267ContagemItem_FSValidacao), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemItem_FSValidacao_Internalname, "Values", (String)(cmbContagemItem_FSValidacao.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_fstipounidade_Internalname, "Unidade", "", "", lblTextblockcontagemitem_fstipounidade_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemItem_FSTipoUnidade, cmbContagemItem_FSTipoUnidade_Internalname, StringUtil.RTrim( A228ContagemItem_FSTipoUnidade), 1, cmbContagemItem_FSTipoUnidade_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "svchar", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemItemGeneral.htm");
            cmbContagemItem_FSTipoUnidade.CurrentValue = StringUtil.RTrim( A228ContagemItem_FSTipoUnidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemItem_FSTipoUnidade_Internalname, "Values", (String)(cmbContagemItem_FSTipoUnidade.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table15_148_552e( true) ;
         }
         else
         {
            wb_table15_148_552e( false) ;
         }
      }

      protected void wb_table5_84_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable2_Internalname, tblUnnamedtable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_lancamento_Internalname, "Lan�amento", "", "", lblTextblockcontagemitem_lancamento_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_Lancamento_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A224ContagemItem_Lancamento), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_Lancamento_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_codigo_Internalname, "Contagem", "", "", lblTextblockcontagem_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            wb_table19_94_552( true) ;
         }
         else
         {
            wb_table19_94_552( false) ;
         }
         return  ;
      }

      protected void wb_table19_94_552e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_requisito_Internalname, "Requisito", "", "", lblTextblockcontagemitem_requisito_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_Requisito_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A229ContagemItem_Requisito), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A229ContagemItem_Requisito), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_Requisito_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Requisito", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfuncaoapf_nome_Internalname, "Fun��o APF", "", "", lblTextblockfuncaoapf_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtFuncaoAPF_Nome_Internalname, A166FuncaoAPF_Nome, edtFuncaoAPF_Nome_Link, "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_84_552e( true) ;
         }
         else
         {
            wb_table5_84_552e( false) ;
         }
      }

      protected void wb_table19_94_552( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontagem_codigo_Internalname, tblTablemergedcontagem_codigo_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagem_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A192Contagem_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A192Contagem_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagem_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_tecnica_Internalname, "T�cnica", "", "", lblTextblockcontagem_tecnica_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Tecnica, cmbContagem_Tecnica_Internalname, StringUtil.RTrim( A195Contagem_Tecnica), 1, cmbContagem_Tecnica_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemItemGeneral.htm");
            cmbContagem_Tecnica.CurrentValue = StringUtil.RTrim( A195Contagem_Tecnica);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagem_Tecnica_Internalname, "Values", (String)(cmbContagem_Tecnica.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagem_tipo_Internalname, "Tipo", "", "", lblTextblockcontagem_tipo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagem_Tipo, cmbContagem_Tipo_Internalname, StringUtil.RTrim( A196Contagem_Tipo), 1, cmbContagem_Tipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemItemGeneral.htm");
            cmbContagem_Tipo.CurrentValue = StringUtil.RTrim( A196Contagem_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagem_Tipo_Internalname, "Values", (String)(cmbContagem_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_sequencial_Internalname, "Sequencial", "", "", lblTextblockcontagemitem_sequencial_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemItem_Sequencial_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A225ContagemItem_Sequencial), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A225ContagemItem_Sequencial), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemItem_Sequencial_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Sequencial", "right", false, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemitem_validacaostatusfinal_Internalname, "Status FINAL do Item", "", "", lblTextblockcontagemitem_validacaostatusfinal_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContagemItemGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemItem_ValidacaoStatusFinal, cmbContagemItem_ValidacaoStatusFinal_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0)), 1, cmbContagemItem_ValidacaoStatusFinal_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 1, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_ContagemItemGeneral.htm");
            cmbContagemItem_ValidacaoStatusFinal.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A260ContagemItem_ValidacaoStatusFinal), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbContagemItem_ValidacaoStatusFinal_Internalname, "Values", (String)(cmbContagemItem_ValidacaoStatusFinal.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table19_94_552e( true) ;
         }
         else
         {
            wb_table19_94_552e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A224ContagemItem_Lancamento = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA552( ) ;
         WS552( ) ;
         WE552( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA224ContagemItem_Lancamento = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA552( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contagemitemgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA552( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A224ContagemItem_Lancamento = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         }
         wcpOA224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA224ContagemItem_Lancamento"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A224ContagemItem_Lancamento != wcpOA224ContagemItem_Lancamento ) ) )
         {
            setjustcreated();
         }
         wcpOA224ContagemItem_Lancamento = A224ContagemItem_Lancamento;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA224ContagemItem_Lancamento = cgiGet( sPrefix+"A224ContagemItem_Lancamento_CTRL");
         if ( StringUtil.Len( sCtrlA224ContagemItem_Lancamento) > 0 )
         {
            A224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( sCtrlA224ContagemItem_Lancamento), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A224ContagemItem_Lancamento", StringUtil.LTrim( StringUtil.Str( (decimal)(A224ContagemItem_Lancamento), 6, 0)));
         }
         else
         {
            A224ContagemItem_Lancamento = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A224ContagemItem_Lancamento_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA552( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS552( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS552( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A224ContagemItem_Lancamento_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A224ContagemItem_Lancamento), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA224ContagemItem_Lancamento)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A224ContagemItem_Lancamento_CTRL", StringUtil.RTrim( sCtrlA224ContagemItem_Lancamento));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE552( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117191297");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contagemitemgeneral.js", "?20203117191297");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontagem_datacriacao_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_DATACRIACAO";
         edtContagem_DataCriacao_Internalname = sPrefix+"CONTAGEM_DATACRIACAO";
         lblTextblockcontagem_areatrabalhocod_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_AREATRABALHOCOD";
         edtContagem_AreaTrabalhoCod_Internalname = sPrefix+"CONTAGEM_AREATRABALHOCOD";
         lblTextblockcontagem_areatrabalhodes_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_AREATRABALHODES";
         edtContagem_AreaTrabalhoDes_Internalname = sPrefix+"CONTAGEM_AREATRABALHODES";
         lblTextblockcontagemitem_pfb_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_PFB";
         edtContagemItem_PFB_Internalname = sPrefix+"CONTAGEMITEM_PFB";
         lblTextblockcontagemitem_pfl_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_PFL";
         edtContagemItem_PFL_Internalname = sPrefix+"CONTAGEMITEM_PFL";
         lblTextblockfuncaoapf_tipo_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_TIPO";
         cmbFuncaoAPF_Tipo_Internalname = sPrefix+"FUNCAOAPF_TIPO";
         lblTextblockcontagemitem_tipounidade_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_TIPOUNIDADE";
         cmbContagemItem_TipoUnidade_Internalname = sPrefix+"CONTAGEMITEM_TIPOUNIDADE";
         lblTextblockcontagemitem_evidencias_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_EVIDENCIAS";
         edtContagemItem_Evidencias_Internalname = sPrefix+"CONTAGEMITEM_EVIDENCIAS";
         lblTextblockcontagemitem_qtdinm_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_QTDINM";
         edtContagemItem_QtdINM_Internalname = sPrefix+"CONTAGEMITEM_QTDINM";
         lblTextblockcontagemitem_cp_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_CP";
         edtContagemItem_CP_Internalname = sPrefix+"CONTAGEMITEM_CP";
         lblTextblockcontagemitem_ra_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_RA";
         edtContagemItem_RA_Internalname = sPrefix+"CONTAGEMITEM_RA";
         lblTextblockcontagemitem_der_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_DER";
         edtContagemItem_DER_Internalname = sPrefix+"CONTAGEMITEM_DER";
         lblTextblockcontagemitem_funcao_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FUNCAO";
         edtContagemItem_Funcao_Internalname = sPrefix+"CONTAGEMITEM_FUNCAO";
         lblTextblockfuncaoapf_complexidade_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_COMPLEXIDADE";
         cmbFuncaoAPF_Complexidade_Internalname = sPrefix+"FUNCAOAPF_COMPLEXIDADE";
         lblTextblockcontagemitem_lancamento_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_LANCAMENTO";
         edtContagemItem_Lancamento_Internalname = sPrefix+"CONTAGEMITEM_LANCAMENTO";
         lblTextblockcontagem_codigo_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_CODIGO";
         edtContagem_Codigo_Internalname = sPrefix+"CONTAGEM_CODIGO";
         lblTextblockcontagem_tecnica_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_TECNICA";
         cmbContagem_Tecnica_Internalname = sPrefix+"CONTAGEM_TECNICA";
         lblTextblockcontagem_tipo_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_TIPO";
         cmbContagem_Tipo_Internalname = sPrefix+"CONTAGEM_TIPO";
         lblTextblockcontagemitem_sequencial_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_SEQUENCIAL";
         edtContagemItem_Sequencial_Internalname = sPrefix+"CONTAGEMITEM_SEQUENCIAL";
         lblTextblockcontagemitem_validacaostatusfinal_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_VALIDACAOSTATUSFINAL";
         cmbContagemItem_ValidacaoStatusFinal_Internalname = sPrefix+"CONTAGEMITEM_VALIDACAOSTATUSFINAL";
         tblTablemergedcontagem_codigo_Internalname = sPrefix+"TABLEMERGEDCONTAGEM_CODIGO";
         lblTextblockcontagemitem_requisito_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_REQUISITO";
         edtContagemItem_Requisito_Internalname = sPrefix+"CONTAGEMITEM_REQUISITO";
         lblTextblockfuncaoapf_nome_Internalname = sPrefix+"TEXTBLOCKFUNCAOAPF_NOME";
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME";
         tblUnnamedtable2_Internalname = sPrefix+"UNNAMEDTABLE2";
         lblTextblockcontagem_fabricasoftwarepessoanom_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_FABRICASOFTWAREPESSOANOM";
         edtContagem_FabricaSoftwarePessoaNom_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWAREPESSOANOM";
         lblTextblockcontagem_usuariocontadorpessoanom_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_USUARIOCONTADORPESSOANOM";
         edtContagem_UsuarioContadorPessoaNom_Internalname = sPrefix+"CONTAGEM_USUARIOCONTADORPESSOANOM";
         lblTextblockcontagemitem_fsvalidacao_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FSVALIDACAO";
         cmbContagemItem_FSValidacao_Internalname = sPrefix+"CONTAGEMITEM_FSVALIDACAO";
         lblTextblockcontagemitem_fstipounidade_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FSTIPOUNIDADE";
         cmbContagemItem_FSTipoUnidade_Internalname = sPrefix+"CONTAGEMITEM_FSTIPOUNIDADE";
         tblTablemergedcontagemitem_fsvalidacao_Internalname = sPrefix+"TABLEMERGEDCONTAGEMITEM_FSVALIDACAO";
         lblTextblockcontagemitem_fsreferenciatecnicanom_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FSREFERENCIATECNICANOM";
         edtContagemItem_FSReferenciaTecnicaNom_Internalname = sPrefix+"CONTAGEMITEM_FSREFERENCIATECNICANOM";
         lblTextblockcontagemitem_fsreferenciatecnicaval_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FSREFERENCIATECNICAVAL";
         edtContagemItem_FSReferenciaTecnicaVal_Internalname = sPrefix+"CONTAGEMITEM_FSREFERENCIATECNICAVAL";
         lblTextblockcontagemitem_fsder_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FSDER";
         edtContagemItem_FSDER_Internalname = sPrefix+"CONTAGEMITEM_FSDER";
         lblContagemitem_fsder_righttext_Internalname = sPrefix+"CONTAGEMITEM_FSDER_RIGHTTEXT";
         cellContagemitem_fsder_righttext_cell_Internalname = sPrefix+"CONTAGEMITEM_FSDER_RIGHTTEXT_CELL";
         lblTextblockcontagemitem_fsar_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FSAR";
         edtContagemItem_FSAR_Internalname = sPrefix+"CONTAGEMITEM_FSAR";
         lblContagemitem_fsar_righttext_Internalname = sPrefix+"CONTAGEMITEM_FSAR_RIGHTTEXT";
         cellContagemitem_fsar_righttext_cell_Internalname = sPrefix+"CONTAGEMITEM_FSAR_RIGHTTEXT_CELL";
         tblTablemergedcontagemitem_fsder_Internalname = sPrefix+"TABLEMERGEDCONTAGEMITEM_FSDER";
         lblTextblockcontagemitem_fsqtdinm_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FSQTDINM";
         edtContagemItem_FSQTDInm_Internalname = sPrefix+"CONTAGEMITEM_FSQTDINM";
         lblTextblockcontagemitem_fscp_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FSCP";
         edtContagemItem_FSCP_Internalname = sPrefix+"CONTAGEMITEM_FSCP";
         tblTablemergedcontagemitem_fsqtdinm_Internalname = sPrefix+"TABLEMERGEDCONTAGEMITEM_FSQTDINM";
         lblTextblockcontagemitem_fspontofuncaobruto_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FSPONTOFUNCAOBRUTO";
         edtContagemItem_FSPontoFuncaoBruto_Internalname = sPrefix+"CONTAGEMITEM_FSPONTOFUNCAOBRUTO";
         lblTextblockcontagemitem_fspontofuncaoliquido_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FSPONTOFUNCAOLIQUIDO";
         edtContagemItem_FSPontoFuncaoLiquido_Internalname = sPrefix+"CONTAGEMITEM_FSPONTOFUNCAOLIQUIDO";
         tblTablemergedcontagemitem_fspontofuncaobruto_Internalname = sPrefix+"TABLEMERGEDCONTAGEMITEM_FSPONTOFUNCAOBRUTO";
         lblTextblockcontagemitem_fsevidencias_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FSEVIDENCIAS";
         edtContagemItem_FSEvidencias_Internalname = sPrefix+"CONTAGEMITEM_FSEVIDENCIAS";
         tblUnnamedtable9_Internalname = sPrefix+"UNNAMEDTABLE9";
         grpUnnamedgroup10_Internalname = sPrefix+"UNNAMEDGROUP10";
         tblUnnamedtable4_Internalname = sPrefix+"UNNAMEDTABLE4";
         tblUnnamedtable5_Internalname = sPrefix+"UNNAMEDTABLE5";
         lblTextblockcontagem_usuarioauditorpessoanom_Internalname = sPrefix+"TEXTBLOCKCONTAGEM_USUARIOAUDITORPESSOANOM";
         edtContagem_UsuarioAuditorPessoaNom_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORPESSOANOM";
         lblTextblockcontagemitem_fmvalidacao_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FMVALIDACAO";
         cmbContagemItem_FMValidacao_Internalname = sPrefix+"CONTAGEMITEM_FMVALIDACAO";
         lblTextblockcontagemitem_fmreferenciatecnicanom_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FMREFERENCIATECNICANOM";
         edtContagemItem_FMReferenciaTecnicaNom_Internalname = sPrefix+"CONTAGEMITEM_FMREFERENCIATECNICANOM";
         lblTextblockcontagemitem_fmreferenciatecnicaval_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FMREFERENCIATECNICAVAL";
         edtContagemItem_FMReferenciaTecnicaVal_Internalname = sPrefix+"CONTAGEMITEM_FMREFERENCIATECNICAVAL";
         lblTextblockcontagemitem_fmder_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FMDER";
         edtContagemItem_FMDER_Internalname = sPrefix+"CONTAGEMITEM_FMDER";
         lblContagemitem_fmder_righttext_Internalname = sPrefix+"CONTAGEMITEM_FMDER_RIGHTTEXT";
         cellContagemitem_fmder_righttext_cell_Internalname = sPrefix+"CONTAGEMITEM_FMDER_RIGHTTEXT_CELL";
         lblTextblockcontagemitem_fmar_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FMAR";
         edtContagemItem_FMAR_Internalname = sPrefix+"CONTAGEMITEM_FMAR";
         lblContagemitem_fmar_righttext_Internalname = sPrefix+"CONTAGEMITEM_FMAR_RIGHTTEXT";
         cellContagemitem_fmar_righttext_cell_Internalname = sPrefix+"CONTAGEMITEM_FMAR_RIGHTTEXT_CELL";
         tblTablemergedcontagemitem_fmder_Internalname = sPrefix+"TABLEMERGEDCONTAGEMITEM_FMDER";
         lblTextblockcontagemitem_fmqtdinm_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FMQTDINM";
         edtContagemItem_FMQTDInm_Internalname = sPrefix+"CONTAGEMITEM_FMQTDINM";
         lblTextblockcontagemitem_fmcp_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FMCP";
         edtContagemItem_FMCP_Internalname = sPrefix+"CONTAGEMITEM_FMCP";
         tblTablemergedcontagemitem_fmqtdinm_Internalname = sPrefix+"TABLEMERGEDCONTAGEMITEM_FMQTDINM";
         lblTextblockcontagemitem_fmpontofuncaobruto_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FMPONTOFUNCAOBRUTO";
         edtContagemItem_FMPontoFuncaoBruto_Internalname = sPrefix+"CONTAGEMITEM_FMPONTOFUNCAOBRUTO";
         lblTextblockcontagemitem_fmpontofuncaoliquido_Internalname = sPrefix+"TEXTBLOCKCONTAGEMITEM_FMPONTOFUNCAOLIQUIDO";
         edtContagemItem_FMPontoFuncaoLiquido_Internalname = sPrefix+"CONTAGEMITEM_FMPONTOFUNCAOLIQUIDO";
         tblTablemergedcontagemitem_fmpontofuncaobruto_Internalname = sPrefix+"TABLEMERGEDCONTAGEMITEM_FMPONTOFUNCAOBRUTO";
         tblUnnamedtable7_Internalname = sPrefix+"UNNAMEDTABLE7";
         grpUnnamedgroup8_Internalname = sPrefix+"UNNAMEDGROUP8";
         tblUnnamedtable6_Internalname = sPrefix+"UNNAMEDTABLE6";
         tblUnnamedtable3_Internalname = sPrefix+"UNNAMEDTABLE3";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtContagemItem_FMReferenciaTecnicaCod_Internalname = sPrefix+"CONTAGEMITEM_FMREFERENCIATECNICACOD";
         edtContagem_UsuarioAuditorCod_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORCOD";
         edtContagem_UsuarioAuditorPessoaCod_Internalname = sPrefix+"CONTAGEM_USUARIOAUDITORPESSOACOD";
         edtContagemItem_FSReferenciaTecnicaCod_Internalname = sPrefix+"CONTAGEMITEM_FSREFERENCIATECNICACOD";
         edtContagem_FabricaSoftwareCod_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWARECOD";
         edtContagem_FabricaSoftwarePessoaCod_Internalname = sPrefix+"CONTAGEM_FABRICASOFTWAREPESSOACOD";
         edtContagem_UsuarioContadorCod_Internalname = sPrefix+"CONTAGEM_USUARIOCONTADORCOD";
         edtContagem_UsuarioContadorPessoaCod_Internalname = sPrefix+"CONTAGEM_USUARIOCONTADORPESSOACOD";
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbContagemItem_ValidacaoStatusFinal_Jsonclick = "";
         edtContagemItem_Sequencial_Jsonclick = "";
         cmbContagem_Tipo_Jsonclick = "";
         cmbContagem_Tecnica_Jsonclick = "";
         edtContagem_Codigo_Jsonclick = "";
         edtContagemItem_Requisito_Jsonclick = "";
         edtContagemItem_Lancamento_Jsonclick = "";
         cmbContagemItem_FSTipoUnidade_Jsonclick = "";
         cmbContagemItem_FSValidacao_Jsonclick = "";
         edtContagemItem_FSAR_Jsonclick = "";
         edtContagemItem_FSDER_Jsonclick = "";
         edtContagemItem_FSCP_Jsonclick = "";
         edtContagemItem_FSQTDInm_Jsonclick = "";
         edtContagemItem_FSPontoFuncaoLiquido_Jsonclick = "";
         edtContagemItem_FSPontoFuncaoBruto_Jsonclick = "";
         edtContagemItem_FSReferenciaTecnicaVal_Jsonclick = "";
         edtContagemItem_FSReferenciaTecnicaNom_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaNom_Jsonclick = "";
         edtContagem_FabricaSoftwarePessoaNom_Jsonclick = "";
         edtContagemItem_FMAR_Jsonclick = "";
         edtContagemItem_FMDER_Jsonclick = "";
         edtContagemItem_FMCP_Jsonclick = "";
         edtContagemItem_FMQTDInm_Jsonclick = "";
         edtContagemItem_FMPontoFuncaoLiquido_Jsonclick = "";
         edtContagemItem_FMPontoFuncaoBruto_Jsonclick = "";
         edtContagemItem_FMReferenciaTecnicaVal_Jsonclick = "";
         edtContagemItem_FMReferenciaTecnicaNom_Jsonclick = "";
         cmbContagemItem_FMValidacao_Jsonclick = "";
         edtContagem_UsuarioAuditorPessoaNom_Jsonclick = "";
         cmbFuncaoAPF_Complexidade_Jsonclick = "";
         edtContagemItem_Funcao_Jsonclick = "";
         edtContagemItem_DER_Jsonclick = "";
         edtContagemItem_RA_Jsonclick = "";
         edtContagemItem_CP_Jsonclick = "";
         edtContagemItem_QtdINM_Jsonclick = "";
         cmbContagemItem_TipoUnidade_Jsonclick = "";
         cmbFuncaoAPF_Tipo_Jsonclick = "";
         edtContagemItem_PFL_Jsonclick = "";
         edtContagemItem_PFB_Jsonclick = "";
         edtContagem_AreaTrabalhoDes_Jsonclick = "";
         edtContagem_AreaTrabalhoCod_Jsonclick = "";
         edtContagem_DataCriacao_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         edtFuncaoAPF_Nome_Link = "";
         edtContagemItem_FSReferenciaTecnicaNom_Link = "";
         edtContagemItem_FMReferenciaTecnicaNom_Link = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         edtFuncaoAPF_Codigo_Visible = 1;
         edtContagem_UsuarioContadorPessoaCod_Jsonclick = "";
         edtContagem_UsuarioContadorPessoaCod_Visible = 1;
         edtContagem_UsuarioContadorCod_Jsonclick = "";
         edtContagem_UsuarioContadorCod_Visible = 1;
         edtContagem_FabricaSoftwarePessoaCod_Jsonclick = "";
         edtContagem_FabricaSoftwarePessoaCod_Visible = 1;
         edtContagem_FabricaSoftwareCod_Jsonclick = "";
         edtContagem_FabricaSoftwareCod_Visible = 1;
         edtContagemItem_FSReferenciaTecnicaCod_Jsonclick = "";
         edtContagemItem_FSReferenciaTecnicaCod_Visible = 1;
         edtContagem_UsuarioAuditorPessoaCod_Jsonclick = "";
         edtContagem_UsuarioAuditorPessoaCod_Visible = 1;
         edtContagem_UsuarioAuditorCod_Jsonclick = "";
         edtContagem_UsuarioAuditorCod_Visible = 1;
         edtContagemItem_FMReferenciaTecnicaCod_Jsonclick = "";
         edtContagemItem_FMReferenciaTecnicaCod_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13552',iparms:[{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14552',iparms:[{av:'A224ContagemItem_Lancamento',fld:'CONTAGEMITEM_LANCAMENTO',pic:'ZZZZZ9',nv:0},{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A192Contagem_Codigo',fld:'CONTAGEM_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         scmdbuf = "";
         H00553_A232ContagemItem_FSDER = new short[1] ;
         H00553_n232ContagemItem_FSDER = new bool[] {false} ;
         H00555_A251ContagemItem_FMDER = new short[1] ;
         H00555_n251ContagemItem_FMDER = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A952ContagemItem_TipoUnidade = "";
         A953ContagemItem_Evidencias = "";
         A955ContagemItem_CP = "";
         A958ContagemItem_Funcao = "";
         A185FuncaoAPF_Complexidade = "";
         A209Contagem_FabricaSoftwarePessoaNom = "";
         A228ContagemItem_FSTipoUnidade = "";
         A235ContagemItem_FSCP = "";
         A238ContagemItem_FSEvidencias = "";
         A218Contagem_UsuarioAuditorPessoaNom = "";
         A240ContagemItem_FMCP = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A184FuncaoAPF_Tipo = "";
         A195Contagem_Tecnica = "";
         A196Contagem_Tipo = "";
         H00558_A214Contagem_UsuarioContadorPessoaCod = new int[1] ;
         H00558_n214Contagem_UsuarioContadorPessoaCod = new bool[] {false} ;
         H00558_A213Contagem_UsuarioContadorCod = new int[1] ;
         H00558_n213Contagem_UsuarioContadorCod = new bool[] {false} ;
         H00558_A208Contagem_FabricaSoftwarePessoaCod = new int[1] ;
         H00558_A207Contagem_FabricaSoftwareCod = new int[1] ;
         H00558_A226ContagemItem_FSReferenciaTecnicaCod = new int[1] ;
         H00558_n226ContagemItem_FSReferenciaTecnicaCod = new bool[] {false} ;
         H00558_A217Contagem_UsuarioAuditorPessoaCod = new int[1] ;
         H00558_A216Contagem_UsuarioAuditorCod = new int[1] ;
         H00558_A255ContagemItem_FMReferenciaTecnicaCod = new int[1] ;
         H00558_n255ContagemItem_FMReferenciaTecnicaCod = new bool[] {false} ;
         H00558_A242ContagemItem_FMPontoFuncaoLiquido = new decimal[1] ;
         H00558_n242ContagemItem_FMPontoFuncaoLiquido = new bool[] {false} ;
         H00558_A241ContagemItem_FMPontoFuncaoBruto = new decimal[1] ;
         H00558_n241ContagemItem_FMPontoFuncaoBruto = new bool[] {false} ;
         H00558_A240ContagemItem_FMCP = new String[] {""} ;
         H00558_n240ContagemItem_FMCP = new bool[] {false} ;
         H00558_A239ContagemItem_FMQTDInm = new int[1] ;
         H00558_n239ContagemItem_FMQTDInm = new bool[] {false} ;
         H00558_A256ContagemItem_FMReferenciaTecnicaVal = new decimal[1] ;
         H00558_n256ContagemItem_FMReferenciaTecnicaVal = new bool[] {false} ;
         H00558_A257ContagemItem_FMReferenciaTecnicaNom = new String[] {""} ;
         H00558_n257ContagemItem_FMReferenciaTecnicaNom = new bool[] {false} ;
         H00558_A218Contagem_UsuarioAuditorPessoaNom = new String[] {""} ;
         H00558_A238ContagemItem_FSEvidencias = new String[] {""} ;
         H00558_n238ContagemItem_FSEvidencias = new bool[] {false} ;
         H00558_A237ContagemItem_FSPontoFuncaoLiquido = new decimal[1] ;
         H00558_n237ContagemItem_FSPontoFuncaoLiquido = new bool[] {false} ;
         H00558_A236ContagemItem_FSPontoFuncaoBruto = new decimal[1] ;
         H00558_n236ContagemItem_FSPontoFuncaoBruto = new bool[] {false} ;
         H00558_A235ContagemItem_FSCP = new String[] {""} ;
         H00558_n235ContagemItem_FSCP = new bool[] {false} ;
         H00558_A234ContagemItem_FSQTDInm = new int[1] ;
         H00558_n234ContagemItem_FSQTDInm = new bool[] {false} ;
         H00558_A227ContagemItem_FSReferenciaTecnicaVal = new decimal[1] ;
         H00558_n227ContagemItem_FSReferenciaTecnicaVal = new bool[] {false} ;
         H00558_A269ContagemItem_FSReferenciaTecnicaNom = new String[] {""} ;
         H00558_n269ContagemItem_FSReferenciaTecnicaNom = new bool[] {false} ;
         H00558_A228ContagemItem_FSTipoUnidade = new String[] {""} ;
         H00558_n228ContagemItem_FSTipoUnidade = new bool[] {false} ;
         H00558_A215Contagem_UsuarioContadorPessoaNom = new String[] {""} ;
         H00558_n215Contagem_UsuarioContadorPessoaNom = new bool[] {false} ;
         H00558_A209Contagem_FabricaSoftwarePessoaNom = new String[] {""} ;
         H00558_A166FuncaoAPF_Nome = new String[] {""} ;
         H00558_A229ContagemItem_Requisito = new short[1] ;
         H00558_n229ContagemItem_Requisito = new bool[] {false} ;
         H00558_A225ContagemItem_Sequencial = new short[1] ;
         H00558_n225ContagemItem_Sequencial = new bool[] {false} ;
         H00558_A196Contagem_Tipo = new String[] {""} ;
         H00558_n196Contagem_Tipo = new bool[] {false} ;
         H00558_A195Contagem_Tecnica = new String[] {""} ;
         H00558_n195Contagem_Tecnica = new bool[] {false} ;
         H00558_A192Contagem_Codigo = new int[1] ;
         H00558_A958ContagemItem_Funcao = new String[] {""} ;
         H00558_n958ContagemItem_Funcao = new bool[] {false} ;
         H00558_A957ContagemItem_DER = new short[1] ;
         H00558_n957ContagemItem_DER = new bool[] {false} ;
         H00558_A956ContagemItem_RA = new short[1] ;
         H00558_n956ContagemItem_RA = new bool[] {false} ;
         H00558_A955ContagemItem_CP = new String[] {""} ;
         H00558_n955ContagemItem_CP = new bool[] {false} ;
         H00558_A954ContagemItem_QtdINM = new short[1] ;
         H00558_n954ContagemItem_QtdINM = new bool[] {false} ;
         H00558_A953ContagemItem_Evidencias = new String[] {""} ;
         H00558_n953ContagemItem_Evidencias = new bool[] {false} ;
         H00558_A952ContagemItem_TipoUnidade = new String[] {""} ;
         H00558_A184FuncaoAPF_Tipo = new String[] {""} ;
         H00558_A951ContagemItem_PFL = new decimal[1] ;
         H00558_n951ContagemItem_PFL = new bool[] {false} ;
         H00558_A950ContagemItem_PFB = new decimal[1] ;
         H00558_n950ContagemItem_PFB = new bool[] {false} ;
         H00558_A194Contagem_AreaTrabalhoDes = new String[] {""} ;
         H00558_n194Contagem_AreaTrabalhoDes = new bool[] {false} ;
         H00558_A193Contagem_AreaTrabalhoCod = new int[1] ;
         H00558_A197Contagem_DataCriacao = new DateTime[] {DateTime.MinValue} ;
         H00558_A224ContagemItem_Lancamento = new int[1] ;
         H00558_A251ContagemItem_FMDER = new short[1] ;
         H00558_n251ContagemItem_FMDER = new bool[] {false} ;
         H00558_A232ContagemItem_FSDER = new short[1] ;
         H00558_n232ContagemItem_FSDER = new bool[] {false} ;
         H00558_A165FuncaoAPF_Codigo = new int[1] ;
         H00558_n165FuncaoAPF_Codigo = new bool[] {false} ;
         H00558_A268ContagemItem_FMValidacao = new short[1] ;
         H00558_n268ContagemItem_FMValidacao = new bool[] {false} ;
         H00558_A267ContagemItem_FSValidacao = new short[1] ;
         H00558_n267ContagemItem_FSValidacao = new bool[] {false} ;
         A257ContagemItem_FMReferenciaTecnicaNom = "";
         A269ContagemItem_FSReferenciaTecnicaNom = "";
         A215Contagem_UsuarioContadorPessoaNom = "";
         A166FuncaoAPF_Nome = "";
         A194Contagem_AreaTrabalhoDes = "";
         A197Contagem_DataCriacao = DateTime.MinValue;
         GXt_char2 = "";
         H005510_A232ContagemItem_FSDER = new short[1] ;
         H005510_n232ContagemItem_FSDER = new bool[] {false} ;
         H005512_A251ContagemItem_FMDER = new short[1] ;
         H005512_n251ContagemItem_FMDER = new bool[] {false} ;
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockcontagem_datacriacao_Jsonclick = "";
         lblTextblockcontagem_areatrabalhocod_Jsonclick = "";
         lblTextblockcontagem_areatrabalhodes_Jsonclick = "";
         lblTextblockcontagemitem_pfb_Jsonclick = "";
         lblTextblockcontagemitem_pfl_Jsonclick = "";
         lblTextblockfuncaoapf_tipo_Jsonclick = "";
         lblTextblockcontagemitem_tipounidade_Jsonclick = "";
         lblTextblockcontagemitem_evidencias_Jsonclick = "";
         lblTextblockcontagemitem_qtdinm_Jsonclick = "";
         lblTextblockcontagemitem_cp_Jsonclick = "";
         lblTextblockcontagemitem_ra_Jsonclick = "";
         lblTextblockcontagemitem_der_Jsonclick = "";
         lblTextblockcontagemitem_funcao_Jsonclick = "";
         lblTextblockfuncaoapf_complexidade_Jsonclick = "";
         lblTextblockcontagem_usuarioauditorpessoanom_Jsonclick = "";
         lblTextblockcontagemitem_fmvalidacao_Jsonclick = "";
         lblTextblockcontagemitem_fmreferenciatecnicanom_Jsonclick = "";
         lblTextblockcontagemitem_fmreferenciatecnicaval_Jsonclick = "";
         lblTextblockcontagemitem_fmder_Jsonclick = "";
         lblTextblockcontagemitem_fmqtdinm_Jsonclick = "";
         lblTextblockcontagemitem_fmpontofuncaobruto_Jsonclick = "";
         lblTextblockcontagemitem_fmpontofuncaoliquido_Jsonclick = "";
         lblTextblockcontagemitem_fmcp_Jsonclick = "";
         lblContagemitem_fmder_righttext_Jsonclick = "";
         lblTextblockcontagemitem_fmar_Jsonclick = "";
         lblContagemitem_fmar_righttext_Jsonclick = "";
         lblTextblockcontagem_fabricasoftwarepessoanom_Jsonclick = "";
         lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick = "";
         lblTextblockcontagemitem_fsvalidacao_Jsonclick = "";
         lblTextblockcontagemitem_fsreferenciatecnicanom_Jsonclick = "";
         lblTextblockcontagemitem_fsreferenciatecnicaval_Jsonclick = "";
         lblTextblockcontagemitem_fsder_Jsonclick = "";
         lblTextblockcontagemitem_fsqtdinm_Jsonclick = "";
         lblTextblockcontagemitem_fspontofuncaobruto_Jsonclick = "";
         lblTextblockcontagemitem_fsevidencias_Jsonclick = "";
         lblTextblockcontagemitem_fspontofuncaoliquido_Jsonclick = "";
         lblTextblockcontagemitem_fscp_Jsonclick = "";
         lblContagemitem_fsder_righttext_Jsonclick = "";
         lblTextblockcontagemitem_fsar_Jsonclick = "";
         lblContagemitem_fsar_righttext_Jsonclick = "";
         lblTextblockcontagemitem_fstipounidade_Jsonclick = "";
         lblTextblockcontagemitem_lancamento_Jsonclick = "";
         lblTextblockcontagem_codigo_Jsonclick = "";
         lblTextblockcontagemitem_requisito_Jsonclick = "";
         lblTextblockfuncaoapf_nome_Jsonclick = "";
         lblTextblockcontagem_tecnica_Jsonclick = "";
         lblTextblockcontagem_tipo_Jsonclick = "";
         lblTextblockcontagemitem_sequencial_Jsonclick = "";
         lblTextblockcontagemitem_validacaostatusfinal_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA224ContagemItem_Lancamento = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemitemgeneral__default(),
            new Object[][] {
                new Object[] {
               H00553_A232ContagemItem_FSDER, H00553_n232ContagemItem_FSDER
               }
               , new Object[] {
               H00555_A251ContagemItem_FMDER, H00555_n251ContagemItem_FMDER
               }
               , new Object[] {
               H00558_A214Contagem_UsuarioContadorPessoaCod, H00558_n214Contagem_UsuarioContadorPessoaCod, H00558_A213Contagem_UsuarioContadorCod, H00558_n213Contagem_UsuarioContadorCod, H00558_A208Contagem_FabricaSoftwarePessoaCod, H00558_A207Contagem_FabricaSoftwareCod, H00558_A226ContagemItem_FSReferenciaTecnicaCod, H00558_n226ContagemItem_FSReferenciaTecnicaCod, H00558_A217Contagem_UsuarioAuditorPessoaCod, H00558_A216Contagem_UsuarioAuditorCod,
               H00558_A255ContagemItem_FMReferenciaTecnicaCod, H00558_n255ContagemItem_FMReferenciaTecnicaCod, H00558_A242ContagemItem_FMPontoFuncaoLiquido, H00558_n242ContagemItem_FMPontoFuncaoLiquido, H00558_A241ContagemItem_FMPontoFuncaoBruto, H00558_n241ContagemItem_FMPontoFuncaoBruto, H00558_A240ContagemItem_FMCP, H00558_n240ContagemItem_FMCP, H00558_A239ContagemItem_FMQTDInm, H00558_n239ContagemItem_FMQTDInm,
               H00558_A256ContagemItem_FMReferenciaTecnicaVal, H00558_n256ContagemItem_FMReferenciaTecnicaVal, H00558_A257ContagemItem_FMReferenciaTecnicaNom, H00558_n257ContagemItem_FMReferenciaTecnicaNom, H00558_A218Contagem_UsuarioAuditorPessoaNom, H00558_A238ContagemItem_FSEvidencias, H00558_n238ContagemItem_FSEvidencias, H00558_A237ContagemItem_FSPontoFuncaoLiquido, H00558_n237ContagemItem_FSPontoFuncaoLiquido, H00558_A236ContagemItem_FSPontoFuncaoBruto,
               H00558_n236ContagemItem_FSPontoFuncaoBruto, H00558_A235ContagemItem_FSCP, H00558_n235ContagemItem_FSCP, H00558_A234ContagemItem_FSQTDInm, H00558_n234ContagemItem_FSQTDInm, H00558_A227ContagemItem_FSReferenciaTecnicaVal, H00558_n227ContagemItem_FSReferenciaTecnicaVal, H00558_A269ContagemItem_FSReferenciaTecnicaNom, H00558_n269ContagemItem_FSReferenciaTecnicaNom, H00558_A228ContagemItem_FSTipoUnidade,
               H00558_n228ContagemItem_FSTipoUnidade, H00558_A215Contagem_UsuarioContadorPessoaNom, H00558_n215Contagem_UsuarioContadorPessoaNom, H00558_A209Contagem_FabricaSoftwarePessoaNom, H00558_A166FuncaoAPF_Nome, H00558_A229ContagemItem_Requisito, H00558_n229ContagemItem_Requisito, H00558_A225ContagemItem_Sequencial, H00558_n225ContagemItem_Sequencial, H00558_A196Contagem_Tipo,
               H00558_n196Contagem_Tipo, H00558_A195Contagem_Tecnica, H00558_n195Contagem_Tecnica, H00558_A192Contagem_Codigo, H00558_A958ContagemItem_Funcao, H00558_n958ContagemItem_Funcao, H00558_A957ContagemItem_DER, H00558_n957ContagemItem_DER, H00558_A956ContagemItem_RA, H00558_n956ContagemItem_RA,
               H00558_A955ContagemItem_CP, H00558_n955ContagemItem_CP, H00558_A954ContagemItem_QtdINM, H00558_n954ContagemItem_QtdINM, H00558_A953ContagemItem_Evidencias, H00558_n953ContagemItem_Evidencias, H00558_A952ContagemItem_TipoUnidade, H00558_A184FuncaoAPF_Tipo, H00558_A951ContagemItem_PFL, H00558_n951ContagemItem_PFL,
               H00558_A950ContagemItem_PFB, H00558_n950ContagemItem_PFB, H00558_A194Contagem_AreaTrabalhoDes, H00558_n194Contagem_AreaTrabalhoDes, H00558_A193Contagem_AreaTrabalhoCod, H00558_A197Contagem_DataCriacao, H00558_A224ContagemItem_Lancamento, H00558_A251ContagemItem_FMDER, H00558_n251ContagemItem_FMDER, H00558_A232ContagemItem_FSDER,
               H00558_n232ContagemItem_FSDER, H00558_A165FuncaoAPF_Codigo, H00558_n165FuncaoAPF_Codigo, H00558_A268ContagemItem_FMValidacao, H00558_n268ContagemItem_FMValidacao, H00558_A267ContagemItem_FSValidacao, H00558_n267ContagemItem_FSValidacao
               }
               , new Object[] {
               H005510_A232ContagemItem_FSDER, H005510_n232ContagemItem_FSDER
               }
               , new Object[] {
               H005512_A251ContagemItem_FMDER, H005512_n251ContagemItem_FMDER
               }
            }
         );
         AV15Pgmname = "ContagemItemGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "ContagemItemGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A232ContagemItem_FSDER ;
      private short A251ContagemItem_FMDER ;
      private short A233ContagemItem_FSAR ;
      private short A252ContagemItem_FMAR ;
      private short A954ContagemItem_QtdINM ;
      private short A956ContagemItem_RA ;
      private short A957ContagemItem_DER ;
      private short A225ContagemItem_Sequencial ;
      private short A260ContagemItem_ValidacaoStatusFinal ;
      private short A229ContagemItem_Requisito ;
      private short A267ContagemItem_FSValidacao ;
      private short A268ContagemItem_FMValidacao ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A224ContagemItem_Lancamento ;
      private int wcpOA224ContagemItem_Lancamento ;
      private int A234ContagemItem_FSQTDInm ;
      private int A239ContagemItem_FMQTDInm ;
      private int A255ContagemItem_FMReferenciaTecnicaCod ;
      private int A216Contagem_UsuarioAuditorCod ;
      private int A217Contagem_UsuarioAuditorPessoaCod ;
      private int A226ContagemItem_FSReferenciaTecnicaCod ;
      private int A207Contagem_FabricaSoftwareCod ;
      private int A208Contagem_FabricaSoftwarePessoaCod ;
      private int A165FuncaoAPF_Codigo ;
      private int edtContagemItem_FMReferenciaTecnicaCod_Visible ;
      private int edtContagem_UsuarioAuditorCod_Visible ;
      private int edtContagem_UsuarioAuditorPessoaCod_Visible ;
      private int edtContagemItem_FSReferenciaTecnicaCod_Visible ;
      private int edtContagem_FabricaSoftwareCod_Visible ;
      private int edtContagem_FabricaSoftwarePessoaCod_Visible ;
      private int A213Contagem_UsuarioContadorCod ;
      private int edtContagem_UsuarioContadorCod_Visible ;
      private int A214Contagem_UsuarioContadorPessoaCod ;
      private int edtContagem_UsuarioContadorPessoaCod_Visible ;
      private int edtFuncaoAPF_Codigo_Visible ;
      private int A192Contagem_Codigo ;
      private int A193Contagem_AreaTrabalhoCod ;
      private int GXt_int1 ;
      private int AV12FuncaoAPF_SistemaCod ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7ContagemItem_Lancamento ;
      private int idxLst ;
      private decimal A950ContagemItem_PFB ;
      private decimal A951ContagemItem_PFL ;
      private decimal A236ContagemItem_FSPontoFuncaoBruto ;
      private decimal A237ContagemItem_FSPontoFuncaoLiquido ;
      private decimal A241ContagemItem_FMPontoFuncaoBruto ;
      private decimal A242ContagemItem_FMPontoFuncaoLiquido ;
      private decimal A256ContagemItem_FMReferenciaTecnicaVal ;
      private decimal A227ContagemItem_FSReferenciaTecnicaVal ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String scmdbuf ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A955ContagemItem_CP ;
      private String A185FuncaoAPF_Complexidade ;
      private String A209Contagem_FabricaSoftwarePessoaNom ;
      private String A235ContagemItem_FSCP ;
      private String A218Contagem_UsuarioAuditorPessoaNom ;
      private String A240ContagemItem_FMCP ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtContagemItem_FMReferenciaTecnicaCod_Internalname ;
      private String edtContagemItem_FMReferenciaTecnicaCod_Jsonclick ;
      private String edtContagem_UsuarioAuditorCod_Internalname ;
      private String edtContagem_UsuarioAuditorCod_Jsonclick ;
      private String edtContagem_UsuarioAuditorPessoaCod_Internalname ;
      private String edtContagem_UsuarioAuditorPessoaCod_Jsonclick ;
      private String edtContagemItem_FSReferenciaTecnicaCod_Internalname ;
      private String edtContagemItem_FSReferenciaTecnicaCod_Jsonclick ;
      private String edtContagem_FabricaSoftwareCod_Internalname ;
      private String edtContagem_FabricaSoftwareCod_Jsonclick ;
      private String edtContagem_FabricaSoftwarePessoaCod_Internalname ;
      private String edtContagem_FabricaSoftwarePessoaCod_Jsonclick ;
      private String edtContagem_UsuarioContadorCod_Internalname ;
      private String edtContagem_UsuarioContadorCod_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaCod_Internalname ;
      private String edtContagem_UsuarioContadorPessoaCod_Jsonclick ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String A184FuncaoAPF_Tipo ;
      private String A195Contagem_Tecnica ;
      private String A196Contagem_Tipo ;
      private String A257ContagemItem_FMReferenciaTecnicaNom ;
      private String A269ContagemItem_FSReferenciaTecnicaNom ;
      private String A215Contagem_UsuarioContadorPessoaNom ;
      private String GXt_char2 ;
      private String edtContagem_DataCriacao_Internalname ;
      private String edtContagem_AreaTrabalhoCod_Internalname ;
      private String edtContagem_AreaTrabalhoDes_Internalname ;
      private String edtContagemItem_PFB_Internalname ;
      private String edtContagemItem_PFL_Internalname ;
      private String cmbFuncaoAPF_Tipo_Internalname ;
      private String cmbContagemItem_TipoUnidade_Internalname ;
      private String edtContagemItem_Evidencias_Internalname ;
      private String edtContagemItem_QtdINM_Internalname ;
      private String edtContagemItem_CP_Internalname ;
      private String edtContagemItem_RA_Internalname ;
      private String edtContagemItem_DER_Internalname ;
      private String edtContagemItem_Funcao_Internalname ;
      private String cmbFuncaoAPF_Complexidade_Internalname ;
      private String edtContagem_Codigo_Internalname ;
      private String cmbContagem_Tecnica_Internalname ;
      private String cmbContagem_Tipo_Internalname ;
      private String edtContagemItem_Sequencial_Internalname ;
      private String cmbContagemItem_ValidacaoStatusFinal_Internalname ;
      private String edtContagemItem_Requisito_Internalname ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String edtContagem_FabricaSoftwarePessoaNom_Internalname ;
      private String edtContagem_UsuarioContadorPessoaNom_Internalname ;
      private String cmbContagemItem_FSValidacao_Internalname ;
      private String cmbContagemItem_FSTipoUnidade_Internalname ;
      private String edtContagemItem_FSReferenciaTecnicaNom_Internalname ;
      private String edtContagemItem_FSReferenciaTecnicaVal_Internalname ;
      private String edtContagemItem_FSDER_Internalname ;
      private String edtContagemItem_FSAR_Internalname ;
      private String edtContagemItem_FSQTDInm_Internalname ;
      private String edtContagemItem_FSCP_Internalname ;
      private String edtContagemItem_FSPontoFuncaoBruto_Internalname ;
      private String edtContagemItem_FSPontoFuncaoLiquido_Internalname ;
      private String edtContagemItem_FSEvidencias_Internalname ;
      private String edtContagem_UsuarioAuditorPessoaNom_Internalname ;
      private String cmbContagemItem_FMValidacao_Internalname ;
      private String edtContagemItem_FMReferenciaTecnicaNom_Internalname ;
      private String edtContagemItem_FMReferenciaTecnicaVal_Internalname ;
      private String edtContagemItem_FMDER_Internalname ;
      private String edtContagemItem_FMAR_Internalname ;
      private String edtContagemItem_FMQTDInm_Internalname ;
      private String edtContagemItem_FMCP_Internalname ;
      private String edtContagemItem_FMPontoFuncaoBruto_Internalname ;
      private String edtContagemItem_FMPontoFuncaoLiquido_Internalname ;
      private String hsh ;
      private String edtContagemItem_FMReferenciaTecnicaNom_Link ;
      private String edtContagemItem_FSReferenciaTecnicaNom_Link ;
      private String edtFuncaoAPF_Nome_Link ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagem_datacriacao_Internalname ;
      private String lblTextblockcontagem_datacriacao_Jsonclick ;
      private String edtContagem_DataCriacao_Jsonclick ;
      private String lblTextblockcontagem_areatrabalhocod_Internalname ;
      private String lblTextblockcontagem_areatrabalhocod_Jsonclick ;
      private String edtContagem_AreaTrabalhoCod_Jsonclick ;
      private String lblTextblockcontagem_areatrabalhodes_Internalname ;
      private String lblTextblockcontagem_areatrabalhodes_Jsonclick ;
      private String edtContagem_AreaTrabalhoDes_Jsonclick ;
      private String lblTextblockcontagemitem_pfb_Internalname ;
      private String lblTextblockcontagemitem_pfb_Jsonclick ;
      private String edtContagemItem_PFB_Jsonclick ;
      private String lblTextblockcontagemitem_pfl_Internalname ;
      private String lblTextblockcontagemitem_pfl_Jsonclick ;
      private String edtContagemItem_PFL_Jsonclick ;
      private String lblTextblockfuncaoapf_tipo_Internalname ;
      private String lblTextblockfuncaoapf_tipo_Jsonclick ;
      private String cmbFuncaoAPF_Tipo_Jsonclick ;
      private String lblTextblockcontagemitem_tipounidade_Internalname ;
      private String lblTextblockcontagemitem_tipounidade_Jsonclick ;
      private String cmbContagemItem_TipoUnidade_Jsonclick ;
      private String lblTextblockcontagemitem_evidencias_Internalname ;
      private String lblTextblockcontagemitem_evidencias_Jsonclick ;
      private String lblTextblockcontagemitem_qtdinm_Internalname ;
      private String lblTextblockcontagemitem_qtdinm_Jsonclick ;
      private String edtContagemItem_QtdINM_Jsonclick ;
      private String lblTextblockcontagemitem_cp_Internalname ;
      private String lblTextblockcontagemitem_cp_Jsonclick ;
      private String edtContagemItem_CP_Jsonclick ;
      private String lblTextblockcontagemitem_ra_Internalname ;
      private String lblTextblockcontagemitem_ra_Jsonclick ;
      private String edtContagemItem_RA_Jsonclick ;
      private String lblTextblockcontagemitem_der_Internalname ;
      private String lblTextblockcontagemitem_der_Jsonclick ;
      private String edtContagemItem_DER_Jsonclick ;
      private String lblTextblockcontagemitem_funcao_Internalname ;
      private String lblTextblockcontagemitem_funcao_Jsonclick ;
      private String edtContagemItem_Funcao_Jsonclick ;
      private String lblTextblockfuncaoapf_complexidade_Internalname ;
      private String lblTextblockfuncaoapf_complexidade_Jsonclick ;
      private String cmbFuncaoAPF_Complexidade_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String tblUnnamedtable3_Internalname ;
      private String tblUnnamedtable6_Internalname ;
      private String grpUnnamedgroup8_Internalname ;
      private String tblUnnamedtable7_Internalname ;
      private String lblTextblockcontagem_usuarioauditorpessoanom_Internalname ;
      private String lblTextblockcontagem_usuarioauditorpessoanom_Jsonclick ;
      private String edtContagem_UsuarioAuditorPessoaNom_Jsonclick ;
      private String lblTextblockcontagemitem_fmvalidacao_Internalname ;
      private String lblTextblockcontagemitem_fmvalidacao_Jsonclick ;
      private String cmbContagemItem_FMValidacao_Jsonclick ;
      private String lblTextblockcontagemitem_fmreferenciatecnicanom_Internalname ;
      private String lblTextblockcontagemitem_fmreferenciatecnicanom_Jsonclick ;
      private String edtContagemItem_FMReferenciaTecnicaNom_Jsonclick ;
      private String lblTextblockcontagemitem_fmreferenciatecnicaval_Internalname ;
      private String lblTextblockcontagemitem_fmreferenciatecnicaval_Jsonclick ;
      private String edtContagemItem_FMReferenciaTecnicaVal_Jsonclick ;
      private String lblTextblockcontagemitem_fmder_Internalname ;
      private String lblTextblockcontagemitem_fmder_Jsonclick ;
      private String lblTextblockcontagemitem_fmqtdinm_Internalname ;
      private String lblTextblockcontagemitem_fmqtdinm_Jsonclick ;
      private String lblTextblockcontagemitem_fmpontofuncaobruto_Internalname ;
      private String lblTextblockcontagemitem_fmpontofuncaobruto_Jsonclick ;
      private String tblTablemergedcontagemitem_fmpontofuncaobruto_Internalname ;
      private String edtContagemItem_FMPontoFuncaoBruto_Jsonclick ;
      private String lblTextblockcontagemitem_fmpontofuncaoliquido_Internalname ;
      private String lblTextblockcontagemitem_fmpontofuncaoliquido_Jsonclick ;
      private String edtContagemItem_FMPontoFuncaoLiquido_Jsonclick ;
      private String tblTablemergedcontagemitem_fmqtdinm_Internalname ;
      private String edtContagemItem_FMQTDInm_Jsonclick ;
      private String lblTextblockcontagemitem_fmcp_Internalname ;
      private String lblTextblockcontagemitem_fmcp_Jsonclick ;
      private String edtContagemItem_FMCP_Jsonclick ;
      private String tblTablemergedcontagemitem_fmder_Internalname ;
      private String edtContagemItem_FMDER_Jsonclick ;
      private String cellContagemitem_fmder_righttext_cell_Internalname ;
      private String lblContagemitem_fmder_righttext_Internalname ;
      private String lblContagemitem_fmder_righttext_Jsonclick ;
      private String lblTextblockcontagemitem_fmar_Internalname ;
      private String lblTextblockcontagemitem_fmar_Jsonclick ;
      private String edtContagemItem_FMAR_Jsonclick ;
      private String cellContagemitem_fmar_righttext_cell_Internalname ;
      private String lblContagemitem_fmar_righttext_Internalname ;
      private String lblContagemitem_fmar_righttext_Jsonclick ;
      private String tblUnnamedtable5_Internalname ;
      private String tblUnnamedtable4_Internalname ;
      private String grpUnnamedgroup10_Internalname ;
      private String tblUnnamedtable9_Internalname ;
      private String lblTextblockcontagem_fabricasoftwarepessoanom_Internalname ;
      private String lblTextblockcontagem_fabricasoftwarepessoanom_Jsonclick ;
      private String edtContagem_FabricaSoftwarePessoaNom_Jsonclick ;
      private String lblTextblockcontagem_usuariocontadorpessoanom_Internalname ;
      private String lblTextblockcontagem_usuariocontadorpessoanom_Jsonclick ;
      private String edtContagem_UsuarioContadorPessoaNom_Jsonclick ;
      private String lblTextblockcontagemitem_fsvalidacao_Internalname ;
      private String lblTextblockcontagemitem_fsvalidacao_Jsonclick ;
      private String lblTextblockcontagemitem_fsreferenciatecnicanom_Internalname ;
      private String lblTextblockcontagemitem_fsreferenciatecnicanom_Jsonclick ;
      private String edtContagemItem_FSReferenciaTecnicaNom_Jsonclick ;
      private String lblTextblockcontagemitem_fsreferenciatecnicaval_Internalname ;
      private String lblTextblockcontagemitem_fsreferenciatecnicaval_Jsonclick ;
      private String edtContagemItem_FSReferenciaTecnicaVal_Jsonclick ;
      private String lblTextblockcontagemitem_fsder_Internalname ;
      private String lblTextblockcontagemitem_fsder_Jsonclick ;
      private String lblTextblockcontagemitem_fsqtdinm_Internalname ;
      private String lblTextblockcontagemitem_fsqtdinm_Jsonclick ;
      private String lblTextblockcontagemitem_fspontofuncaobruto_Internalname ;
      private String lblTextblockcontagemitem_fspontofuncaobruto_Jsonclick ;
      private String lblTextblockcontagemitem_fsevidencias_Internalname ;
      private String lblTextblockcontagemitem_fsevidencias_Jsonclick ;
      private String tblTablemergedcontagemitem_fspontofuncaobruto_Internalname ;
      private String edtContagemItem_FSPontoFuncaoBruto_Jsonclick ;
      private String lblTextblockcontagemitem_fspontofuncaoliquido_Internalname ;
      private String lblTextblockcontagemitem_fspontofuncaoliquido_Jsonclick ;
      private String edtContagemItem_FSPontoFuncaoLiquido_Jsonclick ;
      private String tblTablemergedcontagemitem_fsqtdinm_Internalname ;
      private String edtContagemItem_FSQTDInm_Jsonclick ;
      private String lblTextblockcontagemitem_fscp_Internalname ;
      private String lblTextblockcontagemitem_fscp_Jsonclick ;
      private String edtContagemItem_FSCP_Jsonclick ;
      private String tblTablemergedcontagemitem_fsder_Internalname ;
      private String edtContagemItem_FSDER_Jsonclick ;
      private String cellContagemitem_fsder_righttext_cell_Internalname ;
      private String lblContagemitem_fsder_righttext_Internalname ;
      private String lblContagemitem_fsder_righttext_Jsonclick ;
      private String lblTextblockcontagemitem_fsar_Internalname ;
      private String lblTextblockcontagemitem_fsar_Jsonclick ;
      private String edtContagemItem_FSAR_Jsonclick ;
      private String cellContagemitem_fsar_righttext_cell_Internalname ;
      private String lblContagemitem_fsar_righttext_Internalname ;
      private String lblContagemitem_fsar_righttext_Jsonclick ;
      private String tblTablemergedcontagemitem_fsvalidacao_Internalname ;
      private String cmbContagemItem_FSValidacao_Jsonclick ;
      private String lblTextblockcontagemitem_fstipounidade_Internalname ;
      private String lblTextblockcontagemitem_fstipounidade_Jsonclick ;
      private String cmbContagemItem_FSTipoUnidade_Jsonclick ;
      private String tblUnnamedtable2_Internalname ;
      private String lblTextblockcontagemitem_lancamento_Internalname ;
      private String lblTextblockcontagemitem_lancamento_Jsonclick ;
      private String edtContagemItem_Lancamento_Internalname ;
      private String edtContagemItem_Lancamento_Jsonclick ;
      private String lblTextblockcontagem_codigo_Internalname ;
      private String lblTextblockcontagem_codigo_Jsonclick ;
      private String lblTextblockcontagemitem_requisito_Internalname ;
      private String lblTextblockcontagemitem_requisito_Jsonclick ;
      private String edtContagemItem_Requisito_Jsonclick ;
      private String lblTextblockfuncaoapf_nome_Internalname ;
      private String lblTextblockfuncaoapf_nome_Jsonclick ;
      private String tblTablemergedcontagem_codigo_Internalname ;
      private String edtContagem_Codigo_Jsonclick ;
      private String lblTextblockcontagem_tecnica_Internalname ;
      private String lblTextblockcontagem_tecnica_Jsonclick ;
      private String cmbContagem_Tecnica_Jsonclick ;
      private String lblTextblockcontagem_tipo_Internalname ;
      private String lblTextblockcontagem_tipo_Jsonclick ;
      private String cmbContagem_Tipo_Jsonclick ;
      private String lblTextblockcontagemitem_sequencial_Internalname ;
      private String lblTextblockcontagemitem_sequencial_Jsonclick ;
      private String edtContagemItem_Sequencial_Jsonclick ;
      private String lblTextblockcontagemitem_validacaostatusfinal_Internalname ;
      private String lblTextblockcontagemitem_validacaostatusfinal_Jsonclick ;
      private String cmbContagemItem_ValidacaoStatusFinal_Jsonclick ;
      private String sCtrlA224ContagemItem_Lancamento ;
      private DateTime A197Contagem_DataCriacao ;
      private bool entryPointCalled ;
      private bool n232ContagemItem_FSDER ;
      private bool n251ContagemItem_FMDER ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n195Contagem_Tecnica ;
      private bool n196Contagem_Tipo ;
      private bool n267ContagemItem_FSValidacao ;
      private bool n228ContagemItem_FSTipoUnidade ;
      private bool n268ContagemItem_FMValidacao ;
      private bool n214Contagem_UsuarioContadorPessoaCod ;
      private bool n213Contagem_UsuarioContadorCod ;
      private bool n226ContagemItem_FSReferenciaTecnicaCod ;
      private bool n255ContagemItem_FMReferenciaTecnicaCod ;
      private bool n242ContagemItem_FMPontoFuncaoLiquido ;
      private bool n241ContagemItem_FMPontoFuncaoBruto ;
      private bool n240ContagemItem_FMCP ;
      private bool n239ContagemItem_FMQTDInm ;
      private bool n256ContagemItem_FMReferenciaTecnicaVal ;
      private bool n257ContagemItem_FMReferenciaTecnicaNom ;
      private bool n238ContagemItem_FSEvidencias ;
      private bool n237ContagemItem_FSPontoFuncaoLiquido ;
      private bool n236ContagemItem_FSPontoFuncaoBruto ;
      private bool n235ContagemItem_FSCP ;
      private bool n234ContagemItem_FSQTDInm ;
      private bool n227ContagemItem_FSReferenciaTecnicaVal ;
      private bool n269ContagemItem_FSReferenciaTecnicaNom ;
      private bool n215Contagem_UsuarioContadorPessoaNom ;
      private bool n229ContagemItem_Requisito ;
      private bool n225ContagemItem_Sequencial ;
      private bool n958ContagemItem_Funcao ;
      private bool n957ContagemItem_DER ;
      private bool n956ContagemItem_RA ;
      private bool n955ContagemItem_CP ;
      private bool n954ContagemItem_QtdINM ;
      private bool n953ContagemItem_Evidencias ;
      private bool n951ContagemItem_PFL ;
      private bool n950ContagemItem_PFB ;
      private bool n194Contagem_AreaTrabalhoDes ;
      private bool n165FuncaoAPF_Codigo ;
      private bool returnInSub ;
      private String A953ContagemItem_Evidencias ;
      private String A238ContagemItem_FSEvidencias ;
      private String A952ContagemItem_TipoUnidade ;
      private String A958ContagemItem_Funcao ;
      private String A228ContagemItem_FSTipoUnidade ;
      private String A166FuncaoAPF_Nome ;
      private String A194Contagem_AreaTrabalhoDes ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbFuncaoAPF_Tipo ;
      private GXCombobox cmbContagemItem_TipoUnidade ;
      private GXCombobox cmbFuncaoAPF_Complexidade ;
      private GXCombobox cmbContagem_Tecnica ;
      private GXCombobox cmbContagem_Tipo ;
      private GXCombobox cmbContagemItem_ValidacaoStatusFinal ;
      private GXCombobox cmbContagemItem_FSValidacao ;
      private GXCombobox cmbContagemItem_FSTipoUnidade ;
      private GXCombobox cmbContagemItem_FMValidacao ;
      private IDataStoreProvider pr_default ;
      private short[] H00553_A232ContagemItem_FSDER ;
      private bool[] H00553_n232ContagemItem_FSDER ;
      private short[] H00555_A251ContagemItem_FMDER ;
      private bool[] H00555_n251ContagemItem_FMDER ;
      private int[] H00558_A214Contagem_UsuarioContadorPessoaCod ;
      private bool[] H00558_n214Contagem_UsuarioContadorPessoaCod ;
      private int[] H00558_A213Contagem_UsuarioContadorCod ;
      private bool[] H00558_n213Contagem_UsuarioContadorCod ;
      private int[] H00558_A208Contagem_FabricaSoftwarePessoaCod ;
      private int[] H00558_A207Contagem_FabricaSoftwareCod ;
      private int[] H00558_A226ContagemItem_FSReferenciaTecnicaCod ;
      private bool[] H00558_n226ContagemItem_FSReferenciaTecnicaCod ;
      private int[] H00558_A217Contagem_UsuarioAuditorPessoaCod ;
      private int[] H00558_A216Contagem_UsuarioAuditorCod ;
      private int[] H00558_A255ContagemItem_FMReferenciaTecnicaCod ;
      private bool[] H00558_n255ContagemItem_FMReferenciaTecnicaCod ;
      private decimal[] H00558_A242ContagemItem_FMPontoFuncaoLiquido ;
      private bool[] H00558_n242ContagemItem_FMPontoFuncaoLiquido ;
      private decimal[] H00558_A241ContagemItem_FMPontoFuncaoBruto ;
      private bool[] H00558_n241ContagemItem_FMPontoFuncaoBruto ;
      private String[] H00558_A240ContagemItem_FMCP ;
      private bool[] H00558_n240ContagemItem_FMCP ;
      private int[] H00558_A239ContagemItem_FMQTDInm ;
      private bool[] H00558_n239ContagemItem_FMQTDInm ;
      private decimal[] H00558_A256ContagemItem_FMReferenciaTecnicaVal ;
      private bool[] H00558_n256ContagemItem_FMReferenciaTecnicaVal ;
      private String[] H00558_A257ContagemItem_FMReferenciaTecnicaNom ;
      private bool[] H00558_n257ContagemItem_FMReferenciaTecnicaNom ;
      private String[] H00558_A218Contagem_UsuarioAuditorPessoaNom ;
      private String[] H00558_A238ContagemItem_FSEvidencias ;
      private bool[] H00558_n238ContagemItem_FSEvidencias ;
      private decimal[] H00558_A237ContagemItem_FSPontoFuncaoLiquido ;
      private bool[] H00558_n237ContagemItem_FSPontoFuncaoLiquido ;
      private decimal[] H00558_A236ContagemItem_FSPontoFuncaoBruto ;
      private bool[] H00558_n236ContagemItem_FSPontoFuncaoBruto ;
      private String[] H00558_A235ContagemItem_FSCP ;
      private bool[] H00558_n235ContagemItem_FSCP ;
      private int[] H00558_A234ContagemItem_FSQTDInm ;
      private bool[] H00558_n234ContagemItem_FSQTDInm ;
      private decimal[] H00558_A227ContagemItem_FSReferenciaTecnicaVal ;
      private bool[] H00558_n227ContagemItem_FSReferenciaTecnicaVal ;
      private String[] H00558_A269ContagemItem_FSReferenciaTecnicaNom ;
      private bool[] H00558_n269ContagemItem_FSReferenciaTecnicaNom ;
      private String[] H00558_A228ContagemItem_FSTipoUnidade ;
      private bool[] H00558_n228ContagemItem_FSTipoUnidade ;
      private String[] H00558_A215Contagem_UsuarioContadorPessoaNom ;
      private bool[] H00558_n215Contagem_UsuarioContadorPessoaNom ;
      private String[] H00558_A209Contagem_FabricaSoftwarePessoaNom ;
      private String[] H00558_A166FuncaoAPF_Nome ;
      private short[] H00558_A229ContagemItem_Requisito ;
      private bool[] H00558_n229ContagemItem_Requisito ;
      private short[] H00558_A225ContagemItem_Sequencial ;
      private bool[] H00558_n225ContagemItem_Sequencial ;
      private String[] H00558_A196Contagem_Tipo ;
      private bool[] H00558_n196Contagem_Tipo ;
      private String[] H00558_A195Contagem_Tecnica ;
      private bool[] H00558_n195Contagem_Tecnica ;
      private int[] H00558_A192Contagem_Codigo ;
      private String[] H00558_A958ContagemItem_Funcao ;
      private bool[] H00558_n958ContagemItem_Funcao ;
      private short[] H00558_A957ContagemItem_DER ;
      private bool[] H00558_n957ContagemItem_DER ;
      private short[] H00558_A956ContagemItem_RA ;
      private bool[] H00558_n956ContagemItem_RA ;
      private String[] H00558_A955ContagemItem_CP ;
      private bool[] H00558_n955ContagemItem_CP ;
      private short[] H00558_A954ContagemItem_QtdINM ;
      private bool[] H00558_n954ContagemItem_QtdINM ;
      private String[] H00558_A953ContagemItem_Evidencias ;
      private bool[] H00558_n953ContagemItem_Evidencias ;
      private String[] H00558_A952ContagemItem_TipoUnidade ;
      private String[] H00558_A184FuncaoAPF_Tipo ;
      private decimal[] H00558_A951ContagemItem_PFL ;
      private bool[] H00558_n951ContagemItem_PFL ;
      private decimal[] H00558_A950ContagemItem_PFB ;
      private bool[] H00558_n950ContagemItem_PFB ;
      private String[] H00558_A194Contagem_AreaTrabalhoDes ;
      private bool[] H00558_n194Contagem_AreaTrabalhoDes ;
      private int[] H00558_A193Contagem_AreaTrabalhoCod ;
      private DateTime[] H00558_A197Contagem_DataCriacao ;
      private int[] H00558_A224ContagemItem_Lancamento ;
      private short[] H00558_A251ContagemItem_FMDER ;
      private bool[] H00558_n251ContagemItem_FMDER ;
      private short[] H00558_A232ContagemItem_FSDER ;
      private bool[] H00558_n232ContagemItem_FSDER ;
      private int[] H00558_A165FuncaoAPF_Codigo ;
      private bool[] H00558_n165FuncaoAPF_Codigo ;
      private short[] H00558_A268ContagemItem_FMValidacao ;
      private bool[] H00558_n268ContagemItem_FMValidacao ;
      private short[] H00558_A267ContagemItem_FSValidacao ;
      private bool[] H00558_n267ContagemItem_FSValidacao ;
      private short[] H005510_A232ContagemItem_FSDER ;
      private bool[] H005510_n232ContagemItem_FSDER ;
      private short[] H005512_A251ContagemItem_FMDER ;
      private bool[] H005512_n251ContagemItem_FMDER ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class contagemitemgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00553 ;
          prmH00553 = new Object[] {
          } ;
          Object[] prmH00555 ;
          prmH00555 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00558 ;
          prmH00558 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00558 ;
          cmdBufferH00558=" SELECT T5.[Usuario_PessoaCod] AS Contagem_UsuarioContadorPessoaCod, T4.[Contagem_UsuarioContadorCod] AS Contagem_UsuarioContadorCod, T1.[Contagem_FabricaSoftwarePessoaCod], T1.[Contagem_FabricaSoftwareCod], T1.[ContagemItem_FSReferenciaTecnicaCod] AS ContagemItem_FSReferenciaTecnicaCod, T1.[Contagem_UsuarioAuditorPessoaCod], T1.[Contagem_UsuarioAuditorCod], T1.[ContagemItem_FMReferenciaTecnicaCod] AS ContagemItem_FMReferenciaTecni, T1.[ContagemItem_FMPontoFuncaoLiquido], T1.[ContagemItem_FMPontoFuncaoBruto], T1.[ContagemItem_FMCP], T1.[ContagemItem_FMQTDInm], T3.[ReferenciaTecnica_Valor] AS ContagemItem_FMReferenciaTecni, T3.[ReferenciaTecnica_Nome] AS ContagemItem_FMReferenciaTecni, T1.[Contagem_UsuarioAuditorPessoaNom], T1.[ContagemItem_FSEvidencias], T1.[ContagemItem_FSPontoFuncaoLiquido], T1.[ContagemItem_FSPontoFuncaoBruto], T1.[ContagemItem_FSCP], T1.[ContagemItem_FSQTDInm], T2.[ReferenciaTecnica_Valor] AS ContagemItem_FSReferenciaTecnicaVal, T2.[ReferenciaTecnica_Nome] AS ContagemItem_FSReferenciaTecnicaNom, T1.[ContagemItem_FSTipoUnidade], T6.[Pessoa_Nome] AS Contagem_UsuarioContadorPessoaNom, T1.[Contagem_FabricaSoftwarePessoaNom], T8.[FuncaoAPF_Nome], T1.[ContagemItem_Requisito], T1.[ContagemItem_Sequencial], T4.[Contagem_Tipo], T4.[Contagem_Tecnica], T1.[Contagem_Codigo], T1.[ContagemItem_Funcao], T1.[ContagemItem_DER], T1.[ContagemItem_RA], T1.[ContagemItem_CP], T1.[ContagemItem_QtdINM], T1.[ContagemItem_Evidencias], T1.[ContagemItem_TipoUnidade], T8.[FuncaoAPF_Tipo], T1.[ContagemItem_PFL], T1.[ContagemItem_PFB], T7.[AreaTrabalho_Descricao] AS Contagem_AreaTrabalhoDes, T4.[Contagem_AreaTrabalhoCod] AS Contagem_AreaTrabalhoCod, T4.[Contagem_DataCriacao], T1.[ContagemItem_Lancamento], COALESCE( T9.[ContagemItem_FMDER], 0) AS ContagemItem_FMDER, COALESCE( T10.[ContagemItem_FSDER], "
          + " 0) AS ContagemItem_FSDER, T1.[FuncaoAPF_Codigo], T1.[ContagemItem_FMValidacao], T1.[ContagemItem_FSValidacao] FROM (((((((([ContagemItem] T1 WITH (NOLOCK) LEFT JOIN [ReferenciaTecnica] T2 WITH (NOLOCK) ON T2.[ReferenciaTecnica_Codigo] = T1.[ContagemItem_FSReferenciaTecnicaCod]) LEFT JOIN [ReferenciaTecnica] T3 WITH (NOLOCK) ON T3.[ReferenciaTecnica_Codigo] = T1.[ContagemItem_FMReferenciaTecnicaCod]) INNER JOIN [Contagem] T4 WITH (NOLOCK) ON T4.[Contagem_Codigo] = T1.[Contagem_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T4.[Contagem_UsuarioContadorCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) INNER JOIN [AreaTrabalho] T7 WITH (NOLOCK) ON T7.[AreaTrabalho_Codigo] = T4.[Contagem_AreaTrabalhoCod]) LEFT JOIN [FuncoesAPF] T8 WITH (NOLOCK) ON T8.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_Codigo]) LEFT JOIN (SELECT COUNT(*) AS ContagemItem_FMDER, [ContagemItemAtributosFMetrica_ItemContagemLan] FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) GROUP BY [ContagemItemAtributosFMetrica_ItemContagemLan] ) T9 ON T9.[ContagemItemAtributosFMetrica_ItemContagemLan] = T1.[ContagemItem_Lancamento]),  (SELECT COUNT(*) AS ContagemItem_FSDER FROM [Atributos] WITH (NOLOCK) ) T10 WHERE T1.[ContagemItem_Lancamento] = @ContagemItem_Lancamento ORDER BY T1.[ContagemItem_Lancamento]" ;
          Object[] prmH005510 ;
          prmH005510 = new Object[] {
          } ;
          Object[] prmH005512 ;
          prmH005512 = new Object[] {
          new Object[] {"@ContagemItem_Lancamento",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00553", "SELECT COALESCE( T1.[ContagemItem_FSDER], 0) AS ContagemItem_FSDER FROM (SELECT COUNT(*) AS ContagemItem_FSDER FROM [Atributos] WITH (NOLOCK) ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00553,1,0,true,true )
             ,new CursorDef("H00555", "SELECT COALESCE( T1.[ContagemItem_FMDER], 0) AS ContagemItem_FMDER FROM (SELECT COUNT(*) AS ContagemItem_FMDER, [ContagemItemAtributosFMetrica_ItemContagemLan] FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) GROUP BY [ContagemItemAtributosFMetrica_ItemContagemLan] ) T1 WHERE T1.[ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItem_Lancamento ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00555,1,0,true,true )
             ,new CursorDef("H00558", cmdBufferH00558,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00558,1,0,true,true )
             ,new CursorDef("H005510", "SELECT COALESCE( T1.[ContagemItem_FSDER], 0) AS ContagemItem_FSDER FROM (SELECT COUNT(*) AS ContagemItem_FSDER FROM [Atributos] WITH (NOLOCK) ) T1 ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005510,1,0,true,true )
             ,new CursorDef("H005512", "SELECT COALESCE( T1.[ContagemItem_FMDER], 0) AS ContagemItem_FMDER FROM (SELECT COUNT(*) AS ContagemItem_FMDER, [ContagemItemAtributosFMetrica_ItemContagemLan] FROM [ContagemItemAtributosFMetrica] WITH (NOLOCK) GROUP BY [ContagemItemAtributosFMetrica_ItemContagemLan] ) T1 WHERE T1.[ContagemItemAtributosFMetrica_ItemContagemLan] = @ContagemItem_Lancamento ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH005512,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((decimal[]) buf[14])[0] = rslt.getDecimal(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                ((String[]) buf[16])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(11);
                ((int[]) buf[18])[0] = rslt.getInt(12) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(12);
                ((decimal[]) buf[20])[0] = rslt.getDecimal(13) ;
                ((bool[]) buf[21])[0] = rslt.wasNull(13);
                ((String[]) buf[22])[0] = rslt.getString(14, 50) ;
                ((bool[]) buf[23])[0] = rslt.wasNull(14);
                ((String[]) buf[24])[0] = rslt.getString(15, 50) ;
                ((String[]) buf[25])[0] = rslt.getLongVarchar(16) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(16);
                ((decimal[]) buf[27])[0] = rslt.getDecimal(17) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(17);
                ((decimal[]) buf[29])[0] = rslt.getDecimal(18) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(18);
                ((String[]) buf[31])[0] = rslt.getString(19, 1) ;
                ((bool[]) buf[32])[0] = rslt.wasNull(19);
                ((int[]) buf[33])[0] = rslt.getInt(20) ;
                ((bool[]) buf[34])[0] = rslt.wasNull(20);
                ((decimal[]) buf[35])[0] = rslt.getDecimal(21) ;
                ((bool[]) buf[36])[0] = rslt.wasNull(21);
                ((String[]) buf[37])[0] = rslt.getString(22, 50) ;
                ((bool[]) buf[38])[0] = rslt.wasNull(22);
                ((String[]) buf[39])[0] = rslt.getVarchar(23) ;
                ((bool[]) buf[40])[0] = rslt.wasNull(23);
                ((String[]) buf[41])[0] = rslt.getString(24, 100) ;
                ((bool[]) buf[42])[0] = rslt.wasNull(24);
                ((String[]) buf[43])[0] = rslt.getString(25, 50) ;
                ((String[]) buf[44])[0] = rslt.getVarchar(26) ;
                ((short[]) buf[45])[0] = rslt.getShort(27) ;
                ((bool[]) buf[46])[0] = rslt.wasNull(27);
                ((short[]) buf[47])[0] = rslt.getShort(28) ;
                ((bool[]) buf[48])[0] = rslt.wasNull(28);
                ((String[]) buf[49])[0] = rslt.getString(29, 1) ;
                ((bool[]) buf[50])[0] = rslt.wasNull(29);
                ((String[]) buf[51])[0] = rslt.getString(30, 1) ;
                ((bool[]) buf[52])[0] = rslt.wasNull(30);
                ((int[]) buf[53])[0] = rslt.getInt(31) ;
                ((String[]) buf[54])[0] = rslt.getVarchar(32) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(32);
                ((short[]) buf[56])[0] = rslt.getShort(33) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(33);
                ((short[]) buf[58])[0] = rslt.getShort(34) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(34);
                ((String[]) buf[60])[0] = rslt.getString(35, 1) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(35);
                ((short[]) buf[62])[0] = rslt.getShort(36) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(36);
                ((String[]) buf[64])[0] = rslt.getLongVarchar(37) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(37);
                ((String[]) buf[66])[0] = rslt.getVarchar(38) ;
                ((String[]) buf[67])[0] = rslt.getString(39, 3) ;
                ((decimal[]) buf[68])[0] = rslt.getDecimal(40) ;
                ((bool[]) buf[69])[0] = rslt.wasNull(40);
                ((decimal[]) buf[70])[0] = rslt.getDecimal(41) ;
                ((bool[]) buf[71])[0] = rslt.wasNull(41);
                ((String[]) buf[72])[0] = rslt.getVarchar(42) ;
                ((bool[]) buf[73])[0] = rslt.wasNull(42);
                ((int[]) buf[74])[0] = rslt.getInt(43) ;
                ((DateTime[]) buf[75])[0] = rslt.getGXDate(44) ;
                ((int[]) buf[76])[0] = rslt.getInt(45) ;
                ((short[]) buf[77])[0] = rslt.getShort(46) ;
                ((bool[]) buf[78])[0] = rslt.wasNull(46);
                ((short[]) buf[79])[0] = rslt.getShort(47) ;
                ((bool[]) buf[80])[0] = rslt.wasNull(47);
                ((int[]) buf[81])[0] = rslt.getInt(48) ;
                ((bool[]) buf[82])[0] = rslt.wasNull(48);
                ((short[]) buf[83])[0] = rslt.getShort(49) ;
                ((bool[]) buf[84])[0] = rslt.wasNull(49);
                ((short[]) buf[85])[0] = rslt.getShort(50) ;
                ((bool[]) buf[86])[0] = rslt.wasNull(50);
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
