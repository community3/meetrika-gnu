/*
               File: WP_ExecucaoContratual
        Description: Execucao Contratual
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:5:15.71
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_execucaocontratual : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_execucaocontratual( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_execucaocontratual( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynavContratada_codigo = new GXCombobox();
         cmbavContrato_codigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vCONTRATADA_CODIGO") == 0 )
            {
               ajax_req_read_hidden_sdt(GetNextPar( ), AV12WWPContext);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvCONTRATADA_CODIGONG2( AV12WWPContext) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PANG2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTNG2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020311951574");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_execucaocontratual.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOS", AV13Contratos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOS", AV13Contratos);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPERIODOS", AV8Periodos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPERIODOS", AV8Periodos);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPOS", AV14Tipos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPOS", AV14Tipos);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATO_ATIVO", A92Contrato_Ativo);
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATAVIGENCIAINICIO", context.localUtil.DToC( A82Contrato_DataVigenciaInicio, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATAVIGENCIATERMINO", context.localUtil.DToC( A83Contrato_DataVigenciaTermino, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMERO", StringUtil.RTrim( A77Contrato_Numero));
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOTERMOADITIVO_DATAINICIO", context.localUtil.DToC( A316ContratoTermoAditivo_DataInicio, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATOTERMOADITIVO_DATAFIM", context.localUtil.DToC( A317ContratoTermoAditivo_DataFim, 0, "/"));
         GxWebStd.gx_hidden_field( context, "CONTRATOTERMOADITIVO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A315ContratoTermoAditivo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vVIGENCIA", context.localUtil.DToC( AV10Vigencia, 0, "/"));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPERIODO", AV7Periodo);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPERIODO", AV7Periodo);
         }
         GxWebStd.gx_hidden_field( context, "vVIGENCIA_TO", context.localUtil.DToC( AV11Vigencia_To, 0, "/"));
         GxWebStd.gx_hidden_field( context, "vQ", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9q), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV12WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV12WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Width", StringUtil.RTrim( Innewwindow_Width));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Height", StringUtil.RTrim( Innewwindow_Height));
         GxWebStd.gx_hidden_field( context, "INNEWWINDOW_Target", StringUtil.RTrim( Innewwindow_Target));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WENG2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTNG2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_execucaocontratual.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WP_ExecucaoContratual" ;
      }

      public override String GetPgmdesc( )
      {
         return "Execucao Contratual" ;
      }

      protected void WBNG0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            context.WriteHtmlText( "<p>") ;
            wb_table1_3_NG2( true) ;
         }
         else
         {
            wb_table1_3_NG2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_NG2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "<p>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"INNEWWINDOWContainer"+"\"></div>") ;
            context.WriteHtmlText( "</p>") ;
            context.WriteHtmlText( "<p></p>") ;
         }
         wbLoad = true;
      }

      protected void STARTNG2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Execucao Contratual", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPNG0( ) ;
      }

      protected void WSNG2( )
      {
         STARTNG2( ) ;
         EVTNG2( ) ;
      }

      protected void EVTNG2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11NG2 */
                              E11NG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12NG2 */
                                    E12NG2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATADA_CODIGO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13NG2 */
                              E13NG2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14NG2 */
                              E14NG2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WENG2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PANG2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            dynavContratada_codigo.Name = "vCONTRATADA_CODIGO";
            dynavContratada_codigo.WebTags = "";
            cmbavContrato_codigo.Name = "vCONTRATO_CODIGO";
            cmbavContrato_codigo.WebTags = "";
            cmbavContrato_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)", 0);
            if ( cmbavContrato_codigo.ItemCount > 0 )
            {
               AV6Contrato_Codigo = (int)(NumberUtil.Val( cmbavContrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Contrato_Codigo), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Contrato_Codigo), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = dynavContratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvCONTRATADA_CODIGONG2( wwpbaseobjects.SdtWWPContext AV12WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvCONTRATADA_CODIGO_dataNG2( AV12WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvCONTRATADA_CODIGO_htmlNG2( wwpbaseobjects.SdtWWPContext AV12WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLVvCONTRATADA_CODIGO_dataNG2( AV12WWPContext) ;
         gxdynajaxindex = 1;
         dynavContratada_codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavContratada_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV5Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contratada_Codigo), 6, 0)));
         }
      }

      protected void GXDLVvCONTRATADA_CODIGO_dataNG2( wwpbaseobjects.SdtWWPContext AV12WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor H00NG2 */
         pr_default.execute(0, new Object[] {AV12WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00NG2_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00NG2_A41Contratada_PessoaNom[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynavContratada_codigo.ItemCount > 0 )
         {
            AV5Contratada_Codigo = (int)(NumberUtil.Val( dynavContratada_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contratada_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contratada_Codigo), 6, 0)));
         }
         if ( cmbavContrato_codigo.ItemCount > 0 )
         {
            AV6Contrato_Codigo = (int)(NumberUtil.Val( cmbavContrato_codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV6Contrato_Codigo), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Contrato_Codigo), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFNG2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFNG2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E14NG2 */
            E14NG2 ();
            WBNG0( ) ;
         }
      }

      protected void STRUPNG0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11NG2 */
         E11NG2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         GXVvCONTRATADA_CODIGO_htmlNG2( AV12WWPContext) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            dynavContratada_codigo.CurrentValue = cgiGet( dynavContratada_codigo_Internalname);
            AV5Contratada_Codigo = (int)(NumberUtil.Val( cgiGet( dynavContratada_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV5Contratada_Codigo), 6, 0)));
            cmbavContrato_codigo.CurrentValue = cgiGet( cmbavContrato_codigo_Internalname);
            AV6Contrato_Codigo = (int)(NumberUtil.Val( cgiGet( cmbavContrato_codigo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Contrato_Codigo), 6, 0)));
            /* Read saved values. */
            Innewwindow_Width = cgiGet( "INNEWWINDOW_Width");
            Innewwindow_Height = cgiGet( "INNEWWINDOW_Height");
            Innewwindow_Target = cgiGet( "INNEWWINDOW_Target");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            GXVvCONTRATADA_CODIGO_htmlNG2( AV12WWPContext) ;
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11NG2 */
         E11NG2 ();
         if (returnInSub) return;
      }

      protected void E11NG2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV12WWPContext) ;
      }

      public void GXEnter( )
      {
         /* Execute user event: E12NG2 */
         E12NG2 ();
         if (returnInSub) return;
      }

      protected void E12NG2( )
      {
         /* Enter Routine */
         if ( (0==AV5Contratada_Codigo) )
         {
            GX_msglist.addItem("Escolha uma Contratada antes de gerar o relat�rio!");
         }
         else if ( (0==AV6Contrato_Codigo) )
         {
            GX_msglist.addItem("Escolha um Contrato antes de gerar o relat�rio!");
         }
         else
         {
            Innewwindow_Target = formatLink("arel_execucaocontratual.aspx") + "?" + UrlEncode("" +AV12WWPContext.gxTpr_Areatrabalho_codigo) + "," + UrlEncode("" +AV5Contratada_Codigo) + "," + UrlEncode("" +AV13Contratos.GetNumeric(AV6Contrato_Codigo)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(((SdtSDT_Periodos)AV8Periodos.Item(AV6Contrato_Codigo)).gxTpr_Inicio)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(((SdtSDT_Periodos)AV8Periodos.Item(AV6Contrato_Codigo)).gxTpr_Fim)) + "," + UrlEncode(StringUtil.RTrim(AV14Tipos.GetString(AV6Contrato_Codigo)));
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Target", Innewwindow_Target);
            Innewwindow_Height = "600";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Height", Innewwindow_Height);
            Innewwindow_Width = "800";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Innewwindow_Internalname, "Width", Innewwindow_Width);
            this.executeUsercontrolMethod("", false, "INNEWWINDOWContainer", "OpenWindow", "", new Object[] {});
         }
      }

      protected void E13NG2( )
      {
         /* Contratada_codigo_Click Routine */
         /* Execute user subroutine: 'VIGENCIAS' */
         S112 ();
         if (returnInSub) return;
         cmbavContrato_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6Contrato_Codigo), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_codigo_Internalname, "Values", cmbavContrato_codigo.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV13Contratos", AV13Contratos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14Tipos", AV14Tipos);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7Periodo", AV7Periodo);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8Periodos", AV8Periodos);
      }

      protected void S112( )
      {
         /* 'VIGENCIAS' Routine */
         cmbavContrato_codigo.removeAllItems();
         cmbavContrato_codigo.addItem("0", "Nenhum", 0);
         AV9q = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9q", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9q), 4, 0)));
         /* Using cursor H00NG3 */
         pr_default.execute(1, new Object[] {AV5Contratada_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A74Contrato_Codigo = H00NG3_A74Contrato_Codigo[0];
            A77Contrato_Numero = H00NG3_A77Contrato_Numero[0];
            A52Contratada_AreaTrabalhoCod = H00NG3_A52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00NG3_A43Contratada_Ativo[0];
            A92Contrato_Ativo = H00NG3_A92Contrato_Ativo[0];
            A39Contratada_Codigo = H00NG3_A39Contratada_Codigo[0];
            A82Contrato_DataVigenciaInicio = H00NG3_A82Contrato_DataVigenciaInicio[0];
            A83Contrato_DataVigenciaTermino = H00NG3_A83Contrato_DataVigenciaTermino[0];
            A52Contratada_AreaTrabalhoCod = H00NG3_A52Contratada_AreaTrabalhoCod[0];
            A43Contratada_Ativo = H00NG3_A43Contratada_Ativo[0];
            AV11Vigencia_To = DateTime.MinValue;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Vigencia_To", context.localUtil.Format(AV11Vigencia_To, "99/99/99"));
            if ( ! (DateTime.MinValue==A82Contrato_DataVigenciaInicio) )
            {
               AV10Vigencia = A82Contrato_DataVigenciaInicio;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Vigencia", context.localUtil.Format(AV10Vigencia, "99/99/99"));
               AV11Vigencia_To = A83Contrato_DataVigenciaTermino;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Vigencia_To", context.localUtil.Format(AV11Vigencia_To, "99/99/99"));
               cmbavContrato_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV9q), 6, 0)), "N� "+StringUtil.Trim( A77Contrato_Numero)+"  Vig. "+context.localUtil.DToC( AV10Vigencia, 2, "/")+"-"+context.localUtil.DToC( AV11Vigencia_To, 2, "/"), 0);
               AV13Contratos.Add(A74Contrato_Codigo, 0);
               AV14Tipos.Add("C", 0);
               /* Execute user subroutine: 'ADDPERIODO' */
               S123 ();
               if ( returnInSub )
               {
                  pr_default.close(1);
                  returnInSub = true;
                  if (true) return;
               }
            }
            /* Using cursor H00NG4 */
            pr_default.execute(2, new Object[] {A74Contrato_Codigo, AV11Vigencia_To});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A316ContratoTermoAditivo_DataInicio = H00NG4_A316ContratoTermoAditivo_DataInicio[0];
               n316ContratoTermoAditivo_DataInicio = H00NG4_n316ContratoTermoAditivo_DataInicio[0];
               A317ContratoTermoAditivo_DataFim = H00NG4_A317ContratoTermoAditivo_DataFim[0];
               n317ContratoTermoAditivo_DataFim = H00NG4_n317ContratoTermoAditivo_DataFim[0];
               A315ContratoTermoAditivo_Codigo = H00NG4_A315ContratoTermoAditivo_Codigo[0];
               AV10Vigencia = A316ContratoTermoAditivo_DataInicio;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Vigencia", context.localUtil.Format(AV10Vigencia, "99/99/99"));
               AV11Vigencia_To = A317ContratoTermoAditivo_DataFim;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Vigencia_To", context.localUtil.Format(AV11Vigencia_To, "99/99/99"));
               cmbavContrato_codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV9q), 6, 0)), "N� "+StringUtil.Trim( A77Contrato_Numero)+"  Vig. "+context.localUtil.DToC( AV10Vigencia, 2, "/")+"-"+context.localUtil.DToC( AV11Vigencia_To, 2, "/"), 0);
               AV13Contratos.Add(A315ContratoTermoAditivo_Codigo, 0);
               AV14Tipos.Add("T", 0);
               /* Execute user subroutine: 'ADDPERIODO' */
               S123 ();
               if ( returnInSub )
               {
                  pr_default.close(2);
                  returnInSub = true;
                  if (true) return;
               }
               pr_default.readNext(2);
            }
            pr_default.close(2);
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void S123( )
      {
         /* 'ADDPERIODO' Routine */
         AV7Periodo.gxTpr_Inicio = AV10Vigencia;
         AV7Periodo.gxTpr_Fim = AV11Vigencia_To;
         AV8Periodos.Add(AV7Periodo, 0);
         AV7Periodo = new SdtSDT_Periodos(context);
         AV9q = (short)(AV9q+1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9q", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9q), 4, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E14NG2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_3_NG2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 20, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblContagemtitle_Internalname, "Execu��o contratual", "", "", lblContagemtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_ExecucaoContratual.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Prestadora:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ExecucaoContratual.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavContratada_codigo, dynavContratada_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contratada_Codigo), 6, 0)), 1, dynavContratada_codigo_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATADA_CODIGO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,14);\"", "", true, "HLP_WP_ExecucaoContratual.htm");
            dynavContratada_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV5Contratada_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavContratada_codigo_Internalname, "Values", (String)(dynavContratada_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Contrato / Vig�ncia:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_ExecucaoContratual.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContrato_codigo, cmbavContrato_codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV6Contrato_Codigo), 6, 0)), 1, cmbavContrato_codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "", true, "HLP_WP_ExecucaoContratual.htm");
            cmbavContrato_codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV6Contrato_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_codigo_Internalname, "Values", (String)(cmbavContrato_codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:bottom;height:50px")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Gerar relat�rio", bttButton1_Jsonclick, 5, "Gerar relat�rio", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ExecucaoContratual.htm");
            context.WriteHtmlText( "&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "color:#000000;";
            GxWebStd.gx_button_ctrl( context, bttButton2_Internalname, "", "Fechar", bttButton2_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_ExecucaoContratual.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_NG2e( true) ;
         }
         else
         {
            wb_table1_3_NG2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PANG2( ) ;
         WSNG2( ) ;
         WENG2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202031195169");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_execucaocontratual.js", "?2020311951610");
         context.AddJavascriptSource("Window/InNewWindowRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblContagemtitle_Internalname = "CONTAGEMTITLE";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         dynavContratada_codigo_Internalname = "vCONTRATADA_CODIGO";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         cmbavContrato_codigo_Internalname = "vCONTRATO_CODIGO";
         bttButton1_Internalname = "BUTTON1";
         bttButton2_Internalname = "BUTTON2";
         tblTable1_Internalname = "TABLE1";
         Innewwindow_Internalname = "INNEWWINDOW";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavContrato_codigo_Jsonclick = "";
         dynavContratada_codigo_Jsonclick = "";
         Innewwindow_Target = "";
         Innewwindow_Height = "50";
         Innewwindow_Width = "50";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Execucao Contratual";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12NG2',iparms:[{av:'AV5Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV6Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV13Contratos',fld:'vCONTRATOS',pic:'',nv:null},{av:'AV8Periodos',fld:'vPERIODOS',pic:'',nv:null},{av:'AV14Tipos',fld:'vTIPOS',pic:'',nv:null}],oparms:[{av:'Innewwindow_Target',ctrl:'INNEWWINDOW',prop:'Target'},{av:'Innewwindow_Height',ctrl:'INNEWWINDOW',prop:'Height'},{av:'Innewwindow_Width',ctrl:'INNEWWINDOW',prop:'Width'}]}");
         setEventMetadata("VCONTRATADA_CODIGO.CLICK","{handler:'E13NG2',iparms:[{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5Contratada_Codigo',fld:'vCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A82Contrato_DataVigenciaInicio',fld:'CONTRATO_DATAVIGENCIAINICIO',pic:'',nv:''},{av:'A83Contrato_DataVigenciaTermino',fld:'CONTRATO_DATAVIGENCIATERMINO',pic:'',nv:''},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV13Contratos',fld:'vCONTRATOS',pic:'',nv:null},{av:'AV14Tipos',fld:'vTIPOS',pic:'',nv:null},{av:'A316ContratoTermoAditivo_DataInicio',fld:'CONTRATOTERMOADITIVO_DATAINICIO',pic:'',nv:''},{av:'A317ContratoTermoAditivo_DataFim',fld:'CONTRATOTERMOADITIVO_DATAFIM',pic:'',nv:''},{av:'A315ContratoTermoAditivo_Codigo',fld:'CONTRATOTERMOADITIVO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV10Vigencia',fld:'vVIGENCIA',pic:'',nv:''},{av:'AV7Periodo',fld:'vPERIODO',pic:'',nv:null},{av:'AV11Vigencia_To',fld:'vVIGENCIA_TO',pic:'',nv:''},{av:'AV8Periodos',fld:'vPERIODOS',pic:'',nv:null},{av:'AV9q',fld:'vQ',pic:'ZZZ9',nv:0}],oparms:[{av:'AV6Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV9q',fld:'vQ',pic:'ZZZ9',nv:0},{av:'AV11Vigencia_To',fld:'vVIGENCIA_TO',pic:'',nv:''},{av:'AV10Vigencia',fld:'vVIGENCIA',pic:'',nv:''},{av:'AV13Contratos',fld:'vCONTRATOS',pic:'',nv:null},{av:'AV14Tipos',fld:'vTIPOS',pic:'',nv:null},{av:'AV7Periodo',fld:'vPERIODO',pic:'',nv:null},{av:'AV8Periodos',fld:'vPERIODOS',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV12WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV13Contratos = new GxSimpleCollection();
         AV8Periodos = new GxObjectCollection( context, "SDT_Periodos", "GxEv3Up14_Meetrika", "SdtSDT_Periodos", "GeneXus.Programs");
         AV14Tipos = new GxSimpleCollection();
         A82Contrato_DataVigenciaInicio = DateTime.MinValue;
         A83Contrato_DataVigenciaTermino = DateTime.MinValue;
         A77Contrato_Numero = "";
         A316ContratoTermoAditivo_DataInicio = DateTime.MinValue;
         A317ContratoTermoAditivo_DataFim = DateTime.MinValue;
         AV10Vigencia = DateTime.MinValue;
         AV7Periodo = new SdtSDT_Periodos(context);
         AV11Vigencia_To = DateTime.MinValue;
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00NG2_A40Contratada_PessoaCod = new int[1] ;
         H00NG2_A39Contratada_Codigo = new int[1] ;
         H00NG2_A41Contratada_PessoaNom = new String[] {""} ;
         H00NG2_n41Contratada_PessoaNom = new bool[] {false} ;
         H00NG2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00NG2_A43Contratada_Ativo = new bool[] {false} ;
         H00NG3_A74Contrato_Codigo = new int[1] ;
         H00NG3_A77Contrato_Numero = new String[] {""} ;
         H00NG3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         H00NG3_A43Contratada_Ativo = new bool[] {false} ;
         H00NG3_A92Contrato_Ativo = new bool[] {false} ;
         H00NG3_A39Contratada_Codigo = new int[1] ;
         H00NG3_A82Contrato_DataVigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00NG3_A83Contrato_DataVigenciaTermino = new DateTime[] {DateTime.MinValue} ;
         H00NG4_A74Contrato_Codigo = new int[1] ;
         H00NG4_A316ContratoTermoAditivo_DataInicio = new DateTime[] {DateTime.MinValue} ;
         H00NG4_n316ContratoTermoAditivo_DataInicio = new bool[] {false} ;
         H00NG4_A317ContratoTermoAditivo_DataFim = new DateTime[] {DateTime.MinValue} ;
         H00NG4_n317ContratoTermoAditivo_DataFim = new bool[] {false} ;
         H00NG4_A315ContratoTermoAditivo_Codigo = new int[1] ;
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblContagemtitle_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         TempTags = "";
         lblTextblock2_Jsonclick = "";
         bttButton1_Jsonclick = "";
         bttButton2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_execucaocontratual__default(),
            new Object[][] {
                new Object[] {
               H00NG2_A40Contratada_PessoaCod, H00NG2_A39Contratada_Codigo, H00NG2_A41Contratada_PessoaNom, H00NG2_n41Contratada_PessoaNom, H00NG2_A52Contratada_AreaTrabalhoCod, H00NG2_A43Contratada_Ativo
               }
               , new Object[] {
               H00NG3_A74Contrato_Codigo, H00NG3_A77Contrato_Numero, H00NG3_A52Contratada_AreaTrabalhoCod, H00NG3_A43Contratada_Ativo, H00NG3_A92Contrato_Ativo, H00NG3_A39Contratada_Codigo, H00NG3_A82Contrato_DataVigenciaInicio, H00NG3_A83Contrato_DataVigenciaTermino
               }
               , new Object[] {
               H00NG4_A74Contrato_Codigo, H00NG4_A316ContratoTermoAditivo_DataInicio, H00NG4_n316ContratoTermoAditivo_DataInicio, H00NG4_A317ContratoTermoAditivo_DataFim, H00NG4_n317ContratoTermoAditivo_DataFim, H00NG4_A315ContratoTermoAditivo_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV9q ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A39Contratada_Codigo ;
      private int A74Contrato_Codigo ;
      private int A315ContratoTermoAditivo_Codigo ;
      private int AV6Contrato_Codigo ;
      private int gxdynajaxindex ;
      private int AV5Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A77Contrato_Numero ;
      private String Innewwindow_Width ;
      private String Innewwindow_Height ;
      private String Innewwindow_Target ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String dynavContratada_codigo_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String cmbavContrato_codigo_Internalname ;
      private String Innewwindow_Internalname ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblContagemtitle_Internalname ;
      private String lblContagemtitle_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String TempTags ;
      private String dynavContratada_codigo_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String cmbavContrato_codigo_Jsonclick ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String bttButton2_Internalname ;
      private String bttButton2_Jsonclick ;
      private DateTime A82Contrato_DataVigenciaInicio ;
      private DateTime A83Contrato_DataVigenciaTermino ;
      private DateTime A316ContratoTermoAditivo_DataInicio ;
      private DateTime A317ContratoTermoAditivo_DataFim ;
      private DateTime AV10Vigencia ;
      private DateTime AV11Vigencia_To ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A92Contrato_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool A43Contratada_Ativo ;
      private bool n316ContratoTermoAditivo_DataInicio ;
      private bool n317ContratoTermoAditivo_DataFim ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynavContratada_codigo ;
      private GXCombobox cmbavContrato_codigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00NG2_A40Contratada_PessoaCod ;
      private int[] H00NG2_A39Contratada_Codigo ;
      private String[] H00NG2_A41Contratada_PessoaNom ;
      private bool[] H00NG2_n41Contratada_PessoaNom ;
      private int[] H00NG2_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00NG2_A43Contratada_Ativo ;
      private int[] H00NG3_A74Contrato_Codigo ;
      private String[] H00NG3_A77Contrato_Numero ;
      private int[] H00NG3_A52Contratada_AreaTrabalhoCod ;
      private bool[] H00NG3_A43Contratada_Ativo ;
      private bool[] H00NG3_A92Contrato_Ativo ;
      private int[] H00NG3_A39Contratada_Codigo ;
      private DateTime[] H00NG3_A82Contrato_DataVigenciaInicio ;
      private DateTime[] H00NG3_A83Contrato_DataVigenciaTermino ;
      private int[] H00NG4_A74Contrato_Codigo ;
      private DateTime[] H00NG4_A316ContratoTermoAditivo_DataInicio ;
      private bool[] H00NG4_n316ContratoTermoAditivo_DataInicio ;
      private DateTime[] H00NG4_A317ContratoTermoAditivo_DataFim ;
      private bool[] H00NG4_n317ContratoTermoAditivo_DataFim ;
      private int[] H00NG4_A315ContratoTermoAditivo_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV13Contratos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV14Tipos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Periodos ))]
      private IGxCollection AV8Periodos ;
      private GXWebForm Form ;
      private SdtSDT_Periodos AV7Periodo ;
      private wwpbaseobjects.SdtWWPContext AV12WWPContext ;
   }

   public class wp_execucaocontratual__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00NG2 ;
          prmH00NG2 = new Object[] {
          new Object[] {"@AV12WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NG3 ;
          prmH00NG3 = new Object[] {
          new Object[] {"@AV5Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00NG4 ;
          prmH00NG4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11Vigencia_To",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00NG2", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod], T1.[Contratada_Ativo] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE (T1.[Contratada_Ativo] = 1) AND (T1.[Contratada_AreaTrabalhoCod] = @AV12WWPC_1Areatrabalho_codigo) ORDER BY T2.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NG2,0,0,true,false )
             ,new CursorDef("H00NG3", "SELECT T1.[Contrato_Codigo], T1.[Contrato_Numero], T2.[Contratada_AreaTrabalhoCod], T2.[Contratada_Ativo], T1.[Contrato_Ativo], T1.[Contratada_Codigo], T1.[Contrato_DataVigenciaInicio], T1.[Contrato_DataVigenciaTermino] FROM ([Contrato] T1 WITH (NOLOCK) INNER JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[Contratada_Codigo]) WHERE (T1.[Contratada_Codigo] = @AV5Contratada_Codigo) AND (T1.[Contrato_Ativo] = 1) ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NG3,100,0,true,false )
             ,new CursorDef("H00NG4", "SELECT [Contrato_Codigo], [ContratoTermoAditivo_DataInicio], [ContratoTermoAditivo_DataFim], [ContratoTermoAditivo_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) WHERE ([Contrato_Codigo] = @Contrato_Codigo) AND ([ContratoTermoAditivo_DataInicio] > @AV11Vigencia_To) ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NG4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                ((bool[]) buf[4])[0] = rslt.getBool(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[7])[0] = rslt.getGXDate(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                return;
       }
    }

 }

}
