/*
               File: type_SdtProjeto
        Description: Projeto
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 0:21:11.13
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Projeto" )]
   [XmlType(TypeName =  "Projeto" , Namespace = "GxEv3Up14_Meetrika" )]
   [Serializable]
   public class SdtProjeto : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtProjeto( )
      {
         /* Constructor for serialization */
         gxTv_SdtProjeto_Projeto_nome = "";
         gxTv_SdtProjeto_Projeto_sigla = "";
         gxTv_SdtProjeto_Projeto_tipocontagem = "";
         gxTv_SdtProjeto_Projeto_tecnicacontagem = "";
         gxTv_SdtProjeto_Projeto_introducao = "";
         gxTv_SdtProjeto_Projeto_escopo = "";
         gxTv_SdtProjeto_Projeto_previsao = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_status = "";
         gxTv_SdtProjeto_Mode = "";
         gxTv_SdtProjeto_Projeto_nome_Z = "";
         gxTv_SdtProjeto_Projeto_sigla_Z = "";
         gxTv_SdtProjeto_Projeto_tipocontagem_Z = "";
         gxTv_SdtProjeto_Projeto_tecnicacontagem_Z = "";
         gxTv_SdtProjeto_Projeto_previsao_Z = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_status_Z = "";
      }

      public SdtProjeto( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV648Projeto_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV648Projeto_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"Projeto_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Projeto");
         metadata.Set("BT", "Projeto");
         metadata.Set("PK", "[ \"Projeto_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"Projeto_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"TipoProjeto_Codigo\" ],\"FKMap\":[ \"Projeto_TipoProjetoCod-TipoProjeto_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_sigla_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_tipoprojetocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_tipocontagem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_tecnicacontagem_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_previsao_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_gerentecod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_servicocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_custo_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_prazo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_esforco_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_sistemacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_status_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_fatorescala_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_fatormultiplicador_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_constacocomo_Z_double" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_incremental_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_tipoprojetocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_introducao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_previsao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_gerentecod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_servicocod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_fatorescala_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_fatormultiplicador_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_constacocomo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Projeto_incremental_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtProjeto deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_Meetrika" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtProjeto)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtProjeto obj ;
         obj = this;
         obj.gxTpr_Projeto_codigo = deserialized.gxTpr_Projeto_codigo;
         obj.gxTpr_Projeto_nome = deserialized.gxTpr_Projeto_nome;
         obj.gxTpr_Projeto_sigla = deserialized.gxTpr_Projeto_sigla;
         obj.gxTpr_Projeto_tipoprojetocod = deserialized.gxTpr_Projeto_tipoprojetocod;
         obj.gxTpr_Projeto_tipocontagem = deserialized.gxTpr_Projeto_tipocontagem;
         obj.gxTpr_Projeto_tecnicacontagem = deserialized.gxTpr_Projeto_tecnicacontagem;
         obj.gxTpr_Projeto_introducao = deserialized.gxTpr_Projeto_introducao;
         obj.gxTpr_Projeto_escopo = deserialized.gxTpr_Projeto_escopo;
         obj.gxTpr_Projeto_previsao = deserialized.gxTpr_Projeto_previsao;
         obj.gxTpr_Projeto_gerentecod = deserialized.gxTpr_Projeto_gerentecod;
         obj.gxTpr_Projeto_servicocod = deserialized.gxTpr_Projeto_servicocod;
         obj.gxTpr_Projeto_custo = deserialized.gxTpr_Projeto_custo;
         obj.gxTpr_Projeto_prazo = deserialized.gxTpr_Projeto_prazo;
         obj.gxTpr_Projeto_esforco = deserialized.gxTpr_Projeto_esforco;
         obj.gxTpr_Projeto_sistemacod = deserialized.gxTpr_Projeto_sistemacod;
         obj.gxTpr_Projeto_status = deserialized.gxTpr_Projeto_status;
         obj.gxTpr_Projeto_fatorescala = deserialized.gxTpr_Projeto_fatorescala;
         obj.gxTpr_Projeto_fatormultiplicador = deserialized.gxTpr_Projeto_fatormultiplicador;
         obj.gxTpr_Projeto_constacocomo = deserialized.gxTpr_Projeto_constacocomo;
         obj.gxTpr_Projeto_incremental = deserialized.gxTpr_Projeto_incremental;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Projeto_codigo_Z = deserialized.gxTpr_Projeto_codigo_Z;
         obj.gxTpr_Projeto_nome_Z = deserialized.gxTpr_Projeto_nome_Z;
         obj.gxTpr_Projeto_sigla_Z = deserialized.gxTpr_Projeto_sigla_Z;
         obj.gxTpr_Projeto_tipoprojetocod_Z = deserialized.gxTpr_Projeto_tipoprojetocod_Z;
         obj.gxTpr_Projeto_tipocontagem_Z = deserialized.gxTpr_Projeto_tipocontagem_Z;
         obj.gxTpr_Projeto_tecnicacontagem_Z = deserialized.gxTpr_Projeto_tecnicacontagem_Z;
         obj.gxTpr_Projeto_previsao_Z = deserialized.gxTpr_Projeto_previsao_Z;
         obj.gxTpr_Projeto_gerentecod_Z = deserialized.gxTpr_Projeto_gerentecod_Z;
         obj.gxTpr_Projeto_servicocod_Z = deserialized.gxTpr_Projeto_servicocod_Z;
         obj.gxTpr_Projeto_custo_Z = deserialized.gxTpr_Projeto_custo_Z;
         obj.gxTpr_Projeto_prazo_Z = deserialized.gxTpr_Projeto_prazo_Z;
         obj.gxTpr_Projeto_esforco_Z = deserialized.gxTpr_Projeto_esforco_Z;
         obj.gxTpr_Projeto_sistemacod_Z = deserialized.gxTpr_Projeto_sistemacod_Z;
         obj.gxTpr_Projeto_status_Z = deserialized.gxTpr_Projeto_status_Z;
         obj.gxTpr_Projeto_fatorescala_Z = deserialized.gxTpr_Projeto_fatorescala_Z;
         obj.gxTpr_Projeto_fatormultiplicador_Z = deserialized.gxTpr_Projeto_fatormultiplicador_Z;
         obj.gxTpr_Projeto_constacocomo_Z = deserialized.gxTpr_Projeto_constacocomo_Z;
         obj.gxTpr_Projeto_incremental_Z = deserialized.gxTpr_Projeto_incremental_Z;
         obj.gxTpr_Projeto_tipoprojetocod_N = deserialized.gxTpr_Projeto_tipoprojetocod_N;
         obj.gxTpr_Projeto_introducao_N = deserialized.gxTpr_Projeto_introducao_N;
         obj.gxTpr_Projeto_previsao_N = deserialized.gxTpr_Projeto_previsao_N;
         obj.gxTpr_Projeto_gerentecod_N = deserialized.gxTpr_Projeto_gerentecod_N;
         obj.gxTpr_Projeto_servicocod_N = deserialized.gxTpr_Projeto_servicocod_N;
         obj.gxTpr_Projeto_fatorescala_N = deserialized.gxTpr_Projeto_fatorescala_N;
         obj.gxTpr_Projeto_fatormultiplicador_N = deserialized.gxTpr_Projeto_fatormultiplicador_N;
         obj.gxTpr_Projeto_constacocomo_N = deserialized.gxTpr_Projeto_constacocomo_N;
         obj.gxTpr_Projeto_incremental_N = deserialized.gxTpr_Projeto_incremental_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Codigo") )
               {
                  gxTv_SdtProjeto_Projeto_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Nome") )
               {
                  gxTv_SdtProjeto_Projeto_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Sigla") )
               {
                  gxTv_SdtProjeto_Projeto_sigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TipoProjetoCod") )
               {
                  gxTv_SdtProjeto_Projeto_tipoprojetocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TipoContagem") )
               {
                  gxTv_SdtProjeto_Projeto_tipocontagem = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TecnicaContagem") )
               {
                  gxTv_SdtProjeto_Projeto_tecnicacontagem = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Introducao") )
               {
                  gxTv_SdtProjeto_Projeto_introducao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Escopo") )
               {
                  gxTv_SdtProjeto_Projeto_escopo = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Previsao") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProjeto_Projeto_previsao = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProjeto_Projeto_previsao = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GerenteCod") )
               {
                  gxTv_SdtProjeto_Projeto_gerentecod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ServicoCod") )
               {
                  gxTv_SdtProjeto_Projeto_servicocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Custo") )
               {
                  gxTv_SdtProjeto_Projeto_custo = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Prazo") )
               {
                  gxTv_SdtProjeto_Projeto_prazo = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Esforco") )
               {
                  gxTv_SdtProjeto_Projeto_esforco = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaCod") )
               {
                  gxTv_SdtProjeto_Projeto_sistemacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Status") )
               {
                  gxTv_SdtProjeto_Projeto_status = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorEscala") )
               {
                  gxTv_SdtProjeto_Projeto_fatorescala = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorMultiplicador") )
               {
                  gxTv_SdtProjeto_Projeto_fatormultiplicador = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ConstACocomo") )
               {
                  gxTv_SdtProjeto_Projeto_constacocomo = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Incremental") )
               {
                  gxTv_SdtProjeto_Projeto_incremental = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtProjeto_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtProjeto_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Codigo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Nome_Z") )
               {
                  gxTv_SdtProjeto_Projeto_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Sigla_Z") )
               {
                  gxTv_SdtProjeto_Projeto_sigla_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TipoProjetoCod_Z") )
               {
                  gxTv_SdtProjeto_Projeto_tipoprojetocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TipoContagem_Z") )
               {
                  gxTv_SdtProjeto_Projeto_tipocontagem_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TecnicaContagem_Z") )
               {
                  gxTv_SdtProjeto_Projeto_tecnicacontagem_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Previsao_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtProjeto_Projeto_previsao_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtProjeto_Projeto_previsao_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GerenteCod_Z") )
               {
                  gxTv_SdtProjeto_Projeto_gerentecod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ServicoCod_Z") )
               {
                  gxTv_SdtProjeto_Projeto_servicocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Custo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_custo_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Prazo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_prazo_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Esforco_Z") )
               {
                  gxTv_SdtProjeto_Projeto_esforco_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_SistemaCod_Z") )
               {
                  gxTv_SdtProjeto_Projeto_sistemacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Status_Z") )
               {
                  gxTv_SdtProjeto_Projeto_status_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorEscala_Z") )
               {
                  gxTv_SdtProjeto_Projeto_fatorescala_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorMultiplicador_Z") )
               {
                  gxTv_SdtProjeto_Projeto_fatormultiplicador_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ConstACocomo_Z") )
               {
                  gxTv_SdtProjeto_Projeto_constacocomo_Z = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Incremental_Z") )
               {
                  gxTv_SdtProjeto_Projeto_incremental_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_TipoProjetoCod_N") )
               {
                  gxTv_SdtProjeto_Projeto_tipoprojetocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Introducao_N") )
               {
                  gxTv_SdtProjeto_Projeto_introducao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Previsao_N") )
               {
                  gxTv_SdtProjeto_Projeto_previsao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_GerenteCod_N") )
               {
                  gxTv_SdtProjeto_Projeto_gerentecod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ServicoCod_N") )
               {
                  gxTv_SdtProjeto_Projeto_servicocod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorEscala_N") )
               {
                  gxTv_SdtProjeto_Projeto_fatorescala_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_FatorMultiplicador_N") )
               {
                  gxTv_SdtProjeto_Projeto_fatormultiplicador_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_ConstACocomo_N") )
               {
                  gxTv_SdtProjeto_Projeto_constacocomo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Projeto_Incremental_N") )
               {
                  gxTv_SdtProjeto_Projeto_incremental_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "Projeto";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_Meetrika";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Projeto_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_Nome", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_Sigla", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_sigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_TipoProjetoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_tipoprojetocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_TipoContagem", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_tipocontagem));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_TecnicaContagem", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_tecnicacontagem));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_Introducao", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_introducao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_Escopo", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_escopo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( (DateTime.MinValue==gxTv_SdtProjeto_Projeto_previsao) )
         {
            oWriter.WriteStartElement("Projeto_Previsao");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("Projeto_Previsao", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteElement("Projeto_GerenteCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_gerentecod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_ServicoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_servicocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_Custo", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_custo, 12, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_Prazo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_prazo), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_Esforco", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_esforco), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_SistemaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_sistemacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_Status", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_status));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_FatorEscala", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_fatorescala, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_FatorMultiplicador", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_fatormultiplicador, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_ConstACocomo", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_constacocomo, 6, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         oWriter.WriteElement("Projeto_Incremental", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtProjeto_Projeto_incremental)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtProjeto_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Nome_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Sigla_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_sigla_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_TipoProjetoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_tipoprojetocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_TipoContagem_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_tipocontagem_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_TecnicaContagem_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_tecnicacontagem_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            if ( (DateTime.MinValue==gxTv_SdtProjeto_Projeto_previsao_Z) )
            {
               oWriter.WriteStartElement("Projeto_Previsao_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("Projeto_Previsao_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
               }
            }
            oWriter.WriteElement("Projeto_GerenteCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_gerentecod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_ServicoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_servicocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Custo_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_custo_Z, 12, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Prazo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_prazo_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Esforco_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_esforco_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_SistemaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_sistemacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Status_Z", StringUtil.RTrim( gxTv_SdtProjeto_Projeto_status_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_FatorEscala_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_fatorescala_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_FatorMultiplicador_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_fatormultiplicador_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_ConstACocomo_Z", StringUtil.Trim( StringUtil.Str( gxTv_SdtProjeto_Projeto_constacocomo_Z, 6, 2)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Incremental_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtProjeto_Projeto_incremental_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_TipoProjetoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_tipoprojetocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Introducao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_introducao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Previsao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_previsao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_GerenteCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_gerentecod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_ServicoCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_servicocod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_FatorEscala_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_fatorescala_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_FatorMultiplicador_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_fatormultiplicador_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_ConstACocomo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_constacocomo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
            oWriter.WriteElement("Projeto_Incremental_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtProjeto_Projeto_incremental_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_Meetrika") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_Meetrika");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Projeto_Codigo", gxTv_SdtProjeto_Projeto_codigo, false);
         AddObjectProperty("Projeto_Nome", gxTv_SdtProjeto_Projeto_nome, false);
         AddObjectProperty("Projeto_Sigla", gxTv_SdtProjeto_Projeto_sigla, false);
         AddObjectProperty("Projeto_TipoProjetoCod", gxTv_SdtProjeto_Projeto_tipoprojetocod, false);
         AddObjectProperty("Projeto_TipoContagem", gxTv_SdtProjeto_Projeto_tipocontagem, false);
         AddObjectProperty("Projeto_TecnicaContagem", gxTv_SdtProjeto_Projeto_tecnicacontagem, false);
         AddObjectProperty("Projeto_Introducao", gxTv_SdtProjeto_Projeto_introducao, false);
         AddObjectProperty("Projeto_Escopo", gxTv_SdtProjeto_Projeto_escopo, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_previsao)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("Projeto_Previsao", sDateCnv, false);
         AddObjectProperty("Projeto_GerenteCod", gxTv_SdtProjeto_Projeto_gerentecod, false);
         AddObjectProperty("Projeto_ServicoCod", gxTv_SdtProjeto_Projeto_servicocod, false);
         AddObjectProperty("Projeto_Custo", gxTv_SdtProjeto_Projeto_custo, false);
         AddObjectProperty("Projeto_Prazo", gxTv_SdtProjeto_Projeto_prazo, false);
         AddObjectProperty("Projeto_Esforco", gxTv_SdtProjeto_Projeto_esforco, false);
         AddObjectProperty("Projeto_SistemaCod", gxTv_SdtProjeto_Projeto_sistemacod, false);
         AddObjectProperty("Projeto_Status", gxTv_SdtProjeto_Projeto_status, false);
         AddObjectProperty("Projeto_FatorEscala", gxTv_SdtProjeto_Projeto_fatorescala, false);
         AddObjectProperty("Projeto_FatorMultiplicador", gxTv_SdtProjeto_Projeto_fatormultiplicador, false);
         AddObjectProperty("Projeto_ConstACocomo", gxTv_SdtProjeto_Projeto_constacocomo, false);
         AddObjectProperty("Projeto_Incremental", gxTv_SdtProjeto_Projeto_incremental, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtProjeto_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtProjeto_Initialized, false);
            AddObjectProperty("Projeto_Codigo_Z", gxTv_SdtProjeto_Projeto_codigo_Z, false);
            AddObjectProperty("Projeto_Nome_Z", gxTv_SdtProjeto_Projeto_nome_Z, false);
            AddObjectProperty("Projeto_Sigla_Z", gxTv_SdtProjeto_Projeto_sigla_Z, false);
            AddObjectProperty("Projeto_TipoProjetoCod_Z", gxTv_SdtProjeto_Projeto_tipoprojetocod_Z, false);
            AddObjectProperty("Projeto_TipoContagem_Z", gxTv_SdtProjeto_Projeto_tipocontagem_Z, false);
            AddObjectProperty("Projeto_TecnicaContagem_Z", gxTv_SdtProjeto_Projeto_tecnicacontagem_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtProjeto_Projeto_previsao_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("Projeto_Previsao_Z", sDateCnv, false);
            AddObjectProperty("Projeto_GerenteCod_Z", gxTv_SdtProjeto_Projeto_gerentecod_Z, false);
            AddObjectProperty("Projeto_ServicoCod_Z", gxTv_SdtProjeto_Projeto_servicocod_Z, false);
            AddObjectProperty("Projeto_Custo_Z", gxTv_SdtProjeto_Projeto_custo_Z, false);
            AddObjectProperty("Projeto_Prazo_Z", gxTv_SdtProjeto_Projeto_prazo_Z, false);
            AddObjectProperty("Projeto_Esforco_Z", gxTv_SdtProjeto_Projeto_esforco_Z, false);
            AddObjectProperty("Projeto_SistemaCod_Z", gxTv_SdtProjeto_Projeto_sistemacod_Z, false);
            AddObjectProperty("Projeto_Status_Z", gxTv_SdtProjeto_Projeto_status_Z, false);
            AddObjectProperty("Projeto_FatorEscala_Z", gxTv_SdtProjeto_Projeto_fatorescala_Z, false);
            AddObjectProperty("Projeto_FatorMultiplicador_Z", gxTv_SdtProjeto_Projeto_fatormultiplicador_Z, false);
            AddObjectProperty("Projeto_ConstACocomo_Z", gxTv_SdtProjeto_Projeto_constacocomo_Z, false);
            AddObjectProperty("Projeto_Incremental_Z", gxTv_SdtProjeto_Projeto_incremental_Z, false);
            AddObjectProperty("Projeto_TipoProjetoCod_N", gxTv_SdtProjeto_Projeto_tipoprojetocod_N, false);
            AddObjectProperty("Projeto_Introducao_N", gxTv_SdtProjeto_Projeto_introducao_N, false);
            AddObjectProperty("Projeto_Previsao_N", gxTv_SdtProjeto_Projeto_previsao_N, false);
            AddObjectProperty("Projeto_GerenteCod_N", gxTv_SdtProjeto_Projeto_gerentecod_N, false);
            AddObjectProperty("Projeto_ServicoCod_N", gxTv_SdtProjeto_Projeto_servicocod_N, false);
            AddObjectProperty("Projeto_FatorEscala_N", gxTv_SdtProjeto_Projeto_fatorescala_N, false);
            AddObjectProperty("Projeto_FatorMultiplicador_N", gxTv_SdtProjeto_Projeto_fatormultiplicador_N, false);
            AddObjectProperty("Projeto_ConstACocomo_N", gxTv_SdtProjeto_Projeto_constacocomo_N, false);
            AddObjectProperty("Projeto_Incremental_N", gxTv_SdtProjeto_Projeto_incremental_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "Projeto_Codigo" )]
      [  XmlElement( ElementName = "Projeto_Codigo"   )]
      public int gxTpr_Projeto_codigo
      {
         get {
            return gxTv_SdtProjeto_Projeto_codigo ;
         }

         set {
            if ( gxTv_SdtProjeto_Projeto_codigo != value )
            {
               gxTv_SdtProjeto_Mode = "INS";
               this.gxTv_SdtProjeto_Projeto_codigo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_nome_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_sigla_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_tipoprojetocod_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_tipocontagem_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_tecnicacontagem_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_previsao_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_gerentecod_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_servicocod_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_custo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_prazo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_esforco_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_sistemacod_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_status_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_fatorescala_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_fatormultiplicador_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_constacocomo_Z_SetNull( );
               this.gxTv_SdtProjeto_Projeto_incremental_Z_SetNull( );
            }
            gxTv_SdtProjeto_Projeto_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_Nome" )]
      [  XmlElement( ElementName = "Projeto_Nome"   )]
      public String gxTpr_Projeto_nome
      {
         get {
            return gxTv_SdtProjeto_Projeto_nome ;
         }

         set {
            gxTv_SdtProjeto_Projeto_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_Sigla" )]
      [  XmlElement( ElementName = "Projeto_Sigla"   )]
      public String gxTpr_Projeto_sigla
      {
         get {
            return gxTv_SdtProjeto_Projeto_sigla ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_TipoProjetoCod" )]
      [  XmlElement( ElementName = "Projeto_TipoProjetoCod"   )]
      public int gxTpr_Projeto_tipoprojetocod
      {
         get {
            return gxTv_SdtProjeto_Projeto_tipoprojetocod ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tipoprojetocod_N = 0;
            gxTv_SdtProjeto_Projeto_tipoprojetocod = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_tipoprojetocod_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_tipoprojetocod_N = 1;
         gxTv_SdtProjeto_Projeto_tipoprojetocod = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_tipoprojetocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_TipoContagem" )]
      [  XmlElement( ElementName = "Projeto_TipoContagem"   )]
      public String gxTpr_Projeto_tipocontagem
      {
         get {
            return gxTv_SdtProjeto_Projeto_tipocontagem ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tipocontagem = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_TecnicaContagem" )]
      [  XmlElement( ElementName = "Projeto_TecnicaContagem"   )]
      public String gxTpr_Projeto_tecnicacontagem
      {
         get {
            return gxTv_SdtProjeto_Projeto_tecnicacontagem ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tecnicacontagem = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_Introducao" )]
      [  XmlElement( ElementName = "Projeto_Introducao"   )]
      public String gxTpr_Projeto_introducao
      {
         get {
            return gxTv_SdtProjeto_Projeto_introducao ;
         }

         set {
            gxTv_SdtProjeto_Projeto_introducao_N = 0;
            gxTv_SdtProjeto_Projeto_introducao = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_introducao_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_introducao_N = 1;
         gxTv_SdtProjeto_Projeto_introducao = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_introducao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Escopo" )]
      [  XmlElement( ElementName = "Projeto_Escopo"   )]
      public String gxTpr_Projeto_escopo
      {
         get {
            return gxTv_SdtProjeto_Projeto_escopo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_escopo = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_Previsao" )]
      [  XmlElement( ElementName = "Projeto_Previsao"  , IsNullable=true )]
      public string gxTpr_Projeto_previsao_Nullable
      {
         get {
            if ( gxTv_SdtProjeto_Projeto_previsao == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProjeto_Projeto_previsao).value ;
         }

         set {
            gxTv_SdtProjeto_Projeto_previsao_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProjeto_Projeto_previsao = DateTime.MinValue;
            else
               gxTv_SdtProjeto_Projeto_previsao = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Projeto_previsao
      {
         get {
            return gxTv_SdtProjeto_Projeto_previsao ;
         }

         set {
            gxTv_SdtProjeto_Projeto_previsao_N = 0;
            gxTv_SdtProjeto_Projeto_previsao = (DateTime)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_previsao_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_previsao_N = 1;
         gxTv_SdtProjeto_Projeto_previsao = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_previsao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GerenteCod" )]
      [  XmlElement( ElementName = "Projeto_GerenteCod"   )]
      public int gxTpr_Projeto_gerentecod
      {
         get {
            return gxTv_SdtProjeto_Projeto_gerentecod ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gerentecod_N = 0;
            gxTv_SdtProjeto_Projeto_gerentecod = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gerentecod_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gerentecod_N = 1;
         gxTv_SdtProjeto_Projeto_gerentecod = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gerentecod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ServicoCod" )]
      [  XmlElement( ElementName = "Projeto_ServicoCod"   )]
      public int gxTpr_Projeto_servicocod
      {
         get {
            return gxTv_SdtProjeto_Projeto_servicocod ;
         }

         set {
            gxTv_SdtProjeto_Projeto_servicocod_N = 0;
            gxTv_SdtProjeto_Projeto_servicocod = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_servicocod_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_servicocod_N = 1;
         gxTv_SdtProjeto_Projeto_servicocod = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_servicocod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Custo" )]
      [  XmlElement( ElementName = "Projeto_Custo"   )]
      public double gxTpr_Projeto_custo_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_custo) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custo = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_custo
      {
         get {
            return gxTv_SdtProjeto_Projeto_custo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custo = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_custo_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_custo = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_custo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Prazo" )]
      [  XmlElement( ElementName = "Projeto_Prazo"   )]
      public short gxTpr_Projeto_prazo
      {
         get {
            return gxTv_SdtProjeto_Projeto_prazo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_prazo = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_prazo_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_prazo = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_prazo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Esforco" )]
      [  XmlElement( ElementName = "Projeto_Esforco"   )]
      public short gxTpr_Projeto_esforco
      {
         get {
            return gxTv_SdtProjeto_Projeto_esforco ;
         }

         set {
            gxTv_SdtProjeto_Projeto_esforco = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_esforco_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_esforco = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_esforco_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaCod" )]
      [  XmlElement( ElementName = "Projeto_SistemaCod"   )]
      public int gxTpr_Projeto_sistemacod
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemacod ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemacod = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemacod_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemacod = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Status" )]
      [  XmlElement( ElementName = "Projeto_Status"   )]
      public String gxTpr_Projeto_status
      {
         get {
            return gxTv_SdtProjeto_Projeto_status ;
         }

         set {
            gxTv_SdtProjeto_Projeto_status = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Projeto_FatorEscala" )]
      [  XmlElement( ElementName = "Projeto_FatorEscala"   )]
      public double gxTpr_Projeto_fatorescala_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_fatorescala) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatorescala_N = 0;
            gxTv_SdtProjeto_Projeto_fatorescala = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_fatorescala
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatorescala ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatorescala_N = 0;
            gxTv_SdtProjeto_Projeto_fatorescala = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatorescala_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatorescala_N = 1;
         gxTv_SdtProjeto_Projeto_fatorescala = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatorescala_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_FatorMultiplicador" )]
      [  XmlElement( ElementName = "Projeto_FatorMultiplicador"   )]
      public double gxTpr_Projeto_fatormultiplicador_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_fatormultiplicador) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatormultiplicador_N = 0;
            gxTv_SdtProjeto_Projeto_fatormultiplicador = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_fatormultiplicador
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatormultiplicador ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatormultiplicador_N = 0;
            gxTv_SdtProjeto_Projeto_fatormultiplicador = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatormultiplicador_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatormultiplicador_N = 1;
         gxTv_SdtProjeto_Projeto_fatormultiplicador = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatormultiplicador_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ConstACocomo" )]
      [  XmlElement( ElementName = "Projeto_ConstACocomo"   )]
      public double gxTpr_Projeto_constacocomo_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_constacocomo) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_constacocomo_N = 0;
            gxTv_SdtProjeto_Projeto_constacocomo = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_constacocomo
      {
         get {
            return gxTv_SdtProjeto_Projeto_constacocomo ;
         }

         set {
            gxTv_SdtProjeto_Projeto_constacocomo_N = 0;
            gxTv_SdtProjeto_Projeto_constacocomo = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_constacocomo_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_constacocomo_N = 1;
         gxTv_SdtProjeto_Projeto_constacocomo = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_constacocomo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Incremental" )]
      [  XmlElement( ElementName = "Projeto_Incremental"   )]
      public bool gxTpr_Projeto_incremental
      {
         get {
            return gxTv_SdtProjeto_Projeto_incremental ;
         }

         set {
            gxTv_SdtProjeto_Projeto_incremental_N = 0;
            gxTv_SdtProjeto_Projeto_incremental = value;
         }

      }

      public void gxTv_SdtProjeto_Projeto_incremental_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_incremental_N = 1;
         gxTv_SdtProjeto_Projeto_incremental = false;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_incremental_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtProjeto_Mode ;
         }

         set {
            gxTv_SdtProjeto_Mode = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Mode_SetNull( )
      {
         gxTv_SdtProjeto_Mode = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtProjeto_Initialized ;
         }

         set {
            gxTv_SdtProjeto_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Initialized_SetNull( )
      {
         gxTv_SdtProjeto_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Codigo_Z" )]
      [  XmlElement( ElementName = "Projeto_Codigo_Z"   )]
      public int gxTpr_Projeto_codigo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_codigo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_codigo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Nome_Z" )]
      [  XmlElement( ElementName = "Projeto_Nome_Z"   )]
      public String gxTpr_Projeto_nome_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_nome_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_nome_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Sigla_Z" )]
      [  XmlElement( ElementName = "Projeto_Sigla_Z"   )]
      public String gxTpr_Projeto_sigla_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_sigla_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sigla_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sigla_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sigla_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sigla_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_TipoProjetoCod_Z" )]
      [  XmlElement( ElementName = "Projeto_TipoProjetoCod_Z"   )]
      public int gxTpr_Projeto_tipoprojetocod_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_tipoprojetocod_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tipoprojetocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_tipoprojetocod_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_tipoprojetocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_tipoprojetocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_TipoContagem_Z" )]
      [  XmlElement( ElementName = "Projeto_TipoContagem_Z"   )]
      public String gxTpr_Projeto_tipocontagem_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_tipocontagem_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tipocontagem_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_tipocontagem_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_tipocontagem_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_tipocontagem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_TecnicaContagem_Z" )]
      [  XmlElement( ElementName = "Projeto_TecnicaContagem_Z"   )]
      public String gxTpr_Projeto_tecnicacontagem_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_tecnicacontagem_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tecnicacontagem_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_tecnicacontagem_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_tecnicacontagem_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_tecnicacontagem_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Previsao_Z" )]
      [  XmlElement( ElementName = "Projeto_Previsao_Z"  , IsNullable=true )]
      public string gxTpr_Projeto_previsao_Z_Nullable
      {
         get {
            if ( gxTv_SdtProjeto_Projeto_previsao_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtProjeto_Projeto_previsao_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtProjeto_Projeto_previsao_Z = DateTime.MinValue;
            else
               gxTv_SdtProjeto_Projeto_previsao_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Projeto_previsao_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_previsao_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_previsao_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_previsao_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_previsao_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_previsao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GerenteCod_Z" )]
      [  XmlElement( ElementName = "Projeto_GerenteCod_Z"   )]
      public int gxTpr_Projeto_gerentecod_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_gerentecod_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gerentecod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gerentecod_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gerentecod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gerentecod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ServicoCod_Z" )]
      [  XmlElement( ElementName = "Projeto_ServicoCod_Z"   )]
      public int gxTpr_Projeto_servicocod_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_servicocod_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_servicocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_servicocod_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_servicocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_servicocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Custo_Z" )]
      [  XmlElement( ElementName = "Projeto_Custo_Z"   )]
      public double gxTpr_Projeto_custo_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_custo_Z) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custo_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_custo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_custo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_custo_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_custo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_custo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_custo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Prazo_Z" )]
      [  XmlElement( ElementName = "Projeto_Prazo_Z"   )]
      public short gxTpr_Projeto_prazo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_prazo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_prazo_Z = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_prazo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_prazo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_prazo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Esforco_Z" )]
      [  XmlElement( ElementName = "Projeto_Esforco_Z"   )]
      public short gxTpr_Projeto_esforco_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_esforco_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_esforco_Z = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_esforco_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_esforco_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_esforco_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_SistemaCod_Z" )]
      [  XmlElement( ElementName = "Projeto_SistemaCod_Z"   )]
      public int gxTpr_Projeto_sistemacod_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_sistemacod_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_sistemacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_sistemacod_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_sistemacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_sistemacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Status_Z" )]
      [  XmlElement( ElementName = "Projeto_Status_Z"   )]
      public String gxTpr_Projeto_status_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_status_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_status_Z = (String)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_status_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_status_Z = "";
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_status_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_FatorEscala_Z" )]
      [  XmlElement( ElementName = "Projeto_FatorEscala_Z"   )]
      public double gxTpr_Projeto_fatorescala_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_fatorescala_Z) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatorescala_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_fatorescala_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatorescala_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatorescala_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatorescala_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatorescala_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatorescala_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_FatorMultiplicador_Z" )]
      [  XmlElement( ElementName = "Projeto_FatorMultiplicador_Z"   )]
      public double gxTpr_Projeto_fatormultiplicador_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_fatormultiplicador_Z) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatormultiplicador_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_fatormultiplicador_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatormultiplicador_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatormultiplicador_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatormultiplicador_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatormultiplicador_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatormultiplicador_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ConstACocomo_Z" )]
      [  XmlElement( ElementName = "Projeto_ConstACocomo_Z"   )]
      public double gxTpr_Projeto_constacocomo_Z_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtProjeto_Projeto_constacocomo_Z) ;
         }

         set {
            gxTv_SdtProjeto_Projeto_constacocomo_Z = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Projeto_constacocomo_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_constacocomo_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_constacocomo_Z = (decimal)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_constacocomo_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_constacocomo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_constacocomo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Incremental_Z" )]
      [  XmlElement( ElementName = "Projeto_Incremental_Z"   )]
      public bool gxTpr_Projeto_incremental_Z
      {
         get {
            return gxTv_SdtProjeto_Projeto_incremental_Z ;
         }

         set {
            gxTv_SdtProjeto_Projeto_incremental_Z = value;
         }

      }

      public void gxTv_SdtProjeto_Projeto_incremental_Z_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_incremental_Z = false;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_incremental_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_TipoProjetoCod_N" )]
      [  XmlElement( ElementName = "Projeto_TipoProjetoCod_N"   )]
      public short gxTpr_Projeto_tipoprojetocod_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_tipoprojetocod_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_tipoprojetocod_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_tipoprojetocod_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_tipoprojetocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_tipoprojetocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Introducao_N" )]
      [  XmlElement( ElementName = "Projeto_Introducao_N"   )]
      public short gxTpr_Projeto_introducao_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_introducao_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_introducao_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_introducao_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_introducao_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_introducao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Previsao_N" )]
      [  XmlElement( ElementName = "Projeto_Previsao_N"   )]
      public short gxTpr_Projeto_previsao_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_previsao_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_previsao_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_previsao_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_previsao_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_previsao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_GerenteCod_N" )]
      [  XmlElement( ElementName = "Projeto_GerenteCod_N"   )]
      public short gxTpr_Projeto_gerentecod_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_gerentecod_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_gerentecod_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_gerentecod_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_gerentecod_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_gerentecod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ServicoCod_N" )]
      [  XmlElement( ElementName = "Projeto_ServicoCod_N"   )]
      public short gxTpr_Projeto_servicocod_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_servicocod_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_servicocod_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_servicocod_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_servicocod_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_servicocod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_FatorEscala_N" )]
      [  XmlElement( ElementName = "Projeto_FatorEscala_N"   )]
      public short gxTpr_Projeto_fatorescala_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatorescala_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatorescala_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatorescala_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatorescala_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatorescala_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_FatorMultiplicador_N" )]
      [  XmlElement( ElementName = "Projeto_FatorMultiplicador_N"   )]
      public short gxTpr_Projeto_fatormultiplicador_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_fatormultiplicador_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_fatormultiplicador_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_fatormultiplicador_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_fatormultiplicador_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_fatormultiplicador_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_ConstACocomo_N" )]
      [  XmlElement( ElementName = "Projeto_ConstACocomo_N"   )]
      public short gxTpr_Projeto_constacocomo_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_constacocomo_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_constacocomo_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_constacocomo_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_constacocomo_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_constacocomo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Projeto_Incremental_N" )]
      [  XmlElement( ElementName = "Projeto_Incremental_N"   )]
      public short gxTpr_Projeto_incremental_N
      {
         get {
            return gxTv_SdtProjeto_Projeto_incremental_N ;
         }

         set {
            gxTv_SdtProjeto_Projeto_incremental_N = (short)(value);
         }

      }

      public void gxTv_SdtProjeto_Projeto_incremental_N_SetNull( )
      {
         gxTv_SdtProjeto_Projeto_incremental_N = 0;
         return  ;
      }

      public bool gxTv_SdtProjeto_Projeto_incremental_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtProjeto_Projeto_nome = "";
         gxTv_SdtProjeto_Projeto_sigla = "";
         gxTv_SdtProjeto_Projeto_tipocontagem = "";
         gxTv_SdtProjeto_Projeto_tecnicacontagem = "";
         gxTv_SdtProjeto_Projeto_introducao = "";
         gxTv_SdtProjeto_Projeto_escopo = "";
         gxTv_SdtProjeto_Projeto_previsao = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_status = "A";
         gxTv_SdtProjeto_Mode = "";
         gxTv_SdtProjeto_Projeto_nome_Z = "";
         gxTv_SdtProjeto_Projeto_sigla_Z = "";
         gxTv_SdtProjeto_Projeto_tipocontagem_Z = "";
         gxTv_SdtProjeto_Projeto_tecnicacontagem_Z = "";
         gxTv_SdtProjeto_Projeto_previsao_Z = DateTime.MinValue;
         gxTv_SdtProjeto_Projeto_status_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "projeto", "GeneXus.Programs.projeto_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtProjeto_Projeto_prazo ;
      private short gxTv_SdtProjeto_Projeto_esforco ;
      private short gxTv_SdtProjeto_Initialized ;
      private short gxTv_SdtProjeto_Projeto_prazo_Z ;
      private short gxTv_SdtProjeto_Projeto_esforco_Z ;
      private short gxTv_SdtProjeto_Projeto_tipoprojetocod_N ;
      private short gxTv_SdtProjeto_Projeto_introducao_N ;
      private short gxTv_SdtProjeto_Projeto_previsao_N ;
      private short gxTv_SdtProjeto_Projeto_gerentecod_N ;
      private short gxTv_SdtProjeto_Projeto_servicocod_N ;
      private short gxTv_SdtProjeto_Projeto_fatorescala_N ;
      private short gxTv_SdtProjeto_Projeto_fatormultiplicador_N ;
      private short gxTv_SdtProjeto_Projeto_constacocomo_N ;
      private short gxTv_SdtProjeto_Projeto_incremental_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtProjeto_Projeto_codigo ;
      private int gxTv_SdtProjeto_Projeto_tipoprojetocod ;
      private int gxTv_SdtProjeto_Projeto_gerentecod ;
      private int gxTv_SdtProjeto_Projeto_servicocod ;
      private int gxTv_SdtProjeto_Projeto_sistemacod ;
      private int gxTv_SdtProjeto_Projeto_codigo_Z ;
      private int gxTv_SdtProjeto_Projeto_tipoprojetocod_Z ;
      private int gxTv_SdtProjeto_Projeto_gerentecod_Z ;
      private int gxTv_SdtProjeto_Projeto_servicocod_Z ;
      private int gxTv_SdtProjeto_Projeto_sistemacod_Z ;
      private decimal gxTv_SdtProjeto_Projeto_custo ;
      private decimal gxTv_SdtProjeto_Projeto_fatorescala ;
      private decimal gxTv_SdtProjeto_Projeto_fatormultiplicador ;
      private decimal gxTv_SdtProjeto_Projeto_constacocomo ;
      private decimal gxTv_SdtProjeto_Projeto_custo_Z ;
      private decimal gxTv_SdtProjeto_Projeto_fatorescala_Z ;
      private decimal gxTv_SdtProjeto_Projeto_fatormultiplicador_Z ;
      private decimal gxTv_SdtProjeto_Projeto_constacocomo_Z ;
      private String gxTv_SdtProjeto_Projeto_nome ;
      private String gxTv_SdtProjeto_Projeto_sigla ;
      private String gxTv_SdtProjeto_Projeto_tipocontagem ;
      private String gxTv_SdtProjeto_Projeto_tecnicacontagem ;
      private String gxTv_SdtProjeto_Projeto_status ;
      private String gxTv_SdtProjeto_Mode ;
      private String gxTv_SdtProjeto_Projeto_nome_Z ;
      private String gxTv_SdtProjeto_Projeto_sigla_Z ;
      private String gxTv_SdtProjeto_Projeto_tipocontagem_Z ;
      private String gxTv_SdtProjeto_Projeto_tecnicacontagem_Z ;
      private String gxTv_SdtProjeto_Projeto_status_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtProjeto_Projeto_previsao ;
      private DateTime gxTv_SdtProjeto_Projeto_previsao_Z ;
      private bool gxTv_SdtProjeto_Projeto_incremental ;
      private bool gxTv_SdtProjeto_Projeto_incremental_Z ;
      private String gxTv_SdtProjeto_Projeto_introducao ;
      private String gxTv_SdtProjeto_Projeto_escopo ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"Projeto", Namespace = "GxEv3Up14_Meetrika")]
   public class SdtProjeto_RESTInterface : GxGenericCollectionItem<SdtProjeto>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtProjeto_RESTInterface( ) : base()
      {
      }

      public SdtProjeto_RESTInterface( SdtProjeto psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Projeto_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_codigo
      {
         get {
            return sdt.gxTpr_Projeto_codigo ;
         }

         set {
            sdt.gxTpr_Projeto_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_Nome" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Projeto_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_nome) ;
         }

         set {
            sdt.gxTpr_Projeto_nome = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_Sigla" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Projeto_sigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_sigla) ;
         }

         set {
            sdt.gxTpr_Projeto_sigla = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_TipoProjetoCod" , Order = 3 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_tipoprojetocod
      {
         get {
            return sdt.gxTpr_Projeto_tipoprojetocod ;
         }

         set {
            sdt.gxTpr_Projeto_tipoprojetocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_TipoContagem" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Projeto_tipocontagem
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_tipocontagem) ;
         }

         set {
            sdt.gxTpr_Projeto_tipocontagem = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_TecnicaContagem" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Projeto_tecnicacontagem
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_tecnicacontagem) ;
         }

         set {
            sdt.gxTpr_Projeto_tecnicacontagem = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_Introducao" , Order = 6 )]
      public String gxTpr_Projeto_introducao
      {
         get {
            return sdt.gxTpr_Projeto_introducao ;
         }

         set {
            sdt.gxTpr_Projeto_introducao = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_Escopo" , Order = 7 )]
      public String gxTpr_Projeto_escopo
      {
         get {
            return sdt.gxTpr_Projeto_escopo ;
         }

         set {
            sdt.gxTpr_Projeto_escopo = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_Previsao" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Projeto_previsao
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Projeto_previsao) ;
         }

         set {
            sdt.gxTpr_Projeto_previsao = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "Projeto_GerenteCod" , Order = 9 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_gerentecod
      {
         get {
            return sdt.gxTpr_Projeto_gerentecod ;
         }

         set {
            sdt.gxTpr_Projeto_gerentecod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_ServicoCod" , Order = 10 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_servicocod
      {
         get {
            return sdt.gxTpr_Projeto_servicocod ;
         }

         set {
            sdt.gxTpr_Projeto_servicocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_Custo" , Order = 11 )]
      [GxSeudo()]
      public String gxTpr_Projeto_custo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Projeto_custo, 12, 2)) ;
         }

         set {
            sdt.gxTpr_Projeto_custo = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "Projeto_Prazo" , Order = 12 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Projeto_prazo
      {
         get {
            return sdt.gxTpr_Projeto_prazo ;
         }

         set {
            sdt.gxTpr_Projeto_prazo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_Esforco" , Order = 13 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Projeto_esforco
      {
         get {
            return sdt.gxTpr_Projeto_esforco ;
         }

         set {
            sdt.gxTpr_Projeto_esforco = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_SistemaCod" , Order = 14 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Projeto_sistemacod
      {
         get {
            return sdt.gxTpr_Projeto_sistemacod ;
         }

         set {
            sdt.gxTpr_Projeto_sistemacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_Status" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Projeto_status
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Projeto_status) ;
         }

         set {
            sdt.gxTpr_Projeto_status = (String)(value);
         }

      }

      [DataMember( Name = "Projeto_FatorEscala" , Order = 16 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Projeto_fatorescala
      {
         get {
            return sdt.gxTpr_Projeto_fatorescala ;
         }

         set {
            sdt.gxTpr_Projeto_fatorescala = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_FatorMultiplicador" , Order = 17 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Projeto_fatormultiplicador
      {
         get {
            return sdt.gxTpr_Projeto_fatormultiplicador ;
         }

         set {
            sdt.gxTpr_Projeto_fatormultiplicador = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_ConstACocomo" , Order = 18 )]
      [GxSeudo()]
      public Nullable<decimal> gxTpr_Projeto_constacocomo
      {
         get {
            return sdt.gxTpr_Projeto_constacocomo ;
         }

         set {
            sdt.gxTpr_Projeto_constacocomo = (decimal)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Projeto_Incremental" , Order = 19 )]
      [GxSeudo()]
      public bool gxTpr_Projeto_incremental
      {
         get {
            return sdt.gxTpr_Projeto_incremental ;
         }

         set {
            sdt.gxTpr_Projeto_incremental = value;
         }

      }

      public SdtProjeto sdt
      {
         get {
            return (SdtProjeto)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtProjeto() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 49 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
