/*
               File: ServicoFluxoGeneral
        Description: Servico Fluxo General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 17:28:58.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicofluxogeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public servicofluxogeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public servicofluxogeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ServicoFluxo_Codigo )
      {
         this.A1528ServicoFluxo_Codigo = aP0_ServicoFluxo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A1528ServicoFluxo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A1528ServicoFluxo_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PALH2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "ServicoFluxoGeneral";
               context.Gx_err = 0;
               WSLH2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Servico Fluxo General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203117285814");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("servicofluxogeneral.aspx") + "?" + UrlEncode("" +A1528ServicoFluxo_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA1528ServicoFluxo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_SERVICOPOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_SERVICOFLUXO_SERVICOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormLH2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("servicofluxogeneral.js", "?20203117285815");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ServicoFluxoGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Servico Fluxo General" ;
      }

      protected void WBLH0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "servicofluxogeneral.aspx");
            }
            wb_table1_2_LH2( true) ;
         }
         else
         {
            wb_table1_2_LH2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_LH2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1528ServicoFluxo_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtServicoFluxo_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoFluxoGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_ServicoCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_ServicoCod_Jsonclick, 0, "Attribute", "", "", "", edtServicoFluxo_ServicoCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoFluxoGeneral.htm");
         }
         wbLoad = true;
      }

      protected void STARTLH2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Servico Fluxo General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPLH0( ) ;
            }
         }
      }

      protected void WSLH2( )
      {
         STARTLH2( ) ;
         EVTLH2( ) ;
      }

      protected void EVTLH2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11LH2 */
                                    E11LH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12LH2 */
                                    E12LH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13LH2 */
                                    E13LH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14LH2 */
                                    E14LH2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPLH0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WELH2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormLH2( ) ;
            }
         }
      }

      protected void PALH2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFLH2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "ServicoFluxoGeneral";
         context.Gx_err = 0;
      }

      protected void RFLH2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00LH2 */
            pr_default.execute(0, new Object[] {A1528ServicoFluxo_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1522ServicoFluxo_ServicoCod = H00LH2_A1522ServicoFluxo_ServicoCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOFLUXO_SERVICOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9")));
               A1532ServicoFluxo_Ordem = H00LH2_A1532ServicoFluxo_Ordem[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1532ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1532ServicoFluxo_Ordem), 3, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOFLUXO_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9")));
               n1532ServicoFluxo_Ordem = H00LH2_n1532ServicoFluxo_Ordem[0];
               A1526ServicoFluxo_ServicoPos = H00LH2_A1526ServicoFluxo_ServicoPos[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1526ServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOFLUXO_SERVICOPOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9")));
               n1526ServicoFluxo_ServicoPos = H00LH2_n1526ServicoFluxo_ServicoPos[0];
               A1523ServicoFluxo_ServicoSigla = H00LH2_A1523ServicoFluxo_ServicoSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1523ServicoFluxo_ServicoSigla", A1523ServicoFluxo_ServicoSigla);
               n1523ServicoFluxo_ServicoSigla = H00LH2_n1523ServicoFluxo_ServicoSigla[0];
               A1523ServicoFluxo_ServicoSigla = H00LH2_A1523ServicoFluxo_ServicoSigla[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1523ServicoFluxo_ServicoSigla", A1523ServicoFluxo_ServicoSigla);
               n1523ServicoFluxo_ServicoSigla = H00LH2_n1523ServicoFluxo_ServicoSigla[0];
               /* Execute user event: E12LH2 */
               E12LH2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBLH0( ) ;
         }
      }

      protected void STRUPLH0( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "ServicoFluxoGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11LH2 */
         E11LH2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1523ServicoFluxo_ServicoSigla = StringUtil.Upper( cgiGet( edtServicoFluxo_ServicoSigla_Internalname));
            n1523ServicoFluxo_ServicoSigla = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1523ServicoFluxo_ServicoSigla", A1523ServicoFluxo_ServicoSigla);
            A1526ServicoFluxo_ServicoPos = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_ServicoPos_Internalname), ",", "."));
            n1526ServicoFluxo_ServicoPos = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1526ServicoFluxo_ServicoPos", StringUtil.LTrim( StringUtil.Str( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOFLUXO_SERVICOPOS", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9")));
            A1532ServicoFluxo_Ordem = (short)(context.localUtil.CToN( cgiGet( edtServicoFluxo_Ordem_Internalname), ",", "."));
            n1532ServicoFluxo_Ordem = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1532ServicoFluxo_Ordem", StringUtil.LTrim( StringUtil.Str( (decimal)(A1532ServicoFluxo_Ordem), 3, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOFLUXO_ORDEM", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9")));
            A1522ServicoFluxo_ServicoCod = (int)(context.localUtil.CToN( cgiGet( edtServicoFluxo_ServicoCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1522ServicoFluxo_ServicoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1522ServicoFluxo_ServicoCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_SERVICOFLUXO_SERVICOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1522ServicoFluxo_ServicoCod), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA1528ServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1528ServicoFluxo_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11LH2 */
         E11LH2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11LH2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12LH2( )
      {
         /* Load Routine */
         edtServicoFluxo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_Codigo_Visible), 5, 0)));
         edtServicoFluxo_ServicoCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtServicoFluxo_ServicoCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtServicoFluxo_ServicoCod_Visible), 5, 0)));
      }

      protected void E13LH2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("servicofluxo.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1528ServicoFluxo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14LH2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("servicofluxo.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1528ServicoFluxo_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ServicoFluxo";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "ServicoFluxo_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ServicoFluxo_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_Meetrika"));
      }

      protected void wb_table1_2_LH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_LH2( true) ;
         }
         else
         {
            wb_table2_8_LH2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_LH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_LH2( true) ;
         }
         else
         {
            wb_table3_26_LH2( false) ;
         }
         return  ;
      }

      protected void wb_table3_26_LH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_LH2e( true) ;
         }
         else
         {
            wb_table1_2_LH2e( false) ;
         }
      }

      protected void wb_table3_26_LH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoFluxoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ServicoFluxoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_LH2e( true) ;
         }
         else
         {
            wb_table3_26_LH2e( false) ;
         }
      }

      protected void wb_table2_8_LH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_servicosigla_Internalname, "Servi�o", "", "", lblTextblockservicofluxo_servicosigla_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_ServicoSigla_Internalname, StringUtil.RTrim( A1523ServicoFluxo_ServicoSigla), StringUtil.RTrim( context.localUtil.Format( A1523ServicoFluxo_ServicoSigla, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_ServicoSigla_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ServicoFluxoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_servicopos_Internalname, "Processo", "", "", lblTextblockservicofluxo_servicopos_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_ServicoPos_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1526ServicoFluxo_ServicoPos), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1526ServicoFluxo_ServicoPos), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_ServicoPos_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ServicoFluxoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockservicofluxo_ordem_Internalname, "Ordem", "", "", lblTextblockservicofluxo_ordem_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ServicoFluxoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtServicoFluxo_Ordem_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1532ServicoFluxo_Ordem), 3, 0, ",", "")), context.localUtil.Format( (decimal)(A1532ServicoFluxo_Ordem), "ZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtServicoFluxo_Ordem_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "px", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "Ordem", "right", false, "HLP_ServicoFluxoGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_LH2e( true) ;
         }
         else
         {
            wb_table2_8_LH2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A1528ServicoFluxo_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PALH2( ) ;
         WSLH2( ) ;
         WELH2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA1528ServicoFluxo_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PALH2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "servicofluxogeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PALH2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A1528ServicoFluxo_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
         }
         wcpOA1528ServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA1528ServicoFluxo_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A1528ServicoFluxo_Codigo != wcpOA1528ServicoFluxo_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA1528ServicoFluxo_Codigo = A1528ServicoFluxo_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA1528ServicoFluxo_Codigo = cgiGet( sPrefix+"A1528ServicoFluxo_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA1528ServicoFluxo_Codigo) > 0 )
         {
            A1528ServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA1528ServicoFluxo_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1528ServicoFluxo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1528ServicoFluxo_Codigo), 6, 0)));
         }
         else
         {
            A1528ServicoFluxo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A1528ServicoFluxo_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PALH2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSLH2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSLH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A1528ServicoFluxo_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1528ServicoFluxo_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA1528ServicoFluxo_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A1528ServicoFluxo_Codigo_CTRL", StringUtil.RTrim( sCtrlA1528ServicoFluxo_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WELH2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203117285833");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("servicofluxogeneral.js", "?20203117285833");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockservicofluxo_servicosigla_Internalname = sPrefix+"TEXTBLOCKSERVICOFLUXO_SERVICOSIGLA";
         edtServicoFluxo_ServicoSigla_Internalname = sPrefix+"SERVICOFLUXO_SERVICOSIGLA";
         lblTextblockservicofluxo_servicopos_Internalname = sPrefix+"TEXTBLOCKSERVICOFLUXO_SERVICOPOS";
         edtServicoFluxo_ServicoPos_Internalname = sPrefix+"SERVICOFLUXO_SERVICOPOS";
         lblTextblockservicofluxo_ordem_Internalname = sPrefix+"TEXTBLOCKSERVICOFLUXO_ORDEM";
         edtServicoFluxo_Ordem_Internalname = sPrefix+"SERVICOFLUXO_ORDEM";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtServicoFluxo_Codigo_Internalname = sPrefix+"SERVICOFLUXO_CODIGO";
         edtServicoFluxo_ServicoCod_Internalname = sPrefix+"SERVICOFLUXO_SERVICOCOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtServicoFluxo_Ordem_Jsonclick = "";
         edtServicoFluxo_ServicoPos_Jsonclick = "";
         edtServicoFluxo_ServicoSigla_Jsonclick = "";
         edtServicoFluxo_ServicoCod_Jsonclick = "";
         edtServicoFluxo_ServicoCod_Visible = 1;
         edtServicoFluxo_Codigo_Jsonclick = "";
         edtServicoFluxo_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13LH2',iparms:[{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14LH2',iparms:[{av:'A1528ServicoFluxo_Codigo',fld:'SERVICOFLUXO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00LH2_A1528ServicoFluxo_Codigo = new int[1] ;
         H00LH2_A1522ServicoFluxo_ServicoCod = new int[1] ;
         H00LH2_A1532ServicoFluxo_Ordem = new short[1] ;
         H00LH2_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         H00LH2_A1526ServicoFluxo_ServicoPos = new int[1] ;
         H00LH2_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         H00LH2_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         H00LH2_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         A1523ServicoFluxo_ServicoSigla = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockservicofluxo_servicosigla_Jsonclick = "";
         lblTextblockservicofluxo_servicopos_Jsonclick = "";
         lblTextblockservicofluxo_ordem_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA1528ServicoFluxo_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicofluxogeneral__default(),
            new Object[][] {
                new Object[] {
               H00LH2_A1528ServicoFluxo_Codigo, H00LH2_A1522ServicoFluxo_ServicoCod, H00LH2_A1532ServicoFluxo_Ordem, H00LH2_n1532ServicoFluxo_Ordem, H00LH2_A1526ServicoFluxo_ServicoPos, H00LH2_n1526ServicoFluxo_ServicoPos, H00LH2_A1523ServicoFluxo_ServicoSigla, H00LH2_n1523ServicoFluxo_ServicoSigla
               }
            }
         );
         AV14Pgmname = "ServicoFluxoGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "ServicoFluxoGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1532ServicoFluxo_Ordem ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A1528ServicoFluxo_Codigo ;
      private int wcpOA1528ServicoFluxo_Codigo ;
      private int A1526ServicoFluxo_ServicoPos ;
      private int A1522ServicoFluxo_ServicoCod ;
      private int edtServicoFluxo_Codigo_Visible ;
      private int edtServicoFluxo_ServicoCod_Visible ;
      private int AV7ServicoFluxo_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtServicoFluxo_Codigo_Internalname ;
      private String edtServicoFluxo_Codigo_Jsonclick ;
      private String edtServicoFluxo_ServicoCod_Internalname ;
      private String edtServicoFluxo_ServicoCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String scmdbuf ;
      private String A1523ServicoFluxo_ServicoSigla ;
      private String edtServicoFluxo_ServicoSigla_Internalname ;
      private String edtServicoFluxo_ServicoPos_Internalname ;
      private String edtServicoFluxo_Ordem_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockservicofluxo_servicosigla_Internalname ;
      private String lblTextblockservicofluxo_servicosigla_Jsonclick ;
      private String edtServicoFluxo_ServicoSigla_Jsonclick ;
      private String lblTextblockservicofluxo_servicopos_Internalname ;
      private String lblTextblockservicofluxo_servicopos_Jsonclick ;
      private String edtServicoFluxo_ServicoPos_Jsonclick ;
      private String lblTextblockservicofluxo_ordem_Internalname ;
      private String lblTextblockservicofluxo_ordem_Jsonclick ;
      private String edtServicoFluxo_Ordem_Jsonclick ;
      private String sCtrlA1528ServicoFluxo_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1532ServicoFluxo_Ordem ;
      private bool n1526ServicoFluxo_ServicoPos ;
      private bool n1523ServicoFluxo_ServicoSigla ;
      private bool returnInSub ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00LH2_A1528ServicoFluxo_Codigo ;
      private int[] H00LH2_A1522ServicoFluxo_ServicoCod ;
      private short[] H00LH2_A1532ServicoFluxo_Ordem ;
      private bool[] H00LH2_n1532ServicoFluxo_Ordem ;
      private int[] H00LH2_A1526ServicoFluxo_ServicoPos ;
      private bool[] H00LH2_n1526ServicoFluxo_ServicoPos ;
      private String[] H00LH2_A1523ServicoFluxo_ServicoSigla ;
      private bool[] H00LH2_n1523ServicoFluxo_ServicoSigla ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class servicofluxogeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00LH2 ;
          prmH00LH2 = new Object[] {
          new Object[] {"@ServicoFluxo_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00LH2", "SELECT T1.[ServicoFluxo_Codigo], T1.[ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, T1.[ServicoFluxo_Ordem], T1.[ServicoFluxo_ServicoPos], T2.[Servico_Sigla] AS ServicoFluxo_ServicoSigla FROM ([ServicoFluxo] T1 WITH (NOLOCK) INNER JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[ServicoFluxo_ServicoCod]) WHERE T1.[ServicoFluxo_Codigo] = @ServicoFluxo_Codigo ORDER BY T1.[ServicoFluxo_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00LH2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
