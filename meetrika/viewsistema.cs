/*
               File: ViewSistema
        Description: View Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/26/2020 9:12:51.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class viewsistema : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public viewsistema( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public viewsistema( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo ,
                           String aP1_TabCode )
      {
         this.AV9Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV7TabCode = aP1_TabCode;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9Sistema_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Sistema_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV7TabCode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA3F2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START3F2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203269125142");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTABCODE", StringUtil.RTrim( AV7TabCode));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            WebComp_Tabbedview.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE3F2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT3F2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode)) ;
      }

      public override String GetPgmname( )
      {
         return "ViewSistema" ;
      }

      public override String GetPgmdesc( )
      {
         return "View Sistema" ;
      }

      protected void WB3F0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_3F2( true) ;
         }
         else
         {
            wb_table1_2_3F2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_3F2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START3F2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "View Sistema", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP3F0( ) ;
      }

      protected void WS3F2( )
      {
         START3F2( ) ;
         EVT3F2( ) ;
      }

      protected void EVT3F2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E113F2 */
                              E113F2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E123F2 */
                              E123F2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 16 )
                        {
                           OldTabbedview = cgiGet( "W0016");
                           if ( ( StringUtil.Len( OldTabbedview) == 0 ) || ( StringUtil.StrCmp(OldTabbedview, WebComp_Tabbedview_Component) != 0 ) )
                           {
                              WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", OldTabbedview, new Object[] {context} );
                              WebComp_Tabbedview.ComponentInit();
                              WebComp_Tabbedview.Name = "OldTabbedview";
                              WebComp_Tabbedview_Component = OldTabbedview;
                           }
                           if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
                           {
                              WebComp_Tabbedview.componentprocess("W0016", "", sEvt);
                           }
                           WebComp_Tabbedview_Component = OldTabbedview;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE3F2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA3F2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF3F2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF3F2( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  WebComp_Tabbedview.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H003F2 */
            pr_default.execute(0, new Object[] {AV9Sistema_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A127Sistema_Codigo = H003F2_A127Sistema_Codigo[0];
               n127Sistema_Codigo = H003F2_n127Sistema_Codigo[0];
               A416Sistema_Nome = H003F2_A416Sistema_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SISTEMA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
               /* Execute user event: E123F2 */
               E123F2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB3F0( ) ;
         }
      }

      protected void STRUP3F0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113F2 */
         E113F2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A416Sistema_Nome = StringUtil.Upper( cgiGet( edtSistema_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A416Sistema_Nome", A416Sistema_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_SISTEMA_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!"))));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E113F2 */
         E113F2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E113F2( )
      {
         /* Start Routine */
         /* Using cursor H003F3 */
         pr_default.execute(1, new Object[] {AV9Sistema_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A127Sistema_Codigo = H003F3_A127Sistema_Codigo[0];
            n127Sistema_Codigo = H003F3_n127Sistema_Codigo[0];
            A700Sistema_Tecnica = H003F3_A700Sistema_Tecnica[0];
            n700Sistema_Tecnica = H003F3_n700Sistema_Tecnica[0];
            A699Sistema_Tipo = H003F3_A699Sistema_Tipo[0];
            n699Sistema_Tipo = H003F3_n699Sistema_Tipo[0];
            AV17Sistema_Tecnica = A700Sistema_Tecnica;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Sistema_Tecnica", AV17Sistema_Tecnica);
            AV18Sistema_Tipo = A699Sistema_Tipo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Sistema_Tipo", AV18Sistema_Tipo);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         lblWorkwithlink_Link = formatLink("wwsistema.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         AV24GXLvl16 = 0;
         /* Using cursor H003F4 */
         pr_default.execute(2, new Object[] {AV9Sistema_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A127Sistema_Codigo = H003F4_A127Sistema_Codigo[0];
            n127Sistema_Codigo = H003F4_n127Sistema_Codigo[0];
            A128Sistema_Descricao = H003F4_A128Sistema_Descricao[0];
            n128Sistema_Descricao = H003F4_n128Sistema_Descricao[0];
            AV24GXLvl16 = 1;
            Form.Caption = A128Sistema_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = true;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         if ( AV24GXLvl16 == 0 )
         {
            Form.Caption = "Registro n�o encontrado ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = false;
         }
         if ( AV8Exists )
         {
            /* Execute user subroutine: 'LOADTABS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: 'CHECKTABCONDITIONS' */
            S122 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabbedview_Component), StringUtil.Lower( "WWPBaseObjects.WWPTabbedView")) != 0 )
            {
               WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", "wwpbaseobjects.wwptabbedview", new Object[] {context} );
               WebComp_Tabbedview.ComponentInit();
               WebComp_Tabbedview.Name = "WWPBaseObjects.WWPTabbedView";
               WebComp_Tabbedview_Component = "WWPBaseObjects.WWPTabbedView";
            }
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.setjustcreated();
               WebComp_Tabbedview.componentprepare(new Object[] {(String)"W0016",(String)"",(IGxCollection)AV10Tabs,(String)AV7TabCode});
               WebComp_Tabbedview.componentbind(new Object[] {(String)"",(String)""});
            }
         }
         lblWorkwithlink_Link = formatLink("wwsistema.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
      }

      protected void nextLoad( )
      {
      }

      protected void E123F2( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'LOADTABS' Routine */
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "General";
         AV11Tab.gxTpr_Description = "Sistema";
         AV11Tab.gxTpr_Webcomponent = formatLink("sistemageneral.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 0;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Modulo";
         AV11Tab.gxTpr_Description = "M�dulos";
         AV11Tab.gxTpr_Webcomponent = formatLink("sistemamodulowc.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "FuncaoUsuario";
         AV11Tab.gxTpr_Description = "Fun��es de Usu�rio";
         AV11Tab.gxTpr_Webcomponent = formatLink("sistemafuncaousuariowc.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "FuncaoDados";
         AV11Tab.gxTpr_Description = "Fun��es de Dados";
         AV11Tab.gxTpr_Webcomponent = formatLink("sistemafuncaodadoswc.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "FuncaoAPF";
         AV11Tab.gxTpr_Description = "Fun��es de Transa��o";
         AV11Tab.gxTpr_Webcomponent = formatLink("sistemafuncaopapfwc.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Fronteira";
         AV11Tab.gxTpr_Description = "Fronteira";
         AV11Tab.gxTpr_Webcomponent = formatLink("sistemafronteira.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "WC_SistemaContagens";
         AV11Tab.gxTpr_Description = "Projetos";
         AV11Tab.gxTpr_Webcomponent = formatLink("wc_sistemacontagens.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Baseline";
         AV11Tab.gxTpr_Description = "Baseline";
         AV11Tab.gxTpr_Webcomponent = formatLink("sistemabaseline.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Contagem";
         AV11Tab.gxTpr_Description = "Contagens";
         AV11Tab.gxTpr_Webcomponent = formatLink("sistemacontagem.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Evidencias";
         AV11Tab.gxTpr_Description = "Evid�ncias";
         AV11Tab.gxTpr_Webcomponent = formatLink("sistemaevidenciaswc.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Versionamento";
         AV11Tab.gxTpr_Description = "Versionamento";
         AV11Tab.gxTpr_Webcomponent = formatLink("sistemaversaowc.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewsistema.aspx") + "?" + UrlEncode("" +AV9Sistema_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         /* Using cursor H003F5 */
         pr_default.execute(3, new Object[] {AV9Sistema_Codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            A127Sistema_Codigo = H003F5_A127Sistema_Codigo[0];
            n127Sistema_Codigo = H003F5_n127Sistema_Codigo[0];
            W127Sistema_Codigo = A127Sistema_Codigo;
            n127Sistema_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            AV20Count = 0;
            /* Using cursor H003F6 */
            pr_default.execute(4, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               AV20Count = (short)(AV20Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(2)).gxTpr_Description = "M�dulos ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+")";
               pr_default.readNext(4);
            }
            pr_default.close(4);
            AV20Count = 0;
            /* Using cursor H003F7 */
            pr_default.execute(5, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo});
            while ( (pr_default.getStatus(5) != 101) )
            {
               AV20Count = (short)(AV20Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(3)).gxTpr_Description = "Fun��es de Usu�rio ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+")";
               pr_default.readNext(5);
            }
            pr_default.close(5);
            AV20Count = 0;
            /* Using cursor H003F8 */
            pr_default.execute(6, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A370FuncaoDados_SistemaCod = H003F8_A370FuncaoDados_SistemaCod[0];
               AV20Count = (short)(AV20Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(4)).gxTpr_Description = "Fun��es de Dados ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+")";
               pr_default.readNext(6);
            }
            pr_default.close(6);
            AV20Count = 0;
            /* Using cursor H003F9 */
            pr_default.execute(7, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A360FuncaoAPF_SistemaCod = H003F9_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H003F9_n360FuncaoAPF_SistemaCod[0];
               AV20Count = (short)(AV20Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(5)).gxTpr_Description = "Fun��es de Transa��o ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+")";
               pr_default.readNext(7);
            }
            pr_default.close(7);
            AV20Count = 0;
            /* Using cursor H003F10 */
            pr_default.execute(8, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A370FuncaoDados_SistemaCod = H003F10_A370FuncaoDados_SistemaCod[0];
               A373FuncaoDados_Tipo = H003F10_A373FuncaoDados_Tipo[0];
               AV20Count = (short)(AV20Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(6)).gxTpr_Description = "Fronteira ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+")";
               pr_default.readNext(8);
            }
            pr_default.close(8);
            AV20Count = 0;
            /* Using cursor H003F11 */
            pr_default.execute(9);
            while ( (pr_default.getStatus(9) != 101) )
            {
               A648Projeto_Codigo = H003F11_A648Projeto_Codigo[0];
               GXt_int1 = A740Projeto_SistemaCod;
               new prc_projeto_sistemacod(context ).execute( ref  A648Projeto_Codigo, ref  GXt_int1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A648Projeto_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A648Projeto_Codigo), 6, 0)));
               A740Projeto_SistemaCod = GXt_int1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A740Projeto_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A740Projeto_SistemaCod), 6, 0)));
               if ( A740Projeto_SistemaCod == A127Sistema_Codigo )
               {
                  AV20Count = (short)(AV20Count+1);
                  ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(7)).gxTpr_Description = "Projetos ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+")";
               }
               pr_default.readNext(9);
            }
            pr_default.close(9);
            AV20Count = 0;
            /* Using cursor H003F12 */
            pr_default.execute(10, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo});
            while ( (pr_default.getStatus(10) != 101) )
            {
               A735Baseline_ProjetoMelCod = H003F12_A735Baseline_ProjetoMelCod[0];
               n735Baseline_ProjetoMelCod = H003F12_n735Baseline_ProjetoMelCod[0];
               A698ProjetoMelhoria_FnAPFCod = H003F12_A698ProjetoMelhoria_FnAPFCod[0];
               n698ProjetoMelhoria_FnAPFCod = H003F12_n698ProjetoMelhoria_FnAPFCod[0];
               A359FuncaoAPF_ModuloCod = H003F12_A359FuncaoAPF_ModuloCod[0];
               n359FuncaoAPF_ModuloCod = H003F12_n359FuncaoAPF_ModuloCod[0];
               A698ProjetoMelhoria_FnAPFCod = H003F12_A698ProjetoMelhoria_FnAPFCod[0];
               n698ProjetoMelhoria_FnAPFCod = H003F12_n698ProjetoMelhoria_FnAPFCod[0];
               A359FuncaoAPF_ModuloCod = H003F12_A359FuncaoAPF_ModuloCod[0];
               n359FuncaoAPF_ModuloCod = H003F12_n359FuncaoAPF_ModuloCod[0];
               AV20Count = (short)(AV20Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(8)).gxTpr_Description = "Baseline ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+")";
               pr_default.readNext(10);
            }
            pr_default.close(10);
            AV20Count = 0;
            /* Using cursor H003F13 */
            pr_default.execute(11, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo});
            while ( (pr_default.getStatus(11) != 101) )
            {
               A940Contagem_SistemaCod = H003F13_A940Contagem_SistemaCod[0];
               n940Contagem_SistemaCod = H003F13_n940Contagem_SistemaCod[0];
               AV20Count = (short)(AV20Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(9)).gxTpr_Description = "Contagens ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV20Count), 4, 0))+")";
               pr_default.readNext(11);
            }
            pr_default.close(11);
            A127Sistema_Codigo = W127Sistema_Codigo;
            n127Sistema_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A127Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A127Sistema_Codigo), 6, 0)));
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(3);
      }

      protected void S122( )
      {
         /* 'CHECKTABCONDITIONS' Routine */
         /* Using cursor H003F14 */
         pr_default.execute(12, new Object[] {n127Sistema_Codigo, A127Sistema_Codigo, AV9Sistema_Codigo});
         while ( (pr_default.getStatus(12) != 101) )
         {
            A262Contagem_Status = H003F14_A262Contagem_Status[0];
            n262Contagem_Status = H003F14_n262Contagem_Status[0];
            AV12Index = 1;
            while ( AV12Index <= AV10Tabs.Count )
            {
               AV11Tab = ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(AV12Index));
               AV13Increment = 1;
               if ( StringUtil.StrCmp(AV11Tab.gxTpr_Code, "Modulo") == 0 )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV18Sistema_Tipo, "M") != 0 ) ) )
                  {
                     AV10Tabs.RemoveItem(AV12Index);
                     AV13Increment = 0;
                  }
               }
               else if ( StringUtil.StrCmp(AV11Tab.gxTpr_Code, "FuncaoUsuario") == 0 )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV18Sistema_Tipo, "M") != 0 ) ) )
                  {
                     AV10Tabs.RemoveItem(AV12Index);
                     AV13Increment = 0;
                  }
               }
               else if ( StringUtil.StrCmp(AV11Tab.gxTpr_Code, "FuncaoAPF") == 0 )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV17Sistema_Tecnica, "I") != 0 ) ) )
                  {
                     AV10Tabs.RemoveItem(AV12Index);
                     AV13Increment = 0;
                  }
               }
               else if ( StringUtil.StrCmp(AV11Tab.gxTpr_Code, "Contagem") == 0 )
               {
                  if ( ! ( ( StringUtil.StrCmp(A262Contagem_Status, "C") == 0 ) ) )
                  {
                     AV10Tabs.RemoveItem(AV12Index);
                     AV13Increment = 0;
                  }
               }
               AV12Index = (short)(AV12Index+AV13Increment);
            }
            pr_default.readNext(12);
         }
         pr_default.close(12);
      }

      protected void wb_table1_2_3F2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_5_3F2( true) ;
         }
         else
         {
            wb_table2_5_3F2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_3F2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='TableTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblWorkwithlink_Internalname, "Voltar", lblWorkwithlink_Link, "", lblWorkwithlink_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockLink", 0, "", 1, 1, 0, "HLP_ViewSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0016"+"", StringUtil.RTrim( WebComp_Tabbedview_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0016"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0016"+"");
                  }
                  WebComp_Tabbedview.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3F2e( true) ;
         }
         else
         {
            wb_table1_2_3F2e( false) ;
         }
      }

      protected void wb_table2_5_3F2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Sistema :: ", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtSistema_Nome_Internalname, A416Sistema_Nome, StringUtil.RTrim( context.localUtil.Format( A416Sistema_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtSistema_Nome_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 300, "px", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ViewSistema.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3F2e( true) ;
         }
         else
         {
            wb_table2_5_3F2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9Sistema_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Sistema_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9Sistema_Codigo), "ZZZZZ9")));
         AV7TabCode = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3F2( ) ;
         WS3F2( ) ;
         WE3F2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020326912528");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("viewsistema.js", "?2020326912528");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtSistema_Nome_Internalname = "SISTEMA_NOME";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblWorkwithlink_Internalname = "WORKWITHLINK";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtSistema_Nome_Jsonclick = "";
         lblWorkwithlink_Link = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "View Sistema";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV7TabCode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A416Sistema_Nome = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabbedview = "";
         WebComp_Tabbedview_Component = "";
         scmdbuf = "";
         H003F2_A127Sistema_Codigo = new int[1] ;
         H003F2_n127Sistema_Codigo = new bool[] {false} ;
         H003F2_A416Sistema_Nome = new String[] {""} ;
         H003F3_A127Sistema_Codigo = new int[1] ;
         H003F3_n127Sistema_Codigo = new bool[] {false} ;
         H003F3_A700Sistema_Tecnica = new String[] {""} ;
         H003F3_n700Sistema_Tecnica = new bool[] {false} ;
         H003F3_A699Sistema_Tipo = new String[] {""} ;
         H003F3_n699Sistema_Tipo = new bool[] {false} ;
         A700Sistema_Tecnica = "";
         A699Sistema_Tipo = "";
         AV17Sistema_Tecnica = "";
         AV18Sistema_Tipo = "A";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H003F4_A127Sistema_Codigo = new int[1] ;
         H003F4_n127Sistema_Codigo = new bool[] {false} ;
         H003F4_A128Sistema_Descricao = new String[] {""} ;
         H003F4_n128Sistema_Descricao = new bool[] {false} ;
         A128Sistema_Descricao = "";
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_Meetrika", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         H003F5_A127Sistema_Codigo = new int[1] ;
         H003F5_n127Sistema_Codigo = new bool[] {false} ;
         H003F6_A146Modulo_Codigo = new int[1] ;
         H003F6_A127Sistema_Codigo = new int[1] ;
         H003F6_n127Sistema_Codigo = new bool[] {false} ;
         H003F7_A161FuncaoUsuario_Codigo = new int[1] ;
         H003F7_A127Sistema_Codigo = new int[1] ;
         H003F7_n127Sistema_Codigo = new bool[] {false} ;
         H003F8_A368FuncaoDados_Codigo = new int[1] ;
         H003F8_A370FuncaoDados_SistemaCod = new int[1] ;
         H003F9_A165FuncaoAPF_Codigo = new int[1] ;
         H003F9_A360FuncaoAPF_SistemaCod = new int[1] ;
         H003F9_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H003F10_A368FuncaoDados_Codigo = new int[1] ;
         H003F10_A370FuncaoDados_SistemaCod = new int[1] ;
         H003F10_A373FuncaoDados_Tipo = new String[] {""} ;
         A373FuncaoDados_Tipo = "";
         H003F11_A648Projeto_Codigo = new int[1] ;
         H003F12_A722Baseline_Codigo = new int[1] ;
         H003F12_A735Baseline_ProjetoMelCod = new int[1] ;
         H003F12_n735Baseline_ProjetoMelCod = new bool[] {false} ;
         H003F12_A698ProjetoMelhoria_FnAPFCod = new int[1] ;
         H003F12_n698ProjetoMelhoria_FnAPFCod = new bool[] {false} ;
         H003F12_A359FuncaoAPF_ModuloCod = new int[1] ;
         H003F12_n359FuncaoAPF_ModuloCod = new bool[] {false} ;
         H003F12_A127Sistema_Codigo = new int[1] ;
         H003F12_n127Sistema_Codigo = new bool[] {false} ;
         H003F13_A192Contagem_Codigo = new int[1] ;
         H003F13_A940Contagem_SistemaCod = new int[1] ;
         H003F13_n940Contagem_SistemaCod = new bool[] {false} ;
         H003F14_A192Contagem_Codigo = new int[1] ;
         H003F14_A262Contagem_Status = new String[] {""} ;
         H003F14_n262Contagem_Status = new bool[] {false} ;
         A262Contagem_Status = "";
         sStyleString = "";
         lblWorkwithlink_Jsonclick = "";
         lblViewtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.viewsistema__default(),
            new Object[][] {
                new Object[] {
               H003F2_A127Sistema_Codigo, H003F2_A416Sistema_Nome
               }
               , new Object[] {
               H003F3_A127Sistema_Codigo, H003F3_A700Sistema_Tecnica, H003F3_n700Sistema_Tecnica, H003F3_A699Sistema_Tipo, H003F3_n699Sistema_Tipo
               }
               , new Object[] {
               H003F4_A127Sistema_Codigo, H003F4_A128Sistema_Descricao, H003F4_n128Sistema_Descricao
               }
               , new Object[] {
               H003F5_A127Sistema_Codigo
               }
               , new Object[] {
               H003F6_A146Modulo_Codigo, H003F6_A127Sistema_Codigo
               }
               , new Object[] {
               H003F7_A161FuncaoUsuario_Codigo, H003F7_A127Sistema_Codigo
               }
               , new Object[] {
               H003F8_A368FuncaoDados_Codigo, H003F8_A370FuncaoDados_SistemaCod
               }
               , new Object[] {
               H003F9_A165FuncaoAPF_Codigo, H003F9_A360FuncaoAPF_SistemaCod, H003F9_n360FuncaoAPF_SistemaCod
               }
               , new Object[] {
               H003F10_A368FuncaoDados_Codigo, H003F10_A370FuncaoDados_SistemaCod, H003F10_A373FuncaoDados_Tipo
               }
               , new Object[] {
               H003F11_A648Projeto_Codigo
               }
               , new Object[] {
               H003F12_A722Baseline_Codigo, H003F12_A735Baseline_ProjetoMelCod, H003F12_n735Baseline_ProjetoMelCod, H003F12_A698ProjetoMelhoria_FnAPFCod, H003F12_n698ProjetoMelhoria_FnAPFCod, H003F12_A359FuncaoAPF_ModuloCod, H003F12_n359FuncaoAPF_ModuloCod, H003F12_A127Sistema_Codigo, H003F12_n127Sistema_Codigo
               }
               , new Object[] {
               H003F13_A192Contagem_Codigo, H003F13_A940Contagem_SistemaCod, H003F13_n940Contagem_SistemaCod
               }
               , new Object[] {
               H003F14_A192Contagem_Codigo, H003F14_A262Contagem_Status, H003F14_n262Contagem_Status
               }
            }
         );
         WebComp_Tabbedview = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV24GXLvl16 ;
      private short AV20Count ;
      private short AV12Index ;
      private short AV13Increment ;
      private short nGXWrapped ;
      private int AV9Sistema_Codigo ;
      private int wcpOAV9Sistema_Codigo ;
      private int A127Sistema_Codigo ;
      private int W127Sistema_Codigo ;
      private int A370FuncaoDados_SistemaCod ;
      private int A360FuncaoAPF_SistemaCod ;
      private int A648Projeto_Codigo ;
      private int A740Projeto_SistemaCod ;
      private int GXt_int1 ;
      private int A735Baseline_ProjetoMelCod ;
      private int A698ProjetoMelhoria_FnAPFCod ;
      private int A359FuncaoAPF_ModuloCod ;
      private int A940Contagem_SistemaCod ;
      private int idxLst ;
      private String AV7TabCode ;
      private String wcpOAV7TabCode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabbedview ;
      private String WebComp_Tabbedview_Component ;
      private String scmdbuf ;
      private String edtSistema_Nome_Internalname ;
      private String A700Sistema_Tecnica ;
      private String A699Sistema_Tipo ;
      private String AV17Sistema_Tecnica ;
      private String AV18Sistema_Tipo ;
      private String lblWorkwithlink_Link ;
      private String lblWorkwithlink_Internalname ;
      private String A373FuncaoDados_Tipo ;
      private String A262Contagem_Status ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblWorkwithlink_Jsonclick ;
      private String tblTablemergedviewtitle_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String edtSistema_Nome_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n127Sistema_Codigo ;
      private bool returnInSub ;
      private bool n700Sistema_Tecnica ;
      private bool n699Sistema_Tipo ;
      private bool n128Sistema_Descricao ;
      private bool AV8Exists ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool n735Baseline_ProjetoMelCod ;
      private bool n698ProjetoMelhoria_FnAPFCod ;
      private bool n359FuncaoAPF_ModuloCod ;
      private bool n940Contagem_SistemaCod ;
      private bool n262Contagem_Status ;
      private String A128Sistema_Descricao ;
      private String A416Sistema_Nome ;
      private GXWebComponent WebComp_Tabbedview ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H003F2_A127Sistema_Codigo ;
      private bool[] H003F2_n127Sistema_Codigo ;
      private String[] H003F2_A416Sistema_Nome ;
      private int[] H003F3_A127Sistema_Codigo ;
      private bool[] H003F3_n127Sistema_Codigo ;
      private String[] H003F3_A700Sistema_Tecnica ;
      private bool[] H003F3_n700Sistema_Tecnica ;
      private String[] H003F3_A699Sistema_Tipo ;
      private bool[] H003F3_n699Sistema_Tipo ;
      private int[] H003F4_A127Sistema_Codigo ;
      private bool[] H003F4_n127Sistema_Codigo ;
      private String[] H003F4_A128Sistema_Descricao ;
      private bool[] H003F4_n128Sistema_Descricao ;
      private int[] H003F5_A127Sistema_Codigo ;
      private bool[] H003F5_n127Sistema_Codigo ;
      private int[] H003F6_A146Modulo_Codigo ;
      private int[] H003F6_A127Sistema_Codigo ;
      private bool[] H003F6_n127Sistema_Codigo ;
      private int[] H003F7_A161FuncaoUsuario_Codigo ;
      private int[] H003F7_A127Sistema_Codigo ;
      private bool[] H003F7_n127Sistema_Codigo ;
      private int[] H003F8_A368FuncaoDados_Codigo ;
      private int[] H003F8_A370FuncaoDados_SistemaCod ;
      private int[] H003F9_A165FuncaoAPF_Codigo ;
      private int[] H003F9_A360FuncaoAPF_SistemaCod ;
      private bool[] H003F9_n360FuncaoAPF_SistemaCod ;
      private int[] H003F10_A368FuncaoDados_Codigo ;
      private int[] H003F10_A370FuncaoDados_SistemaCod ;
      private String[] H003F10_A373FuncaoDados_Tipo ;
      private int[] H003F11_A648Projeto_Codigo ;
      private int[] H003F12_A722Baseline_Codigo ;
      private int[] H003F12_A735Baseline_ProjetoMelCod ;
      private bool[] H003F12_n735Baseline_ProjetoMelCod ;
      private int[] H003F12_A698ProjetoMelhoria_FnAPFCod ;
      private bool[] H003F12_n698ProjetoMelhoria_FnAPFCod ;
      private int[] H003F12_A359FuncaoAPF_ModuloCod ;
      private bool[] H003F12_n359FuncaoAPF_ModuloCod ;
      private int[] H003F12_A127Sistema_Codigo ;
      private bool[] H003F12_n127Sistema_Codigo ;
      private int[] H003F13_A192Contagem_Codigo ;
      private int[] H003F13_A940Contagem_SistemaCod ;
      private bool[] H003F13_n940Contagem_SistemaCod ;
      private int[] H003F14_A192Contagem_Codigo ;
      private String[] H003F14_A262Contagem_Status ;
      private bool[] H003F14_n262Contagem_Status ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem ))]
      private IGxCollection AV10Tabs ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem AV11Tab ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class viewsistema__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003F2 ;
          prmH003F2 = new Object[] {
          new Object[] {"@AV9Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003F3 ;
          prmH003F3 = new Object[] {
          new Object[] {"@AV9Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003F4 ;
          prmH003F4 = new Object[] {
          new Object[] {"@AV9Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003F5 ;
          prmH003F5 = new Object[] {
          new Object[] {"@AV9Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003F6 ;
          prmH003F6 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003F7 ;
          prmH003F7 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003F8 ;
          prmH003F8 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003F9 ;
          prmH003F9 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003F10 ;
          prmH003F10 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003F11 ;
          prmH003F11 = new Object[] {
          } ;
          Object[] prmH003F12 ;
          prmH003F12 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003F13 ;
          prmH003F13 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH003F14 ;
          prmH003F14 = new Object[] {
          new Object[] {"@Sistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003F2", "SELECT [Sistema_Codigo], [Sistema_Nome] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV9Sistema_Codigo ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F2,1,0,true,true )
             ,new CursorDef("H003F3", "SELECT TOP 1 [Sistema_Codigo], [Sistema_Tecnica], [Sistema_Tipo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV9Sistema_Codigo ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F3,1,0,false,true )
             ,new CursorDef("H003F4", "SELECT [Sistema_Codigo], [Sistema_Descricao] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV9Sistema_Codigo ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F4,1,0,false,true )
             ,new CursorDef("H003F5", "SELECT [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV9Sistema_Codigo ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F5,1,0,true,true )
             ,new CursorDef("H003F6", "SELECT [Modulo_Codigo], [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F6,100,0,false,false )
             ,new CursorDef("H003F7", "SELECT [FuncaoUsuario_Codigo], [Sistema_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [Sistema_Codigo] = @Sistema_Codigo ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F7,100,0,false,false )
             ,new CursorDef("H003F8", "SELECT [FuncaoDados_Codigo], [FuncaoDados_SistemaCod] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_SistemaCod] = @Sistema_Codigo ORDER BY [FuncaoDados_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F8,100,0,false,false )
             ,new CursorDef("H003F9", "SELECT [FuncaoAPF_Codigo], [FuncaoAPF_SistemaCod] FROM [FuncoesAPF] WITH (NOLOCK) WHERE [FuncaoAPF_SistemaCod] = @Sistema_Codigo ORDER BY [FuncaoAPF_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F9,100,0,false,false )
             ,new CursorDef("H003F10", "SELECT [FuncaoDados_Codigo], [FuncaoDados_SistemaCod], [FuncaoDados_Tipo] FROM [FuncaoDados] WITH (NOLOCK) WHERE ([FuncaoDados_SistemaCod] = @Sistema_Codigo) AND ([FuncaoDados_Tipo] = 'ALI') ORDER BY [FuncaoDados_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F10,100,0,false,false )
             ,new CursorDef("H003F11", "SELECT [Projeto_Codigo] FROM [Projeto] WITH (NOLOCK) ORDER BY [Projeto_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F11,100,0,true,false )
             ,new CursorDef("H003F12", "SELECT T1.[Baseline_Codigo], T1.[Baseline_ProjetoMelCod] AS Baseline_ProjetoMelCod, T2.[ProjetoMelhoria_FnAPFCod] AS ProjetoMelhoria_FnAPFCod, T3.[FuncaoAPF_ModuloCod] AS FuncaoAPF_ModuloCod, T4.[Sistema_Codigo] FROM ((([Baseline] T1 WITH (NOLOCK) LEFT JOIN [ProjetoMelhoria] T2 WITH (NOLOCK) ON T2.[ProjetoMelhoria_Codigo] = T1.[Baseline_ProjetoMelCod]) LEFT JOIN [FuncoesAPF] T3 WITH (NOLOCK) ON T3.[FuncaoAPF_Codigo] = T2.[ProjetoMelhoria_FnAPFCod]) LEFT JOIN [Modulo] T4 WITH (NOLOCK) ON T4.[Modulo_Codigo] = T3.[FuncaoAPF_ModuloCod]) WHERE T4.[Sistema_Codigo] = @Sistema_Codigo ORDER BY T1.[Baseline_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F12,100,0,false,false )
             ,new CursorDef("H003F13", "SELECT [Contagem_Codigo], [Contagem_SistemaCod] FROM [Contagem] WITH (NOLOCK) WHERE [Contagem_SistemaCod] = @Sistema_Codigo ORDER BY [Contagem_SistemaCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F13,100,0,false,false )
             ,new CursorDef("H003F14", "SELECT [Contagem_Codigo], [Contagem_Status] FROM [Contagem] WITH (NOLOCK) WHERE @Sistema_Codigo = @AV9Sistema_Codigo ORDER BY [Contagem_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003F14,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 3) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
