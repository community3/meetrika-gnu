/*
               File: WP_PrazoExecucao
        Description: Prazo de Execu��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/12/2020 21:24:3.15
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_prazoexecucao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_prazoexecucao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_prazoexecucao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           long aP1_UserId )
      {
         this.AV28ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV7UserId = aP1_UserId;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV28ContagemResultado_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28ContagemResultado_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV28ContagemResultado_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV7UserId = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UserId), 18, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UserId), "ZZZZZZZZZZZZZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAOB2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTOB2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20203122124444");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_prazoexecucao.aspx") + "?" + UrlEncode("" +AV28ContagemResultado_Codigo) + "," + UrlEncode("" +AV7UserId)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV26Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV26Codigos);
         }
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7UserId), 18, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTAGEMRESULTADO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPRAZOATUAL", GetSecureSignedToken( "", context.localUtil.Format( AV27PrazoAtual, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV28ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UserId), "ZZZZZZZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV28ContagemResultado_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UserId), "ZZZZZZZZZZZZZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_PrazoExecucao";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV27PrazoAtual, "99/99/99 99:99");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_prazoexecucao:[SendSecurityCheck value for]"+"PrazoAtual:"+context.localUtil.Format( AV27PrazoAtual, "99/99/99 99:99"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEOB2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTOB2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_prazoexecucao.aspx") + "?" + UrlEncode("" +AV28ContagemResultado_Codigo) + "," + UrlEncode("" +AV7UserId) ;
      }

      public override String GetPgmname( )
      {
         return "WP_PrazoExecucao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Prazo de Execu��o" ;
      }

      protected void WBOB0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_OB2( true) ;
         }
         else
         {
            wb_table1_2_OB2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_OB2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_PrazoExecucao.htm");
         }
         wbLoad = true;
      }

      protected void STARTOB2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Prazo de Execu��o", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPOB0( ) ;
      }

      protected void WSOB2( )
      {
         STARTOB2( ) ;
         EVTOB2( ) ;
      }

      protected void EVTOB2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11OB2 */
                              E11OB2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E12OB2 */
                                    E12OB2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13OB2 */
                              E13OB2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEOB2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAOB2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavPrazoatual_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFOB2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavPrazoatual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrazoatual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazoatual_Enabled), 5, 0)));
      }

      protected void RFOB2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E13OB2 */
            E13OB2 ();
            WBOB0( ) ;
         }
      }

      protected void STRUPOB0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavPrazoatual_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrazoatual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrazoatual_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11OB2 */
         E11OB2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( context.localUtil.VCDateTime( cgiGet( edtavPrazoatual_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Prazo Atual"}), 1, "vPRAZOATUAL");
               GX_FocusControl = edtavPrazoatual_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV27PrazoAtual = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27PrazoAtual", context.localUtil.TToC( AV27PrazoAtual, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPRAZOATUAL", GetSecureSignedToken( "", context.localUtil.Format( AV27PrazoAtual, "99/99/99 99:99")));
            }
            else
            {
               AV27PrazoAtual = context.localUtil.CToT( cgiGet( edtavPrazoatual_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27PrazoAtual", context.localUtil.TToC( AV27PrazoAtual, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPRAZOATUAL", GetSecureSignedToken( "", context.localUtil.Format( AV27PrazoAtual, "99/99/99 99:99")));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPrazo_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Prazo"}), 1, "vPRAZO");
               GX_FocusControl = edtavPrazo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV5Prazo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Prazo", context.localUtil.TToC( AV5Prazo, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV5Prazo = context.localUtil.CToT( cgiGet( edtavPrazo_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Prazo", context.localUtil.TToC( AV5Prazo, 8, 5, 0, 3, "/", ":", " "));
            }
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_PrazoExecucao";
            AV27PrazoAtual = context.localUtil.CToT( cgiGet( edtavPrazoatual_Internalname), 0);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27PrazoAtual", context.localUtil.TToC( AV27PrazoAtual, 8, 5, 0, 3, "/", ":", " "));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPRAZOATUAL", GetSecureSignedToken( "", context.localUtil.Format( AV27PrazoAtual, "99/99/99 99:99")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( AV27PrazoAtual, "99/99/99 99:99");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_prazoexecucao:[SecurityCheckFailed value for]"+"PrazoAtual:"+context.localUtil.Format( AV27PrazoAtual, "99/99/99 99:99"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11OB2 */
         E11OB2 ();
         if (returnInSub) return;
      }

      protected void E11OB2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         bttBtnenter_Visible = (AV8WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         AV26Codigos.FromXml(AV30WebSession.Get("Codigos"), "Collection");
         AV30WebSession.Remove("Codigos");
         if ( (0==AV26Codigos.IndexOf(AV28ContagemResultado_Codigo)) )
         {
            AV26Codigos.Add(AV28ContagemResultado_Codigo, 0);
         }
         AV27PrazoAtual = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27PrazoAtual", context.localUtil.TToC( AV27PrazoAtual, 8, 5, 0, 3, "/", ":", " "));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPRAZOATUAL", GetSecureSignedToken( "", context.localUtil.Format( AV27PrazoAtual, "99/99/99 99:99")));
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV26Codigos },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor H00OB2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = H00OB2_A456ContagemResultado_Codigo[0];
            A1351ContagemResultado_DataPrevista = H00OB2_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = H00OB2_n1351ContagemResultado_DataPrevista[0];
            A484ContagemResultado_StatusDmn = H00OB2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00OB2_n484ContagemResultado_StatusDmn[0];
            A508ContagemResultado_Owner = H00OB2_A508ContagemResultado_Owner[0];
            if ( (DateTime.MinValue==AV27PrazoAtual) || ( A1351ContagemResultado_DataPrevista < AV27PrazoAtual ) )
            {
               AV27PrazoAtual = A1351ContagemResultado_DataPrevista;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27PrazoAtual", context.localUtil.TToC( AV27PrazoAtual, 8, 5, 0, 3, "/", ":", " "));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPRAZOATUAL", GetSecureSignedToken( "", context.localUtil.Format( AV27PrazoAtual, "99/99/99 99:99")));
            }
            if ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "D") == 0 )
            {
               AV31ExistemReteronadas = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31ExistemReteronadas", AV31ExistemReteronadas);
            }
            if ( A508ContagemResultado_Owner != AV8WWPContext.gxTpr_Userid )
            {
               AV32OutroOwner = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32OutroOwner", AV32OutroOwner);
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV31ExistemReteronadas )
         {
            bttBtnenter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
            GX_msglist.addItem("N�o � possivel alterar o prazo de demandas Rejeitadas!");
         }
         if ( AV32OutroOwner )
         {
            bttBtnenter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
            GX_msglist.addItem("N�o � possivel alterar o prazo de demandas solicitadas por outros!");
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E12OB2 */
         E12OB2 ();
         if (returnInSub) return;
      }

      protected void E12OB2( )
      {
         /* Enter Routine */
         if ( (DateTime.MinValue==AV5Prazo) )
         {
            GX_msglist.addItem("Informe o novo prazo!");
         }
         else if ( AV5Prazo < DateTimeUtil.ServerNow( context, "DEFAULT") )
         {
            GX_msglist.addItem("Prazo vencido!");
         }
         else if ( AV5Prazo < AV27PrazoAtual )
         {
            GX_msglist.addItem("Prazo menor ao prazo m�nimo!");
         }
         else
         {
            AV36GXV1 = 1;
            while ( AV36GXV1 <= AV26Codigos.Count )
            {
               AV6Codigo = (int)(AV26Codigos.GetNumeric(AV36GXV1));
               AV13OS.Load(AV6Codigo);
               if ( StringUtil.StrCmp(AV13OS.gxTpr_Contagemresultado_statusdmn, "E") != 0 )
               {
                  AV13OS.gxTpr_Contagemresultado_dataentrega = AV5Prazo;
                  GXt_dtime1 = DateTimeUtil.ResetDate(AV5Prazo);
                  AV13OS.gxTpr_Contagemresultado_horaentrega = GXt_dtime1;
               }
               AV13OS.gxTpr_Contagemresultado_dataprevista = AV5Prazo;
               GXt_int2 = 0;
               new prc_diasuteisentre(context ).execute(  AV13OS.gxTpr_Contagemresultado_datadmn,  AV5Prazo,  AV13OS.gxTpr_Contagemresultado_prztpdias, out  GXt_int2) ;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Prazo", context.localUtil.TToC( AV5Prazo, 8, 5, 0, 3, "/", ":", " "));
               AV13OS.gxTpr_Contagemresultado_prazomaisdias = (short)(GXt_int2-AV13OS.gxTpr_Contagemresultado_prazoinicialdias);
               AV13OS.Save();
               if ( AV13OS.Success() )
               {
                  new prc_updcicloexc(context ).execute(  AV6Codigo,  AV5Prazo) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Prazo", context.localUtil.TToC( AV5Prazo, 8, 5, 0, 3, "/", ":", " "));
                  new prc_inslogresponsavel(context ).execute( ref  AV6Codigo,  0,  "U",  "D",  (int)(AV7UserId),  0,  AV13OS.gxTpr_Contagemresultado_statusdmn,  AV13OS.gxTpr_Contagemresultado_statusdmn,  "Prazo alterado.",  AV5Prazo,  true) ;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UserId), 18, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UserId), "ZZZZZZZZZZZZZZZZZ9")));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5Prazo", context.localUtil.TToC( AV5Prazo, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  context.RollbackDataStores( "WP_PrazoExecucao");
                  AV22Messages = AV13OS.GetMessages();
                  AV37GXV2 = 1;
                  while ( AV37GXV2 <= AV22Messages.Count )
                  {
                     AV23oneMessage = ((SdtMessages_Message)AV22Messages.Item(AV37GXV2));
                     GX_msglist.addItem(AV23oneMessage.gxTpr_Description);
                     AV37GXV2 = (int)(AV37GXV2+1);
                  }
                  GX_FocusControl = edtavPrazo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  context.DoAjaxSetFocus(GX_FocusControl);
               }
               AV36GXV1 = (int)(AV36GXV1+1);
            }
            context.CommitDataStores( "WP_PrazoExecucao");
            /* Execute user subroutine: 'FECHAR' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void S112( )
      {
         /* 'FECHAR' Routine */
         lblTbjava_Caption = "<script language=\"javascript\" type=\"javascript\">parent.location.replace(parent.location.href); </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void nextLoad( )
      {
      }

      protected void E13OB2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_OB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "center", "", 1, 10, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Prazo m�nimo:", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_PrazoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPrazoatual_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPrazoatual_Internalname, context.localUtil.TToC( AV27PrazoAtual, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV27PrazoAtual, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,10);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPrazoatual_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPrazoatual_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_PrazoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavPrazoatual_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavPrazoatual_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_PrazoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Novo prazo:", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_PrazoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPrazo_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPrazo_Internalname, context.localUtil.TToC( AV5Prazo, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV5Prazo, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,15);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPrazo_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_PrazoExecucao.htm");
            GxWebStd.gx_bitmap( context, edtavPrazo_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WP_PrazoExecucao.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<td data-align=\"center\" colspan=\"2\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_PrazoExecucao.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCancel_Internalname, "", "Fechar", bttCancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_PrazoExecucao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_OB2e( true) ;
         }
         else
         {
            wb_table1_2_OB2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV28ContagemResultado_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28ContagemResultado_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTAGEMRESULTADO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV28ContagemResultado_Codigo), "ZZZZZ9")));
         AV7UserId = Convert.ToInt64(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7UserId), 18, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vUSERID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7UserId), "ZZZZZZZZZZZZZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAOB2( ) ;
         WSOB2( ) ;
         WEOB2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203122124462");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_prazoexecucao.js", "?20203122124462");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavPrazoatual_Internalname = "vPRAZOATUAL";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         edtavPrazo_Internalname = "vPRAZO";
         bttBtnenter_Internalname = "BTNENTER";
         bttCancel_Internalname = "CANCEL";
         tblTable1_Internalname = "TABLE1";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         bttBtnenter_Visible = 1;
         edtavPrazo_Jsonclick = "";
         edtavPrazoatual_Jsonclick = "";
         edtavPrazoatual_Enabled = 1;
         lblTbjava_Caption = "Script";
         lblTbjava_Visible = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Prazo de Execu��o";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12OB2',iparms:[{av:'AV5Prazo',fld:'vPRAZO',pic:'99/99/99 99:99',nv:''},{av:'AV27PrazoAtual',fld:'vPRAZOATUAL',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'AV26Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV7UserId',fld:'vUSERID',pic:'ZZZZZZZZZZZZZZZZZ9',hsh:true,nv:0}],oparms:[{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV26Codigos = new GxSimpleCollection();
         AV27PrazoAtual = (DateTime)(DateTime.MinValue);
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV5Prazo = (DateTime)(DateTime.MinValue);
         hsh = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV30WebSession = context.GetSession();
         scmdbuf = "";
         H00OB2_A456ContagemResultado_Codigo = new int[1] ;
         H00OB2_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         H00OB2_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         H00OB2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00OB2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00OB2_A508ContagemResultado_Owner = new int[1] ;
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         A484ContagemResultado_StatusDmn = "";
         AV13OS = new SdtContagemResultado(context);
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         AV22Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV23oneMessage = new SdtMessages_Message(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         lblTextblock1_Jsonclick = "";
         TempTags = "";
         lblTextblock2_Jsonclick = "";
         bttBtnenter_Jsonclick = "";
         bttCancel_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_prazoexecucao__default(),
            new Object[][] {
                new Object[] {
               H00OB2_A456ContagemResultado_Codigo, H00OB2_A1351ContagemResultado_DataPrevista, H00OB2_n1351ContagemResultado_DataPrevista, H00OB2_A484ContagemResultado_StatusDmn, H00OB2_n484ContagemResultado_StatusDmn, H00OB2_A508ContagemResultado_Owner
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavPrazoatual_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short GXt_int2 ;
      private short nGXWrapped ;
      private int AV28ContagemResultado_Codigo ;
      private int wcpOAV28ContagemResultado_Codigo ;
      private int lblTbjava_Visible ;
      private int edtavPrazoatual_Enabled ;
      private int bttBtnenter_Visible ;
      private int A456ContagemResultado_Codigo ;
      private int A508ContagemResultado_Owner ;
      private int AV36GXV1 ;
      private int AV6Codigo ;
      private int AV37GXV2 ;
      private int idxLst ;
      private long AV7UserId ;
      private long wcpOAV7UserId ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavPrazoatual_Internalname ;
      private String edtavPrazo_Internalname ;
      private String hsh ;
      private String bttBtnenter_Internalname ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String TempTags ;
      private String edtavPrazoatual_Jsonclick ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String edtavPrazo_Jsonclick ;
      private String bttBtnenter_Jsonclick ;
      private String bttCancel_Internalname ;
      private String bttCancel_Jsonclick ;
      private DateTime AV27PrazoAtual ;
      private DateTime AV5Prazo ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime GXt_dtime1 ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool AV31ExistemReteronadas ;
      private bool AV32OutroOwner ;
      private IGxSession AV30WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00OB2_A456ContagemResultado_Codigo ;
      private DateTime[] H00OB2_A1351ContagemResultado_DataPrevista ;
      private bool[] H00OB2_n1351ContagemResultado_DataPrevista ;
      private String[] H00OB2_A484ContagemResultado_StatusDmn ;
      private bool[] H00OB2_n484ContagemResultado_StatusDmn ;
      private int[] H00OB2_A508ContagemResultado_Owner ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV26Codigos ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV22Messages ;
      private GXWebForm Form ;
      private SdtMessages_Message AV23oneMessage ;
      private SdtContagemResultado AV13OS ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
   }

   public class wp_prazoexecucao__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00OB2( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV26Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_Codigo], [ContagemResultado_DataPrevista], [ContagemResultado_StatusDmn], [ContagemResultado_Owner] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV26Codigos, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_Codigo]";
         GXv_Object3[0] = scmdbuf;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00OB2(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00OB2 ;
          prmH00OB2 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00OB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OB2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
       }
    }

 }

}
