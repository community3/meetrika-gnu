/*
               File: WP_AssociarCatalogoSutentacaoArtefatos
        Description:
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 3/1/2020 19:5:3.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associarcatalogosutentacaoartefatos : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associarcatalogosutentacaoartefatos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associarcatalogosutentacaoartefatos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_CatalogoSutentacao_Codigo )
      {
         this.AV11CatalogoSutentacao_Codigo = aP0_CatalogoSutentacao_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         lstavNotassociatedrecords = new GXListbox();
         lstavAssociatedrecords = new GXListbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV11CatalogoSutentacao_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11CatalogoSutentacao_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCATALOGOSUTENTACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11CatalogoSutentacao_Codigo), "ZZZZZ9")));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAN22( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WSN22( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEN22( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202031195343");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associarcatalogosutentacaoartefatos.aspx") + "?" + UrlEncode("" +AV11CatalogoSutentacao_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDKEYLIST", AV7AddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDKEYLIST", AV7AddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDDSCLIST", AV5AddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDDSCLIST", AV5AddedDscList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDKEYLIST", AV25NotAddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDKEYLIST", AV25NotAddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDDSCLIST", AV23NotAddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDDSCLIST", AV23NotAddedDscList);
         }
         GxWebStd.gx_hidden_field( context, "CATALOGOSUTENTACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1750CatalogoSutentacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCATALOGOSUTENTACAO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11CatalogoSutentacao_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ARTEFATOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1749Artefatos_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATOSERVICOSARTEFATOS", AV13ContratoServicosArtefatos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATOSERVICOSARTEFATOS", AV13ContratoServicosArtefatos);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vCATALOGOSUTENTACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11CatalogoSutentacao_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCATALOGOSUTENTACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11CatalogoSutentacao_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormN22( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociarCatalogoSutentacaoArtefatos" ;
      }

      public override String GetPgmdesc( )
      {
         return "" ;
      }

      protected void WBN20( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_N22( true) ;
         }
         else
         {
            wb_table1_2_N22( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_N22e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddedkeylistxml_Internalname, AV8AddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", 0, edtavAddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddedkeylistxml_Internalname, AV26NotAddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", 0, edtavNotaddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddeddsclistxml_Internalname, AV6AddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", 0, edtavAddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddeddsclistxml_Internalname, AV24NotAddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", 0, edtavNotaddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
         }
         wbLoad = true;
      }

      protected void STARTN22( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPN20( ) ;
      }

      protected void WSN22( )
      {
         STARTN22( ) ;
         EVTN22( ) ;
      }

      protected void EVTN22( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11N22 */
                           E11N22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12N22 */
                           E12N22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13N22 */
                                 E13N22 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14N22 */
                           E14N22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15N22 */
                           E15N22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16N22 */
                           E16N22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17N22 */
                           E17N22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18N22 */
                           E18N22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19N22 */
                           E19N22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20N22 */
                           E20N22 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18N22 */
                           E18N22 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19N22 */
                           E19N22 ();
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEN22( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormN22( ) ;
            }
         }
      }

      protected void PAN22( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            lstavNotassociatedrecords.Name = "vNOTASSOCIATEDRECORDS";
            lstavNotassociatedrecords.WebTags = "";
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Selecione", 0);
            if ( lstavNotassociatedrecords.ItemCount > 0 )
            {
               AV27NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0)));
            }
            lstavAssociatedrecords.Name = "vASSOCIATEDRECORDS";
            lstavAssociatedrecords.WebTags = "";
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Selecione", 0);
            if ( lstavAssociatedrecords.ItemCount > 0 )
            {
               AV10AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = lstavNotassociatedrecords_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( lstavNotassociatedrecords.ItemCount > 0 )
         {
            AV27NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0)));
         }
         if ( lstavAssociatedrecords.ItemCount > 0 )
         {
            AV10AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFN22( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFN22( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12N22 */
         E12N22 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E20N22 */
            E20N22 ();
            WBN20( ) ;
         }
      }

      protected void STRUPN20( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11N22 */
         E11N22 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            lstavNotassociatedrecords.CurrentValue = cgiGet( lstavNotassociatedrecords_Internalname);
            AV27NotAssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavNotassociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0)));
            lstavAssociatedrecords.CurrentValue = cgiGet( lstavAssociatedrecords_Internalname);
            AV10AssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavAssociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0)));
            AV8AddedKeyListXml = cgiGet( edtavAddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV26NotAddedKeyListXml = cgiGet( edtavNotaddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26NotAddedKeyListXml", AV26NotAddedKeyListXml);
            AV6AddedDscListXml = cgiGet( edtavAddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
            AV24NotAddedDscListXml = cgiGet( edtavNotaddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAddedDscListXml", AV24NotAddedDscListXml);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11N22 */
         E11N22 ();
         if (returnInSub) return;
      }

      protected void E11N22( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV30WWPContext) ;
         if ( StringUtil.StrCmp(AV18HTTPRequest.Method, "GET") == 0 )
         {
            AV34GXLvl4 = 0;
            /* Using cursor H00N22 */
            pr_default.execute(0, new Object[] {AV11CatalogoSutentacao_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1750CatalogoSutentacao_Codigo = H00N22_A1750CatalogoSutentacao_Codigo[0];
               AV34GXLvl4 = 1;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV34GXLvl4 == 0 )
            {
               GX_msglist.addItem("Registro n�o encontrado.");
            }
            /* Using cursor H00N23 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A1749Artefatos_Codigo = H00N23_A1749Artefatos_Codigo[0];
               A1751Artefatos_Descricao = H00N23_A1751Artefatos_Descricao[0];
               AV9Artefatos_Codigo = A1749Artefatos_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
               AV17Exist = false;
               /* Using cursor H00N24 */
               pr_default.execute(2, new Object[] {AV11CatalogoSutentacao_Codigo, AV9Artefatos_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1749Artefatos_Codigo = H00N24_A1749Artefatos_Codigo[0];
                  A1750CatalogoSutentacao_Codigo = H00N24_A1750CatalogoSutentacao_Codigo[0];
                  AV17Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               AV16Description = A1751Artefatos_Descricao;
               if ( AV17Exist )
               {
                  AV7AddedKeyList.Add(A1749Artefatos_Codigo, 0);
                  AV5AddedDscList.Add(AV16Description, 0);
               }
               else
               {
                  AV25NotAddedKeyList.Add(A1749Artefatos_Codigo, 0);
                  AV23NotAddedDscList.Add(AV16Description, 0);
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
         edtavAddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddedkeylistxml_Visible), 5, 0)));
         edtavNotaddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddedkeylistxml_Visible), 5, 0)));
         edtavAddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddeddsclistxml_Visible), 5, 0)));
         edtavNotaddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddeddsclistxml_Visible), 5, 0)));
      }

      protected void E12N22( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV30WWPContext) ;
         imgImageassociateselected_Visible = (AV30WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV30WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV30WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV30WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV30WWPContext.gxTpr_Insert||AV30WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25NotAddedKeyList", AV25NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
      }

      public void GXEnter( )
      {
         /* Execute user event: E13N22 */
         E13N22 ();
         if (returnInSub) return;
      }

      protected void E13N22( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV19i = 1;
         AV29Success = true;
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV7AddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV37GXV1));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            GX_msglist.addItem("A: "+StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)+" &AddedKeyList: "+StringUtil.Str( (decimal)(AV7AddedKeyList.Count), 9, 0));
            if ( AV29Success )
            {
               AV17Exist = false;
               /* Using cursor H00N25 */
               pr_default.execute(3, new Object[] {AV11CatalogoSutentacao_Codigo, AV9Artefatos_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A1749Artefatos_Codigo = H00N25_A1749Artefatos_Codigo[0];
                  A1750CatalogoSutentacao_Codigo = H00N25_A1750CatalogoSutentacao_Codigo[0];
                  AV17Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               if ( ! AV17Exist )
               {
                  AV31CatalogoSutentacaoArtefatos = new SdtCatalogoSutentacaoArtefatos(context);
                  AV31CatalogoSutentacaoArtefatos.gxTpr_Catalogosutentacao_codigo = AV11CatalogoSutentacao_Codigo;
                  AV31CatalogoSutentacaoArtefatos.gxTpr_Artefatos_codigo = AV9Artefatos_Codigo;
                  AV31CatalogoSutentacaoArtefatos.Save();
                  if ( ! AV31CatalogoSutentacaoArtefatos.Success() )
                  {
                     AV29Success = false;
                  }
               }
            }
            AV19i = (int)(AV19i+1);
            AV37GXV1 = (int)(AV37GXV1+1);
         }
         AV19i = 1;
         AV39GXV2 = 1;
         while ( AV39GXV2 <= AV25NotAddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV25NotAddedKeyList.GetNumeric(AV39GXV2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            if ( AV29Success )
            {
               AV17Exist = false;
               /* Using cursor H00N26 */
               pr_default.execute(4, new Object[] {AV11CatalogoSutentacao_Codigo, AV9Artefatos_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A1749Artefatos_Codigo = H00N26_A1749Artefatos_Codigo[0];
                  A1750CatalogoSutentacao_Codigo = H00N26_A1750CatalogoSutentacao_Codigo[0];
                  AV17Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
               if ( AV17Exist )
               {
                  AV31CatalogoSutentacaoArtefatos = new SdtCatalogoSutentacaoArtefatos(context);
                  AV31CatalogoSutentacaoArtefatos.Load(AV11CatalogoSutentacao_Codigo, AV9Artefatos_Codigo);
                  if ( AV31CatalogoSutentacaoArtefatos.Success() )
                  {
                     AV31CatalogoSutentacaoArtefatos.Delete();
                  }
                  if ( ! AV31CatalogoSutentacaoArtefatos.Success() )
                  {
                     AV29Success = false;
                  }
               }
            }
            AV19i = (int)(AV19i+1);
            AV39GXV2 = (int)(AV39GXV2+1);
         }
         if ( AV29Success )
         {
            context.CommitDataStores( "WP_AssociarCatalogoSutentacaoArtefatos");
         }
         else
         {
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S142 ();
            if (returnInSub) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25NotAddedKeyList", AV25NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
      }

      protected void E14N22( )
      {
         /* 'Disassociate Selected' Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S152 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25NotAddedKeyList", AV25NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E15N22( )
      {
         /* 'Associate selected' Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S162 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25NotAddedKeyList", AV25NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E16N22( )
      {
         /* 'Associate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S172 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25NotAddedKeyList", AV25NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E17N22( )
      {
         /* 'Disassociate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S172 ();
         if (returnInSub) return;
         AV25NotAddedKeyList = (IGxCollection)(AV7AddedKeyList.Clone());
         AV23NotAddedDscList = (IGxCollection)(AV5AddedDscList.Clone());
         AV5AddedDscList.Clear();
         AV7AddedKeyList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25NotAddedKeyList", AV25NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E18N22( )
      {
         /* Associatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S152 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25NotAddedKeyList", AV25NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E19N22( )
      {
         /* Notassociatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S162 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV25NotAddedKeyList", AV25NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV23NotAddedDscList", AV23NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void S122( )
      {
         /* 'UPDATEASSOCIATIONVARIABLES' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         lstavAssociatedrecords.removeAllItems();
         lstavNotassociatedrecords.removeAllItems();
         AV19i = 1;
         AV41GXV3 = 1;
         while ( AV41GXV3 <= AV7AddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV41GXV3));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            AV16Description = ((String)AV5AddedDscList.Item(AV19i));
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)), StringUtil.Trim( AV16Description), 0);
            AV19i = (int)(AV19i+1);
            AV41GXV3 = (int)(AV41GXV3+1);
         }
         AV19i = 1;
         AV42GXV4 = 1;
         while ( AV42GXV4 <= AV25NotAddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV25NotAddedKeyList.GetNumeric(AV42GXV4));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            AV16Description = ((String)AV23NotAddedDscList.Item(AV19i));
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)), StringUtil.Trim( AV16Description), 0);
            AV19i = (int)(AV19i+1);
            AV42GXV4 = (int)(AV42GXV4+1);
         }
      }

      protected void S142( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV44GXV6 = 1;
         AV43GXV5 = AV13ContratoServicosArtefatos.GetMessages();
         while ( AV44GXV6 <= AV43GXV5.Count )
         {
            AV22Message = ((SdtMessages_Message)AV43GXV5.Item(AV44GXV6));
            if ( AV22Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV22Message.gxTpr_Description);
            }
            AV44GXV6 = (int)(AV44GXV6+1);
         }
      }

      protected void S132( )
      {
         /* 'LOADLISTS' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8AddedKeyListXml)) )
         {
            AV5AddedDscList.FromXml(AV6AddedDscListXml, "Collection");
            AV7AddedKeyList.FromXml(AV8AddedKeyListXml, "Collection");
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26NotAddedKeyListXml)) )
         {
            AV25NotAddedKeyList.FromXml(AV26NotAddedKeyListXml, "Collection");
            AV23NotAddedDscList.FromXml(AV24NotAddedDscListXml, "Collection");
         }
      }

      protected void S112( )
      {
         /* 'SAVELISTS' Routine */
         if ( AV7AddedKeyList.Count > 0 )
         {
            AV8AddedKeyListXml = AV7AddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV6AddedDscListXml = AV5AddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
         }
         else
         {
            AV8AddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV6AddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
         }
         if ( AV25NotAddedKeyList.Count > 0 )
         {
            AV26NotAddedKeyListXml = AV25NotAddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26NotAddedKeyListXml", AV26NotAddedKeyListXml);
            AV24NotAddedDscListXml = AV23NotAddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAddedDscListXml", AV24NotAddedDscListXml);
         }
         else
         {
            AV26NotAddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26NotAddedKeyListXml", AV26NotAddedKeyListXml);
            AV24NotAddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAddedDscListXml", AV24NotAddedDscListXml);
         }
      }

      protected void S172( )
      {
         /* 'ASSOCIATEALL' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV19i = 1;
         AV20InsertIndex = 1;
         AV45GXV7 = 1;
         while ( AV45GXV7 <= AV25NotAddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV25NotAddedKeyList.GetNumeric(AV45GXV7));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            AV16Description = ((String)AV23NotAddedDscList.Item(AV19i));
            while ( ( AV20InsertIndex <= AV5AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV5AddedDscList.Item(AV20InsertIndex)), AV16Description) < 0 ) )
            {
               AV20InsertIndex = (int)(AV20InsertIndex+1);
            }
            AV7AddedKeyList.Add(AV9Artefatos_Codigo, AV20InsertIndex);
            AV5AddedDscList.Add(AV16Description, AV20InsertIndex);
            AV19i = (int)(AV19i+1);
            AV45GXV7 = (int)(AV45GXV7+1);
         }
         AV25NotAddedKeyList.Clear();
         AV23NotAddedDscList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if (returnInSub) return;
      }

      protected void S162( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV19i = 1;
         AV46GXV8 = 1;
         while ( AV46GXV8 <= AV25NotAddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV25NotAddedKeyList.GetNumeric(AV46GXV8));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            if ( AV9Artefatos_Codigo == AV27NotAssociatedRecords )
            {
               if (true) break;
            }
            AV19i = (int)(AV19i+1);
            AV46GXV8 = (int)(AV46GXV8+1);
         }
         if ( AV19i <= AV25NotAddedKeyList.Count )
         {
            AV16Description = ((String)AV23NotAddedDscList.Item(AV19i));
            AV20InsertIndex = 1;
            while ( ( AV20InsertIndex <= AV5AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV5AddedDscList.Item(AV20InsertIndex)), AV16Description) < 0 ) )
            {
               AV20InsertIndex = (int)(AV20InsertIndex+1);
            }
            AV7AddedKeyList.Add(AV27NotAssociatedRecords, AV20InsertIndex);
            AV5AddedDscList.Add(AV16Description, AV20InsertIndex);
            AV25NotAddedKeyList.RemoveItem(AV19i);
            AV23NotAddedDscList.RemoveItem(AV19i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
      }

      protected void S152( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV19i = 1;
         AV47GXV9 = 1;
         while ( AV47GXV9 <= AV7AddedKeyList.Count )
         {
            AV9Artefatos_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV47GXV9));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Artefatos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Artefatos_Codigo), 6, 0)));
            if ( AV9Artefatos_Codigo == AV10AssociatedRecords )
            {
               if (true) break;
            }
            AV19i = (int)(AV19i+1);
            AV47GXV9 = (int)(AV47GXV9+1);
         }
         if ( AV19i <= AV7AddedKeyList.Count )
         {
            AV16Description = ((String)AV5AddedDscList.Item(AV19i));
            AV20InsertIndex = 1;
            while ( ( AV20InsertIndex <= AV23NotAddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV23NotAddedDscList.Item(AV20InsertIndex)), AV16Description) < 0 ) )
            {
               AV20InsertIndex = (int)(AV20InsertIndex+1);
            }
            AV25NotAddedKeyList.Add(AV10AssociatedRecords, AV20InsertIndex);
            AV23NotAddedDscList.Add(AV16Description, AV20InsertIndex);
            AV7AddedKeyList.RemoveItem(AV19i);
            AV5AddedDscList.RemoveItem(AV19i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E20N22( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_N22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_8_N22( true) ;
         }
         else
         {
            wb_table2_8_N22( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_N22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_15_N22( true) ;
         }
         else
         {
            wb_table3_15_N22( false) ;
         }
         return  ;
      }

      protected void wb_table3_15_N22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_47_N22( true) ;
         }
         else
         {
            wb_table4_47_N22( false) ;
         }
         return  ;
      }

      protected void wb_table4_47_N22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_N22e( true) ;
         }
         else
         {
            wb_table1_2_N22e( false) ;
         }
      }

      protected void wb_table4_47_N22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_47_N22e( true) ;
         }
         else
         {
            wb_table4_47_N22e( false) ;
         }
      }

      protected void wb_table3_15_N22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table5_18_N22( true) ;
         }
         else
         {
            wb_table5_18_N22( false) ;
         }
         return  ;
      }

      protected void wb_table5_18_N22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_15_N22e( true) ;
         }
         else
         {
            wb_table3_15_N22e( false) ;
         }
      }

      protected void wb_table5_18_N22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, "Artefatos N�o Associados", "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, "Artefatos Associados", "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavNotassociatedrecords, lstavNotassociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0)), 2, lstavNotassociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27NotAssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", (String)(lstavNotassociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table6_30_N22( true) ;
         }
         else
         {
            wb_table6_30_N22( false) ;
         }
         return  ;
      }

      protected void wb_table6_30_N22e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavAssociatedrecords, lstavAssociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0)), 2, lstavAssociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 6, "em", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10AssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", (String)(lstavAssociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_18_N22e( true) ;
         }
         else
         {
            wb_table5_18_N22e( false) ;
         }
      }

      protected void wb_table6_30_N22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_30_N22e( true) ;
         }
         else
         {
            wb_table6_30_N22e( false) ;
         }
      }

      protected void wb_table2_8_N22( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedassociationtitle_Internalname, tblTablemergedassociationtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Associar neste Cat�logo de Sustenta��o", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociarCatalogoSutentacaoArtefatos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_N22e( true) ;
         }
         else
         {
            wb_table2_8_N22e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV11CatalogoSutentacao_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11CatalogoSutentacao_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11CatalogoSutentacao_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCATALOGOSUTENTACAO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV11CatalogoSutentacao_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAN22( ) ;
         WSN22( ) ;
         WEN22( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?176035");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20203119549");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associarcatalogosutentacaoartefatos.js", "?20203119549");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         tblTablemergedassociationtitle_Internalname = "TABLEMERGEDASSOCIATIONTITLE";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         lstavNotassociatedrecords_Internalname = "vNOTASSOCIATEDRECORDS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblTable1_Internalname = "TABLE1";
         lstavAssociatedrecords_Internalname = "vASSOCIATEDRECORDS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavAddedkeylistxml_Internalname = "vADDEDKEYLISTXML";
         edtavNotaddedkeylistxml_Internalname = "vNOTADDEDKEYLISTXML";
         edtavAddeddsclistxml_Internalname = "vADDEDDSCLISTXML";
         edtavNotaddeddsclistxml_Internalname = "vNOTADDEDDSCLISTXML";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         lstavAssociatedrecords_Jsonclick = "";
         lstavNotassociatedrecords_Jsonclick = "";
         bttBtn_confirm_Visible = 1;
         edtavNotaddeddsclistxml_Visible = 1;
         edtavAddeddsclistxml_Visible = 1;
         edtavNotaddedkeylistxml_Visible = 1;
         edtavAddedkeylistxml_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'imgImageassociateselected_Visible',ctrl:'IMAGEASSOCIATESELECTED',prop:'Visible'},{av:'imgImageassociateall_Visible',ctrl:'IMAGEASSOCIATEALL',prop:'Visible'},{av:'imgImagedisassociateselected_Visible',ctrl:'IMAGEDISASSOCIATESELECTED',prop:'Visible'},{av:'imgImagedisassociateall_Visible',ctrl:'IMAGEDISASSOCIATEALL',prop:'Visible'},{ctrl:'BTN_CONFIRM',prop:'Visible'},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV27NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("ENTER","{handler:'E13N22',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'A1750CatalogoSutentacao_Codigo',fld:'CATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11CatalogoSutentacao_Codigo',fld:'vCATALOGOSUTENTACAO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1749Artefatos_Codigo',fld:'ARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV13ContratoServicosArtefatos',fld:'vCONTRATOSERVICOSARTEFATOS',pic:'',nv:null}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E14N22',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV27NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E15N22',iparms:[{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV27NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV27NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E16N22',iparms:[{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV27NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E17N22',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV27NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VASSOCIATEDRECORDS.DBLCLICK","{handler:'E18N22',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV27NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VNOTASSOCIATEDRECORDS.DBLCLICK","{handler:'E19N22',iparms:[{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV27NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV9Artefatos_Codigo',fld:'vARTEFATOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV25NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV23NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV26NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV24NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV10AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV27NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV7AddedKeyList = new GxSimpleCollection();
         AV5AddedDscList = new GxSimpleCollection();
         AV25NotAddedKeyList = new GxSimpleCollection();
         AV23NotAddedDscList = new GxSimpleCollection();
         AV13ContratoServicosArtefatos = new SdtContratoServicosArtefatos(context);
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV8AddedKeyListXml = "";
         AV26NotAddedKeyListXml = "";
         AV6AddedDscListXml = "";
         AV24NotAddedDscListXml = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV30WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV18HTTPRequest = new GxHttpRequest( context);
         scmdbuf = "";
         H00N22_A1750CatalogoSutentacao_Codigo = new int[1] ;
         H00N23_A1749Artefatos_Codigo = new int[1] ;
         H00N23_A1751Artefatos_Descricao = new String[] {""} ;
         A1751Artefatos_Descricao = "";
         H00N24_A1749Artefatos_Codigo = new int[1] ;
         H00N24_A1750CatalogoSutentacao_Codigo = new int[1] ;
         AV16Description = "";
         H00N25_A1749Artefatos_Codigo = new int[1] ;
         H00N25_A1750CatalogoSutentacao_Codigo = new int[1] ;
         AV31CatalogoSutentacaoArtefatos = new SdtCatalogoSutentacaoArtefatos(context);
         H00N26_A1749Artefatos_Codigo = new int[1] ;
         H00N26_A1750CatalogoSutentacao_Codigo = new int[1] ;
         AV43GXV5 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV22Message = new SdtMessages_Message(context);
         sStyleString = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         lblAssociationtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associarcatalogosutentacaoartefatos__default(),
            new Object[][] {
                new Object[] {
               H00N22_A1750CatalogoSutentacao_Codigo
               }
               , new Object[] {
               H00N23_A1749Artefatos_Codigo, H00N23_A1751Artefatos_Descricao
               }
               , new Object[] {
               H00N24_A1749Artefatos_Codigo, H00N24_A1750CatalogoSutentacao_Codigo
               }
               , new Object[] {
               H00N25_A1749Artefatos_Codigo, H00N25_A1750CatalogoSutentacao_Codigo
               }
               , new Object[] {
               H00N26_A1749Artefatos_Codigo, H00N26_A1750CatalogoSutentacao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV34GXLvl4 ;
      private short nGXWrapped ;
      private int AV11CatalogoSutentacao_Codigo ;
      private int wcpOAV11CatalogoSutentacao_Codigo ;
      private int A1750CatalogoSutentacao_Codigo ;
      private int A1749Artefatos_Codigo ;
      private int edtavAddedkeylistxml_Visible ;
      private int edtavNotaddedkeylistxml_Visible ;
      private int edtavAddeddsclistxml_Visible ;
      private int edtavNotaddeddsclistxml_Visible ;
      private int AV27NotAssociatedRecords ;
      private int AV10AssociatedRecords ;
      private int AV9Artefatos_Codigo ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV19i ;
      private int AV37GXV1 ;
      private int AV39GXV2 ;
      private int AV41GXV3 ;
      private int AV42GXV4 ;
      private int AV44GXV6 ;
      private int AV20InsertIndex ;
      private int AV45GXV7 ;
      private int AV46GXV8 ;
      private int AV47GXV9 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String edtavAddedkeylistxml_Internalname ;
      private String edtavNotaddedkeylistxml_Internalname ;
      private String edtavAddeddsclistxml_Internalname ;
      private String edtavNotaddeddsclistxml_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String lstavNotassociatedrecords_Internalname ;
      private String lstavAssociatedrecords_Internalname ;
      private String scmdbuf ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String lstavNotassociatedrecords_Jsonclick ;
      private String lstavAssociatedrecords_Jsonclick ;
      private String tblTable1_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private String tblTablemergedassociationtitle_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV17Exist ;
      private bool AV29Success ;
      private String AV8AddedKeyListXml ;
      private String AV26NotAddedKeyListXml ;
      private String AV6AddedDscListXml ;
      private String AV24NotAddedDscListXml ;
      private String A1751Artefatos_Descricao ;
      private String AV16Description ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXListbox lstavNotassociatedrecords ;
      private GXListbox lstavAssociatedrecords ;
      private IDataStoreProvider pr_default ;
      private int[] H00N22_A1750CatalogoSutentacao_Codigo ;
      private int[] H00N23_A1749Artefatos_Codigo ;
      private String[] H00N23_A1751Artefatos_Descricao ;
      private int[] H00N24_A1749Artefatos_Codigo ;
      private int[] H00N24_A1750CatalogoSutentacao_Codigo ;
      private int[] H00N25_A1749Artefatos_Codigo ;
      private int[] H00N25_A1750CatalogoSutentacao_Codigo ;
      private int[] H00N26_A1749Artefatos_Codigo ;
      private int[] H00N26_A1750CatalogoSutentacao_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV18HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV7AddedKeyList ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV25NotAddedKeyList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV5AddedDscList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23NotAddedDscList ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV43GXV5 ;
      private SdtContratoServicosArtefatos AV13ContratoServicosArtefatos ;
      private SdtMessages_Message AV22Message ;
      private wwpbaseobjects.SdtWWPContext AV30WWPContext ;
      private SdtCatalogoSutentacaoArtefatos AV31CatalogoSutentacaoArtefatos ;
   }

   public class wp_associarcatalogosutentacaoartefatos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00N22 ;
          prmH00N22 = new Object[] {
          new Object[] {"@AV11CatalogoSutentacao_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00N23 ;
          prmH00N23 = new Object[] {
          } ;
          Object[] prmH00N24 ;
          prmH00N24 = new Object[] {
          new Object[] {"@AV11CatalogoSutentacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00N25 ;
          prmH00N25 = new Object[] {
          new Object[] {"@AV11CatalogoSutentacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00N26 ;
          prmH00N26 = new Object[] {
          new Object[] {"@AV11CatalogoSutentacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Artefatos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00N22", "SELECT [CatalogoSutentacao_Codigo] FROM [CatalogoSutentacao] WITH (NOLOCK) WHERE [CatalogoSutentacao_Codigo] = @AV11CatalogoSutentacao_Codigo ORDER BY [CatalogoSutentacao_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N22,1,0,false,true )
             ,new CursorDef("H00N23", "SELECT [Artefatos_Codigo], [Artefatos_Descricao] FROM [Artefatos] WITH (NOLOCK) ORDER BY [Artefatos_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N23,100,0,true,false )
             ,new CursorDef("H00N24", "SELECT [Artefatos_Codigo], [CatalogoSutentacao_Codigo] FROM [CatalogoSutentacaoArtefatos] WITH (NOLOCK) WHERE [CatalogoSutentacao_Codigo] = @AV11CatalogoSutentacao_Codigo and [Artefatos_Codigo] = @AV9Artefatos_Codigo ORDER BY [CatalogoSutentacao_Codigo], [Artefatos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N24,1,0,false,true )
             ,new CursorDef("H00N25", "SELECT [Artefatos_Codigo], [CatalogoSutentacao_Codigo] FROM [CatalogoSutentacaoArtefatos] WITH (NOLOCK) WHERE [CatalogoSutentacao_Codigo] = @AV11CatalogoSutentacao_Codigo and [Artefatos_Codigo] = @AV9Artefatos_Codigo ORDER BY [CatalogoSutentacao_Codigo], [Artefatos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N25,1,0,false,true )
             ,new CursorDef("H00N26", "SELECT [Artefatos_Codigo], [CatalogoSutentacao_Codigo] FROM [CatalogoSutentacaoArtefatos] WITH (NOLOCK) WHERE [CatalogoSutentacao_Codigo] = @AV11CatalogoSutentacao_Codigo and [Artefatos_Codigo] = @AV9Artefatos_Codigo ORDER BY [CatalogoSutentacao_Codigo], [Artefatos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00N26,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
